﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// IntenseVibration
struct IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70;
// MoreMountains.NiceVibrations.NiceVibrationsDemoManager
struct NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<System.Int64[]>
struct List_1_t64142247412666BB650346DC5C873E6B6BE631A5;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
// System.Int64[][]
struct Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TestIntenseVibration
struct TestIntenseVibration_tB3B9456153E01062B1FCD9F20943D0897E283741;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.TextGenerator
struct TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.FontData
struct FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;

extern RuntimeClass* AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8_il2cpp_TypeInfo_var;
extern RuntimeClass* GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
extern RuntimeClass* Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F_il2cpp_TypeInfo_var;
extern RuntimeClass* Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436_il2cpp_TypeInfo_var;
extern RuntimeClass* IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t64142247412666BB650346DC5C873E6B6BE631A5_il2cpp_TypeInfo_var;
extern RuntimeClass* MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral0BA35307786FF30F0C61E3EAEBAF855BEB8B01FD;
extern String_t* _stringLiteral13F322329E5BEFA04267118213843ABE4D213DA3;
extern String_t* _stringLiteral2E505E500D2290AFA21D9C3EE996726E11732B7C;
extern String_t* _stringLiteral3B9EBB5B58C057AC546538D3297A4BA7FF7D646D;
extern String_t* _stringLiteral3BC15C8AAE3E4124DD409035F32EA2FD6835EFC9;
extern String_t* _stringLiteral4485E86CC86283B27A9E8CE1A03563A635217EF8;
extern String_t* _stringLiteral4FD0653C4F2AEF3B19A3C145BBDC5F4740715A09;
extern String_t* _stringLiteral56CE46DDBD9745CB436EA69D5734317961670909;
extern String_t* _stringLiteral77DFD2135F4DB726C47299BB55BE26F7F4525A46;
extern String_t* _stringLiteral888519795321A07D068DA9B34F02755F71FA03F4;
extern String_t* _stringLiteral97DEE6B586983AC010EC6F2D8AE6B8CC23893478;
extern String_t* _stringLiteral9B5199A3391A89EB28C933EACD494725617C3368;
extern String_t* _stringLiteralAB156EC1176CE1DCF8D64EC957AAC47017C315D9;
extern String_t* _stringLiteralADCB524304C732A6BAE34C5F960A610899CD4EC9;
extern String_t* _stringLiteralBF32F43934B1ADBDAD6D91476DB7CD60A8DC2948;
extern String_t* _stringLiteralC9AF0AA1D7A79E2D3297CF71D4FC61B36079D6C6;
extern String_t* _stringLiteralCDEED05B83A5AF1EA0B1787714584BBB8E8613E0;
extern String_t* _stringLiteralDF66CFC8A268191434B45AE84CFA9A3AD1C9DC1E;
extern String_t* _stringLiteralE7064F0B80F61DBC65915311032D27BAA569AE2A;
extern String_t* _stringLiteralF83B6FE3AEBF13744E866019556D9129CD7A55BE;
extern const RuntimeMethod* AndroidJavaObject_CallStatic_TisAndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E_mD264F5FC6C7BCCE95ABC6BF2B9EC08972F321AF4_RuntimeMethod_var;
extern const RuntimeMethod* Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66_RuntimeMethod_var;
extern const RuntimeMethod* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisIntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_m80457CF2B7F5EF8C427FE7196CC5ACD6CBF5F63A_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1E56AB456B3256FCB45CE87A2AC761DB7A87B4D8_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m14ECA4A74FB6A74C49204B46D7E1B4BFA9C81AFE_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_mEEB799CFF217CF39821D8D6D47D5F89FBBA8C3A9_RuntimeMethod_var;
extern const uint32_t IntenseVibration_Start_mCB5E610C82D0500BF0ABAC2F9CD188037084BE05_MetadataUsageId;
extern const uint32_t IntenseVibration_Vibrate_mB04B291B2CC37CCEC962BDDCB7B552FC200330CD_MetadataUsageId;
extern const uint32_t IntenseVibration__ctor_mE6F82C9F3206E2DCF52C166D85319A6C1CF5E843_MetadataUsageId;
extern const uint32_t IntenseVibration_get_Instance_mFE116297FF4A991B04B37449A9FDAD9AFDE620F3_MetadataUsageId;
extern const uint32_t MMVibrationManager_AndroidCancelVibrations_m8132DDA84F6DE4BD971BA4197D5D183F1B4CBA65_MetadataUsageId;
extern const uint32_t MMVibrationManager_AndroidSDKVersion_m9B4DB79A416324F8E935707AB4074CF830EC5C44_MetadataUsageId;
extern const uint32_t MMVibrationManager_AndroidVibrate_m361B2E7774207B353DC5F02F2F5DE04B5699D75B_MetadataUsageId;
extern const uint32_t MMVibrationManager_AndroidVibrate_m65B2738FAA0092B96FB40359E981213F5A58AFB5_MetadataUsageId;
extern const uint32_t MMVibrationManager_AndroidVibrate_m740729B047BB6785CCEE5EE88A6D63277D5ED85B_MetadataUsageId;
extern const uint32_t MMVibrationManager_AndroidVibrate_mF100214FA12779A238D21B4FE7F1767E40BEC14B_MetadataUsageId;
extern const uint32_t MMVibrationManager_Haptic_m5D576554453FE58B294234C1881EA9C3F1CF1A59_MetadataUsageId;
extern const uint32_t MMVibrationManager_Vibrate_m9D0D2EA4955F2FB16DA913325D5E136B8653FC62_MetadataUsageId;
extern const uint32_t MMVibrationManager_VibrationEffectClassInitialization_mF4742F9FC6D4D62793BE69ED4E19119D252BCD8A_MetadataUsageId;
extern const uint32_t MMVibrationManager__cctor_m77BA09387A36B7FCC502BF60DD37E65F652B13A1_MetadataUsageId;
extern const uint32_t MMVibrationManager_iOSInitializeHaptics_m8284AAE3DB8B6D2D96B029918B27F4B4A88B812F_MetadataUsageId;
extern const uint32_t MMVibrationManager_iOSReleaseHaptics_m7FCFC88A0144455308E7B37BDF9B8782817B41D0_MetadataUsageId;
extern const uint32_t MMVibrationManager_iOSTriggerHaptics_m84712A2A62E3F9FAC08D75E94A10092760DB53E1_MetadataUsageId;
extern const uint32_t NiceVibrationsDemoManager_Awake_m9481861A5E62C75D546E3C5E7A0414CFCCA9CBB5_MetadataUsageId;
extern const uint32_t NiceVibrationsDemoManager_DisplayInformation_m530724622BC5BCA310E744B970250A345A6BDADE_MetadataUsageId;
extern const uint32_t NiceVibrationsDemoManager_OnDisable_mEEBD28B14C1116E99C8AF8B1310D656920216526_MetadataUsageId;
extern const uint32_t NiceVibrationsDemoManager_TriggerFailure_m00C3D424846AEC1B18ADFE0B4544B0DD00C9B3A7_MetadataUsageId;
extern const uint32_t NiceVibrationsDemoManager_TriggerHeavyImpact_mD5892EDCAA93CD8D924012FA479E05131FD05E50_MetadataUsageId;
extern const uint32_t NiceVibrationsDemoManager_TriggerLightImpact_mCACCE403D0779D73B39E03CA79243F8B2EE33CC9_MetadataUsageId;
extern const uint32_t NiceVibrationsDemoManager_TriggerMediumImpact_mF9EAA0A8C365F211B94926336728B8F2B3F950D0_MetadataUsageId;
extern const uint32_t NiceVibrationsDemoManager_TriggerSelection_mDF6FE01A2C6623CE6F2C7FB95D6CD3EC434A72AF_MetadataUsageId;
extern const uint32_t NiceVibrationsDemoManager_TriggerSuccess_m4D116451BB8BF174B4FE2C8D394FAA8CACAD94A8_MetadataUsageId;
extern const uint32_t NiceVibrationsDemoManager_TriggerVibrate_m09ED9399BCF412848A9C23BEEECFD6F2AF13CB57_MetadataUsageId;
extern const uint32_t NiceVibrationsDemoManager_TriggerWarning_m9AF376C7F63F529FA5CD4C929BD2445B66D53198_MetadataUsageId;
extern const uint32_t TestIntenseVibration_OnGUI_m69BE516E2D393C0C9E0EFB9E73B01C3532BFF647_MetadataUsageId;

struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B;


#ifndef U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#define U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EASYVIBRO_TB0B9B23A81DBB11562670BA10599582E401DDFB3_H
#define EASYVIBRO_TB0B9B23A81DBB11562670BA10599582E401DDFB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyVibro
struct  EasyVibro_tB0B9B23A81DBB11562670BA10599582E401DDFB3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASYVIBRO_TB0B9B23A81DBB11562670BA10599582E401DDFB3_H
#ifndef MMVIBRATIONMANAGER_TF61AC21066BC0D4EA57104C332CA7C2B52F37263_H
#define MMVIBRATIONMANAGER_TF61AC21066BC0D4EA57104C332CA7C2B52F37263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.NiceVibrations.MMVibrationManager
struct  MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263  : public RuntimeObject
{
public:

public:
};

struct MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields
{
public:
	// System.Int64 MoreMountains.NiceVibrations.MMVibrationManager::LightDuration
	int64_t ___LightDuration_0;
	// System.Int64 MoreMountains.NiceVibrations.MMVibrationManager::MediumDuration
	int64_t ___MediumDuration_1;
	// System.Int64 MoreMountains.NiceVibrations.MMVibrationManager::HeavyDuration
	int64_t ___HeavyDuration_2;
	// System.Int32 MoreMountains.NiceVibrations.MMVibrationManager::LightAmplitude
	int32_t ___LightAmplitude_3;
	// System.Int32 MoreMountains.NiceVibrations.MMVibrationManager::MediumAmplitude
	int32_t ___MediumAmplitude_4;
	// System.Int32 MoreMountains.NiceVibrations.MMVibrationManager::HeavyAmplitude
	int32_t ___HeavyAmplitude_5;
	// System.Int32 MoreMountains.NiceVibrations.MMVibrationManager::_sdkVersion
	int32_t ____sdkVersion_6;
	// System.Int64[] MoreMountains.NiceVibrations.MMVibrationManager::_successPattern
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____successPattern_7;
	// System.Int32[] MoreMountains.NiceVibrations.MMVibrationManager::_successPatternAmplitude
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____successPatternAmplitude_8;
	// System.Int64[] MoreMountains.NiceVibrations.MMVibrationManager::_warningPattern
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____warningPattern_9;
	// System.Int32[] MoreMountains.NiceVibrations.MMVibrationManager::_warningPatternAmplitude
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____warningPatternAmplitude_10;
	// System.Int64[] MoreMountains.NiceVibrations.MMVibrationManager::_failurePattern
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____failurePattern_11;
	// System.Int32[] MoreMountains.NiceVibrations.MMVibrationManager::_failurePatternAmplitude
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____failurePatternAmplitude_12;
	// UnityEngine.AndroidJavaClass MoreMountains.NiceVibrations.MMVibrationManager::UnityPlayer
	AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * ___UnityPlayer_13;
	// UnityEngine.AndroidJavaObject MoreMountains.NiceVibrations.MMVibrationManager::CurrentActivity
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ___CurrentActivity_14;
	// UnityEngine.AndroidJavaObject MoreMountains.NiceVibrations.MMVibrationManager::AndroidVibrator
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ___AndroidVibrator_15;
	// UnityEngine.AndroidJavaClass MoreMountains.NiceVibrations.MMVibrationManager::VibrationEffectClass
	AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * ___VibrationEffectClass_16;
	// UnityEngine.AndroidJavaObject MoreMountains.NiceVibrations.MMVibrationManager::VibrationEffect
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ___VibrationEffect_17;
	// System.Int32 MoreMountains.NiceVibrations.MMVibrationManager::DefaultAmplitude
	int32_t ___DefaultAmplitude_18;
	// System.Boolean MoreMountains.NiceVibrations.MMVibrationManager::iOSHapticsInitialized
	bool ___iOSHapticsInitialized_19;

public:
	inline static int32_t get_offset_of_LightDuration_0() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___LightDuration_0)); }
	inline int64_t get_LightDuration_0() const { return ___LightDuration_0; }
	inline int64_t* get_address_of_LightDuration_0() { return &___LightDuration_0; }
	inline void set_LightDuration_0(int64_t value)
	{
		___LightDuration_0 = value;
	}

	inline static int32_t get_offset_of_MediumDuration_1() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___MediumDuration_1)); }
	inline int64_t get_MediumDuration_1() const { return ___MediumDuration_1; }
	inline int64_t* get_address_of_MediumDuration_1() { return &___MediumDuration_1; }
	inline void set_MediumDuration_1(int64_t value)
	{
		___MediumDuration_1 = value;
	}

	inline static int32_t get_offset_of_HeavyDuration_2() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___HeavyDuration_2)); }
	inline int64_t get_HeavyDuration_2() const { return ___HeavyDuration_2; }
	inline int64_t* get_address_of_HeavyDuration_2() { return &___HeavyDuration_2; }
	inline void set_HeavyDuration_2(int64_t value)
	{
		___HeavyDuration_2 = value;
	}

	inline static int32_t get_offset_of_LightAmplitude_3() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___LightAmplitude_3)); }
	inline int32_t get_LightAmplitude_3() const { return ___LightAmplitude_3; }
	inline int32_t* get_address_of_LightAmplitude_3() { return &___LightAmplitude_3; }
	inline void set_LightAmplitude_3(int32_t value)
	{
		___LightAmplitude_3 = value;
	}

	inline static int32_t get_offset_of_MediumAmplitude_4() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___MediumAmplitude_4)); }
	inline int32_t get_MediumAmplitude_4() const { return ___MediumAmplitude_4; }
	inline int32_t* get_address_of_MediumAmplitude_4() { return &___MediumAmplitude_4; }
	inline void set_MediumAmplitude_4(int32_t value)
	{
		___MediumAmplitude_4 = value;
	}

	inline static int32_t get_offset_of_HeavyAmplitude_5() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___HeavyAmplitude_5)); }
	inline int32_t get_HeavyAmplitude_5() const { return ___HeavyAmplitude_5; }
	inline int32_t* get_address_of_HeavyAmplitude_5() { return &___HeavyAmplitude_5; }
	inline void set_HeavyAmplitude_5(int32_t value)
	{
		___HeavyAmplitude_5 = value;
	}

	inline static int32_t get_offset_of__sdkVersion_6() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____sdkVersion_6)); }
	inline int32_t get__sdkVersion_6() const { return ____sdkVersion_6; }
	inline int32_t* get_address_of__sdkVersion_6() { return &____sdkVersion_6; }
	inline void set__sdkVersion_6(int32_t value)
	{
		____sdkVersion_6 = value;
	}

	inline static int32_t get_offset_of__successPattern_7() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____successPattern_7)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__successPattern_7() const { return ____successPattern_7; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__successPattern_7() { return &____successPattern_7; }
	inline void set__successPattern_7(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____successPattern_7 = value;
		Il2CppCodeGenWriteBarrier((&____successPattern_7), value);
	}

	inline static int32_t get_offset_of__successPatternAmplitude_8() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____successPatternAmplitude_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__successPatternAmplitude_8() const { return ____successPatternAmplitude_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__successPatternAmplitude_8() { return &____successPatternAmplitude_8; }
	inline void set__successPatternAmplitude_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____successPatternAmplitude_8 = value;
		Il2CppCodeGenWriteBarrier((&____successPatternAmplitude_8), value);
	}

	inline static int32_t get_offset_of__warningPattern_9() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____warningPattern_9)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__warningPattern_9() const { return ____warningPattern_9; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__warningPattern_9() { return &____warningPattern_9; }
	inline void set__warningPattern_9(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____warningPattern_9 = value;
		Il2CppCodeGenWriteBarrier((&____warningPattern_9), value);
	}

	inline static int32_t get_offset_of__warningPatternAmplitude_10() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____warningPatternAmplitude_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__warningPatternAmplitude_10() const { return ____warningPatternAmplitude_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__warningPatternAmplitude_10() { return &____warningPatternAmplitude_10; }
	inline void set__warningPatternAmplitude_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____warningPatternAmplitude_10 = value;
		Il2CppCodeGenWriteBarrier((&____warningPatternAmplitude_10), value);
	}

	inline static int32_t get_offset_of__failurePattern_11() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____failurePattern_11)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__failurePattern_11() const { return ____failurePattern_11; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__failurePattern_11() { return &____failurePattern_11; }
	inline void set__failurePattern_11(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____failurePattern_11 = value;
		Il2CppCodeGenWriteBarrier((&____failurePattern_11), value);
	}

	inline static int32_t get_offset_of__failurePatternAmplitude_12() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____failurePatternAmplitude_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__failurePatternAmplitude_12() const { return ____failurePatternAmplitude_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__failurePatternAmplitude_12() { return &____failurePatternAmplitude_12; }
	inline void set__failurePatternAmplitude_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____failurePatternAmplitude_12 = value;
		Il2CppCodeGenWriteBarrier((&____failurePatternAmplitude_12), value);
	}

	inline static int32_t get_offset_of_UnityPlayer_13() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___UnityPlayer_13)); }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * get_UnityPlayer_13() const { return ___UnityPlayer_13; }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 ** get_address_of_UnityPlayer_13() { return &___UnityPlayer_13; }
	inline void set_UnityPlayer_13(AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * value)
	{
		___UnityPlayer_13 = value;
		Il2CppCodeGenWriteBarrier((&___UnityPlayer_13), value);
	}

	inline static int32_t get_offset_of_CurrentActivity_14() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___CurrentActivity_14)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get_CurrentActivity_14() const { return ___CurrentActivity_14; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of_CurrentActivity_14() { return &___CurrentActivity_14; }
	inline void set_CurrentActivity_14(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		___CurrentActivity_14 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentActivity_14), value);
	}

	inline static int32_t get_offset_of_AndroidVibrator_15() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___AndroidVibrator_15)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get_AndroidVibrator_15() const { return ___AndroidVibrator_15; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of_AndroidVibrator_15() { return &___AndroidVibrator_15; }
	inline void set_AndroidVibrator_15(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		___AndroidVibrator_15 = value;
		Il2CppCodeGenWriteBarrier((&___AndroidVibrator_15), value);
	}

	inline static int32_t get_offset_of_VibrationEffectClass_16() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___VibrationEffectClass_16)); }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * get_VibrationEffectClass_16() const { return ___VibrationEffectClass_16; }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 ** get_address_of_VibrationEffectClass_16() { return &___VibrationEffectClass_16; }
	inline void set_VibrationEffectClass_16(AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * value)
	{
		___VibrationEffectClass_16 = value;
		Il2CppCodeGenWriteBarrier((&___VibrationEffectClass_16), value);
	}

	inline static int32_t get_offset_of_VibrationEffect_17() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___VibrationEffect_17)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get_VibrationEffect_17() const { return ___VibrationEffect_17; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of_VibrationEffect_17() { return &___VibrationEffect_17; }
	inline void set_VibrationEffect_17(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		___VibrationEffect_17 = value;
		Il2CppCodeGenWriteBarrier((&___VibrationEffect_17), value);
	}

	inline static int32_t get_offset_of_DefaultAmplitude_18() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___DefaultAmplitude_18)); }
	inline int32_t get_DefaultAmplitude_18() const { return ___DefaultAmplitude_18; }
	inline int32_t* get_address_of_DefaultAmplitude_18() { return &___DefaultAmplitude_18; }
	inline void set_DefaultAmplitude_18(int32_t value)
	{
		___DefaultAmplitude_18 = value;
	}

	inline static int32_t get_offset_of_iOSHapticsInitialized_19() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___iOSHapticsInitialized_19)); }
	inline bool get_iOSHapticsInitialized_19() const { return ___iOSHapticsInitialized_19; }
	inline bool* get_address_of_iOSHapticsInitialized_19() { return &___iOSHapticsInitialized_19; }
	inline void set_iOSHapticsInitialized_19(bool value)
	{
		___iOSHapticsInitialized_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMVIBRATIONMANAGER_TF61AC21066BC0D4EA57104C332CA7C2B52F37263_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T64142247412666BB650346DC5C873E6B6BE631A5_H
#define LIST_1_T64142247412666BB650346DC5C873E6B6BE631A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int64[]>
struct  List_1_t64142247412666BB650346DC5C873E6B6BE631A5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t64142247412666BB650346DC5C873E6B6BE631A5, ____items_1)); }
	inline Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030* get__items_1() const { return ____items_1; }
	inline Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t64142247412666BB650346DC5C873E6B6BE631A5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t64142247412666BB650346DC5C873E6B6BE631A5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t64142247412666BB650346DC5C873E6B6BE631A5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t64142247412666BB650346DC5C873E6B6BE631A5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t64142247412666BB650346DC5C873E6B6BE631A5_StaticFields, ____emptyArray_5)); }
	inline Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T64142247412666BB650346DC5C873E6B6BE631A5_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ANDROIDJAVAOBJECT_T5B3829FB6E1DBC020F5BA17846F1351EAA982F8E_H
#define ANDROIDJAVAOBJECT_T5B3829FB6E1DBC020F5BA17846F1351EAA982F8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaObject
struct  AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAOBJECT_T5B3829FB6E1DBC020F5BA17846F1351EAA982F8E_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#define INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef ANDROIDJAVACLASS_TFC9C1064BF4849691FEDC988743C0C271C62FDC8_H
#define ANDROIDJAVACLASS_TFC9C1064BF4849691FEDC988743C0C271C62FDC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaClass
struct  AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8  : public AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVACLASS_TFC9C1064BF4849691FEDC988743C0C271C62FDC8_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef HAPTICTYPES_T5841156CF72EED0C5C47C9245C09D2250C9CAAFD_H
#define HAPTICTYPES_T5841156CF72EED0C5C47C9245C09D2250C9CAAFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.NiceVibrations.HapticTypes
struct  HapticTypes_t5841156CF72EED0C5C47C9245C09D2250C9CAAFD 
{
public:
	// System.Int32 MoreMountains.NiceVibrations.HapticTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HapticTypes_t5841156CF72EED0C5C47C9245C09D2250C9CAAFD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HAPTICTYPES_T5841156CF72EED0C5C47C9245C09D2250C9CAAFD_H
#ifndef TYPE_T1060D19522CDA0F7C9A26733BE1E8C8E20AC1278_H
#define TYPE_T1060D19522CDA0F7C9A26733BE1E8C8E20AC1278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayoutOption/Type
struct  Type_t1060D19522CDA0F7C9A26733BE1E8C8E20AC1278 
{
public:
	// System.Int32 UnityEngine.GUILayoutOption/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t1060D19522CDA0F7C9A26733BE1E8C8E20AC1278, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1060D19522CDA0F7C9A26733BE1E8C8E20AC1278_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RUNTIMEPLATFORM_TD5F5737C1BBBCBB115EB104DF2B7876387E80132_H
#define RUNTIMEPLATFORM_TD5F5737C1BBBCBB115EB104DF2B7876387E80132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_TD5F5737C1BBBCBB115EB104DF2B7876387E80132_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GUILAYOUTOPTION_T27A0221AC2F6F53E7B89310FD19F51C565D835A6_H
#define GUILAYOUTOPTION_T27A0221AC2F6F53E7B89310FD19F51C565D835A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayoutOption
struct  GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6  : public RuntimeObject
{
public:
	// UnityEngine.GUILayoutOption/Type UnityEngine.GUILayoutOption::type
	int32_t ___type_0;
	// System.Object UnityEngine.GUILayoutOption::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTOPTION_T27A0221AC2F6F53E7B89310FD19F51C565D835A6_H
#ifndef GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#define GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef INTENSEVIBRATION_TD697DD1DE3CF0E007232BB307CA8479A6F067B70_H
#define INTENSEVIBRATION_TD697DD1DE3CF0E007232BB307CA8479A6F067B70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IntenseVibration
struct  IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single IntenseVibration::mVibrateTimeLeft
	float ___mVibrateTimeLeft_6;
	// System.Collections.Generic.List`1<System.Int64[]> IntenseVibration::mPatterns
	List_1_t64142247412666BB650346DC5C873E6B6BE631A5 * ___mPatterns_7;

public:
	inline static int32_t get_offset_of_mVibrateTimeLeft_6() { return static_cast<int32_t>(offsetof(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70, ___mVibrateTimeLeft_6)); }
	inline float get_mVibrateTimeLeft_6() const { return ___mVibrateTimeLeft_6; }
	inline float* get_address_of_mVibrateTimeLeft_6() { return &___mVibrateTimeLeft_6; }
	inline void set_mVibrateTimeLeft_6(float value)
	{
		___mVibrateTimeLeft_6 = value;
	}

	inline static int32_t get_offset_of_mPatterns_7() { return static_cast<int32_t>(offsetof(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70, ___mPatterns_7)); }
	inline List_1_t64142247412666BB650346DC5C873E6B6BE631A5 * get_mPatterns_7() const { return ___mPatterns_7; }
	inline List_1_t64142247412666BB650346DC5C873E6B6BE631A5 ** get_address_of_mPatterns_7() { return &___mPatterns_7; }
	inline void set_mPatterns_7(List_1_t64142247412666BB650346DC5C873E6B6BE631A5 * value)
	{
		___mPatterns_7 = value;
		Il2CppCodeGenWriteBarrier((&___mPatterns_7), value);
	}
};

struct IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_StaticFields
{
public:
	// IntenseVibration IntenseVibration::mInstance
	IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * ___mInstance_4;
	// UnityEngine.AndroidJavaObject IntenseVibration::mJVibrator
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ___mJVibrator_5;

public:
	inline static int32_t get_offset_of_mInstance_4() { return static_cast<int32_t>(offsetof(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_StaticFields, ___mInstance_4)); }
	inline IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * get_mInstance_4() const { return ___mInstance_4; }
	inline IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 ** get_address_of_mInstance_4() { return &___mInstance_4; }
	inline void set_mInstance_4(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * value)
	{
		___mInstance_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_4), value);
	}

	inline static int32_t get_offset_of_mJVibrator_5() { return static_cast<int32_t>(offsetof(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_StaticFields, ___mJVibrator_5)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get_mJVibrator_5() const { return ___mJVibrator_5; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of_mJVibrator_5() { return &___mJVibrator_5; }
	inline void set_mJVibrator_5(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		___mJVibrator_5 = value;
		Il2CppCodeGenWriteBarrier((&___mJVibrator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTENSEVIBRATION_TD697DD1DE3CF0E007232BB307CA8479A6F067B70_H
#ifndef NICEVIBRATIONSDEMOMANAGER_T2F48E6AE3AC20158BC1CF621740B795B6BF1D07E_H
#define NICEVIBRATIONSDEMOMANAGER_T2F48E6AE3AC20158BC1CF621740B795B6BF1D07E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.NiceVibrations.NiceVibrationsDemoManager
struct  NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text MoreMountains.NiceVibrations.NiceVibrationsDemoManager::DebugTextBox
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___DebugTextBox_4;
	// System.String MoreMountains.NiceVibrations.NiceVibrationsDemoManager::_debugString
	String_t* ____debugString_5;
	// System.String MoreMountains.NiceVibrations.NiceVibrationsDemoManager::_platformString
	String_t* ____platformString_6;

public:
	inline static int32_t get_offset_of_DebugTextBox_4() { return static_cast<int32_t>(offsetof(NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E, ___DebugTextBox_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_DebugTextBox_4() const { return ___DebugTextBox_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_DebugTextBox_4() { return &___DebugTextBox_4; }
	inline void set_DebugTextBox_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___DebugTextBox_4 = value;
		Il2CppCodeGenWriteBarrier((&___DebugTextBox_4), value);
	}

	inline static int32_t get_offset_of__debugString_5() { return static_cast<int32_t>(offsetof(NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E, ____debugString_5)); }
	inline String_t* get__debugString_5() const { return ____debugString_5; }
	inline String_t** get_address_of__debugString_5() { return &____debugString_5; }
	inline void set__debugString_5(String_t* value)
	{
		____debugString_5 = value;
		Il2CppCodeGenWriteBarrier((&____debugString_5), value);
	}

	inline static int32_t get_offset_of__platformString_6() { return static_cast<int32_t>(offsetof(NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E, ____platformString_6)); }
	inline String_t* get__platformString_6() const { return ____platformString_6; }
	inline String_t** get_address_of__platformString_6() { return &____platformString_6; }
	inline void set__platformString_6(String_t* value)
	{
		____platformString_6 = value;
		Il2CppCodeGenWriteBarrier((&____platformString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NICEVIBRATIONSDEMOMANAGER_T2F48E6AE3AC20158BC1CF621740B795B6BF1D07E_H
#ifndef TESTINTENSEVIBRATION_TB3B9456153E01062B1FCD9F20943D0897E283741_H
#define TESTINTENSEVIBRATION_TB3B9456153E01062B1FCD9F20943D0897E283741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestIntenseVibration
struct  TestIntenseVibration_tB3B9456153E01062B1FCD9F20943D0897E283741  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTINTENSEVIBRATION_TB3B9456153E01062B1FCD9F20943D0897E283741_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#define GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifndef MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#define MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_23)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifndef TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
#define TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * ___m_FontData_30;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_31;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCache_32;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCacheForLayout_33;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_35;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ___m_TempVerts_36;

public:
	inline static int32_t get_offset_of_m_FontData_30() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_FontData_30)); }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * get_m_FontData_30() const { return ___m_FontData_30; }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 ** get_address_of_m_FontData_30() { return &___m_FontData_30; }
	inline void set_m_FontData_30(FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * value)
	{
		___m_FontData_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_30), value);
	}

	inline static int32_t get_offset_of_m_Text_31() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_Text_31)); }
	inline String_t* get_m_Text_31() const { return ___m_Text_31; }
	inline String_t** get_address_of_m_Text_31() { return &___m_Text_31; }
	inline void set_m_Text_31(String_t* value)
	{
		___m_Text_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_31), value);
	}

	inline static int32_t get_offset_of_m_TextCache_32() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCache_32)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCache_32() const { return ___m_TextCache_32; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCache_32() { return &___m_TextCache_32; }
	inline void set_m_TextCache_32(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCache_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_32), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_33() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCacheForLayout_33)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCacheForLayout_33() const { return ___m_TextCacheForLayout_33; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCacheForLayout_33() { return &___m_TextCacheForLayout_33; }
	inline void set_m_TextCacheForLayout_33(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCacheForLayout_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_33), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_35() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_DisableFontTextureRebuiltCallback_35)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_35() const { return ___m_DisableFontTextureRebuiltCallback_35; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_35() { return &___m_DisableFontTextureRebuiltCallback_35; }
	inline void set_m_DisableFontTextureRebuiltCallback_35(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_35 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_36() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TempVerts_36)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get_m_TempVerts_36() const { return ___m_TempVerts_36; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of_m_TempVerts_36() { return &___m_TempVerts_36; }
	inline void set_m_TempVerts_36(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		___m_TempVerts_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_36), value);
	}
};

struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultText_34;

public:
	inline static int32_t get_offset_of_s_DefaultText_34() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields, ___s_DefaultText_34)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultText_34() const { return ___s_DefaultText_34; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultText_34() { return &___s_DefaultText_34; }
	inline void set_s_DefaultText_34(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultText_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int64_t m_Items[1];

public:
	inline int64_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int64_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int64_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int64_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int64_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int64_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * m_Items[1];

public:
	inline GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mCB8164FB05F8DCF99E098ADC5E13E96FEF6FC4E9_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::CallStatic<System.Object>(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AndroidJavaObject_CallStatic_TisRuntimeObject_m369AA56D528577AE0E4FF989A3506199111B6167_gshared (AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * __this, String_t* p0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* p1, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Object>()
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared (const RuntimeMethod* method);

// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<IntenseVibration>()
inline IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * GameObject_AddComponent_TisIntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_m80457CF2B7F5EF8C427FE7196CC5ACD6CBF5F63A (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mCB8164FB05F8DCF99E098ADC5E13E96FEF6FC4E9_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<System.Int64[]>::get_Item(System.Int32)
inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* List_1_get_Item_mEEB799CFF217CF39821D8D6D47D5F89FBBA8C3A9 (List_1_t64142247412666BB650346DC5C873E6B6BE631A5 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* (*) (List_1_t64142247412666BB650346DC5C873E6B6BE631A5 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Void IntenseVibration::Vibrate(System.Int64[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration_Vibrate_m7143313618308B2F0A53EB89CD0F4670E4869AAD (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___pattern0, int32_t ___repeat1, const RuntimeMethod* method);
// System.Void IntenseVibration::Init()
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration_Init_m7E06284D4B1F5601718C47D8A8F68D9C76D0DA17 (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int64[]>::Add(!0)
inline void List_1_Add_m1E56AB456B3256FCB45CE87A2AC761DB7A87B4D8 (List_1_t64142247412666BB650346DC5C873E6B6BE631A5 * __this, Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t64142247412666BB650346DC5C873E6B6BE631A5 *, Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F*, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Void IntenseVibration::Cancel()
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration_Cancel_mFC242521004F6AB6D66103E7EF7E87CA54C2657D (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int64[]>::.ctor()
inline void List_1__ctor_m14ECA4A74FB6A74C49204B46D7E1B4BFA9C81AFE (List_1_t64142247412666BB650346DC5C873E6B6BE631A5 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t64142247412666BB650346DC5C873E6B6BE631A5 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Boolean MoreMountains.NiceVibrations.MMVibrationManager::Android()
extern "C" IL2CPP_METHOD_ATTR bool MMVibrationManager_Android_m55747FBB2D6830F0ADA0F324FB4C8BA6F121CC56 (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::AndroidVibrate(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_AndroidVibrate_mF100214FA12779A238D21B4FE7F1767E40BEC14B (int64_t ___milliseconds0, const RuntimeMethod* method);
// System.Boolean MoreMountains.NiceVibrations.MMVibrationManager::iOS()
extern "C" IL2CPP_METHOD_ATTR bool MMVibrationManager_iOS_m0DB6AAABA5C3D14DAF8C2C3186556D958668108F (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::iOSTriggerHaptics(MoreMountains.NiceVibrations.HapticTypes)
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_iOSTriggerHaptics_m84712A2A62E3F9FAC08D75E94A10092760DB53E1 (int32_t ___type0, const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::AndroidVibrate(System.Int64,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_AndroidVibrate_m65B2738FAA0092B96FB40359E981213F5A58AFB5 (int64_t ___milliseconds0, int32_t ___amplitude1, const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::AndroidVibrate(System.Int64[],System.Int32[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_AndroidVibrate_m740729B047BB6785CCEE5EE88A6D63277D5ED85B (Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___pattern0, Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___amplitudes1, int32_t ___repeat2, const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaObject::Call(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR void AndroidJavaObject_Call_m70B7B636F9C052F65AB478AC90520F27A9DCF0B7 (AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * __this, String_t* p0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* p1, const RuntimeMethod* method);
// System.Int32 MoreMountains.NiceVibrations.MMVibrationManager::AndroidSDKVersion()
extern "C" IL2CPP_METHOD_ATTR int32_t MMVibrationManager_AndroidSDKVersion_m9B4DB79A416324F8E935707AB4074CF830EC5C44 (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::VibrationEffectClassInitialization()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_VibrationEffectClassInitialization_mF4742F9FC6D4D62793BE69ED4E19119D252BCD8A (const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::CallStatic<UnityEngine.AndroidJavaObject>(System.String,System.Object[])
inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * AndroidJavaObject_CallStatic_TisAndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E_mD264F5FC6C7BCCE95ABC6BF2B9EC08972F321AF4 (AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * __this, String_t* p0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* p1, const RuntimeMethod* method)
{
	return ((  AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * (*) (AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E *, String_t*, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*))AndroidJavaObject_CallStatic_TisRuntimeObject_m369AA56D528577AE0E4FF989A3506199111B6167_gshared)(__this, p0, p1, method);
}
// !!0[] System.Array::Empty<System.Object>()
inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB (const RuntimeMethod* method)
{
	return ((  ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* (*) (const RuntimeMethod*))Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared)(method);
}
// System.Void UnityEngine.AndroidJavaClass::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void AndroidJavaClass__ctor_mFE0A07AF3670152225C146493AC0918138B81E34 (AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * __this, String_t* p0, const RuntimeMethod* method);
// System.String UnityEngine.SystemInfo::get_operatingSystem()
extern "C" IL2CPP_METHOD_ATTR String_t* SystemInfo_get_operatingSystem_m74AC18E4195899CD3E61258DAED8FBB588B50082 (const RuntimeMethod* method);
// System.Int32 System.String::IndexOf(System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t String_IndexOf_mA9A0117D68338238E51E5928CDA8EB3DC9DA497B (String_t* __this, String_t* p0, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Int32 System.Int32::Parse(System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t Int32_Parse_m5807B6243415790250FC25168F767C08FC16FDEA (String_t* p0, const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::InstantiateFeedbackGenerators()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_InstantiateFeedbackGenerators_mC50BE774724D84E1C7B9E679A4E299D63689C53C (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::ReleaseFeedbackGenerators()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_ReleaseFeedbackGenerators_m100E80D5387646F290C849C6C7A42AD0EECD3A82 (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::iOSInitializeHaptics()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_iOSInitializeHaptics_m8284AAE3DB8B6D2D96B029918B27F4B4A88B812F (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::SelectionHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_SelectionHaptic_mEBF75D55443BBFDDD68041ED7558CA1299D7C6D7 (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::SuccessHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_SuccessHaptic_m3461A89C4FAED7073543B32A94FE8F3B82C58171 (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::WarningHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_WarningHaptic_mA2E535A5BBD522089F8D9AEE4998232775944D9B (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::FailureHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_FailureHaptic_m1F15A4EA767110C00BBF62CE98CCEFC2AC81FBCF (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::LightImpactHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_LightImpactHaptic_mF512F4C60A1384C0BA026425C4E2C0E40BBEF075 (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::MediumImpactHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_MediumImpactHaptic_mEEC9FB401419ADA74AFF26CD5670319647146A61 (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::HeavyImpactHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_HeavyImpactHaptic_m277F4D4D8F04C1F34197C5E74252C981FC0D6B82 (const RuntimeMethod* method);
// System.String UnityEngine.iOS.Device::get_systemVersion()
extern "C" IL2CPP_METHOD_ATTR String_t* Device_get_systemVersion_m2FAF59069686B57244F90EFBAA9E5042301C716E (const RuntimeMethod* method);
// System.String System.Int32::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02 (int32_t* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.String MoreMountains.NiceVibrations.MMVibrationManager::iOSSDKVersion()
extern "C" IL2CPP_METHOD_ATTR String_t* MMVibrationManager_iOSSDKVersion_mCCD6181984E61ACB4E0BDB0DF5351367FB583BEF (const RuntimeMethod* method);
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C" IL2CPP_METHOD_ATTR int32_t Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672 (const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::iOSReleaseHaptics()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_iOSReleaseHaptics_m7FCFC88A0144455308E7B37BDF9B8782817B41D0 (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::Vibrate()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_Vibrate_m9D0D2EA4955F2FB16DA913325D5E136B8653FC62 (const RuntimeMethod* method);
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::Haptic(MoreMountains.NiceVibrations.HapticTypes)
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_Haptic_m5D576554453FE58B294234C1881EA9C3F1CF1A59 (int32_t ___type0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3 (const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150 (const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect)
extern "C" IL2CPP_METHOD_ATTR void GUILayout_BeginArea_mE329140CF26780AF025A41979D054854DD7587B8 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE  p0, const RuntimeMethod* method);
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::MaxHeight(System.Single)
extern "C" IL2CPP_METHOD_ATTR GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * GUILayout_MaxHeight_mE7A6963CEF291A9FB8B8186B1480928D75C0235B (float p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.GUILayout::Button(System.String,UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR bool GUILayout_Button_mACAF3D25298F91F12A312DB687F53258DB0B9918 (String_t* p0, GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* p1, const RuntimeMethod* method);
// IntenseVibration IntenseVibration::get_Instance()
extern "C" IL2CPP_METHOD_ATTR IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * IntenseVibration_get_Instance_mFE116297FF4A991B04B37449A9FDAD9AFDE620F3 (const RuntimeMethod* method);
// System.Void IntenseVibration::Vibrate(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration_Vibrate_m9C63D245C7B601370D0EFFA9A2C72F89A95ABD7A (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, int64_t ___milliseconds0, const RuntimeMethod* method);
// !!0[] System.Array::Empty<UnityEngine.GUILayoutOption>()
inline GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66 (const RuntimeMethod* method)
{
	return ((  GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* (*) (const RuntimeMethod*))Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared)(method);
}
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR void GUILayout_BeginHorizontal_m690713D70FD850E74B529E8929492AC80E673902 (GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* p0, const RuntimeMethod* method);
// System.Void IntenseVibration::Vibrate(System.Int64,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration_Vibrate_mB04B291B2CC37CCEC962BDDCB7B552FC200330CD (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, int64_t ___milliseconds0, int32_t ___intensity1, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::EndHorizontal()
extern "C" IL2CPP_METHOD_ATTR void GUILayout_EndHorizontal_m1DE9883227F98E1DA9309F1AF1370F1158A658C6 (const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::Space(System.Single)
extern "C" IL2CPP_METHOD_ATTR void GUILayout_Space_m3D3D0635EA7BAC83A9A0563005678319FCCED87E (float p0, const RuntimeMethod* method);
// System.Void UnityEngine.Application::Quit()
extern "C" IL2CPP_METHOD_ATTR void Application_Quit_mA005EB22CB989AC3794334754F15E1C0D2FF1C95 (const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::EndArea()
extern "C" IL2CPP_METHOD_ATTR void GUILayout_EndArea_mCBC21D52BCF9CAF29EEFC0A9F986BECE2F1CA754 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EasyVibro::.cctor()
extern "C" IL2CPP_METHOD_ATTR void EasyVibro__cctor_mFA4A985FF9B34A66A46EE657DF9C29D4B1E3C541 (const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean EasyVibro::HasVibrator()
extern "C" IL2CPP_METHOD_ATTR bool EasyVibro_HasVibrator_m2D6229B400C6EA83DB5FDB3C6841D854C5618D36 (const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Void EasyVibro::Cancel()
extern "C" IL2CPP_METHOD_ATTR void EasyVibro_Cancel_m9691BF509783592F603ACCC389C973020FAAE36F (const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void EasyVibro::Vibrate(System.Int64[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void EasyVibro_Vibrate_m4DB710D3021CCE976B79C8ED7DB80DA9763C13BA (Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___pattern0, int32_t ___repeate1, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void EasyVibro::Vibrate(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void EasyVibro_Vibrate_m4C3FD0A9C3A6CE1EA70CBF10ED698AE6533FCB91 (int64_t ___time0, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// IntenseVibration IntenseVibration::get_Instance()
extern "C" IL2CPP_METHOD_ATTR IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * IntenseVibration_get_Instance_mFE116297FF4A991B04B37449A9FDAD9AFDE620F3 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IntenseVibration_get_Instance_mFE116297FF4A991B04B37449A9FDAD9AFDE620F3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_il2cpp_TypeInfo_var);
		IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * L_0 = ((IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_StaticFields*)il2cpp_codegen_static_fields_for(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_il2cpp_TypeInfo_var))->get_mInstance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_2, _stringLiteral56CE46DDBD9745CB436EA69D5734317961670909, /*hidden argument*/NULL);
		NullCheck(L_2);
		IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * L_3 = GameObject_AddComponent_TisIntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_m80457CF2B7F5EF8C427FE7196CC5ACD6CBF5F63A(L_2, /*hidden argument*/GameObject_AddComponent_TisIntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_m80457CF2B7F5EF8C427FE7196CC5ACD6CBF5F63A_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_il2cpp_TypeInfo_var);
		((IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_StaticFields*)il2cpp_codegen_static_fields_for(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_il2cpp_TypeInfo_var))->set_mInstance_4(L_3);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_il2cpp_TypeInfo_var);
		IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * L_4 = ((IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_StaticFields*)il2cpp_codegen_static_fields_for(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_il2cpp_TypeInfo_var))->get_mInstance_4();
		return L_4;
	}
}
// System.Void IntenseVibration::Init()
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration_Init_m7E06284D4B1F5601718C47D8A8F68D9C76D0DA17 (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean IntenseVibration::HasVibrator()
extern "C" IL2CPP_METHOD_ATTR bool IntenseVibration_HasVibrator_m02689B0F1CA824A98EBB138EE76D7DA7B58A2D7F (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Void IntenseVibration::Vibrate(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration_Vibrate_m9C63D245C7B601370D0EFFA9A2C72F89A95ABD7A (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, int64_t ___milliseconds0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void IntenseVibration::Vibrate(System.Int64[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration_Vibrate_m7143313618308B2F0A53EB89CD0F4670E4869AAD (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___pattern0, int32_t ___repeat1, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void IntenseVibration::Vibrate(System.Int64,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration_Vibrate_mB04B291B2CC37CCEC962BDDCB7B552FC200330CD (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, int64_t ___milliseconds0, int32_t ___intensity1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IntenseVibration_Vibrate_mB04B291B2CC37CCEC962BDDCB7B552FC200330CD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___intensity1;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ___intensity1;
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)10))))
		{
			goto IL_002b;
		}
	}
	{
		List_1_t64142247412666BB650346DC5C873E6B6BE631A5 * L_2 = __this->get_mPatterns_7();
		int32_t L_3 = ___intensity1;
		NullCheck(L_2);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_4 = List_1_get_Item_mEEB799CFF217CF39821D8D6D47D5F89FBBA8C3A9(L_2, L_3, /*hidden argument*/List_1_get_Item_mEEB799CFF217CF39821D8D6D47D5F89FBBA8C3A9_RuntimeMethod_var);
		IntenseVibration_Vibrate_m7143313618308B2F0A53EB89CD0F4670E4869AAD(__this, L_4, 0, /*hidden argument*/NULL);
		int64_t L_5 = ___milliseconds0;
		__this->set_mVibrateTimeLeft_6(((float)((float)(((float)((float)L_5)))/(float)(1000.0f))));
		return;
	}

IL_002b:
	{
		return;
	}
}
// System.Void IntenseVibration::Cancel()
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration_Cancel_mFC242521004F6AB6D66103E7EF7E87CA54C2657D (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void IntenseVibration::Start()
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration_Start_mCB5E610C82D0500BF0ABAC2F9CD188037084BE05 (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IntenseVibration_Start_mCB5E610C82D0500BF0ABAC2F9CD188037084BE05_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntenseVibration_Init_m7E06284D4B1F5601718C47D8A8F68D9C76D0DA17(__this, /*hidden argument*/NULL);
		V_0 = 1;
		goto IL_002e;
	}

IL_000a:
	{
		List_1_t64142247412666BB650346DC5C873E6B6BE631A5 * L_0 = __this->get_mPatterns_7();
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_1 = (Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F*)SZArrayNew(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F_il2cpp_TypeInfo_var, (uint32_t)2);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_2 = L_1;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (int64_t)(((int64_t)((int64_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)10), (int32_t)L_3))))));
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_4 = L_2;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (int64_t)(((int64_t)((int64_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)5))))));
		NullCheck(L_0);
		List_1_Add_m1E56AB456B3256FCB45CE87A2AC761DB7A87B4D8(L_0, L_4, /*hidden argument*/List_1_Add_m1E56AB456B3256FCB45CE87A2AC761DB7A87B4D8_RuntimeMethod_var);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_002e:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)10))))
		{
			goto IL_000a;
		}
	}
	{
		return;
	}
}
// System.Void IntenseVibration::Update()
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration_Update_mF2A88ADB391B76FE3268850BD09F51512B76EB57 (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_mVibrateTimeLeft_6();
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0013;
		}
	}
	{
		IntenseVibration_Cancel_mFC242521004F6AB6D66103E7EF7E87CA54C2657D(__this, /*hidden argument*/NULL);
	}

IL_0013:
	{
		float L_1 = __this->get_mVibrateTimeLeft_6();
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_0032;
		}
	}
	{
		float L_2 = __this->get_mVibrateTimeLeft_6();
		float L_3 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		__this->set_mVibrateTimeLeft_6(((float)il2cpp_codegen_subtract((float)L_2, (float)L_3)));
	}

IL_0032:
	{
		return;
	}
}
// System.Void IntenseVibration::.ctor()
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration__ctor_mE6F82C9F3206E2DCF52C166D85319A6C1CF5E843 (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IntenseVibration__ctor_mE6F82C9F3206E2DCF52C166D85319A6C1CF5E843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t64142247412666BB650346DC5C873E6B6BE631A5 * L_0 = (List_1_t64142247412666BB650346DC5C873E6B6BE631A5 *)il2cpp_codegen_object_new(List_1_t64142247412666BB650346DC5C873E6B6BE631A5_il2cpp_TypeInfo_var);
		List_1__ctor_m14ECA4A74FB6A74C49204B46D7E1B4BFA9C81AFE(L_0, /*hidden argument*/List_1__ctor_m14ECA4A74FB6A74C49204B46D7E1B4BFA9C81AFE_RuntimeMethod_var);
		__this->set_mPatterns_7(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IntenseVibration::.cctor()
extern "C" IL2CPP_METHOD_ATTR void IntenseVibration__cctor_m85BEC8771004BE1CD578044D560F3C4921315AF4 (const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean MoreMountains.NiceVibrations.MMVibrationManager::Android()
extern "C" IL2CPP_METHOD_ATTR bool MMVibrationManager_Android_m55747FBB2D6830F0ADA0F324FB4C8BA6F121CC56 (const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean MoreMountains.NiceVibrations.MMVibrationManager::iOS()
extern "C" IL2CPP_METHOD_ATTR bool MMVibrationManager_iOS_m0DB6AAABA5C3D14DAF8C2C3186556D958668108F (const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::Vibrate()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_Vibrate_m9D0D2EA4955F2FB16DA913325D5E136B8653FC62 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MMVibrationManager_Vibrate_m9D0D2EA4955F2FB16DA913325D5E136B8653FC62_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_0 = MMVibrationManager_Android_m55747FBB2D6830F0ADA0F324FB4C8BA6F121CC56(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		int64_t L_1 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_MediumDuration_1();
		MMVibrationManager_AndroidVibrate_mF100214FA12779A238D21B4FE7F1767E40BEC14B(L_1, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_2 = MMVibrationManager_iOS_m0DB6AAABA5C3D14DAF8C2C3186556D958668108F(/*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_iOSTriggerHaptics_m84712A2A62E3F9FAC08D75E94A10092760DB53E1(5, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::Haptic(MoreMountains.NiceVibrations.HapticTypes)
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_Haptic_m5D576554453FE58B294234C1881EA9C3F1CF1A59 (int32_t ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MMVibrationManager_Haptic_m5D576554453FE58B294234C1881EA9C3F1CF1A59_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_0 = MMVibrationManager_Android_m55747FBB2D6830F0ADA0F324FB4C8BA6F121CC56(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_1 = ___type0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_002d;
			}
			case 1:
			{
				goto IL_003d;
			}
			case 2:
			{
				goto IL_004e;
			}
			case 3:
			{
				goto IL_005f;
			}
			case 4:
			{
				goto IL_0070;
			}
			case 5:
			{
				goto IL_0080;
			}
			case 6:
			{
				goto IL_0090;
			}
		}
	}
	{
		return;
	}

IL_002d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		int64_t L_2 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_LightDuration_0();
		int32_t L_3 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_LightAmplitude_3();
		MMVibrationManager_AndroidVibrate_m65B2738FAA0092B96FB40359E981213F5A58AFB5(L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_4 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get__successPattern_7();
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_5 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get__successPatternAmplitude_8();
		MMVibrationManager_AndroidVibrate_m740729B047BB6785CCEE5EE88A6D63277D5ED85B(L_4, L_5, (-1), /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_6 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get__warningPattern_9();
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_7 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get__warningPatternAmplitude_10();
		MMVibrationManager_AndroidVibrate_m740729B047BB6785CCEE5EE88A6D63277D5ED85B(L_6, L_7, (-1), /*hidden argument*/NULL);
		return;
	}

IL_005f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_8 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get__failurePattern_11();
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_9 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get__failurePatternAmplitude_12();
		MMVibrationManager_AndroidVibrate_m740729B047BB6785CCEE5EE88A6D63277D5ED85B(L_8, L_9, (-1), /*hidden argument*/NULL);
		return;
	}

IL_0070:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		int64_t L_10 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_LightDuration_0();
		int32_t L_11 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_LightAmplitude_3();
		MMVibrationManager_AndroidVibrate_m65B2738FAA0092B96FB40359E981213F5A58AFB5(L_10, L_11, /*hidden argument*/NULL);
		return;
	}

IL_0080:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		int64_t L_12 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_MediumDuration_1();
		int32_t L_13 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_MediumAmplitude_4();
		MMVibrationManager_AndroidVibrate_m65B2738FAA0092B96FB40359E981213F5A58AFB5(L_12, L_13, /*hidden argument*/NULL);
		return;
	}

IL_0090:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		int64_t L_14 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_HeavyDuration_2();
		int32_t L_15 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_HeavyAmplitude_5();
		MMVibrationManager_AndroidVibrate_m65B2738FAA0092B96FB40359E981213F5A58AFB5(L_14, L_15, /*hidden argument*/NULL);
		return;
	}

IL_00a0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_16 = MMVibrationManager_iOS_m0DB6AAABA5C3D14DAF8C2C3186556D958668108F(/*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_17 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_iOSTriggerHaptics_m84712A2A62E3F9FAC08D75E94A10092760DB53E1(L_17, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::AndroidVibrate(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_AndroidVibrate_mF100214FA12779A238D21B4FE7F1767E40BEC14B (int64_t ___milliseconds0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MMVibrationManager_AndroidVibrate_mF100214FA12779A238D21B4FE7F1767E40BEC14B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_0 = MMVibrationManager_Android_m55747FBB2D6830F0ADA0F324FB4C8BA6F121CC56(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * L_1 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_AndroidVibrator_15();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = L_2;
		int64_t L_4 = ___milliseconds0;
		int64_t L_5 = L_4;
		RuntimeObject * L_6 = Box(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		NullCheck(L_1);
		AndroidJavaObject_Call_m70B7B636F9C052F65AB478AC90520F27A9DCF0B7(L_1, _stringLiteralCDEED05B83A5AF1EA0B1787714584BBB8E8613E0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::AndroidVibrate(System.Int64,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_AndroidVibrate_m65B2738FAA0092B96FB40359E981213F5A58AFB5 (int64_t ___milliseconds0, int32_t ___amplitude1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MMVibrationManager_AndroidVibrate_m65B2738FAA0092B96FB40359E981213F5A58AFB5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_0 = MMVibrationManager_Android_m55747FBB2D6830F0ADA0F324FB4C8BA6F121CC56(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		int32_t L_1 = MMVibrationManager_AndroidSDKVersion_m9B4DB79A416324F8E935707AB4074CF830EC5C44(/*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)26))))
		{
			goto IL_0018;
		}
	}
	{
		int64_t L_2 = ___milliseconds0;
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_AndroidVibrate_mF100214FA12779A238D21B4FE7F1767E40BEC14B(L_2, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_VibrationEffectClassInitialization_mF4742F9FC6D4D62793BE69ED4E19119D252BCD8A(/*hidden argument*/NULL);
		AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * L_3 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_VibrationEffectClass_16();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_4;
		int64_t L_6 = ___milliseconds0;
		int64_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_5;
		int32_t L_10 = ___amplitude1;
		int32_t L_11 = L_10;
		RuntimeObject * L_12 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_12);
		NullCheck(L_3);
		AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * L_13 = AndroidJavaObject_CallStatic_TisAndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E_mD264F5FC6C7BCCE95ABC6BF2B9EC08972F321AF4(L_3, _stringLiteral2E505E500D2290AFA21D9C3EE996726E11732B7C, L_9, /*hidden argument*/AndroidJavaObject_CallStatic_TisAndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E_mD264F5FC6C7BCCE95ABC6BF2B9EC08972F321AF4_RuntimeMethod_var);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_VibrationEffect_17(L_13);
		AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * L_14 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_AndroidVibrator_15();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_15 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_16 = L_15;
		AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * L_17 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_VibrationEffect_17();
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_17);
		NullCheck(L_14);
		AndroidJavaObject_Call_m70B7B636F9C052F65AB478AC90520F27A9DCF0B7(L_14, _stringLiteralCDEED05B83A5AF1EA0B1787714584BBB8E8613E0, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::AndroidVibrate(System.Int64[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_AndroidVibrate_m361B2E7774207B353DC5F02F2F5DE04B5699D75B (Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___pattern0, int32_t ___repeat1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MMVibrationManager_AndroidVibrate_m361B2E7774207B353DC5F02F2F5DE04B5699D75B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_0 = MMVibrationManager_Android_m55747FBB2D6830F0ADA0F324FB4C8BA6F121CC56(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		int32_t L_1 = MMVibrationManager_AndroidSDKVersion_m9B4DB79A416324F8E935707AB4074CF830EC5C44(/*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)26))))
		{
			goto IL_0034;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * L_2 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_AndroidVibrator_15();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_3;
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_5 = ___pattern0;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = L_4;
		int32_t L_7 = ___repeat1;
		int32_t L_8 = L_7;
		RuntimeObject * L_9 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_9);
		NullCheck(L_2);
		AndroidJavaObject_Call_m70B7B636F9C052F65AB478AC90520F27A9DCF0B7(L_2, _stringLiteralCDEED05B83A5AF1EA0B1787714584BBB8E8613E0, L_6, /*hidden argument*/NULL);
		return;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_VibrationEffectClassInitialization_mF4742F9FC6D4D62793BE69ED4E19119D252BCD8A(/*hidden argument*/NULL);
		AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * L_10 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_VibrationEffectClass_16();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_12 = L_11;
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_13 = ___pattern0;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_13);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_14 = L_12;
		int32_t L_15 = ___repeat1;
		int32_t L_16 = L_15;
		RuntimeObject * L_17 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_17);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_17);
		NullCheck(L_10);
		AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * L_18 = AndroidJavaObject_CallStatic_TisAndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E_mD264F5FC6C7BCCE95ABC6BF2B9EC08972F321AF4(L_10, _stringLiteral4485E86CC86283B27A9E8CE1A03563A635217EF8, L_14, /*hidden argument*/AndroidJavaObject_CallStatic_TisAndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E_mD264F5FC6C7BCCE95ABC6BF2B9EC08972F321AF4_RuntimeMethod_var);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_VibrationEffect_17(L_18);
		AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * L_19 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_AndroidVibrator_15();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_20 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_21 = L_20;
		AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * L_22 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_VibrationEffect_17();
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_22);
		NullCheck(L_19);
		AndroidJavaObject_Call_m70B7B636F9C052F65AB478AC90520F27A9DCF0B7(L_19, _stringLiteralCDEED05B83A5AF1EA0B1787714584BBB8E8613E0, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::AndroidVibrate(System.Int64[],System.Int32[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_AndroidVibrate_m740729B047BB6785CCEE5EE88A6D63277D5ED85B (Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___pattern0, Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___amplitudes1, int32_t ___repeat2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MMVibrationManager_AndroidVibrate_m740729B047BB6785CCEE5EE88A6D63277D5ED85B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_0 = MMVibrationManager_Android_m55747FBB2D6830F0ADA0F324FB4C8BA6F121CC56(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		int32_t L_1 = MMVibrationManager_AndroidSDKVersion_m9B4DB79A416324F8E935707AB4074CF830EC5C44(/*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)26))))
		{
			goto IL_0034;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * L_2 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_AndroidVibrator_15();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_3;
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_5 = ___pattern0;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = L_4;
		int32_t L_7 = ___repeat2;
		int32_t L_8 = L_7;
		RuntimeObject * L_9 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_9);
		NullCheck(L_2);
		AndroidJavaObject_Call_m70B7B636F9C052F65AB478AC90520F27A9DCF0B7(L_2, _stringLiteralCDEED05B83A5AF1EA0B1787714584BBB8E8613E0, L_6, /*hidden argument*/NULL);
		return;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_VibrationEffectClassInitialization_mF4742F9FC6D4D62793BE69ED4E19119D252BCD8A(/*hidden argument*/NULL);
		AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * L_10 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_VibrationEffectClass_16();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)3);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_12 = L_11;
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_13 = ___pattern0;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_13);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_14 = L_12;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_15 = ___amplitudes1;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_15);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_16 = L_14;
		int32_t L_17 = ___repeat2;
		int32_t L_18 = L_17;
		RuntimeObject * L_19 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_19);
		NullCheck(L_10);
		AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * L_20 = AndroidJavaObject_CallStatic_TisAndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E_mD264F5FC6C7BCCE95ABC6BF2B9EC08972F321AF4(L_10, _stringLiteral4485E86CC86283B27A9E8CE1A03563A635217EF8, L_16, /*hidden argument*/AndroidJavaObject_CallStatic_TisAndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E_mD264F5FC6C7BCCE95ABC6BF2B9EC08972F321AF4_RuntimeMethod_var);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_VibrationEffect_17(L_20);
		AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * L_21 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_AndroidVibrator_15();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_22 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_23 = L_22;
		AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * L_24 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_VibrationEffect_17();
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_24);
		NullCheck(L_21);
		AndroidJavaObject_Call_m70B7B636F9C052F65AB478AC90520F27A9DCF0B7(L_21, _stringLiteralCDEED05B83A5AF1EA0B1787714584BBB8E8613E0, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::AndroidCancelVibrations()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_AndroidCancelVibrations_m8132DDA84F6DE4BD971BA4197D5D183F1B4CBA65 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MMVibrationManager_AndroidCancelVibrations_m8132DDA84F6DE4BD971BA4197D5D183F1B4CBA65_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_0 = MMVibrationManager_Android_m55747FBB2D6830F0ADA0F324FB4C8BA6F121CC56(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * L_1 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_AndroidVibrator_15();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB(/*hidden argument*/Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_RuntimeMethod_var);
		NullCheck(L_1);
		AndroidJavaObject_Call_m70B7B636F9C052F65AB478AC90520F27A9DCF0B7(L_1, _stringLiteral4FD0653C4F2AEF3B19A3C145BBDC5F4740715A09, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::VibrationEffectClassInitialization()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_VibrationEffectClassInitialization_mF4742F9FC6D4D62793BE69ED4E19119D252BCD8A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MMVibrationManager_VibrationEffectClassInitialization_mF4742F9FC6D4D62793BE69ED4E19119D252BCD8A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * L_0 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_VibrationEffectClass_16();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * L_1 = (AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 *)il2cpp_codegen_object_new(AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_mFE0A07AF3670152225C146493AC0918138B81E34(L_1, _stringLiteral0BA35307786FF30F0C61E3EAEBAF855BEB8B01FD, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_VibrationEffectClass_16(L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Int32 MoreMountains.NiceVibrations.MMVibrationManager::AndroidSDKVersion()
extern "C" IL2CPP_METHOD_ATTR int32_t MMVibrationManager_AndroidSDKVersion_m9B4DB79A416324F8E935707AB4074CF830EC5C44 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MMVibrationManager_AndroidSDKVersion_m9B4DB79A416324F8E935707AB4074CF830EC5C44_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		int32_t L_0 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get__sdkVersion_6();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_1 = SystemInfo_get_operatingSystem_m74AC18E4195899CD3E61258DAED8FBB588B50082(/*hidden argument*/NULL);
		String_t* L_2 = SystemInfo_get_operatingSystem_m74AC18E4195899CD3E61258DAED8FBB588B50082(/*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_mA9A0117D68338238E51E5928CDA8EB3DC9DA497B(L_2, _stringLiteral3BC15C8AAE3E4124DD409035F32EA2FD6835EFC9, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_4 = String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB(L_1, ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)), 3, /*hidden argument*/NULL);
		int32_t L_5 = Int32_Parse_m5807B6243415790250FC25168F767C08FC16FDEA(L_4, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set__sdkVersion_6(L_6);
		return L_6;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		int32_t L_7 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get__sdkVersion_6();
		return L_7;
	}
}
extern "C" void DEFAULT_CALL InstantiateFeedbackGenerators();
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::InstantiateFeedbackGenerators()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_InstantiateFeedbackGenerators_mC50BE774724D84E1C7B9E679A4E299D63689C53C (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(InstantiateFeedbackGenerators)();

}
extern "C" void DEFAULT_CALL ReleaseFeedbackGenerators();
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::ReleaseFeedbackGenerators()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_ReleaseFeedbackGenerators_m100E80D5387646F290C849C6C7A42AD0EECD3A82 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(ReleaseFeedbackGenerators)();

}
extern "C" void DEFAULT_CALL SelectionHaptic();
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::SelectionHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_SelectionHaptic_mEBF75D55443BBFDDD68041ED7558CA1299D7C6D7 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SelectionHaptic)();

}
extern "C" void DEFAULT_CALL SuccessHaptic();
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::SuccessHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_SuccessHaptic_m3461A89C4FAED7073543B32A94FE8F3B82C58171 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SuccessHaptic)();

}
extern "C" void DEFAULT_CALL WarningHaptic();
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::WarningHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_WarningHaptic_mA2E535A5BBD522089F8D9AEE4998232775944D9B (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(WarningHaptic)();

}
extern "C" void DEFAULT_CALL FailureHaptic();
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::FailureHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_FailureHaptic_m1F15A4EA767110C00BBF62CE98CCEFC2AC81FBCF (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(FailureHaptic)();

}
extern "C" void DEFAULT_CALL LightImpactHaptic();
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::LightImpactHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_LightImpactHaptic_mF512F4C60A1384C0BA026425C4E2C0E40BBEF075 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LightImpactHaptic)();

}
extern "C" void DEFAULT_CALL MediumImpactHaptic();
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::MediumImpactHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_MediumImpactHaptic_mEEC9FB401419ADA74AFF26CD5670319647146A61 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(MediumImpactHaptic)();

}
extern "C" void DEFAULT_CALL HeavyImpactHaptic();
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::HeavyImpactHaptic()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_HeavyImpactHaptic_m277F4D4D8F04C1F34197C5E74252C981FC0D6B82 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(HeavyImpactHaptic)();

}
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::iOSInitializeHaptics()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_iOSInitializeHaptics_m8284AAE3DB8B6D2D96B029918B27F4B4A88B812F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MMVibrationManager_iOSInitializeHaptics_m8284AAE3DB8B6D2D96B029918B27F4B4A88B812F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_0 = MMVibrationManager_iOS_m0DB6AAABA5C3D14DAF8C2C3186556D958668108F(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_InstantiateFeedbackGenerators_mC50BE774724D84E1C7B9E679A4E299D63689C53C(/*hidden argument*/NULL);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_iOSHapticsInitialized_19((bool)1);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::iOSReleaseHaptics()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_iOSReleaseHaptics_m7FCFC88A0144455308E7B37BDF9B8782817B41D0 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MMVibrationManager_iOSReleaseHaptics_m7FCFC88A0144455308E7B37BDF9B8782817B41D0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_0 = MMVibrationManager_iOS_m0DB6AAABA5C3D14DAF8C2C3186556D958668108F(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_ReleaseFeedbackGenerators_m100E80D5387646F290C849C6C7A42AD0EECD3A82(/*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::iOSTriggerHaptics(MoreMountains.NiceVibrations.HapticTypes)
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager_iOSTriggerHaptics_m84712A2A62E3F9FAC08D75E94A10092760DB53E1 (int32_t ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MMVibrationManager_iOSTriggerHaptics_m84712A2A62E3F9FAC08D75E94A10092760DB53E1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_0 = MMVibrationManager_iOS_m0DB6AAABA5C3D14DAF8C2C3186556D958668108F(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_1 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_iOSHapticsInitialized_19();
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_iOSInitializeHaptics_m8284AAE3DB8B6D2D96B029918B27F4B4A88B812F(/*hidden argument*/NULL);
	}

IL_0014:
	{
		int32_t L_2 = ___type0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0037;
			}
			case 1:
			{
				goto IL_003d;
			}
			case 2:
			{
				goto IL_0043;
			}
			case 3:
			{
				goto IL_0049;
			}
			case 4:
			{
				goto IL_004f;
			}
			case 5:
			{
				goto IL_0055;
			}
			case 6:
			{
				goto IL_005b;
			}
		}
	}
	{
		return;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_SelectionHaptic_mEBF75D55443BBFDDD68041ED7558CA1299D7C6D7(/*hidden argument*/NULL);
		return;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_SuccessHaptic_m3461A89C4FAED7073543B32A94FE8F3B82C58171(/*hidden argument*/NULL);
		return;
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_WarningHaptic_mA2E535A5BBD522089F8D9AEE4998232775944D9B(/*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_FailureHaptic_m1F15A4EA767110C00BBF62CE98CCEFC2AC81FBCF(/*hidden argument*/NULL);
		return;
	}

IL_004f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_LightImpactHaptic_mF512F4C60A1384C0BA026425C4E2C0E40BBEF075(/*hidden argument*/NULL);
		return;
	}

IL_0055:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_MediumImpactHaptic_mEEC9FB401419ADA74AFF26CD5670319647146A61(/*hidden argument*/NULL);
		return;
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_HeavyImpactHaptic_m277F4D4D8F04C1F34197C5E74252C981FC0D6B82(/*hidden argument*/NULL);
		return;
	}
}
// System.String MoreMountains.NiceVibrations.MMVibrationManager::iOSSDKVersion()
extern "C" IL2CPP_METHOD_ATTR String_t* MMVibrationManager_iOSSDKVersion_mCCD6181984E61ACB4E0BDB0DF5351367FB583BEF (const RuntimeMethod* method)
{
	{
		String_t* L_0 = Device_get_systemVersion_m2FAF59069686B57244F90EFBAA9E5042301C716E(/*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void MoreMountains.NiceVibrations.MMVibrationManager::.cctor()
extern "C" IL2CPP_METHOD_ATTR void MMVibrationManager__cctor_m77BA09387A36B7FCC502BF60DD37E65F652B13A1 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MMVibrationManager__cctor_m77BA09387A36B7FCC502BF60DD37E65F652B13A1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_LightDuration_0((((int64_t)((int64_t)((int32_t)20)))));
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_MediumDuration_1((((int64_t)((int64_t)((int32_t)40)))));
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_HeavyDuration_2((((int64_t)((int64_t)((int32_t)80)))));
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_LightAmplitude_3(((int32_t)40));
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_MediumAmplitude_4(((int32_t)120));
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_HeavyAmplitude_5(((int32_t)255));
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set__sdkVersion_6((-1));
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_0 = (Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F*)SZArrayNew(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F_il2cpp_TypeInfo_var, (uint32_t)4);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_1 = L_0;
		int64_t L_2 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_LightDuration_0();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (int64_t)L_2);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_3 = L_1;
		int64_t L_4 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_LightDuration_0();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (int64_t)L_4);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_5 = L_3;
		int64_t L_6 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_HeavyDuration_2();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (int64_t)L_6);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set__successPattern_7(L_5);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_7 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var, (uint32_t)4);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_8 = L_7;
		int32_t L_9 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_LightAmplitude_3();
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)L_9);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_10 = L_8;
		int32_t L_11 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_HeavyAmplitude_5();
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (int32_t)L_11);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set__successPatternAmplitude_8(L_10);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_12 = (Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F*)SZArrayNew(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F_il2cpp_TypeInfo_var, (uint32_t)4);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_13 = L_12;
		int64_t L_14 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_HeavyDuration_2();
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (int64_t)L_14);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_15 = L_13;
		int64_t L_16 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_LightDuration_0();
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(2), (int64_t)L_16);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_17 = L_15;
		int64_t L_18 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_MediumDuration_1();
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(3), (int64_t)L_18);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set__warningPattern_9(L_17);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_19 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var, (uint32_t)4);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_20 = L_19;
		int32_t L_21 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_HeavyAmplitude_5();
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)L_21);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_22 = L_20;
		int32_t L_23 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_MediumAmplitude_4();
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (int32_t)L_23);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set__warningPatternAmplitude_10(L_22);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_24 = (Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F*)SZArrayNew(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F_il2cpp_TypeInfo_var, (uint32_t)8);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_25 = L_24;
		int64_t L_26 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_MediumDuration_1();
		NullCheck(L_25);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(1), (int64_t)L_26);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_27 = L_25;
		int64_t L_28 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_LightDuration_0();
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(2), (int64_t)L_28);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_29 = L_27;
		int64_t L_30 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_MediumDuration_1();
		NullCheck(L_29);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (int64_t)L_30);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_31 = L_29;
		int64_t L_32 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_LightDuration_0();
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(4), (int64_t)L_32);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_33 = L_31;
		int64_t L_34 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_HeavyDuration_2();
		NullCheck(L_33);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(5), (int64_t)L_34);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_35 = L_33;
		int64_t L_36 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_LightDuration_0();
		NullCheck(L_35);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(6), (int64_t)L_36);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_37 = L_35;
		int64_t L_38 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_LightDuration_0();
		NullCheck(L_37);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(7), (int64_t)L_38);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set__failurePattern_11(L_37);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_39 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var, (uint32_t)8);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_40 = L_39;
		int32_t L_41 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_MediumAmplitude_4();
		NullCheck(L_40);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)L_41);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_42 = L_40;
		int32_t L_43 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_MediumAmplitude_4();
		NullCheck(L_42);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(3), (int32_t)L_43);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_44 = L_42;
		int32_t L_45 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_HeavyAmplitude_5();
		NullCheck(L_44);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(5), (int32_t)L_45);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_46 = L_44;
		int32_t L_47 = ((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->get_LightAmplitude_3();
		NullCheck(L_46);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(7), (int32_t)L_47);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set__failurePatternAmplitude_12(L_46);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_AndroidVibrator_15((AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E *)NULL);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_VibrationEffectClass_16((AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 *)NULL);
		((MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields*)il2cpp_codegen_static_fields_for(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var))->set_iOSHapticsInitialized_19((bool)0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::Awake()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager_Awake_m9481861A5E62C75D546E3C5E7A0414CFCCA9CBB5 (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NiceVibrationsDemoManager_Awake_m9481861A5E62C75D546E3C5E7A0414CFCCA9CBB5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_iOSInitializeHaptics_m8284AAE3DB8B6D2D96B029918B27F4B4A88B812F(/*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::Start()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager_Start_m94428116F88AFB8D75A20D6ECA3C5F090538EAAB (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	{
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::DisplayInformation() */, __this);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::DisplayInformation()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager_DisplayInformation_m530724622BC5BCA310E744B970250A345A6BDADE (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NiceVibrationsDemoManager_DisplayInformation_m530724622BC5BCA310E744B970250A345A6BDADE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_0 = MMVibrationManager_Android_m55747FBB2D6830F0ADA0F324FB4C8BA6F121CC56(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		int32_t L_1 = MMVibrationManager_AndroidSDKVersion_m9B4DB79A416324F8E935707AB4074CF830EC5C44(/*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_0), /*hidden argument*/NULL);
		String_t* L_3 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral888519795321A07D068DA9B34F02755F71FA03F4, L_2, /*hidden argument*/NULL);
		__this->set__platformString_6(L_3);
		goto IL_005e;
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		bool L_4 = MMVibrationManager_iOS_m0DB6AAABA5C3D14DAF8C2C3186556D958668108F(/*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		String_t* L_5 = MMVibrationManager_iOSSDKVersion_mCCD6181984E61ACB4E0BDB0DF5351367FB583BEF(/*hidden argument*/NULL);
		String_t* L_6 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralAB156EC1176CE1DCF8D64EC957AAC47017C315D9, L_5, /*hidden argument*/NULL);
		__this->set__platformString_6(L_6);
		goto IL_005e;
	}

IL_0044:
	{
		int32_t L_7 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		int32_t L_8 = L_7;
		RuntimeObject * L_9 = Box(RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132_il2cpp_TypeInfo_var, &L_8);
		String_t* L_10 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(L_9, _stringLiteral3B9EBB5B58C057AC546538D3297A4BA7FF7D646D, /*hidden argument*/NULL);
		__this->set__platformString_6(L_10);
	}

IL_005e:
	{
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_11 = __this->get_DebugTextBox_4();
		String_t* L_12 = __this->get__platformString_6();
		String_t* L_13 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteralC9AF0AA1D7A79E2D3297CF71D4FC61B36079D6C6, L_12, _stringLiteral13F322329E5BEFA04267118213843ABE4D213DA3, /*hidden argument*/NULL);
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_13);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager_OnDisable_mEEBD28B14C1116E99C8AF8B1310D656920216526 (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NiceVibrationsDemoManager_OnDisable_mEEBD28B14C1116E99C8AF8B1310D656920216526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_iOSReleaseHaptics_m7FCFC88A0144455308E7B37BDF9B8782817B41D0(/*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::TriggerDefault()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager_TriggerDefault_m63D987F2ACB9CA538F3B10D8E75CBFE696D5E769 (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::TriggerVibrate()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager_TriggerVibrate_m09ED9399BCF412848A9C23BEEECFD6F2AF13CB57 (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NiceVibrationsDemoManager_TriggerVibrate_m09ED9399BCF412848A9C23BEEECFD6F2AF13CB57_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_Vibrate_m9D0D2EA4955F2FB16DA913325D5E136B8653FC62(/*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::TriggerSelection()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager_TriggerSelection_mDF6FE01A2C6623CE6F2C7FB95D6CD3EC434A72AF (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NiceVibrationsDemoManager_TriggerSelection_mDF6FE01A2C6623CE6F2C7FB95D6CD3EC434A72AF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_Haptic_m5D576554453FE58B294234C1881EA9C3F1CF1A59(0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::TriggerSuccess()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager_TriggerSuccess_m4D116451BB8BF174B4FE2C8D394FAA8CACAD94A8 (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NiceVibrationsDemoManager_TriggerSuccess_m4D116451BB8BF174B4FE2C8D394FAA8CACAD94A8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_Haptic_m5D576554453FE58B294234C1881EA9C3F1CF1A59(1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::TriggerWarning()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager_TriggerWarning_m9AF376C7F63F529FA5CD4C929BD2445B66D53198 (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NiceVibrationsDemoManager_TriggerWarning_m9AF376C7F63F529FA5CD4C929BD2445B66D53198_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_Haptic_m5D576554453FE58B294234C1881EA9C3F1CF1A59(2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::TriggerFailure()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager_TriggerFailure_m00C3D424846AEC1B18ADFE0B4544B0DD00C9B3A7 (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NiceVibrationsDemoManager_TriggerFailure_m00C3D424846AEC1B18ADFE0B4544B0DD00C9B3A7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_Haptic_m5D576554453FE58B294234C1881EA9C3F1CF1A59(3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::TriggerLightImpact()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager_TriggerLightImpact_mCACCE403D0779D73B39E03CA79243F8B2EE33CC9 (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NiceVibrationsDemoManager_TriggerLightImpact_mCACCE403D0779D73B39E03CA79243F8B2EE33CC9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_Haptic_m5D576554453FE58B294234C1881EA9C3F1CF1A59(4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::TriggerMediumImpact()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager_TriggerMediumImpact_mF9EAA0A8C365F211B94926336728B8F2B3F950D0 (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NiceVibrationsDemoManager_TriggerMediumImpact_mF9EAA0A8C365F211B94926336728B8F2B3F950D0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_Haptic_m5D576554453FE58B294234C1881EA9C3F1CF1A59(5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::TriggerHeavyImpact()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager_TriggerHeavyImpact_mD5892EDCAA93CD8D924012FA479E05131FD05E50 (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NiceVibrationsDemoManager_TriggerHeavyImpact_mD5892EDCAA93CD8D924012FA479E05131FD05E50_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_il2cpp_TypeInfo_var);
		MMVibrationManager_Haptic_m5D576554453FE58B294234C1881EA9C3F1CF1A59(6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.NiceVibrations.NiceVibrationsDemoManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NiceVibrationsDemoManager__ctor_m5FACC1170192038574D94515BD9E9B72F70AB7AF (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TestIntenseVibration::Start()
extern "C" IL2CPP_METHOD_ATTR void TestIntenseVibration_Start_m7D666EF0BC8432C2B32CE2641516036B76F66D64 (TestIntenseVibration_tB3B9456153E01062B1FCD9F20943D0897E283741 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void TestIntenseVibration::Update()
extern "C" IL2CPP_METHOD_ATTR void TestIntenseVibration_Update_m4A3D99E1E8611A5DC0F8D3AA9DD02292FB81BD09 (TestIntenseVibration_tB3B9456153E01062B1FCD9F20943D0897E283741 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void TestIntenseVibration::OnGUI()
extern "C" IL2CPP_METHOD_ATTR void TestIntenseVibration_OnGUI_m69BE516E2D393C0C9E0EFB9E73B01C3532BFF647 (TestIntenseVibration_tB3B9456153E01062B1FCD9F20943D0897E283741 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestIntenseVibration_OnGUI_m69BE516E2D393C0C9E0EFB9E73B01C3532BFF647_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int64_t V_1 = 0;
	int64_t V_2 = 0;
	int64_t V_3 = 0;
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* V_4 = NULL;
	{
		int32_t L_0 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((&L_2), ((float)il2cpp_codegen_subtract((float)((float)((float)(((float)((float)L_0)))/(float)(2.0f))), (float)(200.0f))), (0.0f), (400.0f), (((float)((float)L_1))), /*hidden argument*/NULL);
		GUILayout_BeginArea_mE329140CF26780AF025A41979D054854DD7587B8(L_2, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_3 = (GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B*)SZArrayNew(GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_4 = L_3;
		GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * L_5 = GUILayout_MaxHeight_mE7A6963CEF291A9FB8B8186B1480928D75C0235B((50.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 *)L_5);
		bool L_6 = GUILayout_Button_mACAF3D25298F91F12A312DB687F53258DB0B9918(_stringLiteralADCB524304C732A6BAE34C5F960A610899CD4EC9, L_4, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0058;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_il2cpp_TypeInfo_var);
		IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * L_7 = IntenseVibration_get_Instance_mFE116297FF4A991B04B37449A9FDAD9AFDE620F3(/*hidden argument*/NULL);
		NullCheck(L_7);
		IntenseVibration_Vibrate_m9C63D245C7B601370D0EFFA9A2C72F89A95ABD7A(L_7, (((int64_t)((int64_t)((int32_t)50)))), /*hidden argument*/NULL);
	}

IL_0058:
	{
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_8 = (GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B*)SZArrayNew(GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_9 = L_8;
		GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * L_10 = GUILayout_MaxHeight_mE7A6963CEF291A9FB8B8186B1480928D75C0235B((50.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 *)L_10);
		bool L_11 = GUILayout_Button_mACAF3D25298F91F12A312DB687F53258DB0B9918(_stringLiteralBF32F43934B1ADBDAD6D91476DB7CD60A8DC2948, L_9, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0087;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_il2cpp_TypeInfo_var);
		IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * L_12 = IntenseVibration_get_Instance_mFE116297FF4A991B04B37449A9FDAD9AFDE620F3(/*hidden argument*/NULL);
		NullCheck(L_12);
		IntenseVibration_Vibrate_m9C63D245C7B601370D0EFFA9A2C72F89A95ABD7A(L_12, (((int64_t)((int64_t)((int32_t)200)))), /*hidden argument*/NULL);
	}

IL_0087:
	{
		V_0 = 0;
		goto IL_0120;
	}

IL_008e:
	{
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_13 = Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66(/*hidden argument*/Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66_RuntimeMethod_var);
		GUILayout_BeginHorizontal_m690713D70FD850E74B529E8929492AC80E673902(L_13, /*hidden argument*/NULL);
		String_t* L_14 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_0), /*hidden argument*/NULL);
		String_t* L_15 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteralDF66CFC8A268191434B45AE84CFA9A3AD1C9DC1E, L_14, _stringLiteralE7064F0B80F61DBC65915311032D27BAA569AE2A, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_16 = (GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B*)SZArrayNew(GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_17 = L_16;
		GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * L_18 = GUILayout_MaxHeight_mE7A6963CEF291A9FB8B8186B1480928D75C0235B((50.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 *)L_18);
		bool L_19 = GUILayout_Button_mACAF3D25298F91F12A312DB687F53258DB0B9918(L_15, L_17, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_il2cpp_TypeInfo_var);
		IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * L_20 = IntenseVibration_get_Instance_mFE116297FF4A991B04B37449A9FDAD9AFDE620F3(/*hidden argument*/NULL);
		int32_t L_21 = V_0;
		NullCheck(L_20);
		IntenseVibration_Vibrate_mB04B291B2CC37CCEC962BDDCB7B552FC200330CD(L_20, (((int64_t)((int64_t)((int32_t)50)))), L_21, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		String_t* L_22 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_0), /*hidden argument*/NULL);
		String_t* L_23 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteral97DEE6B586983AC010EC6F2D8AE6B8CC23893478, L_22, _stringLiteralE7064F0B80F61DBC65915311032D27BAA569AE2A, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_24 = (GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B*)SZArrayNew(GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_25 = L_24;
		GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * L_26 = GUILayout_MaxHeight_mE7A6963CEF291A9FB8B8186B1480928D75C0235B((50.0f), /*hidden argument*/NULL);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_26);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 *)L_26);
		bool L_27 = GUILayout_Button_mACAF3D25298F91F12A312DB687F53258DB0B9918(L_23, L_25, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0117;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_il2cpp_TypeInfo_var);
		IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * L_28 = IntenseVibration_get_Instance_mFE116297FF4A991B04B37449A9FDAD9AFDE620F3(/*hidden argument*/NULL);
		int32_t L_29 = V_0;
		NullCheck(L_28);
		IntenseVibration_Vibrate_mB04B291B2CC37CCEC962BDDCB7B552FC200330CD(L_28, (((int64_t)((int64_t)((int32_t)200)))), L_29, /*hidden argument*/NULL);
	}

IL_0117:
	{
		GUILayout_EndHorizontal_m1DE9883227F98E1DA9309F1AF1370F1158A658C6(/*hidden argument*/NULL);
		int32_t L_30 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
	}

IL_0120:
	{
		int32_t L_31 = V_0;
		if ((((int32_t)L_31) < ((int32_t)((int32_t)10))))
		{
			goto IL_008e;
		}
	}
	{
		GUILayout_Space_m3D3D0635EA7BAC83A9A0563005678319FCCED87E((25.0f), /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_32 = (GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B*)SZArrayNew(GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_33 = L_32;
		GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * L_34 = GUILayout_MaxHeight_mE7A6963CEF291A9FB8B8186B1480928D75C0235B((50.0f), /*hidden argument*/NULL);
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_34);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 *)L_34);
		bool L_35 = GUILayout_Button_mACAF3D25298F91F12A312DB687F53258DB0B9918(_stringLiteral9B5199A3391A89EB28C933EACD494725617C3368, L_33, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_01bd;
		}
	}
	{
		V_1 = (((int64_t)((int64_t)((int32_t)100))));
		V_2 = (((int64_t)((int64_t)((int32_t)300))));
		V_3 = (((int64_t)((int64_t)((int32_t)100))));
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_36 = (Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F*)SZArrayNew(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_37 = L_36;
		int64_t L_38 = V_3;
		NullCheck(L_37);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(0), (int64_t)L_38);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_39 = L_37;
		int64_t L_40 = V_1;
		NullCheck(L_39);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(1), (int64_t)L_40);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_41 = L_39;
		int64_t L_42 = V_3;
		NullCheck(L_41);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(2), (int64_t)L_42);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_43 = L_41;
		int64_t L_44 = V_1;
		NullCheck(L_43);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(3), (int64_t)L_44);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_45 = L_43;
		int64_t L_46 = V_3;
		NullCheck(L_45);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(4), (int64_t)L_46);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_47 = L_45;
		int64_t L_48 = V_1;
		NullCheck(L_47);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(5), (int64_t)L_48);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_49 = L_47;
		int64_t L_50 = V_3;
		NullCheck(L_49);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(6), (int64_t)L_50);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_51 = L_49;
		int64_t L_52 = V_2;
		NullCheck(L_51);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(7), (int64_t)L_52);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_53 = L_51;
		int64_t L_54 = V_3;
		NullCheck(L_53);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(8), (int64_t)L_54);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_55 = L_53;
		int64_t L_56 = V_2;
		NullCheck(L_55);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (int64_t)L_56);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_57 = L_55;
		int64_t L_58 = V_3;
		NullCheck(L_57);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (int64_t)L_58);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_59 = L_57;
		int64_t L_60 = V_1;
		NullCheck(L_59);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (int64_t)L_60);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_61 = L_59;
		int64_t L_62 = V_3;
		NullCheck(L_61);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (int64_t)L_62);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_63 = L_61;
		int64_t L_64 = V_1;
		NullCheck(L_63);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (int64_t)L_64);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_65 = L_63;
		int64_t L_66 = V_3;
		NullCheck(L_65);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (int64_t)L_66);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_67 = L_65;
		int64_t L_68 = V_1;
		NullCheck(L_67);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (int64_t)L_68);
		V_4 = L_67;
		IL2CPP_RUNTIME_CLASS_INIT(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_il2cpp_TypeInfo_var);
		IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * L_69 = IntenseVibration_get_Instance_mFE116297FF4A991B04B37449A9FDAD9AFDE620F3(/*hidden argument*/NULL);
		Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* L_70 = V_4;
		NullCheck(L_69);
		IntenseVibration_Vibrate_m7143313618308B2F0A53EB89CD0F4670E4869AAD(L_69, L_70, (-1), /*hidden argument*/NULL);
	}

IL_01bd:
	{
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_71 = (GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B*)SZArrayNew(GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_72 = L_71;
		GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * L_73 = GUILayout_MaxHeight_mE7A6963CEF291A9FB8B8186B1480928D75C0235B((50.0f), /*hidden argument*/NULL);
		NullCheck(L_72);
		ArrayElementTypeCheck (L_72, L_73);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 *)L_73);
		bool L_74 = GUILayout_Button_mACAF3D25298F91F12A312DB687F53258DB0B9918(_stringLiteral77DFD2135F4DB726C47299BB55BE26F7F4525A46, L_72, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_01e6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_il2cpp_TypeInfo_var);
		IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * L_75 = IntenseVibration_get_Instance_mFE116297FF4A991B04B37449A9FDAD9AFDE620F3(/*hidden argument*/NULL);
		NullCheck(L_75);
		IntenseVibration_Cancel_mFC242521004F6AB6D66103E7EF7E87CA54C2657D(L_75, /*hidden argument*/NULL);
	}

IL_01e6:
	{
		GUILayout_Space_m3D3D0635EA7BAC83A9A0563005678319FCCED87E((25.0f), /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_76 = (GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B*)SZArrayNew(GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_77 = L_76;
		GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * L_78 = GUILayout_MaxHeight_mE7A6963CEF291A9FB8B8186B1480928D75C0235B((50.0f), /*hidden argument*/NULL);
		NullCheck(L_77);
		ArrayElementTypeCheck (L_77, L_78);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 *)L_78);
		bool L_79 = GUILayout_Button_mACAF3D25298F91F12A312DB687F53258DB0B9918(_stringLiteralF83B6FE3AEBF13744E866019556D9129CD7A55BE, L_77, /*hidden argument*/NULL);
		if (!L_79)
		{
			goto IL_0214;
		}
	}
	{
		Application_Quit_mA005EB22CB989AC3794334754F15E1C0D2FF1C95(/*hidden argument*/NULL);
	}

IL_0214:
	{
		GUILayout_EndArea_mCBC21D52BCF9CAF29EEFC0A9F986BECE2F1CA754(/*hidden argument*/NULL);
		return;
	}
}
// System.Void TestIntenseVibration::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TestIntenseVibration__ctor_mE36BE55CBE96FA3107A98B6F35246CD28F5E20C3 (TestIntenseVibration_tB3B9456153E01062B1FCD9F20943D0897E283741 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
