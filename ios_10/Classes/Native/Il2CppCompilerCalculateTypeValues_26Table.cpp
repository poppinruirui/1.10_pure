﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Admin
struct Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60;
// AdministratorManager
struct AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24;
// AdsManager
struct AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A;
// AdventureManager_New
struct AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA;
// BaseScale
struct BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063;
// CFrameAnimationEffect
struct CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10;
// District
struct District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A;
// MoneyCounter[]
struct MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A;
// MonthCard[]
struct MonthCardU5BU5D_t8E5902E05E070A5633F378B633686AFDD254FC9A;
// System.Action`3<UnityEngine.Timeline.TimelineClip,UnityEngine.GameObject,UnityEngine.Playables.Playable>
struct Action_3_t94294D297455C1B4C4B6ACA47BC2B4019ECE97CB;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<AdventureManager_New/eAdventureQuality,AdventureManager_New/sAdventureConfig>
struct Dictionary_2_tD008B5F5B379152550E567E215BF4E5779065775;
// System.Collections.Generic.Dictionary`2<System.Int32,AdministratorManager/sAdminConfig>
struct Dictionary_2_tF731C640B75714140019E77D24BE632B8A927797;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute>
struct Dictionary_2_t58C9CA6651216CBA1E51B3DE5F86E81AB30077B1;
// System.Collections.Generic.Dictionary`2<UnityEngine.Timeline.TrackAsset,UnityEngine.Playables.Playable>
struct Dictionary_2_tA07AA2153D8A38620011D13B62D2BD03F1366F47;
// System.Collections.Generic.HashSet`1<UnityEngine.GameObject>
struct HashSet_1_tA5E1C933DE229CF4E67A77A1764986FC8574A39E;
// System.Collections.Generic.HashSet`1<UnityEngine.Playables.PlayableDirector>
struct HashSet_1_tCCD075D900742C873F3ED61FC80520946929DC59;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Timeline.TrackAsset>
struct IEnumerable_1_t121E17B1B8EA085B72ACE0C1AAB08EF16D87BD5D;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Playables.PlayableBinding>
struct IEnumerator_1_t31C43E315865D4A0AA42E78304D23920B0951EB9;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Timeline.TrackAsset>
struct IEnumerator_1_t2C7CD9D2CAD6C41676D866C33C18CDB283B0DA71;
// System.Collections.Generic.List`1<Admin>
struct List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34;
// System.Collections.Generic.List`1<AdventureManager_New/eAdventureQuality>
struct List_1_t6D1E458C1593DACF4D6FAE5AFEEA855C4AA47AA0;
// System.Collections.Generic.List`1<AdventureManager_New/sRewardConfig>
struct List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Int64[]>
struct List_1_t64142247412666BB650346DC5C873E6B6BE631A5;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UIAdministratorCounter>
struct List_1_t25D2A8A2511EE41AE9FA7D85654386B072B20103;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_t0E065AF84B0711991C51EB17BB73BF70BAFD3916;
// System.Collections.Generic.List`1<UnityEngine.Playables.PlayableDirector>
struct List_1_t1ED8B5CB268BE8D61CB3CCEE88DEF2DF54E67EE7;
// System.Collections.Generic.List`1<UnityEngine.ScriptableObject>
struct List_1_t803BD2FB729584A0A796EBF33774257912427B4E;
// System.Collections.Generic.List`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>
struct List_1_tB3781B4D73201DF66D7FFF614E58A2D0172820B4;
// System.Collections.Generic.List`1<UnityEngine.Timeline.ITimelineEvaluateCallback>
struct List_1_t8D4337C886AB0EA34617A8D20ABB1FE379E68A60;
// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement>
struct List_1_t6F7766D50A23EA9CCE03264AAD6B9B008F54FEDF;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip>
struct List_1_tBE0C2267D3E6C51CE882E2B6B95F8E67011B1376;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset>
struct List_1_tC30659FBAA4D6EE8004DC058D9C7E6B548CCE5BA;
// System.Comparison`1<UnityEngine.Timeline.TimelineClip>
struct Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UIAdministratorCounter
struct UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78;
// UIAdventureCounter
struct UIAdventureCounter_tE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C;
// UIAdventureCounter_New
struct UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6;
// UIAdventureCounter_New[]
struct UIAdventureCounter_NewU5BU5D_t0E2830AF419E9D9E16E0CA0EAD443DDE753657C2;
// UIAdventureRewardCounter[]
struct UIAdventureRewardCounterU5BU5D_t7D81D6F2339C91763E5D69F58B2E6D3CFC55CAE5;
// UIAdventureRewardShow
struct UIAdventureRewardShow_tC98CDF0A1478D1585512C351DF8B6106FCB4C93F;
// UIAdventureRewardShow[]
struct UIAdventureRewardShowU5BU5D_tB9375AB05FDA93565842B054D44F1C0B321149D2;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E;
// UnityEngine.AnimationClip
struct AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AvatarMask
struct AvatarMask_t12E2214B133E61C5CF28DC1E4F6DC2451C75D88A;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.IntervalTree`1<UnityEngine.Timeline.RuntimeElement>
struct IntervalTree_1_t743FB00DC6F26F299230267DDE0DB435606C872D;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_tE94AB2103D501801597F2F14B6A1642246B35347;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD;
// UnityEngine.Playables.PlayableBinding/CreateOutputMethod
struct CreateOutputMethod_tA7B649F49822FC5DD0B0D9F17247C73CAECB1CA3;
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t7EB322901D51EAB67BA4F711C87F3AC1CF5D89AB;
// UnityEngine.Playables.PlayableDirector
struct PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017;
// UnityEngine.Timeline.ActivationMixerPlayable
struct ActivationMixerPlayable_t299EC5947DB712650FF6D52B34BE46862B4034E2;
// UnityEngine.Timeline.AnimationPlayableAsset
struct AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4;
// UnityEngine.Timeline.AnimationTrack
struct AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162;
// UnityEngine.Timeline.AudioPlayableAsset
struct AudioPlayableAsset_t25D7AC7CC32AE16D05D04C479DE263E48E08CAC1;
// UnityEngine.Timeline.AudioTrack
struct AudioTrack_t2A6D40E2647460067ED433025B56D89554743502;
// UnityEngine.Timeline.ITimeControl
struct ITimeControl_t8E0598CDFDBCA33F6F6D40482E23B4CD5E184691;
// UnityEngine.Timeline.TimelineAsset
struct TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F;
// UnityEngine.Timeline.TimelineAsset/EditorSettings
struct EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9;
// UnityEngine.Timeline.TimelineClip
struct TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A;
// UnityEngine.Timeline.TimelineClip[]
struct TimelineClipU5BU5D_t54DF64E1454792297ECC9A75D1E33DB9293334A3;
// UnityEngine.Timeline.TrackAsset
struct TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC;
// UnityEngine.Timeline.TrackAsset[]
struct TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E;
// UnityEngine.Timeline.TrackBindingTypeAttribute
struct TrackBindingTypeAttribute_t5A3A97B13F678C07B1F43C3EB1C7531C64E85075;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.WWW
struct WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664;

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com;



#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#define U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADCONFIG_ADMINU3ED__61_T9FDE21B292E708C0BFEAE410AB1AD0802601B078_H
#define U3CLOADCONFIG_ADMINU3ED__61_T9FDE21B292E708C0BFEAE410AB1AD0802601B078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdministratorManager/<LoadConfig_Admin>d__61
struct  U3CLoadConfig_AdminU3Ed__61_t9FDE21B292E708C0BFEAE410AB1AD0802601B078  : public RuntimeObject
{
public:
	// System.Int32 AdministratorManager/<LoadConfig_Admin>d__61::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AdministratorManager/<LoadConfig_Admin>d__61::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String AdministratorManager/<LoadConfig_Admin>d__61::szFileName
	String_t* ___szFileName_2;
	// AdministratorManager AdministratorManager/<LoadConfig_Admin>d__61::<>4__this
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24 * ___U3CU3E4__this_3;
	// UnityEngine.WWW AdministratorManager/<LoadConfig_Admin>d__61::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdminU3Ed__61_t9FDE21B292E708C0BFEAE410AB1AD0802601B078, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdminU3Ed__61_t9FDE21B292E708C0BFEAE410AB1AD0802601B078, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_szFileName_2() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdminU3Ed__61_t9FDE21B292E708C0BFEAE410AB1AD0802601B078, ___szFileName_2)); }
	inline String_t* get_szFileName_2() const { return ___szFileName_2; }
	inline String_t** get_address_of_szFileName_2() { return &___szFileName_2; }
	inline void set_szFileName_2(String_t* value)
	{
		___szFileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdminU3Ed__61_t9FDE21B292E708C0BFEAE410AB1AD0802601B078, ___U3CU3E4__this_3)); }
	inline AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdminU3Ed__61_t9FDE21B292E708C0BFEAE410AB1AD0802601B078, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONFIG_ADMINU3ED__61_T9FDE21B292E708C0BFEAE410AB1AD0802601B078_H
#ifndef U3CLOADCONFIG_ADSU3ED__28_T7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660_H
#define U3CLOADCONFIG_ADSU3ED__28_T7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdsManager/<LoadConfig_Ads>d__28
struct  U3CLoadConfig_AdsU3Ed__28_t7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660  : public RuntimeObject
{
public:
	// System.Int32 AdsManager/<LoadConfig_Ads>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AdsManager/<LoadConfig_Ads>d__28::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String AdsManager/<LoadConfig_Ads>d__28::szFileName
	String_t* ___szFileName_2;
	// AdsManager AdsManager/<LoadConfig_Ads>d__28::<>4__this
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A * ___U3CU3E4__this_3;
	// UnityEngine.WWW AdsManager/<LoadConfig_Ads>d__28::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdsU3Ed__28_t7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdsU3Ed__28_t7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_szFileName_2() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdsU3Ed__28_t7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660, ___szFileName_2)); }
	inline String_t* get_szFileName_2() const { return ___szFileName_2; }
	inline String_t** get_address_of_szFileName_2() { return &___szFileName_2; }
	inline void set_szFileName_2(String_t* value)
	{
		___szFileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdsU3Ed__28_t7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660, ___U3CU3E4__this_3)); }
	inline AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdsU3Ed__28_t7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONFIG_ADSU3ED__28_T7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660_H
#ifndef U3CLOADCONFIG_ADVENTUREU3ED__58_T898A963584E587F83979F94CE96E82BA45A51A95_H
#define U3CLOADCONFIG_ADVENTUREU3ED__58_T898A963584E587F83979F94CE96E82BA45A51A95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdventureManager_New/<LoadConfig_Adventure>d__58
struct  U3CLoadConfig_AdventureU3Ed__58_t898A963584E587F83979F94CE96E82BA45A51A95  : public RuntimeObject
{
public:
	// System.Int32 AdventureManager_New/<LoadConfig_Adventure>d__58::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AdventureManager_New/<LoadConfig_Adventure>d__58::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String AdventureManager_New/<LoadConfig_Adventure>d__58::szFileName
	String_t* ___szFileName_2;
	// AdventureManager_New AdventureManager_New/<LoadConfig_Adventure>d__58::<>4__this
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA * ___U3CU3E4__this_3;
	// UnityEngine.WWW AdventureManager_New/<LoadConfig_Adventure>d__58::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdventureU3Ed__58_t898A963584E587F83979F94CE96E82BA45A51A95, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdventureU3Ed__58_t898A963584E587F83979F94CE96E82BA45A51A95, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_szFileName_2() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdventureU3Ed__58_t898A963584E587F83979F94CE96E82BA45A51A95, ___szFileName_2)); }
	inline String_t* get_szFileName_2() const { return ___szFileName_2; }
	inline String_t** get_address_of_szFileName_2() { return &___szFileName_2; }
	inline void set_szFileName_2(String_t* value)
	{
		___szFileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdventureU3Ed__58_t898A963584E587F83979F94CE96E82BA45A51A95, ___U3CU3E4__this_3)); }
	inline AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AdventureU3Ed__58_t898A963584E587F83979F94CE96E82BA45A51A95, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONFIG_ADVENTUREU3ED__58_T898A963584E587F83979F94CE96E82BA45A51A95_H
#ifndef EASYVIBRO_TB0B9B23A81DBB11562670BA10599582E401DDFB3_H
#define EASYVIBRO_TB0B9B23A81DBB11562670BA10599582E401DDFB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyVibro
struct  EasyVibro_tB0B9B23A81DBB11562670BA10599582E401DDFB3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASYVIBRO_TB0B9B23A81DBB11562670BA10599582E401DDFB3_H
#ifndef MMVIBRATIONMANAGER_TF61AC21066BC0D4EA57104C332CA7C2B52F37263_H
#define MMVIBRATIONMANAGER_TF61AC21066BC0D4EA57104C332CA7C2B52F37263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.NiceVibrations.MMVibrationManager
struct  MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263  : public RuntimeObject
{
public:

public:
};

struct MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields
{
public:
	// System.Int64 MoreMountains.NiceVibrations.MMVibrationManager::LightDuration
	int64_t ___LightDuration_0;
	// System.Int64 MoreMountains.NiceVibrations.MMVibrationManager::MediumDuration
	int64_t ___MediumDuration_1;
	// System.Int64 MoreMountains.NiceVibrations.MMVibrationManager::HeavyDuration
	int64_t ___HeavyDuration_2;
	// System.Int32 MoreMountains.NiceVibrations.MMVibrationManager::LightAmplitude
	int32_t ___LightAmplitude_3;
	// System.Int32 MoreMountains.NiceVibrations.MMVibrationManager::MediumAmplitude
	int32_t ___MediumAmplitude_4;
	// System.Int32 MoreMountains.NiceVibrations.MMVibrationManager::HeavyAmplitude
	int32_t ___HeavyAmplitude_5;
	// System.Int32 MoreMountains.NiceVibrations.MMVibrationManager::_sdkVersion
	int32_t ____sdkVersion_6;
	// System.Int64[] MoreMountains.NiceVibrations.MMVibrationManager::_successPattern
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____successPattern_7;
	// System.Int32[] MoreMountains.NiceVibrations.MMVibrationManager::_successPatternAmplitude
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____successPatternAmplitude_8;
	// System.Int64[] MoreMountains.NiceVibrations.MMVibrationManager::_warningPattern
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____warningPattern_9;
	// System.Int32[] MoreMountains.NiceVibrations.MMVibrationManager::_warningPatternAmplitude
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____warningPatternAmplitude_10;
	// System.Int64[] MoreMountains.NiceVibrations.MMVibrationManager::_failurePattern
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____failurePattern_11;
	// System.Int32[] MoreMountains.NiceVibrations.MMVibrationManager::_failurePatternAmplitude
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____failurePatternAmplitude_12;
	// UnityEngine.AndroidJavaClass MoreMountains.NiceVibrations.MMVibrationManager::UnityPlayer
	AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * ___UnityPlayer_13;
	// UnityEngine.AndroidJavaObject MoreMountains.NiceVibrations.MMVibrationManager::CurrentActivity
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ___CurrentActivity_14;
	// UnityEngine.AndroidJavaObject MoreMountains.NiceVibrations.MMVibrationManager::AndroidVibrator
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ___AndroidVibrator_15;
	// UnityEngine.AndroidJavaClass MoreMountains.NiceVibrations.MMVibrationManager::VibrationEffectClass
	AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * ___VibrationEffectClass_16;
	// UnityEngine.AndroidJavaObject MoreMountains.NiceVibrations.MMVibrationManager::VibrationEffect
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ___VibrationEffect_17;
	// System.Int32 MoreMountains.NiceVibrations.MMVibrationManager::DefaultAmplitude
	int32_t ___DefaultAmplitude_18;
	// System.Boolean MoreMountains.NiceVibrations.MMVibrationManager::iOSHapticsInitialized
	bool ___iOSHapticsInitialized_19;

public:
	inline static int32_t get_offset_of_LightDuration_0() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___LightDuration_0)); }
	inline int64_t get_LightDuration_0() const { return ___LightDuration_0; }
	inline int64_t* get_address_of_LightDuration_0() { return &___LightDuration_0; }
	inline void set_LightDuration_0(int64_t value)
	{
		___LightDuration_0 = value;
	}

	inline static int32_t get_offset_of_MediumDuration_1() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___MediumDuration_1)); }
	inline int64_t get_MediumDuration_1() const { return ___MediumDuration_1; }
	inline int64_t* get_address_of_MediumDuration_1() { return &___MediumDuration_1; }
	inline void set_MediumDuration_1(int64_t value)
	{
		___MediumDuration_1 = value;
	}

	inline static int32_t get_offset_of_HeavyDuration_2() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___HeavyDuration_2)); }
	inline int64_t get_HeavyDuration_2() const { return ___HeavyDuration_2; }
	inline int64_t* get_address_of_HeavyDuration_2() { return &___HeavyDuration_2; }
	inline void set_HeavyDuration_2(int64_t value)
	{
		___HeavyDuration_2 = value;
	}

	inline static int32_t get_offset_of_LightAmplitude_3() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___LightAmplitude_3)); }
	inline int32_t get_LightAmplitude_3() const { return ___LightAmplitude_3; }
	inline int32_t* get_address_of_LightAmplitude_3() { return &___LightAmplitude_3; }
	inline void set_LightAmplitude_3(int32_t value)
	{
		___LightAmplitude_3 = value;
	}

	inline static int32_t get_offset_of_MediumAmplitude_4() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___MediumAmplitude_4)); }
	inline int32_t get_MediumAmplitude_4() const { return ___MediumAmplitude_4; }
	inline int32_t* get_address_of_MediumAmplitude_4() { return &___MediumAmplitude_4; }
	inline void set_MediumAmplitude_4(int32_t value)
	{
		___MediumAmplitude_4 = value;
	}

	inline static int32_t get_offset_of_HeavyAmplitude_5() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___HeavyAmplitude_5)); }
	inline int32_t get_HeavyAmplitude_5() const { return ___HeavyAmplitude_5; }
	inline int32_t* get_address_of_HeavyAmplitude_5() { return &___HeavyAmplitude_5; }
	inline void set_HeavyAmplitude_5(int32_t value)
	{
		___HeavyAmplitude_5 = value;
	}

	inline static int32_t get_offset_of__sdkVersion_6() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____sdkVersion_6)); }
	inline int32_t get__sdkVersion_6() const { return ____sdkVersion_6; }
	inline int32_t* get_address_of__sdkVersion_6() { return &____sdkVersion_6; }
	inline void set__sdkVersion_6(int32_t value)
	{
		____sdkVersion_6 = value;
	}

	inline static int32_t get_offset_of__successPattern_7() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____successPattern_7)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__successPattern_7() const { return ____successPattern_7; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__successPattern_7() { return &____successPattern_7; }
	inline void set__successPattern_7(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____successPattern_7 = value;
		Il2CppCodeGenWriteBarrier((&____successPattern_7), value);
	}

	inline static int32_t get_offset_of__successPatternAmplitude_8() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____successPatternAmplitude_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__successPatternAmplitude_8() const { return ____successPatternAmplitude_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__successPatternAmplitude_8() { return &____successPatternAmplitude_8; }
	inline void set__successPatternAmplitude_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____successPatternAmplitude_8 = value;
		Il2CppCodeGenWriteBarrier((&____successPatternAmplitude_8), value);
	}

	inline static int32_t get_offset_of__warningPattern_9() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____warningPattern_9)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__warningPattern_9() const { return ____warningPattern_9; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__warningPattern_9() { return &____warningPattern_9; }
	inline void set__warningPattern_9(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____warningPattern_9 = value;
		Il2CppCodeGenWriteBarrier((&____warningPattern_9), value);
	}

	inline static int32_t get_offset_of__warningPatternAmplitude_10() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____warningPatternAmplitude_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__warningPatternAmplitude_10() const { return ____warningPatternAmplitude_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__warningPatternAmplitude_10() { return &____warningPatternAmplitude_10; }
	inline void set__warningPatternAmplitude_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____warningPatternAmplitude_10 = value;
		Il2CppCodeGenWriteBarrier((&____warningPatternAmplitude_10), value);
	}

	inline static int32_t get_offset_of__failurePattern_11() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____failurePattern_11)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__failurePattern_11() const { return ____failurePattern_11; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__failurePattern_11() { return &____failurePattern_11; }
	inline void set__failurePattern_11(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____failurePattern_11 = value;
		Il2CppCodeGenWriteBarrier((&____failurePattern_11), value);
	}

	inline static int32_t get_offset_of__failurePatternAmplitude_12() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ____failurePatternAmplitude_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__failurePatternAmplitude_12() const { return ____failurePatternAmplitude_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__failurePatternAmplitude_12() { return &____failurePatternAmplitude_12; }
	inline void set__failurePatternAmplitude_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____failurePatternAmplitude_12 = value;
		Il2CppCodeGenWriteBarrier((&____failurePatternAmplitude_12), value);
	}

	inline static int32_t get_offset_of_UnityPlayer_13() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___UnityPlayer_13)); }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * get_UnityPlayer_13() const { return ___UnityPlayer_13; }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 ** get_address_of_UnityPlayer_13() { return &___UnityPlayer_13; }
	inline void set_UnityPlayer_13(AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * value)
	{
		___UnityPlayer_13 = value;
		Il2CppCodeGenWriteBarrier((&___UnityPlayer_13), value);
	}

	inline static int32_t get_offset_of_CurrentActivity_14() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___CurrentActivity_14)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get_CurrentActivity_14() const { return ___CurrentActivity_14; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of_CurrentActivity_14() { return &___CurrentActivity_14; }
	inline void set_CurrentActivity_14(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		___CurrentActivity_14 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentActivity_14), value);
	}

	inline static int32_t get_offset_of_AndroidVibrator_15() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___AndroidVibrator_15)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get_AndroidVibrator_15() const { return ___AndroidVibrator_15; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of_AndroidVibrator_15() { return &___AndroidVibrator_15; }
	inline void set_AndroidVibrator_15(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		___AndroidVibrator_15 = value;
		Il2CppCodeGenWriteBarrier((&___AndroidVibrator_15), value);
	}

	inline static int32_t get_offset_of_VibrationEffectClass_16() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___VibrationEffectClass_16)); }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * get_VibrationEffectClass_16() const { return ___VibrationEffectClass_16; }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 ** get_address_of_VibrationEffectClass_16() { return &___VibrationEffectClass_16; }
	inline void set_VibrationEffectClass_16(AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * value)
	{
		___VibrationEffectClass_16 = value;
		Il2CppCodeGenWriteBarrier((&___VibrationEffectClass_16), value);
	}

	inline static int32_t get_offset_of_VibrationEffect_17() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___VibrationEffect_17)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get_VibrationEffect_17() const { return ___VibrationEffect_17; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of_VibrationEffect_17() { return &___VibrationEffect_17; }
	inline void set_VibrationEffect_17(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		___VibrationEffect_17 = value;
		Il2CppCodeGenWriteBarrier((&___VibrationEffect_17), value);
	}

	inline static int32_t get_offset_of_DefaultAmplitude_18() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___DefaultAmplitude_18)); }
	inline int32_t get_DefaultAmplitude_18() const { return ___DefaultAmplitude_18; }
	inline int32_t* get_address_of_DefaultAmplitude_18() { return &___DefaultAmplitude_18; }
	inline void set_DefaultAmplitude_18(int32_t value)
	{
		___DefaultAmplitude_18 = value;
	}

	inline static int32_t get_offset_of_iOSHapticsInitialized_19() { return static_cast<int32_t>(offsetof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields, ___iOSHapticsInitialized_19)); }
	inline bool get_iOSHapticsInitialized_19() const { return ___iOSHapticsInitialized_19; }
	inline bool* get_address_of_iOSHapticsInitialized_19() { return &___iOSHapticsInitialized_19; }
	inline void set_iOSHapticsInitialized_19(bool value)
	{
		___iOSHapticsInitialized_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MMVIBRATIONMANAGER_TF61AC21066BC0D4EA57104C332CA7C2B52F37263_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef PLAYABLEBEHAVIOUR_T5F4AA32E735199182CC5F57D426D27BE8ABA8F01_H
#define PLAYABLEBEHAVIOUR_T5F4AA32E735199182CC5F57D426D27BE8ABA8F01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T5F4AA32E735199182CC5F57D426D27BE8ABA8F01_H
#ifndef ANIMATIONPLAYABLEASSETUPGRADE_TD716C801DFEEC8A11527FC7E5AFD12BA3CF51C96_H
#define ANIMATIONPLAYABLEASSETUPGRADE_TD716C801DFEEC8A11527FC7E5AFD12BA3CF51C96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationPlayableAsset/AnimationPlayableAssetUpgrade
struct  AnimationPlayableAssetUpgrade_tD716C801DFEEC8A11527FC7E5AFD12BA3CF51C96  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEASSETUPGRADE_TD716C801DFEEC8A11527FC7E5AFD12BA3CF51C96_H
#ifndef ANIMATIONTRACKUPGRADE_T1C1BA9D56B6B7D6765318B5D882F218A83B8D448_H
#define ANIMATIONTRACKUPGRADE_T1C1BA9D56B6B7D6765318B5D882F218A83B8D448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationTrack/AnimationTrackUpgrade
struct  AnimationTrackUpgrade_t1C1BA9D56B6B7D6765318B5D882F218A83B8D448  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTRACKUPGRADE_T1C1BA9D56B6B7D6765318B5D882F218A83B8D448_H
#ifndef U3CGETCONTROLABLESCRIPTSU3EC__ITERATOR0_T3F28F4AD4FF876117B187D5AF299DCE19E104D07_H
#define U3CGETCONTROLABLESCRIPTSU3EC__ITERATOR0_T3F28F4AD4FF876117B187D5AF299DCE19E104D07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0
struct  U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::root
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___root_0;
	// UnityEngine.MonoBehaviour[] UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$locvar0
	MonoBehaviourU5BU5D_tE94AB2103D501801597F2F14B6A1642246B35347* ___U24locvar0_1;
	// System.Int32 UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// UnityEngine.MonoBehaviour UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::<script>__1
	MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___U3CscriptU3E__1_3;
	// UnityEngine.MonoBehaviour UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$current
	MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___U24current_4;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07, ___root_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_root_0() const { return ___root_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07, ___U24locvar0_1)); }
	inline MonoBehaviourU5BU5D_tE94AB2103D501801597F2F14B6A1642246B35347* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline MonoBehaviourU5BU5D_tE94AB2103D501801597F2F14B6A1642246B35347** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(MonoBehaviourU5BU5D_tE94AB2103D501801597F2F14B6A1642246B35347* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CscriptU3E__1_3() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07, ___U3CscriptU3E__1_3)); }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * get_U3CscriptU3E__1_3() const { return ___U3CscriptU3E__1_3; }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** get_address_of_U3CscriptU3E__1_3() { return &___U3CscriptU3E__1_3; }
	inline void set_U3CscriptU3E__1_3(MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		___U3CscriptU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscriptU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07, ___U24current_4)); }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * get_U24current_4() const { return ___U24current_4; }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCONTROLABLESCRIPTSU3EC__ITERATOR0_T3F28F4AD4FF876117B187D5AF299DCE19E104D07_H
#ifndef EXTRAPOLATION_TB50CEF33A8A4779DC79A55C0B74A92BD00E6E89D_H
#define EXTRAPOLATION_TB50CEF33A8A4779DC79A55C0B74A92BD00E6E89D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.Extrapolation
struct  Extrapolation_tB50CEF33A8A4779DC79A55C0B74A92BD00E6E89D  : public RuntimeObject
{
public:

public:
};

struct Extrapolation_tB50CEF33A8A4779DC79A55C0B74A92BD00E6E89D_StaticFields
{
public:
	// System.Double UnityEngine.Timeline.Extrapolation::kMinExtrapolationTime
	double ___kMinExtrapolationTime_0;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.Extrapolation::<>f__am$cache0
	Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_kMinExtrapolationTime_0() { return static_cast<int32_t>(offsetof(Extrapolation_tB50CEF33A8A4779DC79A55C0B74A92BD00E6E89D_StaticFields, ___kMinExtrapolationTime_0)); }
	inline double get_kMinExtrapolationTime_0() const { return ___kMinExtrapolationTime_0; }
	inline double* get_address_of_kMinExtrapolationTime_0() { return &___kMinExtrapolationTime_0; }
	inline void set_kMinExtrapolationTime_0(double value)
	{
		___kMinExtrapolationTime_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(Extrapolation_tB50CEF33A8A4779DC79A55C0B74A92BD00E6E89D_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTRAPOLATION_TB50CEF33A8A4779DC79A55C0B74A92BD00E6E89D_H
#ifndef HASHUTILITY_T1DD34AC09A13D051A3FED020EB281D0F3FC0E4B7_H
#define HASHUTILITY_T1DD34AC09A13D051A3FED020EB281D0F3FC0E4B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.HashUtility
struct  HashUtility_t1DD34AC09A13D051A3FED020EB281D0F3FC0E4B7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHUTILITY_T1DD34AC09A13D051A3FED020EB281D0F3FC0E4B7_H
#ifndef RUNTIMEELEMENT_T25E724CA4B982CE9EAD77996253FA3008561ED80_H
#define RUNTIMEELEMENT_T25E724CA4B982CE9EAD77996253FA3008561ED80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.RuntimeElement
struct  RuntimeElement_t25E724CA4B982CE9EAD77996253FA3008561ED80  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEELEMENT_T25E724CA4B982CE9EAD77996253FA3008561ED80_H
#ifndef TIMEUTILITY_TE3DFA7C9986845BDDF19995B106FA30FBE32D870_H
#define TIMEUTILITY_TE3DFA7C9986845BDDF19995B106FA30FBE32D870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimeUtility
struct  TimeUtility_tE3DFA7C9986845BDDF19995B106FA30FBE32D870  : public RuntimeObject
{
public:

public:
};

struct TimeUtility_tE3DFA7C9986845BDDF19995B106FA30FBE32D870_StaticFields
{
public:
	// System.Double UnityEngine.Timeline.TimeUtility::kTimeEpsilon
	double ___kTimeEpsilon_0;
	// System.Double UnityEngine.Timeline.TimeUtility::kFrameRateEpsilon
	double ___kFrameRateEpsilon_1;
	// System.Double UnityEngine.Timeline.TimeUtility::k_MaxTimelineDurationInSeconds
	double ___k_MaxTimelineDurationInSeconds_2;

public:
	inline static int32_t get_offset_of_kTimeEpsilon_0() { return static_cast<int32_t>(offsetof(TimeUtility_tE3DFA7C9986845BDDF19995B106FA30FBE32D870_StaticFields, ___kTimeEpsilon_0)); }
	inline double get_kTimeEpsilon_0() const { return ___kTimeEpsilon_0; }
	inline double* get_address_of_kTimeEpsilon_0() { return &___kTimeEpsilon_0; }
	inline void set_kTimeEpsilon_0(double value)
	{
		___kTimeEpsilon_0 = value;
	}

	inline static int32_t get_offset_of_kFrameRateEpsilon_1() { return static_cast<int32_t>(offsetof(TimeUtility_tE3DFA7C9986845BDDF19995B106FA30FBE32D870_StaticFields, ___kFrameRateEpsilon_1)); }
	inline double get_kFrameRateEpsilon_1() const { return ___kFrameRateEpsilon_1; }
	inline double* get_address_of_kFrameRateEpsilon_1() { return &___kFrameRateEpsilon_1; }
	inline void set_kFrameRateEpsilon_1(double value)
	{
		___kFrameRateEpsilon_1 = value;
	}

	inline static int32_t get_offset_of_k_MaxTimelineDurationInSeconds_2() { return static_cast<int32_t>(offsetof(TimeUtility_tE3DFA7C9986845BDDF19995B106FA30FBE32D870_StaticFields, ___k_MaxTimelineDurationInSeconds_2)); }
	inline double get_k_MaxTimelineDurationInSeconds_2() const { return ___k_MaxTimelineDurationInSeconds_2; }
	inline double* get_address_of_k_MaxTimelineDurationInSeconds_2() { return &___k_MaxTimelineDurationInSeconds_2; }
	inline void set_k_MaxTimelineDurationInSeconds_2(double value)
	{
		___k_MaxTimelineDurationInSeconds_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEUTILITY_TE3DFA7C9986845BDDF19995B106FA30FBE32D870_H
#ifndef EDITORSETTINGS_TD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9_H
#define EDITORSETTINGS_TD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/EditorSettings
struct  EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9  : public RuntimeObject
{
public:
	// System.Single UnityEngine.Timeline.TimelineAsset/EditorSettings::m_Framerate
	float ___m_Framerate_3;

public:
	inline static int32_t get_offset_of_m_Framerate_3() { return static_cast<int32_t>(offsetof(EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9, ___m_Framerate_3)); }
	inline float get_m_Framerate_3() const { return ___m_Framerate_3; }
	inline float* get_address_of_m_Framerate_3() { return &___m_Framerate_3; }
	inline void set_m_Framerate_3(float value)
	{
		___m_Framerate_3 = value;
	}
};

struct EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9_StaticFields
{
public:
	// System.Single UnityEngine.Timeline.TimelineAsset/EditorSettings::kMinFps
	float ___kMinFps_0;
	// System.Single UnityEngine.Timeline.TimelineAsset/EditorSettings::kMaxFps
	float ___kMaxFps_1;
	// System.Single UnityEngine.Timeline.TimelineAsset/EditorSettings::kDefaultFps
	float ___kDefaultFps_2;

public:
	inline static int32_t get_offset_of_kMinFps_0() { return static_cast<int32_t>(offsetof(EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9_StaticFields, ___kMinFps_0)); }
	inline float get_kMinFps_0() const { return ___kMinFps_0; }
	inline float* get_address_of_kMinFps_0() { return &___kMinFps_0; }
	inline void set_kMinFps_0(float value)
	{
		___kMinFps_0 = value;
	}

	inline static int32_t get_offset_of_kMaxFps_1() { return static_cast<int32_t>(offsetof(EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9_StaticFields, ___kMaxFps_1)); }
	inline float get_kMaxFps_1() const { return ___kMaxFps_1; }
	inline float* get_address_of_kMaxFps_1() { return &___kMaxFps_1; }
	inline void set_kMaxFps_1(float value)
	{
		___kMaxFps_1 = value;
	}

	inline static int32_t get_offset_of_kDefaultFps_2() { return static_cast<int32_t>(offsetof(EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9_StaticFields, ___kDefaultFps_2)); }
	inline float get_kDefaultFps_2() const { return ___kDefaultFps_2; }
	inline float* get_address_of_kDefaultFps_2() { return &___kDefaultFps_2; }
	inline void set_kDefaultFps_2(float value)
	{
		___kDefaultFps_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORSETTINGS_TD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9_H
#ifndef TIMELINECLIPUPGRADE_T2E99A55A584778D37767485393B076B1402B4B14_H
#define TIMELINECLIPUPGRADE_T2E99A55A584778D37767485393B076B1402B4B14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClip/TimelineClipUpgrade
struct  TimelineClipUpgrade_t2E99A55A584778D37767485393B076B1402B4B14  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINECLIPUPGRADE_T2E99A55A584778D37767485393B076B1402B4B14_H
#ifndef TIMELINECREATEUTILITIES_T39126B4401FD5B6D8CD74FF6DDA8C88652F215D1_H
#define TIMELINECREATEUTILITIES_T39126B4401FD5B6D8CD74FF6DDA8C88652F215D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineCreateUtilities
struct  TimelineCreateUtilities_t39126B4401FD5B6D8CD74FF6DDA8C88652F215D1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINECREATEUTILITIES_T39126B4401FD5B6D8CD74FF6DDA8C88652F215D1_H
#ifndef U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY0_T12FED3630926B0AEF98BCDC8558A5C3E1EA680CD_H
#define U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY0_T12FED3630926B0AEF98BCDC8558A5C3E1EA680CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey0
struct  U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t12FED3630926B0AEF98BCDC8558A5C3E1EA680CD  : public RuntimeObject
{
public:
	// System.String UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t12FED3630926B0AEF98BCDC8558A5C3E1EA680CD, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY0_T12FED3630926B0AEF98BCDC8558A5C3E1EA680CD_H
#ifndef U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY1_T978BA28AD54A9E03E86679B80567BB82B62BCD5E_H
#define U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY1_T978BA28AD54A9E03E86679B80567BB82B62BCD5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey1
struct  U3CGenerateUniqueActorNameU3Ec__AnonStorey1_t978BA28AD54A9E03E86679B80567BB82B62BCD5E  : public RuntimeObject
{
public:
	// System.String UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey1::result
	String_t* ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CGenerateUniqueActorNameU3Ec__AnonStorey1_t978BA28AD54A9E03E86679B80567BB82B62BCD5E, ___result_0)); }
	inline String_t* get_result_0() const { return ___result_0; }
	inline String_t** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(String_t* value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier((&___result_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY1_T978BA28AD54A9E03E86679B80567BB82B62BCD5E_H
#ifndef TIMELINEUNDO_T194ECE9DA26071CE759EDD3D1E667AC46B3D05D7_H
#define TIMELINEUNDO_T194ECE9DA26071CE759EDD3D1E667AC46B3D05D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineUndo
struct  TimelineUndo_t194ECE9DA26071CE759EDD3D1E667AC46B3D05D7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEUNDO_T194ECE9DA26071CE759EDD3D1E667AC46B3D05D7_H
#ifndef SREWARDCONFIG_T1C8055701C09EA49E5691E40DE09E716269FDB80_H
#define SREWARDCONFIG_T1C8055701C09EA49E5691E40DE09E716269FDB80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdventureManager_New/sRewardConfig
struct  sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80 
{
public:
	// System.Int32 AdventureManager_New/sRewardConfig::nItemId
	int32_t ___nItemId_0;
	// System.Int32 AdventureManager_New/sRewardConfig::nMaxNum
	int32_t ___nMaxNum_1;
	// System.Int32 AdventureManager_New/sRewardConfig::nMinNum
	int32_t ___nMinNum_2;
	// System.Int32 AdventureManager_New/sRewardConfig::nNum
	int32_t ___nNum_3;

public:
	inline static int32_t get_offset_of_nItemId_0() { return static_cast<int32_t>(offsetof(sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80, ___nItemId_0)); }
	inline int32_t get_nItemId_0() const { return ___nItemId_0; }
	inline int32_t* get_address_of_nItemId_0() { return &___nItemId_0; }
	inline void set_nItemId_0(int32_t value)
	{
		___nItemId_0 = value;
	}

	inline static int32_t get_offset_of_nMaxNum_1() { return static_cast<int32_t>(offsetof(sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80, ___nMaxNum_1)); }
	inline int32_t get_nMaxNum_1() const { return ___nMaxNum_1; }
	inline int32_t* get_address_of_nMaxNum_1() { return &___nMaxNum_1; }
	inline void set_nMaxNum_1(int32_t value)
	{
		___nMaxNum_1 = value;
	}

	inline static int32_t get_offset_of_nMinNum_2() { return static_cast<int32_t>(offsetof(sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80, ___nMinNum_2)); }
	inline int32_t get_nMinNum_2() const { return ___nMinNum_2; }
	inline int32_t* get_address_of_nMinNum_2() { return &___nMinNum_2; }
	inline void set_nMinNum_2(int32_t value)
	{
		___nMinNum_2 = value;
	}

	inline static int32_t get_offset_of_nNum_3() { return static_cast<int32_t>(offsetof(sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80, ___nNum_3)); }
	inline int32_t get_nNum_3() const { return ___nNum_3; }
	inline int32_t* get_address_of_nNum_3() { return &___nNum_3; }
	inline void set_nNum_3(int32_t value)
	{
		___nNum_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SREWARDCONFIG_T1C8055701C09EA49E5691E40DE09E716269FDB80_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef PROPERTYNAME_T75EB843FEA2EC372093479A35C24364D2DF98529_H
#define PROPERTYNAME_T75EB843FEA2EC372093479A35C24364D2DF98529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t75EB843FEA2EC372093479A35C24364D2DF98529 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t75EB843FEA2EC372093479A35C24364D2DF98529, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T75EB843FEA2EC372093479A35C24364D2DF98529_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef DIRECTORCONTROLPLAYABLE_T86AA53337442CC16F17A1BECE410318BD1E36819_H
#define DIRECTORCONTROLPLAYABLE_T86AA53337442CC16F17A1BECE410318BD1E36819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.DirectorControlPlayable
struct  DirectorControlPlayable_t86AA53337442CC16F17A1BECE410318BD1E36819  : public PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01
{
public:
	// UnityEngine.Playables.PlayableDirector UnityEngine.Timeline.DirectorControlPlayable::director
	PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * ___director_0;
	// System.Boolean UnityEngine.Timeline.DirectorControlPlayable::m_SyncTime
	bool ___m_SyncTime_1;
	// System.Double UnityEngine.Timeline.DirectorControlPlayable::m_AssetDuration
	double ___m_AssetDuration_2;

public:
	inline static int32_t get_offset_of_director_0() { return static_cast<int32_t>(offsetof(DirectorControlPlayable_t86AA53337442CC16F17A1BECE410318BD1E36819, ___director_0)); }
	inline PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * get_director_0() const { return ___director_0; }
	inline PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 ** get_address_of_director_0() { return &___director_0; }
	inline void set_director_0(PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * value)
	{
		___director_0 = value;
		Il2CppCodeGenWriteBarrier((&___director_0), value);
	}

	inline static int32_t get_offset_of_m_SyncTime_1() { return static_cast<int32_t>(offsetof(DirectorControlPlayable_t86AA53337442CC16F17A1BECE410318BD1E36819, ___m_SyncTime_1)); }
	inline bool get_m_SyncTime_1() const { return ___m_SyncTime_1; }
	inline bool* get_address_of_m_SyncTime_1() { return &___m_SyncTime_1; }
	inline void set_m_SyncTime_1(bool value)
	{
		___m_SyncTime_1 = value;
	}

	inline static int32_t get_offset_of_m_AssetDuration_2() { return static_cast<int32_t>(offsetof(DirectorControlPlayable_t86AA53337442CC16F17A1BECE410318BD1E36819, ___m_AssetDuration_2)); }
	inline double get_m_AssetDuration_2() const { return ___m_AssetDuration_2; }
	inline double* get_address_of_m_AssetDuration_2() { return &___m_AssetDuration_2; }
	inline void set_m_AssetDuration_2(double value)
	{
		___m_AssetDuration_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTORCONTROLPLAYABLE_T86AA53337442CC16F17A1BECE410318BD1E36819_H
#ifndef DISCRETETIME_T046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC_H
#define DISCRETETIME_T046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.DiscreteTime
struct  DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC 
{
public:
	// System.Int64 UnityEngine.Timeline.DiscreteTime::m_DiscreteTime
	int64_t ___m_DiscreteTime_2;

public:
	inline static int32_t get_offset_of_m_DiscreteTime_2() { return static_cast<int32_t>(offsetof(DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC, ___m_DiscreteTime_2)); }
	inline int64_t get_m_DiscreteTime_2() const { return ___m_DiscreteTime_2; }
	inline int64_t* get_address_of_m_DiscreteTime_2() { return &___m_DiscreteTime_2; }
	inline void set_m_DiscreteTime_2(int64_t value)
	{
		___m_DiscreteTime_2 = value;
	}
};

struct DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC_StaticFields
{
public:
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.DiscreteTime::kMaxTime
	DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  ___kMaxTime_1;

public:
	inline static int32_t get_offset_of_kMaxTime_1() { return static_cast<int32_t>(offsetof(DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC_StaticFields, ___kMaxTime_1)); }
	inline DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  get_kMaxTime_1() const { return ___kMaxTime_1; }
	inline DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC * get_address_of_kMaxTime_1() { return &___kMaxTime_1; }
	inline void set_kMaxTime_1(DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  value)
	{
		___kMaxTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCRETETIME_T046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC_H
#ifndef IGNOREONPLAYABLETRACKATTRIBUTE_TB5C2D6CF60A235822F64E8448220E8D2EA9636F6_H
#define IGNOREONPLAYABLETRACKATTRIBUTE_TB5C2D6CF60A235822F64E8448220E8D2EA9636F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.IgnoreOnPlayableTrackAttribute
struct  IgnoreOnPlayableTrackAttribute_tB5C2D6CF60A235822F64E8448220E8D2EA9636F6  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNOREONPLAYABLETRACKATTRIBUTE_TB5C2D6CF60A235822F64E8448220E8D2EA9636F6_H
#ifndef NOTKEYABLEATTRIBUTE_T9A08F5AF42255B6FC72A98AB694411430CD395DA_H
#define NOTKEYABLEATTRIBUTE_T9A08F5AF42255B6FC72A98AB694411430CD395DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.NotKeyableAttribute
struct  NotKeyableAttribute_t9A08F5AF42255B6FC72A98AB694411430CD395DA  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTKEYABLEATTRIBUTE_T9A08F5AF42255B6FC72A98AB694411430CD395DA_H
#ifndef PARTICLECONTROLPLAYABLE_T6F6891ACE77DFDD968CF3655D21615750D8D5AB9_H
#define PARTICLECONTROLPLAYABLE_T6F6891ACE77DFDD968CF3655D21615750D8D5AB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ParticleControlPlayable
struct  ParticleControlPlayable_t6F6891ACE77DFDD968CF3655D21615750D8D5AB9  : public PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01
{
public:
	// System.Single UnityEngine.Timeline.ParticleControlPlayable::m_LastTime
	float ___m_LastTime_0;
	// System.UInt32 UnityEngine.Timeline.ParticleControlPlayable::m_RandomSeed
	uint32_t ___m_RandomSeed_1;
	// System.Single UnityEngine.Timeline.ParticleControlPlayable::m_SystemTime
	float ___m_SystemTime_2;
	// UnityEngine.ParticleSystem UnityEngine.Timeline.ParticleControlPlayable::<particleSystem>k__BackingField
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___U3CparticleSystemU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_LastTime_0() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t6F6891ACE77DFDD968CF3655D21615750D8D5AB9, ___m_LastTime_0)); }
	inline float get_m_LastTime_0() const { return ___m_LastTime_0; }
	inline float* get_address_of_m_LastTime_0() { return &___m_LastTime_0; }
	inline void set_m_LastTime_0(float value)
	{
		___m_LastTime_0 = value;
	}

	inline static int32_t get_offset_of_m_RandomSeed_1() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t6F6891ACE77DFDD968CF3655D21615750D8D5AB9, ___m_RandomSeed_1)); }
	inline uint32_t get_m_RandomSeed_1() const { return ___m_RandomSeed_1; }
	inline uint32_t* get_address_of_m_RandomSeed_1() { return &___m_RandomSeed_1; }
	inline void set_m_RandomSeed_1(uint32_t value)
	{
		___m_RandomSeed_1 = value;
	}

	inline static int32_t get_offset_of_m_SystemTime_2() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t6F6891ACE77DFDD968CF3655D21615750D8D5AB9, ___m_SystemTime_2)); }
	inline float get_m_SystemTime_2() const { return ___m_SystemTime_2; }
	inline float* get_address_of_m_SystemTime_2() { return &___m_SystemTime_2; }
	inline void set_m_SystemTime_2(float value)
	{
		___m_SystemTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CparticleSystemU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t6F6891ACE77DFDD968CF3655D21615750D8D5AB9, ___U3CparticleSystemU3Ek__BackingField_3)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_U3CparticleSystemU3Ek__BackingField_3() const { return ___U3CparticleSystemU3Ek__BackingField_3; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_U3CparticleSystemU3Ek__BackingField_3() { return &___U3CparticleSystemU3Ek__BackingField_3; }
	inline void set_U3CparticleSystemU3Ek__BackingField_3(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___U3CparticleSystemU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparticleSystemU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECONTROLPLAYABLE_T6F6891ACE77DFDD968CF3655D21615750D8D5AB9_H
#ifndef PREFABCONTROLPLAYABLE_TC88C9AA8E25E0AC486369104E9E34CAD49A610E6_H
#define PREFABCONTROLPLAYABLE_TC88C9AA8E25E0AC486369104E9E34CAD49A610E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.PrefabControlPlayable
struct  PrefabControlPlayable_tC88C9AA8E25E0AC486369104E9E34CAD49A610E6  : public PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01
{
public:
	// UnityEngine.GameObject UnityEngine.Timeline.PrefabControlPlayable::m_Instance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_Instance_0;

public:
	inline static int32_t get_offset_of_m_Instance_0() { return static_cast<int32_t>(offsetof(PrefabControlPlayable_tC88C9AA8E25E0AC486369104E9E34CAD49A610E6, ___m_Instance_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_Instance_0() const { return ___m_Instance_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_Instance_0() { return &___m_Instance_0; }
	inline void set_m_Instance_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABCONTROLPLAYABLE_TC88C9AA8E25E0AC486369104E9E34CAD49A610E6_H
#ifndef RUNTIMECLIPBASE_T4BE09F69F59AE0FD3F61D1C3E0DE29F236776083_H
#define RUNTIMECLIPBASE_T4BE09F69F59AE0FD3F61D1C3E0DE29F236776083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.RuntimeClipBase
struct  RuntimeClipBase_t4BE09F69F59AE0FD3F61D1C3E0DE29F236776083  : public RuntimeElement_t25E724CA4B982CE9EAD77996253FA3008561ED80
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMECLIPBASE_T4BE09F69F59AE0FD3F61D1C3E0DE29F236776083_H
#ifndef SUPPORTSCHILDTRACKSATTRIBUTE_TF20FD282A93BCE20932E0A8A030A694D0A8B3E8D_H
#define SUPPORTSCHILDTRACKSATTRIBUTE_TF20FD282A93BCE20932E0A8A030A694D0A8B3E8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.SupportsChildTracksAttribute
struct  SupportsChildTracksAttribute_tF20FD282A93BCE20932E0A8A030A694D0A8B3E8D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type UnityEngine.Timeline.SupportsChildTracksAttribute::childType
	Type_t * ___childType_0;
	// System.Int32 UnityEngine.Timeline.SupportsChildTracksAttribute::levels
	int32_t ___levels_1;

public:
	inline static int32_t get_offset_of_childType_0() { return static_cast<int32_t>(offsetof(SupportsChildTracksAttribute_tF20FD282A93BCE20932E0A8A030A694D0A8B3E8D, ___childType_0)); }
	inline Type_t * get_childType_0() const { return ___childType_0; }
	inline Type_t ** get_address_of_childType_0() { return &___childType_0; }
	inline void set_childType_0(Type_t * value)
	{
		___childType_0 = value;
		Il2CppCodeGenWriteBarrier((&___childType_0), value);
	}

	inline static int32_t get_offset_of_levels_1() { return static_cast<int32_t>(offsetof(SupportsChildTracksAttribute_tF20FD282A93BCE20932E0A8A030A694D0A8B3E8D, ___levels_1)); }
	inline int32_t get_levels_1() const { return ___levels_1; }
	inline int32_t* get_address_of_levels_1() { return &___levels_1; }
	inline void set_levels_1(int32_t value)
	{
		___levels_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTSCHILDTRACKSATTRIBUTE_TF20FD282A93BCE20932E0A8A030A694D0A8B3E8D_H
#ifndef TIMECONTROLPLAYABLE_TF087C878D138911D1FC78E62AB74C1B1AF74C630_H
#define TIMECONTROLPLAYABLE_TF087C878D138911D1FC78E62AB74C1B1AF74C630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimeControlPlayable
struct  TimeControlPlayable_tF087C878D138911D1FC78E62AB74C1B1AF74C630  : public PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01
{
public:
	// UnityEngine.Timeline.ITimeControl UnityEngine.Timeline.TimeControlPlayable::m_timeControl
	RuntimeObject* ___m_timeControl_0;

public:
	inline static int32_t get_offset_of_m_timeControl_0() { return static_cast<int32_t>(offsetof(TimeControlPlayable_tF087C878D138911D1FC78E62AB74C1B1AF74C630, ___m_timeControl_0)); }
	inline RuntimeObject* get_m_timeControl_0() const { return ___m_timeControl_0; }
	inline RuntimeObject** get_address_of_m_timeControl_0() { return &___m_timeControl_0; }
	inline void set_m_timeControl_0(RuntimeObject* value)
	{
		___m_timeControl_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_timeControl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMECONTROLPLAYABLE_TF087C878D138911D1FC78E62AB74C1B1AF74C630_H
#ifndef TIMELINEPLAYABLE_T898A842A48799FF7C7D8CC53110B6AFD4F727283_H
#define TIMELINEPLAYABLE_T898A842A48799FF7C7D8CC53110B6AFD4F727283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelinePlayable
struct  TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283  : public PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01
{
public:
	// UnityEngine.IntervalTree`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_IntervalTree
	IntervalTree_1_t743FB00DC6F26F299230267DDE0DB435606C872D * ___m_IntervalTree_0;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_ActiveClips
	List_1_t6F7766D50A23EA9CCE03264AAD6B9B008F54FEDF * ___m_ActiveClips_1;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_CurrentListOfActiveClips
	List_1_t6F7766D50A23EA9CCE03264AAD6B9B008F54FEDF * ___m_CurrentListOfActiveClips_2;
	// System.Int32 UnityEngine.Timeline.TimelinePlayable::m_ActiveBit
	int32_t ___m_ActiveBit_3;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.ITimelineEvaluateCallback> UnityEngine.Timeline.TimelinePlayable::m_EvaluateCallbacks
	List_1_t8D4337C886AB0EA34617A8D20ABB1FE379E68A60 * ___m_EvaluateCallbacks_4;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Timeline.TrackAsset,UnityEngine.Playables.Playable> UnityEngine.Timeline.TimelinePlayable::m_PlayableCache
	Dictionary_2_tA07AA2153D8A38620011D13B62D2BD03F1366F47 * ___m_PlayableCache_5;

public:
	inline static int32_t get_offset_of_m_IntervalTree_0() { return static_cast<int32_t>(offsetof(TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283, ___m_IntervalTree_0)); }
	inline IntervalTree_1_t743FB00DC6F26F299230267DDE0DB435606C872D * get_m_IntervalTree_0() const { return ___m_IntervalTree_0; }
	inline IntervalTree_1_t743FB00DC6F26F299230267DDE0DB435606C872D ** get_address_of_m_IntervalTree_0() { return &___m_IntervalTree_0; }
	inline void set_m_IntervalTree_0(IntervalTree_1_t743FB00DC6F26F299230267DDE0DB435606C872D * value)
	{
		___m_IntervalTree_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_IntervalTree_0), value);
	}

	inline static int32_t get_offset_of_m_ActiveClips_1() { return static_cast<int32_t>(offsetof(TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283, ___m_ActiveClips_1)); }
	inline List_1_t6F7766D50A23EA9CCE03264AAD6B9B008F54FEDF * get_m_ActiveClips_1() const { return ___m_ActiveClips_1; }
	inline List_1_t6F7766D50A23EA9CCE03264AAD6B9B008F54FEDF ** get_address_of_m_ActiveClips_1() { return &___m_ActiveClips_1; }
	inline void set_m_ActiveClips_1(List_1_t6F7766D50A23EA9CCE03264AAD6B9B008F54FEDF * value)
	{
		___m_ActiveClips_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveClips_1), value);
	}

	inline static int32_t get_offset_of_m_CurrentListOfActiveClips_2() { return static_cast<int32_t>(offsetof(TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283, ___m_CurrentListOfActiveClips_2)); }
	inline List_1_t6F7766D50A23EA9CCE03264AAD6B9B008F54FEDF * get_m_CurrentListOfActiveClips_2() const { return ___m_CurrentListOfActiveClips_2; }
	inline List_1_t6F7766D50A23EA9CCE03264AAD6B9B008F54FEDF ** get_address_of_m_CurrentListOfActiveClips_2() { return &___m_CurrentListOfActiveClips_2; }
	inline void set_m_CurrentListOfActiveClips_2(List_1_t6F7766D50A23EA9CCE03264AAD6B9B008F54FEDF * value)
	{
		___m_CurrentListOfActiveClips_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentListOfActiveClips_2), value);
	}

	inline static int32_t get_offset_of_m_ActiveBit_3() { return static_cast<int32_t>(offsetof(TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283, ___m_ActiveBit_3)); }
	inline int32_t get_m_ActiveBit_3() const { return ___m_ActiveBit_3; }
	inline int32_t* get_address_of_m_ActiveBit_3() { return &___m_ActiveBit_3; }
	inline void set_m_ActiveBit_3(int32_t value)
	{
		___m_ActiveBit_3 = value;
	}

	inline static int32_t get_offset_of_m_EvaluateCallbacks_4() { return static_cast<int32_t>(offsetof(TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283, ___m_EvaluateCallbacks_4)); }
	inline List_1_t8D4337C886AB0EA34617A8D20ABB1FE379E68A60 * get_m_EvaluateCallbacks_4() const { return ___m_EvaluateCallbacks_4; }
	inline List_1_t8D4337C886AB0EA34617A8D20ABB1FE379E68A60 ** get_address_of_m_EvaluateCallbacks_4() { return &___m_EvaluateCallbacks_4; }
	inline void set_m_EvaluateCallbacks_4(List_1_t8D4337C886AB0EA34617A8D20ABB1FE379E68A60 * value)
	{
		___m_EvaluateCallbacks_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EvaluateCallbacks_4), value);
	}

	inline static int32_t get_offset_of_m_PlayableCache_5() { return static_cast<int32_t>(offsetof(TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283, ___m_PlayableCache_5)); }
	inline Dictionary_2_tA07AA2153D8A38620011D13B62D2BD03F1366F47 * get_m_PlayableCache_5() const { return ___m_PlayableCache_5; }
	inline Dictionary_2_tA07AA2153D8A38620011D13B62D2BD03F1366F47 ** get_address_of_m_PlayableCache_5() { return &___m_PlayableCache_5; }
	inline void set_m_PlayableCache_5(Dictionary_2_tA07AA2153D8A38620011D13B62D2BD03F1366F47 * value)
	{
		___m_PlayableCache_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayableCache_5), value);
	}
};

struct TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283_StaticFields
{
public:
	// System.Boolean UnityEngine.Timeline.TimelinePlayable::muteAudioScrubbing
	bool ___muteAudioScrubbing_6;

public:
	inline static int32_t get_offset_of_muteAudioScrubbing_6() { return static_cast<int32_t>(offsetof(TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283_StaticFields, ___muteAudioScrubbing_6)); }
	inline bool get_muteAudioScrubbing_6() const { return ___muteAudioScrubbing_6; }
	inline bool* get_address_of_muteAudioScrubbing_6() { return &___muteAudioScrubbing_6; }
	inline void set_muteAudioScrubbing_6(bool value)
	{
		___muteAudioScrubbing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEPLAYABLE_T898A842A48799FF7C7D8CC53110B6AFD4F727283_H
#ifndef TRACKCLIPTYPEATTRIBUTE_T36C1E6CD0C0EBAE99E7DAE0AD1F9630EDCE4FC0B_H
#define TRACKCLIPTYPEATTRIBUTE_T36C1E6CD0C0EBAE99E7DAE0AD1F9630EDCE4FC0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackClipTypeAttribute
struct  TrackClipTypeAttribute_t36C1E6CD0C0EBAE99E7DAE0AD1F9630EDCE4FC0B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type UnityEngine.Timeline.TrackClipTypeAttribute::inspectedType
	Type_t * ___inspectedType_0;
	// System.Boolean UnityEngine.Timeline.TrackClipTypeAttribute::allowAutoCreate
	bool ___allowAutoCreate_1;

public:
	inline static int32_t get_offset_of_inspectedType_0() { return static_cast<int32_t>(offsetof(TrackClipTypeAttribute_t36C1E6CD0C0EBAE99E7DAE0AD1F9630EDCE4FC0B, ___inspectedType_0)); }
	inline Type_t * get_inspectedType_0() const { return ___inspectedType_0; }
	inline Type_t ** get_address_of_inspectedType_0() { return &___inspectedType_0; }
	inline void set_inspectedType_0(Type_t * value)
	{
		___inspectedType_0 = value;
		Il2CppCodeGenWriteBarrier((&___inspectedType_0), value);
	}

	inline static int32_t get_offset_of_allowAutoCreate_1() { return static_cast<int32_t>(offsetof(TrackClipTypeAttribute_t36C1E6CD0C0EBAE99E7DAE0AD1F9630EDCE4FC0B, ___allowAutoCreate_1)); }
	inline bool get_allowAutoCreate_1() const { return ___allowAutoCreate_1; }
	inline bool* get_address_of_allowAutoCreate_1() { return &___allowAutoCreate_1; }
	inline void set_allowAutoCreate_1(bool value)
	{
		___allowAutoCreate_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKCLIPTYPEATTRIBUTE_T36C1E6CD0C0EBAE99E7DAE0AD1F9630EDCE4FC0B_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef EADMINFUNCTYPE_TF7B1C80396FD2B29BD0659D720064CE0F64EC460_H
#define EADMINFUNCTYPE_TF7B1C80396FD2B29BD0659D720064CE0F64EC460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdministratorManager/eAdminFuncType
struct  eAdminFuncType_tF7B1C80396FD2B29BD0659D720064CE0F64EC460 
{
public:
	// System.Int32 AdministratorManager/eAdminFuncType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eAdminFuncType_tF7B1C80396FD2B29BD0659D720064CE0F64EC460, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EADMINFUNCTYPE_TF7B1C80396FD2B29BD0659D720064CE0F64EC460_H
#ifndef EUISNGADMINSTATUS_T3535F7389D1BB62376817770D1110E3D0011D2B1_H
#define EUISNGADMINSTATUS_T3535F7389D1BB62376817770D1110E3D0011D2B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdministratorManager/eUisngAdminStatus
struct  eUisngAdminStatus_t3535F7389D1BB62376817770D1110E3D0011D2B1 
{
public:
	// System.Int32 AdministratorManager/eUisngAdminStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eUisngAdminStatus_t3535F7389D1BB62376817770D1110E3D0011D2B1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EUISNGADMINSTATUS_T3535F7389D1BB62376817770D1110E3D0011D2B1_H
#ifndef EADSTYPE_T2B6FF0BDE868ADDF19A539CB13A12489863F8CFB_H
#define EADSTYPE_T2B6FF0BDE868ADDF19A539CB13A12489863F8CFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdsManager/eAdsType
struct  eAdsType_t2B6FF0BDE868ADDF19A539CB13A12489863F8CFB 
{
public:
	// System.Int32 AdsManager/eAdsType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eAdsType_t2B6FF0BDE868ADDF19A539CB13A12489863F8CFB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EADSTYPE_T2B6FF0BDE868ADDF19A539CB13A12489863F8CFB_H
#ifndef EADVENTUREQUALITY_TEEC00413F249D6C1C4D8619161955635920131D3_H
#define EADVENTUREQUALITY_TEEC00413F249D6C1C4D8619161955635920131D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdventureManager_New/eAdventureQuality
struct  eAdventureQuality_tEEC00413F249D6C1C4D8619161955635920131D3 
{
public:
	// System.Int32 AdventureManager_New/eAdventureQuality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eAdventureQuality_tEEC00413F249D6C1C4D8619161955635920131D3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EADVENTUREQUALITY_TEEC00413F249D6C1C4D8619161955635920131D3_H
#ifndef HAPTICTYPES_T5841156CF72EED0C5C47C9245C09D2250C9CAAFD_H
#define HAPTICTYPES_T5841156CF72EED0C5C47C9245C09D2250C9CAAFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.NiceVibrations.HapticTypes
struct  HapticTypes_t5841156CF72EED0C5C47C9245C09D2250C9CAAFD 
{
public:
	// System.Int32 MoreMountains.NiceVibrations.HapticTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HapticTypes_t5841156CF72EED0C5C47C9245C09D2250C9CAAFD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HAPTICTYPES_T5841156CF72EED0C5C47C9245C09D2250C9CAAFD_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef PLAYABLEHANDLE_T9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_H
#define PLAYABLEHANDLE_T9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableHandle::m_Version
	uint32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182, ___m_Version_1)); }
	inline uint32_t get_m_Version_1() const { return ___m_Version_1; }
	inline uint32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(uint32_t value)
	{
		___m_Version_1 = value;
	}
};

struct PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_StaticFields
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::m_Null
	PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  ___m_Null_2;

public:
	inline static int32_t get_offset_of_m_Null_2() { return static_cast<int32_t>(offsetof(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_StaticFields, ___m_Null_2)); }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  get_m_Null_2() const { return ___m_Null_2; }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182 * get_address_of_m_Null_2() { return &___m_Null_2; }
	inline void set_m_Null_2(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  value)
	{
		___m_Null_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T9D3B4E540D4413CED81DDD6A24C5373BEFA1D182_H
#ifndef PLAYABLEOUTPUTHANDLE_T0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922_H
#define PLAYABLEOUTPUTHANDLE_T0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	uint32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922, ___m_Version_1)); }
	inline uint32_t get_m_Version_1() const { return ___m_Version_1; }
	inline uint32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(uint32_t value)
	{
		___m_Version_1 = value;
	}
};

struct PlayableOutputHandle_t0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922_StaticFields
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::m_Null
	PlayableOutputHandle_t0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922  ___m_Null_2;

public:
	inline static int32_t get_offset_of_m_Null_2() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922_StaticFields, ___m_Null_2)); }
	inline PlayableOutputHandle_t0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922  get_m_Null_2() const { return ___m_Null_2; }
	inline PlayableOutputHandle_t0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922 * get_address_of_m_Null_2() { return &___m_Null_2; }
	inline void set_m_Null_2(PlayableOutputHandle_t0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922  value)
	{
		___m_Null_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922_H
#ifndef POSTPLAYBACKSTATE_T2A1219857A4AF2EA533FD59DFEE53B2DB462A05A_H
#define POSTPLAYBACKSTATE_T2A1219857A4AF2EA533FD59DFEE53B2DB462A05A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState
struct  PostPlaybackState_t2A1219857A4AF2EA533FD59DFEE53B2DB462A05A 
{
public:
	// System.Int32 UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PostPlaybackState_t2A1219857A4AF2EA533FD59DFEE53B2DB462A05A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPLAYBACKSTATE_T2A1219857A4AF2EA533FD59DFEE53B2DB462A05A_H
#ifndef POSTPLAYBACKSTATE_TD3667806DCEDE0B4EAEF0EEEFE6E240465AD3F3E_H
#define POSTPLAYBACKSTATE_TD3667806DCEDE0B4EAEF0EEEFE6E240465AD3F3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationTrack/PostPlaybackState
struct  PostPlaybackState_tD3667806DCEDE0B4EAEF0EEEFE6E240465AD3F3E 
{
public:
	// System.Int32 UnityEngine.Timeline.ActivationTrack/PostPlaybackState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PostPlaybackState_tD3667806DCEDE0B4EAEF0EEEFE6E240465AD3F3E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPLAYBACKSTATE_TD3667806DCEDE0B4EAEF0EEEFE6E240465AD3F3E_H
#ifndef APPLIEDOFFSETMODE_TE0C23C461E39490CB48E37927E04C9E0B2F97697_H
#define APPLIEDOFFSETMODE_TE0C23C461E39490CB48E37927E04C9E0B2F97697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AppliedOffsetMode
struct  AppliedOffsetMode_tE0C23C461E39490CB48E37927E04C9E0B2F97697 
{
public:
	// System.Int32 UnityEngine.Timeline.AppliedOffsetMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AppliedOffsetMode_tE0C23C461E39490CB48E37927E04C9E0B2F97697, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLIEDOFFSETMODE_TE0C23C461E39490CB48E37927E04C9E0B2F97697_H
#ifndef CLIPCAPS_TB3FEBFA5CA7980C1E2E5676F27C92E77A769E971_H
#define CLIPCAPS_TB3FEBFA5CA7980C1E2E5676F27C92E77A769E971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ClipCaps
struct  ClipCaps_tB3FEBFA5CA7980C1E2E5676F27C92E77A769E971 
{
public:
	// System.Int32 UnityEngine.Timeline.ClipCaps::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClipCaps_tB3FEBFA5CA7980C1E2E5676F27C92E77A769E971, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPCAPS_TB3FEBFA5CA7980C1E2E5676F27C92E77A769E971_H
#ifndef MATCHTARGETFIELDS_T196139A2E266EF6660F3905BB110D7D41830219F_H
#define MATCHTARGETFIELDS_T196139A2E266EF6660F3905BB110D7D41830219F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.MatchTargetFields
struct  MatchTargetFields_t196139A2E266EF6660F3905BB110D7D41830219F 
{
public:
	// System.Int32 UnityEngine.Timeline.MatchTargetFields::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MatchTargetFields_t196139A2E266EF6660F3905BB110D7D41830219F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHTARGETFIELDS_T196139A2E266EF6660F3905BB110D7D41830219F_H
#ifndef DURATIONMODE_T8AA15A59EC315FA48C475D926B78FB950BD59F92_H
#define DURATIONMODE_T8AA15A59EC315FA48C475D926B78FB950BD59F92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/DurationMode
struct  DurationMode_t8AA15A59EC315FA48C475D926B78FB950BD59F92 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineAsset/DurationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DurationMode_t8AA15A59EC315FA48C475D926B78FB950BD59F92, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DURATIONMODE_T8AA15A59EC315FA48C475D926B78FB950BD59F92_H
#ifndef BLENDCURVEMODE_TFB15710CA3A221CD7B8B4FBE07DF8149E35D2F68_H
#define BLENDCURVEMODE_TFB15710CA3A221CD7B8B4FBE07DF8149E35D2F68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClip/BlendCurveMode
struct  BlendCurveMode_tFB15710CA3A221CD7B8B4FBE07DF8149E35D2F68 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineClip/BlendCurveMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlendCurveMode_tFB15710CA3A221CD7B8B4FBE07DF8149E35D2F68, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDCURVEMODE_TFB15710CA3A221CD7B8B4FBE07DF8149E35D2F68_H
#ifndef CLIPEXTRAPOLATION_TB3EB49D54D1E1E18AE03A394041AE9EC5E325750_H
#define CLIPEXTRAPOLATION_TB3EB49D54D1E1E18AE03A394041AE9EC5E325750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClip/ClipExtrapolation
struct  ClipExtrapolation_tB3EB49D54D1E1E18AE03A394041AE9EC5E325750 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineClip/ClipExtrapolation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClipExtrapolation_tB3EB49D54D1E1E18AE03A394041AE9EC5E325750, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPEXTRAPOLATION_TB3EB49D54D1E1E18AE03A394041AE9EC5E325750_H
#ifndef TRACKBINDINGFLAGS_TDF44A084A5ED972CC8A2D116695916EB95EE7172_H
#define TRACKBINDINGFLAGS_TDF44A084A5ED972CC8A2D116695916EB95EE7172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackBindingFlags
struct  TrackBindingFlags_tDF44A084A5ED972CC8A2D116695916EB95EE7172 
{
public:
	// System.Int32 UnityEngine.Timeline.TrackBindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackBindingFlags_tDF44A084A5ED972CC8A2D116695916EB95EE7172, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBINDINGFLAGS_TDF44A084A5ED972CC8A2D116695916EB95EE7172_H
#ifndef TRACKCOLORATTRIBUTE_TF37C45505BE134E4203DB4273F369282AFD2E9EA_H
#define TRACKCOLORATTRIBUTE_TF37C45505BE134E4203DB4273F369282AFD2E9EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackColorAttribute
struct  TrackColorAttribute_tF37C45505BE134E4203DB4273F369282AFD2E9EA  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// UnityEngine.Color UnityEngine.Timeline.TrackColorAttribute::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_0;

public:
	inline static int32_t get_offset_of_m_Color_0() { return static_cast<int32_t>(offsetof(TrackColorAttribute_tF37C45505BE134E4203DB4273F369282AFD2E9EA, ___m_Color_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_0() const { return ___m_Color_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_0() { return &___m_Color_0; }
	inline void set_m_Color_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKCOLORATTRIBUTE_TF37C45505BE134E4203DB4273F369282AFD2E9EA_H
#ifndef TRACKOFFSET_TE00CFCF66C03B86C63ACE5FADC64AB25D68A80CC_H
#define TRACKOFFSET_TE00CFCF66C03B86C63ACE5FADC64AB25D68A80CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackOffset
struct  TrackOffset_tE00CFCF66C03B86C63ACE5FADC64AB25D68A80CC 
{
public:
	// System.Int32 UnityEngine.Timeline.TrackOffset::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackOffset_tE00CFCF66C03B86C63ACE5FADC64AB25D68A80CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKOFFSET_TE00CFCF66C03B86C63ACE5FADC64AB25D68A80CC_H
#ifndef SADMINCONFIG_TCD81360114F98AE183656DD00005E18A400A4827_H
#define SADMINCONFIG_TCD81360114F98AE183656DD00005E18A400A4827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdministratorManager/sAdminConfig
struct  sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827 
{
public:
	// System.Int32 AdministratorManager/sAdminConfig::nId
	int32_t ___nId_0;
	// System.String AdministratorManager/sAdminConfig::szName
	String_t* ___szName_1;
	// System.String AdministratorManager/sAdminConfig::szQuality
	String_t* ___szQuality_2;
	// System.Int32 AdministratorManager/sAdminConfig::nQuality
	int32_t ___nQuality_3;
	// System.String AdministratorManager/sAdminConfig::szDesc
	String_t* ___szDesc_4;
	// AdministratorManager/eAdminFuncType AdministratorManager/sAdminConfig::eType
	int32_t ___eType_5;
	// System.Single AdministratorManager/sAdminConfig::fValue
	float ___fValue_6;
	// System.Int32 AdministratorManager/sAdminConfig::nDuration
	int32_t ___nDuration_7;
	// System.Int32 AdministratorManager/sAdminConfig::nColddown
	int32_t ___nColddown_8;
	// System.Int32 AdministratorManager/sAdminConfig::nProbabilityWeight
	int32_t ___nProbabilityWeight_9;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_szName_1() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___szName_1)); }
	inline String_t* get_szName_1() const { return ___szName_1; }
	inline String_t** get_address_of_szName_1() { return &___szName_1; }
	inline void set_szName_1(String_t* value)
	{
		___szName_1 = value;
		Il2CppCodeGenWriteBarrier((&___szName_1), value);
	}

	inline static int32_t get_offset_of_szQuality_2() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___szQuality_2)); }
	inline String_t* get_szQuality_2() const { return ___szQuality_2; }
	inline String_t** get_address_of_szQuality_2() { return &___szQuality_2; }
	inline void set_szQuality_2(String_t* value)
	{
		___szQuality_2 = value;
		Il2CppCodeGenWriteBarrier((&___szQuality_2), value);
	}

	inline static int32_t get_offset_of_nQuality_3() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___nQuality_3)); }
	inline int32_t get_nQuality_3() const { return ___nQuality_3; }
	inline int32_t* get_address_of_nQuality_3() { return &___nQuality_3; }
	inline void set_nQuality_3(int32_t value)
	{
		___nQuality_3 = value;
	}

	inline static int32_t get_offset_of_szDesc_4() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___szDesc_4)); }
	inline String_t* get_szDesc_4() const { return ___szDesc_4; }
	inline String_t** get_address_of_szDesc_4() { return &___szDesc_4; }
	inline void set_szDesc_4(String_t* value)
	{
		___szDesc_4 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_4), value);
	}

	inline static int32_t get_offset_of_eType_5() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___eType_5)); }
	inline int32_t get_eType_5() const { return ___eType_5; }
	inline int32_t* get_address_of_eType_5() { return &___eType_5; }
	inline void set_eType_5(int32_t value)
	{
		___eType_5 = value;
	}

	inline static int32_t get_offset_of_fValue_6() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___fValue_6)); }
	inline float get_fValue_6() const { return ___fValue_6; }
	inline float* get_address_of_fValue_6() { return &___fValue_6; }
	inline void set_fValue_6(float value)
	{
		___fValue_6 = value;
	}

	inline static int32_t get_offset_of_nDuration_7() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___nDuration_7)); }
	inline int32_t get_nDuration_7() const { return ___nDuration_7; }
	inline int32_t* get_address_of_nDuration_7() { return &___nDuration_7; }
	inline void set_nDuration_7(int32_t value)
	{
		___nDuration_7 = value;
	}

	inline static int32_t get_offset_of_nColddown_8() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___nColddown_8)); }
	inline int32_t get_nColddown_8() const { return ___nColddown_8; }
	inline int32_t* get_address_of_nColddown_8() { return &___nColddown_8; }
	inline void set_nColddown_8(int32_t value)
	{
		___nColddown_8 = value;
	}

	inline static int32_t get_offset_of_nProbabilityWeight_9() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___nProbabilityWeight_9)); }
	inline int32_t get_nProbabilityWeight_9() const { return ___nProbabilityWeight_9; }
	inline int32_t* get_address_of_nProbabilityWeight_9() { return &___nProbabilityWeight_9; }
	inline void set_nProbabilityWeight_9(int32_t value)
	{
		___nProbabilityWeight_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of AdministratorManager/sAdminConfig
struct sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827_marshaled_pinvoke
{
	int32_t ___nId_0;
	char* ___szName_1;
	char* ___szQuality_2;
	int32_t ___nQuality_3;
	char* ___szDesc_4;
	int32_t ___eType_5;
	float ___fValue_6;
	int32_t ___nDuration_7;
	int32_t ___nColddown_8;
	int32_t ___nProbabilityWeight_9;
};
// Native definition for COM marshalling of AdministratorManager/sAdminConfig
struct sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827_marshaled_com
{
	int32_t ___nId_0;
	Il2CppChar* ___szName_1;
	Il2CppChar* ___szQuality_2;
	int32_t ___nQuality_3;
	Il2CppChar* ___szDesc_4;
	int32_t ___eType_5;
	float ___fValue_6;
	int32_t ___nDuration_7;
	int32_t ___nColddown_8;
	int32_t ___nProbabilityWeight_9;
};
#endif // SADMINCONFIG_TCD81360114F98AE183656DD00005E18A400A4827_H
#ifndef SADVENTURECONFIG_TF39377E053A6EF748EF650DAA67DAFE090903FB1_H
#define SADVENTURECONFIG_TF39377E053A6EF748EF650DAA67DAFE090903FB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdventureManager_New/sAdventureConfig
struct  sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1 
{
public:
	// AdventureManager_New/eAdventureQuality AdventureManager_New/sAdventureConfig::eQuality
	int32_t ___eQuality_0;
	// System.String AdventureManager_New/sAdventureConfig::szDesc
	String_t* ___szDesc_1;
	// System.Single AdventureManager_New/sAdventureConfig::fDuration
	float ___fDuration_2;
	// System.Int32 AdventureManager_New/sAdventureConfig::nProbability
	int32_t ___nProbability_3;
	// System.Collections.Generic.List`1<AdventureManager_New/sRewardConfig> AdventureManager_New/sAdventureConfig::lstMainRewardPool
	List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * ___lstMainRewardPool_4;
	// System.Collections.Generic.List`1<AdventureManager_New/sRewardConfig> AdventureManager_New/sAdventureConfig::lstViceRewardPool
	List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * ___lstViceRewardPool_5;
	// System.Int32 AdventureManager_New/sAdventureConfig::nViceRewardNum
	int32_t ___nViceRewardNum_6;

public:
	inline static int32_t get_offset_of_eQuality_0() { return static_cast<int32_t>(offsetof(sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1, ___eQuality_0)); }
	inline int32_t get_eQuality_0() const { return ___eQuality_0; }
	inline int32_t* get_address_of_eQuality_0() { return &___eQuality_0; }
	inline void set_eQuality_0(int32_t value)
	{
		___eQuality_0 = value;
	}

	inline static int32_t get_offset_of_szDesc_1() { return static_cast<int32_t>(offsetof(sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1, ___szDesc_1)); }
	inline String_t* get_szDesc_1() const { return ___szDesc_1; }
	inline String_t** get_address_of_szDesc_1() { return &___szDesc_1; }
	inline void set_szDesc_1(String_t* value)
	{
		___szDesc_1 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_1), value);
	}

	inline static int32_t get_offset_of_fDuration_2() { return static_cast<int32_t>(offsetof(sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1, ___fDuration_2)); }
	inline float get_fDuration_2() const { return ___fDuration_2; }
	inline float* get_address_of_fDuration_2() { return &___fDuration_2; }
	inline void set_fDuration_2(float value)
	{
		___fDuration_2 = value;
	}

	inline static int32_t get_offset_of_nProbability_3() { return static_cast<int32_t>(offsetof(sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1, ___nProbability_3)); }
	inline int32_t get_nProbability_3() const { return ___nProbability_3; }
	inline int32_t* get_address_of_nProbability_3() { return &___nProbability_3; }
	inline void set_nProbability_3(int32_t value)
	{
		___nProbability_3 = value;
	}

	inline static int32_t get_offset_of_lstMainRewardPool_4() { return static_cast<int32_t>(offsetof(sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1, ___lstMainRewardPool_4)); }
	inline List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * get_lstMainRewardPool_4() const { return ___lstMainRewardPool_4; }
	inline List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F ** get_address_of_lstMainRewardPool_4() { return &___lstMainRewardPool_4; }
	inline void set_lstMainRewardPool_4(List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * value)
	{
		___lstMainRewardPool_4 = value;
		Il2CppCodeGenWriteBarrier((&___lstMainRewardPool_4), value);
	}

	inline static int32_t get_offset_of_lstViceRewardPool_5() { return static_cast<int32_t>(offsetof(sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1, ___lstViceRewardPool_5)); }
	inline List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * get_lstViceRewardPool_5() const { return ___lstViceRewardPool_5; }
	inline List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F ** get_address_of_lstViceRewardPool_5() { return &___lstViceRewardPool_5; }
	inline void set_lstViceRewardPool_5(List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * value)
	{
		___lstViceRewardPool_5 = value;
		Il2CppCodeGenWriteBarrier((&___lstViceRewardPool_5), value);
	}

	inline static int32_t get_offset_of_nViceRewardNum_6() { return static_cast<int32_t>(offsetof(sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1, ___nViceRewardNum_6)); }
	inline int32_t get_nViceRewardNum_6() const { return ___nViceRewardNum_6; }
	inline int32_t* get_address_of_nViceRewardNum_6() { return &___nViceRewardNum_6; }
	inline void set_nViceRewardNum_6(int32_t value)
	{
		___nViceRewardNum_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of AdventureManager_New/sAdventureConfig
struct sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1_marshaled_pinvoke
{
	int32_t ___eQuality_0;
	char* ___szDesc_1;
	float ___fDuration_2;
	int32_t ___nProbability_3;
	List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * ___lstMainRewardPool_4;
	List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * ___lstViceRewardPool_5;
	int32_t ___nViceRewardNum_6;
};
// Native definition for COM marshalling of AdventureManager_New/sAdventureConfig
struct sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1_marshaled_com
{
	int32_t ___eQuality_0;
	Il2CppChar* ___szDesc_1;
	float ___fDuration_2;
	int32_t ___nProbability_3;
	List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * ___lstMainRewardPool_4;
	List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * ___lstViceRewardPool_5;
	int32_t ___nViceRewardNum_6;
};
#endif // SADVENTURECONFIG_TF39377E053A6EF748EF650DAA67DAFE090903FB1_H
#ifndef ANIMATIONLAYERMIXERPLAYABLE_T699CCDE32ABD6FC79BFC09064E473D785D9F9371_H
#define ANIMATIONLAYERMIXERPLAYABLE_T699CCDE32ABD6FC79BFC09064E473D785D9F9371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationLayerMixerPlayable
struct  AnimationLayerMixerPlayable_t699CCDE32ABD6FC79BFC09064E473D785D9F9371 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::m_Handle
	PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_t699CCDE32ABD6FC79BFC09064E473D785D9F9371, ___m_Handle_0)); }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimationLayerMixerPlayable_t699CCDE32ABD6FC79BFC09064E473D785D9F9371_StaticFields
{
public:
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::m_NullPlayable
	AnimationLayerMixerPlayable_t699CCDE32ABD6FC79BFC09064E473D785D9F9371  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_t699CCDE32ABD6FC79BFC09064E473D785D9F9371_StaticFields, ___m_NullPlayable_1)); }
	inline AnimationLayerMixerPlayable_t699CCDE32ABD6FC79BFC09064E473D785D9F9371  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimationLayerMixerPlayable_t699CCDE32ABD6FC79BFC09064E473D785D9F9371 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimationLayerMixerPlayable_t699CCDE32ABD6FC79BFC09064E473D785D9F9371  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONLAYERMIXERPLAYABLE_T699CCDE32ABD6FC79BFC09064E473D785D9F9371_H
#ifndef ANIMATIONMIXERPLAYABLE_TA71C834654979CF92B034B537EE5A3DA9713030A_H
#define ANIMATIONMIXERPLAYABLE_TA71C834654979CF92B034B537EE5A3DA9713030A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationMixerPlayable
struct  AnimationMixerPlayable_tA71C834654979CF92B034B537EE5A3DA9713030A 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::m_Handle
	PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationMixerPlayable_tA71C834654979CF92B034B537EE5A3DA9713030A, ___m_Handle_0)); }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimationMixerPlayable_tA71C834654979CF92B034B537EE5A3DA9713030A_StaticFields
{
public:
	// UnityEngine.Animations.AnimationMixerPlayable UnityEngine.Animations.AnimationMixerPlayable::m_NullPlayable
	AnimationMixerPlayable_tA71C834654979CF92B034B537EE5A3DA9713030A  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimationMixerPlayable_tA71C834654979CF92B034B537EE5A3DA9713030A_StaticFields, ___m_NullPlayable_1)); }
	inline AnimationMixerPlayable_tA71C834654979CF92B034B537EE5A3DA9713030A  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimationMixerPlayable_tA71C834654979CF92B034B537EE5A3DA9713030A * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimationMixerPlayable_tA71C834654979CF92B034B537EE5A3DA9713030A  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONMIXERPLAYABLE_TA71C834654979CF92B034B537EE5A3DA9713030A_H
#ifndef ANIMATIONMOTIONXTODELTAPLAYABLE_TA5F0BE3BA966E1A6661311F185C1544F90302CDC_H
#define ANIMATIONMOTIONXTODELTAPLAYABLE_TA5F0BE3BA966E1A6661311F185C1544F90302CDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationMotionXToDeltaPlayable
struct  AnimationMotionXToDeltaPlayable_tA5F0BE3BA966E1A6661311F185C1544F90302CDC 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMotionXToDeltaPlayable::m_Handle
	PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationMotionXToDeltaPlayable_tA5F0BE3BA966E1A6661311F185C1544F90302CDC, ___m_Handle_0)); }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimationMotionXToDeltaPlayable_tA5F0BE3BA966E1A6661311F185C1544F90302CDC_StaticFields
{
public:
	// UnityEngine.Animations.AnimationMotionXToDeltaPlayable UnityEngine.Animations.AnimationMotionXToDeltaPlayable::m_NullPlayable
	AnimationMotionXToDeltaPlayable_tA5F0BE3BA966E1A6661311F185C1544F90302CDC  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimationMotionXToDeltaPlayable_tA5F0BE3BA966E1A6661311F185C1544F90302CDC_StaticFields, ___m_NullPlayable_1)); }
	inline AnimationMotionXToDeltaPlayable_tA5F0BE3BA966E1A6661311F185C1544F90302CDC  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimationMotionXToDeltaPlayable_tA5F0BE3BA966E1A6661311F185C1544F90302CDC * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimationMotionXToDeltaPlayable_tA5F0BE3BA966E1A6661311F185C1544F90302CDC  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONMOTIONXTODELTAPLAYABLE_TA5F0BE3BA966E1A6661311F185C1544F90302CDC_H
#ifndef ANIMATIONPLAYABLEOUTPUT_TA10178429D6528BDB4516F6788CE680E349553E6_H
#define ANIMATIONPLAYABLEOUTPUT_TA10178429D6528BDB4516F6788CE680E349553E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationPlayableOutput
struct  AnimationPlayableOutput_tA10178429D6528BDB4516F6788CE680E349553E6 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::m_Handle
	PlayableOutputHandle_t0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationPlayableOutput_tA10178429D6528BDB4516F6788CE680E349553E6, ___m_Handle_0)); }
	inline PlayableOutputHandle_t0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t0D0C9D8ACC1A4061BD4EAEB61F3EE0357052F922  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEOUTPUT_TA10178429D6528BDB4516F6788CE680E349553E6_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef EXPOSEDREFERENCE_1_TFE0DB3E784D85584A88D53B1118D208ECA2B7059_H
#define EXPOSEDREFERENCE_1_TFE0DB3E784D85584A88D53B1118D208ECA2B7059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ExposedReference`1<UnityEngine.GameObject>
struct  ExposedReference_1_tFE0DB3E784D85584A88D53B1118D208ECA2B7059 
{
public:
	// UnityEngine.PropertyName UnityEngine.ExposedReference`1::exposedName
	PropertyName_t75EB843FEA2EC372093479A35C24364D2DF98529  ___exposedName_0;
	// UnityEngine.Object UnityEngine.ExposedReference`1::defaultValue
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___defaultValue_1;

public:
	inline static int32_t get_offset_of_exposedName_0() { return static_cast<int32_t>(offsetof(ExposedReference_1_tFE0DB3E784D85584A88D53B1118D208ECA2B7059, ___exposedName_0)); }
	inline PropertyName_t75EB843FEA2EC372093479A35C24364D2DF98529  get_exposedName_0() const { return ___exposedName_0; }
	inline PropertyName_t75EB843FEA2EC372093479A35C24364D2DF98529 * get_address_of_exposedName_0() { return &___exposedName_0; }
	inline void set_exposedName_0(PropertyName_t75EB843FEA2EC372093479A35C24364D2DF98529  value)
	{
		___exposedName_0 = value;
	}

	inline static int32_t get_offset_of_defaultValue_1() { return static_cast<int32_t>(offsetof(ExposedReference_1_tFE0DB3E784D85584A88D53B1118D208ECA2B7059, ___defaultValue_1)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_defaultValue_1() const { return ___defaultValue_1; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_defaultValue_1() { return &___defaultValue_1; }
	inline void set_defaultValue_1(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___defaultValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ExposedReference`1
#ifndef ExposedReference_1_tDE76909D901EFA3B702C9472E5616C20297CEBE1_marshaled_pinvoke_define
#define ExposedReference_1_tDE76909D901EFA3B702C9472E5616C20297CEBE1_marshaled_pinvoke_define
struct ExposedReference_1_tDE76909D901EFA3B702C9472E5616C20297CEBE1_marshaled_pinvoke
{
	PropertyName_t75EB843FEA2EC372093479A35C24364D2DF98529  ___exposedName_0;
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke ___defaultValue_1;
};
#endif
// Native definition for COM marshalling of UnityEngine.ExposedReference`1
#ifndef ExposedReference_1_tDE76909D901EFA3B702C9472E5616C20297CEBE1_marshaled_com_define
#define ExposedReference_1_tDE76909D901EFA3B702C9472E5616C20297CEBE1_marshaled_com_define
struct ExposedReference_1_tDE76909D901EFA3B702C9472E5616C20297CEBE1_marshaled_com
{
	PropertyName_t75EB843FEA2EC372093479A35C24364D2DF98529  ___exposedName_0;
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com* ___defaultValue_1;
};
#endif
#endif // EXPOSEDREFERENCE_1_TFE0DB3E784D85584A88D53B1118D208ECA2B7059_H
#ifndef PLAYABLE_T4ABB910C374FCAB6B926DA4D34A85857A59950D0_H
#define PLAYABLE_T4ABB910C374FCAB6B926DA4D34A85857A59950D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0, ___m_Handle_0)); }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t9D3B4E540D4413CED81DDD6A24C5373BEFA1D182  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T4ABB910C374FCAB6B926DA4D34A85857A59950D0_H
#ifndef PLAYABLEBINDING_T4D92F4CF16B8608DD83947E5D40CB7690F23F9C8_H
#define PLAYABLEBINDING_T4D92F4CF16B8608DD83947E5D40CB7690F23F9C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBinding
struct  PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8 
{
public:
	// System.String UnityEngine.Playables.PlayableBinding::m_StreamName
	String_t* ___m_StreamName_0;
	// UnityEngine.Object UnityEngine.Playables.PlayableBinding::m_SourceObject
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___m_SourceObject_1;
	// System.Type UnityEngine.Playables.PlayableBinding::m_SourceBindingType
	Type_t * ___m_SourceBindingType_2;
	// UnityEngine.Playables.PlayableBinding/CreateOutputMethod UnityEngine.Playables.PlayableBinding::m_CreateOutputMethod
	CreateOutputMethod_tA7B649F49822FC5DD0B0D9F17247C73CAECB1CA3 * ___m_CreateOutputMethod_3;

public:
	inline static int32_t get_offset_of_m_StreamName_0() { return static_cast<int32_t>(offsetof(PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8, ___m_StreamName_0)); }
	inline String_t* get_m_StreamName_0() const { return ___m_StreamName_0; }
	inline String_t** get_address_of_m_StreamName_0() { return &___m_StreamName_0; }
	inline void set_m_StreamName_0(String_t* value)
	{
		___m_StreamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_StreamName_0), value);
	}

	inline static int32_t get_offset_of_m_SourceObject_1() { return static_cast<int32_t>(offsetof(PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8, ___m_SourceObject_1)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_m_SourceObject_1() const { return ___m_SourceObject_1; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_m_SourceObject_1() { return &___m_SourceObject_1; }
	inline void set_m_SourceObject_1(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___m_SourceObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceObject_1), value);
	}

	inline static int32_t get_offset_of_m_SourceBindingType_2() { return static_cast<int32_t>(offsetof(PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8, ___m_SourceBindingType_2)); }
	inline Type_t * get_m_SourceBindingType_2() const { return ___m_SourceBindingType_2; }
	inline Type_t ** get_address_of_m_SourceBindingType_2() { return &___m_SourceBindingType_2; }
	inline void set_m_SourceBindingType_2(Type_t * value)
	{
		___m_SourceBindingType_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceBindingType_2), value);
	}

	inline static int32_t get_offset_of_m_CreateOutputMethod_3() { return static_cast<int32_t>(offsetof(PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8, ___m_CreateOutputMethod_3)); }
	inline CreateOutputMethod_tA7B649F49822FC5DD0B0D9F17247C73CAECB1CA3 * get_m_CreateOutputMethod_3() const { return ___m_CreateOutputMethod_3; }
	inline CreateOutputMethod_tA7B649F49822FC5DD0B0D9F17247C73CAECB1CA3 ** get_address_of_m_CreateOutputMethod_3() { return &___m_CreateOutputMethod_3; }
	inline void set_m_CreateOutputMethod_3(CreateOutputMethod_tA7B649F49822FC5DD0B0D9F17247C73CAECB1CA3 * value)
	{
		___m_CreateOutputMethod_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreateOutputMethod_3), value);
	}
};

struct PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8_StaticFields
{
public:
	// UnityEngine.Playables.PlayableBinding[] UnityEngine.Playables.PlayableBinding::None
	PlayableBindingU5BU5D_t7EB322901D51EAB67BA4F711C87F3AC1CF5D89AB* ___None_4;
	// System.Double UnityEngine.Playables.PlayableBinding::DefaultDuration
	double ___DefaultDuration_5;

public:
	inline static int32_t get_offset_of_None_4() { return static_cast<int32_t>(offsetof(PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8_StaticFields, ___None_4)); }
	inline PlayableBindingU5BU5D_t7EB322901D51EAB67BA4F711C87F3AC1CF5D89AB* get_None_4() const { return ___None_4; }
	inline PlayableBindingU5BU5D_t7EB322901D51EAB67BA4F711C87F3AC1CF5D89AB** get_address_of_None_4() { return &___None_4; }
	inline void set_None_4(PlayableBindingU5BU5D_t7EB322901D51EAB67BA4F711C87F3AC1CF5D89AB* value)
	{
		___None_4 = value;
		Il2CppCodeGenWriteBarrier((&___None_4), value);
	}

	inline static int32_t get_offset_of_DefaultDuration_5() { return static_cast<int32_t>(offsetof(PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8_StaticFields, ___DefaultDuration_5)); }
	inline double get_DefaultDuration_5() const { return ___DefaultDuration_5; }
	inline double* get_address_of_DefaultDuration_5() { return &___DefaultDuration_5; }
	inline void set_DefaultDuration_5(double value)
	{
		___DefaultDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8_marshaled_pinvoke
{
	char* ___m_StreamName_0;
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke ___m_SourceObject_1;
	Type_t * ___m_SourceBindingType_2;
	Il2CppMethodPointer ___m_CreateOutputMethod_3;
};
// Native definition for COM marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8_marshaled_com
{
	Il2CppChar* ___m_StreamName_0;
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com* ___m_SourceObject_1;
	Type_t * ___m_SourceBindingType_2;
	Il2CppMethodPointer ___m_CreateOutputMethod_3;
};
#endif // PLAYABLEBINDING_T4D92F4CF16B8608DD83947E5D40CB7690F23F9C8_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef ACTIVATIONCONTROLPLAYABLE_TCA39D16B1A31D59AAD5AE50F950D328A1091BEA1_H
#define ACTIVATIONCONTROLPLAYABLE_TCA39D16B1A31D59AAD5AE50F950D328A1091BEA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationControlPlayable
struct  ActivationControlPlayable_tCA39D16B1A31D59AAD5AE50F950D328A1091BEA1  : public PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01
{
public:
	// UnityEngine.GameObject UnityEngine.Timeline.ActivationControlPlayable::gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject_0;
	// UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState UnityEngine.Timeline.ActivationControlPlayable::postPlayback
	int32_t ___postPlayback_1;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(ActivationControlPlayable_tCA39D16B1A31D59AAD5AE50F950D328A1091BEA1, ___gameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_0), value);
	}

	inline static int32_t get_offset_of_postPlayback_1() { return static_cast<int32_t>(offsetof(ActivationControlPlayable_tCA39D16B1A31D59AAD5AE50F950D328A1091BEA1, ___postPlayback_1)); }
	inline int32_t get_postPlayback_1() const { return ___postPlayback_1; }
	inline int32_t* get_address_of_postPlayback_1() { return &___postPlayback_1; }
	inline void set_postPlayback_1(int32_t value)
	{
		___postPlayback_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONCONTROLPLAYABLE_TCA39D16B1A31D59AAD5AE50F950D328A1091BEA1_H
#ifndef ACTIVATIONMIXERPLAYABLE_T299EC5947DB712650FF6D52B34BE46862B4034E2_H
#define ACTIVATIONMIXERPLAYABLE_T299EC5947DB712650FF6D52B34BE46862B4034E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationMixerPlayable
struct  ActivationMixerPlayable_t299EC5947DB712650FF6D52B34BE46862B4034E2  : public PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01
{
public:
	// UnityEngine.Timeline.ActivationTrack/PostPlaybackState UnityEngine.Timeline.ActivationMixerPlayable::m_PostPlaybackState
	int32_t ___m_PostPlaybackState_0;
	// System.Boolean UnityEngine.Timeline.ActivationMixerPlayable::m_BoundGameObjectInitialStateIsActive
	bool ___m_BoundGameObjectInitialStateIsActive_1;
	// UnityEngine.GameObject UnityEngine.Timeline.ActivationMixerPlayable::m_BoundGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_BoundGameObject_2;

public:
	inline static int32_t get_offset_of_m_PostPlaybackState_0() { return static_cast<int32_t>(offsetof(ActivationMixerPlayable_t299EC5947DB712650FF6D52B34BE46862B4034E2, ___m_PostPlaybackState_0)); }
	inline int32_t get_m_PostPlaybackState_0() const { return ___m_PostPlaybackState_0; }
	inline int32_t* get_address_of_m_PostPlaybackState_0() { return &___m_PostPlaybackState_0; }
	inline void set_m_PostPlaybackState_0(int32_t value)
	{
		___m_PostPlaybackState_0 = value;
	}

	inline static int32_t get_offset_of_m_BoundGameObjectInitialStateIsActive_1() { return static_cast<int32_t>(offsetof(ActivationMixerPlayable_t299EC5947DB712650FF6D52B34BE46862B4034E2, ___m_BoundGameObjectInitialStateIsActive_1)); }
	inline bool get_m_BoundGameObjectInitialStateIsActive_1() const { return ___m_BoundGameObjectInitialStateIsActive_1; }
	inline bool* get_address_of_m_BoundGameObjectInitialStateIsActive_1() { return &___m_BoundGameObjectInitialStateIsActive_1; }
	inline void set_m_BoundGameObjectInitialStateIsActive_1(bool value)
	{
		___m_BoundGameObjectInitialStateIsActive_1 = value;
	}

	inline static int32_t get_offset_of_m_BoundGameObject_2() { return static_cast<int32_t>(offsetof(ActivationMixerPlayable_t299EC5947DB712650FF6D52B34BE46862B4034E2, ___m_BoundGameObject_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_BoundGameObject_2() const { return ___m_BoundGameObject_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_BoundGameObject_2() { return &___m_BoundGameObject_2; }
	inline void set_m_BoundGameObject_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_BoundGameObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundGameObject_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONMIXERPLAYABLE_T299EC5947DB712650FF6D52B34BE46862B4034E2_H
#ifndef MATCHTARGETFIELDCONSTANTS_TC9BE7A88A2EF5CFAF9B99F3292489096805555C5_H
#define MATCHTARGETFIELDCONSTANTS_TC9BE7A88A2EF5CFAF9B99F3292489096805555C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.MatchTargetFieldConstants
struct  MatchTargetFieldConstants_tC9BE7A88A2EF5CFAF9B99F3292489096805555C5  : public RuntimeObject
{
public:

public:
};

struct MatchTargetFieldConstants_tC9BE7A88A2EF5CFAF9B99F3292489096805555C5_StaticFields
{
public:
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::All
	int32_t ___All_0;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::None
	int32_t ___None_1;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::Position
	int32_t ___Position_2;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::Rotation
	int32_t ___Rotation_3;

public:
	inline static int32_t get_offset_of_All_0() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_tC9BE7A88A2EF5CFAF9B99F3292489096805555C5_StaticFields, ___All_0)); }
	inline int32_t get_All_0() const { return ___All_0; }
	inline int32_t* get_address_of_All_0() { return &___All_0; }
	inline void set_All_0(int32_t value)
	{
		___All_0 = value;
	}

	inline static int32_t get_offset_of_None_1() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_tC9BE7A88A2EF5CFAF9B99F3292489096805555C5_StaticFields, ___None_1)); }
	inline int32_t get_None_1() const { return ___None_1; }
	inline int32_t* get_address_of_None_1() { return &___None_1; }
	inline void set_None_1(int32_t value)
	{
		___None_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_tC9BE7A88A2EF5CFAF9B99F3292489096805555C5_StaticFields, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_Rotation_3() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_tC9BE7A88A2EF5CFAF9B99F3292489096805555C5_StaticFields, ___Rotation_3)); }
	inline int32_t get_Rotation_3() const { return ___Rotation_3; }
	inline int32_t* get_address_of_Rotation_3() { return &___Rotation_3; }
	inline void set_Rotation_3(int32_t value)
	{
		___Rotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHTARGETFIELDCONSTANTS_TC9BE7A88A2EF5CFAF9B99F3292489096805555C5_H
#ifndef TIMELINECLIP_T45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_H
#define TIMELINECLIP_T45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClip
struct  TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A  : public RuntimeObject
{
public:
	// System.Double UnityEngine.Timeline.TimelineClip::m_Start
	double ___m_Start_6;
	// System.Double UnityEngine.Timeline.TimelineClip::m_ClipIn
	double ___m_ClipIn_7;
	// UnityEngine.Object UnityEngine.Timeline.TimelineClip::m_Asset
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___m_Asset_8;
	// System.Double UnityEngine.Timeline.TimelineClip::m_Duration
	double ___m_Duration_9;
	// System.Double UnityEngine.Timeline.TimelineClip::m_TimeScale
	double ___m_TimeScale_10;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TimelineClip::m_ParentTrack
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC * ___m_ParentTrack_11;
	// System.Double UnityEngine.Timeline.TimelineClip::m_EaseInDuration
	double ___m_EaseInDuration_12;
	// System.Double UnityEngine.Timeline.TimelineClip::m_EaseOutDuration
	double ___m_EaseOutDuration_13;
	// System.Double UnityEngine.Timeline.TimelineClip::m_BlendInDuration
	double ___m_BlendInDuration_14;
	// System.Double UnityEngine.Timeline.TimelineClip::m_BlendOutDuration
	double ___m_BlendOutDuration_15;
	// UnityEngine.AnimationCurve UnityEngine.Timeline.TimelineClip::m_MixInCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_MixInCurve_16;
	// UnityEngine.AnimationCurve UnityEngine.Timeline.TimelineClip::m_MixOutCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_MixOutCurve_17;
	// UnityEngine.Timeline.TimelineClip/BlendCurveMode UnityEngine.Timeline.TimelineClip::m_BlendInCurveMode
	int32_t ___m_BlendInCurveMode_18;
	// UnityEngine.Timeline.TimelineClip/BlendCurveMode UnityEngine.Timeline.TimelineClip::m_BlendOutCurveMode
	int32_t ___m_BlendOutCurveMode_19;
	// System.Collections.Generic.List`1<System.String> UnityEngine.Timeline.TimelineClip::m_ExposedParameterNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___m_ExposedParameterNames_20;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TimelineClip::m_AnimationCurves
	AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * ___m_AnimationCurves_21;
	// System.Boolean UnityEngine.Timeline.TimelineClip::m_Recordable
	bool ___m_Recordable_22;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.TimelineClip::m_PostExtrapolationMode
	int32_t ___m_PostExtrapolationMode_23;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.TimelineClip::m_PreExtrapolationMode
	int32_t ___m_PreExtrapolationMode_24;
	// System.Double UnityEngine.Timeline.TimelineClip::m_PostExtrapolationTime
	double ___m_PostExtrapolationTime_25;
	// System.Double UnityEngine.Timeline.TimelineClip::m_PreExtrapolationTime
	double ___m_PreExtrapolationTime_26;
	// System.String UnityEngine.Timeline.TimelineClip::m_DisplayName
	String_t* ___m_DisplayName_27;
	// System.Int32 UnityEngine.Timeline.TimelineClip::<dirtyHash>k__BackingField
	int32_t ___U3CdirtyHashU3Ek__BackingField_28;
	// System.Int32 UnityEngine.Timeline.TimelineClip::m_Version
	int32_t ___m_Version_30;

public:
	inline static int32_t get_offset_of_m_Start_6() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_Start_6)); }
	inline double get_m_Start_6() const { return ___m_Start_6; }
	inline double* get_address_of_m_Start_6() { return &___m_Start_6; }
	inline void set_m_Start_6(double value)
	{
		___m_Start_6 = value;
	}

	inline static int32_t get_offset_of_m_ClipIn_7() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_ClipIn_7)); }
	inline double get_m_ClipIn_7() const { return ___m_ClipIn_7; }
	inline double* get_address_of_m_ClipIn_7() { return &___m_ClipIn_7; }
	inline void set_m_ClipIn_7(double value)
	{
		___m_ClipIn_7 = value;
	}

	inline static int32_t get_offset_of_m_Asset_8() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_Asset_8)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_m_Asset_8() const { return ___m_Asset_8; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_m_Asset_8() { return &___m_Asset_8; }
	inline void set_m_Asset_8(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___m_Asset_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Asset_8), value);
	}

	inline static int32_t get_offset_of_m_Duration_9() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_Duration_9)); }
	inline double get_m_Duration_9() const { return ___m_Duration_9; }
	inline double* get_address_of_m_Duration_9() { return &___m_Duration_9; }
	inline void set_m_Duration_9(double value)
	{
		___m_Duration_9 = value;
	}

	inline static int32_t get_offset_of_m_TimeScale_10() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_TimeScale_10)); }
	inline double get_m_TimeScale_10() const { return ___m_TimeScale_10; }
	inline double* get_address_of_m_TimeScale_10() { return &___m_TimeScale_10; }
	inline void set_m_TimeScale_10(double value)
	{
		___m_TimeScale_10 = value;
	}

	inline static int32_t get_offset_of_m_ParentTrack_11() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_ParentTrack_11)); }
	inline TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC * get_m_ParentTrack_11() const { return ___m_ParentTrack_11; }
	inline TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC ** get_address_of_m_ParentTrack_11() { return &___m_ParentTrack_11; }
	inline void set_m_ParentTrack_11(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC * value)
	{
		___m_ParentTrack_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentTrack_11), value);
	}

	inline static int32_t get_offset_of_m_EaseInDuration_12() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_EaseInDuration_12)); }
	inline double get_m_EaseInDuration_12() const { return ___m_EaseInDuration_12; }
	inline double* get_address_of_m_EaseInDuration_12() { return &___m_EaseInDuration_12; }
	inline void set_m_EaseInDuration_12(double value)
	{
		___m_EaseInDuration_12 = value;
	}

	inline static int32_t get_offset_of_m_EaseOutDuration_13() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_EaseOutDuration_13)); }
	inline double get_m_EaseOutDuration_13() const { return ___m_EaseOutDuration_13; }
	inline double* get_address_of_m_EaseOutDuration_13() { return &___m_EaseOutDuration_13; }
	inline void set_m_EaseOutDuration_13(double value)
	{
		___m_EaseOutDuration_13 = value;
	}

	inline static int32_t get_offset_of_m_BlendInDuration_14() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_BlendInDuration_14)); }
	inline double get_m_BlendInDuration_14() const { return ___m_BlendInDuration_14; }
	inline double* get_address_of_m_BlendInDuration_14() { return &___m_BlendInDuration_14; }
	inline void set_m_BlendInDuration_14(double value)
	{
		___m_BlendInDuration_14 = value;
	}

	inline static int32_t get_offset_of_m_BlendOutDuration_15() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_BlendOutDuration_15)); }
	inline double get_m_BlendOutDuration_15() const { return ___m_BlendOutDuration_15; }
	inline double* get_address_of_m_BlendOutDuration_15() { return &___m_BlendOutDuration_15; }
	inline void set_m_BlendOutDuration_15(double value)
	{
		___m_BlendOutDuration_15 = value;
	}

	inline static int32_t get_offset_of_m_MixInCurve_16() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_MixInCurve_16)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_MixInCurve_16() const { return ___m_MixInCurve_16; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_MixInCurve_16() { return &___m_MixInCurve_16; }
	inline void set_m_MixInCurve_16(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_MixInCurve_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_MixInCurve_16), value);
	}

	inline static int32_t get_offset_of_m_MixOutCurve_17() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_MixOutCurve_17)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_MixOutCurve_17() const { return ___m_MixOutCurve_17; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_MixOutCurve_17() { return &___m_MixOutCurve_17; }
	inline void set_m_MixOutCurve_17(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_MixOutCurve_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_MixOutCurve_17), value);
	}

	inline static int32_t get_offset_of_m_BlendInCurveMode_18() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_BlendInCurveMode_18)); }
	inline int32_t get_m_BlendInCurveMode_18() const { return ___m_BlendInCurveMode_18; }
	inline int32_t* get_address_of_m_BlendInCurveMode_18() { return &___m_BlendInCurveMode_18; }
	inline void set_m_BlendInCurveMode_18(int32_t value)
	{
		___m_BlendInCurveMode_18 = value;
	}

	inline static int32_t get_offset_of_m_BlendOutCurveMode_19() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_BlendOutCurveMode_19)); }
	inline int32_t get_m_BlendOutCurveMode_19() const { return ___m_BlendOutCurveMode_19; }
	inline int32_t* get_address_of_m_BlendOutCurveMode_19() { return &___m_BlendOutCurveMode_19; }
	inline void set_m_BlendOutCurveMode_19(int32_t value)
	{
		___m_BlendOutCurveMode_19 = value;
	}

	inline static int32_t get_offset_of_m_ExposedParameterNames_20() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_ExposedParameterNames_20)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_m_ExposedParameterNames_20() const { return ___m_ExposedParameterNames_20; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_m_ExposedParameterNames_20() { return &___m_ExposedParameterNames_20; }
	inline void set_m_ExposedParameterNames_20(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___m_ExposedParameterNames_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExposedParameterNames_20), value);
	}

	inline static int32_t get_offset_of_m_AnimationCurves_21() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_AnimationCurves_21)); }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * get_m_AnimationCurves_21() const { return ___m_AnimationCurves_21; }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE ** get_address_of_m_AnimationCurves_21() { return &___m_AnimationCurves_21; }
	inline void set_m_AnimationCurves_21(AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * value)
	{
		___m_AnimationCurves_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationCurves_21), value);
	}

	inline static int32_t get_offset_of_m_Recordable_22() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_Recordable_22)); }
	inline bool get_m_Recordable_22() const { return ___m_Recordable_22; }
	inline bool* get_address_of_m_Recordable_22() { return &___m_Recordable_22; }
	inline void set_m_Recordable_22(bool value)
	{
		___m_Recordable_22 = value;
	}

	inline static int32_t get_offset_of_m_PostExtrapolationMode_23() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_PostExtrapolationMode_23)); }
	inline int32_t get_m_PostExtrapolationMode_23() const { return ___m_PostExtrapolationMode_23; }
	inline int32_t* get_address_of_m_PostExtrapolationMode_23() { return &___m_PostExtrapolationMode_23; }
	inline void set_m_PostExtrapolationMode_23(int32_t value)
	{
		___m_PostExtrapolationMode_23 = value;
	}

	inline static int32_t get_offset_of_m_PreExtrapolationMode_24() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_PreExtrapolationMode_24)); }
	inline int32_t get_m_PreExtrapolationMode_24() const { return ___m_PreExtrapolationMode_24; }
	inline int32_t* get_address_of_m_PreExtrapolationMode_24() { return &___m_PreExtrapolationMode_24; }
	inline void set_m_PreExtrapolationMode_24(int32_t value)
	{
		___m_PreExtrapolationMode_24 = value;
	}

	inline static int32_t get_offset_of_m_PostExtrapolationTime_25() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_PostExtrapolationTime_25)); }
	inline double get_m_PostExtrapolationTime_25() const { return ___m_PostExtrapolationTime_25; }
	inline double* get_address_of_m_PostExtrapolationTime_25() { return &___m_PostExtrapolationTime_25; }
	inline void set_m_PostExtrapolationTime_25(double value)
	{
		___m_PostExtrapolationTime_25 = value;
	}

	inline static int32_t get_offset_of_m_PreExtrapolationTime_26() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_PreExtrapolationTime_26)); }
	inline double get_m_PreExtrapolationTime_26() const { return ___m_PreExtrapolationTime_26; }
	inline double* get_address_of_m_PreExtrapolationTime_26() { return &___m_PreExtrapolationTime_26; }
	inline void set_m_PreExtrapolationTime_26(double value)
	{
		___m_PreExtrapolationTime_26 = value;
	}

	inline static int32_t get_offset_of_m_DisplayName_27() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_DisplayName_27)); }
	inline String_t* get_m_DisplayName_27() const { return ___m_DisplayName_27; }
	inline String_t** get_address_of_m_DisplayName_27() { return &___m_DisplayName_27; }
	inline void set_m_DisplayName_27(String_t* value)
	{
		___m_DisplayName_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisplayName_27), value);
	}

	inline static int32_t get_offset_of_U3CdirtyHashU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___U3CdirtyHashU3Ek__BackingField_28)); }
	inline int32_t get_U3CdirtyHashU3Ek__BackingField_28() const { return ___U3CdirtyHashU3Ek__BackingField_28; }
	inline int32_t* get_address_of_U3CdirtyHashU3Ek__BackingField_28() { return &___U3CdirtyHashU3Ek__BackingField_28; }
	inline void set_U3CdirtyHashU3Ek__BackingField_28(int32_t value)
	{
		___U3CdirtyHashU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_m_Version_30() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A, ___m_Version_30)); }
	inline int32_t get_m_Version_30() const { return ___m_Version_30; }
	inline int32_t* get_address_of_m_Version_30() { return &___m_Version_30; }
	inline void set_m_Version_30(int32_t value)
	{
		___m_Version_30 = value;
	}
};

struct TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields
{
public:
	// UnityEngine.Timeline.ClipCaps UnityEngine.Timeline.TimelineClip::kDefaultClipCaps
	int32_t ___kDefaultClipCaps_0;
	// System.Single UnityEngine.Timeline.TimelineClip::kDefaultClipDurationInSeconds
	float ___kDefaultClipDurationInSeconds_1;
	// System.Double UnityEngine.Timeline.TimelineClip::kTimeScaleMin
	double ___kTimeScaleMin_2;
	// System.Double UnityEngine.Timeline.TimelineClip::kTimeScaleMax
	double ___kTimeScaleMax_3;
	// System.Double UnityEngine.Timeline.TimelineClip::kMinDuration
	double ___kMinDuration_4;
	// System.Double UnityEngine.Timeline.TimelineClip::kMaxTimeValue
	double ___kMaxTimeValue_5;

public:
	inline static int32_t get_offset_of_kDefaultClipCaps_0() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields, ___kDefaultClipCaps_0)); }
	inline int32_t get_kDefaultClipCaps_0() const { return ___kDefaultClipCaps_0; }
	inline int32_t* get_address_of_kDefaultClipCaps_0() { return &___kDefaultClipCaps_0; }
	inline void set_kDefaultClipCaps_0(int32_t value)
	{
		___kDefaultClipCaps_0 = value;
	}

	inline static int32_t get_offset_of_kDefaultClipDurationInSeconds_1() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields, ___kDefaultClipDurationInSeconds_1)); }
	inline float get_kDefaultClipDurationInSeconds_1() const { return ___kDefaultClipDurationInSeconds_1; }
	inline float* get_address_of_kDefaultClipDurationInSeconds_1() { return &___kDefaultClipDurationInSeconds_1; }
	inline void set_kDefaultClipDurationInSeconds_1(float value)
	{
		___kDefaultClipDurationInSeconds_1 = value;
	}

	inline static int32_t get_offset_of_kTimeScaleMin_2() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields, ___kTimeScaleMin_2)); }
	inline double get_kTimeScaleMin_2() const { return ___kTimeScaleMin_2; }
	inline double* get_address_of_kTimeScaleMin_2() { return &___kTimeScaleMin_2; }
	inline void set_kTimeScaleMin_2(double value)
	{
		___kTimeScaleMin_2 = value;
	}

	inline static int32_t get_offset_of_kTimeScaleMax_3() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields, ___kTimeScaleMax_3)); }
	inline double get_kTimeScaleMax_3() const { return ___kTimeScaleMax_3; }
	inline double* get_address_of_kTimeScaleMax_3() { return &___kTimeScaleMax_3; }
	inline void set_kTimeScaleMax_3(double value)
	{
		___kTimeScaleMax_3 = value;
	}

	inline static int32_t get_offset_of_kMinDuration_4() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields, ___kMinDuration_4)); }
	inline double get_kMinDuration_4() const { return ___kMinDuration_4; }
	inline double* get_address_of_kMinDuration_4() { return &___kMinDuration_4; }
	inline void set_kMinDuration_4(double value)
	{
		___kMinDuration_4 = value;
	}

	inline static int32_t get_offset_of_kMaxTimeValue_5() { return static_cast<int32_t>(offsetof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields, ___kMaxTimeValue_5)); }
	inline double get_kMaxTimeValue_5() const { return ___kMaxTimeValue_5; }
	inline double* get_address_of_kMaxTimeValue_5() { return &___kMaxTimeValue_5; }
	inline void set_kMaxTimeValue_5(double value)
	{
		___kMaxTimeValue_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINECLIP_T45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_H
#ifndef TRACKBINDINGTYPEATTRIBUTE_T5A3A97B13F678C07B1F43C3EB1C7531C64E85075_H
#define TRACKBINDINGTYPEATTRIBUTE_T5A3A97B13F678C07B1F43C3EB1C7531C64E85075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackBindingTypeAttribute
struct  TrackBindingTypeAttribute_t5A3A97B13F678C07B1F43C3EB1C7531C64E85075  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type UnityEngine.Timeline.TrackBindingTypeAttribute::type
	Type_t * ___type_0;
	// UnityEngine.Timeline.TrackBindingFlags UnityEngine.Timeline.TrackBindingTypeAttribute::flags
	int32_t ___flags_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TrackBindingTypeAttribute_t5A3A97B13F678C07B1F43C3EB1C7531C64E85075, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_flags_1() { return static_cast<int32_t>(offsetof(TrackBindingTypeAttribute_t5A3A97B13F678C07B1F43C3EB1C7531C64E85075, ___flags_1)); }
	inline int32_t get_flags_1() const { return ___flags_1; }
	inline int32_t* get_address_of_flags_1() { return &___flags_1; }
	inline void set_flags_1(int32_t value)
	{
		___flags_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBINDINGTYPEATTRIBUTE_T5A3A97B13F678C07B1F43C3EB1C7531C64E85075_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef PLAYABLEASSET_T28B670EFE526C0D383A1C5A5AE2A150424E989AD_H
#define PLAYABLEASSET_T28B670EFE526C0D383A1C5A5AE2A150424E989AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T28B670EFE526C0D383A1C5A5AE2A150424E989AD_H
#ifndef ANIMATIONOUTPUTWEIGHTPROCESSOR_T7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2_H
#define ANIMATIONOUTPUTWEIGHTPROCESSOR_T7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationOutputWeightProcessor
struct  AnimationOutputWeightProcessor_t7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2  : public RuntimeObject
{
public:
	// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Timeline.AnimationOutputWeightProcessor::m_Output
	AnimationPlayableOutput_tA10178429D6528BDB4516F6788CE680E349553E6  ___m_Output_0;
	// UnityEngine.Animations.AnimationMotionXToDeltaPlayable UnityEngine.Timeline.AnimationOutputWeightProcessor::m_MotionXPlayable
	AnimationMotionXToDeltaPlayable_tA5F0BE3BA966E1A6661311F185C1544F90302CDC  ___m_MotionXPlayable_1;
	// UnityEngine.Animations.AnimationMixerPlayable UnityEngine.Timeline.AnimationOutputWeightProcessor::m_PoseMixer
	AnimationMixerPlayable_tA71C834654979CF92B034B537EE5A3DA9713030A  ___m_PoseMixer_2;
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Timeline.AnimationOutputWeightProcessor::m_LayerMixer
	AnimationLayerMixerPlayable_t699CCDE32ABD6FC79BFC09064E473D785D9F9371  ___m_LayerMixer_3;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo> UnityEngine.Timeline.AnimationOutputWeightProcessor::m_Mixers
	List_1_tB3781B4D73201DF66D7FFF614E58A2D0172820B4 * ___m_Mixers_4;

public:
	inline static int32_t get_offset_of_m_Output_0() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2, ___m_Output_0)); }
	inline AnimationPlayableOutput_tA10178429D6528BDB4516F6788CE680E349553E6  get_m_Output_0() const { return ___m_Output_0; }
	inline AnimationPlayableOutput_tA10178429D6528BDB4516F6788CE680E349553E6 * get_address_of_m_Output_0() { return &___m_Output_0; }
	inline void set_m_Output_0(AnimationPlayableOutput_tA10178429D6528BDB4516F6788CE680E349553E6  value)
	{
		___m_Output_0 = value;
	}

	inline static int32_t get_offset_of_m_MotionXPlayable_1() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2, ___m_MotionXPlayable_1)); }
	inline AnimationMotionXToDeltaPlayable_tA5F0BE3BA966E1A6661311F185C1544F90302CDC  get_m_MotionXPlayable_1() const { return ___m_MotionXPlayable_1; }
	inline AnimationMotionXToDeltaPlayable_tA5F0BE3BA966E1A6661311F185C1544F90302CDC * get_address_of_m_MotionXPlayable_1() { return &___m_MotionXPlayable_1; }
	inline void set_m_MotionXPlayable_1(AnimationMotionXToDeltaPlayable_tA5F0BE3BA966E1A6661311F185C1544F90302CDC  value)
	{
		___m_MotionXPlayable_1 = value;
	}

	inline static int32_t get_offset_of_m_PoseMixer_2() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2, ___m_PoseMixer_2)); }
	inline AnimationMixerPlayable_tA71C834654979CF92B034B537EE5A3DA9713030A  get_m_PoseMixer_2() const { return ___m_PoseMixer_2; }
	inline AnimationMixerPlayable_tA71C834654979CF92B034B537EE5A3DA9713030A * get_address_of_m_PoseMixer_2() { return &___m_PoseMixer_2; }
	inline void set_m_PoseMixer_2(AnimationMixerPlayable_tA71C834654979CF92B034B537EE5A3DA9713030A  value)
	{
		___m_PoseMixer_2 = value;
	}

	inline static int32_t get_offset_of_m_LayerMixer_3() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2, ___m_LayerMixer_3)); }
	inline AnimationLayerMixerPlayable_t699CCDE32ABD6FC79BFC09064E473D785D9F9371  get_m_LayerMixer_3() const { return ___m_LayerMixer_3; }
	inline AnimationLayerMixerPlayable_t699CCDE32ABD6FC79BFC09064E473D785D9F9371 * get_address_of_m_LayerMixer_3() { return &___m_LayerMixer_3; }
	inline void set_m_LayerMixer_3(AnimationLayerMixerPlayable_t699CCDE32ABD6FC79BFC09064E473D785D9F9371  value)
	{
		___m_LayerMixer_3 = value;
	}

	inline static int32_t get_offset_of_m_Mixers_4() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2, ___m_Mixers_4)); }
	inline List_1_tB3781B4D73201DF66D7FFF614E58A2D0172820B4 * get_m_Mixers_4() const { return ___m_Mixers_4; }
	inline List_1_tB3781B4D73201DF66D7FFF614E58A2D0172820B4 ** get_address_of_m_Mixers_4() { return &___m_Mixers_4; }
	inline void set_m_Mixers_4(List_1_tB3781B4D73201DF66D7FFF614E58A2D0172820B4 * value)
	{
		___m_Mixers_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mixers_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONOUTPUTWEIGHTPROCESSOR_T7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2_H
#ifndef WEIGHTINFO_T0DC40EEC1B90C9BB32621A1978483832819E22CA_H
#define WEIGHTINFO_T0DC40EEC1B90C9BB32621A1978483832819E22CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct  WeightInfo_t0DC40EEC1B90C9BB32621A1978483832819E22CA 
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::mixer
	Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  ___mixer_0;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::parentMixer
	Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  ___parentMixer_1;
	// System.Int32 UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::port
	int32_t ___port_2;
	// System.Boolean UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::modulate
	bool ___modulate_3;

public:
	inline static int32_t get_offset_of_mixer_0() { return static_cast<int32_t>(offsetof(WeightInfo_t0DC40EEC1B90C9BB32621A1978483832819E22CA, ___mixer_0)); }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  get_mixer_0() const { return ___mixer_0; }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0 * get_address_of_mixer_0() { return &___mixer_0; }
	inline void set_mixer_0(Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  value)
	{
		___mixer_0 = value;
	}

	inline static int32_t get_offset_of_parentMixer_1() { return static_cast<int32_t>(offsetof(WeightInfo_t0DC40EEC1B90C9BB32621A1978483832819E22CA, ___parentMixer_1)); }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  get_parentMixer_1() const { return ___parentMixer_1; }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0 * get_address_of_parentMixer_1() { return &___parentMixer_1; }
	inline void set_parentMixer_1(Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  value)
	{
		___parentMixer_1 = value;
	}

	inline static int32_t get_offset_of_port_2() { return static_cast<int32_t>(offsetof(WeightInfo_t0DC40EEC1B90C9BB32621A1978483832819E22CA, ___port_2)); }
	inline int32_t get_port_2() const { return ___port_2; }
	inline int32_t* get_address_of_port_2() { return &___port_2; }
	inline void set_port_2(int32_t value)
	{
		___port_2 = value;
	}

	inline static int32_t get_offset_of_modulate_3() { return static_cast<int32_t>(offsetof(WeightInfo_t0DC40EEC1B90C9BB32621A1978483832819E22CA, ___modulate_3)); }
	inline bool get_modulate_3() const { return ___modulate_3; }
	inline bool* get_address_of_modulate_3() { return &___modulate_3; }
	inline void set_modulate_3(bool value)
	{
		___modulate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct WeightInfo_t0DC40EEC1B90C9BB32621A1978483832819E22CA_marshaled_pinvoke
{
	Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  ___mixer_0;
	Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  ___parentMixer_1;
	int32_t ___port_2;
	int32_t ___modulate_3;
};
// Native definition for COM marshalling of UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct WeightInfo_t0DC40EEC1B90C9BB32621A1978483832819E22CA_marshaled_com
{
	Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  ___mixer_0;
	Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  ___parentMixer_1;
	int32_t ___port_2;
	int32_t ___modulate_3;
};
#endif // WEIGHTINFO_T0DC40EEC1B90C9BB32621A1978483832819E22CA_H
#ifndef U3CU3EC__ITERATOR0_T0E4328F8640EF4E59CCC6B6AB6460DB0D1241EB2_H
#define U3CU3EC__ITERATOR0_T0E4328F8640EF4E59CCC6B6AB6460DB0D1241EB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t0E4328F8640EF4E59CCC6B6AB6460DB0D1241EB2  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.AnimationPlayableAsset UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$this
	AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4 * ___U24this_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$current
	PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  ___U24current_1;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t0E4328F8640EF4E59CCC6B6AB6460DB0D1241EB2, ___U24this_0)); }
	inline AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4 * get_U24this_0() const { return ___U24this_0; }
	inline AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t0E4328F8640EF4E59CCC6B6AB6460DB0D1241EB2, ___U24current_1)); }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  get_U24current_1() const { return ___U24current_1; }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8 * get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  value)
	{
		___U24current_1 = value;
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t0E4328F8640EF4E59CCC6B6AB6460DB0D1241EB2, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t0E4328F8640EF4E59CCC6B6AB6460DB0D1241EB2, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T0E4328F8640EF4E59CCC6B6AB6460DB0D1241EB2_H
#ifndef U3CU3EC__ITERATOR0_TF4C0961AD99D073D1907CEEFF67C5BCB317BA967_H
#define U3CU3EC__ITERATOR0_TF4C0961AD99D073D1907CEEFF67C5BCB317BA967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationTrack/<>c__Iterator0
struct  U3CU3Ec__Iterator0_tF4C0961AD99D073D1907CEEFF67C5BCB317BA967  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.AnimationTrack UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$this
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162 * ___U24this_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$current
	PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  ___U24current_1;
	// System.Boolean UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tF4C0961AD99D073D1907CEEFF67C5BCB317BA967, ___U24this_0)); }
	inline AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162 * get_U24this_0() const { return ___U24this_0; }
	inline AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tF4C0961AD99D073D1907CEEFF67C5BCB317BA967, ___U24current_1)); }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  get_U24current_1() const { return ___U24current_1; }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8 * get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  value)
	{
		___U24current_1 = value;
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tF4C0961AD99D073D1907CEEFF67C5BCB317BA967, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tF4C0961AD99D073D1907CEEFF67C5BCB317BA967, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_TF4C0961AD99D073D1907CEEFF67C5BCB317BA967_H
#ifndef U3CU3EC__ITERATOR0_T25B1DCE9AABCA12916A77273AEAFE5BF2368D758_H
#define U3CU3EC__ITERATOR0_T25B1DCE9AABCA12916A77273AEAFE5BF2368D758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t25B1DCE9AABCA12916A77273AEAFE5BF2368D758  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.AudioPlayableAsset UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$this
	AudioPlayableAsset_t25D7AC7CC32AE16D05D04C479DE263E48E08CAC1 * ___U24this_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$current
	PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  ___U24current_1;
	// System.Boolean UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t25B1DCE9AABCA12916A77273AEAFE5BF2368D758, ___U24this_0)); }
	inline AudioPlayableAsset_t25D7AC7CC32AE16D05D04C479DE263E48E08CAC1 * get_U24this_0() const { return ___U24this_0; }
	inline AudioPlayableAsset_t25D7AC7CC32AE16D05D04C479DE263E48E08CAC1 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AudioPlayableAsset_t25D7AC7CC32AE16D05D04C479DE263E48E08CAC1 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t25B1DCE9AABCA12916A77273AEAFE5BF2368D758, ___U24current_1)); }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  get_U24current_1() const { return ___U24current_1; }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8 * get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  value)
	{
		___U24current_1 = value;
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t25B1DCE9AABCA12916A77273AEAFE5BF2368D758, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t25B1DCE9AABCA12916A77273AEAFE5BF2368D758, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T25B1DCE9AABCA12916A77273AEAFE5BF2368D758_H
#ifndef U3CU3EC__ITERATOR0_TA3A5D78967E5AC878FF21E74C4EFB6DD690681A2_H
#define U3CU3EC__ITERATOR0_TA3A5D78967E5AC878FF21E74C4EFB6DD690681A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioTrack/<>c__Iterator0
struct  U3CU3Ec__Iterator0_tA3A5D78967E5AC878FF21E74C4EFB6DD690681A2  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.AudioTrack UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$this
	AudioTrack_t2A6D40E2647460067ED433025B56D89554743502 * ___U24this_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$current
	PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  ___U24current_1;
	// System.Boolean UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tA3A5D78967E5AC878FF21E74C4EFB6DD690681A2, ___U24this_0)); }
	inline AudioTrack_t2A6D40E2647460067ED433025B56D89554743502 * get_U24this_0() const { return ___U24this_0; }
	inline AudioTrack_t2A6D40E2647460067ED433025B56D89554743502 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AudioTrack_t2A6D40E2647460067ED433025B56D89554743502 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tA3A5D78967E5AC878FF21E74C4EFB6DD690681A2, ___U24current_1)); }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  get_U24current_1() const { return ___U24current_1; }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8 * get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  value)
	{
		___U24current_1 = value;
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tA3A5D78967E5AC878FF21E74C4EFB6DD690681A2, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tA3A5D78967E5AC878FF21E74C4EFB6DD690681A2, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_TA3A5D78967E5AC878FF21E74C4EFB6DD690681A2_H
#ifndef BASICPLAYABLEBEHAVIOUR_T41A7C66B0272D5149F113C941A89FE2275DD2AB4_H
#define BASICPLAYABLEBEHAVIOUR_T41A7C66B0272D5149F113C941A89FE2275DD2AB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.BasicPlayableBehaviour
struct  BasicPlayableBehaviour_t41A7C66B0272D5149F113C941A89FE2275DD2AB4  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICPLAYABLEBEHAVIOUR_T41A7C66B0272D5149F113C941A89FE2275DD2AB4_H
#ifndef INFINITERUNTIMECLIP_T29A5B0CCB080BB3C14C3789D14D5FBD21FA631C1_H
#define INFINITERUNTIMECLIP_T29A5B0CCB080BB3C14C3789D14D5FBD21FA631C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.InfiniteRuntimeClip
struct  InfiniteRuntimeClip_t29A5B0CCB080BB3C14C3789D14D5FBD21FA631C1  : public RuntimeElement_t25E724CA4B982CE9EAD77996253FA3008561ED80
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Timeline.InfiniteRuntimeClip::m_Playable
	Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  ___m_Playable_0;

public:
	inline static int32_t get_offset_of_m_Playable_0() { return static_cast<int32_t>(offsetof(InfiniteRuntimeClip_t29A5B0CCB080BB3C14C3789D14D5FBD21FA631C1, ___m_Playable_0)); }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  get_m_Playable_0() const { return ___m_Playable_0; }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0 * get_address_of_m_Playable_0() { return &___m_Playable_0; }
	inline void set_m_Playable_0(Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  value)
	{
		___m_Playable_0 = value;
	}
};

struct InfiniteRuntimeClip_t29A5B0CCB080BB3C14C3789D14D5FBD21FA631C1_StaticFields
{
public:
	// System.Int64 UnityEngine.Timeline.InfiniteRuntimeClip::kIntervalEnd
	int64_t ___kIntervalEnd_1;

public:
	inline static int32_t get_offset_of_kIntervalEnd_1() { return static_cast<int32_t>(offsetof(InfiniteRuntimeClip_t29A5B0CCB080BB3C14C3789D14D5FBD21FA631C1_StaticFields, ___kIntervalEnd_1)); }
	inline int64_t get_kIntervalEnd_1() const { return ___kIntervalEnd_1; }
	inline int64_t* get_address_of_kIntervalEnd_1() { return &___kIntervalEnd_1; }
	inline void set_kIntervalEnd_1(int64_t value)
	{
		___kIntervalEnd_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFINITERUNTIMECLIP_T29A5B0CCB080BB3C14C3789D14D5FBD21FA631C1_H
#ifndef RUNTIMECLIP_TAAFF82B65DD64256025D661E771DEC34A79C74DF_H
#define RUNTIMECLIP_TAAFF82B65DD64256025D661E771DEC34A79C74DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.RuntimeClip
struct  RuntimeClip_tAAFF82B65DD64256025D661E771DEC34A79C74DF  : public RuntimeClipBase_t4BE09F69F59AE0FD3F61D1C3E0DE29F236776083
{
public:
	// UnityEngine.Timeline.TimelineClip UnityEngine.Timeline.RuntimeClip::m_Clip
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A * ___m_Clip_0;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.RuntimeClip::m_Playable
	Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  ___m_Playable_1;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.RuntimeClip::m_ParentMixer
	Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  ___m_ParentMixer_2;

public:
	inline static int32_t get_offset_of_m_Clip_0() { return static_cast<int32_t>(offsetof(RuntimeClip_tAAFF82B65DD64256025D661E771DEC34A79C74DF, ___m_Clip_0)); }
	inline TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A * get_m_Clip_0() const { return ___m_Clip_0; }
	inline TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A ** get_address_of_m_Clip_0() { return &___m_Clip_0; }
	inline void set_m_Clip_0(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A * value)
	{
		___m_Clip_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_0), value);
	}

	inline static int32_t get_offset_of_m_Playable_1() { return static_cast<int32_t>(offsetof(RuntimeClip_tAAFF82B65DD64256025D661E771DEC34A79C74DF, ___m_Playable_1)); }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  get_m_Playable_1() const { return ___m_Playable_1; }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0 * get_address_of_m_Playable_1() { return &___m_Playable_1; }
	inline void set_m_Playable_1(Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  value)
	{
		___m_Playable_1 = value;
	}

	inline static int32_t get_offset_of_m_ParentMixer_2() { return static_cast<int32_t>(offsetof(RuntimeClip_tAAFF82B65DD64256025D661E771DEC34A79C74DF, ___m_ParentMixer_2)); }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  get_m_ParentMixer_2() const { return ___m_ParentMixer_2; }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0 * get_address_of_m_ParentMixer_2() { return &___m_ParentMixer_2; }
	inline void set_m_ParentMixer_2(Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  value)
	{
		___m_ParentMixer_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMECLIP_TAAFF82B65DD64256025D661E771DEC34A79C74DF_H
#ifndef SCHEDULERUNTIMECLIP_TA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B_H
#define SCHEDULERUNTIMECLIP_TA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ScheduleRuntimeClip
struct  ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B  : public RuntimeClipBase_t4BE09F69F59AE0FD3F61D1C3E0DE29F236776083
{
public:
	// UnityEngine.Timeline.TimelineClip UnityEngine.Timeline.ScheduleRuntimeClip::m_Clip
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A * ___m_Clip_0;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.ScheduleRuntimeClip::m_Playable
	Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  ___m_Playable_1;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.ScheduleRuntimeClip::m_ParentMixer
	Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  ___m_ParentMixer_2;
	// System.Double UnityEngine.Timeline.ScheduleRuntimeClip::m_StartDelay
	double ___m_StartDelay_3;
	// System.Double UnityEngine.Timeline.ScheduleRuntimeClip::m_FinishTail
	double ___m_FinishTail_4;
	// System.Boolean UnityEngine.Timeline.ScheduleRuntimeClip::m_Started
	bool ___m_Started_5;

public:
	inline static int32_t get_offset_of_m_Clip_0() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B, ___m_Clip_0)); }
	inline TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A * get_m_Clip_0() const { return ___m_Clip_0; }
	inline TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A ** get_address_of_m_Clip_0() { return &___m_Clip_0; }
	inline void set_m_Clip_0(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A * value)
	{
		___m_Clip_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_0), value);
	}

	inline static int32_t get_offset_of_m_Playable_1() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B, ___m_Playable_1)); }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  get_m_Playable_1() const { return ___m_Playable_1; }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0 * get_address_of_m_Playable_1() { return &___m_Playable_1; }
	inline void set_m_Playable_1(Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  value)
	{
		___m_Playable_1 = value;
	}

	inline static int32_t get_offset_of_m_ParentMixer_2() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B, ___m_ParentMixer_2)); }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  get_m_ParentMixer_2() const { return ___m_ParentMixer_2; }
	inline Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0 * get_address_of_m_ParentMixer_2() { return &___m_ParentMixer_2; }
	inline void set_m_ParentMixer_2(Playable_t4ABB910C374FCAB6B926DA4D34A85857A59950D0  value)
	{
		___m_ParentMixer_2 = value;
	}

	inline static int32_t get_offset_of_m_StartDelay_3() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B, ___m_StartDelay_3)); }
	inline double get_m_StartDelay_3() const { return ___m_StartDelay_3; }
	inline double* get_address_of_m_StartDelay_3() { return &___m_StartDelay_3; }
	inline void set_m_StartDelay_3(double value)
	{
		___m_StartDelay_3 = value;
	}

	inline static int32_t get_offset_of_m_FinishTail_4() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B, ___m_FinishTail_4)); }
	inline double get_m_FinishTail_4() const { return ___m_FinishTail_4; }
	inline double* get_address_of_m_FinishTail_4() { return &___m_FinishTail_4; }
	inline void set_m_FinishTail_4(double value)
	{
		___m_FinishTail_4 = value;
	}

	inline static int32_t get_offset_of_m_Started_5() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B, ___m_Started_5)); }
	inline bool get_m_Started_5() const { return ___m_Started_5; }
	inline bool* get_address_of_m_Started_5() { return &___m_Started_5; }
	inline void set_m_Started_5(bool value)
	{
		___m_Started_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEDULERUNTIMECLIP_TA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B_H
#ifndef U3CU3EC__ITERATOR0_T2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98_H
#define U3CU3EC__ITERATOR0_T2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_0;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::<outputTracks>__1
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC * ___U3CoutputTracksU3E__1_1;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Playables.PlayableBinding> UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$locvar1
	RuntimeObject* ___U24locvar1_2;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::<output>__2
	PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  ___U3CoutputU3E__2_3;
	// UnityEngine.Timeline.TimelineAsset UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$this
	TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F * ___U24this_4;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$current
	PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  ___U24current_5;
	// System.Boolean UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 UnityEngine.Timeline.TimelineAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98, ___U24locvar0_0)); }
	inline RuntimeObject* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline RuntimeObject** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(RuntimeObject* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U3CoutputTracksU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98, ___U3CoutputTracksU3E__1_1)); }
	inline TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC * get_U3CoutputTracksU3E__1_1() const { return ___U3CoutputTracksU3E__1_1; }
	inline TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC ** get_address_of_U3CoutputTracksU3E__1_1() { return &___U3CoutputTracksU3E__1_1; }
	inline void set_U3CoutputTracksU3E__1_1(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC * value)
	{
		___U3CoutputTracksU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoutputTracksU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98, ___U24locvar1_2)); }
	inline RuntimeObject* get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline RuntimeObject** get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(RuntimeObject* value)
	{
		___U24locvar1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_2), value);
	}

	inline static int32_t get_offset_of_U3CoutputU3E__2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98, ___U3CoutputU3E__2_3)); }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  get_U3CoutputU3E__2_3() const { return ___U3CoutputU3E__2_3; }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8 * get_address_of_U3CoutputU3E__2_3() { return &___U3CoutputU3E__2_3; }
	inline void set_U3CoutputU3E__2_3(PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  value)
	{
		___U3CoutputU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98, ___U24this_4)); }
	inline TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F * get_U24this_4() const { return ___U24this_4; }
	inline TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98, ___U24current_5)); }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  get_U24current_5() const { return ___U24current_5; }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8 * get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  value)
	{
		___U24current_5 = value;
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98_H
#ifndef U3CU3EC__ITERATOR0_T77A545F7789D8D30C9B859376E1F7D8C692B198D_H
#define U3CU3EC__ITERATOR0_T77A545F7789D8D30C9B859376E1F7D8C692B198D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.TrackBindingTypeAttribute UnityEngine.Timeline.TrackAsset/<>c__Iterator0::<attribute>__0
	TrackBindingTypeAttribute_t5A3A97B13F678C07B1F43C3EB1C7531C64E85075 * ___U3CattributeU3E__0_0;
	// System.Type UnityEngine.Timeline.TrackAsset/<>c__Iterator0::<trackBindingType>__0
	Type_t * ___U3CtrackBindingTypeU3E__0_1;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$this
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC * ___U24this_2;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$current
	PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  ___U24current_3;
	// System.Boolean UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CattributeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D, ___U3CattributeU3E__0_0)); }
	inline TrackBindingTypeAttribute_t5A3A97B13F678C07B1F43C3EB1C7531C64E85075 * get_U3CattributeU3E__0_0() const { return ___U3CattributeU3E__0_0; }
	inline TrackBindingTypeAttribute_t5A3A97B13F678C07B1F43C3EB1C7531C64E85075 ** get_address_of_U3CattributeU3E__0_0() { return &___U3CattributeU3E__0_0; }
	inline void set_U3CattributeU3E__0_0(TrackBindingTypeAttribute_t5A3A97B13F678C07B1F43C3EB1C7531C64E85075 * value)
	{
		___U3CattributeU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CattributeU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CtrackBindingTypeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D, ___U3CtrackBindingTypeU3E__0_1)); }
	inline Type_t * get_U3CtrackBindingTypeU3E__0_1() const { return ___U3CtrackBindingTypeU3E__0_1; }
	inline Type_t ** get_address_of_U3CtrackBindingTypeU3E__0_1() { return &___U3CtrackBindingTypeU3E__0_1; }
	inline void set_U3CtrackBindingTypeU3E__0_1(Type_t * value)
	{
		___U3CtrackBindingTypeU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtrackBindingTypeU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D, ___U24this_2)); }
	inline TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC * get_U24this_2() const { return ___U24this_2; }
	inline TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D, ___U24current_3)); }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  get_U24current_3() const { return ___U24current_3; }
	inline PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8 * get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(PlayableBinding_t4D92F4CF16B8608DD83947E5D40CB7690F23F9C8  value)
	{
		___U24current_3 = value;
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T77A545F7789D8D30C9B859376E1F7D8C692B198D_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ACTIVATIONPLAYABLEASSET_T0660B3267843569F5B790ED336634B022B276AEB_H
#define ACTIVATIONPLAYABLEASSET_T0660B3267843569F5B790ED336634B022B276AEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationPlayableAsset
struct  ActivationPlayableAsset_t0660B3267843569F5B790ED336634B022B276AEB  : public PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONPLAYABLEASSET_T0660B3267843569F5B790ED336634B022B276AEB_H
#ifndef ANIMATIONPLAYABLEASSET_T71227EE3B9E197764F7EE22B080D2AFE95C8C4F4_H
#define ANIMATIONPLAYABLEASSET_T71227EE3B9E197764F7EE22B080D2AFE95C8C4F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationPlayableAsset
struct  AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4  : public PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD
{
public:
	// UnityEngine.AnimationClip UnityEngine.Timeline.AnimationPlayableAsset::m_Clip
	AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * ___m_Clip_4;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationPlayableAsset::m_Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Position_5;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationPlayableAsset::m_EulerAngles
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_EulerAngles_6;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset::m_UseTrackMatchFields
	bool ___m_UseTrackMatchFields_7;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.AnimationPlayableAsset::m_MatchTargetFields
	int32_t ___m_MatchTargetFields_8;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset::m_RemoveStartOffset
	bool ___m_RemoveStartOffset_9;
	// UnityEngine.Timeline.AppliedOffsetMode UnityEngine.Timeline.AnimationPlayableAsset::<appliedOffsetMode>k__BackingField
	int32_t ___U3CappliedOffsetModeU3Ek__BackingField_10;
	// System.Int32 UnityEngine.Timeline.AnimationPlayableAsset::m_Version
	int32_t ___m_Version_12;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationPlayableAsset::m_Rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_Rotation_13;

public:
	inline static int32_t get_offset_of_m_Clip_4() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4, ___m_Clip_4)); }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * get_m_Clip_4() const { return ___m_Clip_4; }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE ** get_address_of_m_Clip_4() { return &___m_Clip_4; }
	inline void set_m_Clip_4(AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * value)
	{
		___m_Clip_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_4), value);
	}

	inline static int32_t get_offset_of_m_Position_5() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4, ___m_Position_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Position_5() const { return ___m_Position_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Position_5() { return &___m_Position_5; }
	inline void set_m_Position_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Position_5 = value;
	}

	inline static int32_t get_offset_of_m_EulerAngles_6() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4, ___m_EulerAngles_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_EulerAngles_6() const { return ___m_EulerAngles_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_EulerAngles_6() { return &___m_EulerAngles_6; }
	inline void set_m_EulerAngles_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_EulerAngles_6 = value;
	}

	inline static int32_t get_offset_of_m_UseTrackMatchFields_7() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4, ___m_UseTrackMatchFields_7)); }
	inline bool get_m_UseTrackMatchFields_7() const { return ___m_UseTrackMatchFields_7; }
	inline bool* get_address_of_m_UseTrackMatchFields_7() { return &___m_UseTrackMatchFields_7; }
	inline void set_m_UseTrackMatchFields_7(bool value)
	{
		___m_UseTrackMatchFields_7 = value;
	}

	inline static int32_t get_offset_of_m_MatchTargetFields_8() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4, ___m_MatchTargetFields_8)); }
	inline int32_t get_m_MatchTargetFields_8() const { return ___m_MatchTargetFields_8; }
	inline int32_t* get_address_of_m_MatchTargetFields_8() { return &___m_MatchTargetFields_8; }
	inline void set_m_MatchTargetFields_8(int32_t value)
	{
		___m_MatchTargetFields_8 = value;
	}

	inline static int32_t get_offset_of_m_RemoveStartOffset_9() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4, ___m_RemoveStartOffset_9)); }
	inline bool get_m_RemoveStartOffset_9() const { return ___m_RemoveStartOffset_9; }
	inline bool* get_address_of_m_RemoveStartOffset_9() { return &___m_RemoveStartOffset_9; }
	inline void set_m_RemoveStartOffset_9(bool value)
	{
		___m_RemoveStartOffset_9 = value;
	}

	inline static int32_t get_offset_of_U3CappliedOffsetModeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4, ___U3CappliedOffsetModeU3Ek__BackingField_10)); }
	inline int32_t get_U3CappliedOffsetModeU3Ek__BackingField_10() const { return ___U3CappliedOffsetModeU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CappliedOffsetModeU3Ek__BackingField_10() { return &___U3CappliedOffsetModeU3Ek__BackingField_10; }
	inline void set_U3CappliedOffsetModeU3Ek__BackingField_10(int32_t value)
	{
		___U3CappliedOffsetModeU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_m_Version_12() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4, ___m_Version_12)); }
	inline int32_t get_m_Version_12() const { return ___m_Version_12; }
	inline int32_t* get_address_of_m_Version_12() { return &___m_Version_12; }
	inline void set_m_Version_12(int32_t value)
	{
		___m_Version_12 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_13() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4, ___m_Rotation_13)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_Rotation_13() const { return ___m_Rotation_13; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_Rotation_13() { return &___m_Rotation_13; }
	inline void set_m_Rotation_13(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_Rotation_13 = value;
	}
};

struct AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4_StaticFields
{
public:
	// System.Int32 UnityEngine.Timeline.AnimationPlayableAsset::k_LatestVersion
	int32_t ___k_LatestVersion_11;

public:
	inline static int32_t get_offset_of_k_LatestVersion_11() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4_StaticFields, ___k_LatestVersion_11)); }
	inline int32_t get_k_LatestVersion_11() const { return ___k_LatestVersion_11; }
	inline int32_t* get_address_of_k_LatestVersion_11() { return &___k_LatestVersion_11; }
	inline void set_k_LatestVersion_11(int32_t value)
	{
		___k_LatestVersion_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEASSET_T71227EE3B9E197764F7EE22B080D2AFE95C8C4F4_H
#ifndef AUDIOPLAYABLEASSET_T25D7AC7CC32AE16D05D04C479DE263E48E08CAC1_H
#define AUDIOPLAYABLEASSET_T25D7AC7CC32AE16D05D04C479DE263E48E08CAC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioPlayableAsset
struct  AudioPlayableAsset_t25D7AC7CC32AE16D05D04C479DE263E48E08CAC1  : public PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD
{
public:
	// UnityEngine.AudioClip UnityEngine.Timeline.AudioPlayableAsset::m_Clip
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___m_Clip_4;
	// System.Boolean UnityEngine.Timeline.AudioPlayableAsset::m_Loop
	bool ___m_Loop_5;
	// System.Single UnityEngine.Timeline.AudioPlayableAsset::m_bufferingTime
	float ___m_bufferingTime_6;

public:
	inline static int32_t get_offset_of_m_Clip_4() { return static_cast<int32_t>(offsetof(AudioPlayableAsset_t25D7AC7CC32AE16D05D04C479DE263E48E08CAC1, ___m_Clip_4)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_m_Clip_4() const { return ___m_Clip_4; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_m_Clip_4() { return &___m_Clip_4; }
	inline void set_m_Clip_4(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___m_Clip_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_4), value);
	}

	inline static int32_t get_offset_of_m_Loop_5() { return static_cast<int32_t>(offsetof(AudioPlayableAsset_t25D7AC7CC32AE16D05D04C479DE263E48E08CAC1, ___m_Loop_5)); }
	inline bool get_m_Loop_5() const { return ___m_Loop_5; }
	inline bool* get_address_of_m_Loop_5() { return &___m_Loop_5; }
	inline void set_m_Loop_5(bool value)
	{
		___m_Loop_5 = value;
	}

	inline static int32_t get_offset_of_m_bufferingTime_6() { return static_cast<int32_t>(offsetof(AudioPlayableAsset_t25D7AC7CC32AE16D05D04C479DE263E48E08CAC1, ___m_bufferingTime_6)); }
	inline float get_m_bufferingTime_6() const { return ___m_bufferingTime_6; }
	inline float* get_address_of_m_bufferingTime_6() { return &___m_bufferingTime_6; }
	inline void set_m_bufferingTime_6(float value)
	{
		___m_bufferingTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOPLAYABLEASSET_T25D7AC7CC32AE16D05D04C479DE263E48E08CAC1_H
#ifndef CONTROLPLAYABLEASSET_T402B1672C75CE1DB938A0CFCD6FA90F3325439E6_H
#define CONTROLPLAYABLEASSET_T402B1672C75CE1DB938A0CFCD6FA90F3325439E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ControlPlayableAsset
struct  ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6  : public PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD
{
public:
	// UnityEngine.ExposedReference`1<UnityEngine.GameObject> UnityEngine.Timeline.ControlPlayableAsset::sourceGameObject
	ExposedReference_1_tFE0DB3E784D85584A88D53B1118D208ECA2B7059  ___sourceGameObject_7;
	// UnityEngine.GameObject UnityEngine.Timeline.ControlPlayableAsset::prefabGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___prefabGameObject_8;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::updateParticle
	bool ___updateParticle_9;
	// System.UInt32 UnityEngine.Timeline.ControlPlayableAsset::particleRandomSeed
	uint32_t ___particleRandomSeed_10;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::updateDirector
	bool ___updateDirector_11;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::updateITimeControl
	bool ___updateITimeControl_12;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::searchHierarchy
	bool ___searchHierarchy_13;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::active
	bool ___active_14;
	// UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState UnityEngine.Timeline.ControlPlayableAsset::postPlayback
	int32_t ___postPlayback_15;
	// UnityEngine.Playables.PlayableAsset UnityEngine.Timeline.ControlPlayableAsset::m_ControlDirectorAsset
	PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD * ___m_ControlDirectorAsset_16;
	// System.Double UnityEngine.Timeline.ControlPlayableAsset::m_Duration
	double ___m_Duration_17;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::m_SupportLoop
	bool ___m_SupportLoop_18;

public:
	inline static int32_t get_offset_of_sourceGameObject_7() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6, ___sourceGameObject_7)); }
	inline ExposedReference_1_tFE0DB3E784D85584A88D53B1118D208ECA2B7059  get_sourceGameObject_7() const { return ___sourceGameObject_7; }
	inline ExposedReference_1_tFE0DB3E784D85584A88D53B1118D208ECA2B7059 * get_address_of_sourceGameObject_7() { return &___sourceGameObject_7; }
	inline void set_sourceGameObject_7(ExposedReference_1_tFE0DB3E784D85584A88D53B1118D208ECA2B7059  value)
	{
		___sourceGameObject_7 = value;
	}

	inline static int32_t get_offset_of_prefabGameObject_8() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6, ___prefabGameObject_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_prefabGameObject_8() const { return ___prefabGameObject_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_prefabGameObject_8() { return &___prefabGameObject_8; }
	inline void set_prefabGameObject_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___prefabGameObject_8 = value;
		Il2CppCodeGenWriteBarrier((&___prefabGameObject_8), value);
	}

	inline static int32_t get_offset_of_updateParticle_9() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6, ___updateParticle_9)); }
	inline bool get_updateParticle_9() const { return ___updateParticle_9; }
	inline bool* get_address_of_updateParticle_9() { return &___updateParticle_9; }
	inline void set_updateParticle_9(bool value)
	{
		___updateParticle_9 = value;
	}

	inline static int32_t get_offset_of_particleRandomSeed_10() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6, ___particleRandomSeed_10)); }
	inline uint32_t get_particleRandomSeed_10() const { return ___particleRandomSeed_10; }
	inline uint32_t* get_address_of_particleRandomSeed_10() { return &___particleRandomSeed_10; }
	inline void set_particleRandomSeed_10(uint32_t value)
	{
		___particleRandomSeed_10 = value;
	}

	inline static int32_t get_offset_of_updateDirector_11() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6, ___updateDirector_11)); }
	inline bool get_updateDirector_11() const { return ___updateDirector_11; }
	inline bool* get_address_of_updateDirector_11() { return &___updateDirector_11; }
	inline void set_updateDirector_11(bool value)
	{
		___updateDirector_11 = value;
	}

	inline static int32_t get_offset_of_updateITimeControl_12() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6, ___updateITimeControl_12)); }
	inline bool get_updateITimeControl_12() const { return ___updateITimeControl_12; }
	inline bool* get_address_of_updateITimeControl_12() { return &___updateITimeControl_12; }
	inline void set_updateITimeControl_12(bool value)
	{
		___updateITimeControl_12 = value;
	}

	inline static int32_t get_offset_of_searchHierarchy_13() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6, ___searchHierarchy_13)); }
	inline bool get_searchHierarchy_13() const { return ___searchHierarchy_13; }
	inline bool* get_address_of_searchHierarchy_13() { return &___searchHierarchy_13; }
	inline void set_searchHierarchy_13(bool value)
	{
		___searchHierarchy_13 = value;
	}

	inline static int32_t get_offset_of_active_14() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6, ___active_14)); }
	inline bool get_active_14() const { return ___active_14; }
	inline bool* get_address_of_active_14() { return &___active_14; }
	inline void set_active_14(bool value)
	{
		___active_14 = value;
	}

	inline static int32_t get_offset_of_postPlayback_15() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6, ___postPlayback_15)); }
	inline int32_t get_postPlayback_15() const { return ___postPlayback_15; }
	inline int32_t* get_address_of_postPlayback_15() { return &___postPlayback_15; }
	inline void set_postPlayback_15(int32_t value)
	{
		___postPlayback_15 = value;
	}

	inline static int32_t get_offset_of_m_ControlDirectorAsset_16() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6, ___m_ControlDirectorAsset_16)); }
	inline PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD * get_m_ControlDirectorAsset_16() const { return ___m_ControlDirectorAsset_16; }
	inline PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD ** get_address_of_m_ControlDirectorAsset_16() { return &___m_ControlDirectorAsset_16; }
	inline void set_m_ControlDirectorAsset_16(PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD * value)
	{
		___m_ControlDirectorAsset_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_ControlDirectorAsset_16), value);
	}

	inline static int32_t get_offset_of_m_Duration_17() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6, ___m_Duration_17)); }
	inline double get_m_Duration_17() const { return ___m_Duration_17; }
	inline double* get_address_of_m_Duration_17() { return &___m_Duration_17; }
	inline void set_m_Duration_17(double value)
	{
		___m_Duration_17 = value;
	}

	inline static int32_t get_offset_of_m_SupportLoop_18() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6, ___m_SupportLoop_18)); }
	inline bool get_m_SupportLoop_18() const { return ___m_SupportLoop_18; }
	inline bool* get_address_of_m_SupportLoop_18() { return &___m_SupportLoop_18; }
	inline void set_m_SupportLoop_18(bool value)
	{
		___m_SupportLoop_18 = value;
	}
};

struct ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Timeline.ControlPlayableAsset::k_EmptyDirectorsList
	List_1_t1ED8B5CB268BE8D61CB3CCEE88DEF2DF54E67EE7 * ___k_EmptyDirectorsList_5;
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> UnityEngine.Timeline.ControlPlayableAsset::k_EmptyParticlesList
	List_1_t0E065AF84B0711991C51EB17BB73BF70BAFD3916 * ___k_EmptyParticlesList_6;
	// System.Collections.Generic.HashSet`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Timeline.ControlPlayableAsset::s_ProcessedDirectors
	HashSet_1_tCCD075D900742C873F3ED61FC80520946929DC59 * ___s_ProcessedDirectors_19;
	// System.Collections.Generic.HashSet`1<UnityEngine.GameObject> UnityEngine.Timeline.ControlPlayableAsset::s_CreatedPrefabs
	HashSet_1_tA5E1C933DE229CF4E67A77A1764986FC8574A39E * ___s_CreatedPrefabs_20;

public:
	inline static int32_t get_offset_of_k_EmptyDirectorsList_5() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6_StaticFields, ___k_EmptyDirectorsList_5)); }
	inline List_1_t1ED8B5CB268BE8D61CB3CCEE88DEF2DF54E67EE7 * get_k_EmptyDirectorsList_5() const { return ___k_EmptyDirectorsList_5; }
	inline List_1_t1ED8B5CB268BE8D61CB3CCEE88DEF2DF54E67EE7 ** get_address_of_k_EmptyDirectorsList_5() { return &___k_EmptyDirectorsList_5; }
	inline void set_k_EmptyDirectorsList_5(List_1_t1ED8B5CB268BE8D61CB3CCEE88DEF2DF54E67EE7 * value)
	{
		___k_EmptyDirectorsList_5 = value;
		Il2CppCodeGenWriteBarrier((&___k_EmptyDirectorsList_5), value);
	}

	inline static int32_t get_offset_of_k_EmptyParticlesList_6() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6_StaticFields, ___k_EmptyParticlesList_6)); }
	inline List_1_t0E065AF84B0711991C51EB17BB73BF70BAFD3916 * get_k_EmptyParticlesList_6() const { return ___k_EmptyParticlesList_6; }
	inline List_1_t0E065AF84B0711991C51EB17BB73BF70BAFD3916 ** get_address_of_k_EmptyParticlesList_6() { return &___k_EmptyParticlesList_6; }
	inline void set_k_EmptyParticlesList_6(List_1_t0E065AF84B0711991C51EB17BB73BF70BAFD3916 * value)
	{
		___k_EmptyParticlesList_6 = value;
		Il2CppCodeGenWriteBarrier((&___k_EmptyParticlesList_6), value);
	}

	inline static int32_t get_offset_of_s_ProcessedDirectors_19() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6_StaticFields, ___s_ProcessedDirectors_19)); }
	inline HashSet_1_tCCD075D900742C873F3ED61FC80520946929DC59 * get_s_ProcessedDirectors_19() const { return ___s_ProcessedDirectors_19; }
	inline HashSet_1_tCCD075D900742C873F3ED61FC80520946929DC59 ** get_address_of_s_ProcessedDirectors_19() { return &___s_ProcessedDirectors_19; }
	inline void set_s_ProcessedDirectors_19(HashSet_1_tCCD075D900742C873F3ED61FC80520946929DC59 * value)
	{
		___s_ProcessedDirectors_19 = value;
		Il2CppCodeGenWriteBarrier((&___s_ProcessedDirectors_19), value);
	}

	inline static int32_t get_offset_of_s_CreatedPrefabs_20() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6_StaticFields, ___s_CreatedPrefabs_20)); }
	inline HashSet_1_tA5E1C933DE229CF4E67A77A1764986FC8574A39E * get_s_CreatedPrefabs_20() const { return ___s_CreatedPrefabs_20; }
	inline HashSet_1_tA5E1C933DE229CF4E67A77A1764986FC8574A39E ** get_address_of_s_CreatedPrefabs_20() { return &___s_CreatedPrefabs_20; }
	inline void set_s_CreatedPrefabs_20(HashSet_1_tA5E1C933DE229CF4E67A77A1764986FC8574A39E * value)
	{
		___s_CreatedPrefabs_20 = value;
		Il2CppCodeGenWriteBarrier((&___s_CreatedPrefabs_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLPLAYABLEASSET_T402B1672C75CE1DB938A0CFCD6FA90F3325439E6_H
#ifndef TIMELINEASSET_T2FACE300C5EDB28B0750F4AB44CBF2D0E642343F_H
#define TIMELINEASSET_T2FACE300C5EDB28B0750F4AB44CBF2D0E642343F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineAsset
struct  TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F  : public PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineAsset::m_NextId
	int32_t ___m_NextId_4;
	// System.Collections.Generic.List`1<UnityEngine.ScriptableObject> UnityEngine.Timeline.TimelineAsset::m_Tracks
	List_1_t803BD2FB729584A0A796EBF33774257912427B4E * ___m_Tracks_5;
	// System.Double UnityEngine.Timeline.TimelineAsset::m_FixedDuration
	double ___m_FixedDuration_6;
	// UnityEngine.Timeline.TrackAsset[] UnityEngine.Timeline.TimelineAsset::m_CacheOutputTracks
	TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E* ___m_CacheOutputTracks_7;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TimelineAsset::m_CacheRootTracks
	List_1_tC30659FBAA4D6EE8004DC058D9C7E6B548CCE5BA * ___m_CacheRootTracks_8;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TimelineAsset::m_CacheFlattenedTracks
	List_1_tC30659FBAA4D6EE8004DC058D9C7E6B548CCE5BA * ___m_CacheFlattenedTracks_9;
	// UnityEngine.Timeline.TimelineAsset/EditorSettings UnityEngine.Timeline.TimelineAsset::m_EditorSettings
	EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9 * ___m_EditorSettings_10;
	// UnityEngine.Timeline.TimelineAsset/DurationMode UnityEngine.Timeline.TimelineAsset::m_DurationMode
	int32_t ___m_DurationMode_11;
	// System.Int32 UnityEngine.Timeline.TimelineAsset::m_Version
	int32_t ___m_Version_13;

public:
	inline static int32_t get_offset_of_m_NextId_4() { return static_cast<int32_t>(offsetof(TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F, ___m_NextId_4)); }
	inline int32_t get_m_NextId_4() const { return ___m_NextId_4; }
	inline int32_t* get_address_of_m_NextId_4() { return &___m_NextId_4; }
	inline void set_m_NextId_4(int32_t value)
	{
		___m_NextId_4 = value;
	}

	inline static int32_t get_offset_of_m_Tracks_5() { return static_cast<int32_t>(offsetof(TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F, ___m_Tracks_5)); }
	inline List_1_t803BD2FB729584A0A796EBF33774257912427B4E * get_m_Tracks_5() const { return ___m_Tracks_5; }
	inline List_1_t803BD2FB729584A0A796EBF33774257912427B4E ** get_address_of_m_Tracks_5() { return &___m_Tracks_5; }
	inline void set_m_Tracks_5(List_1_t803BD2FB729584A0A796EBF33774257912427B4E * value)
	{
		___m_Tracks_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tracks_5), value);
	}

	inline static int32_t get_offset_of_m_FixedDuration_6() { return static_cast<int32_t>(offsetof(TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F, ___m_FixedDuration_6)); }
	inline double get_m_FixedDuration_6() const { return ___m_FixedDuration_6; }
	inline double* get_address_of_m_FixedDuration_6() { return &___m_FixedDuration_6; }
	inline void set_m_FixedDuration_6(double value)
	{
		___m_FixedDuration_6 = value;
	}

	inline static int32_t get_offset_of_m_CacheOutputTracks_7() { return static_cast<int32_t>(offsetof(TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F, ___m_CacheOutputTracks_7)); }
	inline TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E* get_m_CacheOutputTracks_7() const { return ___m_CacheOutputTracks_7; }
	inline TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E** get_address_of_m_CacheOutputTracks_7() { return &___m_CacheOutputTracks_7; }
	inline void set_m_CacheOutputTracks_7(TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E* value)
	{
		___m_CacheOutputTracks_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheOutputTracks_7), value);
	}

	inline static int32_t get_offset_of_m_CacheRootTracks_8() { return static_cast<int32_t>(offsetof(TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F, ___m_CacheRootTracks_8)); }
	inline List_1_tC30659FBAA4D6EE8004DC058D9C7E6B548CCE5BA * get_m_CacheRootTracks_8() const { return ___m_CacheRootTracks_8; }
	inline List_1_tC30659FBAA4D6EE8004DC058D9C7E6B548CCE5BA ** get_address_of_m_CacheRootTracks_8() { return &___m_CacheRootTracks_8; }
	inline void set_m_CacheRootTracks_8(List_1_tC30659FBAA4D6EE8004DC058D9C7E6B548CCE5BA * value)
	{
		___m_CacheRootTracks_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheRootTracks_8), value);
	}

	inline static int32_t get_offset_of_m_CacheFlattenedTracks_9() { return static_cast<int32_t>(offsetof(TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F, ___m_CacheFlattenedTracks_9)); }
	inline List_1_tC30659FBAA4D6EE8004DC058D9C7E6B548CCE5BA * get_m_CacheFlattenedTracks_9() const { return ___m_CacheFlattenedTracks_9; }
	inline List_1_tC30659FBAA4D6EE8004DC058D9C7E6B548CCE5BA ** get_address_of_m_CacheFlattenedTracks_9() { return &___m_CacheFlattenedTracks_9; }
	inline void set_m_CacheFlattenedTracks_9(List_1_tC30659FBAA4D6EE8004DC058D9C7E6B548CCE5BA * value)
	{
		___m_CacheFlattenedTracks_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheFlattenedTracks_9), value);
	}

	inline static int32_t get_offset_of_m_EditorSettings_10() { return static_cast<int32_t>(offsetof(TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F, ___m_EditorSettings_10)); }
	inline EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9 * get_m_EditorSettings_10() const { return ___m_EditorSettings_10; }
	inline EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9 ** get_address_of_m_EditorSettings_10() { return &___m_EditorSettings_10; }
	inline void set_m_EditorSettings_10(EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9 * value)
	{
		___m_EditorSettings_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_EditorSettings_10), value);
	}

	inline static int32_t get_offset_of_m_DurationMode_11() { return static_cast<int32_t>(offsetof(TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F, ___m_DurationMode_11)); }
	inline int32_t get_m_DurationMode_11() const { return ___m_DurationMode_11; }
	inline int32_t* get_address_of_m_DurationMode_11() { return &___m_DurationMode_11; }
	inline void set_m_DurationMode_11(int32_t value)
	{
		___m_DurationMode_11 = value;
	}

	inline static int32_t get_offset_of_m_Version_13() { return static_cast<int32_t>(offsetof(TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F, ___m_Version_13)); }
	inline int32_t get_m_Version_13() const { return ___m_Version_13; }
	inline int32_t* get_address_of_m_Version_13() { return &___m_Version_13; }
	inline void set_m_Version_13(int32_t value)
	{
		___m_Version_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEASSET_T2FACE300C5EDB28B0750F4AB44CBF2D0E642343F_H
#ifndef TRACKASSET_T6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_H
#define TRACKASSET_T6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset
struct  TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC  : public PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD
{
public:
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Locked
	bool ___m_Locked_4;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Muted
	bool ___m_Muted_5;
	// System.String UnityEngine.Timeline.TrackAsset::m_CustomPlayableFullTypename
	String_t* ___m_CustomPlayableFullTypename_6;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TrackAsset::m_AnimClip
	AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * ___m_AnimClip_7;
	// UnityEngine.Playables.PlayableAsset UnityEngine.Timeline.TrackAsset::m_Parent
	PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD * ___m_Parent_8;
	// System.Collections.Generic.List`1<UnityEngine.ScriptableObject> UnityEngine.Timeline.TrackAsset::m_Children
	List_1_t803BD2FB729584A0A796EBF33774257912427B4E * ___m_Children_9;
	// System.Int32 UnityEngine.Timeline.TrackAsset::m_ItemsHash
	int32_t ___m_ItemsHash_10;
	// UnityEngine.Timeline.TimelineClip[] UnityEngine.Timeline.TrackAsset::m_ClipsCache
	TimelineClipU5BU5D_t54DF64E1454792297ECC9A75D1E33DB9293334A3* ___m_ClipsCache_11;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_Start
	DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  ___m_Start_12;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_End
	DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  ___m_End_13;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_CacheSorted
	bool ___m_CacheSorted_14;
	// System.Collections.Generic.IEnumerable`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TrackAsset::m_ChildTrackCache
	RuntimeObject* ___m_ChildTrackCache_16;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::m_Clips
	List_1_tBE0C2267D3E6C51CE882E2B6B95F8E67011B1376 * ___m_Clips_18;
	// System.Int32 UnityEngine.Timeline.TrackAsset::m_Version
	int32_t ___m_Version_21;

public:
	inline static int32_t get_offset_of_m_Locked_4() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Locked_4)); }
	inline bool get_m_Locked_4() const { return ___m_Locked_4; }
	inline bool* get_address_of_m_Locked_4() { return &___m_Locked_4; }
	inline void set_m_Locked_4(bool value)
	{
		___m_Locked_4 = value;
	}

	inline static int32_t get_offset_of_m_Muted_5() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Muted_5)); }
	inline bool get_m_Muted_5() const { return ___m_Muted_5; }
	inline bool* get_address_of_m_Muted_5() { return &___m_Muted_5; }
	inline void set_m_Muted_5(bool value)
	{
		___m_Muted_5 = value;
	}

	inline static int32_t get_offset_of_m_CustomPlayableFullTypename_6() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_CustomPlayableFullTypename_6)); }
	inline String_t* get_m_CustomPlayableFullTypename_6() const { return ___m_CustomPlayableFullTypename_6; }
	inline String_t** get_address_of_m_CustomPlayableFullTypename_6() { return &___m_CustomPlayableFullTypename_6; }
	inline void set_m_CustomPlayableFullTypename_6(String_t* value)
	{
		___m_CustomPlayableFullTypename_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomPlayableFullTypename_6), value);
	}

	inline static int32_t get_offset_of_m_AnimClip_7() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_AnimClip_7)); }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * get_m_AnimClip_7() const { return ___m_AnimClip_7; }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE ** get_address_of_m_AnimClip_7() { return &___m_AnimClip_7; }
	inline void set_m_AnimClip_7(AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * value)
	{
		___m_AnimClip_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimClip_7), value);
	}

	inline static int32_t get_offset_of_m_Parent_8() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Parent_8)); }
	inline PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD * get_m_Parent_8() const { return ___m_Parent_8; }
	inline PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD ** get_address_of_m_Parent_8() { return &___m_Parent_8; }
	inline void set_m_Parent_8(PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD * value)
	{
		___m_Parent_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parent_8), value);
	}

	inline static int32_t get_offset_of_m_Children_9() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Children_9)); }
	inline List_1_t803BD2FB729584A0A796EBF33774257912427B4E * get_m_Children_9() const { return ___m_Children_9; }
	inline List_1_t803BD2FB729584A0A796EBF33774257912427B4E ** get_address_of_m_Children_9() { return &___m_Children_9; }
	inline void set_m_Children_9(List_1_t803BD2FB729584A0A796EBF33774257912427B4E * value)
	{
		___m_Children_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Children_9), value);
	}

	inline static int32_t get_offset_of_m_ItemsHash_10() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_ItemsHash_10)); }
	inline int32_t get_m_ItemsHash_10() const { return ___m_ItemsHash_10; }
	inline int32_t* get_address_of_m_ItemsHash_10() { return &___m_ItemsHash_10; }
	inline void set_m_ItemsHash_10(int32_t value)
	{
		___m_ItemsHash_10 = value;
	}

	inline static int32_t get_offset_of_m_ClipsCache_11() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_ClipsCache_11)); }
	inline TimelineClipU5BU5D_t54DF64E1454792297ECC9A75D1E33DB9293334A3* get_m_ClipsCache_11() const { return ___m_ClipsCache_11; }
	inline TimelineClipU5BU5D_t54DF64E1454792297ECC9A75D1E33DB9293334A3** get_address_of_m_ClipsCache_11() { return &___m_ClipsCache_11; }
	inline void set_m_ClipsCache_11(TimelineClipU5BU5D_t54DF64E1454792297ECC9A75D1E33DB9293334A3* value)
	{
		___m_ClipsCache_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClipsCache_11), value);
	}

	inline static int32_t get_offset_of_m_Start_12() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Start_12)); }
	inline DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  get_m_Start_12() const { return ___m_Start_12; }
	inline DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC * get_address_of_m_Start_12() { return &___m_Start_12; }
	inline void set_m_Start_12(DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  value)
	{
		___m_Start_12 = value;
	}

	inline static int32_t get_offset_of_m_End_13() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_End_13)); }
	inline DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  get_m_End_13() const { return ___m_End_13; }
	inline DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC * get_address_of_m_End_13() { return &___m_End_13; }
	inline void set_m_End_13(DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  value)
	{
		___m_End_13 = value;
	}

	inline static int32_t get_offset_of_m_CacheSorted_14() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_CacheSorted_14)); }
	inline bool get_m_CacheSorted_14() const { return ___m_CacheSorted_14; }
	inline bool* get_address_of_m_CacheSorted_14() { return &___m_CacheSorted_14; }
	inline void set_m_CacheSorted_14(bool value)
	{
		___m_CacheSorted_14 = value;
	}

	inline static int32_t get_offset_of_m_ChildTrackCache_16() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_ChildTrackCache_16)); }
	inline RuntimeObject* get_m_ChildTrackCache_16() const { return ___m_ChildTrackCache_16; }
	inline RuntimeObject** get_address_of_m_ChildTrackCache_16() { return &___m_ChildTrackCache_16; }
	inline void set_m_ChildTrackCache_16(RuntimeObject* value)
	{
		___m_ChildTrackCache_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChildTrackCache_16), value);
	}

	inline static int32_t get_offset_of_m_Clips_18() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Clips_18)); }
	inline List_1_tBE0C2267D3E6C51CE882E2B6B95F8E67011B1376 * get_m_Clips_18() const { return ___m_Clips_18; }
	inline List_1_tBE0C2267D3E6C51CE882E2B6B95F8E67011B1376 ** get_address_of_m_Clips_18() { return &___m_Clips_18; }
	inline void set_m_Clips_18(List_1_tBE0C2267D3E6C51CE882E2B6B95F8E67011B1376 * value)
	{
		___m_Clips_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clips_18), value);
	}

	inline static int32_t get_offset_of_m_Version_21() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Version_21)); }
	inline int32_t get_m_Version_21() const { return ___m_Version_21; }
	inline int32_t* get_address_of_m_Version_21() { return &___m_Version_21; }
	inline void set_m_Version_21(int32_t value)
	{
		___m_Version_21 = value;
	}
};

struct TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields
{
public:
	// UnityEngine.Timeline.TrackAsset[] UnityEngine.Timeline.TrackAsset::s_EmptyCache
	TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E* ___s_EmptyCache_15;
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute> UnityEngine.Timeline.TrackAsset::s_TrackBindingTypeAttributeCache
	Dictionary_2_t58C9CA6651216CBA1E51B3DE5F86E81AB30077B1 * ___s_TrackBindingTypeAttributeCache_17;
	// System.Action`3<UnityEngine.Timeline.TimelineClip,UnityEngine.GameObject,UnityEngine.Playables.Playable> UnityEngine.Timeline.TrackAsset::OnPlayableCreate
	Action_3_t94294D297455C1B4C4B6ACA47BC2B4019ECE97CB * ___OnPlayableCreate_19;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::<>f__am$cache0
	Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8 * ___U3CU3Ef__amU24cache0_22;

public:
	inline static int32_t get_offset_of_s_EmptyCache_15() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields, ___s_EmptyCache_15)); }
	inline TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E* get_s_EmptyCache_15() const { return ___s_EmptyCache_15; }
	inline TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E** get_address_of_s_EmptyCache_15() { return &___s_EmptyCache_15; }
	inline void set_s_EmptyCache_15(TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E* value)
	{
		___s_EmptyCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_EmptyCache_15), value);
	}

	inline static int32_t get_offset_of_s_TrackBindingTypeAttributeCache_17() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields, ___s_TrackBindingTypeAttributeCache_17)); }
	inline Dictionary_2_t58C9CA6651216CBA1E51B3DE5F86E81AB30077B1 * get_s_TrackBindingTypeAttributeCache_17() const { return ___s_TrackBindingTypeAttributeCache_17; }
	inline Dictionary_2_t58C9CA6651216CBA1E51B3DE5F86E81AB30077B1 ** get_address_of_s_TrackBindingTypeAttributeCache_17() { return &___s_TrackBindingTypeAttributeCache_17; }
	inline void set_s_TrackBindingTypeAttributeCache_17(Dictionary_2_t58C9CA6651216CBA1E51B3DE5F86E81AB30077B1 * value)
	{
		___s_TrackBindingTypeAttributeCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_TrackBindingTypeAttributeCache_17), value);
	}

	inline static int32_t get_offset_of_OnPlayableCreate_19() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields, ___OnPlayableCreate_19)); }
	inline Action_3_t94294D297455C1B4C4B6ACA47BC2B4019ECE97CB * get_OnPlayableCreate_19() const { return ___OnPlayableCreate_19; }
	inline Action_3_t94294D297455C1B4C4B6ACA47BC2B4019ECE97CB ** get_address_of_OnPlayableCreate_19() { return &___OnPlayableCreate_19; }
	inline void set_OnPlayableCreate_19(Action_3_t94294D297455C1B4C4B6ACA47BC2B4019ECE97CB * value)
	{
		___OnPlayableCreate_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnPlayableCreate_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_22() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields, ___U3CU3Ef__amU24cache0_22)); }
	inline Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8 * get_U3CU3Ef__amU24cache0_22() const { return ___U3CU3Ef__amU24cache0_22; }
	inline Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8 ** get_address_of_U3CU3Ef__amU24cache0_22() { return &___U3CU3Ef__amU24cache0_22; }
	inline void set_U3CU3Ef__amU24cache0_22(Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8 * value)
	{
		___U3CU3Ef__amU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKASSET_T6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_H
#ifndef ACCOUNTSYSTEM_T973757358DCC87227FF205F0DAFECF2EC5A14672_H
#define ACCOUNTSYSTEM_T973757358DCC87227FF205F0DAFECF2EC5A14672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccountSystem
struct  AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject AccountSystem::_uiGeneralMoneyCounters
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiGeneralMoneyCounters_5;
	// MoneyCounter[] AccountSystem::m_aryGreenCash
	MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* ___m_aryGreenCash_6;
	// System.Double AccountSystem::m_nGreenCash
	double ___m_nGreenCash_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> AccountSystem::m_dicTalentPointBuyTimes
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_dicTalentPointBuyTimes_8;
	// MonthCard[] AccountSystem::m_aryMonthCards
	MonthCardU5BU5D_t8E5902E05E070A5633F378B633686AFDD254FC9A* ___m_aryMonthCards_10;
	// System.Boolean AccountSystem::m_bGeneralMoneyCounterVisible
	bool ___m_bGeneralMoneyCounterVisible_11;

public:
	inline static int32_t get_offset_of__uiGeneralMoneyCounters_5() { return static_cast<int32_t>(offsetof(AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672, ____uiGeneralMoneyCounters_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiGeneralMoneyCounters_5() const { return ____uiGeneralMoneyCounters_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiGeneralMoneyCounters_5() { return &____uiGeneralMoneyCounters_5; }
	inline void set__uiGeneralMoneyCounters_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiGeneralMoneyCounters_5 = value;
		Il2CppCodeGenWriteBarrier((&____uiGeneralMoneyCounters_5), value);
	}

	inline static int32_t get_offset_of_m_aryGreenCash_6() { return static_cast<int32_t>(offsetof(AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672, ___m_aryGreenCash_6)); }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* get_m_aryGreenCash_6() const { return ___m_aryGreenCash_6; }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A** get_address_of_m_aryGreenCash_6() { return &___m_aryGreenCash_6; }
	inline void set_m_aryGreenCash_6(MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* value)
	{
		___m_aryGreenCash_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryGreenCash_6), value);
	}

	inline static int32_t get_offset_of_m_nGreenCash_7() { return static_cast<int32_t>(offsetof(AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672, ___m_nGreenCash_7)); }
	inline double get_m_nGreenCash_7() const { return ___m_nGreenCash_7; }
	inline double* get_address_of_m_nGreenCash_7() { return &___m_nGreenCash_7; }
	inline void set_m_nGreenCash_7(double value)
	{
		___m_nGreenCash_7 = value;
	}

	inline static int32_t get_offset_of_m_dicTalentPointBuyTimes_8() { return static_cast<int32_t>(offsetof(AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672, ___m_dicTalentPointBuyTimes_8)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_dicTalentPointBuyTimes_8() const { return ___m_dicTalentPointBuyTimes_8; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_dicTalentPointBuyTimes_8() { return &___m_dicTalentPointBuyTimes_8; }
	inline void set_m_dicTalentPointBuyTimes_8(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_dicTalentPointBuyTimes_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicTalentPointBuyTimes_8), value);
	}

	inline static int32_t get_offset_of_m_aryMonthCards_10() { return static_cast<int32_t>(offsetof(AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672, ___m_aryMonthCards_10)); }
	inline MonthCardU5BU5D_t8E5902E05E070A5633F378B633686AFDD254FC9A* get_m_aryMonthCards_10() const { return ___m_aryMonthCards_10; }
	inline MonthCardU5BU5D_t8E5902E05E070A5633F378B633686AFDD254FC9A** get_address_of_m_aryMonthCards_10() { return &___m_aryMonthCards_10; }
	inline void set_m_aryMonthCards_10(MonthCardU5BU5D_t8E5902E05E070A5633F378B633686AFDD254FC9A* value)
	{
		___m_aryMonthCards_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMonthCards_10), value);
	}

	inline static int32_t get_offset_of_m_bGeneralMoneyCounterVisible_11() { return static_cast<int32_t>(offsetof(AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672, ___m_bGeneralMoneyCounterVisible_11)); }
	inline bool get_m_bGeneralMoneyCounterVisible_11() const { return ___m_bGeneralMoneyCounterVisible_11; }
	inline bool* get_address_of_m_bGeneralMoneyCounterVisible_11() { return &___m_bGeneralMoneyCounterVisible_11; }
	inline void set_m_bGeneralMoneyCounterVisible_11(bool value)
	{
		___m_bGeneralMoneyCounterVisible_11 = value;
	}
};

struct AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672_StaticFields
{
public:
	// AccountSystem AccountSystem::s_Instance
	AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672_StaticFields, ___s_Instance_4)); }
	inline AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCOUNTSYSTEM_T973757358DCC87227FF205F0DAFECF2EC5A14672_H
#ifndef ACTIVITY_TCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3_H
#define ACTIVITY_TCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Activity
struct  Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Activity::m_fWatchAdsBuyVehicleFreeInterval
	float ___m_fWatchAdsBuyVehicleFreeInterval_5;
	// System.Single Activity::m_fWatchAdsBuyVehicleFreeTimeElaspe
	float ___m_fWatchAdsBuyVehicleFreeTimeElaspe_6;
	// System.Int32 Activity::m_nCurWatchAdsLevel
	int32_t ___m_nCurWatchAdsLevel_7;
	// System.Int32 Activity::m_nCurPlanetId
	int32_t ___m_nCurPlanetId_8;
	// System.Int32 Activity::m_nTrackId
	int32_t ___m_nTrackId_9;
	// System.DateTime Activity::m_dtLastWatchAdsTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dtLastWatchAdsTime_10;

public:
	inline static int32_t get_offset_of_m_fWatchAdsBuyVehicleFreeInterval_5() { return static_cast<int32_t>(offsetof(Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3, ___m_fWatchAdsBuyVehicleFreeInterval_5)); }
	inline float get_m_fWatchAdsBuyVehicleFreeInterval_5() const { return ___m_fWatchAdsBuyVehicleFreeInterval_5; }
	inline float* get_address_of_m_fWatchAdsBuyVehicleFreeInterval_5() { return &___m_fWatchAdsBuyVehicleFreeInterval_5; }
	inline void set_m_fWatchAdsBuyVehicleFreeInterval_5(float value)
	{
		___m_fWatchAdsBuyVehicleFreeInterval_5 = value;
	}

	inline static int32_t get_offset_of_m_fWatchAdsBuyVehicleFreeTimeElaspe_6() { return static_cast<int32_t>(offsetof(Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3, ___m_fWatchAdsBuyVehicleFreeTimeElaspe_6)); }
	inline float get_m_fWatchAdsBuyVehicleFreeTimeElaspe_6() const { return ___m_fWatchAdsBuyVehicleFreeTimeElaspe_6; }
	inline float* get_address_of_m_fWatchAdsBuyVehicleFreeTimeElaspe_6() { return &___m_fWatchAdsBuyVehicleFreeTimeElaspe_6; }
	inline void set_m_fWatchAdsBuyVehicleFreeTimeElaspe_6(float value)
	{
		___m_fWatchAdsBuyVehicleFreeTimeElaspe_6 = value;
	}

	inline static int32_t get_offset_of_m_nCurWatchAdsLevel_7() { return static_cast<int32_t>(offsetof(Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3, ___m_nCurWatchAdsLevel_7)); }
	inline int32_t get_m_nCurWatchAdsLevel_7() const { return ___m_nCurWatchAdsLevel_7; }
	inline int32_t* get_address_of_m_nCurWatchAdsLevel_7() { return &___m_nCurWatchAdsLevel_7; }
	inline void set_m_nCurWatchAdsLevel_7(int32_t value)
	{
		___m_nCurWatchAdsLevel_7 = value;
	}

	inline static int32_t get_offset_of_m_nCurPlanetId_8() { return static_cast<int32_t>(offsetof(Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3, ___m_nCurPlanetId_8)); }
	inline int32_t get_m_nCurPlanetId_8() const { return ___m_nCurPlanetId_8; }
	inline int32_t* get_address_of_m_nCurPlanetId_8() { return &___m_nCurPlanetId_8; }
	inline void set_m_nCurPlanetId_8(int32_t value)
	{
		___m_nCurPlanetId_8 = value;
	}

	inline static int32_t get_offset_of_m_nTrackId_9() { return static_cast<int32_t>(offsetof(Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3, ___m_nTrackId_9)); }
	inline int32_t get_m_nTrackId_9() const { return ___m_nTrackId_9; }
	inline int32_t* get_address_of_m_nTrackId_9() { return &___m_nTrackId_9; }
	inline void set_m_nTrackId_9(int32_t value)
	{
		___m_nTrackId_9 = value;
	}

	inline static int32_t get_offset_of_m_dtLastWatchAdsTime_10() { return static_cast<int32_t>(offsetof(Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3, ___m_dtLastWatchAdsTime_10)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dtLastWatchAdsTime_10() const { return ___m_dtLastWatchAdsTime_10; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dtLastWatchAdsTime_10() { return &___m_dtLastWatchAdsTime_10; }
	inline void set_m_dtLastWatchAdsTime_10(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dtLastWatchAdsTime_10 = value;
	}
};

struct Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3_StaticFields
{
public:
	// Activity Activity::s_Instance
	Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3_StaticFields, ___s_Instance_4)); }
	inline Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVITY_TCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3_H
#ifndef ADMIN_T32563B36AF9D92E819A9499DA78CEA80663DDF60_H
#define ADMIN_T32563B36AF9D92E819A9499DA78CEA80663DDF60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Admin
struct  Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// AdministratorManager/sAdminConfig Admin::m_Config
	sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827  ___m_Config_5;
	// System.Int32 Admin::m_nColddownLeftTime
	int32_t ___m_nColddownLeftTime_6;
	// System.Int32 Admin::m_nDurationLeftTime
	int32_t ___m_nDurationLeftTime_7;
	// System.Int32 Admin::m_nRealDuration
	int32_t ___m_nRealDuration_8;
	// System.Boolean Admin::m_bUsing
	bool ___m_bUsing_9;
	// AdministratorManager/eUisngAdminStatus Admin::m_eStatus
	int32_t ___m_eStatus_10;
	// System.DateTime Admin::m_dateSkillStartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dateSkillStartTime_11;
	// System.Single Admin::m_fColddownPercent
	float ___m_fColddownPercent_12;
	// UIAdministratorCounter Admin::m_BoundCounter
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78 * ___m_BoundCounter_13;
	// District Admin::m_BoundTrack
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * ___m_BoundTrack_14;
	// System.Double Admin::m_nBuyPrice
	double ___m_nBuyPrice_15;
	// System.Int32[] Admin::m_aryIntParams
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_aryIntParams_16;
	// System.Single Admin::m_fTimeElaspe
	float ___m_fTimeElaspe_17;
	// System.Single Admin::m_fSkillGainTimeElaspe
	float ___m_fSkillGainTimeElaspe_18;

public:
	inline static int32_t get_offset_of_m_Config_5() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_Config_5)); }
	inline sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827  get_m_Config_5() const { return ___m_Config_5; }
	inline sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827 * get_address_of_m_Config_5() { return &___m_Config_5; }
	inline void set_m_Config_5(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827  value)
	{
		___m_Config_5 = value;
	}

	inline static int32_t get_offset_of_m_nColddownLeftTime_6() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_nColddownLeftTime_6)); }
	inline int32_t get_m_nColddownLeftTime_6() const { return ___m_nColddownLeftTime_6; }
	inline int32_t* get_address_of_m_nColddownLeftTime_6() { return &___m_nColddownLeftTime_6; }
	inline void set_m_nColddownLeftTime_6(int32_t value)
	{
		___m_nColddownLeftTime_6 = value;
	}

	inline static int32_t get_offset_of_m_nDurationLeftTime_7() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_nDurationLeftTime_7)); }
	inline int32_t get_m_nDurationLeftTime_7() const { return ___m_nDurationLeftTime_7; }
	inline int32_t* get_address_of_m_nDurationLeftTime_7() { return &___m_nDurationLeftTime_7; }
	inline void set_m_nDurationLeftTime_7(int32_t value)
	{
		___m_nDurationLeftTime_7 = value;
	}

	inline static int32_t get_offset_of_m_nRealDuration_8() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_nRealDuration_8)); }
	inline int32_t get_m_nRealDuration_8() const { return ___m_nRealDuration_8; }
	inline int32_t* get_address_of_m_nRealDuration_8() { return &___m_nRealDuration_8; }
	inline void set_m_nRealDuration_8(int32_t value)
	{
		___m_nRealDuration_8 = value;
	}

	inline static int32_t get_offset_of_m_bUsing_9() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_bUsing_9)); }
	inline bool get_m_bUsing_9() const { return ___m_bUsing_9; }
	inline bool* get_address_of_m_bUsing_9() { return &___m_bUsing_9; }
	inline void set_m_bUsing_9(bool value)
	{
		___m_bUsing_9 = value;
	}

	inline static int32_t get_offset_of_m_eStatus_10() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_eStatus_10)); }
	inline int32_t get_m_eStatus_10() const { return ___m_eStatus_10; }
	inline int32_t* get_address_of_m_eStatus_10() { return &___m_eStatus_10; }
	inline void set_m_eStatus_10(int32_t value)
	{
		___m_eStatus_10 = value;
	}

	inline static int32_t get_offset_of_m_dateSkillStartTime_11() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_dateSkillStartTime_11)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dateSkillStartTime_11() const { return ___m_dateSkillStartTime_11; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dateSkillStartTime_11() { return &___m_dateSkillStartTime_11; }
	inline void set_m_dateSkillStartTime_11(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dateSkillStartTime_11 = value;
	}

	inline static int32_t get_offset_of_m_fColddownPercent_12() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_fColddownPercent_12)); }
	inline float get_m_fColddownPercent_12() const { return ___m_fColddownPercent_12; }
	inline float* get_address_of_m_fColddownPercent_12() { return &___m_fColddownPercent_12; }
	inline void set_m_fColddownPercent_12(float value)
	{
		___m_fColddownPercent_12 = value;
	}

	inline static int32_t get_offset_of_m_BoundCounter_13() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_BoundCounter_13)); }
	inline UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78 * get_m_BoundCounter_13() const { return ___m_BoundCounter_13; }
	inline UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78 ** get_address_of_m_BoundCounter_13() { return &___m_BoundCounter_13; }
	inline void set_m_BoundCounter_13(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78 * value)
	{
		___m_BoundCounter_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundCounter_13), value);
	}

	inline static int32_t get_offset_of_m_BoundTrack_14() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_BoundTrack_14)); }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * get_m_BoundTrack_14() const { return ___m_BoundTrack_14; }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A ** get_address_of_m_BoundTrack_14() { return &___m_BoundTrack_14; }
	inline void set_m_BoundTrack_14(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * value)
	{
		___m_BoundTrack_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundTrack_14), value);
	}

	inline static int32_t get_offset_of_m_nBuyPrice_15() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_nBuyPrice_15)); }
	inline double get_m_nBuyPrice_15() const { return ___m_nBuyPrice_15; }
	inline double* get_address_of_m_nBuyPrice_15() { return &___m_nBuyPrice_15; }
	inline void set_m_nBuyPrice_15(double value)
	{
		___m_nBuyPrice_15 = value;
	}

	inline static int32_t get_offset_of_m_aryIntParams_16() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_aryIntParams_16)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_aryIntParams_16() const { return ___m_aryIntParams_16; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_aryIntParams_16() { return &___m_aryIntParams_16; }
	inline void set_m_aryIntParams_16(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_aryIntParams_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryIntParams_16), value);
	}

	inline static int32_t get_offset_of_m_fTimeElaspe_17() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_fTimeElaspe_17)); }
	inline float get_m_fTimeElaspe_17() const { return ___m_fTimeElaspe_17; }
	inline float* get_address_of_m_fTimeElaspe_17() { return &___m_fTimeElaspe_17; }
	inline void set_m_fTimeElaspe_17(float value)
	{
		___m_fTimeElaspe_17 = value;
	}

	inline static int32_t get_offset_of_m_fSkillGainTimeElaspe_18() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60, ___m_fSkillGainTimeElaspe_18)); }
	inline float get_m_fSkillGainTimeElaspe_18() const { return ___m_fSkillGainTimeElaspe_18; }
	inline float* get_address_of_m_fSkillGainTimeElaspe_18() { return &___m_fSkillGainTimeElaspe_18; }
	inline void set_m_fSkillGainTimeElaspe_18(float value)
	{
		___m_fSkillGainTimeElaspe_18 = value;
	}
};

struct Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60_StaticFields
{
public:
	// UnityEngine.Vector3 Admin::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_4;

public:
	inline static int32_t get_offset_of_vecTempScale_4() { return static_cast<int32_t>(offsetof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60_StaticFields, ___vecTempScale_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_4() const { return ___vecTempScale_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_4() { return &___vecTempScale_4; }
	inline void set_vecTempScale_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADMIN_T32563B36AF9D92E819A9499DA78CEA80663DDF60_H
#ifndef ADMINISTRATORMANAGER_T5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24_H
#define ADMINISTRATORMANAGER_T5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdministratorManager
struct  AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Sprite[] AdministratorManager::m_arySkillIcons
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_arySkillIcons_4;
	// UnityEngine.GameObject AdministratorManager::_effectCasting
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____effectCasting_5;
	// System.Int32 AdministratorManager::MAX_QUALITY_LEVEL
	int32_t ___MAX_QUALITY_LEVEL_6;
	// BaseScale AdministratorManager::_basescaleCPosCoinIcon
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____basescaleCPosCoinIcon_9;
	// UnityEngine.Vector3 AdministratorManager::m_vecCoinFlyStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecCoinFlyStartPos_10;
	// UnityEngine.Vector3 AdministratorManager::m_vecCoinFlyMiddlePos_Left
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecCoinFlyMiddlePos_Left_11;
	// UnityEngine.Vector3 AdministratorManager::m_vecCoinFlyMiddlePos_Right
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecCoinFlyMiddlePos_Right_12;
	// UnityEngine.Vector3 AdministratorManager::m_vecCoinFlyEndPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecCoinFlyEndPos_13;
	// System.Single AdministratorManager::m_fSkillGainInterval
	float ___m_fSkillGainInterval_14;
	// UnityEngine.Vector3 AdministratorManager::m_vecTiaoZiPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecTiaoZiPos_16;
	// UnityEngine.Vector3 AdministratorManager::m_vecInitAndMaxScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecInitAndMaxScale_17;
	// System.Int32 AdministratorManager::m_nAvatarIndex
	int32_t ___m_nAvatarIndex_18;
	// UnityEngine.Color AdministratorManager::m_colorUsing
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorUsing_19;
	// UnityEngine.Color AdministratorManager::m_colorNotUsing
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorNotUsing_20;
	// UnityEngine.Sprite[] AdministratorManager::m_aryAvatar
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAvatar_21;
	// UnityEngine.Sprite[] AdministratorManager::m_aryAvatar_Small
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAvatar_Small_22;
	// UnityEngine.Sprite AdministratorManager::m_sprNotUsingBg
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprNotUsingBg_23;
	// UnityEngine.Sprite AdministratorManager::m_sprUsingBg
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprUsingBg_24;
	// UnityEngine.GameObject AdministratorManager::m_preAdminCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preAdminCounter_25;
	// UnityEngine.GameObject AdministratorManager::m_preAdmin
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preAdmin_26;
	// UnityEngine.GameObject AdministratorManager::_panelMain
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelMain_27;
	// UnityEngine.UI.Image AdministratorManager::_imgSceneSkillIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgSceneSkillIcon_28;
	// UnityEngine.GameObject AdministratorManager::_subpanelUsing
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____subpanelUsing_29;
	// UnityEngine.GameObject AdministratorManager::_containerNotUsing
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerNotUsing_30;
	// UnityEngine.GameObject AdministratorManager::_subpanelNotUsing
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____subpanelNotUsing_31;
	// UnityEngine.GameObject AdministratorManager::_containerUsing
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerUsing_32;
	// System.Collections.Generic.List`1<UIAdministratorCounter> AdministratorManager::m_lstAllCounters
	List_1_t25D2A8A2511EE41AE9FA7D85654386B072B20103 * ___m_lstAllCounters_33;
	// UnityEngine.GameObject AdministratorManager::m_containerRecycledCounters
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_containerRecycledCounters_34;
	// UnityEngine.GameObject AdministratorManager::_subpanelSell
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____subpanelSell_35;
	// UnityEngine.GameObject AdministratorManager::_containerTiaoZi
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerTiaoZi_36;
	// UnityEngine.UI.Image AdministratorManager::_imgMoneyIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgMoneyIcon_37;
	// UnityEngine.UI.Text AdministratorManager::_txtPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPrice_38;
	// UnityEngine.UI.Button AdministratorManager::_btnCastSkill
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnCastSkill_39;
	// UnityEngine.UI.Image AdministratorManager::_imgUsingAdminAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgUsingAdminAvatar_40;
	// UnityEngine.UI.Image AdministratorManager::_imgAvatarMask
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgAvatarMask_41;
	// UnityEngine.UI.Text AdministratorManager::_txtLeftTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtLeftTime_42;
	// UnityEngine.UI.Text AdministratorManager::_txtBuyPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtBuyPrice_43;
	// UnityEngine.UI.Image AdministratorManager::_imgSellMoneyIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgSellMoneyIcon_44;
	// UnityEngine.UI.Text AdministratorManager::_txtNum
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtNum_45;
	// System.Int32 AdministratorManager::m_nUnlockTrackLevel
	int32_t ___m_nUnlockTrackLevel_46;
	// System.Int32 AdministratorManager::m_nMaxBuyNum
	int32_t ___m_nMaxBuyNum_47;
	// System.Single AdministratorManager::m_fPriceRisePercent
	float ___m_fPriceRisePercent_48;
	// System.Double AdministratorManager::m_nCurPrice
	double ___m_nCurPrice_49;
	// Admin AdministratorManager::m_CurUsingAdmin
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60 * ___m_CurUsingAdmin_50;
	// Admin AdministratorManager::m_AdminToSell
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60 * ___m_AdminToSell_51;
	// CFrameAnimationEffect AdministratorManager::_effectCastingSkill
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * ____effectCastingSkill_52;
	// AdministratorManager/sAdminConfig AdministratorManager::tempAdminConfig
	sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827  ___tempAdminConfig_53;
	// System.Collections.Generic.Dictionary`2<System.Int32,AdministratorManager/sAdminConfig> AdministratorManager::m_dicAdminConfig
	Dictionary_2_tF731C640B75714140019E77D24BE632B8A927797 * ___m_dicAdminConfig_54;
	// System.Collections.Generic.List`1<System.Int32> AdministratorManager::m_lstProbabilityPool
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_lstProbabilityPool_55;
	// System.Boolean AdministratorManager::m_bAdminConfigLoaded
	bool ___m_bAdminConfigLoaded_56;
	// System.Boolean AdministratorManager::m_bIsVisible
	bool ___m_bIsVisible_57;
	// System.Collections.Generic.List`1<UIAdministratorCounter> AdministratorManager::m_lstRecycledCounters
	List_1_t25D2A8A2511EE41AE9FA7D85654386B072B20103 * ___m_lstRecycledCounters_58;
	// System.Collections.Generic.List`1<Admin> AdministratorManager::m_lstRecycledAdmins
	List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34 * ___m_lstRecycledAdmins_59;

public:
	inline static int32_t get_offset_of_m_arySkillIcons_4() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_arySkillIcons_4)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_arySkillIcons_4() const { return ___m_arySkillIcons_4; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_arySkillIcons_4() { return &___m_arySkillIcons_4; }
	inline void set_m_arySkillIcons_4(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_arySkillIcons_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillIcons_4), value);
	}

	inline static int32_t get_offset_of__effectCasting_5() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____effectCasting_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__effectCasting_5() const { return ____effectCasting_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__effectCasting_5() { return &____effectCasting_5; }
	inline void set__effectCasting_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____effectCasting_5 = value;
		Il2CppCodeGenWriteBarrier((&____effectCasting_5), value);
	}

	inline static int32_t get_offset_of_MAX_QUALITY_LEVEL_6() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___MAX_QUALITY_LEVEL_6)); }
	inline int32_t get_MAX_QUALITY_LEVEL_6() const { return ___MAX_QUALITY_LEVEL_6; }
	inline int32_t* get_address_of_MAX_QUALITY_LEVEL_6() { return &___MAX_QUALITY_LEVEL_6; }
	inline void set_MAX_QUALITY_LEVEL_6(int32_t value)
	{
		___MAX_QUALITY_LEVEL_6 = value;
	}

	inline static int32_t get_offset_of__basescaleCPosCoinIcon_9() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____basescaleCPosCoinIcon_9)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__basescaleCPosCoinIcon_9() const { return ____basescaleCPosCoinIcon_9; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__basescaleCPosCoinIcon_9() { return &____basescaleCPosCoinIcon_9; }
	inline void set__basescaleCPosCoinIcon_9(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____basescaleCPosCoinIcon_9 = value;
		Il2CppCodeGenWriteBarrier((&____basescaleCPosCoinIcon_9), value);
	}

	inline static int32_t get_offset_of_m_vecCoinFlyStartPos_10() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_vecCoinFlyStartPos_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecCoinFlyStartPos_10() const { return ___m_vecCoinFlyStartPos_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecCoinFlyStartPos_10() { return &___m_vecCoinFlyStartPos_10; }
	inline void set_m_vecCoinFlyStartPos_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecCoinFlyStartPos_10 = value;
	}

	inline static int32_t get_offset_of_m_vecCoinFlyMiddlePos_Left_11() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_vecCoinFlyMiddlePos_Left_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecCoinFlyMiddlePos_Left_11() const { return ___m_vecCoinFlyMiddlePos_Left_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecCoinFlyMiddlePos_Left_11() { return &___m_vecCoinFlyMiddlePos_Left_11; }
	inline void set_m_vecCoinFlyMiddlePos_Left_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecCoinFlyMiddlePos_Left_11 = value;
	}

	inline static int32_t get_offset_of_m_vecCoinFlyMiddlePos_Right_12() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_vecCoinFlyMiddlePos_Right_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecCoinFlyMiddlePos_Right_12() const { return ___m_vecCoinFlyMiddlePos_Right_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecCoinFlyMiddlePos_Right_12() { return &___m_vecCoinFlyMiddlePos_Right_12; }
	inline void set_m_vecCoinFlyMiddlePos_Right_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecCoinFlyMiddlePos_Right_12 = value;
	}

	inline static int32_t get_offset_of_m_vecCoinFlyEndPos_13() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_vecCoinFlyEndPos_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecCoinFlyEndPos_13() const { return ___m_vecCoinFlyEndPos_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecCoinFlyEndPos_13() { return &___m_vecCoinFlyEndPos_13; }
	inline void set_m_vecCoinFlyEndPos_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecCoinFlyEndPos_13 = value;
	}

	inline static int32_t get_offset_of_m_fSkillGainInterval_14() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_fSkillGainInterval_14)); }
	inline float get_m_fSkillGainInterval_14() const { return ___m_fSkillGainInterval_14; }
	inline float* get_address_of_m_fSkillGainInterval_14() { return &___m_fSkillGainInterval_14; }
	inline void set_m_fSkillGainInterval_14(float value)
	{
		___m_fSkillGainInterval_14 = value;
	}

	inline static int32_t get_offset_of_m_vecTiaoZiPos_16() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_vecTiaoZiPos_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecTiaoZiPos_16() const { return ___m_vecTiaoZiPos_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecTiaoZiPos_16() { return &___m_vecTiaoZiPos_16; }
	inline void set_m_vecTiaoZiPos_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecTiaoZiPos_16 = value;
	}

	inline static int32_t get_offset_of_m_vecInitAndMaxScale_17() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_vecInitAndMaxScale_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecInitAndMaxScale_17() const { return ___m_vecInitAndMaxScale_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecInitAndMaxScale_17() { return &___m_vecInitAndMaxScale_17; }
	inline void set_m_vecInitAndMaxScale_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecInitAndMaxScale_17 = value;
	}

	inline static int32_t get_offset_of_m_nAvatarIndex_18() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_nAvatarIndex_18)); }
	inline int32_t get_m_nAvatarIndex_18() const { return ___m_nAvatarIndex_18; }
	inline int32_t* get_address_of_m_nAvatarIndex_18() { return &___m_nAvatarIndex_18; }
	inline void set_m_nAvatarIndex_18(int32_t value)
	{
		___m_nAvatarIndex_18 = value;
	}

	inline static int32_t get_offset_of_m_colorUsing_19() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_colorUsing_19)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorUsing_19() const { return ___m_colorUsing_19; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorUsing_19() { return &___m_colorUsing_19; }
	inline void set_m_colorUsing_19(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorUsing_19 = value;
	}

	inline static int32_t get_offset_of_m_colorNotUsing_20() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_colorNotUsing_20)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorNotUsing_20() const { return ___m_colorNotUsing_20; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorNotUsing_20() { return &___m_colorNotUsing_20; }
	inline void set_m_colorNotUsing_20(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorNotUsing_20 = value;
	}

	inline static int32_t get_offset_of_m_aryAvatar_21() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_aryAvatar_21)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAvatar_21() const { return ___m_aryAvatar_21; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAvatar_21() { return &___m_aryAvatar_21; }
	inline void set_m_aryAvatar_21(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAvatar_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAvatar_21), value);
	}

	inline static int32_t get_offset_of_m_aryAvatar_Small_22() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_aryAvatar_Small_22)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAvatar_Small_22() const { return ___m_aryAvatar_Small_22; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAvatar_Small_22() { return &___m_aryAvatar_Small_22; }
	inline void set_m_aryAvatar_Small_22(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAvatar_Small_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAvatar_Small_22), value);
	}

	inline static int32_t get_offset_of_m_sprNotUsingBg_23() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_sprNotUsingBg_23)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprNotUsingBg_23() const { return ___m_sprNotUsingBg_23; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprNotUsingBg_23() { return &___m_sprNotUsingBg_23; }
	inline void set_m_sprNotUsingBg_23(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprNotUsingBg_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprNotUsingBg_23), value);
	}

	inline static int32_t get_offset_of_m_sprUsingBg_24() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_sprUsingBg_24)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprUsingBg_24() const { return ___m_sprUsingBg_24; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprUsingBg_24() { return &___m_sprUsingBg_24; }
	inline void set_m_sprUsingBg_24(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprUsingBg_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprUsingBg_24), value);
	}

	inline static int32_t get_offset_of_m_preAdminCounter_25() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_preAdminCounter_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preAdminCounter_25() const { return ___m_preAdminCounter_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preAdminCounter_25() { return &___m_preAdminCounter_25; }
	inline void set_m_preAdminCounter_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preAdminCounter_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_preAdminCounter_25), value);
	}

	inline static int32_t get_offset_of_m_preAdmin_26() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_preAdmin_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preAdmin_26() const { return ___m_preAdmin_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preAdmin_26() { return &___m_preAdmin_26; }
	inline void set_m_preAdmin_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preAdmin_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_preAdmin_26), value);
	}

	inline static int32_t get_offset_of__panelMain_27() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____panelMain_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelMain_27() const { return ____panelMain_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelMain_27() { return &____panelMain_27; }
	inline void set__panelMain_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelMain_27 = value;
		Il2CppCodeGenWriteBarrier((&____panelMain_27), value);
	}

	inline static int32_t get_offset_of__imgSceneSkillIcon_28() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____imgSceneSkillIcon_28)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgSceneSkillIcon_28() const { return ____imgSceneSkillIcon_28; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgSceneSkillIcon_28() { return &____imgSceneSkillIcon_28; }
	inline void set__imgSceneSkillIcon_28(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgSceneSkillIcon_28 = value;
		Il2CppCodeGenWriteBarrier((&____imgSceneSkillIcon_28), value);
	}

	inline static int32_t get_offset_of__subpanelUsing_29() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____subpanelUsing_29)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__subpanelUsing_29() const { return ____subpanelUsing_29; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__subpanelUsing_29() { return &____subpanelUsing_29; }
	inline void set__subpanelUsing_29(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____subpanelUsing_29 = value;
		Il2CppCodeGenWriteBarrier((&____subpanelUsing_29), value);
	}

	inline static int32_t get_offset_of__containerNotUsing_30() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____containerNotUsing_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerNotUsing_30() const { return ____containerNotUsing_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerNotUsing_30() { return &____containerNotUsing_30; }
	inline void set__containerNotUsing_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerNotUsing_30 = value;
		Il2CppCodeGenWriteBarrier((&____containerNotUsing_30), value);
	}

	inline static int32_t get_offset_of__subpanelNotUsing_31() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____subpanelNotUsing_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__subpanelNotUsing_31() const { return ____subpanelNotUsing_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__subpanelNotUsing_31() { return &____subpanelNotUsing_31; }
	inline void set__subpanelNotUsing_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____subpanelNotUsing_31 = value;
		Il2CppCodeGenWriteBarrier((&____subpanelNotUsing_31), value);
	}

	inline static int32_t get_offset_of__containerUsing_32() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____containerUsing_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerUsing_32() const { return ____containerUsing_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerUsing_32() { return &____containerUsing_32; }
	inline void set__containerUsing_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerUsing_32 = value;
		Il2CppCodeGenWriteBarrier((&____containerUsing_32), value);
	}

	inline static int32_t get_offset_of_m_lstAllCounters_33() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_lstAllCounters_33)); }
	inline List_1_t25D2A8A2511EE41AE9FA7D85654386B072B20103 * get_m_lstAllCounters_33() const { return ___m_lstAllCounters_33; }
	inline List_1_t25D2A8A2511EE41AE9FA7D85654386B072B20103 ** get_address_of_m_lstAllCounters_33() { return &___m_lstAllCounters_33; }
	inline void set_m_lstAllCounters_33(List_1_t25D2A8A2511EE41AE9FA7D85654386B072B20103 * value)
	{
		___m_lstAllCounters_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstAllCounters_33), value);
	}

	inline static int32_t get_offset_of_m_containerRecycledCounters_34() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_containerRecycledCounters_34)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_containerRecycledCounters_34() const { return ___m_containerRecycledCounters_34; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_containerRecycledCounters_34() { return &___m_containerRecycledCounters_34; }
	inline void set_m_containerRecycledCounters_34(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_containerRecycledCounters_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_containerRecycledCounters_34), value);
	}

	inline static int32_t get_offset_of__subpanelSell_35() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____subpanelSell_35)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__subpanelSell_35() const { return ____subpanelSell_35; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__subpanelSell_35() { return &____subpanelSell_35; }
	inline void set__subpanelSell_35(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____subpanelSell_35 = value;
		Il2CppCodeGenWriteBarrier((&____subpanelSell_35), value);
	}

	inline static int32_t get_offset_of__containerTiaoZi_36() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____containerTiaoZi_36)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerTiaoZi_36() const { return ____containerTiaoZi_36; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerTiaoZi_36() { return &____containerTiaoZi_36; }
	inline void set__containerTiaoZi_36(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerTiaoZi_36 = value;
		Il2CppCodeGenWriteBarrier((&____containerTiaoZi_36), value);
	}

	inline static int32_t get_offset_of__imgMoneyIcon_37() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____imgMoneyIcon_37)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgMoneyIcon_37() const { return ____imgMoneyIcon_37; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgMoneyIcon_37() { return &____imgMoneyIcon_37; }
	inline void set__imgMoneyIcon_37(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgMoneyIcon_37 = value;
		Il2CppCodeGenWriteBarrier((&____imgMoneyIcon_37), value);
	}

	inline static int32_t get_offset_of__txtPrice_38() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____txtPrice_38)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPrice_38() const { return ____txtPrice_38; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPrice_38() { return &____txtPrice_38; }
	inline void set__txtPrice_38(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPrice_38 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrice_38), value);
	}

	inline static int32_t get_offset_of__btnCastSkill_39() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____btnCastSkill_39)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnCastSkill_39() const { return ____btnCastSkill_39; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnCastSkill_39() { return &____btnCastSkill_39; }
	inline void set__btnCastSkill_39(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnCastSkill_39 = value;
		Il2CppCodeGenWriteBarrier((&____btnCastSkill_39), value);
	}

	inline static int32_t get_offset_of__imgUsingAdminAvatar_40() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____imgUsingAdminAvatar_40)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgUsingAdminAvatar_40() const { return ____imgUsingAdminAvatar_40; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgUsingAdminAvatar_40() { return &____imgUsingAdminAvatar_40; }
	inline void set__imgUsingAdminAvatar_40(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgUsingAdminAvatar_40 = value;
		Il2CppCodeGenWriteBarrier((&____imgUsingAdminAvatar_40), value);
	}

	inline static int32_t get_offset_of__imgAvatarMask_41() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____imgAvatarMask_41)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgAvatarMask_41() const { return ____imgAvatarMask_41; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgAvatarMask_41() { return &____imgAvatarMask_41; }
	inline void set__imgAvatarMask_41(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgAvatarMask_41 = value;
		Il2CppCodeGenWriteBarrier((&____imgAvatarMask_41), value);
	}

	inline static int32_t get_offset_of__txtLeftTime_42() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____txtLeftTime_42)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtLeftTime_42() const { return ____txtLeftTime_42; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtLeftTime_42() { return &____txtLeftTime_42; }
	inline void set__txtLeftTime_42(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtLeftTime_42 = value;
		Il2CppCodeGenWriteBarrier((&____txtLeftTime_42), value);
	}

	inline static int32_t get_offset_of__txtBuyPrice_43() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____txtBuyPrice_43)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtBuyPrice_43() const { return ____txtBuyPrice_43; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtBuyPrice_43() { return &____txtBuyPrice_43; }
	inline void set__txtBuyPrice_43(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtBuyPrice_43 = value;
		Il2CppCodeGenWriteBarrier((&____txtBuyPrice_43), value);
	}

	inline static int32_t get_offset_of__imgSellMoneyIcon_44() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____imgSellMoneyIcon_44)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgSellMoneyIcon_44() const { return ____imgSellMoneyIcon_44; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgSellMoneyIcon_44() { return &____imgSellMoneyIcon_44; }
	inline void set__imgSellMoneyIcon_44(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgSellMoneyIcon_44 = value;
		Il2CppCodeGenWriteBarrier((&____imgSellMoneyIcon_44), value);
	}

	inline static int32_t get_offset_of__txtNum_45() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____txtNum_45)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtNum_45() const { return ____txtNum_45; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtNum_45() { return &____txtNum_45; }
	inline void set__txtNum_45(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtNum_45 = value;
		Il2CppCodeGenWriteBarrier((&____txtNum_45), value);
	}

	inline static int32_t get_offset_of_m_nUnlockTrackLevel_46() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_nUnlockTrackLevel_46)); }
	inline int32_t get_m_nUnlockTrackLevel_46() const { return ___m_nUnlockTrackLevel_46; }
	inline int32_t* get_address_of_m_nUnlockTrackLevel_46() { return &___m_nUnlockTrackLevel_46; }
	inline void set_m_nUnlockTrackLevel_46(int32_t value)
	{
		___m_nUnlockTrackLevel_46 = value;
	}

	inline static int32_t get_offset_of_m_nMaxBuyNum_47() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_nMaxBuyNum_47)); }
	inline int32_t get_m_nMaxBuyNum_47() const { return ___m_nMaxBuyNum_47; }
	inline int32_t* get_address_of_m_nMaxBuyNum_47() { return &___m_nMaxBuyNum_47; }
	inline void set_m_nMaxBuyNum_47(int32_t value)
	{
		___m_nMaxBuyNum_47 = value;
	}

	inline static int32_t get_offset_of_m_fPriceRisePercent_48() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_fPriceRisePercent_48)); }
	inline float get_m_fPriceRisePercent_48() const { return ___m_fPriceRisePercent_48; }
	inline float* get_address_of_m_fPriceRisePercent_48() { return &___m_fPriceRisePercent_48; }
	inline void set_m_fPriceRisePercent_48(float value)
	{
		___m_fPriceRisePercent_48 = value;
	}

	inline static int32_t get_offset_of_m_nCurPrice_49() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_nCurPrice_49)); }
	inline double get_m_nCurPrice_49() const { return ___m_nCurPrice_49; }
	inline double* get_address_of_m_nCurPrice_49() { return &___m_nCurPrice_49; }
	inline void set_m_nCurPrice_49(double value)
	{
		___m_nCurPrice_49 = value;
	}

	inline static int32_t get_offset_of_m_CurUsingAdmin_50() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_CurUsingAdmin_50)); }
	inline Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60 * get_m_CurUsingAdmin_50() const { return ___m_CurUsingAdmin_50; }
	inline Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60 ** get_address_of_m_CurUsingAdmin_50() { return &___m_CurUsingAdmin_50; }
	inline void set_m_CurUsingAdmin_50(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60 * value)
	{
		___m_CurUsingAdmin_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurUsingAdmin_50), value);
	}

	inline static int32_t get_offset_of_m_AdminToSell_51() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_AdminToSell_51)); }
	inline Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60 * get_m_AdminToSell_51() const { return ___m_AdminToSell_51; }
	inline Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60 ** get_address_of_m_AdminToSell_51() { return &___m_AdminToSell_51; }
	inline void set_m_AdminToSell_51(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60 * value)
	{
		___m_AdminToSell_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_AdminToSell_51), value);
	}

	inline static int32_t get_offset_of__effectCastingSkill_52() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ____effectCastingSkill_52)); }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * get__effectCastingSkill_52() const { return ____effectCastingSkill_52; }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 ** get_address_of__effectCastingSkill_52() { return &____effectCastingSkill_52; }
	inline void set__effectCastingSkill_52(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * value)
	{
		____effectCastingSkill_52 = value;
		Il2CppCodeGenWriteBarrier((&____effectCastingSkill_52), value);
	}

	inline static int32_t get_offset_of_tempAdminConfig_53() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___tempAdminConfig_53)); }
	inline sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827  get_tempAdminConfig_53() const { return ___tempAdminConfig_53; }
	inline sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827 * get_address_of_tempAdminConfig_53() { return &___tempAdminConfig_53; }
	inline void set_tempAdminConfig_53(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827  value)
	{
		___tempAdminConfig_53 = value;
	}

	inline static int32_t get_offset_of_m_dicAdminConfig_54() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_dicAdminConfig_54)); }
	inline Dictionary_2_tF731C640B75714140019E77D24BE632B8A927797 * get_m_dicAdminConfig_54() const { return ___m_dicAdminConfig_54; }
	inline Dictionary_2_tF731C640B75714140019E77D24BE632B8A927797 ** get_address_of_m_dicAdminConfig_54() { return &___m_dicAdminConfig_54; }
	inline void set_m_dicAdminConfig_54(Dictionary_2_tF731C640B75714140019E77D24BE632B8A927797 * value)
	{
		___m_dicAdminConfig_54 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAdminConfig_54), value);
	}

	inline static int32_t get_offset_of_m_lstProbabilityPool_55() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_lstProbabilityPool_55)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_lstProbabilityPool_55() const { return ___m_lstProbabilityPool_55; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_lstProbabilityPool_55() { return &___m_lstProbabilityPool_55; }
	inline void set_m_lstProbabilityPool_55(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_lstProbabilityPool_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstProbabilityPool_55), value);
	}

	inline static int32_t get_offset_of_m_bAdminConfigLoaded_56() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_bAdminConfigLoaded_56)); }
	inline bool get_m_bAdminConfigLoaded_56() const { return ___m_bAdminConfigLoaded_56; }
	inline bool* get_address_of_m_bAdminConfigLoaded_56() { return &___m_bAdminConfigLoaded_56; }
	inline void set_m_bAdminConfigLoaded_56(bool value)
	{
		___m_bAdminConfigLoaded_56 = value;
	}

	inline static int32_t get_offset_of_m_bIsVisible_57() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_bIsVisible_57)); }
	inline bool get_m_bIsVisible_57() const { return ___m_bIsVisible_57; }
	inline bool* get_address_of_m_bIsVisible_57() { return &___m_bIsVisible_57; }
	inline void set_m_bIsVisible_57(bool value)
	{
		___m_bIsVisible_57 = value;
	}

	inline static int32_t get_offset_of_m_lstRecycledCounters_58() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_lstRecycledCounters_58)); }
	inline List_1_t25D2A8A2511EE41AE9FA7D85654386B072B20103 * get_m_lstRecycledCounters_58() const { return ___m_lstRecycledCounters_58; }
	inline List_1_t25D2A8A2511EE41AE9FA7D85654386B072B20103 ** get_address_of_m_lstRecycledCounters_58() { return &___m_lstRecycledCounters_58; }
	inline void set_m_lstRecycledCounters_58(List_1_t25D2A8A2511EE41AE9FA7D85654386B072B20103 * value)
	{
		___m_lstRecycledCounters_58 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledCounters_58), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledAdmins_59() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24, ___m_lstRecycledAdmins_59)); }
	inline List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34 * get_m_lstRecycledAdmins_59() const { return ___m_lstRecycledAdmins_59; }
	inline List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34 ** get_address_of_m_lstRecycledAdmins_59() { return &___m_lstRecycledAdmins_59; }
	inline void set_m_lstRecycledAdmins_59(List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34 * value)
	{
		___m_lstRecycledAdmins_59 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledAdmins_59), value);
	}
};

struct AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24_StaticFields
{
public:
	// UnityEngine.Vector3 AdministratorManager::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_7;
	// UnityEngine.Vector3 AdministratorManager::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_8;
	// AdministratorManager AdministratorManager::s_Instance
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24 * ___s_Instance_15;

public:
	inline static int32_t get_offset_of_vecTempScale_7() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24_StaticFields, ___vecTempScale_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_7() const { return ___vecTempScale_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_7() { return &___vecTempScale_7; }
	inline void set_vecTempScale_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_7 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_8() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24_StaticFields, ___vecTempPos_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_8() const { return ___vecTempPos_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_8() { return &___vecTempPos_8; }
	inline void set_vecTempPos_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_8 = value;
	}

	inline static int32_t get_offset_of_s_Instance_15() { return static_cast<int32_t>(offsetof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24_StaticFields, ___s_Instance_15)); }
	inline AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24 * get_s_Instance_15() const { return ___s_Instance_15; }
	inline AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24 ** get_address_of_s_Instance_15() { return &___s_Instance_15; }
	inline void set_s_Instance_15(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24 * value)
	{
		___s_Instance_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADMINISTRATORMANAGER_T5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24_H
#ifndef ADSMANAGER_TF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A_H
#define ADSMANAGER_TF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdsManager
struct  AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single AdsManager::m_fTotalTime
	float ___m_fTotalTime_5;
	// System.Single AdsManager::m_fCurTime
	float ___m_fCurTime_6;
	// UnityEngine.GameObject AdsManager::_panelAds
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelAds_7;
	// UnityEngine.UI.Button AdsManager::_btnOpenAdsPanel
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnOpenAdsPanel_8;
	// UnityEngine.UI.Button AdsManager::_btnWatchAds
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnWatchAds_9;
	// UnityEngine.GameObject AdsManager::_panelPreAds
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelPreAds_10;
	// UnityEngine.UI.Text AdsManager::_txtAdsLeftTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtAdsLeftTime_11;
	// UnityEngine.UI.Text AdsManager::_txtTitleTiSheng
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTitleTiSheng_12;
	// UnityEngine.UI.Text AdsManager::_txtTitleTiShengJiaoBiao
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTitleTiShengJiaoBiao_13;
	// UnityEngine.UI.Text AdsManager::_txtTitle
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTitle_14;
	// UnityEngine.UI.Text AdsManager::_txtDesc
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDesc_15;
	// UnityEngine.UI.Text AdsManager::_txtLeftTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtLeftTime_16;
	// UnityEngine.UI.Image AdsManager::_imgProgressBar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgProgressBar_17;
	// System.Single AdsManager::m_fBaseRaise
	float ___m_fBaseRaise_18;
	// System.Single AdsManager::m_fRaiseDuration
	float ___m_fRaiseDuration_19;
	// System.Single AdsManager::m_fAdsPlayTime
	float ___m_fAdsPlayTime_20;
	// System.Single AdsManager::m_fRealCoinPromote
	float ___m_fRealCoinPromote_21;
	// System.Single AdsManager::m_fRealDuraton
	float ___m_fRealDuraton_22;
	// System.Single AdsManager::m_fRealTotalTime
	float ___m_fRealTotalTime_23;
	// AdsManager/eAdsType AdsManager::m_eAdsType
	int32_t ___m_eAdsType_24;
	// System.Boolean AdsManager::m_bAdsConfigLoaded
	bool ___m_bAdsConfigLoaded_25;
	// System.DateTime AdsManager::m_dtTotalTimeStartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dtTotalTimeStartTime_26;
	// System.Boolean AdsManager::m_bVisible
	bool ___m_bVisible_27;
	// System.Single AdsManager::m_fPromotePercent
	float ___m_fPromotePercent_28;
	// System.Single AdsManager::m_fMainLoopTimeElapse
	float ___m_fMainLoopTimeElapse_29;
	// System.Single AdsManager::m_fTotalTimeTimeElaspe
	float ___m_fTotalTimeTimeElaspe_30;

public:
	inline static int32_t get_offset_of_m_fTotalTime_5() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_fTotalTime_5)); }
	inline float get_m_fTotalTime_5() const { return ___m_fTotalTime_5; }
	inline float* get_address_of_m_fTotalTime_5() { return &___m_fTotalTime_5; }
	inline void set_m_fTotalTime_5(float value)
	{
		___m_fTotalTime_5 = value;
	}

	inline static int32_t get_offset_of_m_fCurTime_6() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_fCurTime_6)); }
	inline float get_m_fCurTime_6() const { return ___m_fCurTime_6; }
	inline float* get_address_of_m_fCurTime_6() { return &___m_fCurTime_6; }
	inline void set_m_fCurTime_6(float value)
	{
		___m_fCurTime_6 = value;
	}

	inline static int32_t get_offset_of__panelAds_7() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ____panelAds_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelAds_7() const { return ____panelAds_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelAds_7() { return &____panelAds_7; }
	inline void set__panelAds_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelAds_7 = value;
		Il2CppCodeGenWriteBarrier((&____panelAds_7), value);
	}

	inline static int32_t get_offset_of__btnOpenAdsPanel_8() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ____btnOpenAdsPanel_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnOpenAdsPanel_8() const { return ____btnOpenAdsPanel_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnOpenAdsPanel_8() { return &____btnOpenAdsPanel_8; }
	inline void set__btnOpenAdsPanel_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnOpenAdsPanel_8 = value;
		Il2CppCodeGenWriteBarrier((&____btnOpenAdsPanel_8), value);
	}

	inline static int32_t get_offset_of__btnWatchAds_9() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ____btnWatchAds_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnWatchAds_9() const { return ____btnWatchAds_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnWatchAds_9() { return &____btnWatchAds_9; }
	inline void set__btnWatchAds_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnWatchAds_9 = value;
		Il2CppCodeGenWriteBarrier((&____btnWatchAds_9), value);
	}

	inline static int32_t get_offset_of__panelPreAds_10() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ____panelPreAds_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelPreAds_10() const { return ____panelPreAds_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelPreAds_10() { return &____panelPreAds_10; }
	inline void set__panelPreAds_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelPreAds_10 = value;
		Il2CppCodeGenWriteBarrier((&____panelPreAds_10), value);
	}

	inline static int32_t get_offset_of__txtAdsLeftTime_11() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ____txtAdsLeftTime_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtAdsLeftTime_11() const { return ____txtAdsLeftTime_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtAdsLeftTime_11() { return &____txtAdsLeftTime_11; }
	inline void set__txtAdsLeftTime_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtAdsLeftTime_11 = value;
		Il2CppCodeGenWriteBarrier((&____txtAdsLeftTime_11), value);
	}

	inline static int32_t get_offset_of__txtTitleTiSheng_12() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ____txtTitleTiSheng_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTitleTiSheng_12() const { return ____txtTitleTiSheng_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTitleTiSheng_12() { return &____txtTitleTiSheng_12; }
	inline void set__txtTitleTiSheng_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTitleTiSheng_12 = value;
		Il2CppCodeGenWriteBarrier((&____txtTitleTiSheng_12), value);
	}

	inline static int32_t get_offset_of__txtTitleTiShengJiaoBiao_13() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ____txtTitleTiShengJiaoBiao_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTitleTiShengJiaoBiao_13() const { return ____txtTitleTiShengJiaoBiao_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTitleTiShengJiaoBiao_13() { return &____txtTitleTiShengJiaoBiao_13; }
	inline void set__txtTitleTiShengJiaoBiao_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTitleTiShengJiaoBiao_13 = value;
		Il2CppCodeGenWriteBarrier((&____txtTitleTiShengJiaoBiao_13), value);
	}

	inline static int32_t get_offset_of__txtTitle_14() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ____txtTitle_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTitle_14() const { return ____txtTitle_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTitle_14() { return &____txtTitle_14; }
	inline void set__txtTitle_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTitle_14 = value;
		Il2CppCodeGenWriteBarrier((&____txtTitle_14), value);
	}

	inline static int32_t get_offset_of__txtDesc_15() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ____txtDesc_15)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDesc_15() const { return ____txtDesc_15; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDesc_15() { return &____txtDesc_15; }
	inline void set__txtDesc_15(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDesc_15 = value;
		Il2CppCodeGenWriteBarrier((&____txtDesc_15), value);
	}

	inline static int32_t get_offset_of__txtLeftTime_16() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ____txtLeftTime_16)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtLeftTime_16() const { return ____txtLeftTime_16; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtLeftTime_16() { return &____txtLeftTime_16; }
	inline void set__txtLeftTime_16(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtLeftTime_16 = value;
		Il2CppCodeGenWriteBarrier((&____txtLeftTime_16), value);
	}

	inline static int32_t get_offset_of__imgProgressBar_17() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ____imgProgressBar_17)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgProgressBar_17() const { return ____imgProgressBar_17; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgProgressBar_17() { return &____imgProgressBar_17; }
	inline void set__imgProgressBar_17(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgProgressBar_17 = value;
		Il2CppCodeGenWriteBarrier((&____imgProgressBar_17), value);
	}

	inline static int32_t get_offset_of_m_fBaseRaise_18() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_fBaseRaise_18)); }
	inline float get_m_fBaseRaise_18() const { return ___m_fBaseRaise_18; }
	inline float* get_address_of_m_fBaseRaise_18() { return &___m_fBaseRaise_18; }
	inline void set_m_fBaseRaise_18(float value)
	{
		___m_fBaseRaise_18 = value;
	}

	inline static int32_t get_offset_of_m_fRaiseDuration_19() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_fRaiseDuration_19)); }
	inline float get_m_fRaiseDuration_19() const { return ___m_fRaiseDuration_19; }
	inline float* get_address_of_m_fRaiseDuration_19() { return &___m_fRaiseDuration_19; }
	inline void set_m_fRaiseDuration_19(float value)
	{
		___m_fRaiseDuration_19 = value;
	}

	inline static int32_t get_offset_of_m_fAdsPlayTime_20() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_fAdsPlayTime_20)); }
	inline float get_m_fAdsPlayTime_20() const { return ___m_fAdsPlayTime_20; }
	inline float* get_address_of_m_fAdsPlayTime_20() { return &___m_fAdsPlayTime_20; }
	inline void set_m_fAdsPlayTime_20(float value)
	{
		___m_fAdsPlayTime_20 = value;
	}

	inline static int32_t get_offset_of_m_fRealCoinPromote_21() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_fRealCoinPromote_21)); }
	inline float get_m_fRealCoinPromote_21() const { return ___m_fRealCoinPromote_21; }
	inline float* get_address_of_m_fRealCoinPromote_21() { return &___m_fRealCoinPromote_21; }
	inline void set_m_fRealCoinPromote_21(float value)
	{
		___m_fRealCoinPromote_21 = value;
	}

	inline static int32_t get_offset_of_m_fRealDuraton_22() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_fRealDuraton_22)); }
	inline float get_m_fRealDuraton_22() const { return ___m_fRealDuraton_22; }
	inline float* get_address_of_m_fRealDuraton_22() { return &___m_fRealDuraton_22; }
	inline void set_m_fRealDuraton_22(float value)
	{
		___m_fRealDuraton_22 = value;
	}

	inline static int32_t get_offset_of_m_fRealTotalTime_23() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_fRealTotalTime_23)); }
	inline float get_m_fRealTotalTime_23() const { return ___m_fRealTotalTime_23; }
	inline float* get_address_of_m_fRealTotalTime_23() { return &___m_fRealTotalTime_23; }
	inline void set_m_fRealTotalTime_23(float value)
	{
		___m_fRealTotalTime_23 = value;
	}

	inline static int32_t get_offset_of_m_eAdsType_24() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_eAdsType_24)); }
	inline int32_t get_m_eAdsType_24() const { return ___m_eAdsType_24; }
	inline int32_t* get_address_of_m_eAdsType_24() { return &___m_eAdsType_24; }
	inline void set_m_eAdsType_24(int32_t value)
	{
		___m_eAdsType_24 = value;
	}

	inline static int32_t get_offset_of_m_bAdsConfigLoaded_25() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_bAdsConfigLoaded_25)); }
	inline bool get_m_bAdsConfigLoaded_25() const { return ___m_bAdsConfigLoaded_25; }
	inline bool* get_address_of_m_bAdsConfigLoaded_25() { return &___m_bAdsConfigLoaded_25; }
	inline void set_m_bAdsConfigLoaded_25(bool value)
	{
		___m_bAdsConfigLoaded_25 = value;
	}

	inline static int32_t get_offset_of_m_dtTotalTimeStartTime_26() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_dtTotalTimeStartTime_26)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dtTotalTimeStartTime_26() const { return ___m_dtTotalTimeStartTime_26; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dtTotalTimeStartTime_26() { return &___m_dtTotalTimeStartTime_26; }
	inline void set_m_dtTotalTimeStartTime_26(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dtTotalTimeStartTime_26 = value;
	}

	inline static int32_t get_offset_of_m_bVisible_27() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_bVisible_27)); }
	inline bool get_m_bVisible_27() const { return ___m_bVisible_27; }
	inline bool* get_address_of_m_bVisible_27() { return &___m_bVisible_27; }
	inline void set_m_bVisible_27(bool value)
	{
		___m_bVisible_27 = value;
	}

	inline static int32_t get_offset_of_m_fPromotePercent_28() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_fPromotePercent_28)); }
	inline float get_m_fPromotePercent_28() const { return ___m_fPromotePercent_28; }
	inline float* get_address_of_m_fPromotePercent_28() { return &___m_fPromotePercent_28; }
	inline void set_m_fPromotePercent_28(float value)
	{
		___m_fPromotePercent_28 = value;
	}

	inline static int32_t get_offset_of_m_fMainLoopTimeElapse_29() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_fMainLoopTimeElapse_29)); }
	inline float get_m_fMainLoopTimeElapse_29() const { return ___m_fMainLoopTimeElapse_29; }
	inline float* get_address_of_m_fMainLoopTimeElapse_29() { return &___m_fMainLoopTimeElapse_29; }
	inline void set_m_fMainLoopTimeElapse_29(float value)
	{
		___m_fMainLoopTimeElapse_29 = value;
	}

	inline static int32_t get_offset_of_m_fTotalTimeTimeElaspe_30() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A, ___m_fTotalTimeTimeElaspe_30)); }
	inline float get_m_fTotalTimeTimeElaspe_30() const { return ___m_fTotalTimeTimeElaspe_30; }
	inline float* get_address_of_m_fTotalTimeTimeElaspe_30() { return &___m_fTotalTimeTimeElaspe_30; }
	inline void set_m_fTotalTimeTimeElaspe_30(float value)
	{
		___m_fTotalTimeTimeElaspe_30 = value;
	}
};

struct AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A_StaticFields
{
public:
	// AdsManager AdsManager::s_Instance
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A_StaticFields, ___s_Instance_4)); }
	inline AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A * get_s_Instance_4() const { return ___s_Instance_4; }
	inline AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADSMANAGER_TF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A_H
#ifndef ADVENTUREMANAGER_TC7BDA893675D603C9C2733574D9DC4F29B2B430E_H
#define ADVENTUREMANAGER_TC7BDA893675D603C9C2733574D9DC4F29B2B430E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdventureManager
struct  AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UIAdventureRewardShow[] AdventureManager::m_aryAdventureRewardShow
	UIAdventureRewardShowU5BU5D_tB9375AB05FDA93565842B054D44F1C0B321149D2* ___m_aryAdventureRewardShow_7;
	// UnityEngine.UI.Text AdventureManager::_txtLeftTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtLeftTime_8;
	// UnityEngine.UI.Image AdventureManager::_txtProgressbar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____txtProgressbar_9;
	// UnityEngine.GameObject AdventureManager::_containerProgressBar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerProgressBar_10;
	// UnityEngine.UI.Button AdventureManager::_btnCollect
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnCollect_11;
	// UnityEngine.UI.Image AdventureManager::_imgCollectRewardBg
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCollectRewardBg_12;
	// System.Boolean AdventureManager::m_bAdventuring
	bool ___m_bAdventuring_13;
	// System.Int32 AdventureManager::m_nLeftTime
	int32_t ___m_nLeftTime_14;
	// System.Int32 AdventureManager::m_nTotalTime
	int32_t ___m_nTotalTime_15;
	// UIAdventureCounter AdventureManager::m_CurAdventure
	UIAdventureCounter_tE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C * ___m_CurAdventure_16;
	// UnityEngine.GameObject AdventureManager::_panelAdventure
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelAdventure_17;
	// UnityEngine.GameObject AdventureManager::_panelAdventuring
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelAdventuring_18;
	// System.Int32 AdventureManager::m_nRewardStatus
	int32_t ___m_nRewardStatus_19;
	// System.Int32 AdventureManager::m_nTotalRewardNum
	int32_t ___m_nTotalRewardNum_20;
	// System.Int32 AdventureManager::m_nCurShowedRewardNum
	int32_t ___m_nCurShowedRewardNum_21;
	// System.Single AdventureManager::m_fRewardCounterStartPos
	float ___m_fRewardCounterStartPos_22;
	// System.Single AdventureManager::m_fRewardCounterEndPos
	float ___m_fRewardCounterEndPos_23;
	// UIAdventureRewardShow[] AdventureManager::m_aryRewardShowCounters
	UIAdventureRewardShowU5BU5D_tB9375AB05FDA93565842B054D44F1C0B321149D2* ___m_aryRewardShowCounters_24;
	// UIAdventureRewardShow AdventureManager::m_CurShowingRewardCounter
	UIAdventureRewardShow_tC98CDF0A1478D1585512C351DF8B6106FCB4C93F * ___m_CurShowingRewardCounter_25;
	// System.Single AdventureManager::m_fShowRewardWaitingTime
	float ___m_fShowRewardWaitingTime_26;
	// System.Single AdventureManager::m_fShowRewardMovingTime
	float ___m_fShowRewardMovingTime_27;
	// System.Single AdventureManager::m_fShowRewardMovingSpeed
	float ___m_fShowRewardMovingSpeed_28;
	// System.Single AdventureManager::m_fShowRewardScalingSpeed
	float ___m_fShowRewardScalingSpeed_29;
	// System.Boolean AdventureManager::m_bShowingAdventurePanel
	bool ___m_bShowingAdventurePanel_30;
	// System.Single AdventureManager::m_fTimeElapse
	float ___m_fTimeElapse_31;
	// System.Single AdventureManager::m_fShowRewardTimeElapse
	float ___m_fShowRewardTimeElapse_32;

public:
	inline static int32_t get_offset_of_m_aryAdventureRewardShow_7() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_aryAdventureRewardShow_7)); }
	inline UIAdventureRewardShowU5BU5D_tB9375AB05FDA93565842B054D44F1C0B321149D2* get_m_aryAdventureRewardShow_7() const { return ___m_aryAdventureRewardShow_7; }
	inline UIAdventureRewardShowU5BU5D_tB9375AB05FDA93565842B054D44F1C0B321149D2** get_address_of_m_aryAdventureRewardShow_7() { return &___m_aryAdventureRewardShow_7; }
	inline void set_m_aryAdventureRewardShow_7(UIAdventureRewardShowU5BU5D_tB9375AB05FDA93565842B054D44F1C0B321149D2* value)
	{
		___m_aryAdventureRewardShow_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAdventureRewardShow_7), value);
	}

	inline static int32_t get_offset_of__txtLeftTime_8() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ____txtLeftTime_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtLeftTime_8() const { return ____txtLeftTime_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtLeftTime_8() { return &____txtLeftTime_8; }
	inline void set__txtLeftTime_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtLeftTime_8 = value;
		Il2CppCodeGenWriteBarrier((&____txtLeftTime_8), value);
	}

	inline static int32_t get_offset_of__txtProgressbar_9() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ____txtProgressbar_9)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__txtProgressbar_9() const { return ____txtProgressbar_9; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__txtProgressbar_9() { return &____txtProgressbar_9; }
	inline void set__txtProgressbar_9(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____txtProgressbar_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtProgressbar_9), value);
	}

	inline static int32_t get_offset_of__containerProgressBar_10() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ____containerProgressBar_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerProgressBar_10() const { return ____containerProgressBar_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerProgressBar_10() { return &____containerProgressBar_10; }
	inline void set__containerProgressBar_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerProgressBar_10 = value;
		Il2CppCodeGenWriteBarrier((&____containerProgressBar_10), value);
	}

	inline static int32_t get_offset_of__btnCollect_11() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ____btnCollect_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnCollect_11() const { return ____btnCollect_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnCollect_11() { return &____btnCollect_11; }
	inline void set__btnCollect_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnCollect_11 = value;
		Il2CppCodeGenWriteBarrier((&____btnCollect_11), value);
	}

	inline static int32_t get_offset_of__imgCollectRewardBg_12() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ____imgCollectRewardBg_12)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCollectRewardBg_12() const { return ____imgCollectRewardBg_12; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCollectRewardBg_12() { return &____imgCollectRewardBg_12; }
	inline void set__imgCollectRewardBg_12(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCollectRewardBg_12 = value;
		Il2CppCodeGenWriteBarrier((&____imgCollectRewardBg_12), value);
	}

	inline static int32_t get_offset_of_m_bAdventuring_13() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_bAdventuring_13)); }
	inline bool get_m_bAdventuring_13() const { return ___m_bAdventuring_13; }
	inline bool* get_address_of_m_bAdventuring_13() { return &___m_bAdventuring_13; }
	inline void set_m_bAdventuring_13(bool value)
	{
		___m_bAdventuring_13 = value;
	}

	inline static int32_t get_offset_of_m_nLeftTime_14() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_nLeftTime_14)); }
	inline int32_t get_m_nLeftTime_14() const { return ___m_nLeftTime_14; }
	inline int32_t* get_address_of_m_nLeftTime_14() { return &___m_nLeftTime_14; }
	inline void set_m_nLeftTime_14(int32_t value)
	{
		___m_nLeftTime_14 = value;
	}

	inline static int32_t get_offset_of_m_nTotalTime_15() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_nTotalTime_15)); }
	inline int32_t get_m_nTotalTime_15() const { return ___m_nTotalTime_15; }
	inline int32_t* get_address_of_m_nTotalTime_15() { return &___m_nTotalTime_15; }
	inline void set_m_nTotalTime_15(int32_t value)
	{
		___m_nTotalTime_15 = value;
	}

	inline static int32_t get_offset_of_m_CurAdventure_16() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_CurAdventure_16)); }
	inline UIAdventureCounter_tE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C * get_m_CurAdventure_16() const { return ___m_CurAdventure_16; }
	inline UIAdventureCounter_tE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C ** get_address_of_m_CurAdventure_16() { return &___m_CurAdventure_16; }
	inline void set_m_CurAdventure_16(UIAdventureCounter_tE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C * value)
	{
		___m_CurAdventure_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurAdventure_16), value);
	}

	inline static int32_t get_offset_of__panelAdventure_17() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ____panelAdventure_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelAdventure_17() const { return ____panelAdventure_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelAdventure_17() { return &____panelAdventure_17; }
	inline void set__panelAdventure_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelAdventure_17 = value;
		Il2CppCodeGenWriteBarrier((&____panelAdventure_17), value);
	}

	inline static int32_t get_offset_of__panelAdventuring_18() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ____panelAdventuring_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelAdventuring_18() const { return ____panelAdventuring_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelAdventuring_18() { return &____panelAdventuring_18; }
	inline void set__panelAdventuring_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelAdventuring_18 = value;
		Il2CppCodeGenWriteBarrier((&____panelAdventuring_18), value);
	}

	inline static int32_t get_offset_of_m_nRewardStatus_19() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_nRewardStatus_19)); }
	inline int32_t get_m_nRewardStatus_19() const { return ___m_nRewardStatus_19; }
	inline int32_t* get_address_of_m_nRewardStatus_19() { return &___m_nRewardStatus_19; }
	inline void set_m_nRewardStatus_19(int32_t value)
	{
		___m_nRewardStatus_19 = value;
	}

	inline static int32_t get_offset_of_m_nTotalRewardNum_20() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_nTotalRewardNum_20)); }
	inline int32_t get_m_nTotalRewardNum_20() const { return ___m_nTotalRewardNum_20; }
	inline int32_t* get_address_of_m_nTotalRewardNum_20() { return &___m_nTotalRewardNum_20; }
	inline void set_m_nTotalRewardNum_20(int32_t value)
	{
		___m_nTotalRewardNum_20 = value;
	}

	inline static int32_t get_offset_of_m_nCurShowedRewardNum_21() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_nCurShowedRewardNum_21)); }
	inline int32_t get_m_nCurShowedRewardNum_21() const { return ___m_nCurShowedRewardNum_21; }
	inline int32_t* get_address_of_m_nCurShowedRewardNum_21() { return &___m_nCurShowedRewardNum_21; }
	inline void set_m_nCurShowedRewardNum_21(int32_t value)
	{
		___m_nCurShowedRewardNum_21 = value;
	}

	inline static int32_t get_offset_of_m_fRewardCounterStartPos_22() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_fRewardCounterStartPos_22)); }
	inline float get_m_fRewardCounterStartPos_22() const { return ___m_fRewardCounterStartPos_22; }
	inline float* get_address_of_m_fRewardCounterStartPos_22() { return &___m_fRewardCounterStartPos_22; }
	inline void set_m_fRewardCounterStartPos_22(float value)
	{
		___m_fRewardCounterStartPos_22 = value;
	}

	inline static int32_t get_offset_of_m_fRewardCounterEndPos_23() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_fRewardCounterEndPos_23)); }
	inline float get_m_fRewardCounterEndPos_23() const { return ___m_fRewardCounterEndPos_23; }
	inline float* get_address_of_m_fRewardCounterEndPos_23() { return &___m_fRewardCounterEndPos_23; }
	inline void set_m_fRewardCounterEndPos_23(float value)
	{
		___m_fRewardCounterEndPos_23 = value;
	}

	inline static int32_t get_offset_of_m_aryRewardShowCounters_24() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_aryRewardShowCounters_24)); }
	inline UIAdventureRewardShowU5BU5D_tB9375AB05FDA93565842B054D44F1C0B321149D2* get_m_aryRewardShowCounters_24() const { return ___m_aryRewardShowCounters_24; }
	inline UIAdventureRewardShowU5BU5D_tB9375AB05FDA93565842B054D44F1C0B321149D2** get_address_of_m_aryRewardShowCounters_24() { return &___m_aryRewardShowCounters_24; }
	inline void set_m_aryRewardShowCounters_24(UIAdventureRewardShowU5BU5D_tB9375AB05FDA93565842B054D44F1C0B321149D2* value)
	{
		___m_aryRewardShowCounters_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryRewardShowCounters_24), value);
	}

	inline static int32_t get_offset_of_m_CurShowingRewardCounter_25() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_CurShowingRewardCounter_25)); }
	inline UIAdventureRewardShow_tC98CDF0A1478D1585512C351DF8B6106FCB4C93F * get_m_CurShowingRewardCounter_25() const { return ___m_CurShowingRewardCounter_25; }
	inline UIAdventureRewardShow_tC98CDF0A1478D1585512C351DF8B6106FCB4C93F ** get_address_of_m_CurShowingRewardCounter_25() { return &___m_CurShowingRewardCounter_25; }
	inline void set_m_CurShowingRewardCounter_25(UIAdventureRewardShow_tC98CDF0A1478D1585512C351DF8B6106FCB4C93F * value)
	{
		___m_CurShowingRewardCounter_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurShowingRewardCounter_25), value);
	}

	inline static int32_t get_offset_of_m_fShowRewardWaitingTime_26() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_fShowRewardWaitingTime_26)); }
	inline float get_m_fShowRewardWaitingTime_26() const { return ___m_fShowRewardWaitingTime_26; }
	inline float* get_address_of_m_fShowRewardWaitingTime_26() { return &___m_fShowRewardWaitingTime_26; }
	inline void set_m_fShowRewardWaitingTime_26(float value)
	{
		___m_fShowRewardWaitingTime_26 = value;
	}

	inline static int32_t get_offset_of_m_fShowRewardMovingTime_27() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_fShowRewardMovingTime_27)); }
	inline float get_m_fShowRewardMovingTime_27() const { return ___m_fShowRewardMovingTime_27; }
	inline float* get_address_of_m_fShowRewardMovingTime_27() { return &___m_fShowRewardMovingTime_27; }
	inline void set_m_fShowRewardMovingTime_27(float value)
	{
		___m_fShowRewardMovingTime_27 = value;
	}

	inline static int32_t get_offset_of_m_fShowRewardMovingSpeed_28() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_fShowRewardMovingSpeed_28)); }
	inline float get_m_fShowRewardMovingSpeed_28() const { return ___m_fShowRewardMovingSpeed_28; }
	inline float* get_address_of_m_fShowRewardMovingSpeed_28() { return &___m_fShowRewardMovingSpeed_28; }
	inline void set_m_fShowRewardMovingSpeed_28(float value)
	{
		___m_fShowRewardMovingSpeed_28 = value;
	}

	inline static int32_t get_offset_of_m_fShowRewardScalingSpeed_29() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_fShowRewardScalingSpeed_29)); }
	inline float get_m_fShowRewardScalingSpeed_29() const { return ___m_fShowRewardScalingSpeed_29; }
	inline float* get_address_of_m_fShowRewardScalingSpeed_29() { return &___m_fShowRewardScalingSpeed_29; }
	inline void set_m_fShowRewardScalingSpeed_29(float value)
	{
		___m_fShowRewardScalingSpeed_29 = value;
	}

	inline static int32_t get_offset_of_m_bShowingAdventurePanel_30() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_bShowingAdventurePanel_30)); }
	inline bool get_m_bShowingAdventurePanel_30() const { return ___m_bShowingAdventurePanel_30; }
	inline bool* get_address_of_m_bShowingAdventurePanel_30() { return &___m_bShowingAdventurePanel_30; }
	inline void set_m_bShowingAdventurePanel_30(bool value)
	{
		___m_bShowingAdventurePanel_30 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_31() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_fTimeElapse_31)); }
	inline float get_m_fTimeElapse_31() const { return ___m_fTimeElapse_31; }
	inline float* get_address_of_m_fTimeElapse_31() { return &___m_fTimeElapse_31; }
	inline void set_m_fTimeElapse_31(float value)
	{
		___m_fTimeElapse_31 = value;
	}

	inline static int32_t get_offset_of_m_fShowRewardTimeElapse_32() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E, ___m_fShowRewardTimeElapse_32)); }
	inline float get_m_fShowRewardTimeElapse_32() const { return ___m_fShowRewardTimeElapse_32; }
	inline float* get_address_of_m_fShowRewardTimeElapse_32() { return &___m_fShowRewardTimeElapse_32; }
	inline void set_m_fShowRewardTimeElapse_32(float value)
	{
		___m_fShowRewardTimeElapse_32 = value;
	}
};

struct AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E_StaticFields
{
public:
	// AdventureManager AdventureManager::s_Instance
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E * ___s_Instance_4;
	// UnityEngine.Vector3 AdventureManager::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_5;
	// UnityEngine.Vector3 AdventureManager::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_6;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E_StaticFields, ___s_Instance_4)); }
	inline AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E * get_s_Instance_4() const { return ___s_Instance_4; }
	inline AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E_StaticFields, ___vecTempPos_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_5 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_6() { return static_cast<int32_t>(offsetof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E_StaticFields, ___vecTempScale_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_6() const { return ___vecTempScale_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_6() { return &___vecTempScale_6; }
	inline void set_vecTempScale_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVENTUREMANAGER_TC7BDA893675D603C9C2733574D9DC4F29B2B430E_H
#ifndef ADVENTUREMANAGER_NEW_T90D8071C513B6888CA09B0F1646C590B7D1D89DA_H
#define ADVENTUREMANAGER_NEW_T90D8071C513B6888CA09B0F1646C590B7D1D89DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdventureManager_New
struct  AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Sprite[] AdventureManager_New::m_aryAdventureQualityBg
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAdventureQualityBg_6;
	// UnityEngine.Color[] AdventureManager_New::m_aryAdventureQualityColor
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___m_aryAdventureQualityColor_7;
	// System.String[] AdventureManager_New::m_aryAdventureName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_aryAdventureName_8;
	// UnityEngine.GameObject AdventureManager_New::_panelAdventure
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelAdventure_9;
	// UnityEngine.GameObject AdventureManager_New::_subpanelSelectAdventure
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____subpanelSelectAdventure_10;
	// UnityEngine.GameObject AdventureManager_New::_subpanelAdventuring
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____subpanelAdventuring_11;
	// UnityEngine.GameObject AdventureManager_New::_subpanelCollect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____subpanelCollect_12;
	// UIAdventureCounter_New AdventureManager_New::_counterAdventuring
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6 * ____counterAdventuring_13;
	// UnityEngine.UI.Text AdventureManager_New::_txtRefreshColddown
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtRefreshColddown_14;
	// UnityEngine.UI.Text AdventureManager_New::_txtWatchAdsColddown
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtWatchAdsColddown_15;
	// UnityEngine.UI.Text AdventureManager_New::_txtWatchAdsReduce
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtWatchAdsReduce_16;
	// UnityEngine.GameObject AdventureManager_New::_containerPlay
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerPlay_17;
	// UnityEngine.UI.Image AdventureManager_New::_imgPlay
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgPlay_18;
	// UnityEngine.UI.Text AdventureManager_New::_txtTimeLeft
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTimeLeft_19;
	// UIAdventureCounter_New[] AdventureManager_New::m_aryAdventureCounters
	UIAdventureCounter_NewU5BU5D_t0E2830AF419E9D9E16E0CA0EAD443DDE753657C2* ___m_aryAdventureCounters_20;
	// UnityEngine.UI.Button AdventureManager_New::_btnCollect
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnCollect_21;
	// UnityEngine.UI.Button AdventureManager_New::_btnWatchAds
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnWatchAds_22;
	// System.Single AdventureManager_New::m_fAdsReduceTime
	float ___m_fAdsReduceTime_24;
	// System.Single AdventureManager_New::m_fAdsColdDownTotalTime
	float ___m_fAdsColdDownTotalTime_25;
	// System.Single AdventureManager_New::m_fRefreshColddownTime
	float ___m_fRefreshColddownTime_26;
	// System.Int32 AdventureManager_New::m_nRealTimeTotalTime
	int32_t ___m_nRealTimeTotalTime_27;
	// System.Int32 AdventureManager_New::m_nRealAdsColdDownTotalTime
	int32_t ___m_nRealAdsColdDownTotalTime_28;
	// System.Int32 AdventureManager_New::m_nRealRefreshColdDownTotalTime
	int32_t ___m_nRealRefreshColdDownTotalTime_29;
	// System.Int32 AdventureManager_New::m_bAdventuring
	int32_t ___m_bAdventuring_30;
	// System.DateTime AdventureManager_New::m_dtAdventureStartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dtAdventureStartTime_31;
	// System.DateTime AdventureManager_New::m_dtRefreshColddownStartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dtRefreshColddownStartTime_32;
	// System.DateTime AdventureManager_New::m_dtWatchAdsColdDownStartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dtWatchAdsColdDownStartTime_33;
	// System.Boolean AdventureManager_New::m_bCollecting
	bool ___m_bCollecting_34;
	// System.Boolean AdventureManager_New::m_bRefreshColddowning
	bool ___m_bRefreshColddowning_35;
	// System.Boolean AdventureManager_New::m_bWatchAdsColddowning
	bool ___m_bWatchAdsColddowning_36;
	// System.Int32 AdventureManager_New::m_nSelectedAdentureCounterIndex
	int32_t ___m_nSelectedAdentureCounterIndex_37;
	// System.Collections.Generic.Dictionary`2<AdventureManager_New/eAdventureQuality,AdventureManager_New/sAdventureConfig> AdventureManager_New::m_dicAdventureConfig
	Dictionary_2_tD008B5F5B379152550E567E215BF4E5779065775 * ___m_dicAdventureConfig_38;
	// System.Collections.Generic.List`1<AdventureManager_New/eAdventureQuality> AdventureManager_New::m_lstProbility
	List_1_t6D1E458C1593DACF4D6FAE5AFEEA855C4AA47AA0 * ___m_lstProbility_39;
	// System.Boolean AdventureManager_New::m_bIsShowingAdventurePanel
	bool ___m_bIsShowingAdventurePanel_40;
	// System.Single AdventureManager_New::m_fTimeElapse
	float ___m_fTimeElapse_41;
	// System.Collections.Generic.List`1<AdventureManager_New/sRewardConfig> AdventureManager_New::lstRewardTotalTemp
	List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * ___lstRewardTotalTemp_42;
	// System.Collections.Generic.List`1<AdventureManager_New/sRewardConfig> AdventureManager_New::lstRewardSelectedTemp
	List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * ___lstRewardSelectedTemp_43;
	// System.Boolean AdventureManager_New::m_bAdventureConfigLoaded
	bool ___m_bAdventureConfigLoaded_44;
	// UnityEngine.UI.Text AdventureManager_New::_txtCollectDesc
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCollectDesc_45;
	// BaseScale AdventureManager_New::_baseScaleAvatarContainer
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____baseScaleAvatarContainer_46;
	// UnityEngine.UI.Image AdventureManager_New::_imgCollectAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCollectAvatar_47;
	// System.Single AdventureManager_New::m_fCollectAvatarShowTime
	float ___m_fCollectAvatarShowTime_48;
	// System.Single AdventureManager_New::m_fCollectAvatarFadeTime
	float ___m_fCollectAvatarFadeTime_49;
	// System.Int32 AdventureManager_New::m_nCollectIndex
	int32_t ___m_nCollectIndex_50;
	// System.Int32 AdventureManager_New::m_bCollectShowStatus
	int32_t ___m_bCollectShowStatus_51;
	// System.Single AdventureManager_New::m_fCollectTimeElapse
	float ___m_fCollectTimeElapse_52;
	// System.Single AdventureManager_New::m_fFadeScaleSpeed
	float ___m_fFadeScaleSpeed_53;

public:
	inline static int32_t get_offset_of_m_aryAdventureQualityBg_6() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_aryAdventureQualityBg_6)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAdventureQualityBg_6() const { return ___m_aryAdventureQualityBg_6; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAdventureQualityBg_6() { return &___m_aryAdventureQualityBg_6; }
	inline void set_m_aryAdventureQualityBg_6(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAdventureQualityBg_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAdventureQualityBg_6), value);
	}

	inline static int32_t get_offset_of_m_aryAdventureQualityColor_7() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_aryAdventureQualityColor_7)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_m_aryAdventureQualityColor_7() const { return ___m_aryAdventureQualityColor_7; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_m_aryAdventureQualityColor_7() { return &___m_aryAdventureQualityColor_7; }
	inline void set_m_aryAdventureQualityColor_7(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___m_aryAdventureQualityColor_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAdventureQualityColor_7), value);
	}

	inline static int32_t get_offset_of_m_aryAdventureName_8() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_aryAdventureName_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_aryAdventureName_8() const { return ___m_aryAdventureName_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_aryAdventureName_8() { return &___m_aryAdventureName_8; }
	inline void set_m_aryAdventureName_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_aryAdventureName_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAdventureName_8), value);
	}

	inline static int32_t get_offset_of__panelAdventure_9() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____panelAdventure_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelAdventure_9() const { return ____panelAdventure_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelAdventure_9() { return &____panelAdventure_9; }
	inline void set__panelAdventure_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelAdventure_9 = value;
		Il2CppCodeGenWriteBarrier((&____panelAdventure_9), value);
	}

	inline static int32_t get_offset_of__subpanelSelectAdventure_10() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____subpanelSelectAdventure_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__subpanelSelectAdventure_10() const { return ____subpanelSelectAdventure_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__subpanelSelectAdventure_10() { return &____subpanelSelectAdventure_10; }
	inline void set__subpanelSelectAdventure_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____subpanelSelectAdventure_10 = value;
		Il2CppCodeGenWriteBarrier((&____subpanelSelectAdventure_10), value);
	}

	inline static int32_t get_offset_of__subpanelAdventuring_11() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____subpanelAdventuring_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__subpanelAdventuring_11() const { return ____subpanelAdventuring_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__subpanelAdventuring_11() { return &____subpanelAdventuring_11; }
	inline void set__subpanelAdventuring_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____subpanelAdventuring_11 = value;
		Il2CppCodeGenWriteBarrier((&____subpanelAdventuring_11), value);
	}

	inline static int32_t get_offset_of__subpanelCollect_12() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____subpanelCollect_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__subpanelCollect_12() const { return ____subpanelCollect_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__subpanelCollect_12() { return &____subpanelCollect_12; }
	inline void set__subpanelCollect_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____subpanelCollect_12 = value;
		Il2CppCodeGenWriteBarrier((&____subpanelCollect_12), value);
	}

	inline static int32_t get_offset_of__counterAdventuring_13() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____counterAdventuring_13)); }
	inline UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6 * get__counterAdventuring_13() const { return ____counterAdventuring_13; }
	inline UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6 ** get_address_of__counterAdventuring_13() { return &____counterAdventuring_13; }
	inline void set__counterAdventuring_13(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6 * value)
	{
		____counterAdventuring_13 = value;
		Il2CppCodeGenWriteBarrier((&____counterAdventuring_13), value);
	}

	inline static int32_t get_offset_of__txtRefreshColddown_14() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____txtRefreshColddown_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtRefreshColddown_14() const { return ____txtRefreshColddown_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtRefreshColddown_14() { return &____txtRefreshColddown_14; }
	inline void set__txtRefreshColddown_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtRefreshColddown_14 = value;
		Il2CppCodeGenWriteBarrier((&____txtRefreshColddown_14), value);
	}

	inline static int32_t get_offset_of__txtWatchAdsColddown_15() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____txtWatchAdsColddown_15)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtWatchAdsColddown_15() const { return ____txtWatchAdsColddown_15; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtWatchAdsColddown_15() { return &____txtWatchAdsColddown_15; }
	inline void set__txtWatchAdsColddown_15(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtWatchAdsColddown_15 = value;
		Il2CppCodeGenWriteBarrier((&____txtWatchAdsColddown_15), value);
	}

	inline static int32_t get_offset_of__txtWatchAdsReduce_16() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____txtWatchAdsReduce_16)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtWatchAdsReduce_16() const { return ____txtWatchAdsReduce_16; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtWatchAdsReduce_16() { return &____txtWatchAdsReduce_16; }
	inline void set__txtWatchAdsReduce_16(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtWatchAdsReduce_16 = value;
		Il2CppCodeGenWriteBarrier((&____txtWatchAdsReduce_16), value);
	}

	inline static int32_t get_offset_of__containerPlay_17() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____containerPlay_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerPlay_17() const { return ____containerPlay_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerPlay_17() { return &____containerPlay_17; }
	inline void set__containerPlay_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerPlay_17 = value;
		Il2CppCodeGenWriteBarrier((&____containerPlay_17), value);
	}

	inline static int32_t get_offset_of__imgPlay_18() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____imgPlay_18)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgPlay_18() const { return ____imgPlay_18; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgPlay_18() { return &____imgPlay_18; }
	inline void set__imgPlay_18(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgPlay_18 = value;
		Il2CppCodeGenWriteBarrier((&____imgPlay_18), value);
	}

	inline static int32_t get_offset_of__txtTimeLeft_19() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____txtTimeLeft_19)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTimeLeft_19() const { return ____txtTimeLeft_19; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTimeLeft_19() { return &____txtTimeLeft_19; }
	inline void set__txtTimeLeft_19(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTimeLeft_19 = value;
		Il2CppCodeGenWriteBarrier((&____txtTimeLeft_19), value);
	}

	inline static int32_t get_offset_of_m_aryAdventureCounters_20() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_aryAdventureCounters_20)); }
	inline UIAdventureCounter_NewU5BU5D_t0E2830AF419E9D9E16E0CA0EAD443DDE753657C2* get_m_aryAdventureCounters_20() const { return ___m_aryAdventureCounters_20; }
	inline UIAdventureCounter_NewU5BU5D_t0E2830AF419E9D9E16E0CA0EAD443DDE753657C2** get_address_of_m_aryAdventureCounters_20() { return &___m_aryAdventureCounters_20; }
	inline void set_m_aryAdventureCounters_20(UIAdventureCounter_NewU5BU5D_t0E2830AF419E9D9E16E0CA0EAD443DDE753657C2* value)
	{
		___m_aryAdventureCounters_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAdventureCounters_20), value);
	}

	inline static int32_t get_offset_of__btnCollect_21() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____btnCollect_21)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnCollect_21() const { return ____btnCollect_21; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnCollect_21() { return &____btnCollect_21; }
	inline void set__btnCollect_21(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnCollect_21 = value;
		Il2CppCodeGenWriteBarrier((&____btnCollect_21), value);
	}

	inline static int32_t get_offset_of__btnWatchAds_22() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____btnWatchAds_22)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnWatchAds_22() const { return ____btnWatchAds_22; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnWatchAds_22() { return &____btnWatchAds_22; }
	inline void set__btnWatchAds_22(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnWatchAds_22 = value;
		Il2CppCodeGenWriteBarrier((&____btnWatchAds_22), value);
	}

	inline static int32_t get_offset_of_m_fAdsReduceTime_24() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_fAdsReduceTime_24)); }
	inline float get_m_fAdsReduceTime_24() const { return ___m_fAdsReduceTime_24; }
	inline float* get_address_of_m_fAdsReduceTime_24() { return &___m_fAdsReduceTime_24; }
	inline void set_m_fAdsReduceTime_24(float value)
	{
		___m_fAdsReduceTime_24 = value;
	}

	inline static int32_t get_offset_of_m_fAdsColdDownTotalTime_25() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_fAdsColdDownTotalTime_25)); }
	inline float get_m_fAdsColdDownTotalTime_25() const { return ___m_fAdsColdDownTotalTime_25; }
	inline float* get_address_of_m_fAdsColdDownTotalTime_25() { return &___m_fAdsColdDownTotalTime_25; }
	inline void set_m_fAdsColdDownTotalTime_25(float value)
	{
		___m_fAdsColdDownTotalTime_25 = value;
	}

	inline static int32_t get_offset_of_m_fRefreshColddownTime_26() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_fRefreshColddownTime_26)); }
	inline float get_m_fRefreshColddownTime_26() const { return ___m_fRefreshColddownTime_26; }
	inline float* get_address_of_m_fRefreshColddownTime_26() { return &___m_fRefreshColddownTime_26; }
	inline void set_m_fRefreshColddownTime_26(float value)
	{
		___m_fRefreshColddownTime_26 = value;
	}

	inline static int32_t get_offset_of_m_nRealTimeTotalTime_27() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_nRealTimeTotalTime_27)); }
	inline int32_t get_m_nRealTimeTotalTime_27() const { return ___m_nRealTimeTotalTime_27; }
	inline int32_t* get_address_of_m_nRealTimeTotalTime_27() { return &___m_nRealTimeTotalTime_27; }
	inline void set_m_nRealTimeTotalTime_27(int32_t value)
	{
		___m_nRealTimeTotalTime_27 = value;
	}

	inline static int32_t get_offset_of_m_nRealAdsColdDownTotalTime_28() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_nRealAdsColdDownTotalTime_28)); }
	inline int32_t get_m_nRealAdsColdDownTotalTime_28() const { return ___m_nRealAdsColdDownTotalTime_28; }
	inline int32_t* get_address_of_m_nRealAdsColdDownTotalTime_28() { return &___m_nRealAdsColdDownTotalTime_28; }
	inline void set_m_nRealAdsColdDownTotalTime_28(int32_t value)
	{
		___m_nRealAdsColdDownTotalTime_28 = value;
	}

	inline static int32_t get_offset_of_m_nRealRefreshColdDownTotalTime_29() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_nRealRefreshColdDownTotalTime_29)); }
	inline int32_t get_m_nRealRefreshColdDownTotalTime_29() const { return ___m_nRealRefreshColdDownTotalTime_29; }
	inline int32_t* get_address_of_m_nRealRefreshColdDownTotalTime_29() { return &___m_nRealRefreshColdDownTotalTime_29; }
	inline void set_m_nRealRefreshColdDownTotalTime_29(int32_t value)
	{
		___m_nRealRefreshColdDownTotalTime_29 = value;
	}

	inline static int32_t get_offset_of_m_bAdventuring_30() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_bAdventuring_30)); }
	inline int32_t get_m_bAdventuring_30() const { return ___m_bAdventuring_30; }
	inline int32_t* get_address_of_m_bAdventuring_30() { return &___m_bAdventuring_30; }
	inline void set_m_bAdventuring_30(int32_t value)
	{
		___m_bAdventuring_30 = value;
	}

	inline static int32_t get_offset_of_m_dtAdventureStartTime_31() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_dtAdventureStartTime_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dtAdventureStartTime_31() const { return ___m_dtAdventureStartTime_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dtAdventureStartTime_31() { return &___m_dtAdventureStartTime_31; }
	inline void set_m_dtAdventureStartTime_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dtAdventureStartTime_31 = value;
	}

	inline static int32_t get_offset_of_m_dtRefreshColddownStartTime_32() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_dtRefreshColddownStartTime_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dtRefreshColddownStartTime_32() const { return ___m_dtRefreshColddownStartTime_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dtRefreshColddownStartTime_32() { return &___m_dtRefreshColddownStartTime_32; }
	inline void set_m_dtRefreshColddownStartTime_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dtRefreshColddownStartTime_32 = value;
	}

	inline static int32_t get_offset_of_m_dtWatchAdsColdDownStartTime_33() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_dtWatchAdsColdDownStartTime_33)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dtWatchAdsColdDownStartTime_33() const { return ___m_dtWatchAdsColdDownStartTime_33; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dtWatchAdsColdDownStartTime_33() { return &___m_dtWatchAdsColdDownStartTime_33; }
	inline void set_m_dtWatchAdsColdDownStartTime_33(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dtWatchAdsColdDownStartTime_33 = value;
	}

	inline static int32_t get_offset_of_m_bCollecting_34() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_bCollecting_34)); }
	inline bool get_m_bCollecting_34() const { return ___m_bCollecting_34; }
	inline bool* get_address_of_m_bCollecting_34() { return &___m_bCollecting_34; }
	inline void set_m_bCollecting_34(bool value)
	{
		___m_bCollecting_34 = value;
	}

	inline static int32_t get_offset_of_m_bRefreshColddowning_35() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_bRefreshColddowning_35)); }
	inline bool get_m_bRefreshColddowning_35() const { return ___m_bRefreshColddowning_35; }
	inline bool* get_address_of_m_bRefreshColddowning_35() { return &___m_bRefreshColddowning_35; }
	inline void set_m_bRefreshColddowning_35(bool value)
	{
		___m_bRefreshColddowning_35 = value;
	}

	inline static int32_t get_offset_of_m_bWatchAdsColddowning_36() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_bWatchAdsColddowning_36)); }
	inline bool get_m_bWatchAdsColddowning_36() const { return ___m_bWatchAdsColddowning_36; }
	inline bool* get_address_of_m_bWatchAdsColddowning_36() { return &___m_bWatchAdsColddowning_36; }
	inline void set_m_bWatchAdsColddowning_36(bool value)
	{
		___m_bWatchAdsColddowning_36 = value;
	}

	inline static int32_t get_offset_of_m_nSelectedAdentureCounterIndex_37() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_nSelectedAdentureCounterIndex_37)); }
	inline int32_t get_m_nSelectedAdentureCounterIndex_37() const { return ___m_nSelectedAdentureCounterIndex_37; }
	inline int32_t* get_address_of_m_nSelectedAdentureCounterIndex_37() { return &___m_nSelectedAdentureCounterIndex_37; }
	inline void set_m_nSelectedAdentureCounterIndex_37(int32_t value)
	{
		___m_nSelectedAdentureCounterIndex_37 = value;
	}

	inline static int32_t get_offset_of_m_dicAdventureConfig_38() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_dicAdventureConfig_38)); }
	inline Dictionary_2_tD008B5F5B379152550E567E215BF4E5779065775 * get_m_dicAdventureConfig_38() const { return ___m_dicAdventureConfig_38; }
	inline Dictionary_2_tD008B5F5B379152550E567E215BF4E5779065775 ** get_address_of_m_dicAdventureConfig_38() { return &___m_dicAdventureConfig_38; }
	inline void set_m_dicAdventureConfig_38(Dictionary_2_tD008B5F5B379152550E567E215BF4E5779065775 * value)
	{
		___m_dicAdventureConfig_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAdventureConfig_38), value);
	}

	inline static int32_t get_offset_of_m_lstProbility_39() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_lstProbility_39)); }
	inline List_1_t6D1E458C1593DACF4D6FAE5AFEEA855C4AA47AA0 * get_m_lstProbility_39() const { return ___m_lstProbility_39; }
	inline List_1_t6D1E458C1593DACF4D6FAE5AFEEA855C4AA47AA0 ** get_address_of_m_lstProbility_39() { return &___m_lstProbility_39; }
	inline void set_m_lstProbility_39(List_1_t6D1E458C1593DACF4D6FAE5AFEEA855C4AA47AA0 * value)
	{
		___m_lstProbility_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstProbility_39), value);
	}

	inline static int32_t get_offset_of_m_bIsShowingAdventurePanel_40() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_bIsShowingAdventurePanel_40)); }
	inline bool get_m_bIsShowingAdventurePanel_40() const { return ___m_bIsShowingAdventurePanel_40; }
	inline bool* get_address_of_m_bIsShowingAdventurePanel_40() { return &___m_bIsShowingAdventurePanel_40; }
	inline void set_m_bIsShowingAdventurePanel_40(bool value)
	{
		___m_bIsShowingAdventurePanel_40 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_41() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_fTimeElapse_41)); }
	inline float get_m_fTimeElapse_41() const { return ___m_fTimeElapse_41; }
	inline float* get_address_of_m_fTimeElapse_41() { return &___m_fTimeElapse_41; }
	inline void set_m_fTimeElapse_41(float value)
	{
		___m_fTimeElapse_41 = value;
	}

	inline static int32_t get_offset_of_lstRewardTotalTemp_42() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___lstRewardTotalTemp_42)); }
	inline List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * get_lstRewardTotalTemp_42() const { return ___lstRewardTotalTemp_42; }
	inline List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F ** get_address_of_lstRewardTotalTemp_42() { return &___lstRewardTotalTemp_42; }
	inline void set_lstRewardTotalTemp_42(List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * value)
	{
		___lstRewardTotalTemp_42 = value;
		Il2CppCodeGenWriteBarrier((&___lstRewardTotalTemp_42), value);
	}

	inline static int32_t get_offset_of_lstRewardSelectedTemp_43() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___lstRewardSelectedTemp_43)); }
	inline List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * get_lstRewardSelectedTemp_43() const { return ___lstRewardSelectedTemp_43; }
	inline List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F ** get_address_of_lstRewardSelectedTemp_43() { return &___lstRewardSelectedTemp_43; }
	inline void set_lstRewardSelectedTemp_43(List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * value)
	{
		___lstRewardSelectedTemp_43 = value;
		Il2CppCodeGenWriteBarrier((&___lstRewardSelectedTemp_43), value);
	}

	inline static int32_t get_offset_of_m_bAdventureConfigLoaded_44() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_bAdventureConfigLoaded_44)); }
	inline bool get_m_bAdventureConfigLoaded_44() const { return ___m_bAdventureConfigLoaded_44; }
	inline bool* get_address_of_m_bAdventureConfigLoaded_44() { return &___m_bAdventureConfigLoaded_44; }
	inline void set_m_bAdventureConfigLoaded_44(bool value)
	{
		___m_bAdventureConfigLoaded_44 = value;
	}

	inline static int32_t get_offset_of__txtCollectDesc_45() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____txtCollectDesc_45)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCollectDesc_45() const { return ____txtCollectDesc_45; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCollectDesc_45() { return &____txtCollectDesc_45; }
	inline void set__txtCollectDesc_45(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCollectDesc_45 = value;
		Il2CppCodeGenWriteBarrier((&____txtCollectDesc_45), value);
	}

	inline static int32_t get_offset_of__baseScaleAvatarContainer_46() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____baseScaleAvatarContainer_46)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__baseScaleAvatarContainer_46() const { return ____baseScaleAvatarContainer_46; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__baseScaleAvatarContainer_46() { return &____baseScaleAvatarContainer_46; }
	inline void set__baseScaleAvatarContainer_46(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____baseScaleAvatarContainer_46 = value;
		Il2CppCodeGenWriteBarrier((&____baseScaleAvatarContainer_46), value);
	}

	inline static int32_t get_offset_of__imgCollectAvatar_47() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ____imgCollectAvatar_47)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCollectAvatar_47() const { return ____imgCollectAvatar_47; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCollectAvatar_47() { return &____imgCollectAvatar_47; }
	inline void set__imgCollectAvatar_47(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCollectAvatar_47 = value;
		Il2CppCodeGenWriteBarrier((&____imgCollectAvatar_47), value);
	}

	inline static int32_t get_offset_of_m_fCollectAvatarShowTime_48() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_fCollectAvatarShowTime_48)); }
	inline float get_m_fCollectAvatarShowTime_48() const { return ___m_fCollectAvatarShowTime_48; }
	inline float* get_address_of_m_fCollectAvatarShowTime_48() { return &___m_fCollectAvatarShowTime_48; }
	inline void set_m_fCollectAvatarShowTime_48(float value)
	{
		___m_fCollectAvatarShowTime_48 = value;
	}

	inline static int32_t get_offset_of_m_fCollectAvatarFadeTime_49() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_fCollectAvatarFadeTime_49)); }
	inline float get_m_fCollectAvatarFadeTime_49() const { return ___m_fCollectAvatarFadeTime_49; }
	inline float* get_address_of_m_fCollectAvatarFadeTime_49() { return &___m_fCollectAvatarFadeTime_49; }
	inline void set_m_fCollectAvatarFadeTime_49(float value)
	{
		___m_fCollectAvatarFadeTime_49 = value;
	}

	inline static int32_t get_offset_of_m_nCollectIndex_50() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_nCollectIndex_50)); }
	inline int32_t get_m_nCollectIndex_50() const { return ___m_nCollectIndex_50; }
	inline int32_t* get_address_of_m_nCollectIndex_50() { return &___m_nCollectIndex_50; }
	inline void set_m_nCollectIndex_50(int32_t value)
	{
		___m_nCollectIndex_50 = value;
	}

	inline static int32_t get_offset_of_m_bCollectShowStatus_51() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_bCollectShowStatus_51)); }
	inline int32_t get_m_bCollectShowStatus_51() const { return ___m_bCollectShowStatus_51; }
	inline int32_t* get_address_of_m_bCollectShowStatus_51() { return &___m_bCollectShowStatus_51; }
	inline void set_m_bCollectShowStatus_51(int32_t value)
	{
		___m_bCollectShowStatus_51 = value;
	}

	inline static int32_t get_offset_of_m_fCollectTimeElapse_52() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_fCollectTimeElapse_52)); }
	inline float get_m_fCollectTimeElapse_52() const { return ___m_fCollectTimeElapse_52; }
	inline float* get_address_of_m_fCollectTimeElapse_52() { return &___m_fCollectTimeElapse_52; }
	inline void set_m_fCollectTimeElapse_52(float value)
	{
		___m_fCollectTimeElapse_52 = value;
	}

	inline static int32_t get_offset_of_m_fFadeScaleSpeed_53() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA, ___m_fFadeScaleSpeed_53)); }
	inline float get_m_fFadeScaleSpeed_53() const { return ___m_fFadeScaleSpeed_53; }
	inline float* get_address_of_m_fFadeScaleSpeed_53() { return &___m_fFadeScaleSpeed_53; }
	inline void set_m_fFadeScaleSpeed_53(float value)
	{
		___m_fFadeScaleSpeed_53 = value;
	}
};

struct AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA_StaticFields
{
public:
	// AdventureManager_New AdventureManager_New::s_Instance
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA * ___s_Instance_4;
	// UnityEngine.Vector3 AdventureManager_New::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_5;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA_StaticFields, ___s_Instance_4)); }
	inline AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA * get_s_Instance_4() const { return ___s_Instance_4; }
	inline AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA_StaticFields, ___vecTempScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVENTUREMANAGER_NEW_T90D8071C513B6888CA09B0F1646C590B7D1D89DA_H
#ifndef INTENSEVIBRATION_TD697DD1DE3CF0E007232BB307CA8479A6F067B70_H
#define INTENSEVIBRATION_TD697DD1DE3CF0E007232BB307CA8479A6F067B70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IntenseVibration
struct  IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single IntenseVibration::mVibrateTimeLeft
	float ___mVibrateTimeLeft_6;
	// System.Collections.Generic.List`1<System.Int64[]> IntenseVibration::mPatterns
	List_1_t64142247412666BB650346DC5C873E6B6BE631A5 * ___mPatterns_7;

public:
	inline static int32_t get_offset_of_mVibrateTimeLeft_6() { return static_cast<int32_t>(offsetof(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70, ___mVibrateTimeLeft_6)); }
	inline float get_mVibrateTimeLeft_6() const { return ___mVibrateTimeLeft_6; }
	inline float* get_address_of_mVibrateTimeLeft_6() { return &___mVibrateTimeLeft_6; }
	inline void set_mVibrateTimeLeft_6(float value)
	{
		___mVibrateTimeLeft_6 = value;
	}

	inline static int32_t get_offset_of_mPatterns_7() { return static_cast<int32_t>(offsetof(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70, ___mPatterns_7)); }
	inline List_1_t64142247412666BB650346DC5C873E6B6BE631A5 * get_mPatterns_7() const { return ___mPatterns_7; }
	inline List_1_t64142247412666BB650346DC5C873E6B6BE631A5 ** get_address_of_mPatterns_7() { return &___mPatterns_7; }
	inline void set_mPatterns_7(List_1_t64142247412666BB650346DC5C873E6B6BE631A5 * value)
	{
		___mPatterns_7 = value;
		Il2CppCodeGenWriteBarrier((&___mPatterns_7), value);
	}
};

struct IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_StaticFields
{
public:
	// IntenseVibration IntenseVibration::mInstance
	IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * ___mInstance_4;
	// UnityEngine.AndroidJavaObject IntenseVibration::mJVibrator
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ___mJVibrator_5;

public:
	inline static int32_t get_offset_of_mInstance_4() { return static_cast<int32_t>(offsetof(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_StaticFields, ___mInstance_4)); }
	inline IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * get_mInstance_4() const { return ___mInstance_4; }
	inline IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 ** get_address_of_mInstance_4() { return &___mInstance_4; }
	inline void set_mInstance_4(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70 * value)
	{
		___mInstance_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_4), value);
	}

	inline static int32_t get_offset_of_mJVibrator_5() { return static_cast<int32_t>(offsetof(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_StaticFields, ___mJVibrator_5)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get_mJVibrator_5() const { return ___mJVibrator_5; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of_mJVibrator_5() { return &___mJVibrator_5; }
	inline void set_mJVibrator_5(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		___mJVibrator_5 = value;
		Il2CppCodeGenWriteBarrier((&___mJVibrator_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTENSEVIBRATION_TD697DD1DE3CF0E007232BB307CA8479A6F067B70_H
#ifndef NICEVIBRATIONSDEMOMANAGER_T2F48E6AE3AC20158BC1CF621740B795B6BF1D07E_H
#define NICEVIBRATIONSDEMOMANAGER_T2F48E6AE3AC20158BC1CF621740B795B6BF1D07E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoreMountains.NiceVibrations.NiceVibrationsDemoManager
struct  NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text MoreMountains.NiceVibrations.NiceVibrationsDemoManager::DebugTextBox
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___DebugTextBox_4;
	// System.String MoreMountains.NiceVibrations.NiceVibrationsDemoManager::_debugString
	String_t* ____debugString_5;
	// System.String MoreMountains.NiceVibrations.NiceVibrationsDemoManager::_platformString
	String_t* ____platformString_6;

public:
	inline static int32_t get_offset_of_DebugTextBox_4() { return static_cast<int32_t>(offsetof(NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E, ___DebugTextBox_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_DebugTextBox_4() const { return ___DebugTextBox_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_DebugTextBox_4() { return &___DebugTextBox_4; }
	inline void set_DebugTextBox_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___DebugTextBox_4 = value;
		Il2CppCodeGenWriteBarrier((&___DebugTextBox_4), value);
	}

	inline static int32_t get_offset_of__debugString_5() { return static_cast<int32_t>(offsetof(NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E, ____debugString_5)); }
	inline String_t* get__debugString_5() const { return ____debugString_5; }
	inline String_t** get_address_of__debugString_5() { return &____debugString_5; }
	inline void set__debugString_5(String_t* value)
	{
		____debugString_5 = value;
		Il2CppCodeGenWriteBarrier((&____debugString_5), value);
	}

	inline static int32_t get_offset_of__platformString_6() { return static_cast<int32_t>(offsetof(NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E, ____platformString_6)); }
	inline String_t* get__platformString_6() const { return ____platformString_6; }
	inline String_t** get_address_of__platformString_6() { return &____platformString_6; }
	inline void set__platformString_6(String_t* value)
	{
		____platformString_6 = value;
		Il2CppCodeGenWriteBarrier((&____platformString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NICEVIBRATIONSDEMOMANAGER_T2F48E6AE3AC20158BC1CF621740B795B6BF1D07E_H
#ifndef TESTINTENSEVIBRATION_TB3B9456153E01062B1FCD9F20943D0897E283741_H
#define TESTINTENSEVIBRATION_TB3B9456153E01062B1FCD9F20943D0897E283741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestIntenseVibration
struct  TestIntenseVibration_tB3B9456153E01062B1FCD9F20943D0897E283741  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTINTENSEVIBRATION_TB3B9456153E01062B1FCD9F20943D0897E283741_H
#ifndef UIADVENTURECOUNTER_NEW_TED6A10048A58F582BA0350510888860A539ABAF6_H
#define UIADVENTURECOUNTER_NEW_TED6A10048A58F582BA0350510888860A539ABAF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAdventureCounter_New
struct  UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UIAdventureRewardCounter[] UIAdventureCounter_New::m_aryRewardCounters
	UIAdventureRewardCounterU5BU5D_t7D81D6F2339C91763E5D69F58B2E6D3CFC55CAE5* ___m_aryRewardCounters_4;
	// UnityEngine.UI.Text UIAdventureCounter_New::_txtTitle
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTitle_5;
	// UnityEngine.UI.Text UIAdventureCounter_New::_txtTotalTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTotalTime_6;
	// UnityEngine.UI.Text UIAdventureCounter_New::_txtName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtName_7;
	// UnityEngine.UI.Text UIAdventureCounter_New::_txtReduceTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtReduceTime_8;
	// UnityEngine.UI.Text UIAdventureCounter_New::_txtReduceTime_ColdDown
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtReduceTime_ColdDown_9;
	// UnityEngine.GameObject UIAdventureCounter_New::_containerPlay
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerPlay_10;
	// UnityEngine.UI.Image UIAdventureCounter_New::_imgQualityBg
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgQualityBg_11;
	// AdventureManager_New/sAdventureConfig UIAdventureCounter_New::m_GeneralConfig
	sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1  ___m_GeneralConfig_12;
	// AdventureManager_New/sRewardConfig UIAdventureCounter_New::m_MainRewardConfig
	sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80  ___m_MainRewardConfig_13;
	// System.Collections.Generic.List`1<AdventureManager_New/sRewardConfig> UIAdventureCounter_New::m_lstViceRewardConfig
	List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * ___m_lstViceRewardConfig_14;
	// System.Int32 UIAdventureCounter_New::m_nViceNum
	int32_t ___m_nViceNum_15;
	// System.Int32 UIAdventureCounter_New::m_nCounterIndex
	int32_t ___m_nCounterIndex_16;

public:
	inline static int32_t get_offset_of_m_aryRewardCounters_4() { return static_cast<int32_t>(offsetof(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6, ___m_aryRewardCounters_4)); }
	inline UIAdventureRewardCounterU5BU5D_t7D81D6F2339C91763E5D69F58B2E6D3CFC55CAE5* get_m_aryRewardCounters_4() const { return ___m_aryRewardCounters_4; }
	inline UIAdventureRewardCounterU5BU5D_t7D81D6F2339C91763E5D69F58B2E6D3CFC55CAE5** get_address_of_m_aryRewardCounters_4() { return &___m_aryRewardCounters_4; }
	inline void set_m_aryRewardCounters_4(UIAdventureRewardCounterU5BU5D_t7D81D6F2339C91763E5D69F58B2E6D3CFC55CAE5* value)
	{
		___m_aryRewardCounters_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryRewardCounters_4), value);
	}

	inline static int32_t get_offset_of__txtTitle_5() { return static_cast<int32_t>(offsetof(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6, ____txtTitle_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTitle_5() const { return ____txtTitle_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTitle_5() { return &____txtTitle_5; }
	inline void set__txtTitle_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTitle_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtTitle_5), value);
	}

	inline static int32_t get_offset_of__txtTotalTime_6() { return static_cast<int32_t>(offsetof(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6, ____txtTotalTime_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTotalTime_6() const { return ____txtTotalTime_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTotalTime_6() { return &____txtTotalTime_6; }
	inline void set__txtTotalTime_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTotalTime_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtTotalTime_6), value);
	}

	inline static int32_t get_offset_of__txtName_7() { return static_cast<int32_t>(offsetof(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6, ____txtName_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtName_7() const { return ____txtName_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtName_7() { return &____txtName_7; }
	inline void set__txtName_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtName_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtName_7), value);
	}

	inline static int32_t get_offset_of__txtReduceTime_8() { return static_cast<int32_t>(offsetof(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6, ____txtReduceTime_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtReduceTime_8() const { return ____txtReduceTime_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtReduceTime_8() { return &____txtReduceTime_8; }
	inline void set__txtReduceTime_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtReduceTime_8 = value;
		Il2CppCodeGenWriteBarrier((&____txtReduceTime_8), value);
	}

	inline static int32_t get_offset_of__txtReduceTime_ColdDown_9() { return static_cast<int32_t>(offsetof(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6, ____txtReduceTime_ColdDown_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtReduceTime_ColdDown_9() const { return ____txtReduceTime_ColdDown_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtReduceTime_ColdDown_9() { return &____txtReduceTime_ColdDown_9; }
	inline void set__txtReduceTime_ColdDown_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtReduceTime_ColdDown_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtReduceTime_ColdDown_9), value);
	}

	inline static int32_t get_offset_of__containerPlay_10() { return static_cast<int32_t>(offsetof(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6, ____containerPlay_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerPlay_10() const { return ____containerPlay_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerPlay_10() { return &____containerPlay_10; }
	inline void set__containerPlay_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerPlay_10 = value;
		Il2CppCodeGenWriteBarrier((&____containerPlay_10), value);
	}

	inline static int32_t get_offset_of__imgQualityBg_11() { return static_cast<int32_t>(offsetof(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6, ____imgQualityBg_11)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgQualityBg_11() const { return ____imgQualityBg_11; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgQualityBg_11() { return &____imgQualityBg_11; }
	inline void set__imgQualityBg_11(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgQualityBg_11 = value;
		Il2CppCodeGenWriteBarrier((&____imgQualityBg_11), value);
	}

	inline static int32_t get_offset_of_m_GeneralConfig_12() { return static_cast<int32_t>(offsetof(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6, ___m_GeneralConfig_12)); }
	inline sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1  get_m_GeneralConfig_12() const { return ___m_GeneralConfig_12; }
	inline sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1 * get_address_of_m_GeneralConfig_12() { return &___m_GeneralConfig_12; }
	inline void set_m_GeneralConfig_12(sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1  value)
	{
		___m_GeneralConfig_12 = value;
	}

	inline static int32_t get_offset_of_m_MainRewardConfig_13() { return static_cast<int32_t>(offsetof(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6, ___m_MainRewardConfig_13)); }
	inline sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80  get_m_MainRewardConfig_13() const { return ___m_MainRewardConfig_13; }
	inline sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80 * get_address_of_m_MainRewardConfig_13() { return &___m_MainRewardConfig_13; }
	inline void set_m_MainRewardConfig_13(sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80  value)
	{
		___m_MainRewardConfig_13 = value;
	}

	inline static int32_t get_offset_of_m_lstViceRewardConfig_14() { return static_cast<int32_t>(offsetof(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6, ___m_lstViceRewardConfig_14)); }
	inline List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * get_m_lstViceRewardConfig_14() const { return ___m_lstViceRewardConfig_14; }
	inline List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F ** get_address_of_m_lstViceRewardConfig_14() { return &___m_lstViceRewardConfig_14; }
	inline void set_m_lstViceRewardConfig_14(List_1_t2C1C72D63B479A7E883F8F0988296C82FF7C327F * value)
	{
		___m_lstViceRewardConfig_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstViceRewardConfig_14), value);
	}

	inline static int32_t get_offset_of_m_nViceNum_15() { return static_cast<int32_t>(offsetof(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6, ___m_nViceNum_15)); }
	inline int32_t get_m_nViceNum_15() const { return ___m_nViceNum_15; }
	inline int32_t* get_address_of_m_nViceNum_15() { return &___m_nViceNum_15; }
	inline void set_m_nViceNum_15(int32_t value)
	{
		___m_nViceNum_15 = value;
	}

	inline static int32_t get_offset_of_m_nCounterIndex_16() { return static_cast<int32_t>(offsetof(UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6, ___m_nCounterIndex_16)); }
	inline int32_t get_m_nCounterIndex_16() const { return ___m_nCounterIndex_16; }
	inline int32_t* get_address_of_m_nCounterIndex_16() { return &___m_nCounterIndex_16; }
	inline void set_m_nCounterIndex_16(int32_t value)
	{
		___m_nCounterIndex_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIADVENTURECOUNTER_NEW_TED6A10048A58F582BA0350510888860A539ABAF6_H
#ifndef UIADVENTUREREWARDCOUNTER_T620140F5CBF326FC39B3E3D993019E832EA9CEBE_H
#define UIADVENTUREREWARDCOUNTER_T620140F5CBF326FC39B3E3D993019E832EA9CEBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAdventureRewardCounter
struct  UIAdventureRewardCounter_t620140F5CBF326FC39B3E3D993019E832EA9CEBE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image UIAdventureRewardCounter::_imgAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgAvatar_4;
	// UnityEngine.UI.Text UIAdventureRewardCounter::_txtDesc
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDesc_5;
	// UnityEngine.UI.Text UIAdventureRewardCounter::_txtNum
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtNum_6;

public:
	inline static int32_t get_offset_of__imgAvatar_4() { return static_cast<int32_t>(offsetof(UIAdventureRewardCounter_t620140F5CBF326FC39B3E3D993019E832EA9CEBE, ____imgAvatar_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgAvatar_4() const { return ____imgAvatar_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgAvatar_4() { return &____imgAvatar_4; }
	inline void set__imgAvatar_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgAvatar_4 = value;
		Il2CppCodeGenWriteBarrier((&____imgAvatar_4), value);
	}

	inline static int32_t get_offset_of__txtDesc_5() { return static_cast<int32_t>(offsetof(UIAdventureRewardCounter_t620140F5CBF326FC39B3E3D993019E832EA9CEBE, ____txtDesc_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDesc_5() const { return ____txtDesc_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDesc_5() { return &____txtDesc_5; }
	inline void set__txtDesc_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDesc_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtDesc_5), value);
	}

	inline static int32_t get_offset_of__txtNum_6() { return static_cast<int32_t>(offsetof(UIAdventureRewardCounter_t620140F5CBF326FC39B3E3D993019E832EA9CEBE, ____txtNum_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtNum_6() const { return ____txtNum_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtNum_6() { return &____txtNum_6; }
	inline void set__txtNum_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtNum_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtNum_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIADVENTUREREWARDCOUNTER_T620140F5CBF326FC39B3E3D993019E832EA9CEBE_H
#ifndef ACTIVATIONTRACK_T1EFFCF321B9408DA59AECDF657A609411D041469_H
#define ACTIVATIONTRACK_T1EFFCF321B9408DA59AECDF657A609411D041469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationTrack
struct  ActivationTrack_t1EFFCF321B9408DA59AECDF657A609411D041469  : public TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC
{
public:
	// UnityEngine.Timeline.ActivationTrack/PostPlaybackState UnityEngine.Timeline.ActivationTrack::m_PostPlaybackState
	int32_t ___m_PostPlaybackState_23;
	// UnityEngine.Timeline.ActivationMixerPlayable UnityEngine.Timeline.ActivationTrack::m_ActivationMixer
	ActivationMixerPlayable_t299EC5947DB712650FF6D52B34BE46862B4034E2 * ___m_ActivationMixer_24;

public:
	inline static int32_t get_offset_of_m_PostPlaybackState_23() { return static_cast<int32_t>(offsetof(ActivationTrack_t1EFFCF321B9408DA59AECDF657A609411D041469, ___m_PostPlaybackState_23)); }
	inline int32_t get_m_PostPlaybackState_23() const { return ___m_PostPlaybackState_23; }
	inline int32_t* get_address_of_m_PostPlaybackState_23() { return &___m_PostPlaybackState_23; }
	inline void set_m_PostPlaybackState_23(int32_t value)
	{
		___m_PostPlaybackState_23 = value;
	}

	inline static int32_t get_offset_of_m_ActivationMixer_24() { return static_cast<int32_t>(offsetof(ActivationTrack_t1EFFCF321B9408DA59AECDF657A609411D041469, ___m_ActivationMixer_24)); }
	inline ActivationMixerPlayable_t299EC5947DB712650FF6D52B34BE46862B4034E2 * get_m_ActivationMixer_24() const { return ___m_ActivationMixer_24; }
	inline ActivationMixerPlayable_t299EC5947DB712650FF6D52B34BE46862B4034E2 ** get_address_of_m_ActivationMixer_24() { return &___m_ActivationMixer_24; }
	inline void set_m_ActivationMixer_24(ActivationMixerPlayable_t299EC5947DB712650FF6D52B34BE46862B4034E2 * value)
	{
		___m_ActivationMixer_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActivationMixer_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONTRACK_T1EFFCF321B9408DA59AECDF657A609411D041469_H
#ifndef ANIMATIONTRACK_T1103F3D88654B88259A8C59E7C145826A2B9D162_H
#define ANIMATIONTRACK_T1103F3D88654B88259A8C59E7C145826A2B9D162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationTrack
struct  AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162  : public TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC
{
public:
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.AnimationTrack::m_OpenClipPreExtrapolation
	int32_t ___m_OpenClipPreExtrapolation_23;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.AnimationTrack::m_OpenClipPostExtrapolation
	int32_t ___m_OpenClipPostExtrapolation_24;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_OpenClipOffsetPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_OpenClipOffsetPosition_25;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_OpenClipOffsetEulerAngles
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_OpenClipOffsetEulerAngles_26;
	// System.Double UnityEngine.Timeline.AnimationTrack::m_OpenClipTimeOffset
	double ___m_OpenClipTimeOffset_27;
	// System.Boolean UnityEngine.Timeline.AnimationTrack::m_OpenClipRemoveOffset
	bool ___m_OpenClipRemoveOffset_28;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.AnimationTrack::m_MatchTargetFields
	int32_t ___m_MatchTargetFields_29;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Position_30;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_EulerAngles
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_EulerAngles_31;
	// UnityEngine.AvatarMask UnityEngine.Timeline.AnimationTrack::m_AvatarMask
	AvatarMask_t12E2214B133E61C5CF28DC1E4F6DC2451C75D88A * ___m_AvatarMask_32;
	// System.Boolean UnityEngine.Timeline.AnimationTrack::m_ApplyAvatarMask
	bool ___m_ApplyAvatarMask_33;
	// UnityEngine.Timeline.TrackOffset UnityEngine.Timeline.AnimationTrack::m_TrackOffset
	int32_t ___m_TrackOffset_34;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationTrack::m_OpenClipOffsetRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_OpenClipOffsetRotation_35;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationTrack::m_Rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_Rotation_36;
	// System.Boolean UnityEngine.Timeline.AnimationTrack::m_ApplyOffsets
	bool ___m_ApplyOffsets_37;

public:
	inline static int32_t get_offset_of_m_OpenClipPreExtrapolation_23() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_OpenClipPreExtrapolation_23)); }
	inline int32_t get_m_OpenClipPreExtrapolation_23() const { return ___m_OpenClipPreExtrapolation_23; }
	inline int32_t* get_address_of_m_OpenClipPreExtrapolation_23() { return &___m_OpenClipPreExtrapolation_23; }
	inline void set_m_OpenClipPreExtrapolation_23(int32_t value)
	{
		___m_OpenClipPreExtrapolation_23 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipPostExtrapolation_24() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_OpenClipPostExtrapolation_24)); }
	inline int32_t get_m_OpenClipPostExtrapolation_24() const { return ___m_OpenClipPostExtrapolation_24; }
	inline int32_t* get_address_of_m_OpenClipPostExtrapolation_24() { return &___m_OpenClipPostExtrapolation_24; }
	inline void set_m_OpenClipPostExtrapolation_24(int32_t value)
	{
		___m_OpenClipPostExtrapolation_24 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipOffsetPosition_25() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_OpenClipOffsetPosition_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_OpenClipOffsetPosition_25() const { return ___m_OpenClipOffsetPosition_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_OpenClipOffsetPosition_25() { return &___m_OpenClipOffsetPosition_25; }
	inline void set_m_OpenClipOffsetPosition_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_OpenClipOffsetPosition_25 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipOffsetEulerAngles_26() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_OpenClipOffsetEulerAngles_26)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_OpenClipOffsetEulerAngles_26() const { return ___m_OpenClipOffsetEulerAngles_26; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_OpenClipOffsetEulerAngles_26() { return &___m_OpenClipOffsetEulerAngles_26; }
	inline void set_m_OpenClipOffsetEulerAngles_26(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_OpenClipOffsetEulerAngles_26 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipTimeOffset_27() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_OpenClipTimeOffset_27)); }
	inline double get_m_OpenClipTimeOffset_27() const { return ___m_OpenClipTimeOffset_27; }
	inline double* get_address_of_m_OpenClipTimeOffset_27() { return &___m_OpenClipTimeOffset_27; }
	inline void set_m_OpenClipTimeOffset_27(double value)
	{
		___m_OpenClipTimeOffset_27 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipRemoveOffset_28() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_OpenClipRemoveOffset_28)); }
	inline bool get_m_OpenClipRemoveOffset_28() const { return ___m_OpenClipRemoveOffset_28; }
	inline bool* get_address_of_m_OpenClipRemoveOffset_28() { return &___m_OpenClipRemoveOffset_28; }
	inline void set_m_OpenClipRemoveOffset_28(bool value)
	{
		___m_OpenClipRemoveOffset_28 = value;
	}

	inline static int32_t get_offset_of_m_MatchTargetFields_29() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_MatchTargetFields_29)); }
	inline int32_t get_m_MatchTargetFields_29() const { return ___m_MatchTargetFields_29; }
	inline int32_t* get_address_of_m_MatchTargetFields_29() { return &___m_MatchTargetFields_29; }
	inline void set_m_MatchTargetFields_29(int32_t value)
	{
		___m_MatchTargetFields_29 = value;
	}

	inline static int32_t get_offset_of_m_Position_30() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_Position_30)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Position_30() const { return ___m_Position_30; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Position_30() { return &___m_Position_30; }
	inline void set_m_Position_30(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Position_30 = value;
	}

	inline static int32_t get_offset_of_m_EulerAngles_31() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_EulerAngles_31)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_EulerAngles_31() const { return ___m_EulerAngles_31; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_EulerAngles_31() { return &___m_EulerAngles_31; }
	inline void set_m_EulerAngles_31(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_EulerAngles_31 = value;
	}

	inline static int32_t get_offset_of_m_AvatarMask_32() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_AvatarMask_32)); }
	inline AvatarMask_t12E2214B133E61C5CF28DC1E4F6DC2451C75D88A * get_m_AvatarMask_32() const { return ___m_AvatarMask_32; }
	inline AvatarMask_t12E2214B133E61C5CF28DC1E4F6DC2451C75D88A ** get_address_of_m_AvatarMask_32() { return &___m_AvatarMask_32; }
	inline void set_m_AvatarMask_32(AvatarMask_t12E2214B133E61C5CF28DC1E4F6DC2451C75D88A * value)
	{
		___m_AvatarMask_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_AvatarMask_32), value);
	}

	inline static int32_t get_offset_of_m_ApplyAvatarMask_33() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_ApplyAvatarMask_33)); }
	inline bool get_m_ApplyAvatarMask_33() const { return ___m_ApplyAvatarMask_33; }
	inline bool* get_address_of_m_ApplyAvatarMask_33() { return &___m_ApplyAvatarMask_33; }
	inline void set_m_ApplyAvatarMask_33(bool value)
	{
		___m_ApplyAvatarMask_33 = value;
	}

	inline static int32_t get_offset_of_m_TrackOffset_34() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_TrackOffset_34)); }
	inline int32_t get_m_TrackOffset_34() const { return ___m_TrackOffset_34; }
	inline int32_t* get_address_of_m_TrackOffset_34() { return &___m_TrackOffset_34; }
	inline void set_m_TrackOffset_34(int32_t value)
	{
		___m_TrackOffset_34 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipOffsetRotation_35() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_OpenClipOffsetRotation_35)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_OpenClipOffsetRotation_35() const { return ___m_OpenClipOffsetRotation_35; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_OpenClipOffsetRotation_35() { return &___m_OpenClipOffsetRotation_35; }
	inline void set_m_OpenClipOffsetRotation_35(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_OpenClipOffsetRotation_35 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_36() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_Rotation_36)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_Rotation_36() const { return ___m_Rotation_36; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_Rotation_36() { return &___m_Rotation_36; }
	inline void set_m_Rotation_36(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_Rotation_36 = value;
	}

	inline static int32_t get_offset_of_m_ApplyOffsets_37() { return static_cast<int32_t>(offsetof(AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162, ___m_ApplyOffsets_37)); }
	inline bool get_m_ApplyOffsets_37() const { return ___m_ApplyOffsets_37; }
	inline bool* get_address_of_m_ApplyOffsets_37() { return &___m_ApplyOffsets_37; }
	inline void set_m_ApplyOffsets_37(bool value)
	{
		___m_ApplyOffsets_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTRACK_T1103F3D88654B88259A8C59E7C145826A2B9D162_H
#ifndef AUDIOTRACK_T2A6D40E2647460067ED433025B56D89554743502_H
#define AUDIOTRACK_T2A6D40E2647460067ED433025B56D89554743502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioTrack
struct  AudioTrack_t2A6D40E2647460067ED433025B56D89554743502  : public TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOTRACK_T2A6D40E2647460067ED433025B56D89554743502_H
#ifndef CONTROLTRACK_T4DDF09758220B0908E2932B2D2C47817DA1DE819_H
#define CONTROLTRACK_T4DDF09758220B0908E2932B2D2C47817DA1DE819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ControlTrack
struct  ControlTrack_t4DDF09758220B0908E2932B2D2C47817DA1DE819  : public TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLTRACK_T4DDF09758220B0908E2932B2D2C47817DA1DE819_H
#ifndef GROUPTRACK_T84A0E622EA4B8A122ADD8967B920F6C79670DCF7_H
#define GROUPTRACK_T84A0E622EA4B8A122ADD8967B920F6C79670DCF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.GroupTrack
struct  GroupTrack_t84A0E622EA4B8A122ADD8967B920F6C79670DCF7  : public TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPTRACK_T84A0E622EA4B8A122ADD8967B920F6C79670DCF7_H
#ifndef PLAYABLETRACK_T3D123D2F1485F7C140359B76D3F5DDAB43307396_H
#define PLAYABLETRACK_T3D123D2F1485F7C140359B76D3F5DDAB43307396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.PlayableTrack
struct  PlayableTrack_t3D123D2F1485F7C140359B76D3F5DDAB43307396  : public TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLETRACK_T3D123D2F1485F7C140359B76D3F5DDAB43307396_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (GroupTrack_t84A0E622EA4B8A122ADD8967B920F6C79670DCF7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2602[10] = 
{
	TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F::get_offset_of_m_NextId_4(),
	TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F::get_offset_of_m_Tracks_5(),
	TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F::get_offset_of_m_FixedDuration_6(),
	TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F::get_offset_of_m_CacheOutputTracks_7(),
	TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F::get_offset_of_m_CacheRootTracks_8(),
	TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F::get_offset_of_m_CacheFlattenedTracks_9(),
	TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F::get_offset_of_m_EditorSettings_10(),
	TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F::get_offset_of_m_DurationMode_11(),
	0,
	TimelineAsset_t2FACE300C5EDB28B0750F4AB44CBF2D0E642343F::get_offset_of_m_Version_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (DurationMode_t8AA15A59EC315FA48C475D926B78FB950BD59F92)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2603[3] = 
{
	DurationMode_t8AA15A59EC315FA48C475D926B78FB950BD59F92::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9), -1, sizeof(EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2604[4] = 
{
	EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9_StaticFields::get_offset_of_kMinFps_0(),
	EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9_StaticFields::get_offset_of_kMaxFps_1(),
	EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9_StaticFields::get_offset_of_kDefaultFps_2(),
	EditorSettings_tD45E5C0E7B33972C87CEBF0C62CC6E8DBB59AEB9::get_offset_of_m_Framerate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[8] = 
{
	U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98::get_offset_of_U3CoutputTracksU3E__1_1(),
	U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98::get_offset_of_U24locvar1_2(),
	U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98::get_offset_of_U3CoutputU3E__2_3(),
	U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98::get_offset_of_U24this_4(),
	U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98::get_offset_of_U24disposing_6(),
	U3CU3Ec__Iterator0_t2944832319BAB9BAF1BCCBE6186AC3EBFA7D5E98::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (TrackClipTypeAttribute_t36C1E6CD0C0EBAE99E7DAE0AD1F9630EDCE4FC0B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2606[2] = 
{
	TrackClipTypeAttribute_t36C1E6CD0C0EBAE99E7DAE0AD1F9630EDCE4FC0B::get_offset_of_inspectedType_0(),
	TrackClipTypeAttribute_t36C1E6CD0C0EBAE99E7DAE0AD1F9630EDCE4FC0B::get_offset_of_allowAutoCreate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (NotKeyableAttribute_t9A08F5AF42255B6FC72A98AB694411430CD395DA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (TrackBindingFlags_tDF44A084A5ED972CC8A2D116695916EB95EE7172)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2608[4] = 
{
	TrackBindingFlags_tDF44A084A5ED972CC8A2D116695916EB95EE7172::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (TrackBindingTypeAttribute_t5A3A97B13F678C07B1F43C3EB1C7531C64E85075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2609[2] = 
{
	TrackBindingTypeAttribute_t5A3A97B13F678C07B1F43C3EB1C7531C64E85075::get_offset_of_type_0(),
	TrackBindingTypeAttribute_t5A3A97B13F678C07B1F43C3EB1C7531C64E85075::get_offset_of_flags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (SupportsChildTracksAttribute_tF20FD282A93BCE20932E0A8A030A694D0A8B3E8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2610[2] = 
{
	SupportsChildTracksAttribute_tF20FD282A93BCE20932E0A8A030A694D0A8B3E8D::get_offset_of_childType_0(),
	SupportsChildTracksAttribute_tF20FD282A93BCE20932E0A8A030A694D0A8B3E8D::get_offset_of_levels_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (IgnoreOnPlayableTrackAttribute_tB5C2D6CF60A235822F64E8448220E8D2EA9636F6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A), -1, sizeof(TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2614[31] = 
{
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields::get_offset_of_kDefaultClipCaps_0(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields::get_offset_of_kDefaultClipDurationInSeconds_1(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields::get_offset_of_kTimeScaleMin_2(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields::get_offset_of_kTimeScaleMax_3(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields::get_offset_of_kMinDuration_4(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A_StaticFields::get_offset_of_kMaxTimeValue_5(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_Start_6(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_ClipIn_7(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_Asset_8(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_Duration_9(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_TimeScale_10(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_ParentTrack_11(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_EaseInDuration_12(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_EaseOutDuration_13(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_BlendInDuration_14(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_BlendOutDuration_15(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_MixInCurve_16(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_MixOutCurve_17(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_BlendInCurveMode_18(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_BlendOutCurveMode_19(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_ExposedParameterNames_20(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_AnimationCurves_21(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_Recordable_22(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_PostExtrapolationMode_23(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_PreExtrapolationMode_24(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_PostExtrapolationTime_25(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_PreExtrapolationTime_26(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_DisplayName_27(),
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_U3CdirtyHashU3Ek__BackingField_28(),
	0,
	TimelineClip_t45F9949FAF8A648D6A8E5F81221FC7B0A1B42E9A::get_offset_of_m_Version_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (ClipExtrapolation_tB3EB49D54D1E1E18AE03A394041AE9EC5E325750)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2615[6] = 
{
	ClipExtrapolation_tB3EB49D54D1E1E18AE03A394041AE9EC5E325750::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (BlendCurveMode_tFB15710CA3A221CD7B8B4FBE07DF8149E35D2F68)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2616[3] = 
{
	BlendCurveMode_tFB15710CA3A221CD7B8B4FBE07DF8149E35D2F68::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (TimelineClipUpgrade_t2E99A55A584778D37767485393B076B1402B4B14), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283), -1, sizeof(TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2619[7] = 
{
	TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283::get_offset_of_m_IntervalTree_0(),
	TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283::get_offset_of_m_ActiveClips_1(),
	TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283::get_offset_of_m_CurrentListOfActiveClips_2(),
	TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283::get_offset_of_m_ActiveBit_3(),
	TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283::get_offset_of_m_EvaluateCallbacks_4(),
	TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283::get_offset_of_m_PlayableCache_5(),
	TimelinePlayable_t898A842A48799FF7C7D8CC53110B6AFD4F727283_StaticFields::get_offset_of_muteAudioScrubbing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC), -1, sizeof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2620[19] = 
{
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_Locked_4(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_Muted_5(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_CustomPlayableFullTypename_6(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_AnimClip_7(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_Parent_8(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_Children_9(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_ItemsHash_10(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_ClipsCache_11(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_Start_12(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_End_13(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_CacheSorted_14(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields::get_offset_of_s_EmptyCache_15(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_ChildTrackCache_16(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields::get_offset_of_s_TrackBindingTypeAttributeCache_17(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_Clips_18(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields::get_offset_of_OnPlayableCreate_19(),
	0,
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC::get_offset_of_m_Version_21(),
	TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[6] = 
{
	U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D::get_offset_of_U3CattributeU3E__0_0(),
	U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D::get_offset_of_U3CtrackBindingTypeU3E__0_1(),
	U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t77A545F7789D8D30C9B859376E1F7D8C692B198D::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (ActivationMixerPlayable_t299EC5947DB712650FF6D52B34BE46862B4034E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2622[3] = 
{
	ActivationMixerPlayable_t299EC5947DB712650FF6D52B34BE46862B4034E2::get_offset_of_m_PostPlaybackState_0(),
	ActivationMixerPlayable_t299EC5947DB712650FF6D52B34BE46862B4034E2::get_offset_of_m_BoundGameObjectInitialStateIsActive_1(),
	ActivationMixerPlayable_t299EC5947DB712650FF6D52B34BE46862B4034E2::get_offset_of_m_BoundGameObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (ActivationPlayableAsset_t0660B3267843569F5B790ED336634B022B276AEB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (ActivationTrack_t1EFFCF321B9408DA59AECDF657A609411D041469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[2] = 
{
	ActivationTrack_t1EFFCF321B9408DA59AECDF657A609411D041469::get_offset_of_m_PostPlaybackState_23(),
	ActivationTrack_t1EFFCF321B9408DA59AECDF657A609411D041469::get_offset_of_m_ActivationMixer_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (PostPlaybackState_tD3667806DCEDE0B4EAEF0EEEFE6E240465AD3F3E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2625[5] = 
{
	PostPlaybackState_tD3667806DCEDE0B4EAEF0EEEFE6E240465AD3F3E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (AnimationOutputWeightProcessor_t7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[5] = 
{
	AnimationOutputWeightProcessor_t7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2::get_offset_of_m_Output_0(),
	AnimationOutputWeightProcessor_t7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2::get_offset_of_m_MotionXPlayable_1(),
	AnimationOutputWeightProcessor_t7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2::get_offset_of_m_PoseMixer_2(),
	AnimationOutputWeightProcessor_t7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2::get_offset_of_m_LayerMixer_3(),
	AnimationOutputWeightProcessor_t7D84E0D1730DB1F93A6B65CCAF2DAFD1C96129D2::get_offset_of_m_Mixers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (WeightInfo_t0DC40EEC1B90C9BB32621A1978483832819E22CA)+ sizeof (RuntimeObject), sizeof(WeightInfo_t0DC40EEC1B90C9BB32621A1978483832819E22CA_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2627[4] = 
{
	WeightInfo_t0DC40EEC1B90C9BB32621A1978483832819E22CA::get_offset_of_mixer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WeightInfo_t0DC40EEC1B90C9BB32621A1978483832819E22CA::get_offset_of_parentMixer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WeightInfo_t0DC40EEC1B90C9BB32621A1978483832819E22CA::get_offset_of_port_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WeightInfo_t0DC40EEC1B90C9BB32621A1978483832819E22CA::get_offset_of_modulate_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4), -1, sizeof(AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2628[10] = 
{
	AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4::get_offset_of_m_Clip_4(),
	AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4::get_offset_of_m_Position_5(),
	AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4::get_offset_of_m_EulerAngles_6(),
	AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4::get_offset_of_m_UseTrackMatchFields_7(),
	AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4::get_offset_of_m_MatchTargetFields_8(),
	AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4::get_offset_of_m_RemoveStartOffset_9(),
	AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4::get_offset_of_U3CappliedOffsetModeU3Ek__BackingField_10(),
	AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4_StaticFields::get_offset_of_k_LatestVersion_11(),
	AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4::get_offset_of_m_Version_12(),
	AnimationPlayableAsset_t71227EE3B9E197764F7EE22B080D2AFE95C8C4F4::get_offset_of_m_Rotation_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (AnimationPlayableAssetUpgrade_tD716C801DFEEC8A11527FC7E5AFD12BA3CF51C96), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (U3CU3Ec__Iterator0_t0E4328F8640EF4E59CCC6B6AB6460DB0D1241EB2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2630[4] = 
{
	U3CU3Ec__Iterator0_t0E4328F8640EF4E59CCC6B6AB6460DB0D1241EB2::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_t0E4328F8640EF4E59CCC6B6AB6460DB0D1241EB2::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_t0E4328F8640EF4E59CCC6B6AB6460DB0D1241EB2::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_t0E4328F8640EF4E59CCC6B6AB6460DB0D1241EB2::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (MatchTargetFields_t196139A2E266EF6660F3905BB110D7D41830219F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2631[7] = 
{
	MatchTargetFields_t196139A2E266EF6660F3905BB110D7D41830219F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (TrackOffset_tE00CFCF66C03B86C63ACE5FADC64AB25D68A80CC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2632[4] = 
{
	TrackOffset_tE00CFCF66C03B86C63ACE5FADC64AB25D68A80CC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (AppliedOffsetMode_tE0C23C461E39490CB48E37927E04C9E0B2F97697)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2633[8] = 
{
	AppliedOffsetMode_tE0C23C461E39490CB48E37927E04C9E0B2F97697::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (MatchTargetFieldConstants_tC9BE7A88A2EF5CFAF9B99F3292489096805555C5), -1, sizeof(MatchTargetFieldConstants_tC9BE7A88A2EF5CFAF9B99F3292489096805555C5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2634[4] = 
{
	MatchTargetFieldConstants_tC9BE7A88A2EF5CFAF9B99F3292489096805555C5_StaticFields::get_offset_of_All_0(),
	MatchTargetFieldConstants_tC9BE7A88A2EF5CFAF9B99F3292489096805555C5_StaticFields::get_offset_of_None_1(),
	MatchTargetFieldConstants_tC9BE7A88A2EF5CFAF9B99F3292489096805555C5_StaticFields::get_offset_of_Position_2(),
	MatchTargetFieldConstants_tC9BE7A88A2EF5CFAF9B99F3292489096805555C5_StaticFields::get_offset_of_Rotation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2635[15] = 
{
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_OpenClipPreExtrapolation_23(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_OpenClipPostExtrapolation_24(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_OpenClipOffsetPosition_25(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_OpenClipOffsetEulerAngles_26(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_OpenClipTimeOffset_27(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_OpenClipRemoveOffset_28(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_MatchTargetFields_29(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_Position_30(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_EulerAngles_31(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_AvatarMask_32(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_ApplyAvatarMask_33(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_TrackOffset_34(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_OpenClipOffsetRotation_35(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_Rotation_36(),
	AnimationTrack_t1103F3D88654B88259A8C59E7C145826A2B9D162::get_offset_of_m_ApplyOffsets_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (AnimationTrackUpgrade_t1C1BA9D56B6B7D6765318B5D882F218A83B8D448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (U3CU3Ec__Iterator0_tF4C0961AD99D073D1907CEEFF67C5BCB317BA967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2637[4] = 
{
	U3CU3Ec__Iterator0_tF4C0961AD99D073D1907CEEFF67C5BCB317BA967::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_tF4C0961AD99D073D1907CEEFF67C5BCB317BA967::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_tF4C0961AD99D073D1907CEEFF67C5BCB317BA967::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_tF4C0961AD99D073D1907CEEFF67C5BCB317BA967::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (TrackColorAttribute_tF37C45505BE134E4203DB4273F369282AFD2E9EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2638[1] = 
{
	TrackColorAttribute_tF37C45505BE134E4203DB4273F369282AFD2E9EA::get_offset_of_m_Color_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (AudioPlayableAsset_t25D7AC7CC32AE16D05D04C479DE263E48E08CAC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2639[3] = 
{
	AudioPlayableAsset_t25D7AC7CC32AE16D05D04C479DE263E48E08CAC1::get_offset_of_m_Clip_4(),
	AudioPlayableAsset_t25D7AC7CC32AE16D05D04C479DE263E48E08CAC1::get_offset_of_m_Loop_5(),
	AudioPlayableAsset_t25D7AC7CC32AE16D05D04C479DE263E48E08CAC1::get_offset_of_m_bufferingTime_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (U3CU3Ec__Iterator0_t25B1DCE9AABCA12916A77273AEAFE5BF2368D758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[4] = 
{
	U3CU3Ec__Iterator0_t25B1DCE9AABCA12916A77273AEAFE5BF2368D758::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_t25B1DCE9AABCA12916A77273AEAFE5BF2368D758::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_t25B1DCE9AABCA12916A77273AEAFE5BF2368D758::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_t25B1DCE9AABCA12916A77273AEAFE5BF2368D758::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (AudioTrack_t2A6D40E2647460067ED433025B56D89554743502), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (U3CU3Ec__Iterator0_tA3A5D78967E5AC878FF21E74C4EFB6DD690681A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[4] = 
{
	U3CU3Ec__Iterator0_tA3A5D78967E5AC878FF21E74C4EFB6DD690681A2::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_tA3A5D78967E5AC878FF21E74C4EFB6DD690681A2::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_tA3A5D78967E5AC878FF21E74C4EFB6DD690681A2::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_tA3A5D78967E5AC878FF21E74C4EFB6DD690681A2::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6), -1, sizeof(ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2643[17] = 
{
	0,
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6_StaticFields::get_offset_of_k_EmptyDirectorsList_5(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6_StaticFields::get_offset_of_k_EmptyParticlesList_6(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6::get_offset_of_sourceGameObject_7(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6::get_offset_of_prefabGameObject_8(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6::get_offset_of_updateParticle_9(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6::get_offset_of_particleRandomSeed_10(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6::get_offset_of_updateDirector_11(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6::get_offset_of_updateITimeControl_12(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6::get_offset_of_searchHierarchy_13(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6::get_offset_of_active_14(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6::get_offset_of_postPlayback_15(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6::get_offset_of_m_ControlDirectorAsset_16(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6::get_offset_of_m_Duration_17(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6::get_offset_of_m_SupportLoop_18(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6_StaticFields::get_offset_of_s_ProcessedDirectors_19(),
	ControlPlayableAsset_t402B1672C75CE1DB938A0CFCD6FA90F3325439E6_StaticFields::get_offset_of_s_CreatedPrefabs_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[7] = 
{
	U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07::get_offset_of_root_0(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07::get_offset_of_U24locvar0_1(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07::get_offset_of_U24locvar1_2(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07::get_offset_of_U3CscriptU3E__1_3(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07::get_offset_of_U24current_4(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07::get_offset_of_U24disposing_5(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3F28F4AD4FF876117B187D5AF299DCE19E104D07::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (ControlTrack_t4DDF09758220B0908E2932B2D2C47817DA1DE819), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (InfiniteRuntimeClip_t29A5B0CCB080BB3C14C3789D14D5FBD21FA631C1), -1, sizeof(InfiniteRuntimeClip_t29A5B0CCB080BB3C14C3789D14D5FBD21FA631C1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2646[2] = 
{
	InfiniteRuntimeClip_t29A5B0CCB080BB3C14C3789D14D5FBD21FA631C1::get_offset_of_m_Playable_0(),
	InfiniteRuntimeClip_t29A5B0CCB080BB3C14C3789D14D5FBD21FA631C1_StaticFields::get_offset_of_kIntervalEnd_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2649[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2650[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (RuntimeClip_tAAFF82B65DD64256025D661E771DEC34A79C74DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[3] = 
{
	RuntimeClip_tAAFF82B65DD64256025D661E771DEC34A79C74DF::get_offset_of_m_Clip_0(),
	RuntimeClip_tAAFF82B65DD64256025D661E771DEC34A79C74DF::get_offset_of_m_Playable_1(),
	RuntimeClip_tAAFF82B65DD64256025D661E771DEC34A79C74DF::get_offset_of_m_ParentMixer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (RuntimeClipBase_t4BE09F69F59AE0FD3F61D1C3E0DE29F236776083), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (RuntimeElement_t25E724CA4B982CE9EAD77996253FA3008561ED80), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[6] = 
{
	ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B::get_offset_of_m_Clip_0(),
	ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B::get_offset_of_m_Playable_1(),
	ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B::get_offset_of_m_ParentMixer_2(),
	ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B::get_offset_of_m_StartDelay_3(),
	ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B::get_offset_of_m_FinishTail_4(),
	ScheduleRuntimeClip_tA85F2BE69B3FBFD4E3C0F97FE16CA8D5CBF5E80B::get_offset_of_m_Started_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (ActivationControlPlayable_tCA39D16B1A31D59AAD5AE50F950D328A1091BEA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2655[2] = 
{
	ActivationControlPlayable_tCA39D16B1A31D59AAD5AE50F950D328A1091BEA1::get_offset_of_gameObject_0(),
	ActivationControlPlayable_tCA39D16B1A31D59AAD5AE50F950D328A1091BEA1::get_offset_of_postPlayback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (PostPlaybackState_t2A1219857A4AF2EA533FD59DFEE53B2DB462A05A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2656[4] = 
{
	PostPlaybackState_t2A1219857A4AF2EA533FD59DFEE53B2DB462A05A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (BasicPlayableBehaviour_t41A7C66B0272D5149F113C941A89FE2275DD2AB4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (DirectorControlPlayable_t86AA53337442CC16F17A1BECE410318BD1E36819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[3] = 
{
	DirectorControlPlayable_t86AA53337442CC16F17A1BECE410318BD1E36819::get_offset_of_director_0(),
	DirectorControlPlayable_t86AA53337442CC16F17A1BECE410318BD1E36819::get_offset_of_m_SyncTime_1(),
	DirectorControlPlayable_t86AA53337442CC16F17A1BECE410318BD1E36819::get_offset_of_m_AssetDuration_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (ParticleControlPlayable_t6F6891ACE77DFDD968CF3655D21615750D8D5AB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2660[4] = 
{
	ParticleControlPlayable_t6F6891ACE77DFDD968CF3655D21615750D8D5AB9::get_offset_of_m_LastTime_0(),
	ParticleControlPlayable_t6F6891ACE77DFDD968CF3655D21615750D8D5AB9::get_offset_of_m_RandomSeed_1(),
	ParticleControlPlayable_t6F6891ACE77DFDD968CF3655D21615750D8D5AB9::get_offset_of_m_SystemTime_2(),
	ParticleControlPlayable_t6F6891ACE77DFDD968CF3655D21615750D8D5AB9::get_offset_of_U3CparticleSystemU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (PrefabControlPlayable_tC88C9AA8E25E0AC486369104E9E34CAD49A610E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[1] = 
{
	PrefabControlPlayable_tC88C9AA8E25E0AC486369104E9E34CAD49A610E6::get_offset_of_m_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (TimeControlPlayable_tF087C878D138911D1FC78E62AB74C1B1AF74C630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[1] = 
{
	TimeControlPlayable_tF087C878D138911D1FC78E62AB74C1B1AF74C630::get_offset_of_m_timeControl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (PlayableTrack_t3D123D2F1485F7C140359B76D3F5DDAB43307396), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (Extrapolation_tB50CEF33A8A4779DC79A55C0B74A92BD00E6E89D), -1, sizeof(Extrapolation_tB50CEF33A8A4779DC79A55C0B74A92BD00E6E89D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2664[2] = 
{
	Extrapolation_tB50CEF33A8A4779DC79A55C0B74A92BD00E6E89D_StaticFields::get_offset_of_kMinExtrapolationTime_0(),
	Extrapolation_tB50CEF33A8A4779DC79A55C0B74A92BD00E6E89D_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (HashUtility_t1DD34AC09A13D051A3FED020EB281D0F3FC0E4B7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (TimeUtility_tE3DFA7C9986845BDDF19995B106FA30FBE32D870), -1, sizeof(TimeUtility_tE3DFA7C9986845BDDF19995B106FA30FBE32D870_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2668[3] = 
{
	TimeUtility_tE3DFA7C9986845BDDF19995B106FA30FBE32D870_StaticFields::get_offset_of_kTimeEpsilon_0(),
	TimeUtility_tE3DFA7C9986845BDDF19995B106FA30FBE32D870_StaticFields::get_offset_of_kFrameRateEpsilon_1(),
	TimeUtility_tE3DFA7C9986845BDDF19995B106FA30FBE32D870_StaticFields::get_offset_of_k_MaxTimelineDurationInSeconds_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (TimelineCreateUtilities_t39126B4401FD5B6D8CD74FF6DDA8C88652F215D1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t12FED3630926B0AEF98BCDC8558A5C3E1EA680CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[1] = 
{
	U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t12FED3630926B0AEF98BCDC8558A5C3E1EA680CD::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (U3CGenerateUniqueActorNameU3Ec__AnonStorey1_t978BA28AD54A9E03E86679B80567BB82B62BCD5E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[1] = 
{
	U3CGenerateUniqueActorNameU3Ec__AnonStorey1_t978BA28AD54A9E03E86679B80567BB82B62BCD5E::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (TimelineUndo_t194ECE9DA26071CE759EDD3D1E667AC46B3D05D7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70), -1, sizeof(IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2674[4] = 
{
	IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_StaticFields::get_offset_of_mInstance_4(),
	IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70_StaticFields::get_offset_of_mJVibrator_5(),
	IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70::get_offset_of_mVibrateTimeLeft_6(),
	IntenseVibration_tD697DD1DE3CF0E007232BB307CA8479A6F067B70::get_offset_of_mPatterns_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (TestIntenseVibration_tB3B9456153E01062B1FCD9F20943D0897E283741), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (EasyVibro_tB0B9B23A81DBB11562670BA10599582E401DDFB3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (HapticTypes_t5841156CF72EED0C5C47C9245C09D2250C9CAAFD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2677[8] = 
{
	HapticTypes_t5841156CF72EED0C5C47C9245C09D2250C9CAAFD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263), -1, sizeof(MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2678[20] = 
{
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of_LightDuration_0(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of_MediumDuration_1(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of_HeavyDuration_2(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of_LightAmplitude_3(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of_MediumAmplitude_4(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of_HeavyAmplitude_5(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of__sdkVersion_6(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of__successPattern_7(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of__successPatternAmplitude_8(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of__warningPattern_9(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of__warningPatternAmplitude_10(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of__failurePattern_11(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of__failurePatternAmplitude_12(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of_UnityPlayer_13(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of_CurrentActivity_14(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of_AndroidVibrator_15(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of_VibrationEffectClass_16(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of_VibrationEffect_17(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of_DefaultAmplitude_18(),
	MMVibrationManager_tF61AC21066BC0D4EA57104C332CA7C2B52F37263_StaticFields::get_offset_of_iOSHapticsInitialized_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[4] = 
{
	NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E::get_offset_of_DebugTextBox_4(),
	NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E::get_offset_of__debugString_5(),
	NiceVibrationsDemoManager_t2F48E6AE3AC20158BC1CF621740B795B6BF1D07E::get_offset_of__platformString_6(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672), -1, sizeof(AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2681[8] = 
{
	AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672_StaticFields::get_offset_of_s_Instance_4(),
	AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672::get_offset_of__uiGeneralMoneyCounters_5(),
	AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672::get_offset_of_m_aryGreenCash_6(),
	AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672::get_offset_of_m_nGreenCash_7(),
	AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672::get_offset_of_m_dicTalentPointBuyTimes_8(),
	0,
	AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672::get_offset_of_m_aryMonthCards_10(),
	AccountSystem_t973757358DCC87227FF205F0DAFECF2EC5A14672::get_offset_of_m_bGeneralMoneyCounterVisible_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3), -1, sizeof(Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2682[7] = 
{
	Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3_StaticFields::get_offset_of_s_Instance_4(),
	Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3::get_offset_of_m_fWatchAdsBuyVehicleFreeInterval_5(),
	Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3::get_offset_of_m_fWatchAdsBuyVehicleFreeTimeElaspe_6(),
	Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3::get_offset_of_m_nCurWatchAdsLevel_7(),
	Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3::get_offset_of_m_nCurPlanetId_8(),
	Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3::get_offset_of_m_nTrackId_9(),
	Activity_tCF0A46CC7CCE037909971F6DAA6CC470FCFB2EF3::get_offset_of_m_dtLastWatchAdsTime_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60), -1, sizeof(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2683[15] = 
{
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60_StaticFields::get_offset_of_vecTempScale_4(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_Config_5(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_nColddownLeftTime_6(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_nDurationLeftTime_7(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_nRealDuration_8(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_bUsing_9(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_eStatus_10(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_dateSkillStartTime_11(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_fColddownPercent_12(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_BoundCounter_13(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_BoundTrack_14(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_nBuyPrice_15(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_aryIntParams_16(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_fTimeElaspe_17(),
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60::get_offset_of_m_fSkillGainTimeElaspe_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24), -1, sizeof(AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2684[56] = 
{
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_arySkillIcons_4(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__effectCasting_5(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_MAX_QUALITY_LEVEL_6(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24_StaticFields::get_offset_of_vecTempScale_7(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24_StaticFields::get_offset_of_vecTempPos_8(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__basescaleCPosCoinIcon_9(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_vecCoinFlyStartPos_10(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_vecCoinFlyMiddlePos_Left_11(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_vecCoinFlyMiddlePos_Right_12(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_vecCoinFlyEndPos_13(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_fSkillGainInterval_14(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24_StaticFields::get_offset_of_s_Instance_15(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_vecTiaoZiPos_16(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_vecInitAndMaxScale_17(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_nAvatarIndex_18(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_colorUsing_19(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_colorNotUsing_20(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_aryAvatar_21(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_aryAvatar_Small_22(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_sprNotUsingBg_23(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_sprUsingBg_24(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_preAdminCounter_25(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_preAdmin_26(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__panelMain_27(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__imgSceneSkillIcon_28(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__subpanelUsing_29(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__containerNotUsing_30(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__subpanelNotUsing_31(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__containerUsing_32(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_lstAllCounters_33(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_containerRecycledCounters_34(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__subpanelSell_35(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__containerTiaoZi_36(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__imgMoneyIcon_37(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__txtPrice_38(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__btnCastSkill_39(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__imgUsingAdminAvatar_40(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__imgAvatarMask_41(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__txtLeftTime_42(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__txtBuyPrice_43(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__imgSellMoneyIcon_44(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__txtNum_45(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_nUnlockTrackLevel_46(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_nMaxBuyNum_47(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_fPriceRisePercent_48(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_nCurPrice_49(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_CurUsingAdmin_50(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_AdminToSell_51(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of__effectCastingSkill_52(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_tempAdminConfig_53(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_dicAdminConfig_54(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_lstProbabilityPool_55(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_bAdminConfigLoaded_56(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_bIsVisible_57(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_lstRecycledCounters_58(),
	AdministratorManager_t5F2A65A9FE6D518D47CD1DA985EB2C403C41EC24::get_offset_of_m_lstRecycledAdmins_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (eAdminFuncType_tF7B1C80396FD2B29BD0659D720064CE0F64EC460)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2685[5] = 
{
	eAdminFuncType_tF7B1C80396FD2B29BD0659D720064CE0F64EC460::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (eUisngAdminStatus_t3535F7389D1BB62376817770D1110E3D0011D2B1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2686[4] = 
{
	eUisngAdminStatus_t3535F7389D1BB62376817770D1110E3D0011D2B1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827)+ sizeof (RuntimeObject), sizeof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2687[10] = 
{
	sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827::get_offset_of_nId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827::get_offset_of_szName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827::get_offset_of_szQuality_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827::get_offset_of_nQuality_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827::get_offset_of_szDesc_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827::get_offset_of_eType_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827::get_offset_of_fValue_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827::get_offset_of_nDuration_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827::get_offset_of_nColddown_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827::get_offset_of_nProbabilityWeight_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (U3CLoadConfig_AdminU3Ed__61_t9FDE21B292E708C0BFEAE410AB1AD0802601B078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[5] = 
{
	U3CLoadConfig_AdminU3Ed__61_t9FDE21B292E708C0BFEAE410AB1AD0802601B078::get_offset_of_U3CU3E1__state_0(),
	U3CLoadConfig_AdminU3Ed__61_t9FDE21B292E708C0BFEAE410AB1AD0802601B078::get_offset_of_U3CU3E2__current_1(),
	U3CLoadConfig_AdminU3Ed__61_t9FDE21B292E708C0BFEAE410AB1AD0802601B078::get_offset_of_szFileName_2(),
	U3CLoadConfig_AdminU3Ed__61_t9FDE21B292E708C0BFEAE410AB1AD0802601B078::get_offset_of_U3CU3E4__this_3(),
	U3CLoadConfig_AdminU3Ed__61_t9FDE21B292E708C0BFEAE410AB1AD0802601B078::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A), -1, sizeof(AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2689[27] = 
{
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A_StaticFields::get_offset_of_s_Instance_4(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_fTotalTime_5(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_fCurTime_6(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of__panelAds_7(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of__btnOpenAdsPanel_8(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of__btnWatchAds_9(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of__panelPreAds_10(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of__txtAdsLeftTime_11(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of__txtTitleTiSheng_12(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of__txtTitleTiShengJiaoBiao_13(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of__txtTitle_14(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of__txtDesc_15(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of__txtLeftTime_16(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of__imgProgressBar_17(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_fBaseRaise_18(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_fRaiseDuration_19(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_fAdsPlayTime_20(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_fRealCoinPromote_21(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_fRealDuraton_22(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_fRealTotalTime_23(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_eAdsType_24(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_bAdsConfigLoaded_25(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_dtTotalTimeStartTime_26(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_bVisible_27(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_fPromotePercent_28(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_fMainLoopTimeElapse_29(),
	AdsManager_tF7BBE20B4952B2F41404F84FF96A2DB2C6DDAD4A::get_offset_of_m_fTotalTimeTimeElaspe_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (eAdsType_t2B6FF0BDE868ADDF19A539CB13A12489863F8CFB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2690[6] = 
{
	eAdsType_t2B6FF0BDE868ADDF19A539CB13A12489863F8CFB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (U3CLoadConfig_AdsU3Ed__28_t7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[5] = 
{
	U3CLoadConfig_AdsU3Ed__28_t7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660::get_offset_of_U3CU3E1__state_0(),
	U3CLoadConfig_AdsU3Ed__28_t7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660::get_offset_of_U3CU3E2__current_1(),
	U3CLoadConfig_AdsU3Ed__28_t7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660::get_offset_of_szFileName_2(),
	U3CLoadConfig_AdsU3Ed__28_t7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660::get_offset_of_U3CU3E4__this_3(),
	U3CLoadConfig_AdsU3Ed__28_t7C0CE81CBCA1A6FD02B8A450B158FDDEC6BD4660::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA), -1, sizeof(AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2692[50] = 
{
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA_StaticFields::get_offset_of_s_Instance_4(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA_StaticFields::get_offset_of_vecTempScale_5(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_aryAdventureQualityBg_6(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_aryAdventureQualityColor_7(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_aryAdventureName_8(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__panelAdventure_9(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__subpanelSelectAdventure_10(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__subpanelAdventuring_11(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__subpanelCollect_12(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__counterAdventuring_13(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__txtRefreshColddown_14(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__txtWatchAdsColddown_15(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__txtWatchAdsReduce_16(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__containerPlay_17(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__imgPlay_18(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__txtTimeLeft_19(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_aryAdventureCounters_20(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__btnCollect_21(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__btnWatchAds_22(),
	0,
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_fAdsReduceTime_24(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_fAdsColdDownTotalTime_25(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_fRefreshColddownTime_26(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_nRealTimeTotalTime_27(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_nRealAdsColdDownTotalTime_28(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_nRealRefreshColdDownTotalTime_29(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_bAdventuring_30(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_dtAdventureStartTime_31(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_dtRefreshColddownStartTime_32(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_dtWatchAdsColdDownStartTime_33(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_bCollecting_34(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_bRefreshColddowning_35(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_bWatchAdsColddowning_36(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_nSelectedAdentureCounterIndex_37(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_dicAdventureConfig_38(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_lstProbility_39(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_bIsShowingAdventurePanel_40(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_fTimeElapse_41(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_lstRewardTotalTemp_42(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_lstRewardSelectedTemp_43(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_bAdventureConfigLoaded_44(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__txtCollectDesc_45(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__baseScaleAvatarContainer_46(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of__imgCollectAvatar_47(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_fCollectAvatarShowTime_48(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_fCollectAvatarFadeTime_49(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_nCollectIndex_50(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_bCollectShowStatus_51(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_fCollectTimeElapse_52(),
	AdventureManager_New_t90D8071C513B6888CA09B0F1646C590B7D1D89DA::get_offset_of_m_fFadeScaleSpeed_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (eAdventureQuality_tEEC00413F249D6C1C4D8619161955635920131D3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2693[4] = 
{
	eAdventureQuality_tEEC00413F249D6C1C4D8619161955635920131D3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80)+ sizeof (RuntimeObject), sizeof(sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2694[4] = 
{
	sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80::get_offset_of_nItemId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80::get_offset_of_nMaxNum_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80::get_offset_of_nMinNum_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sRewardConfig_t1C8055701C09EA49E5691E40DE09E716269FDB80::get_offset_of_nNum_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[7] = 
{
	sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1::get_offset_of_eQuality_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1::get_offset_of_szDesc_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1::get_offset_of_fDuration_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1::get_offset_of_nProbability_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1::get_offset_of_lstMainRewardPool_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1::get_offset_of_lstViceRewardPool_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAdventureConfig_tF39377E053A6EF748EF650DAA67DAFE090903FB1::get_offset_of_nViceRewardNum_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (U3CLoadConfig_AdventureU3Ed__58_t898A963584E587F83979F94CE96E82BA45A51A95), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2696[5] = 
{
	U3CLoadConfig_AdventureU3Ed__58_t898A963584E587F83979F94CE96E82BA45A51A95::get_offset_of_U3CU3E1__state_0(),
	U3CLoadConfig_AdventureU3Ed__58_t898A963584E587F83979F94CE96E82BA45A51A95::get_offset_of_U3CU3E2__current_1(),
	U3CLoadConfig_AdventureU3Ed__58_t898A963584E587F83979F94CE96E82BA45A51A95::get_offset_of_szFileName_2(),
	U3CLoadConfig_AdventureU3Ed__58_t898A963584E587F83979F94CE96E82BA45A51A95::get_offset_of_U3CU3E4__this_3(),
	U3CLoadConfig_AdventureU3Ed__58_t898A963584E587F83979F94CE96E82BA45A51A95::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[13] = 
{
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6::get_offset_of_m_aryRewardCounters_4(),
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6::get_offset_of__txtTitle_5(),
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6::get_offset_of__txtTotalTime_6(),
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6::get_offset_of__txtName_7(),
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6::get_offset_of__txtReduceTime_8(),
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6::get_offset_of__txtReduceTime_ColdDown_9(),
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6::get_offset_of__containerPlay_10(),
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6::get_offset_of__imgQualityBg_11(),
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6::get_offset_of_m_GeneralConfig_12(),
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6::get_offset_of_m_MainRewardConfig_13(),
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6::get_offset_of_m_lstViceRewardConfig_14(),
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6::get_offset_of_m_nViceNum_15(),
	UIAdventureCounter_New_tED6A10048A58F582BA0350510888860A539ABAF6::get_offset_of_m_nCounterIndex_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (UIAdventureRewardCounter_t620140F5CBF326FC39B3E3D993019E832EA9CEBE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[3] = 
{
	UIAdventureRewardCounter_t620140F5CBF326FC39B3E3D993019E832EA9CEBE::get_offset_of__imgAvatar_4(),
	UIAdventureRewardCounter_t620140F5CBF326FC39B3E3D993019E832EA9CEBE::get_offset_of__txtDesc_5(),
	UIAdventureRewardCounter_t620140F5CBF326FC39B3E3D993019E832EA9CEBE::get_offset_of__txtNum_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E), -1, sizeof(AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2699[29] = 
{
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E_StaticFields::get_offset_of_s_Instance_4(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E_StaticFields::get_offset_of_vecTempPos_5(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E_StaticFields::get_offset_of_vecTempScale_6(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_aryAdventureRewardShow_7(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of__txtLeftTime_8(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of__txtProgressbar_9(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of__containerProgressBar_10(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of__btnCollect_11(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of__imgCollectRewardBg_12(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_bAdventuring_13(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_nLeftTime_14(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_nTotalTime_15(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_CurAdventure_16(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of__panelAdventure_17(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of__panelAdventuring_18(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_nRewardStatus_19(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_nTotalRewardNum_20(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_nCurShowedRewardNum_21(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_fRewardCounterStartPos_22(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_fRewardCounterEndPos_23(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_aryRewardShowCounters_24(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_CurShowingRewardCounter_25(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_fShowRewardWaitingTime_26(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_fShowRewardMovingTime_27(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_fShowRewardMovingSpeed_28(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_fShowRewardScalingSpeed_29(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_bShowingAdventurePanel_30(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_fTimeElapse_31(),
	AdventureManager_tC7BDA893675D603C9C2733574D9DC4F29B2B430E::get_offset_of_m_fShowRewardTimeElapse_32(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
