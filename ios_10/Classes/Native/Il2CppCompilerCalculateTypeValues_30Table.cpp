﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Spine.Bone
struct Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E;
// Spine.Skeleton
struct Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782;
// Spine.Unity.ISkeletonAnimation
struct ISkeletonAnimation_tDF41FB227A23465F8A2D0F38B5E02D6B26BE6F60;
// Spine.Unity.MeshGenerator
struct MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157;
// Spine.Unity.MeshRendererBuffers
struct MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107;
// Spine.Unity.Modules.SkeletonGhostRenderer
struct SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6;
// Spine.Unity.Modules.SkeletonGhostRenderer[]
struct SkeletonGhostRendererU5BU5D_t684BE74B1631BEFA357B9DEAB343C897B20E4804;
// Spine.Unity.Modules.SkeletonRagdoll
struct SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0;
// Spine.Unity.Modules.SkeletonRagdoll2D
struct SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4;
// Spine.Unity.SkeletonGraphic
struct SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A;
// Spine.Unity.SkeletonRenderer
struct SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03;
// Spine.Unity.SkeletonRendererInstruction
struct SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A;
// Spine.Unity.SkeletonUtility
struct SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0;
// Spine.Unity.SkeletonUtilityBone
struct SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Spine.AtlasRegion,UnityEngine.Texture2D>
struct Dictionary_2_t917CD6850A65021B15B8A9BF5060E9F630AD53A2;
// System.Collections.Generic.Dictionary`2<Spine.Bone,UnityEngine.Transform>
struct Dictionary_2_t0933035DE5961A26FC54331E79B0DB3951A2AF94;
// System.Collections.Generic.Dictionary`2<Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair,UnityEngine.Material>
struct Dictionary_2_t54C49C1E7A32D13A8D7285BC47E6BEEE576F454C;
// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material>
struct Dictionary_2_t87B6F3EB6208E12A08960AA208E68480EA2C7926;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonPartsRenderer>
struct List_1_tC62EA284874C134BCFA59817E630FFFD009FDEA2;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride>
struct List_1_t641FD355D7A09DCD729156F64BA661AEF7501211;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride>
struct List_1_tAFA7DD41E205AC6A69EE6C6F5F28D8B6975844C9;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair>
struct List_1_t941310C818AB9A721ED8F4B0F2D7A14D9069AC8D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_tD84B8B60A0D0578F20C5B6EE527F0C57547C88B8;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3EB9781D1A1DE2674F0632C956A66AA423343139;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTACHMENTCLONEEXTENSIONS_TD2E5C3ECD925AF6F1B362CC5D63D3426D657BEB0_H
#define ATTACHMENTCLONEEXTENSIONS_TD2E5C3ECD925AF6F1B362CC5D63D3426D657BEB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.AttachmentCloneExtensions
struct  AttachmentCloneExtensions_tD2E5C3ECD925AF6F1B362CC5D63D3426D657BEB0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTCLONEEXTENSIONS_TD2E5C3ECD925AF6F1B362CC5D63D3426D657BEB0_H
#ifndef ATTACHMENTREGIONEXTENSIONS_TF2133D86E087352C30B685F00CCEEE741152FD9D_H
#define ATTACHMENTREGIONEXTENSIONS_TF2133D86E087352C30B685F00CCEEE741152FD9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.AttachmentRegionExtensions
struct  AttachmentRegionExtensions_tF2133D86E087352C30B685F00CCEEE741152FD9D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTREGIONEXTENSIONS_TF2133D86E087352C30B685F00CCEEE741152FD9D_H
#ifndef SKINUTILITIES_T14C5F179F43847921F1296F66D569729AE1720CE_H
#define SKINUTILITIES_T14C5F179F43847921F1296F66D569729AE1720CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.SkinUtilities
struct  SkinUtilities_t14C5F179F43847921F1296F66D569729AE1720CE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKINUTILITIES_T14C5F179F43847921F1296F66D569729AE1720CE_H
#ifndef U3CFADEU3ED__7_T543FB46255AB4D6A0BB976116220E57E7EBF7637_H
#define U3CFADEU3ED__7_T543FB46255AB4D6A0BB976116220E57E7EBF7637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>d__7
struct  U3CFadeU3Ed__7_t543FB46255AB4D6A0BB976116220E57E7EBF7637  : public RuntimeObject
{
public:
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Spine.Unity.Modules.SkeletonGhostRenderer Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>d__7::<>4__this
	SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6 * ___U3CU3E4__this_2;
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>d__7::<t>5__2
	int32_t ___U3CtU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFadeU3Ed__7_t543FB46255AB4D6A0BB976116220E57E7EBF7637, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFadeU3Ed__7_t543FB46255AB4D6A0BB976116220E57E7EBF7637, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFadeU3Ed__7_t543FB46255AB4D6A0BB976116220E57E7EBF7637, ___U3CU3E4__this_2)); }
	inline SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CFadeU3Ed__7_t543FB46255AB4D6A0BB976116220E57E7EBF7637, ___U3CtU3E5__2_3)); }
	inline int32_t get_U3CtU3E5__2_3() const { return ___U3CtU3E5__2_3; }
	inline int32_t* get_address_of_U3CtU3E5__2_3() { return &___U3CtU3E5__2_3; }
	inline void set_U3CtU3E5__2_3(int32_t value)
	{
		___U3CtU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEU3ED__7_T543FB46255AB4D6A0BB976116220E57E7EBF7637_H
#ifndef U3CSMOOTHMIXCOROUTINEU3ED__40_TF117CA3E66591E1844AABF287CC86A08DF785394_H
#define U3CSMOOTHMIXCOROUTINEU3ED__40_TF117CA3E66591E1844AABF287CC86A08DF785394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>d__40
struct  U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394  : public RuntimeObject
{
public:
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>d__40::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>d__40::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Spine.Unity.Modules.SkeletonRagdoll Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>d__40::<>4__this
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0 * ___U3CU3E4__this_2;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>d__40::target
	float ___target_3;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>d__40::duration
	float ___duration_4;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>d__40::<startTime>5__2
	float ___U3CstartTimeU3E5__2_5;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>d__40::<startMix>5__3
	float ___U3CstartMixU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394, ___U3CU3E4__this_2)); }
	inline SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394, ___target_3)); }
	inline float get_target_3() const { return ___target_3; }
	inline float* get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(float value)
	{
		___target_3 = value;
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394, ___U3CstartTimeU3E5__2_5)); }
	inline float get_U3CstartTimeU3E5__2_5() const { return ___U3CstartTimeU3E5__2_5; }
	inline float* get_address_of_U3CstartTimeU3E5__2_5() { return &___U3CstartTimeU3E5__2_5; }
	inline void set_U3CstartTimeU3E5__2_5(float value)
	{
		___U3CstartTimeU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CstartMixU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394, ___U3CstartMixU3E5__3_6)); }
	inline float get_U3CstartMixU3E5__3_6() const { return ___U3CstartMixU3E5__3_6; }
	inline float* get_address_of_U3CstartMixU3E5__3_6() { return &___U3CstartMixU3E5__3_6; }
	inline void set_U3CstartMixU3E5__3_6(float value)
	{
		___U3CstartMixU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSMOOTHMIXCOROUTINEU3ED__40_TF117CA3E66591E1844AABF287CC86A08DF785394_H
#ifndef U3CSTARTU3ED__33_T0F3854A111EB39AE09107EDAF5BD78E6F5164EE8_H
#define U3CSTARTU3ED__33_T0F3854A111EB39AE09107EDAF5BD78E6F5164EE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll/<Start>d__33
struct  U3CStartU3Ed__33_t0F3854A111EB39AE09107EDAF5BD78E6F5164EE8  : public RuntimeObject
{
public:
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll/<Start>d__33::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll/<Start>d__33::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Spine.Unity.Modules.SkeletonRagdoll Spine.Unity.Modules.SkeletonRagdoll/<Start>d__33::<>4__this
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__33_t0F3854A111EB39AE09107EDAF5BD78E6F5164EE8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__33_t0F3854A111EB39AE09107EDAF5BD78E6F5164EE8, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__33_t0F3854A111EB39AE09107EDAF5BD78E6F5164EE8, ___U3CU3E4__this_2)); }
	inline SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__33_T0F3854A111EB39AE09107EDAF5BD78E6F5164EE8_H
#ifndef U3CSMOOTHMIXCOROUTINEU3ED__39_TF4B7705E322F3B68C4C1EE92153E75E2478CF276_H
#define U3CSMOOTHMIXCOROUTINEU3ED__39_TF4B7705E322F3B68C4C1EE92153E75E2478CF276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>d__39
struct  U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276  : public RuntimeObject
{
public:
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>d__39::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>d__39::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Spine.Unity.Modules.SkeletonRagdoll2D Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>d__39::<>4__this
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4 * ___U3CU3E4__this_2;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>d__39::target
	float ___target_3;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>d__39::duration
	float ___duration_4;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>d__39::<startTime>5__2
	float ___U3CstartTimeU3E5__2_5;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>d__39::<startMix>5__3
	float ___U3CstartMixU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276, ___U3CU3E4__this_2)); }
	inline SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276, ___target_3)); }
	inline float get_target_3() const { return ___target_3; }
	inline float* get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(float value)
	{
		___target_3 = value;
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276, ___U3CstartTimeU3E5__2_5)); }
	inline float get_U3CstartTimeU3E5__2_5() const { return ___U3CstartTimeU3E5__2_5; }
	inline float* get_address_of_U3CstartTimeU3E5__2_5() { return &___U3CstartTimeU3E5__2_5; }
	inline void set_U3CstartTimeU3E5__2_5(float value)
	{
		___U3CstartTimeU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CstartMixU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276, ___U3CstartMixU3E5__3_6)); }
	inline float get_U3CstartMixU3E5__3_6() const { return ___U3CstartMixU3E5__3_6; }
	inline float* get_address_of_U3CstartMixU3E5__3_6() { return &___U3CstartMixU3E5__3_6; }
	inline void set_U3CstartMixU3E5__3_6(float value)
	{
		___U3CstartMixU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSMOOTHMIXCOROUTINEU3ED__39_TF4B7705E322F3B68C4C1EE92153E75E2478CF276_H
#ifndef U3CSTARTU3ED__32_TE8398F55DD18CE7B534129B6B2265BA36EE8AA13_H
#define U3CSTARTU3ED__32_TE8398F55DD18CE7B534129B6B2265BA36EE8AA13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll2D/<Start>d__32
struct  U3CStartU3Ed__32_tE8398F55DD18CE7B534129B6B2265BA36EE8AA13  : public RuntimeObject
{
public:
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll2D/<Start>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll2D/<Start>d__32::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Spine.Unity.Modules.SkeletonRagdoll2D Spine.Unity.Modules.SkeletonRagdoll2D/<Start>d__32::<>4__this
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__32_tE8398F55DD18CE7B534129B6B2265BA36EE8AA13, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__32_tE8398F55DD18CE7B534129B6B2265BA36EE8AA13, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__32_tE8398F55DD18CE7B534129B6B2265BA36EE8AA13, ___U3CU3E4__this_2)); }
	inline SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__32_TE8398F55DD18CE7B534129B6B2265BA36EE8AA13_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef __STATICARRAYINITTYPESIZEU3D20_TD65589242911778C66D1E5AC9009597568746382_H
#define __STATICARRAYINITTYPESIZEU3D20_TD65589242911778C66D1E5AC9009597568746382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20
struct  __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D20_TD65589242911778C66D1E5AC9009597568746382_H
#ifndef __STATICARRAYINITTYPESIZEU3D24_T0186834C621050282F5EF637E836A6E1DC934A34_H
#define __STATICARRAYINITTYPESIZEU3D24_T0186834C621050282F5EF637E836A6E1DC934A34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D24_T0186834C621050282F5EF637E836A6E1DC934A34_H
#ifndef ATLASMATERIALOVERRIDE_TBE247B8D7F31F3D8513F0F4789E683C137E32B04_H
#define ATLASMATERIALOVERRIDE_TBE247B8D7F31F3D8513F0F4789E683C137E32B04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride
struct  AtlasMaterialOverride_tBE247B8D7F31F3D8513F0F4789E683C137E32B04 
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride::overrideDisabled
	bool ___overrideDisabled_0;
	// UnityEngine.Material Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride::originalMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___originalMaterial_1;
	// UnityEngine.Material Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride::replacementMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___replacementMaterial_2;

public:
	inline static int32_t get_offset_of_overrideDisabled_0() { return static_cast<int32_t>(offsetof(AtlasMaterialOverride_tBE247B8D7F31F3D8513F0F4789E683C137E32B04, ___overrideDisabled_0)); }
	inline bool get_overrideDisabled_0() const { return ___overrideDisabled_0; }
	inline bool* get_address_of_overrideDisabled_0() { return &___overrideDisabled_0; }
	inline void set_overrideDisabled_0(bool value)
	{
		___overrideDisabled_0 = value;
	}

	inline static int32_t get_offset_of_originalMaterial_1() { return static_cast<int32_t>(offsetof(AtlasMaterialOverride_tBE247B8D7F31F3D8513F0F4789E683C137E32B04, ___originalMaterial_1)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_originalMaterial_1() const { return ___originalMaterial_1; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_originalMaterial_1() { return &___originalMaterial_1; }
	inline void set_originalMaterial_1(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___originalMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___originalMaterial_1), value);
	}

	inline static int32_t get_offset_of_replacementMaterial_2() { return static_cast<int32_t>(offsetof(AtlasMaterialOverride_tBE247B8D7F31F3D8513F0F4789E683C137E32B04, ___replacementMaterial_2)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_replacementMaterial_2() const { return ___replacementMaterial_2; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_replacementMaterial_2() { return &___replacementMaterial_2; }
	inline void set_replacementMaterial_2(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___replacementMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___replacementMaterial_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride
struct AtlasMaterialOverride_tBE247B8D7F31F3D8513F0F4789E683C137E32B04_marshaled_pinvoke
{
	int32_t ___overrideDisabled_0;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___originalMaterial_1;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___replacementMaterial_2;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride
struct AtlasMaterialOverride_tBE247B8D7F31F3D8513F0F4789E683C137E32B04_marshaled_com
{
	int32_t ___overrideDisabled_0;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___originalMaterial_1;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___replacementMaterial_2;
};
#endif // ATLASMATERIALOVERRIDE_TBE247B8D7F31F3D8513F0F4789E683C137E32B04_H
#ifndef SLOTMATERIALOVERRIDE_TB3B24DEF79660E3AAB3498522B88CB4155039A39_H
#define SLOTMATERIALOVERRIDE_TB3B24DEF79660E3AAB3498522B88CB4155039A39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride
struct  SlotMaterialOverride_tB3B24DEF79660E3AAB3498522B88CB4155039A39 
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride::overrideDisabled
	bool ___overrideDisabled_0;
	// System.String Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride::slotName
	String_t* ___slotName_1;
	// UnityEngine.Material Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_2;

public:
	inline static int32_t get_offset_of_overrideDisabled_0() { return static_cast<int32_t>(offsetof(SlotMaterialOverride_tB3B24DEF79660E3AAB3498522B88CB4155039A39, ___overrideDisabled_0)); }
	inline bool get_overrideDisabled_0() const { return ___overrideDisabled_0; }
	inline bool* get_address_of_overrideDisabled_0() { return &___overrideDisabled_0; }
	inline void set_overrideDisabled_0(bool value)
	{
		___overrideDisabled_0 = value;
	}

	inline static int32_t get_offset_of_slotName_1() { return static_cast<int32_t>(offsetof(SlotMaterialOverride_tB3B24DEF79660E3AAB3498522B88CB4155039A39, ___slotName_1)); }
	inline String_t* get_slotName_1() const { return ___slotName_1; }
	inline String_t** get_address_of_slotName_1() { return &___slotName_1; }
	inline void set_slotName_1(String_t* value)
	{
		___slotName_1 = value;
		Il2CppCodeGenWriteBarrier((&___slotName_1), value);
	}

	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(SlotMaterialOverride_tB3B24DEF79660E3AAB3498522B88CB4155039A39, ___material_2)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_2() const { return ___material_2; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((&___material_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride
struct SlotMaterialOverride_tB3B24DEF79660E3AAB3498522B88CB4155039A39_marshaled_pinvoke
{
	int32_t ___overrideDisabled_0;
	char* ___slotName_1;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_2;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride
struct SlotMaterialOverride_tB3B24DEF79660E3AAB3498522B88CB4155039A39_marshaled_com
{
	int32_t ___overrideDisabled_0;
	Il2CppChar* ___slotName_1;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_2;
};
#endif // SLOTMATERIALOVERRIDE_TB3B24DEF79660E3AAB3498522B88CB4155039A39_H
#ifndef TRANSFORMPAIR_T6B3FF6F7FDA997AA7F77E791DC21438B8873B22E_H
#define TRANSFORMPAIR_T6B3FF6F7FDA997AA7F77E791DC21438B8873B22E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair
struct  TransformPair_t6B3FF6F7FDA997AA7F77E791DC21438B8873B22E 
{
public:
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair::dest
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___dest_0;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair::src
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___src_1;

public:
	inline static int32_t get_offset_of_dest_0() { return static_cast<int32_t>(offsetof(TransformPair_t6B3FF6F7FDA997AA7F77E791DC21438B8873B22E, ___dest_0)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_dest_0() const { return ___dest_0; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_dest_0() { return &___dest_0; }
	inline void set_dest_0(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___dest_0 = value;
		Il2CppCodeGenWriteBarrier((&___dest_0), value);
	}

	inline static int32_t get_offset_of_src_1() { return static_cast<int32_t>(offsetof(TransformPair_t6B3FF6F7FDA997AA7F77E791DC21438B8873B22E, ___src_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_src_1() const { return ___src_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_src_1() { return &___src_1; }
	inline void set_src_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___src_1 = value;
		Il2CppCodeGenWriteBarrier((&___src_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair
struct TransformPair_t6B3FF6F7FDA997AA7F77E791DC21438B8873B22E_marshaled_pinvoke
{
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___dest_0;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___src_1;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair
struct TransformPair_t6B3FF6F7FDA997AA7F77E791DC21438B8873B22E_marshaled_com
{
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___dest_0;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___src_1;
};
#endif // TRANSFORMPAIR_T6B3FF6F7FDA997AA7F77E791DC21438B8873B22E_H
#ifndef MATERIALTEXTUREPAIR_TC333F7CBCFAB82A2F3735B0BFA42576FFC13C8AC_H
#define MATERIALTEXTUREPAIR_TC333F7CBCFAB82A2F3735B0BFA42576FFC13C8AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair
struct  MaterialTexturePair_tC333F7CBCFAB82A2F3735B0BFA42576FFC13C8AC 
{
public:
	// UnityEngine.Texture2D Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair::texture2D
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture2D_0;
	// UnityEngine.Material Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_1;

public:
	inline static int32_t get_offset_of_texture2D_0() { return static_cast<int32_t>(offsetof(MaterialTexturePair_tC333F7CBCFAB82A2F3735B0BFA42576FFC13C8AC, ___texture2D_0)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_texture2D_0() const { return ___texture2D_0; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_texture2D_0() { return &___texture2D_0; }
	inline void set_texture2D_0(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___texture2D_0 = value;
		Il2CppCodeGenWriteBarrier((&___texture2D_0), value);
	}

	inline static int32_t get_offset_of_material_1() { return static_cast<int32_t>(offsetof(MaterialTexturePair_tC333F7CBCFAB82A2F3735B0BFA42576FFC13C8AC, ___material_1)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_1() const { return ___material_1; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_1() { return &___material_1; }
	inline void set_material_1(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_1 = value;
		Il2CppCodeGenWriteBarrier((&___material_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair
struct MaterialTexturePair_tC333F7CBCFAB82A2F3735B0BFA42576FFC13C8AC_marshaled_pinvoke
{
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture2D_0;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_1;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair
struct MaterialTexturePair_tC333F7CBCFAB82A2F3735B0BFA42576FFC13C8AC_marshaled_com
{
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture2D_0;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_1;
};
#endif // MATERIALTEXTUREPAIR_TC333F7CBCFAB82A2F3735B0BFA42576FFC13C8AC_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#define LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifndef PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#define PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::23447D2BC3FF6984AB09F575BC63CFE460337394
	__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  ___23447D2BC3FF6984AB09F575BC63CFE460337394_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A
	__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  ___6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::B6E6EA57C32297E83203480EE50A22C3581AA09C
	__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  ___B6E6EA57C32297E83203480EE50A22C3581AA09C_2;

public:
	inline static int32_t get_offset_of_U323447D2BC3FF6984AB09F575BC63CFE460337394_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___23447D2BC3FF6984AB09F575BC63CFE460337394_0)); }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  get_U323447D2BC3FF6984AB09F575BC63CFE460337394_0() const { return ___23447D2BC3FF6984AB09F575BC63CFE460337394_0; }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 * get_address_of_U323447D2BC3FF6984AB09F575BC63CFE460337394_0() { return &___23447D2BC3FF6984AB09F575BC63CFE460337394_0; }
	inline void set_U323447D2BC3FF6984AB09F575BC63CFE460337394_0(__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  value)
	{
		___23447D2BC3FF6984AB09F575BC63CFE460337394_0 = value;
	}

	inline static int32_t get_offset_of_U36D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1)); }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  get_U36D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() const { return ___6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1; }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 * get_address_of_U36D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() { return &___6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1; }
	inline void set_U36D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1(__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  value)
	{
		___6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1 = value;
	}

	inline static int32_t get_offset_of_B6E6EA57C32297E83203480EE50A22C3581AA09C_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___B6E6EA57C32297E83203480EE50A22C3581AA09C_2)); }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  get_B6E6EA57C32297E83203480EE50A22C3581AA09C_2() const { return ___B6E6EA57C32297E83203480EE50A22C3581AA09C_2; }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 * get_address_of_B6E6EA57C32297E83203480EE50A22C3581AA09C_2() { return &___B6E6EA57C32297E83203480EE50A22C3581AA09C_2; }
	inline void set_B6E6EA57C32297E83203480EE50A22C3581AA09C_2(__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  value)
	{
		___B6E6EA57C32297E83203480EE50A22C3581AA09C_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifndef U3CFADEADDITIVEU3ED__8_TB399326982888CAED12990EDC25131F37ED15F7A_H
#define U3CFADEADDITIVEU3ED__8_TB399326982888CAED12990EDC25131F37ED15F7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>d__8
struct  U3CFadeAdditiveU3Ed__8_tB399326982888CAED12990EDC25131F37ED15F7A  : public RuntimeObject
{
public:
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Spine.Unity.Modules.SkeletonGhostRenderer Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>d__8::<>4__this
	SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6 * ___U3CU3E4__this_2;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>d__8::<black>5__2
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___U3CblackU3E5__2_3;
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>d__8::<t>5__3
	int32_t ___U3CtU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ed__8_tB399326982888CAED12990EDC25131F37ED15F7A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ed__8_tB399326982888CAED12990EDC25131F37ED15F7A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ed__8_tB399326982888CAED12990EDC25131F37ED15F7A, ___U3CU3E4__this_2)); }
	inline SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CblackU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ed__8_tB399326982888CAED12990EDC25131F37ED15F7A, ___U3CblackU3E5__2_3)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_U3CblackU3E5__2_3() const { return ___U3CblackU3E5__2_3; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_U3CblackU3E5__2_3() { return &___U3CblackU3E5__2_3; }
	inline void set_U3CblackU3E5__2_3(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___U3CblackU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ed__8_tB399326982888CAED12990EDC25131F37ED15F7A, ___U3CtU3E5__3_4)); }
	inline int32_t get_U3CtU3E5__3_4() const { return ___U3CtU3E5__3_4; }
	inline int32_t* get_address_of_U3CtU3E5__3_4() { return &___U3CtU3E5__3_4; }
	inline void set_U3CtU3E5__3_4(int32_t value)
	{
		___U3CtU3E5__3_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEADDITIVEU3ED__8_TB399326982888CAED12990EDC25131F37ED15F7A_H
#ifndef LAYERFIELDATTRIBUTE_T2FC9981248BCEC5DD4E73C6F32449DFDD7CBDBC1_H
#define LAYERFIELDATTRIBUTE_T2FC9981248BCEC5DD4E73C6F32449DFDD7CBDBC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll/LayerFieldAttribute
struct  LayerFieldAttribute_t2FC9981248BCEC5DD4E73C6F32449DFDD7CBDBC1  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERFIELDATTRIBUTE_T2FC9981248BCEC5DD4E73C6F32449DFDD7CBDBC1_H
#ifndef PHYSICSSYSTEM_T03BDDD6C578659C24D71BA8F0347C300BA88D218_H
#define PHYSICSSYSTEM_T03BDDD6C578659C24D71BA8F0347C300BA88D218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityKinematicShadow/PhysicsSystem
struct  PhysicsSystem_t03BDDD6C578659C24D71BA8F0347C300BA88D218 
{
public:
	// System.Int32 Spine.Unity.Modules.SkeletonUtilityKinematicShadow/PhysicsSystem::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PhysicsSystem_t03BDDD6C578659C24D71BA8F0347C300BA88D218, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICSSYSTEM_T03BDDD6C578659C24D71BA8F0347C300BA88D218_H
#ifndef HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#define HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#define TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#ifndef ATLASUTILITIES_TA9C61E45558430A4D715DD7155EC0C582E845E28_H
#define ATLASUTILITIES_TA9C61E45558430A4D715DD7155EC0C582E845E28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.AtlasUtilities
struct  AtlasUtilities_tA9C61E45558430A4D715DD7155EC0C582E845E28  : public RuntimeObject
{
public:

public:
};

struct AtlasUtilities_tA9C61E45558430A4D715DD7155EC0C582E845E28_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Spine.AtlasRegion,UnityEngine.Texture2D> Spine.Unity.Modules.AttachmentTools.AtlasUtilities::CachedRegionTextures
	Dictionary_2_t917CD6850A65021B15B8A9BF5060E9F630AD53A2 * ___CachedRegionTextures_5;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> Spine.Unity.Modules.AttachmentTools.AtlasUtilities::CachedRegionTexturesList
	List_1_tD84B8B60A0D0578F20C5B6EE527F0C57547C88B8 * ___CachedRegionTexturesList_6;

public:
	inline static int32_t get_offset_of_CachedRegionTextures_5() { return static_cast<int32_t>(offsetof(AtlasUtilities_tA9C61E45558430A4D715DD7155EC0C582E845E28_StaticFields, ___CachedRegionTextures_5)); }
	inline Dictionary_2_t917CD6850A65021B15B8A9BF5060E9F630AD53A2 * get_CachedRegionTextures_5() const { return ___CachedRegionTextures_5; }
	inline Dictionary_2_t917CD6850A65021B15B8A9BF5060E9F630AD53A2 ** get_address_of_CachedRegionTextures_5() { return &___CachedRegionTextures_5; }
	inline void set_CachedRegionTextures_5(Dictionary_2_t917CD6850A65021B15B8A9BF5060E9F630AD53A2 * value)
	{
		___CachedRegionTextures_5 = value;
		Il2CppCodeGenWriteBarrier((&___CachedRegionTextures_5), value);
	}

	inline static int32_t get_offset_of_CachedRegionTexturesList_6() { return static_cast<int32_t>(offsetof(AtlasUtilities_tA9C61E45558430A4D715DD7155EC0C582E845E28_StaticFields, ___CachedRegionTexturesList_6)); }
	inline List_1_tD84B8B60A0D0578F20C5B6EE527F0C57547C88B8 * get_CachedRegionTexturesList_6() const { return ___CachedRegionTexturesList_6; }
	inline List_1_tD84B8B60A0D0578F20C5B6EE527F0C57547C88B8 ** get_address_of_CachedRegionTexturesList_6() { return &___CachedRegionTexturesList_6; }
	inline void set_CachedRegionTexturesList_6(List_1_tD84B8B60A0D0578F20C5B6EE527F0C57547C88B8 * value)
	{
		___CachedRegionTexturesList_6 = value;
		Il2CppCodeGenWriteBarrier((&___CachedRegionTexturesList_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASUTILITIES_TA9C61E45558430A4D715DD7155EC0C582E845E28_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef SKELETONGHOST_T226F5FBA7A7E7588518FE0936F72727552D6EFC9_H
#define SKELETONGHOST_T226F5FBA7A7E7588518FE0936F72727552D6EFC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhost
struct  SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonGhost::ghostingEnabled
	bool ___ghostingEnabled_6;
	// System.Single Spine.Unity.Modules.SkeletonGhost::spawnRate
	float ___spawnRate_7;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhost::color
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_8;
	// System.Boolean Spine.Unity.Modules.SkeletonGhost::additive
	bool ___additive_9;
	// System.Int32 Spine.Unity.Modules.SkeletonGhost::maximumGhosts
	int32_t ___maximumGhosts_10;
	// System.Single Spine.Unity.Modules.SkeletonGhost::fadeSpeed
	float ___fadeSpeed_11;
	// UnityEngine.Shader Spine.Unity.Modules.SkeletonGhost::ghostShader
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___ghostShader_12;
	// System.Single Spine.Unity.Modules.SkeletonGhost::textureFade
	float ___textureFade_13;
	// System.Boolean Spine.Unity.Modules.SkeletonGhost::sortWithDistanceOnly
	bool ___sortWithDistanceOnly_14;
	// System.Single Spine.Unity.Modules.SkeletonGhost::zOffset
	float ___zOffset_15;
	// System.Single Spine.Unity.Modules.SkeletonGhost::nextSpawnTime
	float ___nextSpawnTime_16;
	// Spine.Unity.Modules.SkeletonGhostRenderer[] Spine.Unity.Modules.SkeletonGhost::pool
	SkeletonGhostRendererU5BU5D_t684BE74B1631BEFA357B9DEAB343C897B20E4804* ___pool_17;
	// System.Int32 Spine.Unity.Modules.SkeletonGhost::poolIndex
	int32_t ___poolIndex_18;
	// Spine.Unity.SkeletonRenderer Spine.Unity.Modules.SkeletonGhost::skeletonRenderer
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * ___skeletonRenderer_19;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonGhost::meshRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___meshRenderer_20;
	// UnityEngine.MeshFilter Spine.Unity.Modules.SkeletonGhost::meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___meshFilter_21;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material> Spine.Unity.Modules.SkeletonGhost::materialTable
	Dictionary_2_t87B6F3EB6208E12A08960AA208E68480EA2C7926 * ___materialTable_22;

public:
	inline static int32_t get_offset_of_ghostingEnabled_6() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___ghostingEnabled_6)); }
	inline bool get_ghostingEnabled_6() const { return ___ghostingEnabled_6; }
	inline bool* get_address_of_ghostingEnabled_6() { return &___ghostingEnabled_6; }
	inline void set_ghostingEnabled_6(bool value)
	{
		___ghostingEnabled_6 = value;
	}

	inline static int32_t get_offset_of_spawnRate_7() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___spawnRate_7)); }
	inline float get_spawnRate_7() const { return ___spawnRate_7; }
	inline float* get_address_of_spawnRate_7() { return &___spawnRate_7; }
	inline void set_spawnRate_7(float value)
	{
		___spawnRate_7 = value;
	}

	inline static int32_t get_offset_of_color_8() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___color_8)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_color_8() const { return ___color_8; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_color_8() { return &___color_8; }
	inline void set_color_8(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___color_8 = value;
	}

	inline static int32_t get_offset_of_additive_9() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___additive_9)); }
	inline bool get_additive_9() const { return ___additive_9; }
	inline bool* get_address_of_additive_9() { return &___additive_9; }
	inline void set_additive_9(bool value)
	{
		___additive_9 = value;
	}

	inline static int32_t get_offset_of_maximumGhosts_10() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___maximumGhosts_10)); }
	inline int32_t get_maximumGhosts_10() const { return ___maximumGhosts_10; }
	inline int32_t* get_address_of_maximumGhosts_10() { return &___maximumGhosts_10; }
	inline void set_maximumGhosts_10(int32_t value)
	{
		___maximumGhosts_10 = value;
	}

	inline static int32_t get_offset_of_fadeSpeed_11() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___fadeSpeed_11)); }
	inline float get_fadeSpeed_11() const { return ___fadeSpeed_11; }
	inline float* get_address_of_fadeSpeed_11() { return &___fadeSpeed_11; }
	inline void set_fadeSpeed_11(float value)
	{
		___fadeSpeed_11 = value;
	}

	inline static int32_t get_offset_of_ghostShader_12() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___ghostShader_12)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_ghostShader_12() const { return ___ghostShader_12; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_ghostShader_12() { return &___ghostShader_12; }
	inline void set_ghostShader_12(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___ghostShader_12 = value;
		Il2CppCodeGenWriteBarrier((&___ghostShader_12), value);
	}

	inline static int32_t get_offset_of_textureFade_13() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___textureFade_13)); }
	inline float get_textureFade_13() const { return ___textureFade_13; }
	inline float* get_address_of_textureFade_13() { return &___textureFade_13; }
	inline void set_textureFade_13(float value)
	{
		___textureFade_13 = value;
	}

	inline static int32_t get_offset_of_sortWithDistanceOnly_14() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___sortWithDistanceOnly_14)); }
	inline bool get_sortWithDistanceOnly_14() const { return ___sortWithDistanceOnly_14; }
	inline bool* get_address_of_sortWithDistanceOnly_14() { return &___sortWithDistanceOnly_14; }
	inline void set_sortWithDistanceOnly_14(bool value)
	{
		___sortWithDistanceOnly_14 = value;
	}

	inline static int32_t get_offset_of_zOffset_15() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___zOffset_15)); }
	inline float get_zOffset_15() const { return ___zOffset_15; }
	inline float* get_address_of_zOffset_15() { return &___zOffset_15; }
	inline void set_zOffset_15(float value)
	{
		___zOffset_15 = value;
	}

	inline static int32_t get_offset_of_nextSpawnTime_16() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___nextSpawnTime_16)); }
	inline float get_nextSpawnTime_16() const { return ___nextSpawnTime_16; }
	inline float* get_address_of_nextSpawnTime_16() { return &___nextSpawnTime_16; }
	inline void set_nextSpawnTime_16(float value)
	{
		___nextSpawnTime_16 = value;
	}

	inline static int32_t get_offset_of_pool_17() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___pool_17)); }
	inline SkeletonGhostRendererU5BU5D_t684BE74B1631BEFA357B9DEAB343C897B20E4804* get_pool_17() const { return ___pool_17; }
	inline SkeletonGhostRendererU5BU5D_t684BE74B1631BEFA357B9DEAB343C897B20E4804** get_address_of_pool_17() { return &___pool_17; }
	inline void set_pool_17(SkeletonGhostRendererU5BU5D_t684BE74B1631BEFA357B9DEAB343C897B20E4804* value)
	{
		___pool_17 = value;
		Il2CppCodeGenWriteBarrier((&___pool_17), value);
	}

	inline static int32_t get_offset_of_poolIndex_18() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___poolIndex_18)); }
	inline int32_t get_poolIndex_18() const { return ___poolIndex_18; }
	inline int32_t* get_address_of_poolIndex_18() { return &___poolIndex_18; }
	inline void set_poolIndex_18(int32_t value)
	{
		___poolIndex_18 = value;
	}

	inline static int32_t get_offset_of_skeletonRenderer_19() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___skeletonRenderer_19)); }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * get_skeletonRenderer_19() const { return ___skeletonRenderer_19; }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 ** get_address_of_skeletonRenderer_19() { return &___skeletonRenderer_19; }
	inline void set_skeletonRenderer_19(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * value)
	{
		___skeletonRenderer_19 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_19), value);
	}

	inline static int32_t get_offset_of_meshRenderer_20() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___meshRenderer_20)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_meshRenderer_20() const { return ___meshRenderer_20; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_meshRenderer_20() { return &___meshRenderer_20; }
	inline void set_meshRenderer_20(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___meshRenderer_20 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_20), value);
	}

	inline static int32_t get_offset_of_meshFilter_21() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___meshFilter_21)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_meshFilter_21() const { return ___meshFilter_21; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_meshFilter_21() { return &___meshFilter_21; }
	inline void set_meshFilter_21(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___meshFilter_21 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_21), value);
	}

	inline static int32_t get_offset_of_materialTable_22() { return static_cast<int32_t>(offsetof(SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9, ___materialTable_22)); }
	inline Dictionary_2_t87B6F3EB6208E12A08960AA208E68480EA2C7926 * get_materialTable_22() const { return ___materialTable_22; }
	inline Dictionary_2_t87B6F3EB6208E12A08960AA208E68480EA2C7926 ** get_address_of_materialTable_22() { return &___materialTable_22; }
	inline void set_materialTable_22(Dictionary_2_t87B6F3EB6208E12A08960AA208E68480EA2C7926 * value)
	{
		___materialTable_22 = value;
		Il2CppCodeGenWriteBarrier((&___materialTable_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGHOST_T226F5FBA7A7E7588518FE0936F72727552D6EFC9_H
#ifndef SKELETONGHOSTRENDERER_TEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6_H
#define SKELETONGHOSTRENDERER_TEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhostRenderer
struct  SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Spine.Unity.Modules.SkeletonGhostRenderer::fadeSpeed
	float ___fadeSpeed_4;
	// UnityEngine.Color32[] Spine.Unity.Modules.SkeletonGhostRenderer::colors
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___colors_5;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer::black
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___black_6;
	// UnityEngine.MeshFilter Spine.Unity.Modules.SkeletonGhostRenderer::meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___meshFilter_7;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonGhostRenderer::meshRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___meshRenderer_8;

public:
	inline static int32_t get_offset_of_fadeSpeed_4() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6, ___fadeSpeed_4)); }
	inline float get_fadeSpeed_4() const { return ___fadeSpeed_4; }
	inline float* get_address_of_fadeSpeed_4() { return &___fadeSpeed_4; }
	inline void set_fadeSpeed_4(float value)
	{
		___fadeSpeed_4 = value;
	}

	inline static int32_t get_offset_of_colors_5() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6, ___colors_5)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_colors_5() const { return ___colors_5; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_colors_5() { return &___colors_5; }
	inline void set_colors_5(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___colors_5 = value;
		Il2CppCodeGenWriteBarrier((&___colors_5), value);
	}

	inline static int32_t get_offset_of_black_6() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6, ___black_6)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_black_6() const { return ___black_6; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_black_6() { return &___black_6; }
	inline void set_black_6(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___black_6 = value;
	}

	inline static int32_t get_offset_of_meshFilter_7() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6, ___meshFilter_7)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_meshFilter_7() const { return ___meshFilter_7; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_meshFilter_7() { return &___meshFilter_7; }
	inline void set_meshFilter_7(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___meshFilter_7 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_7), value);
	}

	inline static int32_t get_offset_of_meshRenderer_8() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6, ___meshRenderer_8)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_meshRenderer_8() const { return ___meshRenderer_8; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_meshRenderer_8() { return &___meshRenderer_8; }
	inline void set_meshRenderer_8(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___meshRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGHOSTRENDERER_TEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6_H
#ifndef SKELETONGRAPHICMIRROR_T51407345B7C22DCBDF0A2E52A33A375801DDAB86_H
#define SKELETONGRAPHICMIRROR_T51407345B7C22DCBDF0A2E52A33A375801DDAB86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGraphicMirror
struct  SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.Modules.SkeletonGraphicMirror::source
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * ___source_4;
	// System.Boolean Spine.Unity.Modules.SkeletonGraphicMirror::mirrorOnStart
	bool ___mirrorOnStart_5;
	// System.Boolean Spine.Unity.Modules.SkeletonGraphicMirror::restoreOnDisable
	bool ___restoreOnDisable_6;
	// Spine.Unity.SkeletonGraphic Spine.Unity.Modules.SkeletonGraphicMirror::skeletonGraphic
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * ___skeletonGraphic_7;
	// Spine.Skeleton Spine.Unity.Modules.SkeletonGraphicMirror::originalSkeleton
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * ___originalSkeleton_8;
	// System.Boolean Spine.Unity.Modules.SkeletonGraphicMirror::originalFreeze
	bool ___originalFreeze_9;
	// UnityEngine.Texture2D Spine.Unity.Modules.SkeletonGraphicMirror::overrideTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___overrideTexture_10;

public:
	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86, ___source_4)); }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * get_source_4() const { return ___source_4; }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier((&___source_4), value);
	}

	inline static int32_t get_offset_of_mirrorOnStart_5() { return static_cast<int32_t>(offsetof(SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86, ___mirrorOnStart_5)); }
	inline bool get_mirrorOnStart_5() const { return ___mirrorOnStart_5; }
	inline bool* get_address_of_mirrorOnStart_5() { return &___mirrorOnStart_5; }
	inline void set_mirrorOnStart_5(bool value)
	{
		___mirrorOnStart_5 = value;
	}

	inline static int32_t get_offset_of_restoreOnDisable_6() { return static_cast<int32_t>(offsetof(SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86, ___restoreOnDisable_6)); }
	inline bool get_restoreOnDisable_6() const { return ___restoreOnDisable_6; }
	inline bool* get_address_of_restoreOnDisable_6() { return &___restoreOnDisable_6; }
	inline void set_restoreOnDisable_6(bool value)
	{
		___restoreOnDisable_6 = value;
	}

	inline static int32_t get_offset_of_skeletonGraphic_7() { return static_cast<int32_t>(offsetof(SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86, ___skeletonGraphic_7)); }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * get_skeletonGraphic_7() const { return ___skeletonGraphic_7; }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A ** get_address_of_skeletonGraphic_7() { return &___skeletonGraphic_7; }
	inline void set_skeletonGraphic_7(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * value)
	{
		___skeletonGraphic_7 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonGraphic_7), value);
	}

	inline static int32_t get_offset_of_originalSkeleton_8() { return static_cast<int32_t>(offsetof(SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86, ___originalSkeleton_8)); }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * get_originalSkeleton_8() const { return ___originalSkeleton_8; }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 ** get_address_of_originalSkeleton_8() { return &___originalSkeleton_8; }
	inline void set_originalSkeleton_8(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * value)
	{
		___originalSkeleton_8 = value;
		Il2CppCodeGenWriteBarrier((&___originalSkeleton_8), value);
	}

	inline static int32_t get_offset_of_originalFreeze_9() { return static_cast<int32_t>(offsetof(SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86, ___originalFreeze_9)); }
	inline bool get_originalFreeze_9() const { return ___originalFreeze_9; }
	inline bool* get_address_of_originalFreeze_9() { return &___originalFreeze_9; }
	inline void set_originalFreeze_9(bool value)
	{
		___originalFreeze_9 = value;
	}

	inline static int32_t get_offset_of_overrideTexture_10() { return static_cast<int32_t>(offsetof(SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86, ___overrideTexture_10)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_overrideTexture_10() const { return ___overrideTexture_10; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_overrideTexture_10() { return &___overrideTexture_10; }
	inline void set_overrideTexture_10(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___overrideTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___overrideTexture_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGRAPHICMIRROR_T51407345B7C22DCBDF0A2E52A33A375801DDAB86_H
#ifndef SKELETONPARTSRENDERER_TFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7_H
#define SKELETONPARTSRENDERER_TFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonPartsRenderer
struct  SkeletonPartsRenderer_tFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Spine.Unity.MeshGenerator Spine.Unity.Modules.SkeletonPartsRenderer::meshGenerator
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * ___meshGenerator_4;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonPartsRenderer::meshRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___meshRenderer_5;
	// UnityEngine.MeshFilter Spine.Unity.Modules.SkeletonPartsRenderer::meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___meshFilter_6;
	// Spine.Unity.MeshRendererBuffers Spine.Unity.Modules.SkeletonPartsRenderer::buffers
	MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107 * ___buffers_7;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.Modules.SkeletonPartsRenderer::currentInstructions
	SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * ___currentInstructions_8;

public:
	inline static int32_t get_offset_of_meshGenerator_4() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_tFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7, ___meshGenerator_4)); }
	inline MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * get_meshGenerator_4() const { return ___meshGenerator_4; }
	inline MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 ** get_address_of_meshGenerator_4() { return &___meshGenerator_4; }
	inline void set_meshGenerator_4(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * value)
	{
		___meshGenerator_4 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_4), value);
	}

	inline static int32_t get_offset_of_meshRenderer_5() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_tFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7, ___meshRenderer_5)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_meshRenderer_5() const { return ___meshRenderer_5; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_meshRenderer_5() { return &___meshRenderer_5; }
	inline void set_meshRenderer_5(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___meshRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_5), value);
	}

	inline static int32_t get_offset_of_meshFilter_6() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_tFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7, ___meshFilter_6)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_meshFilter_6() const { return ___meshFilter_6; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_meshFilter_6() { return &___meshFilter_6; }
	inline void set_meshFilter_6(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___meshFilter_6 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_6), value);
	}

	inline static int32_t get_offset_of_buffers_7() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_tFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7, ___buffers_7)); }
	inline MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107 * get_buffers_7() const { return ___buffers_7; }
	inline MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107 ** get_address_of_buffers_7() { return &___buffers_7; }
	inline void set_buffers_7(MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107 * value)
	{
		___buffers_7 = value;
		Il2CppCodeGenWriteBarrier((&___buffers_7), value);
	}

	inline static int32_t get_offset_of_currentInstructions_8() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_tFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7, ___currentInstructions_8)); }
	inline SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * get_currentInstructions_8() const { return ___currentInstructions_8; }
	inline SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A ** get_address_of_currentInstructions_8() { return &___currentInstructions_8; }
	inline void set_currentInstructions_8(SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * value)
	{
		___currentInstructions_8 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONPARTSRENDERER_TFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7_H
#ifndef SKELETONRAGDOLL_TC76A9A9F49054BE7EC030246835E87CF2323FDD0_H
#define SKELETONRAGDOLL_TC76A9A9F49054BE7EC030246835E87CF2323FDD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll
struct  SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Spine.Unity.Modules.SkeletonRagdoll::startingBoneName
	String_t* ___startingBoneName_5;
	// System.Collections.Generic.List`1<System.String> Spine.Unity.Modules.SkeletonRagdoll::stopBoneNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___stopBoneNames_6;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::applyOnStart
	bool ___applyOnStart_7;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::disableIK
	bool ___disableIK_8;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::disableOtherConstraints
	bool ___disableOtherConstraints_9;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::pinStartBone
	bool ___pinStartBone_10;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::enableJointCollision
	bool ___enableJointCollision_11;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::useGravity
	bool ___useGravity_12;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::thickness
	float ___thickness_13;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::rotationLimit
	float ___rotationLimit_14;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::rootMass
	float ___rootMass_15;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::massFalloffFactor
	float ___massFalloffFactor_16;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll::colliderLayer
	int32_t ___colliderLayer_17;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::mix
	float ___mix_18;
	// Spine.Unity.ISkeletonAnimation Spine.Unity.Modules.SkeletonRagdoll::targetSkeletonComponent
	RuntimeObject* ___targetSkeletonComponent_19;
	// Spine.Skeleton Spine.Unity.Modules.SkeletonRagdoll::skeleton
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * ___skeleton_20;
	// System.Collections.Generic.Dictionary`2<Spine.Bone,UnityEngine.Transform> Spine.Unity.Modules.SkeletonRagdoll::boneTable
	Dictionary_2_t0933035DE5961A26FC54331E79B0DB3951A2AF94 * ___boneTable_21;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll::ragdollRoot
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___ragdollRoot_22;
	// UnityEngine.Rigidbody Spine.Unity.Modules.SkeletonRagdoll::<RootRigidbody>k__BackingField
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___U3CRootRigidbodyU3Ek__BackingField_23;
	// Spine.Bone Spine.Unity.Modules.SkeletonRagdoll::<StartingBone>k__BackingField
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * ___U3CStartingBoneU3Ek__BackingField_24;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonRagdoll::rootOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rootOffset_25;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::isActive
	bool ___isActive_26;

public:
	inline static int32_t get_offset_of_startingBoneName_5() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___startingBoneName_5)); }
	inline String_t* get_startingBoneName_5() const { return ___startingBoneName_5; }
	inline String_t** get_address_of_startingBoneName_5() { return &___startingBoneName_5; }
	inline void set_startingBoneName_5(String_t* value)
	{
		___startingBoneName_5 = value;
		Il2CppCodeGenWriteBarrier((&___startingBoneName_5), value);
	}

	inline static int32_t get_offset_of_stopBoneNames_6() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___stopBoneNames_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_stopBoneNames_6() const { return ___stopBoneNames_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_stopBoneNames_6() { return &___stopBoneNames_6; }
	inline void set_stopBoneNames_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___stopBoneNames_6 = value;
		Il2CppCodeGenWriteBarrier((&___stopBoneNames_6), value);
	}

	inline static int32_t get_offset_of_applyOnStart_7() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___applyOnStart_7)); }
	inline bool get_applyOnStart_7() const { return ___applyOnStart_7; }
	inline bool* get_address_of_applyOnStart_7() { return &___applyOnStart_7; }
	inline void set_applyOnStart_7(bool value)
	{
		___applyOnStart_7 = value;
	}

	inline static int32_t get_offset_of_disableIK_8() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___disableIK_8)); }
	inline bool get_disableIK_8() const { return ___disableIK_8; }
	inline bool* get_address_of_disableIK_8() { return &___disableIK_8; }
	inline void set_disableIK_8(bool value)
	{
		___disableIK_8 = value;
	}

	inline static int32_t get_offset_of_disableOtherConstraints_9() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___disableOtherConstraints_9)); }
	inline bool get_disableOtherConstraints_9() const { return ___disableOtherConstraints_9; }
	inline bool* get_address_of_disableOtherConstraints_9() { return &___disableOtherConstraints_9; }
	inline void set_disableOtherConstraints_9(bool value)
	{
		___disableOtherConstraints_9 = value;
	}

	inline static int32_t get_offset_of_pinStartBone_10() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___pinStartBone_10)); }
	inline bool get_pinStartBone_10() const { return ___pinStartBone_10; }
	inline bool* get_address_of_pinStartBone_10() { return &___pinStartBone_10; }
	inline void set_pinStartBone_10(bool value)
	{
		___pinStartBone_10 = value;
	}

	inline static int32_t get_offset_of_enableJointCollision_11() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___enableJointCollision_11)); }
	inline bool get_enableJointCollision_11() const { return ___enableJointCollision_11; }
	inline bool* get_address_of_enableJointCollision_11() { return &___enableJointCollision_11; }
	inline void set_enableJointCollision_11(bool value)
	{
		___enableJointCollision_11 = value;
	}

	inline static int32_t get_offset_of_useGravity_12() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___useGravity_12)); }
	inline bool get_useGravity_12() const { return ___useGravity_12; }
	inline bool* get_address_of_useGravity_12() { return &___useGravity_12; }
	inline void set_useGravity_12(bool value)
	{
		___useGravity_12 = value;
	}

	inline static int32_t get_offset_of_thickness_13() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___thickness_13)); }
	inline float get_thickness_13() const { return ___thickness_13; }
	inline float* get_address_of_thickness_13() { return &___thickness_13; }
	inline void set_thickness_13(float value)
	{
		___thickness_13 = value;
	}

	inline static int32_t get_offset_of_rotationLimit_14() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___rotationLimit_14)); }
	inline float get_rotationLimit_14() const { return ___rotationLimit_14; }
	inline float* get_address_of_rotationLimit_14() { return &___rotationLimit_14; }
	inline void set_rotationLimit_14(float value)
	{
		___rotationLimit_14 = value;
	}

	inline static int32_t get_offset_of_rootMass_15() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___rootMass_15)); }
	inline float get_rootMass_15() const { return ___rootMass_15; }
	inline float* get_address_of_rootMass_15() { return &___rootMass_15; }
	inline void set_rootMass_15(float value)
	{
		___rootMass_15 = value;
	}

	inline static int32_t get_offset_of_massFalloffFactor_16() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___massFalloffFactor_16)); }
	inline float get_massFalloffFactor_16() const { return ___massFalloffFactor_16; }
	inline float* get_address_of_massFalloffFactor_16() { return &___massFalloffFactor_16; }
	inline void set_massFalloffFactor_16(float value)
	{
		___massFalloffFactor_16 = value;
	}

	inline static int32_t get_offset_of_colliderLayer_17() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___colliderLayer_17)); }
	inline int32_t get_colliderLayer_17() const { return ___colliderLayer_17; }
	inline int32_t* get_address_of_colliderLayer_17() { return &___colliderLayer_17; }
	inline void set_colliderLayer_17(int32_t value)
	{
		___colliderLayer_17 = value;
	}

	inline static int32_t get_offset_of_mix_18() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___mix_18)); }
	inline float get_mix_18() const { return ___mix_18; }
	inline float* get_address_of_mix_18() { return &___mix_18; }
	inline void set_mix_18(float value)
	{
		___mix_18 = value;
	}

	inline static int32_t get_offset_of_targetSkeletonComponent_19() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___targetSkeletonComponent_19)); }
	inline RuntimeObject* get_targetSkeletonComponent_19() const { return ___targetSkeletonComponent_19; }
	inline RuntimeObject** get_address_of_targetSkeletonComponent_19() { return &___targetSkeletonComponent_19; }
	inline void set_targetSkeletonComponent_19(RuntimeObject* value)
	{
		___targetSkeletonComponent_19 = value;
		Il2CppCodeGenWriteBarrier((&___targetSkeletonComponent_19), value);
	}

	inline static int32_t get_offset_of_skeleton_20() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___skeleton_20)); }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * get_skeleton_20() const { return ___skeleton_20; }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 ** get_address_of_skeleton_20() { return &___skeleton_20; }
	inline void set_skeleton_20(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * value)
	{
		___skeleton_20 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_20), value);
	}

	inline static int32_t get_offset_of_boneTable_21() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___boneTable_21)); }
	inline Dictionary_2_t0933035DE5961A26FC54331E79B0DB3951A2AF94 * get_boneTable_21() const { return ___boneTable_21; }
	inline Dictionary_2_t0933035DE5961A26FC54331E79B0DB3951A2AF94 ** get_address_of_boneTable_21() { return &___boneTable_21; }
	inline void set_boneTable_21(Dictionary_2_t0933035DE5961A26FC54331E79B0DB3951A2AF94 * value)
	{
		___boneTable_21 = value;
		Il2CppCodeGenWriteBarrier((&___boneTable_21), value);
	}

	inline static int32_t get_offset_of_ragdollRoot_22() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___ragdollRoot_22)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_ragdollRoot_22() const { return ___ragdollRoot_22; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_ragdollRoot_22() { return &___ragdollRoot_22; }
	inline void set_ragdollRoot_22(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___ragdollRoot_22 = value;
		Il2CppCodeGenWriteBarrier((&___ragdollRoot_22), value);
	}

	inline static int32_t get_offset_of_U3CRootRigidbodyU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___U3CRootRigidbodyU3Ek__BackingField_23)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_U3CRootRigidbodyU3Ek__BackingField_23() const { return ___U3CRootRigidbodyU3Ek__BackingField_23; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_U3CRootRigidbodyU3Ek__BackingField_23() { return &___U3CRootRigidbodyU3Ek__BackingField_23; }
	inline void set_U3CRootRigidbodyU3Ek__BackingField_23(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___U3CRootRigidbodyU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootRigidbodyU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CStartingBoneU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___U3CStartingBoneU3Ek__BackingField_24)); }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * get_U3CStartingBoneU3Ek__BackingField_24() const { return ___U3CStartingBoneU3Ek__BackingField_24; }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E ** get_address_of_U3CStartingBoneU3Ek__BackingField_24() { return &___U3CStartingBoneU3Ek__BackingField_24; }
	inline void set_U3CStartingBoneU3Ek__BackingField_24(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * value)
	{
		___U3CStartingBoneU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStartingBoneU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of_rootOffset_25() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___rootOffset_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rootOffset_25() const { return ___rootOffset_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rootOffset_25() { return &___rootOffset_25; }
	inline void set_rootOffset_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rootOffset_25 = value;
	}

	inline static int32_t get_offset_of_isActive_26() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0, ___isActive_26)); }
	inline bool get_isActive_26() const { return ___isActive_26; }
	inline bool* get_address_of_isActive_26() { return &___isActive_26; }
	inline void set_isActive_26(bool value)
	{
		___isActive_26 = value;
	}
};

struct SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0_StaticFields
{
public:
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll::parentSpaceHelper
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parentSpaceHelper_4;

public:
	inline static int32_t get_offset_of_parentSpaceHelper_4() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0_StaticFields, ___parentSpaceHelper_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_parentSpaceHelper_4() const { return ___parentSpaceHelper_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_parentSpaceHelper_4() { return &___parentSpaceHelper_4; }
	inline void set_parentSpaceHelper_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___parentSpaceHelper_4 = value;
		Il2CppCodeGenWriteBarrier((&___parentSpaceHelper_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRAGDOLL_TC76A9A9F49054BE7EC030246835E87CF2323FDD0_H
#ifndef SKELETONRAGDOLL2D_TFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4_H
#define SKELETONRAGDOLL2D_TFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll2D
struct  SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Spine.Unity.Modules.SkeletonRagdoll2D::startingBoneName
	String_t* ___startingBoneName_5;
	// System.Collections.Generic.List`1<System.String> Spine.Unity.Modules.SkeletonRagdoll2D::stopBoneNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___stopBoneNames_6;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::applyOnStart
	bool ___applyOnStart_7;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::disableIK
	bool ___disableIK_8;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::disableOtherConstraints
	bool ___disableOtherConstraints_9;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::pinStartBone
	bool ___pinStartBone_10;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::gravityScale
	float ___gravityScale_11;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::thickness
	float ___thickness_12;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::rotationLimit
	float ___rotationLimit_13;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::rootMass
	float ___rootMass_14;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::massFalloffFactor
	float ___massFalloffFactor_15;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll2D::colliderLayer
	int32_t ___colliderLayer_16;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::mix
	float ___mix_17;
	// Spine.Unity.ISkeletonAnimation Spine.Unity.Modules.SkeletonRagdoll2D::targetSkeletonComponent
	RuntimeObject* ___targetSkeletonComponent_18;
	// Spine.Skeleton Spine.Unity.Modules.SkeletonRagdoll2D::skeleton
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * ___skeleton_19;
	// System.Collections.Generic.Dictionary`2<Spine.Bone,UnityEngine.Transform> Spine.Unity.Modules.SkeletonRagdoll2D::boneTable
	Dictionary_2_t0933035DE5961A26FC54331E79B0DB3951A2AF94 * ___boneTable_20;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll2D::ragdollRoot
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___ragdollRoot_21;
	// UnityEngine.Rigidbody2D Spine.Unity.Modules.SkeletonRagdoll2D::<RootRigidbody>k__BackingField
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___U3CRootRigidbodyU3Ek__BackingField_22;
	// Spine.Bone Spine.Unity.Modules.SkeletonRagdoll2D::<StartingBone>k__BackingField
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * ___U3CStartingBoneU3Ek__BackingField_23;
	// UnityEngine.Vector2 Spine.Unity.Modules.SkeletonRagdoll2D::rootOffset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rootOffset_24;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::isActive
	bool ___isActive_25;

public:
	inline static int32_t get_offset_of_startingBoneName_5() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___startingBoneName_5)); }
	inline String_t* get_startingBoneName_5() const { return ___startingBoneName_5; }
	inline String_t** get_address_of_startingBoneName_5() { return &___startingBoneName_5; }
	inline void set_startingBoneName_5(String_t* value)
	{
		___startingBoneName_5 = value;
		Il2CppCodeGenWriteBarrier((&___startingBoneName_5), value);
	}

	inline static int32_t get_offset_of_stopBoneNames_6() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___stopBoneNames_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_stopBoneNames_6() const { return ___stopBoneNames_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_stopBoneNames_6() { return &___stopBoneNames_6; }
	inline void set_stopBoneNames_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___stopBoneNames_6 = value;
		Il2CppCodeGenWriteBarrier((&___stopBoneNames_6), value);
	}

	inline static int32_t get_offset_of_applyOnStart_7() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___applyOnStart_7)); }
	inline bool get_applyOnStart_7() const { return ___applyOnStart_7; }
	inline bool* get_address_of_applyOnStart_7() { return &___applyOnStart_7; }
	inline void set_applyOnStart_7(bool value)
	{
		___applyOnStart_7 = value;
	}

	inline static int32_t get_offset_of_disableIK_8() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___disableIK_8)); }
	inline bool get_disableIK_8() const { return ___disableIK_8; }
	inline bool* get_address_of_disableIK_8() { return &___disableIK_8; }
	inline void set_disableIK_8(bool value)
	{
		___disableIK_8 = value;
	}

	inline static int32_t get_offset_of_disableOtherConstraints_9() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___disableOtherConstraints_9)); }
	inline bool get_disableOtherConstraints_9() const { return ___disableOtherConstraints_9; }
	inline bool* get_address_of_disableOtherConstraints_9() { return &___disableOtherConstraints_9; }
	inline void set_disableOtherConstraints_9(bool value)
	{
		___disableOtherConstraints_9 = value;
	}

	inline static int32_t get_offset_of_pinStartBone_10() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___pinStartBone_10)); }
	inline bool get_pinStartBone_10() const { return ___pinStartBone_10; }
	inline bool* get_address_of_pinStartBone_10() { return &___pinStartBone_10; }
	inline void set_pinStartBone_10(bool value)
	{
		___pinStartBone_10 = value;
	}

	inline static int32_t get_offset_of_gravityScale_11() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___gravityScale_11)); }
	inline float get_gravityScale_11() const { return ___gravityScale_11; }
	inline float* get_address_of_gravityScale_11() { return &___gravityScale_11; }
	inline void set_gravityScale_11(float value)
	{
		___gravityScale_11 = value;
	}

	inline static int32_t get_offset_of_thickness_12() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___thickness_12)); }
	inline float get_thickness_12() const { return ___thickness_12; }
	inline float* get_address_of_thickness_12() { return &___thickness_12; }
	inline void set_thickness_12(float value)
	{
		___thickness_12 = value;
	}

	inline static int32_t get_offset_of_rotationLimit_13() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___rotationLimit_13)); }
	inline float get_rotationLimit_13() const { return ___rotationLimit_13; }
	inline float* get_address_of_rotationLimit_13() { return &___rotationLimit_13; }
	inline void set_rotationLimit_13(float value)
	{
		___rotationLimit_13 = value;
	}

	inline static int32_t get_offset_of_rootMass_14() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___rootMass_14)); }
	inline float get_rootMass_14() const { return ___rootMass_14; }
	inline float* get_address_of_rootMass_14() { return &___rootMass_14; }
	inline void set_rootMass_14(float value)
	{
		___rootMass_14 = value;
	}

	inline static int32_t get_offset_of_massFalloffFactor_15() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___massFalloffFactor_15)); }
	inline float get_massFalloffFactor_15() const { return ___massFalloffFactor_15; }
	inline float* get_address_of_massFalloffFactor_15() { return &___massFalloffFactor_15; }
	inline void set_massFalloffFactor_15(float value)
	{
		___massFalloffFactor_15 = value;
	}

	inline static int32_t get_offset_of_colliderLayer_16() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___colliderLayer_16)); }
	inline int32_t get_colliderLayer_16() const { return ___colliderLayer_16; }
	inline int32_t* get_address_of_colliderLayer_16() { return &___colliderLayer_16; }
	inline void set_colliderLayer_16(int32_t value)
	{
		___colliderLayer_16 = value;
	}

	inline static int32_t get_offset_of_mix_17() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___mix_17)); }
	inline float get_mix_17() const { return ___mix_17; }
	inline float* get_address_of_mix_17() { return &___mix_17; }
	inline void set_mix_17(float value)
	{
		___mix_17 = value;
	}

	inline static int32_t get_offset_of_targetSkeletonComponent_18() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___targetSkeletonComponent_18)); }
	inline RuntimeObject* get_targetSkeletonComponent_18() const { return ___targetSkeletonComponent_18; }
	inline RuntimeObject** get_address_of_targetSkeletonComponent_18() { return &___targetSkeletonComponent_18; }
	inline void set_targetSkeletonComponent_18(RuntimeObject* value)
	{
		___targetSkeletonComponent_18 = value;
		Il2CppCodeGenWriteBarrier((&___targetSkeletonComponent_18), value);
	}

	inline static int32_t get_offset_of_skeleton_19() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___skeleton_19)); }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * get_skeleton_19() const { return ___skeleton_19; }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 ** get_address_of_skeleton_19() { return &___skeleton_19; }
	inline void set_skeleton_19(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * value)
	{
		___skeleton_19 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_19), value);
	}

	inline static int32_t get_offset_of_boneTable_20() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___boneTable_20)); }
	inline Dictionary_2_t0933035DE5961A26FC54331E79B0DB3951A2AF94 * get_boneTable_20() const { return ___boneTable_20; }
	inline Dictionary_2_t0933035DE5961A26FC54331E79B0DB3951A2AF94 ** get_address_of_boneTable_20() { return &___boneTable_20; }
	inline void set_boneTable_20(Dictionary_2_t0933035DE5961A26FC54331E79B0DB3951A2AF94 * value)
	{
		___boneTable_20 = value;
		Il2CppCodeGenWriteBarrier((&___boneTable_20), value);
	}

	inline static int32_t get_offset_of_ragdollRoot_21() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___ragdollRoot_21)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_ragdollRoot_21() const { return ___ragdollRoot_21; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_ragdollRoot_21() { return &___ragdollRoot_21; }
	inline void set_ragdollRoot_21(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___ragdollRoot_21 = value;
		Il2CppCodeGenWriteBarrier((&___ragdollRoot_21), value);
	}

	inline static int32_t get_offset_of_U3CRootRigidbodyU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___U3CRootRigidbodyU3Ek__BackingField_22)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_U3CRootRigidbodyU3Ek__BackingField_22() const { return ___U3CRootRigidbodyU3Ek__BackingField_22; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_U3CRootRigidbodyU3Ek__BackingField_22() { return &___U3CRootRigidbodyU3Ek__BackingField_22; }
	inline void set_U3CRootRigidbodyU3Ek__BackingField_22(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___U3CRootRigidbodyU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootRigidbodyU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CStartingBoneU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___U3CStartingBoneU3Ek__BackingField_23)); }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * get_U3CStartingBoneU3Ek__BackingField_23() const { return ___U3CStartingBoneU3Ek__BackingField_23; }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E ** get_address_of_U3CStartingBoneU3Ek__BackingField_23() { return &___U3CStartingBoneU3Ek__BackingField_23; }
	inline void set_U3CStartingBoneU3Ek__BackingField_23(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * value)
	{
		___U3CStartingBoneU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStartingBoneU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_rootOffset_24() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___rootOffset_24)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rootOffset_24() const { return ___rootOffset_24; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rootOffset_24() { return &___rootOffset_24; }
	inline void set_rootOffset_24(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rootOffset_24 = value;
	}

	inline static int32_t get_offset_of_isActive_25() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4, ___isActive_25)); }
	inline bool get_isActive_25() const { return ___isActive_25; }
	inline bool* get_address_of_isActive_25() { return &___isActive_25; }
	inline void set_isActive_25(bool value)
	{
		___isActive_25 = value;
	}
};

struct SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4_StaticFields
{
public:
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll2D::parentSpaceHelper
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parentSpaceHelper_4;

public:
	inline static int32_t get_offset_of_parentSpaceHelper_4() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4_StaticFields, ___parentSpaceHelper_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_parentSpaceHelper_4() const { return ___parentSpaceHelper_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_parentSpaceHelper_4() { return &___parentSpaceHelper_4; }
	inline void set_parentSpaceHelper_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___parentSpaceHelper_4 = value;
		Il2CppCodeGenWriteBarrier((&___parentSpaceHelper_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRAGDOLL2D_TFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4_H
#ifndef SKELETONRENDERSEPARATOR_T6D6C646C1E36D701FEA4D0F612C11130C55ADE2F_H
#define SKELETONRENDERSEPARATOR_T6D6C646C1E36D701FEA4D0F612C11130C55ADE2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRenderSeparator
struct  SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.Modules.SkeletonRenderSeparator::skeletonRenderer
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * ___skeletonRenderer_5;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonRenderSeparator::mainMeshRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___mainMeshRenderer_6;
	// System.Boolean Spine.Unity.Modules.SkeletonRenderSeparator::copyPropertyBlock
	bool ___copyPropertyBlock_7;
	// System.Boolean Spine.Unity.Modules.SkeletonRenderSeparator::copyMeshRendererFlags
	bool ___copyMeshRendererFlags_8;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonPartsRenderer> Spine.Unity.Modules.SkeletonRenderSeparator::partsRenderers
	List_1_tC62EA284874C134BCFA59817E630FFFD009FDEA2 * ___partsRenderers_9;
	// UnityEngine.MaterialPropertyBlock Spine.Unity.Modules.SkeletonRenderSeparator::copiedBlock
	MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * ___copiedBlock_10;

public:
	inline static int32_t get_offset_of_skeletonRenderer_5() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F, ___skeletonRenderer_5)); }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * get_skeletonRenderer_5() const { return ___skeletonRenderer_5; }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 ** get_address_of_skeletonRenderer_5() { return &___skeletonRenderer_5; }
	inline void set_skeletonRenderer_5(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * value)
	{
		___skeletonRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_5), value);
	}

	inline static int32_t get_offset_of_mainMeshRenderer_6() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F, ___mainMeshRenderer_6)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_mainMeshRenderer_6() const { return ___mainMeshRenderer_6; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_mainMeshRenderer_6() { return &___mainMeshRenderer_6; }
	inline void set_mainMeshRenderer_6(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___mainMeshRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainMeshRenderer_6), value);
	}

	inline static int32_t get_offset_of_copyPropertyBlock_7() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F, ___copyPropertyBlock_7)); }
	inline bool get_copyPropertyBlock_7() const { return ___copyPropertyBlock_7; }
	inline bool* get_address_of_copyPropertyBlock_7() { return &___copyPropertyBlock_7; }
	inline void set_copyPropertyBlock_7(bool value)
	{
		___copyPropertyBlock_7 = value;
	}

	inline static int32_t get_offset_of_copyMeshRendererFlags_8() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F, ___copyMeshRendererFlags_8)); }
	inline bool get_copyMeshRendererFlags_8() const { return ___copyMeshRendererFlags_8; }
	inline bool* get_address_of_copyMeshRendererFlags_8() { return &___copyMeshRendererFlags_8; }
	inline void set_copyMeshRendererFlags_8(bool value)
	{
		___copyMeshRendererFlags_8 = value;
	}

	inline static int32_t get_offset_of_partsRenderers_9() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F, ___partsRenderers_9)); }
	inline List_1_tC62EA284874C134BCFA59817E630FFFD009FDEA2 * get_partsRenderers_9() const { return ___partsRenderers_9; }
	inline List_1_tC62EA284874C134BCFA59817E630FFFD009FDEA2 ** get_address_of_partsRenderers_9() { return &___partsRenderers_9; }
	inline void set_partsRenderers_9(List_1_tC62EA284874C134BCFA59817E630FFFD009FDEA2 * value)
	{
		___partsRenderers_9 = value;
		Il2CppCodeGenWriteBarrier((&___partsRenderers_9), value);
	}

	inline static int32_t get_offset_of_copiedBlock_10() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F, ___copiedBlock_10)); }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * get_copiedBlock_10() const { return ___copiedBlock_10; }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 ** get_address_of_copiedBlock_10() { return &___copiedBlock_10; }
	inline void set_copiedBlock_10(MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * value)
	{
		___copiedBlock_10 = value;
		Il2CppCodeGenWriteBarrier((&___copiedBlock_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERSEPARATOR_T6D6C646C1E36D701FEA4D0F612C11130C55ADE2F_H
#ifndef SKELETONRENDERERCUSTOMMATERIALS_TF8328F988989FBE47D96F21ACDADC59F5C5449F4_H
#define SKELETONRENDERERCUSTOMMATERIALS_TF8328F988989FBE47D96F21ACDADC59F5C5449F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRendererCustomMaterials
struct  SkeletonRendererCustomMaterials_tF8328F988989FBE47D96F21ACDADC59F5C5449F4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.Modules.SkeletonRendererCustomMaterials::skeletonRenderer
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * ___skeletonRenderer_4;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride> Spine.Unity.Modules.SkeletonRendererCustomMaterials::customSlotMaterials
	List_1_tAFA7DD41E205AC6A69EE6C6F5F28D8B6975844C9 * ___customSlotMaterials_5;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride> Spine.Unity.Modules.SkeletonRendererCustomMaterials::customMaterialOverrides
	List_1_t641FD355D7A09DCD729156F64BA661AEF7501211 * ___customMaterialOverrides_6;

public:
	inline static int32_t get_offset_of_skeletonRenderer_4() { return static_cast<int32_t>(offsetof(SkeletonRendererCustomMaterials_tF8328F988989FBE47D96F21ACDADC59F5C5449F4, ___skeletonRenderer_4)); }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * get_skeletonRenderer_4() const { return ___skeletonRenderer_4; }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 ** get_address_of_skeletonRenderer_4() { return &___skeletonRenderer_4; }
	inline void set_skeletonRenderer_4(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * value)
	{
		___skeletonRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_4), value);
	}

	inline static int32_t get_offset_of_customSlotMaterials_5() { return static_cast<int32_t>(offsetof(SkeletonRendererCustomMaterials_tF8328F988989FBE47D96F21ACDADC59F5C5449F4, ___customSlotMaterials_5)); }
	inline List_1_tAFA7DD41E205AC6A69EE6C6F5F28D8B6975844C9 * get_customSlotMaterials_5() const { return ___customSlotMaterials_5; }
	inline List_1_tAFA7DD41E205AC6A69EE6C6F5F28D8B6975844C9 ** get_address_of_customSlotMaterials_5() { return &___customSlotMaterials_5; }
	inline void set_customSlotMaterials_5(List_1_tAFA7DD41E205AC6A69EE6C6F5F28D8B6975844C9 * value)
	{
		___customSlotMaterials_5 = value;
		Il2CppCodeGenWriteBarrier((&___customSlotMaterials_5), value);
	}

	inline static int32_t get_offset_of_customMaterialOverrides_6() { return static_cast<int32_t>(offsetof(SkeletonRendererCustomMaterials_tF8328F988989FBE47D96F21ACDADC59F5C5449F4, ___customMaterialOverrides_6)); }
	inline List_1_t641FD355D7A09DCD729156F64BA661AEF7501211 * get_customMaterialOverrides_6() const { return ___customMaterialOverrides_6; }
	inline List_1_t641FD355D7A09DCD729156F64BA661AEF7501211 ** get_address_of_customMaterialOverrides_6() { return &___customMaterialOverrides_6; }
	inline void set_customMaterialOverrides_6(List_1_t641FD355D7A09DCD729156F64BA661AEF7501211 * value)
	{
		___customMaterialOverrides_6 = value;
		Il2CppCodeGenWriteBarrier((&___customMaterialOverrides_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERERCUSTOMMATERIALS_TF8328F988989FBE47D96F21ACDADC59F5C5449F4_H
#ifndef SKELETONUTILITYKINEMATICSHADOW_T64E3056E78EC108325A2C392F9E2D35FA9B25DA4_H
#define SKELETONUTILITYKINEMATICSHADOW_T64E3056E78EC108325A2C392F9E2D35FA9B25DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityKinematicShadow
struct  SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityKinematicShadow::detachedShadow
	bool ___detachedShadow_4;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityKinematicShadow::parent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parent_5;
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityKinematicShadow::hideShadow
	bool ___hideShadow_6;
	// Spine.Unity.Modules.SkeletonUtilityKinematicShadow/PhysicsSystem Spine.Unity.Modules.SkeletonUtilityKinematicShadow::physicsSystem
	int32_t ___physicsSystem_7;
	// UnityEngine.GameObject Spine.Unity.Modules.SkeletonUtilityKinematicShadow::shadowRoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___shadowRoot_8;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair> Spine.Unity.Modules.SkeletonUtilityKinematicShadow::shadowTable
	List_1_t941310C818AB9A721ED8F4B0F2D7A14D9069AC8D * ___shadowTable_9;

public:
	inline static int32_t get_offset_of_detachedShadow_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4, ___detachedShadow_4)); }
	inline bool get_detachedShadow_4() const { return ___detachedShadow_4; }
	inline bool* get_address_of_detachedShadow_4() { return &___detachedShadow_4; }
	inline void set_detachedShadow_4(bool value)
	{
		___detachedShadow_4 = value;
	}

	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4, ___parent_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_parent_5() const { return ___parent_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier((&___parent_5), value);
	}

	inline static int32_t get_offset_of_hideShadow_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4, ___hideShadow_6)); }
	inline bool get_hideShadow_6() const { return ___hideShadow_6; }
	inline bool* get_address_of_hideShadow_6() { return &___hideShadow_6; }
	inline void set_hideShadow_6(bool value)
	{
		___hideShadow_6 = value;
	}

	inline static int32_t get_offset_of_physicsSystem_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4, ___physicsSystem_7)); }
	inline int32_t get_physicsSystem_7() const { return ___physicsSystem_7; }
	inline int32_t* get_address_of_physicsSystem_7() { return &___physicsSystem_7; }
	inline void set_physicsSystem_7(int32_t value)
	{
		___physicsSystem_7 = value;
	}

	inline static int32_t get_offset_of_shadowRoot_8() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4, ___shadowRoot_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_shadowRoot_8() const { return ___shadowRoot_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_shadowRoot_8() { return &___shadowRoot_8; }
	inline void set_shadowRoot_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___shadowRoot_8 = value;
		Il2CppCodeGenWriteBarrier((&___shadowRoot_8), value);
	}

	inline static int32_t get_offset_of_shadowTable_9() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4, ___shadowTable_9)); }
	inline List_1_t941310C818AB9A721ED8F4B0F2D7A14D9069AC8D * get_shadowTable_9() const { return ___shadowTable_9; }
	inline List_1_t941310C818AB9A721ED8F4B0F2D7A14D9069AC8D ** get_address_of_shadowTable_9() { return &___shadowTable_9; }
	inline void set_shadowTable_9(List_1_t941310C818AB9A721ED8F4B0F2D7A14D9069AC8D * value)
	{
		___shadowTable_9 = value;
		Il2CppCodeGenWriteBarrier((&___shadowTable_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYKINEMATICSHADOW_T64E3056E78EC108325A2C392F9E2D35FA9B25DA4_H
#ifndef SLOTBLENDMODES_T84FAB47A99585B431051AA1057D53ACEB1835E3B_H
#define SLOTBLENDMODES_T84FAB47A99585B431051AA1057D53ACEB1835E3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SlotBlendModes
struct  SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Material Spine.Unity.Modules.SlotBlendModes::multiplyMaterialSource
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___multiplyMaterialSource_5;
	// UnityEngine.Material Spine.Unity.Modules.SlotBlendModes::screenMaterialSource
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___screenMaterialSource_6;
	// UnityEngine.Texture2D Spine.Unity.Modules.SlotBlendModes::texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture_7;
	// System.Boolean Spine.Unity.Modules.SlotBlendModes::<Applied>k__BackingField
	bool ___U3CAppliedU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_multiplyMaterialSource_5() { return static_cast<int32_t>(offsetof(SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B, ___multiplyMaterialSource_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_multiplyMaterialSource_5() const { return ___multiplyMaterialSource_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_multiplyMaterialSource_5() { return &___multiplyMaterialSource_5; }
	inline void set_multiplyMaterialSource_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___multiplyMaterialSource_5 = value;
		Il2CppCodeGenWriteBarrier((&___multiplyMaterialSource_5), value);
	}

	inline static int32_t get_offset_of_screenMaterialSource_6() { return static_cast<int32_t>(offsetof(SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B, ___screenMaterialSource_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_screenMaterialSource_6() const { return ___screenMaterialSource_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_screenMaterialSource_6() { return &___screenMaterialSource_6; }
	inline void set_screenMaterialSource_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___screenMaterialSource_6 = value;
		Il2CppCodeGenWriteBarrier((&___screenMaterialSource_6), value);
	}

	inline static int32_t get_offset_of_texture_7() { return static_cast<int32_t>(offsetof(SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B, ___texture_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_texture_7() const { return ___texture_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_texture_7() { return &___texture_7; }
	inline void set_texture_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___texture_7 = value;
		Il2CppCodeGenWriteBarrier((&___texture_7), value);
	}

	inline static int32_t get_offset_of_U3CAppliedU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B, ___U3CAppliedU3Ek__BackingField_8)); }
	inline bool get_U3CAppliedU3Ek__BackingField_8() const { return ___U3CAppliedU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CAppliedU3Ek__BackingField_8() { return &___U3CAppliedU3Ek__BackingField_8; }
	inline void set_U3CAppliedU3Ek__BackingField_8(bool value)
	{
		___U3CAppliedU3Ek__BackingField_8 = value;
	}
};

struct SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair,UnityEngine.Material> Spine.Unity.Modules.SlotBlendModes::materialTable
	Dictionary_2_t54C49C1E7A32D13A8D7285BC47E6BEEE576F454C * ___materialTable_4;

public:
	inline static int32_t get_offset_of_materialTable_4() { return static_cast<int32_t>(offsetof(SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B_StaticFields, ___materialTable_4)); }
	inline Dictionary_2_t54C49C1E7A32D13A8D7285BC47E6BEEE576F454C * get_materialTable_4() const { return ___materialTable_4; }
	inline Dictionary_2_t54C49C1E7A32D13A8D7285BC47E6BEEE576F454C ** get_address_of_materialTable_4() { return &___materialTable_4; }
	inline void set_materialTable_4(Dictionary_2_t54C49C1E7A32D13A8D7285BC47E6BEEE576F454C * value)
	{
		___materialTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___materialTable_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTBLENDMODES_T84FAB47A99585B431051AA1057D53ACEB1835E3B_H
#ifndef SKELETONUTILITYCONSTRAINT_TC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0_H
#define SKELETONUTILITYCONSTRAINT_TC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityConstraint
struct  SkeletonUtilityConstraint_tC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Spine.Unity.SkeletonUtilityBone Spine.Unity.SkeletonUtilityConstraint::utilBone
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39 * ___utilBone_4;
	// Spine.Unity.SkeletonUtility Spine.Unity.SkeletonUtilityConstraint::skeletonUtility
	SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0 * ___skeletonUtility_5;

public:
	inline static int32_t get_offset_of_utilBone_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityConstraint_tC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0, ___utilBone_4)); }
	inline SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39 * get_utilBone_4() const { return ___utilBone_4; }
	inline SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39 ** get_address_of_utilBone_4() { return &___utilBone_4; }
	inline void set_utilBone_4(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39 * value)
	{
		___utilBone_4 = value;
		Il2CppCodeGenWriteBarrier((&___utilBone_4), value);
	}

	inline static int32_t get_offset_of_skeletonUtility_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityConstraint_tC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0, ___skeletonUtility_5)); }
	inline SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0 * get_skeletonUtility_5() const { return ___skeletonUtility_5; }
	inline SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0 ** get_address_of_skeletonUtility_5() { return &___skeletonUtility_5; }
	inline void set_skeletonUtility_5(SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0 * value)
	{
		___skeletonUtility_5 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonUtility_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYCONSTRAINT_TC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0_H
#ifndef SKELETONUTILITYEYECONSTRAINT_TDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A_H
#define SKELETONUTILITYEYECONSTRAINT_TDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityEyeConstraint
struct  SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A  : public SkeletonUtilityConstraint_tC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0
{
public:
	// UnityEngine.Transform[] Spine.Unity.Modules.SkeletonUtilityEyeConstraint::eyes
	TransformU5BU5D_t3EB9781D1A1DE2674F0632C956A66AA423343139* ___eyes_6;
	// System.Single Spine.Unity.Modules.SkeletonUtilityEyeConstraint::radius
	float ___radius_7;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityEyeConstraint::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_8;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityEyeConstraint::targetPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPosition_9;
	// System.Single Spine.Unity.Modules.SkeletonUtilityEyeConstraint::speed
	float ___speed_10;
	// UnityEngine.Vector3[] Spine.Unity.Modules.SkeletonUtilityEyeConstraint::origins
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___origins_11;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityEyeConstraint::centerPoint
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___centerPoint_12;

public:
	inline static int32_t get_offset_of_eyes_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A, ___eyes_6)); }
	inline TransformU5BU5D_t3EB9781D1A1DE2674F0632C956A66AA423343139* get_eyes_6() const { return ___eyes_6; }
	inline TransformU5BU5D_t3EB9781D1A1DE2674F0632C956A66AA423343139** get_address_of_eyes_6() { return &___eyes_6; }
	inline void set_eyes_6(TransformU5BU5D_t3EB9781D1A1DE2674F0632C956A66AA423343139* value)
	{
		___eyes_6 = value;
		Il2CppCodeGenWriteBarrier((&___eyes_6), value);
	}

	inline static int32_t get_offset_of_radius_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A, ___radius_7)); }
	inline float get_radius_7() const { return ___radius_7; }
	inline float* get_address_of_radius_7() { return &___radius_7; }
	inline void set_radius_7(float value)
	{
		___radius_7 = value;
	}

	inline static int32_t get_offset_of_target_8() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A, ___target_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_8() const { return ___target_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_8() { return &___target_8; }
	inline void set_target_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_8 = value;
		Il2CppCodeGenWriteBarrier((&___target_8), value);
	}

	inline static int32_t get_offset_of_targetPosition_9() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A, ___targetPosition_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPosition_9() const { return ___targetPosition_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPosition_9() { return &___targetPosition_9; }
	inline void set_targetPosition_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPosition_9 = value;
	}

	inline static int32_t get_offset_of_speed_10() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A, ___speed_10)); }
	inline float get_speed_10() const { return ___speed_10; }
	inline float* get_address_of_speed_10() { return &___speed_10; }
	inline void set_speed_10(float value)
	{
		___speed_10 = value;
	}

	inline static int32_t get_offset_of_origins_11() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A, ___origins_11)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_origins_11() const { return ___origins_11; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_origins_11() { return &___origins_11; }
	inline void set_origins_11(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___origins_11 = value;
		Il2CppCodeGenWriteBarrier((&___origins_11), value);
	}

	inline static int32_t get_offset_of_centerPoint_12() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A, ___centerPoint_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_centerPoint_12() const { return ___centerPoint_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_centerPoint_12() { return &___centerPoint_12; }
	inline void set_centerPoint_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___centerPoint_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYEYECONSTRAINT_TDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A_H
#ifndef SKELETONUTILITYGROUNDCONSTRAINT_TA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC_H
#define SKELETONUTILITYGROUNDCONSTRAINT_TA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityGroundConstraint
struct  SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC  : public SkeletonUtilityConstraint_tC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0
{
public:
	// UnityEngine.LayerMask Spine.Unity.Modules.SkeletonUtilityGroundConstraint::groundMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___groundMask_6;
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityGroundConstraint::use2D
	bool ___use2D_7;
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityGroundConstraint::useRadius
	bool ___useRadius_8;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::castRadius
	float ___castRadius_9;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::castDistance
	float ___castDistance_10;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::castOffset
	float ___castOffset_11;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::groundOffset
	float ___groundOffset_12;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::adjustSpeed
	float ___adjustSpeed_13;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityGroundConstraint::rayOrigin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rayOrigin_14;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityGroundConstraint::rayDir
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rayDir_15;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::hitY
	float ___hitY_16;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::lastHitY
	float ___lastHitY_17;

public:
	inline static int32_t get_offset_of_groundMask_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC, ___groundMask_6)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_groundMask_6() const { return ___groundMask_6; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_groundMask_6() { return &___groundMask_6; }
	inline void set_groundMask_6(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___groundMask_6 = value;
	}

	inline static int32_t get_offset_of_use2D_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC, ___use2D_7)); }
	inline bool get_use2D_7() const { return ___use2D_7; }
	inline bool* get_address_of_use2D_7() { return &___use2D_7; }
	inline void set_use2D_7(bool value)
	{
		___use2D_7 = value;
	}

	inline static int32_t get_offset_of_useRadius_8() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC, ___useRadius_8)); }
	inline bool get_useRadius_8() const { return ___useRadius_8; }
	inline bool* get_address_of_useRadius_8() { return &___useRadius_8; }
	inline void set_useRadius_8(bool value)
	{
		___useRadius_8 = value;
	}

	inline static int32_t get_offset_of_castRadius_9() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC, ___castRadius_9)); }
	inline float get_castRadius_9() const { return ___castRadius_9; }
	inline float* get_address_of_castRadius_9() { return &___castRadius_9; }
	inline void set_castRadius_9(float value)
	{
		___castRadius_9 = value;
	}

	inline static int32_t get_offset_of_castDistance_10() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC, ___castDistance_10)); }
	inline float get_castDistance_10() const { return ___castDistance_10; }
	inline float* get_address_of_castDistance_10() { return &___castDistance_10; }
	inline void set_castDistance_10(float value)
	{
		___castDistance_10 = value;
	}

	inline static int32_t get_offset_of_castOffset_11() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC, ___castOffset_11)); }
	inline float get_castOffset_11() const { return ___castOffset_11; }
	inline float* get_address_of_castOffset_11() { return &___castOffset_11; }
	inline void set_castOffset_11(float value)
	{
		___castOffset_11 = value;
	}

	inline static int32_t get_offset_of_groundOffset_12() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC, ___groundOffset_12)); }
	inline float get_groundOffset_12() const { return ___groundOffset_12; }
	inline float* get_address_of_groundOffset_12() { return &___groundOffset_12; }
	inline void set_groundOffset_12(float value)
	{
		___groundOffset_12 = value;
	}

	inline static int32_t get_offset_of_adjustSpeed_13() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC, ___adjustSpeed_13)); }
	inline float get_adjustSpeed_13() const { return ___adjustSpeed_13; }
	inline float* get_address_of_adjustSpeed_13() { return &___adjustSpeed_13; }
	inline void set_adjustSpeed_13(float value)
	{
		___adjustSpeed_13 = value;
	}

	inline static int32_t get_offset_of_rayOrigin_14() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC, ___rayOrigin_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rayOrigin_14() const { return ___rayOrigin_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rayOrigin_14() { return &___rayOrigin_14; }
	inline void set_rayOrigin_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rayOrigin_14 = value;
	}

	inline static int32_t get_offset_of_rayDir_15() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC, ___rayDir_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rayDir_15() const { return ___rayDir_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rayDir_15() { return &___rayDir_15; }
	inline void set_rayDir_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rayDir_15 = value;
	}

	inline static int32_t get_offset_of_hitY_16() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC, ___hitY_16)); }
	inline float get_hitY_16() const { return ___hitY_16; }
	inline float* get_address_of_hitY_16() { return &___hitY_16; }
	inline void set_hitY_16(float value)
	{
		___hitY_16 = value;
	}

	inline static int32_t get_offset_of_lastHitY_17() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC, ___lastHitY_17)); }
	inline float get_lastHitY_17() const { return ___lastHitY_17; }
	inline float* get_address_of_lastHitY_17() { return &___lastHitY_17; }
	inline void set_lastHitY_17(float value)
	{
		___lastHitY_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYGROUNDCONSTRAINT_TA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (SkeletonRendererCustomMaterials_tF8328F988989FBE47D96F21ACDADC59F5C5449F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3000[3] = 
{
	SkeletonRendererCustomMaterials_tF8328F988989FBE47D96F21ACDADC59F5C5449F4::get_offset_of_skeletonRenderer_4(),
	SkeletonRendererCustomMaterials_tF8328F988989FBE47D96F21ACDADC59F5C5449F4::get_offset_of_customSlotMaterials_5(),
	SkeletonRendererCustomMaterials_tF8328F988989FBE47D96F21ACDADC59F5C5449F4::get_offset_of_customMaterialOverrides_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (SlotMaterialOverride_tB3B24DEF79660E3AAB3498522B88CB4155039A39)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3001[3] = 
{
	SlotMaterialOverride_tB3B24DEF79660E3AAB3498522B88CB4155039A39::get_offset_of_overrideDisabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SlotMaterialOverride_tB3B24DEF79660E3AAB3498522B88CB4155039A39::get_offset_of_slotName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SlotMaterialOverride_tB3B24DEF79660E3AAB3498522B88CB4155039A39::get_offset_of_material_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (AtlasMaterialOverride_tBE247B8D7F31F3D8513F0F4789E683C137E32B04)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3002[3] = 
{
	AtlasMaterialOverride_tBE247B8D7F31F3D8513F0F4789E683C137E32B04::get_offset_of_overrideDisabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AtlasMaterialOverride_tBE247B8D7F31F3D8513F0F4789E683C137E32B04::get_offset_of_originalMaterial_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AtlasMaterialOverride_tBE247B8D7F31F3D8513F0F4789E683C137E32B04::get_offset_of_replacementMaterial_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3003[19] = 
{
	0,
	0,
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_ghostingEnabled_6(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_spawnRate_7(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_color_8(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_additive_9(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_maximumGhosts_10(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_fadeSpeed_11(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_ghostShader_12(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_textureFade_13(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_sortWithDistanceOnly_14(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_zOffset_15(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_nextSpawnTime_16(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_pool_17(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_poolIndex_18(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_skeletonRenderer_19(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_meshRenderer_20(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_meshFilter_21(),
	SkeletonGhost_t226F5FBA7A7E7588518FE0936F72727552D6EFC9::get_offset_of_materialTable_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3004[5] = 
{
	SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6::get_offset_of_fadeSpeed_4(),
	SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6::get_offset_of_colors_5(),
	SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6::get_offset_of_black_6(),
	SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6::get_offset_of_meshFilter_7(),
	SkeletonGhostRenderer_tEFE5F6A274F142148E1AFEEEEB08C41454F9F4B6::get_offset_of_meshRenderer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (U3CFadeU3Ed__7_t543FB46255AB4D6A0BB976116220E57E7EBF7637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3005[4] = 
{
	U3CFadeU3Ed__7_t543FB46255AB4D6A0BB976116220E57E7EBF7637::get_offset_of_U3CU3E1__state_0(),
	U3CFadeU3Ed__7_t543FB46255AB4D6A0BB976116220E57E7EBF7637::get_offset_of_U3CU3E2__current_1(),
	U3CFadeU3Ed__7_t543FB46255AB4D6A0BB976116220E57E7EBF7637::get_offset_of_U3CU3E4__this_2(),
	U3CFadeU3Ed__7_t543FB46255AB4D6A0BB976116220E57E7EBF7637::get_offset_of_U3CtU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (U3CFadeAdditiveU3Ed__8_tB399326982888CAED12990EDC25131F37ED15F7A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3006[5] = 
{
	U3CFadeAdditiveU3Ed__8_tB399326982888CAED12990EDC25131F37ED15F7A::get_offset_of_U3CU3E1__state_0(),
	U3CFadeAdditiveU3Ed__8_tB399326982888CAED12990EDC25131F37ED15F7A::get_offset_of_U3CU3E2__current_1(),
	U3CFadeAdditiveU3Ed__8_tB399326982888CAED12990EDC25131F37ED15F7A::get_offset_of_U3CU3E4__this_2(),
	U3CFadeAdditiveU3Ed__8_tB399326982888CAED12990EDC25131F37ED15F7A::get_offset_of_U3CblackU3E5__2_3(),
	U3CFadeAdditiveU3Ed__8_tB399326982888CAED12990EDC25131F37ED15F7A::get_offset_of_U3CtU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0), -1, sizeof(SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3007[23] = 
{
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0_StaticFields::get_offset_of_parentSpaceHelper_4(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_startingBoneName_5(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_stopBoneNames_6(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_applyOnStart_7(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_disableIK_8(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_disableOtherConstraints_9(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_pinStartBone_10(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_enableJointCollision_11(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_useGravity_12(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_thickness_13(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_rotationLimit_14(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_rootMass_15(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_massFalloffFactor_16(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_colliderLayer_17(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_mix_18(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_targetSkeletonComponent_19(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_skeleton_20(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_boneTable_21(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_ragdollRoot_22(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_U3CRootRigidbodyU3Ek__BackingField_23(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_U3CStartingBoneU3Ek__BackingField_24(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_rootOffset_25(),
	SkeletonRagdoll_tC76A9A9F49054BE7EC030246835E87CF2323FDD0::get_offset_of_isActive_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (LayerFieldAttribute_t2FC9981248BCEC5DD4E73C6F32449DFDD7CBDBC1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (U3CStartU3Ed__33_t0F3854A111EB39AE09107EDAF5BD78E6F5164EE8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3009[3] = 
{
	U3CStartU3Ed__33_t0F3854A111EB39AE09107EDAF5BD78E6F5164EE8::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__33_t0F3854A111EB39AE09107EDAF5BD78E6F5164EE8::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__33_t0F3854A111EB39AE09107EDAF5BD78E6F5164EE8::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3010[7] = 
{
	U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394::get_offset_of_U3CU3E1__state_0(),
	U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394::get_offset_of_U3CU3E2__current_1(),
	U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394::get_offset_of_U3CU3E4__this_2(),
	U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394::get_offset_of_target_3(),
	U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394::get_offset_of_duration_4(),
	U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394::get_offset_of_U3CstartTimeU3E5__2_5(),
	U3CSmoothMixCoroutineU3Ed__40_tF117CA3E66591E1844AABF287CC86A08DF785394::get_offset_of_U3CstartMixU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4), -1, sizeof(SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3011[22] = 
{
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4_StaticFields::get_offset_of_parentSpaceHelper_4(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_startingBoneName_5(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_stopBoneNames_6(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_applyOnStart_7(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_disableIK_8(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_disableOtherConstraints_9(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_pinStartBone_10(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_gravityScale_11(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_thickness_12(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_rotationLimit_13(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_rootMass_14(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_massFalloffFactor_15(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_colliderLayer_16(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_mix_17(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_targetSkeletonComponent_18(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_skeleton_19(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_boneTable_20(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_ragdollRoot_21(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_U3CRootRigidbodyU3Ek__BackingField_22(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_U3CStartingBoneU3Ek__BackingField_23(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_rootOffset_24(),
	SkeletonRagdoll2D_tFFDA412A4CF0B6C844BB3E47F24A6410D4124AB4::get_offset_of_isActive_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (U3CStartU3Ed__32_tE8398F55DD18CE7B534129B6B2265BA36EE8AA13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3012[3] = 
{
	U3CStartU3Ed__32_tE8398F55DD18CE7B534129B6B2265BA36EE8AA13::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__32_tE8398F55DD18CE7B534129B6B2265BA36EE8AA13::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__32_tE8398F55DD18CE7B534129B6B2265BA36EE8AA13::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3013[7] = 
{
	U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276::get_offset_of_U3CU3E1__state_0(),
	U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276::get_offset_of_U3CU3E2__current_1(),
	U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276::get_offset_of_U3CU3E4__this_2(),
	U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276::get_offset_of_target_3(),
	U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276::get_offset_of_duration_4(),
	U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276::get_offset_of_U3CstartTimeU3E5__2_5(),
	U3CSmoothMixCoroutineU3Ed__39_tF4B7705E322F3B68C4C1EE92153E75E2478CF276::get_offset_of_U3CstartMixU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3014[7] = 
{
	SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86::get_offset_of_source_4(),
	SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86::get_offset_of_mirrorOnStart_5(),
	SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86::get_offset_of_restoreOnDisable_6(),
	SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86::get_offset_of_skeletonGraphic_7(),
	SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86::get_offset_of_originalSkeleton_8(),
	SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86::get_offset_of_originalFreeze_9(),
	SkeletonGraphicMirror_t51407345B7C22DCBDF0A2E52A33A375801DDAB86::get_offset_of_overrideTexture_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (SkeletonPartsRenderer_tFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3015[5] = 
{
	SkeletonPartsRenderer_tFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7::get_offset_of_meshGenerator_4(),
	SkeletonPartsRenderer_tFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7::get_offset_of_meshRenderer_5(),
	SkeletonPartsRenderer_tFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7::get_offset_of_meshFilter_6(),
	SkeletonPartsRenderer_tFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7::get_offset_of_buffers_7(),
	SkeletonPartsRenderer_tFD0773CBF68F3EC2CC0FE2C608743E5ED954C4A7::get_offset_of_currentInstructions_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3016[7] = 
{
	0,
	SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F::get_offset_of_skeletonRenderer_5(),
	SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F::get_offset_of_mainMeshRenderer_6(),
	SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F::get_offset_of_copyPropertyBlock_7(),
	SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F::get_offset_of_copyMeshRendererFlags_8(),
	SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F::get_offset_of_partsRenderers_9(),
	SkeletonRenderSeparator_t6D6C646C1E36D701FEA4D0F612C11130C55ADE2F::get_offset_of_copiedBlock_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3017[7] = 
{
	SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A::get_offset_of_eyes_6(),
	SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A::get_offset_of_radius_7(),
	SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A::get_offset_of_target_8(),
	SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A::get_offset_of_targetPosition_9(),
	SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A::get_offset_of_speed_10(),
	SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A::get_offset_of_origins_11(),
	SkeletonUtilityEyeConstraint_tDDC2CF67FDD6D6EDD872E4E8635A2361DC669E3A::get_offset_of_centerPoint_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3018[12] = 
{
	SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC::get_offset_of_groundMask_6(),
	SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC::get_offset_of_use2D_7(),
	SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC::get_offset_of_useRadius_8(),
	SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC::get_offset_of_castRadius_9(),
	SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC::get_offset_of_castDistance_10(),
	SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC::get_offset_of_castOffset_11(),
	SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC::get_offset_of_groundOffset_12(),
	SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC::get_offset_of_adjustSpeed_13(),
	SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC::get_offset_of_rayOrigin_14(),
	SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC::get_offset_of_rayDir_15(),
	SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC::get_offset_of_hitY_16(),
	SkeletonUtilityGroundConstraint_tA6E3E4A0A9FBCD7332349A228B9DEF5CBF63CECC::get_offset_of_lastHitY_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3019[6] = 
{
	SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4::get_offset_of_detachedShadow_4(),
	SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4::get_offset_of_parent_5(),
	SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4::get_offset_of_hideShadow_6(),
	SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4::get_offset_of_physicsSystem_7(),
	SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4::get_offset_of_shadowRoot_8(),
	SkeletonUtilityKinematicShadow_t64E3056E78EC108325A2C392F9E2D35FA9B25DA4::get_offset_of_shadowTable_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (TransformPair_t6B3FF6F7FDA997AA7F77E791DC21438B8873B22E)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3020[2] = 
{
	TransformPair_t6B3FF6F7FDA997AA7F77E791DC21438B8873B22E::get_offset_of_dest_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformPair_t6B3FF6F7FDA997AA7F77E791DC21438B8873B22E::get_offset_of_src_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (PhysicsSystem_t03BDDD6C578659C24D71BA8F0347C300BA88D218)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3021[3] = 
{
	PhysicsSystem_t03BDDD6C578659C24D71BA8F0347C300BA88D218::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B), -1, sizeof(SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3022[5] = 
{
	SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B_StaticFields::get_offset_of_materialTable_4(),
	SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B::get_offset_of_multiplyMaterialSource_5(),
	SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B::get_offset_of_screenMaterialSource_6(),
	SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B::get_offset_of_texture_7(),
	SlotBlendModes_t84FAB47A99585B431051AA1057D53ACEB1835E3B::get_offset_of_U3CAppliedU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (MaterialTexturePair_tC333F7CBCFAB82A2F3735B0BFA42576FFC13C8AC)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3023[2] = 
{
	MaterialTexturePair_tC333F7CBCFAB82A2F3735B0BFA42576FFC13C8AC::get_offset_of_texture2D_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialTexturePair_tC333F7CBCFAB82A2F3735B0BFA42576FFC13C8AC::get_offset_of_material_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (AttachmentRegionExtensions_tF2133D86E087352C30B685F00CCEEE741152FD9D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (AtlasUtilities_tA9C61E45558430A4D715DD7155EC0C582E845E28), -1, sizeof(AtlasUtilities_tA9C61E45558430A4D715DD7155EC0C582E845E28_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3025[7] = 
{
	0,
	0,
	0,
	0,
	0,
	AtlasUtilities_tA9C61E45558430A4D715DD7155EC0C582E845E28_StaticFields::get_offset_of_CachedRegionTextures_5(),
	AtlasUtilities_tA9C61E45558430A4D715DD7155EC0C582E845E28_StaticFields::get_offset_of_CachedRegionTexturesList_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (SkinUtilities_t14C5F179F43847921F1296F66D569729AE1720CE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (AttachmentCloneExtensions_tD2E5C3ECD925AF6F1B362CC5D63D3426D657BEB0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A), -1, sizeof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3028[3] = 
{
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U323447D2BC3FF6984AB09F575BC63CFE460337394_0(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U36D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_B6E6EA57C32297E83203480EE50A22C3581AA09C_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
