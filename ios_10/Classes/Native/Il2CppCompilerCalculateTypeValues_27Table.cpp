﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BaseScale
struct BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063;
// BaseScale/delegateScaleEnd
struct delegateScaleEnd_t697D8B0D83495ED3E1853627A6FCACD68409376F;
// BaseShake
struct BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A;
// CFrameAnimationEffect
struct CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10;
// DataManager
struct DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30;
// District
struct District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A;
// District[]
struct DistrictU5BU5D_t4A806ECBA985428889EBCDE245832B5D81274468;
// EnviromentMask
struct EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73;
// ItemSystem
struct ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677;
// Lot
struct Lot_t51B999792800E5A264A2CA51A4C521311958F874;
// Lot[]
struct LotU5BU5D_tDF39035F8B8E0E8A8915E650FAC90FE38BFD335F;
// Main/sAutoRunLocation[]
struct sAutoRunLocationU5BU5D_t1B4B4B1E8F7FDC3043F544A45744E0D6A98B8864;
// MoneyCounter
struct MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF;
// MoneyCounter[]
struct MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A;
// MoneyTrigger
struct MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464;
// OfflineManager
struct OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860;
// Paw
struct Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011;
// Paw/delegateMoveEnd
struct delegateMoveEnd_t1F76CEA26063F0B11A971CAB2747FA1BFA8241F7;
// Plane
struct Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273;
// Planet
struct Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B;
// Planet[]
struct PlanetU5BU5D_tFAD0DE71ED69B99F92FC1525DDAE53548F72893D;
// ResearchCounter
struct ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD;
// ResearchManager
struct ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F;
// SceneUiButton
struct SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507;
// SceneUiPrice
struct SceneUiPrice_tDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2;
// ScienceLeaf
struct ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718;
// ScienceLeaf[]
struct ScienceLeafU5BU5D_t6A4CF4ED08D2B3E79C7D82250A9BECA8687F3C39;
// ScienceTree
struct ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E;
// ScienceTreeConfig
struct ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766;
// ShoppinMall
struct ShoppinMall_t6389C5391B85521387589717F7A53592DB917811;
// ShoppinMallTypeContainer
struct ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555;
// Spine.Unity.SkeletonGraphic
struct SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A;
// StartBelt
struct StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD;
// StartRunArrow[]
struct StartRunArrowU5BU5D_t03C581BC0F67FE5F0F67FEED77CA35F307D55353;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<AudioManager/eSE,System.Collections.Generic.List`1<UnityEngine.AudioSource>>
struct Dictionary_2_tCB4384C8D69355F95AE7A997BD0EBA8D389A27B2;
// System.Collections.Generic.Dictionary`2<FreshGuide/eGuideType,System.Boolean>
struct Dictionary_2_tE758D927E90AD1ECFDE01DDC3DB458104EDA3D92;
// System.Collections.Generic.Dictionary`2<Main/ePosType,System.Collections.Generic.List`1<Main/sAutoRunLocation>>
struct Dictionary_2_t8BBCAEE5B6551D2028E4CE6BBACCA1D12D8070C0;
// System.Collections.Generic.Dictionary`2<ScienceTree/eBranchType,ScienceLeaf[]>
struct Dictionary_2_t3CFE57C822373C69789FA5C250542814D1C2F75D;
// System.Collections.Generic.Dictionary`2<ScienceTree/eBranchType,System.Collections.Generic.List`1<ScienceTree/sLeafConfig>>
struct Dictionary_2_t370E87F7C108A681DC68DB080035C48F72ECE0BE;
// System.Collections.Generic.Dictionary`2<ScienceTree/eBranchType,System.Int32>
struct Dictionary_2_t5651FDBECE7EFA5777F480852B72CC08092E9231;
// System.Collections.Generic.Dictionary`2<ShoppinMall/eItemType,System.Collections.Generic.List`1<ShoppinMall/sShoppingMallItemConfig>>
struct Dictionary_2_tC88F31954E3D7F66998673CFA25C2B6EB95412DF;
// System.Collections.Generic.Dictionary`2<ShoppinMall/eItemType,System.Collections.Generic.List`1<UIShoppinAndItemCounter>>
struct Dictionary_2_tE1ED95465856839DB9DE1550C696F1B7505215AC;
// System.Collections.Generic.Dictionary`2<ShoppinMall/eItemType,System.Int32>
struct Dictionary_2_tEA60B96640F887360F2D1F134CCCD59FC6C37840;
// System.Collections.Generic.Dictionary`2<ShoppinMall/eItemType,UnityEngine.Sprite[]>
struct Dictionary_2_t32BE7FCA071A61538F1D015AB5D7D62E70FA4CAA;
// System.Collections.Generic.Dictionary`2<SkillManager/eSkillType,Skill>
struct Dictionary_2_t5BEFDBA4420F57163BF6DF9697D75434371B6E60;
// System.Collections.Generic.Dictionary`2<SkillManager/eSkillType,SkillManager/sSkillConfig[]>
struct Dictionary_2_t773BB6E1D26D4A9ACB7CB6040164FD7F4C4F3A6D;
// System.Collections.Generic.Dictionary`2<System.Int32,DataManager/sPlanetConfig>
struct Dictionary_2_t35A7F0D04BB2998E9DD0CB0865E4F0442AE488AF;
// System.Collections.Generic.Dictionary`2<System.Int32,ItemSystem/sItemConfig>
struct Dictionary_2_tE48FC1442A682263A6270B86D3E7E0A91D86F986;
// System.Collections.Generic.Dictionary`2<System.Int32,Plane>
struct Dictionary_2_t9EA66ECB4AD0D1857C3BAE8BDD9D84DEC3B499F2;
// System.Collections.Generic.Dictionary`2<System.Int32,ResearchCounter>
struct Dictionary_2_t1D475F8089C35FF3718700296FB38C143E09149F;
// System.Collections.Generic.Dictionary`2<System.Int32,ShoppinMall/sShoppingMallItemConfig>
struct Dictionary_2_t85BD9605BBAFF169EBF8E31434102C544A2649ED;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct Dictionary_2_t27BCF11A3E40FEB6B728C0EA39A63FAB1AE16D5C;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8;
// System.Collections.Generic.Dictionary`2<System.Int32,UIResearchCounterContainer>
struct Dictionary_2_tC10E085FDFF6C3AB8005D16563E90234EF657A79;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Sprite[]>
struct Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E;
// System.Collections.Generic.Dictionary`2<System.String,DataManager/sAutomobileConfig>
struct Dictionary_2_t575E591ADF0DE43EE8FBD5432FDB44C619218BBC;
// System.Collections.Generic.Dictionary`2<System.String,DataManager/sPrestigeConfig>
struct Dictionary_2_t6927BC972CD2E0E92AF0DF540164BAF9ECB65E17;
// System.Collections.Generic.Dictionary`2<System.String,DataManager/sTrackConfig>
struct Dictionary_2_t0FCB090B06AD243E1AEFE7A77D6E44B002171473;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.Int32,ResearchManager/sResearchConfig>>
struct Dictionary_2_t29FAE8F1E76FC04AF92864502318A91DA5A83336;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<UIVehicleCounter>>
struct Dictionary_2_t23E80E3DB14B3FC8A2D8537F3FCAC9C8780F1654;
// System.Collections.Generic.Dictionary`2<System.String,System.Double>
struct Dictionary_2_tD9E97F40821434226F92E86AFDAE50D233468602;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t89F484A77FD6673A172CE80BD0A37031FBD1FD6A;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.String,UIZengShouCounter>
struct Dictionary_2_tA42C789C20DBB051A4F863D661877067E6AE8081;
// System.Collections.Generic.List`1<Admin>
struct List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34;
// System.Collections.Generic.List`1<District/sLevelAndCost>
struct List_1_tB9FA86EA4317FBDAC182273F5D02A08CB5B68D86;
// System.Collections.Generic.List`1<Main/sAutoRunLocation>
struct List_1_t1517B4D31CFE0D5A31B279B7CC226F6BA9FA9E43;
// System.Collections.Generic.List`1<Plane>
struct List_1_t47760497A2262813AF37CFF6B714399A5DE17D22;
// System.Collections.Generic.List`1<ScienceLeaf[]>
struct List_1_t423D8BD0139A80038103E8A487DD03999EA830C8;
// System.Collections.Generic.List`1<ShoppinMallTypeContainer>
struct List_1_t5485117BEBD07C46F8E6C085089A3EF3A8103DB4;
// System.Collections.Generic.List`1<SnowFlake>
struct List_1_t0EA6D74B5B5964105FF9E918795F54CBDC306BD9;
// System.Collections.Generic.List`1<System.Double>
struct List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t026D7A8C4D989218772DB3E051A624F753A60859;
// System.Collections.Generic.List`1<UIFlyingCoin>
struct List_1_tB36688DD81A4ADEF07F2EB8C42C12B19153C0C95;
// System.Collections.Generic.List`1<UIItem>
struct List_1_t7302F8A5155D7A00D77040895186DF4F7D8A896D;
// System.Collections.Generic.List`1<UIItemInBag>
struct List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB;
// System.Collections.Generic.List`1<UIShoppinAndItemCounter>
struct List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B;
// System.Collections.Generic.List`1<UIVehicleCounter>
struct List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UIAdministratorCounter
struct UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78;
// UIBuffCounter[]
struct UIBuffCounterU5BU5D_t8C5FC9F38732088DCC192DD53CEF2E923FDA4645;
// UIDistrict[]
struct UIDistrictU5BU5D_tDD148C70027D94D63D232B5C9FBB142D5EA721CB;
// UIItem
struct UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957;
// UIItemBagContainer
struct UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6;
// UIItemInBag
struct UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6;
// UIMainLand[]
struct UIMainLandU5BU5D_tF159C8131B1BA4E2EEDF316C136F4BA6CE07B280;
// UIPlanet[]
struct UIPlanetU5BU5D_t8587E1E90D0FB3095F0BBAA9DF7CE4A5A6A94298;
// UIPrestige
struct UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C;
// UIResearchCounterContainer
struct UIResearchCounterContainer_tCD786B78332DEDA681A84956DC136BCFE55100F3;
// UIShoppinAndItemCounter
struct UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169;
// UIShoppinAndItemCounter[]
struct UIShoppinAndItemCounterU5BU5D_t414AC543955220267F28047390018B8E86CB56C8;
// UISkillCastCounter
struct UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324;
// UISkillCastCounter[]
struct UISkillCastCounterU5BU5D_t4E7D995D5ECD704EBC1A808B62174C6F4EA76349;
// UIStars
struct UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF;
// UnityEngine.Collider2D
struct Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Font
struct Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067;
// UnityEngine.Gyroscope
struct Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Rendering.SortingGroup
struct SortingGroup_tB115E99DFEC155BA39717DC7970E92586F995AED;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.SpriteMask
struct SpriteMask_tE6D9EAE54A65A5207E4635728D2D01385B5D1804;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_tA8FE422195BB4C0A7ABA1BFC136CB8D1F174FA32;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.TrailRenderer
struct TrailRenderer_t9AC23ED5E8A7955A3288A9C37865C11382DA668D;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Dropdown
struct Dropdown_tF6331401084B1213CAB10587A6EC81461501930F;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.Outline
struct Outline_tB750E496976B072E79142D51C0A991AC20183095;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188;
// UnityEngine.UI.Toggle
struct Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106;
// UnityEngine.UI.VerticalLayoutGroup
struct VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.WWW
struct WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADCONFIG_AUTOMOBILEU3ED__42_T38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0_H
#define U3CLOADCONFIG_AUTOMOBILEU3ED__42_T38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/<LoadConfig_Automobile>d__42
struct  U3CLoadConfig_AutomobileU3Ed__42_t38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0  : public RuntimeObject
{
public:
	// System.Int32 DataManager/<LoadConfig_Automobile>d__42::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DataManager/<LoadConfig_Automobile>d__42::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String DataManager/<LoadConfig_Automobile>d__42::szFileName
	String_t* ___szFileName_2;
	// DataManager DataManager/<LoadConfig_Automobile>d__42::<>4__this
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * ___U3CU3E4__this_3;
	// UnityEngine.WWW DataManager/<LoadConfig_Automobile>d__42::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AutomobileU3Ed__42_t38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AutomobileU3Ed__42_t38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_szFileName_2() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AutomobileU3Ed__42_t38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0, ___szFileName_2)); }
	inline String_t* get_szFileName_2() const { return ___szFileName_2; }
	inline String_t** get_address_of_szFileName_2() { return &___szFileName_2; }
	inline void set_szFileName_2(String_t* value)
	{
		___szFileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AutomobileU3Ed__42_t38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0, ___U3CU3E4__this_3)); }
	inline DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadConfig_AutomobileU3Ed__42_t38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONFIG_AUTOMOBILEU3ED__42_T38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0_H
#ifndef U3CLOADCONFIG_PLANETU3ED__35_TC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133_H
#define U3CLOADCONFIG_PLANETU3ED__35_TC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/<LoadConfig_Planet>d__35
struct  U3CLoadConfig_PlanetU3Ed__35_tC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133  : public RuntimeObject
{
public:
	// System.Int32 DataManager/<LoadConfig_Planet>d__35::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DataManager/<LoadConfig_Planet>d__35::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String DataManager/<LoadConfig_Planet>d__35::szFileName
	String_t* ___szFileName_2;
	// DataManager DataManager/<LoadConfig_Planet>d__35::<>4__this
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * ___U3CU3E4__this_3;
	// UnityEngine.WWW DataManager/<LoadConfig_Planet>d__35::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadConfig_PlanetU3Ed__35_tC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadConfig_PlanetU3Ed__35_tC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_szFileName_2() { return static_cast<int32_t>(offsetof(U3CLoadConfig_PlanetU3Ed__35_tC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133, ___szFileName_2)); }
	inline String_t* get_szFileName_2() const { return ___szFileName_2; }
	inline String_t** get_address_of_szFileName_2() { return &___szFileName_2; }
	inline void set_szFileName_2(String_t* value)
	{
		___szFileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadConfig_PlanetU3Ed__35_tC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133, ___U3CU3E4__this_3)); }
	inline DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadConfig_PlanetU3Ed__35_tC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONFIG_PLANETU3ED__35_TC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133_H
#ifndef U3CLOADCONFIG_TRACKU3ED__37_TF50D32EEA406D9E38804EEC52B82308D941DCA68_H
#define U3CLOADCONFIG_TRACKU3ED__37_TF50D32EEA406D9E38804EEC52B82308D941DCA68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/<LoadConfig_Track>d__37
struct  U3CLoadConfig_TrackU3Ed__37_tF50D32EEA406D9E38804EEC52B82308D941DCA68  : public RuntimeObject
{
public:
	// System.Int32 DataManager/<LoadConfig_Track>d__37::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DataManager/<LoadConfig_Track>d__37::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String DataManager/<LoadConfig_Track>d__37::szFileName
	String_t* ___szFileName_2;
	// DataManager DataManager/<LoadConfig_Track>d__37::<>4__this
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * ___U3CU3E4__this_3;
	// UnityEngine.WWW DataManager/<LoadConfig_Track>d__37::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadConfig_TrackU3Ed__37_tF50D32EEA406D9E38804EEC52B82308D941DCA68, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadConfig_TrackU3Ed__37_tF50D32EEA406D9E38804EEC52B82308D941DCA68, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_szFileName_2() { return static_cast<int32_t>(offsetof(U3CLoadConfig_TrackU3Ed__37_tF50D32EEA406D9E38804EEC52B82308D941DCA68, ___szFileName_2)); }
	inline String_t* get_szFileName_2() const { return ___szFileName_2; }
	inline String_t** get_address_of_szFileName_2() { return &___szFileName_2; }
	inline void set_szFileName_2(String_t* value)
	{
		___szFileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadConfig_TrackU3Ed__37_tF50D32EEA406D9E38804EEC52B82308D941DCA68, ___U3CU3E4__this_3)); }
	inline DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadConfig_TrackU3Ed__37_tF50D32EEA406D9E38804EEC52B82308D941DCA68, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONFIG_TRACKU3ED__37_TF50D32EEA406D9E38804EEC52B82308D941DCA68_H
#ifndef GETDROPINTERVAL_TECA142D4B048205911AC241AF307139A969168E5_H
#define GETDROPINTERVAL_TECA142D4B048205911AC241AF307139A969168E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetDropInterval
struct  GetDropInterval_tECA142D4B048205911AC241AF307139A969168E5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDROPINTERVAL_TECA142D4B048205911AC241AF307139A969168E5_H
#ifndef U3CLOADCONFIG_ITEMU3ED__25_T2925A912974D437EC95D48E799EC131D0C0BDF9D_H
#define U3CLOADCONFIG_ITEMU3ED__25_T2925A912974D437EC95D48E799EC131D0C0BDF9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemSystem/<LoadConfig_Item>d__25
struct  U3CLoadConfig_ItemU3Ed__25_t2925A912974D437EC95D48E799EC131D0C0BDF9D  : public RuntimeObject
{
public:
	// System.Int32 ItemSystem/<LoadConfig_Item>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ItemSystem/<LoadConfig_Item>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String ItemSystem/<LoadConfig_Item>d__25::szFileName
	String_t* ___szFileName_2;
	// ItemSystem ItemSystem/<LoadConfig_Item>d__25::<>4__this
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677 * ___U3CU3E4__this_3;
	// UnityEngine.WWW ItemSystem/<LoadConfig_Item>d__25::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadConfig_ItemU3Ed__25_t2925A912974D437EC95D48E799EC131D0C0BDF9D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadConfig_ItemU3Ed__25_t2925A912974D437EC95D48E799EC131D0C0BDF9D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_szFileName_2() { return static_cast<int32_t>(offsetof(U3CLoadConfig_ItemU3Ed__25_t2925A912974D437EC95D48E799EC131D0C0BDF9D, ___szFileName_2)); }
	inline String_t* get_szFileName_2() const { return ___szFileName_2; }
	inline String_t** get_address_of_szFileName_2() { return &___szFileName_2; }
	inline void set_szFileName_2(String_t* value)
	{
		___szFileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadConfig_ItemU3Ed__25_t2925A912974D437EC95D48E799EC131D0C0BDF9D, ___U3CU3E4__this_3)); }
	inline ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadConfig_ItemU3Ed__25_t2925A912974D437EC95D48E799EC131D0C0BDF9D, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONFIG_ITEMU3ED__25_T2925A912974D437EC95D48E799EC131D0C0BDF9D_H
#ifndef U3CLOADCONFIG_OFFLINEU3ED__14_T1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A_H
#define U3CLOADCONFIG_OFFLINEU3ED__14_T1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfflineManager/<LoadConfig_Offline>d__14
struct  U3CLoadConfig_OfflineU3Ed__14_t1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A  : public RuntimeObject
{
public:
	// System.Int32 OfflineManager/<LoadConfig_Offline>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object OfflineManager/<LoadConfig_Offline>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String OfflineManager/<LoadConfig_Offline>d__14::szFileName
	String_t* ___szFileName_2;
	// OfflineManager OfflineManager/<LoadConfig_Offline>d__14::<>4__this
	OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860 * ___U3CU3E4__this_3;
	// UnityEngine.WWW OfflineManager/<LoadConfig_Offline>d__14::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadConfig_OfflineU3Ed__14_t1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadConfig_OfflineU3Ed__14_t1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_szFileName_2() { return static_cast<int32_t>(offsetof(U3CLoadConfig_OfflineU3Ed__14_t1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A, ___szFileName_2)); }
	inline String_t* get_szFileName_2() const { return ___szFileName_2; }
	inline String_t** get_address_of_szFileName_2() { return &___szFileName_2; }
	inline void set_szFileName_2(String_t* value)
	{
		___szFileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadConfig_OfflineU3Ed__14_t1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A, ___U3CU3E4__this_3)); }
	inline OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadConfig_OfflineU3Ed__14_t1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONFIG_OFFLINEU3ED__14_T1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A_H
#ifndef U3CLOADCONFIG_RESEARCHU3ED__14_TF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B_H
#define U3CLOADCONFIG_RESEARCHU3ED__14_TF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResearchManager/<LoadConfig_Research>d__14
struct  U3CLoadConfig_ResearchU3Ed__14_tF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B  : public RuntimeObject
{
public:
	// System.Int32 ResearchManager/<LoadConfig_Research>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ResearchManager/<LoadConfig_Research>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String ResearchManager/<LoadConfig_Research>d__14::szFileName
	String_t* ___szFileName_2;
	// ResearchManager ResearchManager/<LoadConfig_Research>d__14::<>4__this
	ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F * ___U3CU3E4__this_3;
	// UnityEngine.WWW ResearchManager/<LoadConfig_Research>d__14::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadConfig_ResearchU3Ed__14_tF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadConfig_ResearchU3Ed__14_tF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_szFileName_2() { return static_cast<int32_t>(offsetof(U3CLoadConfig_ResearchU3Ed__14_tF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B, ___szFileName_2)); }
	inline String_t* get_szFileName_2() const { return ___szFileName_2; }
	inline String_t** get_address_of_szFileName_2() { return &___szFileName_2; }
	inline void set_szFileName_2(String_t* value)
	{
		___szFileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadConfig_ResearchU3Ed__14_tF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B, ___U3CU3E4__this_3)); }
	inline ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadConfig_ResearchU3Ed__14_tF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONFIG_RESEARCHU3ED__14_TF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B_H
#ifndef U3CLOADCONFIG_TALENTGENERALU3ED__61_T7C66D1F4C165678DBE343AEA87115D05CCE67AD9_H
#define U3CLOADCONFIG_TALENTGENERALU3ED__61_T7C66D1F4C165678DBE343AEA87115D05CCE67AD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScienceTree/<LoadConfig_TalentGeneral>d__61
struct  U3CLoadConfig_TalentGeneralU3Ed__61_t7C66D1F4C165678DBE343AEA87115D05CCE67AD9  : public RuntimeObject
{
public:
	// System.Int32 ScienceTree/<LoadConfig_TalentGeneral>d__61::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ScienceTree/<LoadConfig_TalentGeneral>d__61::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String ScienceTree/<LoadConfig_TalentGeneral>d__61::szFileName
	String_t* ___szFileName_2;
	// ScienceTree ScienceTree/<LoadConfig_TalentGeneral>d__61::<>4__this
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E * ___U3CU3E4__this_3;
	// UnityEngine.WWW ScienceTree/<LoadConfig_TalentGeneral>d__61::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadConfig_TalentGeneralU3Ed__61_t7C66D1F4C165678DBE343AEA87115D05CCE67AD9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadConfig_TalentGeneralU3Ed__61_t7C66D1F4C165678DBE343AEA87115D05CCE67AD9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_szFileName_2() { return static_cast<int32_t>(offsetof(U3CLoadConfig_TalentGeneralU3Ed__61_t7C66D1F4C165678DBE343AEA87115D05CCE67AD9, ___szFileName_2)); }
	inline String_t* get_szFileName_2() const { return ___szFileName_2; }
	inline String_t** get_address_of_szFileName_2() { return &___szFileName_2; }
	inline void set_szFileName_2(String_t* value)
	{
		___szFileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadConfig_TalentGeneralU3Ed__61_t7C66D1F4C165678DBE343AEA87115D05CCE67AD9, ___U3CU3E4__this_3)); }
	inline ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadConfig_TalentGeneralU3Ed__61_t7C66D1F4C165678DBE343AEA87115D05CCE67AD9, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONFIG_TALENTGENERALU3ED__61_T7C66D1F4C165678DBE343AEA87115D05CCE67AD9_H
#ifndef U3CLOADCONFIG_SHOPPINGMALLU3ED__49_T6419239B92C5C5B6EC20B06F98A7552A7FE79176_H
#define U3CLOADCONFIG_SHOPPINGMALLU3ED__49_T6419239B92C5C5B6EC20B06F98A7552A7FE79176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppinMall/<LoadConfig_Shoppingmall>d__49
struct  U3CLoadConfig_ShoppingmallU3Ed__49_t6419239B92C5C5B6EC20B06F98A7552A7FE79176  : public RuntimeObject
{
public:
	// System.Int32 ShoppinMall/<LoadConfig_Shoppingmall>d__49::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ShoppinMall/<LoadConfig_Shoppingmall>d__49::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String ShoppinMall/<LoadConfig_Shoppingmall>d__49::szFileName
	String_t* ___szFileName_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadConfig_ShoppingmallU3Ed__49_t6419239B92C5C5B6EC20B06F98A7552A7FE79176, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadConfig_ShoppingmallU3Ed__49_t6419239B92C5C5B6EC20B06F98A7552A7FE79176, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_szFileName_2() { return static_cast<int32_t>(offsetof(U3CLoadConfig_ShoppingmallU3Ed__49_t6419239B92C5C5B6EC20B06F98A7552A7FE79176, ___szFileName_2)); }
	inline String_t* get_szFileName_2() const { return ___szFileName_2; }
	inline String_t** get_address_of_szFileName_2() { return &___szFileName_2; }
	inline void set_szFileName_2(String_t* value)
	{
		___szFileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONFIG_SHOPPINGMALLU3ED__49_T6419239B92C5C5B6EC20B06F98A7552A7FE79176_H
#ifndef U3CLOADCONFIG_SHOPPINGMALL_NEWU3ED__44_T9DD82782DF06252A4AE472EEF05064F8AF6CF0F0_H
#define U3CLOADCONFIG_SHOPPINGMALL_NEWU3ED__44_T9DD82782DF06252A4AE472EEF05064F8AF6CF0F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppinMall/<LoadConfig_Shoppingmall_New>d__44
struct  U3CLoadConfig_Shoppingmall_NewU3Ed__44_t9DD82782DF06252A4AE472EEF05064F8AF6CF0F0  : public RuntimeObject
{
public:
	// System.Int32 ShoppinMall/<LoadConfig_Shoppingmall_New>d__44::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ShoppinMall/<LoadConfig_Shoppingmall_New>d__44::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String ShoppinMall/<LoadConfig_Shoppingmall_New>d__44::szFileName
	String_t* ___szFileName_2;
	// ShoppinMall ShoppinMall/<LoadConfig_Shoppingmall_New>d__44::<>4__this
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811 * ___U3CU3E4__this_3;
	// UnityEngine.WWW ShoppinMall/<LoadConfig_Shoppingmall_New>d__44::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadConfig_Shoppingmall_NewU3Ed__44_t9DD82782DF06252A4AE472EEF05064F8AF6CF0F0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadConfig_Shoppingmall_NewU3Ed__44_t9DD82782DF06252A4AE472EEF05064F8AF6CF0F0, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_szFileName_2() { return static_cast<int32_t>(offsetof(U3CLoadConfig_Shoppingmall_NewU3Ed__44_t9DD82782DF06252A4AE472EEF05064F8AF6CF0F0, ___szFileName_2)); }
	inline String_t* get_szFileName_2() const { return ___szFileName_2; }
	inline String_t** get_address_of_szFileName_2() { return &___szFileName_2; }
	inline void set_szFileName_2(String_t* value)
	{
		___szFileName_2 = value;
		Il2CppCodeGenWriteBarrier((&___szFileName_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadConfig_Shoppingmall_NewU3Ed__44_t9DD82782DF06252A4AE472EEF05064F8AF6CF0F0, ___U3CU3E4__this_3)); }
	inline ShoppinMall_t6389C5391B85521387589717F7A53592DB917811 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ShoppinMall_t6389C5391B85521387589717F7A53592DB917811 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadConfig_Shoppingmall_NewU3Ed__44_t9DD82782DF06252A4AE472EEF05064F8AF6CF0F0, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONFIG_SHOPPINGMALL_NEWU3ED__44_T9DD82782DF06252A4AE472EEF05064F8AF6CF0F0_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef DOU_T6892A900BE5AC1C3D8B4DFBA12DC4445953E1EFD_H
#define DOU_T6892A900BE5AC1C3D8B4DFBA12DC4445953E1EFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// dou
struct  dou_t6892A900BE5AC1C3D8B4DFBA12DC4445953E1EFD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOU_T6892A900BE5AC1C3D8B4DFBA12DC4445953E1EFD_H
#ifndef SAUTOMOBILECONFIG_T0F7305B3147D29DC77AFD945BD716711D8271B20_H
#define SAUTOMOBILECONFIG_T0F7305B3147D29DC77AFD945BD716711D8271B20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/sAutomobileConfig
struct  sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20 
{
public:
	// System.Int32 DataManager/sAutomobileConfig::nId
	int32_t ___nId_0;
	// System.Int32 DataManager/sAutomobileConfig::nLevel
	int32_t ___nLevel_1;
	// System.Int32 DataManager/sAutomobileConfig::nResId
	int32_t ___nResId_2;
	// System.String DataManager/sAutomobileConfig::szName
	String_t* ___szName_3;
	// System.Int32 DataManager/sAutomobileConfig::eCoinType
	int32_t ___eCoinType_4;
	// System.Double DataManager/sAutomobileConfig::nStartPrice_CoinValue
	double ___nStartPrice_CoinValue_5;
	// System.Single DataManager/sAutomobileConfig::fPriceRaisePercentPerBuy
	float ___fPriceRaisePercentPerBuy_6;
	// System.Int32 DataManager/sAutomobileConfig::nPriceDiamond
	int32_t ___nPriceDiamond_7;
	// System.Int32 DataManager/sAutomobileConfig::nCanUnlockLevel
	int32_t ___nCanUnlockLevel_8;
	// System.String DataManager/sAutomobileConfig::szSuitableTrackId
	String_t* ___szSuitableTrackId_9;
	// System.Double DataManager/sAutomobileConfig::nBaseGain
	double ___nBaseGain_10;
	// System.Single DataManager/sAutomobileConfig::fBaseSpeed
	float ___fBaseSpeed_11;
	// System.Int32 DataManager/sAutomobileConfig::nDropLevel
	int32_t ___nDropLevel_12;
	// System.Int32 DataManager/sAutomobileConfig::nDropInterval
	int32_t ___nDropInterval_13;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_nLevel_1() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nLevel_1)); }
	inline int32_t get_nLevel_1() const { return ___nLevel_1; }
	inline int32_t* get_address_of_nLevel_1() { return &___nLevel_1; }
	inline void set_nLevel_1(int32_t value)
	{
		___nLevel_1 = value;
	}

	inline static int32_t get_offset_of_nResId_2() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nResId_2)); }
	inline int32_t get_nResId_2() const { return ___nResId_2; }
	inline int32_t* get_address_of_nResId_2() { return &___nResId_2; }
	inline void set_nResId_2(int32_t value)
	{
		___nResId_2 = value;
	}

	inline static int32_t get_offset_of_szName_3() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___szName_3)); }
	inline String_t* get_szName_3() const { return ___szName_3; }
	inline String_t** get_address_of_szName_3() { return &___szName_3; }
	inline void set_szName_3(String_t* value)
	{
		___szName_3 = value;
		Il2CppCodeGenWriteBarrier((&___szName_3), value);
	}

	inline static int32_t get_offset_of_eCoinType_4() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___eCoinType_4)); }
	inline int32_t get_eCoinType_4() const { return ___eCoinType_4; }
	inline int32_t* get_address_of_eCoinType_4() { return &___eCoinType_4; }
	inline void set_eCoinType_4(int32_t value)
	{
		___eCoinType_4 = value;
	}

	inline static int32_t get_offset_of_nStartPrice_CoinValue_5() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nStartPrice_CoinValue_5)); }
	inline double get_nStartPrice_CoinValue_5() const { return ___nStartPrice_CoinValue_5; }
	inline double* get_address_of_nStartPrice_CoinValue_5() { return &___nStartPrice_CoinValue_5; }
	inline void set_nStartPrice_CoinValue_5(double value)
	{
		___nStartPrice_CoinValue_5 = value;
	}

	inline static int32_t get_offset_of_fPriceRaisePercentPerBuy_6() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___fPriceRaisePercentPerBuy_6)); }
	inline float get_fPriceRaisePercentPerBuy_6() const { return ___fPriceRaisePercentPerBuy_6; }
	inline float* get_address_of_fPriceRaisePercentPerBuy_6() { return &___fPriceRaisePercentPerBuy_6; }
	inline void set_fPriceRaisePercentPerBuy_6(float value)
	{
		___fPriceRaisePercentPerBuy_6 = value;
	}

	inline static int32_t get_offset_of_nPriceDiamond_7() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nPriceDiamond_7)); }
	inline int32_t get_nPriceDiamond_7() const { return ___nPriceDiamond_7; }
	inline int32_t* get_address_of_nPriceDiamond_7() { return &___nPriceDiamond_7; }
	inline void set_nPriceDiamond_7(int32_t value)
	{
		___nPriceDiamond_7 = value;
	}

	inline static int32_t get_offset_of_nCanUnlockLevel_8() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nCanUnlockLevel_8)); }
	inline int32_t get_nCanUnlockLevel_8() const { return ___nCanUnlockLevel_8; }
	inline int32_t* get_address_of_nCanUnlockLevel_8() { return &___nCanUnlockLevel_8; }
	inline void set_nCanUnlockLevel_8(int32_t value)
	{
		___nCanUnlockLevel_8 = value;
	}

	inline static int32_t get_offset_of_szSuitableTrackId_9() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___szSuitableTrackId_9)); }
	inline String_t* get_szSuitableTrackId_9() const { return ___szSuitableTrackId_9; }
	inline String_t** get_address_of_szSuitableTrackId_9() { return &___szSuitableTrackId_9; }
	inline void set_szSuitableTrackId_9(String_t* value)
	{
		___szSuitableTrackId_9 = value;
		Il2CppCodeGenWriteBarrier((&___szSuitableTrackId_9), value);
	}

	inline static int32_t get_offset_of_nBaseGain_10() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nBaseGain_10)); }
	inline double get_nBaseGain_10() const { return ___nBaseGain_10; }
	inline double* get_address_of_nBaseGain_10() { return &___nBaseGain_10; }
	inline void set_nBaseGain_10(double value)
	{
		___nBaseGain_10 = value;
	}

	inline static int32_t get_offset_of_fBaseSpeed_11() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___fBaseSpeed_11)); }
	inline float get_fBaseSpeed_11() const { return ___fBaseSpeed_11; }
	inline float* get_address_of_fBaseSpeed_11() { return &___fBaseSpeed_11; }
	inline void set_fBaseSpeed_11(float value)
	{
		___fBaseSpeed_11 = value;
	}

	inline static int32_t get_offset_of_nDropLevel_12() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nDropLevel_12)); }
	inline int32_t get_nDropLevel_12() const { return ___nDropLevel_12; }
	inline int32_t* get_address_of_nDropLevel_12() { return &___nDropLevel_12; }
	inline void set_nDropLevel_12(int32_t value)
	{
		___nDropLevel_12 = value;
	}

	inline static int32_t get_offset_of_nDropInterval_13() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nDropInterval_13)); }
	inline int32_t get_nDropInterval_13() const { return ___nDropInterval_13; }
	inline int32_t* get_address_of_nDropInterval_13() { return &___nDropInterval_13; }
	inline void set_nDropInterval_13(int32_t value)
	{
		___nDropInterval_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DataManager/sAutomobileConfig
struct sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20_marshaled_pinvoke
{
	int32_t ___nId_0;
	int32_t ___nLevel_1;
	int32_t ___nResId_2;
	char* ___szName_3;
	int32_t ___eCoinType_4;
	double ___nStartPrice_CoinValue_5;
	float ___fPriceRaisePercentPerBuy_6;
	int32_t ___nPriceDiamond_7;
	int32_t ___nCanUnlockLevel_8;
	char* ___szSuitableTrackId_9;
	double ___nBaseGain_10;
	float ___fBaseSpeed_11;
	int32_t ___nDropLevel_12;
	int32_t ___nDropInterval_13;
};
// Native definition for COM marshalling of DataManager/sAutomobileConfig
struct sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20_marshaled_com
{
	int32_t ___nId_0;
	int32_t ___nLevel_1;
	int32_t ___nResId_2;
	Il2CppChar* ___szName_3;
	int32_t ___eCoinType_4;
	double ___nStartPrice_CoinValue_5;
	float ___fPriceRaisePercentPerBuy_6;
	int32_t ___nPriceDiamond_7;
	int32_t ___nCanUnlockLevel_8;
	Il2CppChar* ___szSuitableTrackId_9;
	double ___nBaseGain_10;
	float ___fBaseSpeed_11;
	int32_t ___nDropLevel_12;
	int32_t ___nDropInterval_13;
};
#endif // SAUTOMOBILECONFIG_T0F7305B3147D29DC77AFD945BD716711D8271B20_H
#ifndef SPRESTIGECONFIG_T397FC1BE163EC0B938F2EC3B56694DCA340BAC63_H
#define SPRESTIGECONFIG_T397FC1BE163EC0B938F2EC3B56694DCA340BAC63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/sPrestigeConfig
struct  sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63 
{
public:
	// System.Collections.Generic.List`1<System.Double> DataManager/sPrestigeConfig::aryCoinCost
	List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786 * ___aryCoinCost_0;
	// System.Collections.Generic.List`1<System.Single> DataManager/sPrestigeConfig::aryCoinPromote
	List_1_t026D7A8C4D989218772DB3E051A624F753A60859 * ___aryCoinPromote_1;

public:
	inline static int32_t get_offset_of_aryCoinCost_0() { return static_cast<int32_t>(offsetof(sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63, ___aryCoinCost_0)); }
	inline List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786 * get_aryCoinCost_0() const { return ___aryCoinCost_0; }
	inline List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786 ** get_address_of_aryCoinCost_0() { return &___aryCoinCost_0; }
	inline void set_aryCoinCost_0(List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786 * value)
	{
		___aryCoinCost_0 = value;
		Il2CppCodeGenWriteBarrier((&___aryCoinCost_0), value);
	}

	inline static int32_t get_offset_of_aryCoinPromote_1() { return static_cast<int32_t>(offsetof(sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63, ___aryCoinPromote_1)); }
	inline List_1_t026D7A8C4D989218772DB3E051A624F753A60859 * get_aryCoinPromote_1() const { return ___aryCoinPromote_1; }
	inline List_1_t026D7A8C4D989218772DB3E051A624F753A60859 ** get_address_of_aryCoinPromote_1() { return &___aryCoinPromote_1; }
	inline void set_aryCoinPromote_1(List_1_t026D7A8C4D989218772DB3E051A624F753A60859 * value)
	{
		___aryCoinPromote_1 = value;
		Il2CppCodeGenWriteBarrier((&___aryCoinPromote_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DataManager/sPrestigeConfig
struct sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63_marshaled_pinvoke
{
	List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786 * ___aryCoinCost_0;
	List_1_t026D7A8C4D989218772DB3E051A624F753A60859 * ___aryCoinPromote_1;
};
// Native definition for COM marshalling of DataManager/sPrestigeConfig
struct sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63_marshaled_com
{
	List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786 * ___aryCoinCost_0;
	List_1_t026D7A8C4D989218772DB3E051A624F753A60859 * ___aryCoinPromote_1;
};
#endif // SPRESTIGECONFIG_T397FC1BE163EC0B938F2EC3B56694DCA340BAC63_H
#ifndef STRACKCONFIG_TC4F21E8F5FB966041FF5D2421A3AB04E318A005B_H
#define STRACKCONFIG_TC4F21E8F5FB966041FF5D2421A3AB04E318A005B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/sTrackConfig
struct  sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B 
{
public:
	// System.Int32 DataManager/sTrackConfig::nId
	int32_t ___nId_0;
	// System.String DataManager/sTrackConfig::szName
	String_t* ___szName_1;
	// System.Int32 DataManager/sTrackConfig::nPlanetId
	int32_t ___nPlanetId_2;
	// System.Double DataManager/sTrackConfig::nUnlockPrice
	double ___nUnlockPrice_3;
	// System.Single DataManager/sTrackConfig::fEarning
	float ___fEarning_4;
	// System.Double DataManager/sTrackConfig::nBuyAdminBasePrice
	double ___nBuyAdminBasePrice_5;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_szName_1() { return static_cast<int32_t>(offsetof(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B, ___szName_1)); }
	inline String_t* get_szName_1() const { return ___szName_1; }
	inline String_t** get_address_of_szName_1() { return &___szName_1; }
	inline void set_szName_1(String_t* value)
	{
		___szName_1 = value;
		Il2CppCodeGenWriteBarrier((&___szName_1), value);
	}

	inline static int32_t get_offset_of_nPlanetId_2() { return static_cast<int32_t>(offsetof(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B, ___nPlanetId_2)); }
	inline int32_t get_nPlanetId_2() const { return ___nPlanetId_2; }
	inline int32_t* get_address_of_nPlanetId_2() { return &___nPlanetId_2; }
	inline void set_nPlanetId_2(int32_t value)
	{
		___nPlanetId_2 = value;
	}

	inline static int32_t get_offset_of_nUnlockPrice_3() { return static_cast<int32_t>(offsetof(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B, ___nUnlockPrice_3)); }
	inline double get_nUnlockPrice_3() const { return ___nUnlockPrice_3; }
	inline double* get_address_of_nUnlockPrice_3() { return &___nUnlockPrice_3; }
	inline void set_nUnlockPrice_3(double value)
	{
		___nUnlockPrice_3 = value;
	}

	inline static int32_t get_offset_of_fEarning_4() { return static_cast<int32_t>(offsetof(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B, ___fEarning_4)); }
	inline float get_fEarning_4() const { return ___fEarning_4; }
	inline float* get_address_of_fEarning_4() { return &___fEarning_4; }
	inline void set_fEarning_4(float value)
	{
		___fEarning_4 = value;
	}

	inline static int32_t get_offset_of_nBuyAdminBasePrice_5() { return static_cast<int32_t>(offsetof(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B, ___nBuyAdminBasePrice_5)); }
	inline double get_nBuyAdminBasePrice_5() const { return ___nBuyAdminBasePrice_5; }
	inline double* get_address_of_nBuyAdminBasePrice_5() { return &___nBuyAdminBasePrice_5; }
	inline void set_nBuyAdminBasePrice_5(double value)
	{
		___nBuyAdminBasePrice_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DataManager/sTrackConfig
struct sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B_marshaled_pinvoke
{
	int32_t ___nId_0;
	char* ___szName_1;
	int32_t ___nPlanetId_2;
	double ___nUnlockPrice_3;
	float ___fEarning_4;
	double ___nBuyAdminBasePrice_5;
};
// Native definition for COM marshalling of DataManager/sTrackConfig
struct sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B_marshaled_com
{
	int32_t ___nId_0;
	Il2CppChar* ___szName_1;
	int32_t ___nPlanetId_2;
	double ___nUnlockPrice_3;
	float ___fEarning_4;
	double ___nBuyAdminBasePrice_5;
};
#endif // STRACKCONFIG_TC4F21E8F5FB966041FF5D2421A3AB04E318A005B_H
#ifndef SLEVELANDCOST_T54392C7967F904A78CC7723B1259304E036E0EBA_H
#define SLEVELANDCOST_T54392C7967F904A78CC7723B1259304E036E0EBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// District/sLevelAndCost
struct  sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA 
{
public:
	// System.Int32 District/sLevelAndCost::nLevel
	int32_t ___nLevel_0;
	// System.Double District/sLevelAndCost::dCurPrice
	double ___dCurPrice_1;
	// System.Double District/sLevelAndCost::dCost
	double ___dCost_2;
	// System.Int32 District/sLevelAndCost::nCurBuyTimes
	int32_t ___nCurBuyTimes_3;
	// System.Int32 District/sLevelAndCost::nToBuyTime
	int32_t ___nToBuyTime_4;

public:
	inline static int32_t get_offset_of_nLevel_0() { return static_cast<int32_t>(offsetof(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA, ___nLevel_0)); }
	inline int32_t get_nLevel_0() const { return ___nLevel_0; }
	inline int32_t* get_address_of_nLevel_0() { return &___nLevel_0; }
	inline void set_nLevel_0(int32_t value)
	{
		___nLevel_0 = value;
	}

	inline static int32_t get_offset_of_dCurPrice_1() { return static_cast<int32_t>(offsetof(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA, ___dCurPrice_1)); }
	inline double get_dCurPrice_1() const { return ___dCurPrice_1; }
	inline double* get_address_of_dCurPrice_1() { return &___dCurPrice_1; }
	inline void set_dCurPrice_1(double value)
	{
		___dCurPrice_1 = value;
	}

	inline static int32_t get_offset_of_dCost_2() { return static_cast<int32_t>(offsetof(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA, ___dCost_2)); }
	inline double get_dCost_2() const { return ___dCost_2; }
	inline double* get_address_of_dCost_2() { return &___dCost_2; }
	inline void set_dCost_2(double value)
	{
		___dCost_2 = value;
	}

	inline static int32_t get_offset_of_nCurBuyTimes_3() { return static_cast<int32_t>(offsetof(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA, ___nCurBuyTimes_3)); }
	inline int32_t get_nCurBuyTimes_3() const { return ___nCurBuyTimes_3; }
	inline int32_t* get_address_of_nCurBuyTimes_3() { return &___nCurBuyTimes_3; }
	inline void set_nCurBuyTimes_3(int32_t value)
	{
		___nCurBuyTimes_3 = value;
	}

	inline static int32_t get_offset_of_nToBuyTime_4() { return static_cast<int32_t>(offsetof(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA, ___nToBuyTime_4)); }
	inline int32_t get_nToBuyTime_4() const { return ___nToBuyTime_4; }
	inline int32_t* get_address_of_nToBuyTime_4() { return &___nToBuyTime_4; }
	inline void set_nToBuyTime_4(int32_t value)
	{
		___nToBuyTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLEVELANDCOST_T54392C7967F904A78CC7723B1259304E036E0EBA_H
#ifndef SITEMCONFIG_T9C277270FC8FC12356FC8BA5BD234112769E425C_H
#define SITEMCONFIG_T9C277270FC8FC12356FC8BA5BD234112769E425C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemSystem/sItemConfig
struct  sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C 
{
public:
	// System.Int32 ItemSystem/sItemConfig::nId
	int32_t ___nId_0;
	// System.String ItemSystem/sItemConfig::szName
	String_t* ___szName_1;
	// System.Int32 ItemSystem/sItemConfig::nType
	int32_t ___nType_2;
	// System.Int32 ItemSystem/sItemConfig::nResId
	int32_t ___nResId_3;
	// System.String ItemSystem/sItemConfig::szParams
	String_t* ___szParams_4;
	// System.Int32 ItemSystem/sItemConfig::nDuration
	int32_t ___nDuration_5;
	// System.String ItemSystem/sItemConfig::szDesc
	String_t* ___szDesc_6;
	// System.Int32[] ItemSystem/sItemConfig::aryIntParams
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___aryIntParams_7;
	// System.Single[] ItemSystem/sItemConfig::aryFloatParams
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___aryFloatParams_8;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_szName_1() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___szName_1)); }
	inline String_t* get_szName_1() const { return ___szName_1; }
	inline String_t** get_address_of_szName_1() { return &___szName_1; }
	inline void set_szName_1(String_t* value)
	{
		___szName_1 = value;
		Il2CppCodeGenWriteBarrier((&___szName_1), value);
	}

	inline static int32_t get_offset_of_nType_2() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___nType_2)); }
	inline int32_t get_nType_2() const { return ___nType_2; }
	inline int32_t* get_address_of_nType_2() { return &___nType_2; }
	inline void set_nType_2(int32_t value)
	{
		___nType_2 = value;
	}

	inline static int32_t get_offset_of_nResId_3() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___nResId_3)); }
	inline int32_t get_nResId_3() const { return ___nResId_3; }
	inline int32_t* get_address_of_nResId_3() { return &___nResId_3; }
	inline void set_nResId_3(int32_t value)
	{
		___nResId_3 = value;
	}

	inline static int32_t get_offset_of_szParams_4() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___szParams_4)); }
	inline String_t* get_szParams_4() const { return ___szParams_4; }
	inline String_t** get_address_of_szParams_4() { return &___szParams_4; }
	inline void set_szParams_4(String_t* value)
	{
		___szParams_4 = value;
		Il2CppCodeGenWriteBarrier((&___szParams_4), value);
	}

	inline static int32_t get_offset_of_nDuration_5() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___nDuration_5)); }
	inline int32_t get_nDuration_5() const { return ___nDuration_5; }
	inline int32_t* get_address_of_nDuration_5() { return &___nDuration_5; }
	inline void set_nDuration_5(int32_t value)
	{
		___nDuration_5 = value;
	}

	inline static int32_t get_offset_of_szDesc_6() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___szDesc_6)); }
	inline String_t* get_szDesc_6() const { return ___szDesc_6; }
	inline String_t** get_address_of_szDesc_6() { return &___szDesc_6; }
	inline void set_szDesc_6(String_t* value)
	{
		___szDesc_6 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_6), value);
	}

	inline static int32_t get_offset_of_aryIntParams_7() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___aryIntParams_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_aryIntParams_7() const { return ___aryIntParams_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_aryIntParams_7() { return &___aryIntParams_7; }
	inline void set_aryIntParams_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___aryIntParams_7 = value;
		Il2CppCodeGenWriteBarrier((&___aryIntParams_7), value);
	}

	inline static int32_t get_offset_of_aryFloatParams_8() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___aryFloatParams_8)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_aryFloatParams_8() const { return ___aryFloatParams_8; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_aryFloatParams_8() { return &___aryFloatParams_8; }
	inline void set_aryFloatParams_8(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___aryFloatParams_8 = value;
		Il2CppCodeGenWriteBarrier((&___aryFloatParams_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ItemSystem/sItemConfig
struct sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C_marshaled_pinvoke
{
	int32_t ___nId_0;
	char* ___szName_1;
	int32_t ___nType_2;
	int32_t ___nResId_3;
	char* ___szParams_4;
	int32_t ___nDuration_5;
	char* ___szDesc_6;
	int32_t* ___aryIntParams_7;
	float* ___aryFloatParams_8;
};
// Native definition for COM marshalling of ItemSystem/sItemConfig
struct sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C_marshaled_com
{
	int32_t ___nId_0;
	Il2CppChar* ___szName_1;
	int32_t ___nType_2;
	int32_t ___nResId_3;
	Il2CppChar* ___szParams_4;
	int32_t ___nDuration_5;
	Il2CppChar* ___szDesc_6;
	int32_t* ___aryIntParams_7;
	float* ___aryFloatParams_8;
};
#endif // SITEMCONFIG_T9C277270FC8FC12356FC8BA5BD234112769E425C_H
#ifndef SRESEARCHCONFIG_T0C6B8A3EB463658005C7DD0B324869F729AD21C9_H
#define SRESEARCHCONFIG_T0C6B8A3EB463658005C7DD0B324869F729AD21C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResearchManager/sResearchConfig
struct  sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9 
{
public:
	// System.Double ResearchManager/sResearchConfig::nCoinCost
	double ___nCoinCost_0;
	// System.Int32 ResearchManager/sResearchConfig::nTimeCost
	int32_t ___nTimeCost_1;
	// System.Int32 ResearchManager/sResearchConfig::nDiamondCost
	int32_t ___nDiamondCost_2;

public:
	inline static int32_t get_offset_of_nCoinCost_0() { return static_cast<int32_t>(offsetof(sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9, ___nCoinCost_0)); }
	inline double get_nCoinCost_0() const { return ___nCoinCost_0; }
	inline double* get_address_of_nCoinCost_0() { return &___nCoinCost_0; }
	inline void set_nCoinCost_0(double value)
	{
		___nCoinCost_0 = value;
	}

	inline static int32_t get_offset_of_nTimeCost_1() { return static_cast<int32_t>(offsetof(sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9, ___nTimeCost_1)); }
	inline int32_t get_nTimeCost_1() const { return ___nTimeCost_1; }
	inline int32_t* get_address_of_nTimeCost_1() { return &___nTimeCost_1; }
	inline void set_nTimeCost_1(int32_t value)
	{
		___nTimeCost_1 = value;
	}

	inline static int32_t get_offset_of_nDiamondCost_2() { return static_cast<int32_t>(offsetof(sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9, ___nDiamondCost_2)); }
	inline int32_t get_nDiamondCost_2() const { return ___nDiamondCost_2; }
	inline int32_t* get_address_of_nDiamondCost_2() { return &___nDiamondCost_2; }
	inline void set_nDiamondCost_2(int32_t value)
	{
		___nDiamondCost_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SRESEARCHCONFIG_T0C6B8A3EB463658005C7DD0B324869F729AD21C9_H
#ifndef SSKILLCONFIG_TC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E_H
#define SSKILLCONFIG_TC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkillManager/sSkillConfig
struct  sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E 
{
public:
	// System.Single SkillManager/sSkillConfig::fValue
	float ___fValue_0;
	// System.Single SkillManager/sSkillConfig::nColdDown
	float ___nColdDown_1;
	// System.Single SkillManager/sSkillConfig::nDuration
	float ___nDuration_2;

public:
	inline static int32_t get_offset_of_fValue_0() { return static_cast<int32_t>(offsetof(sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E, ___fValue_0)); }
	inline float get_fValue_0() const { return ___fValue_0; }
	inline float* get_address_of_fValue_0() { return &___fValue_0; }
	inline void set_fValue_0(float value)
	{
		___fValue_0 = value;
	}

	inline static int32_t get_offset_of_nColdDown_1() { return static_cast<int32_t>(offsetof(sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E, ___nColdDown_1)); }
	inline float get_nColdDown_1() const { return ___nColdDown_1; }
	inline float* get_address_of_nColdDown_1() { return &___nColdDown_1; }
	inline void set_nColdDown_1(float value)
	{
		___nColdDown_1 = value;
	}

	inline static int32_t get_offset_of_nDuration_2() { return static_cast<int32_t>(offsetof(sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E, ___nDuration_2)); }
	inline float get_nDuration_2() const { return ___nDuration_2; }
	inline float* get_address_of_nDuration_2() { return &___nDuration_2; }
	inline void set_nDuration_2(float value)
	{
		___nDuration_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSKILLCONFIG_TC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef EADVENTUREPROFITTYPE_TD199C31799F1616902B9916C0195AE7BBE3D60CC_H
#define EADVENTUREPROFITTYPE_TD199C31799F1616902B9916C0195AE7BBE3D60CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdventureManager/eAdventureProfitType
struct  eAdventureProfitType_tD199C31799F1616902B9916C0195AE7BBE3D60CC 
{
public:
	// System.Int32 AdventureManager/eAdventureProfitType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eAdventureProfitType_tD199C31799F1616902B9916C0195AE7BBE3D60CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EADVENTUREPROFITTYPE_TD199C31799F1616902B9916C0195AE7BBE3D60CC_H
#ifndef ESE_TEBFC5E9CC8CA17804BEE458B4DAE21100B2641FD_H
#define ESE_TEBFC5E9CC8CA17804BEE458B4DAE21100B2641FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager/eSE
struct  eSE_tEBFC5E9CC8CA17804BEE458B4DAE21100B2641FD 
{
public:
	// System.Int32 AudioManager/eSE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eSE_tEBFC5E9CC8CA17804BEE458B4DAE21100B2641FD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESE_TEBFC5E9CC8CA17804BEE458B4DAE21100B2641FD_H
#ifndef ESE_NEW_TED4E1E0AAC6B985EE81D0661351734AC7DFFDFC2_H
#define ESE_NEW_TED4E1E0AAC6B985EE81D0661351734AC7DFFDFC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager/eSe_New
struct  eSe_New_tED4E1E0AAC6B985EE81D0661351734AC7DFFDFC2 
{
public:
	// System.Int32 AudioManager/eSe_New::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eSe_New_tED4E1E0AAC6B985EE81D0661351734AC7DFFDFC2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESE_NEW_TED4E1E0AAC6B985EE81D0661351734AC7DFFDFC2_H
#ifndef ESCALEAXIS_T2049C60FBA33DD8A1C5CAC79ECF499AF981117A2_H
#define ESCALEAXIS_T2049C60FBA33DD8A1C5CAC79ECF499AF981117A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseScale/eScaleAxis
struct  eScaleAxis_t2049C60FBA33DD8A1C5CAC79ECF499AF981117A2 
{
public:
	// System.Int32 BaseScale/eScaleAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eScaleAxis_t2049C60FBA33DD8A1C5CAC79ECF499AF981117A2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESCALEAXIS_T2049C60FBA33DD8A1C5CAC79ECF499AF981117A2_H
#ifndef EBOXTYPE_TC389FEFF432D0270A9F0BF3093CC5603026BE301_H
#define EBOXTYPE_TC389FEFF432D0270A9F0BF3093CC5603026BE301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoxManager/eBoxType
struct  eBoxType_tC389FEFF432D0270A9F0BF3093CC5603026BE301 
{
public:
	// System.Int32 BoxManager/eBoxType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eBoxType_tC389FEFF432D0270A9F0BF3093CC5603026BE301, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EBOXTYPE_TC389FEFF432D0270A9F0BF3093CC5603026BE301_H
#ifndef EBUFFTYPE_T78DAFF47F067B735F9426A12B7585E2FCE124F8E_H
#define EBUFFTYPE_T78DAFF47F067B735F9426A12B7585E2FCE124F8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuffManager/eBuffType
struct  eBuffType_t78DAFF47F067B735F9426A12B7585E2FCE124F8E 
{
public:
	// System.Int32 BuffManager/eBuffType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eBuffType_t78DAFF47F067B735F9426A12B7585E2FCE124F8E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EBUFFTYPE_T78DAFF47F067B735F9426A12B7585E2FCE124F8E_H
#ifndef EMONEYTYPE_T9A22FBEEECC2803A11A7756315481457B389F4C1_H
#define EMONEYTYPE_T9A22FBEEECC2803A11A7756315481457B389F4C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/eMoneyType
struct  eMoneyType_t9A22FBEEECC2803A11A7756315481457B389F4C1 
{
public:
	// System.Int32 DataManager/eMoneyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eMoneyType_t9A22FBEEECC2803A11A7756315481457B389F4C1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMONEYTYPE_T9A22FBEEECC2803A11A7756315481457B389F4C1_H
#ifndef EEFFECTTYPE_T37AF40DF21D81FB2776F142F88D342C51372F583_H
#define EEFFECTTYPE_T37AF40DF21D81FB2776F142F88D342C51372F583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectManager/eEffectType
struct  eEffectType_t37AF40DF21D81FB2776F142F88D342C51372F583 
{
public:
	// System.Int32 EffectManager/eEffectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eEffectType_t37AF40DF21D81FB2776F142F88D342C51372F583, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EEFFECTTYPE_T37AF40DF21D81FB2776F142F88D342C51372F583_H
#ifndef EGUIDETYPE_T90774CD9942800464AB6E6A8D72E0F77452E0288_H
#define EGUIDETYPE_T90774CD9942800464AB6E6A8D72E0F77452E0288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FreshGuide/eGuideType
struct  eGuideType_t90774CD9942800464AB6E6A8D72E0F77452E0288 
{
public:
	// System.Int32 FreshGuide/eGuideType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eGuideType_t90774CD9942800464AB6E6A8D72E0F77452E0288, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EGUIDETYPE_T90774CD9942800464AB6E6A8D72E0F77452E0288_H
#ifndef EPLANESTATUS_TAA6F08AE7DEE870620C7F8E6459B8578DECEC0F4_H
#define EPLANESTATUS_TAA6F08AE7DEE870620C7F8E6459B8578DECEC0F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main/ePlaneStatus
struct  ePlaneStatus_tAA6F08AE7DEE870620C7F8E6459B8578DECEC0F4 
{
public:
	// System.Int32 Main/ePlaneStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ePlaneStatus_tAA6F08AE7DEE870620C7F8E6459B8578DECEC0F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EPLANESTATUS_TAA6F08AE7DEE870620C7F8E6459B8578DECEC0F4_H
#ifndef EPOSTYPE_TE6D03887D78F8D40B765866D3E62F40C088C676A_H
#define EPOSTYPE_TE6D03887D78F8D40B765866D3E62F40C088C676A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main/ePosType
struct  ePosType_tE6D03887D78F8D40B765866D3E62F40C088C676A 
{
public:
	// System.Int32 Main/ePosType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ePosType_tE6D03887D78F8D40B765866D3E62F40C088C676A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EPOSTYPE_TE6D03887D78F8D40B765866D3E62F40C088C676A_H
#ifndef SAUTORUNLOCATION_T1B6EE8072FCC8B63AD96FF14493824FE7F1AC949_H
#define SAUTORUNLOCATION_T1B6EE8072FCC8B63AD96FF14493824FE7F1AC949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main/sAutoRunLocation
struct  sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949 
{
public:
	// System.Int32 Main/sAutoRunLocation::nLocationIndex
	int32_t ___nLocationIndex_0;
	// UnityEngine.Vector3 Main/sAutoRunLocation::vecSrc
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecSrc_1;
	// UnityEngine.Vector3 Main/sAutoRunLocation::vecDest
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecDest_2;
	// UnityEngine.Vector3 Main/sAutoRunLocation::vecDir
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecDir_3;
	// UnityEngine.Vector3 Main/sAutoRunLocation::vecShowDir
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecShowDir_4;
	// System.Single Main/sAutoRunLocation::fRotateAngle
	float ___fRotateAngle_5;
	// System.Single Main/sAutoRunLocation::fDeltaAngleToNextNode
	float ___fDeltaAngleToNextNode_6;
	// System.Single Main/sAutoRunLocation::fDistanceToNextNode
	float ___fDistanceToNextNode_7;

public:
	inline static int32_t get_offset_of_nLocationIndex_0() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___nLocationIndex_0)); }
	inline int32_t get_nLocationIndex_0() const { return ___nLocationIndex_0; }
	inline int32_t* get_address_of_nLocationIndex_0() { return &___nLocationIndex_0; }
	inline void set_nLocationIndex_0(int32_t value)
	{
		___nLocationIndex_0 = value;
	}

	inline static int32_t get_offset_of_vecSrc_1() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___vecSrc_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecSrc_1() const { return ___vecSrc_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecSrc_1() { return &___vecSrc_1; }
	inline void set_vecSrc_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecSrc_1 = value;
	}

	inline static int32_t get_offset_of_vecDest_2() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___vecDest_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecDest_2() const { return ___vecDest_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecDest_2() { return &___vecDest_2; }
	inline void set_vecDest_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecDest_2 = value;
	}

	inline static int32_t get_offset_of_vecDir_3() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___vecDir_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecDir_3() const { return ___vecDir_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecDir_3() { return &___vecDir_3; }
	inline void set_vecDir_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecDir_3 = value;
	}

	inline static int32_t get_offset_of_vecShowDir_4() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___vecShowDir_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecShowDir_4() const { return ___vecShowDir_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecShowDir_4() { return &___vecShowDir_4; }
	inline void set_vecShowDir_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecShowDir_4 = value;
	}

	inline static int32_t get_offset_of_fRotateAngle_5() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___fRotateAngle_5)); }
	inline float get_fRotateAngle_5() const { return ___fRotateAngle_5; }
	inline float* get_address_of_fRotateAngle_5() { return &___fRotateAngle_5; }
	inline void set_fRotateAngle_5(float value)
	{
		___fRotateAngle_5 = value;
	}

	inline static int32_t get_offset_of_fDeltaAngleToNextNode_6() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___fDeltaAngleToNextNode_6)); }
	inline float get_fDeltaAngleToNextNode_6() const { return ___fDeltaAngleToNextNode_6; }
	inline float* get_address_of_fDeltaAngleToNextNode_6() { return &___fDeltaAngleToNextNode_6; }
	inline void set_fDeltaAngleToNextNode_6(float value)
	{
		___fDeltaAngleToNextNode_6 = value;
	}

	inline static int32_t get_offset_of_fDistanceToNextNode_7() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___fDistanceToNextNode_7)); }
	inline float get_fDistanceToNextNode_7() const { return ___fDistanceToNextNode_7; }
	inline float* get_address_of_fDistanceToNextNode_7() { return &___fDistanceToNextNode_7; }
	inline void set_fDistanceToNextNode_7(float value)
	{
		___fDistanceToNextNode_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAUTORUNLOCATION_T1B6EE8072FCC8B63AD96FF14493824FE7F1AC949_H
#ifndef EDISTRICTSTATUS_TA21EDB657F7184F24E00E5C52D26254A33458EC8_H
#define EDISTRICTSTATUS_TA21EDB657F7184F24E00E5C52D26254A33458EC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapManager/eDistrictStatus
struct  eDistrictStatus_tA21EDB657F7184F24E00E5C52D26254A33458EC8 
{
public:
	// System.Int32 MapManager/eDistrictStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eDistrictStatus_tA21EDB657F7184F24E00E5C52D26254A33458EC8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDISTRICTSTATUS_TA21EDB657F7184F24E00E5C52D26254A33458EC8_H
#ifndef EPLANETID_T35BB1983FD70DAE2F267AF836478B4561EECF2D4_H
#define EPLANETID_T35BB1983FD70DAE2F267AF836478B4561EECF2D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapManager/ePlanetId
struct  ePlanetId_t35BB1983FD70DAE2F267AF836478B4561EECF2D4 
{
public:
	// System.Int32 MapManager/ePlanetId::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ePlanetId_t35BB1983FD70DAE2F267AF836478B4561EECF2D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EPLANETID_T35BB1983FD70DAE2F267AF836478B4561EECF2D4_H
#ifndef EPLANETSTATUS_TC2E1F5015F795EE2F74D5F17749C432ECF4567EE_H
#define EPLANETSTATUS_TC2E1F5015F795EE2F74D5F17749C432ECF4567EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapManager/ePlanetStatus
struct  ePlanetStatus_tC2E1F5015F795EE2F74D5F17749C432ECF4567EE 
{
public:
	// System.Int32 MapManager/ePlanetStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ePlanetStatus_tC2E1F5015F795EE2F74D5F17749C432ECF4567EE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EPLANETSTATUS_TC2E1F5015F795EE2F74D5F17749C432ECF4567EE_H
#ifndef ERESEARCHCOUNTERSTATUS_TBBB7AB208D35C7D68CE9FD1CC40A13533A0F325F_H
#define ERESEARCHCOUNTERSTATUS_TBBB7AB208D35C7D68CE9FD1CC40A13533A0F325F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResearchManager/eResearchCounterStatus
struct  eResearchCounterStatus_tBBB7AB208D35C7D68CE9FD1CC40A13533A0F325F 
{
public:
	// System.Int32 ResearchManager/eResearchCounterStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eResearchCounterStatus_tBBB7AB208D35C7D68CE9FD1CC40A13533A0F325F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERESEARCHCOUNTERSTATUS_TBBB7AB208D35C7D68CE9FD1CC40A13533A0F325F_H
#ifndef ERESOLUTION_TF2B80E36A8A199AEEF1E37CDF36DFE5F7EE3F0FE_H
#define ERESOLUTION_TF2B80E36A8A199AEEF1E37CDF36DFE5F7EE3F0FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResolutionManager/eResolution
struct  eResolution_tF2B80E36A8A199AEEF1E37CDF36DFE5F7EE3F0FE 
{
public:
	// System.Int32 ResolutionManager/eResolution::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eResolution_tF2B80E36A8A199AEEF1E37CDF36DFE5F7EE3F0FE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERESOLUTION_TF2B80E36A8A199AEEF1E37CDF36DFE5F7EE3F0FE_H
#ifndef EITEMICONTYPE_T1AA9127543953891EBC2D8BC6E1A8E917CED3F49_H
#define EITEMICONTYPE_T1AA9127543953891EBC2D8BC6E1A8E917CED3F49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager/eItemIconType
struct  eItemIconType_t1AA9127543953891EBC2D8BC6E1A8E917CED3F49 
{
public:
	// System.Int32 ResourceManager/eItemIconType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eItemIconType_t1AA9127543953891EBC2D8BC6E1A8E917CED3F49, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EITEMICONTYPE_T1AA9127543953891EBC2D8BC6E1A8E917CED3F49_H
#ifndef EBRANCHTYPE_TA7C9751A118EB41EC27607C7387348B868619540_H
#define EBRANCHTYPE_TA7C9751A118EB41EC27607C7387348B868619540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScienceTree/eBranchType
struct  eBranchType_tA7C9751A118EB41EC27607C7387348B868619540 
{
public:
	// System.Int32 ScienceTree/eBranchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eBranchType_tA7C9751A118EB41EC27607C7387348B868619540, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EBRANCHTYPE_TA7C9751A118EB41EC27607C7387348B868619540_H
#ifndef ELEAFCONFIG_TE6D039658A5FBCA8E009DDCD81DBCE5ECF027A09_H
#define ELEAFCONFIG_TE6D039658A5FBCA8E009DDCD81DBCE5ECF027A09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScienceTree/eLeafConfig
struct  eLeafConfig_tE6D039658A5FBCA8E009DDCD81DBCE5ECF027A09 
{
public:
	// System.Int32 ScienceTree/eLeafConfig::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eLeafConfig_tE6D039658A5FBCA8E009DDCD81DBCE5ECF027A09, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEAFCONFIG_TE6D039658A5FBCA8E009DDCD81DBCE5ECF027A09_H
#ifndef ESCIENCETYPE_T407F29E64187E9619231D131CBD33C05C0E3DB4E_H
#define ESCIENCETYPE_T407F29E64187E9619231D131CBD33C05C0E3DB4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScienceTree/eScienceType
struct  eScienceType_t407F29E64187E9619231D131CBD33C05C0E3DB4E 
{
public:
	// System.Int32 ScienceTree/eScienceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eScienceType_t407F29E64187E9619231D131CBD33C05C0E3DB4E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESCIENCETYPE_T407F29E64187E9619231D131CBD33C05C0E3DB4E_H
#ifndef EITEMTYPE_T796F7E98F0ED3F4BC872A243ECB26EBF2A5A25A4_H
#define EITEMTYPE_T796F7E98F0ED3F4BC872A243ECB26EBF2A5A25A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppinMall/eItemType
struct  eItemType_t796F7E98F0ED3F4BC872A243ECB26EBF2A5A25A4 
{
public:
	// System.Int32 ShoppinMall/eItemType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eItemType_t796F7E98F0ED3F4BC872A243ECB26EBF2A5A25A4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EITEMTYPE_T796F7E98F0ED3F4BC872A243ECB26EBF2A5A25A4_H
#ifndef EPRICESUBTYPE_T452224C5C4361AD927ABAAB951C185B7D2801B5D_H
#define EPRICESUBTYPE_T452224C5C4361AD927ABAAB951C185B7D2801B5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppinMall/ePriceSubType
struct  ePriceSubType_t452224C5C4361AD927ABAAB951C185B7D2801B5D 
{
public:
	// System.Int32 ShoppinMall/ePriceSubType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ePriceSubType_t452224C5C4361AD927ABAAB951C185B7D2801B5D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EPRICESUBTYPE_T452224C5C4361AD927ABAAB951C185B7D2801B5D_H
#ifndef EPRICETYPE_T2A0ABDBAB8B516FAA133FC73FC2F9C0F3B46E4EA_H
#define EPRICETYPE_T2A0ABDBAB8B516FAA133FC73FC2F9C0F3B46E4EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppinMall/ePriceType
struct  ePriceType_t2A0ABDBAB8B516FAA133FC73FC2F9C0F3B46E4EA 
{
public:
	// System.Int32 ShoppinMall/ePriceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ePriceType_t2A0ABDBAB8B516FAA133FC73FC2F9C0F3B46E4EA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EPRICETYPE_T2A0ABDBAB8B516FAA133FC73FC2F9C0F3B46E4EA_H
#ifndef SSHOPPINGMALLITEMCONFIG_T77C43AC55058BC53A189F0EEC0D09D27D9B0D058_H
#define SSHOPPINGMALLITEMCONFIG_T77C43AC55058BC53A189F0EEC0D09D27D9B0D058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppinMall/sShoppingMallItemConfig
struct  sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058 
{
public:
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nId
	int32_t ___nId_0;
	// ItemSystem/sItemConfig ShoppinMall/sShoppingMallItemConfig::itemConfig
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C  ___itemConfig_1;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nPriceType1
	int32_t ___nPriceType1_2;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nPriceSubType1
	int32_t ___nPriceSubType1_3;
	// System.Double ShoppinMall/sShoppingMallItemConfig::nPrice1
	double ___nPrice1_4;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nNum1
	int32_t ___nNum1_5;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nPriceType2
	int32_t ___nPriceType2_6;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nPriceSubType2
	int32_t ___nPriceSubType2_7;
	// System.Double ShoppinMall/sShoppingMallItemConfig::nPrice2
	double ___nPrice2_8;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nNum2
	int32_t ___nNum2_9;
	// System.Single ShoppinMall/sShoppingMallItemConfig::fRisePricePercent
	float ___fRisePricePercent_10;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nNum
	int32_t ___nNum_11;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nOn
	int32_t ___nOn_12;
	// System.String ShoppinMall/sShoppingMallItemConfig::szDesc
	String_t* ___szDesc_13;
	// System.String ShoppinMall/sShoppingMallItemConfig::szIconId
	String_t* ___szIconId_14;
	// System.Single ShoppinMall/sShoppingMallItemConfig::fPriceRiseEachBuy
	float ___fPriceRiseEachBuy_15;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_itemConfig_1() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___itemConfig_1)); }
	inline sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C  get_itemConfig_1() const { return ___itemConfig_1; }
	inline sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C * get_address_of_itemConfig_1() { return &___itemConfig_1; }
	inline void set_itemConfig_1(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C  value)
	{
		___itemConfig_1 = value;
	}

	inline static int32_t get_offset_of_nPriceType1_2() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nPriceType1_2)); }
	inline int32_t get_nPriceType1_2() const { return ___nPriceType1_2; }
	inline int32_t* get_address_of_nPriceType1_2() { return &___nPriceType1_2; }
	inline void set_nPriceType1_2(int32_t value)
	{
		___nPriceType1_2 = value;
	}

	inline static int32_t get_offset_of_nPriceSubType1_3() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nPriceSubType1_3)); }
	inline int32_t get_nPriceSubType1_3() const { return ___nPriceSubType1_3; }
	inline int32_t* get_address_of_nPriceSubType1_3() { return &___nPriceSubType1_3; }
	inline void set_nPriceSubType1_3(int32_t value)
	{
		___nPriceSubType1_3 = value;
	}

	inline static int32_t get_offset_of_nPrice1_4() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nPrice1_4)); }
	inline double get_nPrice1_4() const { return ___nPrice1_4; }
	inline double* get_address_of_nPrice1_4() { return &___nPrice1_4; }
	inline void set_nPrice1_4(double value)
	{
		___nPrice1_4 = value;
	}

	inline static int32_t get_offset_of_nNum1_5() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nNum1_5)); }
	inline int32_t get_nNum1_5() const { return ___nNum1_5; }
	inline int32_t* get_address_of_nNum1_5() { return &___nNum1_5; }
	inline void set_nNum1_5(int32_t value)
	{
		___nNum1_5 = value;
	}

	inline static int32_t get_offset_of_nPriceType2_6() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nPriceType2_6)); }
	inline int32_t get_nPriceType2_6() const { return ___nPriceType2_6; }
	inline int32_t* get_address_of_nPriceType2_6() { return &___nPriceType2_6; }
	inline void set_nPriceType2_6(int32_t value)
	{
		___nPriceType2_6 = value;
	}

	inline static int32_t get_offset_of_nPriceSubType2_7() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nPriceSubType2_7)); }
	inline int32_t get_nPriceSubType2_7() const { return ___nPriceSubType2_7; }
	inline int32_t* get_address_of_nPriceSubType2_7() { return &___nPriceSubType2_7; }
	inline void set_nPriceSubType2_7(int32_t value)
	{
		___nPriceSubType2_7 = value;
	}

	inline static int32_t get_offset_of_nPrice2_8() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nPrice2_8)); }
	inline double get_nPrice2_8() const { return ___nPrice2_8; }
	inline double* get_address_of_nPrice2_8() { return &___nPrice2_8; }
	inline void set_nPrice2_8(double value)
	{
		___nPrice2_8 = value;
	}

	inline static int32_t get_offset_of_nNum2_9() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nNum2_9)); }
	inline int32_t get_nNum2_9() const { return ___nNum2_9; }
	inline int32_t* get_address_of_nNum2_9() { return &___nNum2_9; }
	inline void set_nNum2_9(int32_t value)
	{
		___nNum2_9 = value;
	}

	inline static int32_t get_offset_of_fRisePricePercent_10() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___fRisePricePercent_10)); }
	inline float get_fRisePricePercent_10() const { return ___fRisePricePercent_10; }
	inline float* get_address_of_fRisePricePercent_10() { return &___fRisePricePercent_10; }
	inline void set_fRisePricePercent_10(float value)
	{
		___fRisePricePercent_10 = value;
	}

	inline static int32_t get_offset_of_nNum_11() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nNum_11)); }
	inline int32_t get_nNum_11() const { return ___nNum_11; }
	inline int32_t* get_address_of_nNum_11() { return &___nNum_11; }
	inline void set_nNum_11(int32_t value)
	{
		___nNum_11 = value;
	}

	inline static int32_t get_offset_of_nOn_12() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nOn_12)); }
	inline int32_t get_nOn_12() const { return ___nOn_12; }
	inline int32_t* get_address_of_nOn_12() { return &___nOn_12; }
	inline void set_nOn_12(int32_t value)
	{
		___nOn_12 = value;
	}

	inline static int32_t get_offset_of_szDesc_13() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___szDesc_13)); }
	inline String_t* get_szDesc_13() const { return ___szDesc_13; }
	inline String_t** get_address_of_szDesc_13() { return &___szDesc_13; }
	inline void set_szDesc_13(String_t* value)
	{
		___szDesc_13 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_13), value);
	}

	inline static int32_t get_offset_of_szIconId_14() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___szIconId_14)); }
	inline String_t* get_szIconId_14() const { return ___szIconId_14; }
	inline String_t** get_address_of_szIconId_14() { return &___szIconId_14; }
	inline void set_szIconId_14(String_t* value)
	{
		___szIconId_14 = value;
		Il2CppCodeGenWriteBarrier((&___szIconId_14), value);
	}

	inline static int32_t get_offset_of_fPriceRiseEachBuy_15() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___fPriceRiseEachBuy_15)); }
	inline float get_fPriceRiseEachBuy_15() const { return ___fPriceRiseEachBuy_15; }
	inline float* get_address_of_fPriceRiseEachBuy_15() { return &___fPriceRiseEachBuy_15; }
	inline void set_fPriceRiseEachBuy_15(float value)
	{
		___fPriceRiseEachBuy_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ShoppinMall/sShoppingMallItemConfig
struct sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058_marshaled_pinvoke
{
	int32_t ___nId_0;
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C_marshaled_pinvoke ___itemConfig_1;
	int32_t ___nPriceType1_2;
	int32_t ___nPriceSubType1_3;
	double ___nPrice1_4;
	int32_t ___nNum1_5;
	int32_t ___nPriceType2_6;
	int32_t ___nPriceSubType2_7;
	double ___nPrice2_8;
	int32_t ___nNum2_9;
	float ___fRisePricePercent_10;
	int32_t ___nNum_11;
	int32_t ___nOn_12;
	char* ___szDesc_13;
	char* ___szIconId_14;
	float ___fPriceRiseEachBuy_15;
};
// Native definition for COM marshalling of ShoppinMall/sShoppingMallItemConfig
struct sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058_marshaled_com
{
	int32_t ___nId_0;
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C_marshaled_com ___itemConfig_1;
	int32_t ___nPriceType1_2;
	int32_t ___nPriceSubType1_3;
	double ___nPrice1_4;
	int32_t ___nNum1_5;
	int32_t ___nPriceType2_6;
	int32_t ___nPriceSubType2_7;
	double ___nPrice2_8;
	int32_t ___nNum2_9;
	float ___fRisePricePercent_10;
	int32_t ___nNum_11;
	int32_t ___nOn_12;
	Il2CppChar* ___szDesc_13;
	Il2CppChar* ___szIconId_14;
	float ___fPriceRiseEachBuy_15;
};
#endif // SSHOPPINGMALLITEMCONFIG_T77C43AC55058BC53A189F0EEC0D09D27D9B0D058_H
#ifndef ESKILLSTATUS_T7ACD9CEC3A4B4D024083BE0233781CAE1E8DA753_H
#define ESKILLSTATUS_T7ACD9CEC3A4B4D024083BE0233781CAE1E8DA753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkillManager/eSkillStatus
struct  eSkillStatus_t7ACD9CEC3A4B4D024083BE0233781CAE1E8DA753 
{
public:
	// System.Int32 SkillManager/eSkillStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eSkillStatus_t7ACD9CEC3A4B4D024083BE0233781CAE1E8DA753, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESKILLSTATUS_T7ACD9CEC3A4B4D024083BE0233781CAE1E8DA753_H
#ifndef ESKILLTYPE_T6B98D3D74E24B3B140959305730C30677538C578_H
#define ESKILLTYPE_T6B98D3D74E24B3B140959305730C30677538C578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkillManager/eSkillType
struct  eSkillType_t6B98D3D74E24B3B140959305730C30677538C578 
{
public:
	// System.Int32 SkillManager/eSkillType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eSkillType_t6B98D3D74E24B3B140959305730C30677538C578, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESKILLTYPE_T6B98D3D74E24B3B140959305730C30677538C578_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef EVEHICLECOOUNTERSTATUS_T0D796DADCA513036C03B7F33D32F457BE2C3D475_H
#define EVEHICLECOOUNTERSTATUS_T0D796DADCA513036C03B7F33D32F457BE2C3D475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TanGeChe/eVehicleCoounterStatus
struct  eVehicleCoounterStatus_t0D796DADCA513036C03B7F33D32F457BE2C3D475 
{
public:
	// System.Int32 TanGeChe/eVehicleCoounterStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eVehicleCoounterStatus_t0D796DADCA513036C03B7F33D32F457BE2C3D475, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVEHICLECOOUNTERSTATUS_T0D796DADCA513036C03B7F33D32F457BE2C3D475_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef SPLANETCONFIG_TB3752904FD33E8B33195C2F67744EF265D750AA9_H
#define SPLANETCONFIG_TB3752904FD33E8B33195C2F67744EF265D750AA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/sPlanetConfig
struct  sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9 
{
public:
	// System.String DataManager/sPlanetConfig::szName
	String_t* ___szName_0;
	// System.Int32 DataManager/sPlanetConfig::nId
	int32_t ___nId_1;
	// System.Double DataManager/sPlanetConfig::nUnlockPrice
	double ___nUnlockPrice_2;
	// DataManager/eMoneyType DataManager/sPlanetConfig::eUnlockMoneyType
	int32_t ___eUnlockMoneyType_3;
	// System.Double DataManager/sPlanetConfig::dInitCoin
	double ___dInitCoin_4;
	// System.Double DataManager/sPlanetConfig::dInitSkillPoint
	double ___dInitSkillPoint_5;

public:
	inline static int32_t get_offset_of_szName_0() { return static_cast<int32_t>(offsetof(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9, ___szName_0)); }
	inline String_t* get_szName_0() const { return ___szName_0; }
	inline String_t** get_address_of_szName_0() { return &___szName_0; }
	inline void set_szName_0(String_t* value)
	{
		___szName_0 = value;
		Il2CppCodeGenWriteBarrier((&___szName_0), value);
	}

	inline static int32_t get_offset_of_nId_1() { return static_cast<int32_t>(offsetof(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9, ___nId_1)); }
	inline int32_t get_nId_1() const { return ___nId_1; }
	inline int32_t* get_address_of_nId_1() { return &___nId_1; }
	inline void set_nId_1(int32_t value)
	{
		___nId_1 = value;
	}

	inline static int32_t get_offset_of_nUnlockPrice_2() { return static_cast<int32_t>(offsetof(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9, ___nUnlockPrice_2)); }
	inline double get_nUnlockPrice_2() const { return ___nUnlockPrice_2; }
	inline double* get_address_of_nUnlockPrice_2() { return &___nUnlockPrice_2; }
	inline void set_nUnlockPrice_2(double value)
	{
		___nUnlockPrice_2 = value;
	}

	inline static int32_t get_offset_of_eUnlockMoneyType_3() { return static_cast<int32_t>(offsetof(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9, ___eUnlockMoneyType_3)); }
	inline int32_t get_eUnlockMoneyType_3() const { return ___eUnlockMoneyType_3; }
	inline int32_t* get_address_of_eUnlockMoneyType_3() { return &___eUnlockMoneyType_3; }
	inline void set_eUnlockMoneyType_3(int32_t value)
	{
		___eUnlockMoneyType_3 = value;
	}

	inline static int32_t get_offset_of_dInitCoin_4() { return static_cast<int32_t>(offsetof(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9, ___dInitCoin_4)); }
	inline double get_dInitCoin_4() const { return ___dInitCoin_4; }
	inline double* get_address_of_dInitCoin_4() { return &___dInitCoin_4; }
	inline void set_dInitCoin_4(double value)
	{
		___dInitCoin_4 = value;
	}

	inline static int32_t get_offset_of_dInitSkillPoint_5() { return static_cast<int32_t>(offsetof(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9, ___dInitSkillPoint_5)); }
	inline double get_dInitSkillPoint_5() const { return ___dInitSkillPoint_5; }
	inline double* get_address_of_dInitSkillPoint_5() { return &___dInitSkillPoint_5; }
	inline void set_dInitSkillPoint_5(double value)
	{
		___dInitSkillPoint_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DataManager/sPlanetConfig
struct sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9_marshaled_pinvoke
{
	char* ___szName_0;
	int32_t ___nId_1;
	double ___nUnlockPrice_2;
	int32_t ___eUnlockMoneyType_3;
	double ___dInitCoin_4;
	double ___dInitSkillPoint_5;
};
// Native definition for COM marshalling of DataManager/sPlanetConfig
struct sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9_marshaled_com
{
	Il2CppChar* ___szName_0;
	int32_t ___nId_1;
	double ___nUnlockPrice_2;
	int32_t ___eUnlockMoneyType_3;
	double ___dInitCoin_4;
	double ___dInitSkillPoint_5;
};
#endif // SPLANETCONFIG_TB3752904FD33E8B33195C2F67744EF265D750AA9_H
#ifndef SLEAFCONFIG_T101684B4AA2B2843705D9A91099D4EA539E12F11_H
#define SLEAFCONFIG_T101684B4AA2B2843705D9A91099D4EA539E12F11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScienceTree/sLeafConfig
struct  sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11 
{
public:
	// ScienceTree/eLeafConfig ScienceTree/sLeafConfig::eType
	int32_t ___eType_0;
	// System.Int32 ScienceTree/sLeafConfig::nType
	int32_t ___nType_1;
	// System.Int32[] ScienceTree/sLeafConfig::aryIntParams
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___aryIntParams_2;
	// System.Single[] ScienceTree/sLeafConfig::aryFloatParams
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___aryFloatParams_3;
	// System.String ScienceTree/sLeafConfig::szDesc
	String_t* ___szDesc_4;
	// System.Boolean ScienceTree/sLeafConfig::bSwitch
	bool ___bSwitch_5;

public:
	inline static int32_t get_offset_of_eType_0() { return static_cast<int32_t>(offsetof(sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11, ___eType_0)); }
	inline int32_t get_eType_0() const { return ___eType_0; }
	inline int32_t* get_address_of_eType_0() { return &___eType_0; }
	inline void set_eType_0(int32_t value)
	{
		___eType_0 = value;
	}

	inline static int32_t get_offset_of_nType_1() { return static_cast<int32_t>(offsetof(sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11, ___nType_1)); }
	inline int32_t get_nType_1() const { return ___nType_1; }
	inline int32_t* get_address_of_nType_1() { return &___nType_1; }
	inline void set_nType_1(int32_t value)
	{
		___nType_1 = value;
	}

	inline static int32_t get_offset_of_aryIntParams_2() { return static_cast<int32_t>(offsetof(sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11, ___aryIntParams_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_aryIntParams_2() const { return ___aryIntParams_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_aryIntParams_2() { return &___aryIntParams_2; }
	inline void set_aryIntParams_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___aryIntParams_2 = value;
		Il2CppCodeGenWriteBarrier((&___aryIntParams_2), value);
	}

	inline static int32_t get_offset_of_aryFloatParams_3() { return static_cast<int32_t>(offsetof(sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11, ___aryFloatParams_3)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_aryFloatParams_3() const { return ___aryFloatParams_3; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_aryFloatParams_3() { return &___aryFloatParams_3; }
	inline void set_aryFloatParams_3(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___aryFloatParams_3 = value;
		Il2CppCodeGenWriteBarrier((&___aryFloatParams_3), value);
	}

	inline static int32_t get_offset_of_szDesc_4() { return static_cast<int32_t>(offsetof(sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11, ___szDesc_4)); }
	inline String_t* get_szDesc_4() const { return ___szDesc_4; }
	inline String_t** get_address_of_szDesc_4() { return &___szDesc_4; }
	inline void set_szDesc_4(String_t* value)
	{
		___szDesc_4 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_4), value);
	}

	inline static int32_t get_offset_of_bSwitch_5() { return static_cast<int32_t>(offsetof(sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11, ___bSwitch_5)); }
	inline bool get_bSwitch_5() const { return ___bSwitch_5; }
	inline bool* get_address_of_bSwitch_5() { return &___bSwitch_5; }
	inline void set_bSwitch_5(bool value)
	{
		___bSwitch_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ScienceTree/sLeafConfig
struct sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11_marshaled_pinvoke
{
	int32_t ___eType_0;
	int32_t ___nType_1;
	int32_t* ___aryIntParams_2;
	float* ___aryFloatParams_3;
	char* ___szDesc_4;
	int32_t ___bSwitch_5;
};
// Native definition for COM marshalling of ScienceTree/sLeafConfig
struct sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11_marshaled_com
{
	int32_t ___eType_0;
	int32_t ___nType_1;
	int32_t* ___aryIntParams_2;
	float* ___aryFloatParams_3;
	Il2CppChar* ___szDesc_4;
	int32_t ___bSwitch_5;
};
#endif // SLEAFCONFIG_T101684B4AA2B2843705D9A91099D4EA539E12F11_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef DELEGATESCALEEND_T697D8B0D83495ED3E1853627A6FCACD68409376F_H
#define DELEGATESCALEEND_T697D8B0D83495ED3E1853627A6FCACD68409376F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseScale/delegateScaleEnd
struct  delegateScaleEnd_t697D8B0D83495ED3E1853627A6FCACD68409376F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATESCALEEND_T697D8B0D83495ED3E1853627A6FCACD68409376F_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef AIRLINEMANAGER_TB3B70C62CE6925B6930EB9E670E3AD17BF575F47_H
#define AIRLINEMANAGER_TB3B70C62CE6925B6930EB9E670E3AD17BF575F47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AirlineManager
struct  AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3 AirlineManager::m_vecLeft
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecLeft_6;
	// UnityEngine.Vector3 AirlineManager::m_vecLeftTop
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecLeftTop_7;
	// UnityEngine.Vector3 AirlineManager::m_vecTop
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecTop_8;
	// UnityEngine.Vector3 AirlineManager::m_vecRightTop
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecRightTop_9;
	// UnityEngine.Vector3 AirlineManager::m_vecRight
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecRight_10;
	// UnityEngine.Vector3 AirlineManager::m_vecRightBottom
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecRightBottom_11;
	// UnityEngine.Vector3 AirlineManager::m_vecBottom
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecBottom_12;
	// UnityEngine.Vector3 AirlineManager::m_vecLeftBottom
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecLeftBottom_13;
	// UnityEngine.Vector3 AirlineManager::m_vecCenterLeftTop
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecCenterLeftTop_14;
	// UnityEngine.Vector3 AirlineManager::m_vecCenterRightTop
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecCenterRightTop_15;
	// UnityEngine.Vector3 AirlineManager::m_vecCenterRightBottom
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecCenterRightBottom_16;
	// UnityEngine.Vector3 AirlineManager::m_vecCenterLeftBottom
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecCenterLeftBottom_17;
	// UnityEngine.GameObject[] AirlineManager::m_aryKeyFramePoints
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryKeyFramePoints_18;

public:
	inline static int32_t get_offset_of_m_vecLeft_6() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47, ___m_vecLeft_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecLeft_6() const { return ___m_vecLeft_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecLeft_6() { return &___m_vecLeft_6; }
	inline void set_m_vecLeft_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecLeft_6 = value;
	}

	inline static int32_t get_offset_of_m_vecLeftTop_7() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47, ___m_vecLeftTop_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecLeftTop_7() const { return ___m_vecLeftTop_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecLeftTop_7() { return &___m_vecLeftTop_7; }
	inline void set_m_vecLeftTop_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecLeftTop_7 = value;
	}

	inline static int32_t get_offset_of_m_vecTop_8() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47, ___m_vecTop_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecTop_8() const { return ___m_vecTop_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecTop_8() { return &___m_vecTop_8; }
	inline void set_m_vecTop_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecTop_8 = value;
	}

	inline static int32_t get_offset_of_m_vecRightTop_9() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47, ___m_vecRightTop_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecRightTop_9() const { return ___m_vecRightTop_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecRightTop_9() { return &___m_vecRightTop_9; }
	inline void set_m_vecRightTop_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecRightTop_9 = value;
	}

	inline static int32_t get_offset_of_m_vecRight_10() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47, ___m_vecRight_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecRight_10() const { return ___m_vecRight_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecRight_10() { return &___m_vecRight_10; }
	inline void set_m_vecRight_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecRight_10 = value;
	}

	inline static int32_t get_offset_of_m_vecRightBottom_11() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47, ___m_vecRightBottom_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecRightBottom_11() const { return ___m_vecRightBottom_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecRightBottom_11() { return &___m_vecRightBottom_11; }
	inline void set_m_vecRightBottom_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecRightBottom_11 = value;
	}

	inline static int32_t get_offset_of_m_vecBottom_12() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47, ___m_vecBottom_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecBottom_12() const { return ___m_vecBottom_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecBottom_12() { return &___m_vecBottom_12; }
	inline void set_m_vecBottom_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecBottom_12 = value;
	}

	inline static int32_t get_offset_of_m_vecLeftBottom_13() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47, ___m_vecLeftBottom_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecLeftBottom_13() const { return ___m_vecLeftBottom_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecLeftBottom_13() { return &___m_vecLeftBottom_13; }
	inline void set_m_vecLeftBottom_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecLeftBottom_13 = value;
	}

	inline static int32_t get_offset_of_m_vecCenterLeftTop_14() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47, ___m_vecCenterLeftTop_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecCenterLeftTop_14() const { return ___m_vecCenterLeftTop_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecCenterLeftTop_14() { return &___m_vecCenterLeftTop_14; }
	inline void set_m_vecCenterLeftTop_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecCenterLeftTop_14 = value;
	}

	inline static int32_t get_offset_of_m_vecCenterRightTop_15() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47, ___m_vecCenterRightTop_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecCenterRightTop_15() const { return ___m_vecCenterRightTop_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecCenterRightTop_15() { return &___m_vecCenterRightTop_15; }
	inline void set_m_vecCenterRightTop_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecCenterRightTop_15 = value;
	}

	inline static int32_t get_offset_of_m_vecCenterRightBottom_16() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47, ___m_vecCenterRightBottom_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecCenterRightBottom_16() const { return ___m_vecCenterRightBottom_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecCenterRightBottom_16() { return &___m_vecCenterRightBottom_16; }
	inline void set_m_vecCenterRightBottom_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecCenterRightBottom_16 = value;
	}

	inline static int32_t get_offset_of_m_vecCenterLeftBottom_17() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47, ___m_vecCenterLeftBottom_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecCenterLeftBottom_17() const { return ___m_vecCenterLeftBottom_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecCenterLeftBottom_17() { return &___m_vecCenterLeftBottom_17; }
	inline void set_m_vecCenterLeftBottom_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecCenterLeftBottom_17 = value;
	}

	inline static int32_t get_offset_of_m_aryKeyFramePoints_18() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47, ___m_aryKeyFramePoints_18)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryKeyFramePoints_18() const { return ___m_aryKeyFramePoints_18; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryKeyFramePoints_18() { return &___m_aryKeyFramePoints_18; }
	inline void set_m_aryKeyFramePoints_18(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryKeyFramePoints_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryKeyFramePoints_18), value);
	}
};

struct AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47_StaticFields
{
public:
	// AirlineManager AirlineManager::s_Instance
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47 * ___s_Instance_4;
	// UnityEngine.Vector3 AirlineManager::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_5;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47_StaticFields, ___s_Instance_4)); }
	inline AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47_StaticFields, ___vecTempPos_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AIRLINEMANAGER_TB3B70C62CE6925B6930EB9E670E3AD17BF575F47_H
#ifndef AUDIOMANAGER_T67152D1A926351222F6AD0C0F2442EAE024C7D23_H
#define AUDIOMANAGER_T67152D1A926351222F6AD0C0F2442EAE024C7D23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager
struct  AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioSource[] AudioManager::m_aryBGM
	AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* ___m_aryBGM_5;
	// UnityEngine.AudioSource[] AudioManager::m_arySE
	AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* ___m_arySE_6;
	// UnityEngine.AudioSource[] AudioManager::m_arySE_New
	AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* ___m_arySE_New_7;
	// UnityEngine.AudioSource AudioManager::_audioDefaultClickButton
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ____audioDefaultClickButton_8;
	// UnityEngine.GameObject[] AudioManager::m_aryAuidoPrefabs
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryAuidoPrefabs_9;
	// UnityEngine.AudioSource AudioManager::_BMG
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ____BMG_10;
	// System.Collections.Generic.Dictionary`2<AudioManager/eSE,System.Collections.Generic.List`1<UnityEngine.AudioSource>> AudioManager::m_dicPlayingSE
	Dictionary_2_tCB4384C8D69355F95AE7A997BD0EBA8D389A27B2 * ___m_dicPlayingSE_11;
	// System.Boolean AudioManager::m_bBGM
	bool ___m_bBGM_12;
	// System.Int32 AudioManager::m_nMusicIndex
	int32_t ___m_nMusicIndex_13;

public:
	inline static int32_t get_offset_of_m_aryBGM_5() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_aryBGM_5)); }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* get_m_aryBGM_5() const { return ___m_aryBGM_5; }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF** get_address_of_m_aryBGM_5() { return &___m_aryBGM_5; }
	inline void set_m_aryBGM_5(AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* value)
	{
		___m_aryBGM_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBGM_5), value);
	}

	inline static int32_t get_offset_of_m_arySE_6() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_arySE_6)); }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* get_m_arySE_6() const { return ___m_arySE_6; }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF** get_address_of_m_arySE_6() { return &___m_arySE_6; }
	inline void set_m_arySE_6(AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* value)
	{
		___m_arySE_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySE_6), value);
	}

	inline static int32_t get_offset_of_m_arySE_New_7() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_arySE_New_7)); }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* get_m_arySE_New_7() const { return ___m_arySE_New_7; }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF** get_address_of_m_arySE_New_7() { return &___m_arySE_New_7; }
	inline void set_m_arySE_New_7(AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* value)
	{
		___m_arySE_New_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySE_New_7), value);
	}

	inline static int32_t get_offset_of__audioDefaultClickButton_8() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ____audioDefaultClickButton_8)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get__audioDefaultClickButton_8() const { return ____audioDefaultClickButton_8; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of__audioDefaultClickButton_8() { return &____audioDefaultClickButton_8; }
	inline void set__audioDefaultClickButton_8(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		____audioDefaultClickButton_8 = value;
		Il2CppCodeGenWriteBarrier((&____audioDefaultClickButton_8), value);
	}

	inline static int32_t get_offset_of_m_aryAuidoPrefabs_9() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_aryAuidoPrefabs_9)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryAuidoPrefabs_9() const { return ___m_aryAuidoPrefabs_9; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryAuidoPrefabs_9() { return &___m_aryAuidoPrefabs_9; }
	inline void set_m_aryAuidoPrefabs_9(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryAuidoPrefabs_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAuidoPrefabs_9), value);
	}

	inline static int32_t get_offset_of__BMG_10() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ____BMG_10)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get__BMG_10() const { return ____BMG_10; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of__BMG_10() { return &____BMG_10; }
	inline void set__BMG_10(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		____BMG_10 = value;
		Il2CppCodeGenWriteBarrier((&____BMG_10), value);
	}

	inline static int32_t get_offset_of_m_dicPlayingSE_11() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_dicPlayingSE_11)); }
	inline Dictionary_2_tCB4384C8D69355F95AE7A997BD0EBA8D389A27B2 * get_m_dicPlayingSE_11() const { return ___m_dicPlayingSE_11; }
	inline Dictionary_2_tCB4384C8D69355F95AE7A997BD0EBA8D389A27B2 ** get_address_of_m_dicPlayingSE_11() { return &___m_dicPlayingSE_11; }
	inline void set_m_dicPlayingSE_11(Dictionary_2_tCB4384C8D69355F95AE7A997BD0EBA8D389A27B2 * value)
	{
		___m_dicPlayingSE_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPlayingSE_11), value);
	}

	inline static int32_t get_offset_of_m_bBGM_12() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_bBGM_12)); }
	inline bool get_m_bBGM_12() const { return ___m_bBGM_12; }
	inline bool* get_address_of_m_bBGM_12() { return &___m_bBGM_12; }
	inline void set_m_bBGM_12(bool value)
	{
		___m_bBGM_12 = value;
	}

	inline static int32_t get_offset_of_m_nMusicIndex_13() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_nMusicIndex_13)); }
	inline int32_t get_m_nMusicIndex_13() const { return ___m_nMusicIndex_13; }
	inline int32_t* get_address_of_m_nMusicIndex_13() { return &___m_nMusicIndex_13; }
	inline void set_m_nMusicIndex_13(int32_t value)
	{
		___m_nMusicIndex_13 = value;
	}
};

struct AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields
{
public:
	// AudioManager AudioManager::s_Instance
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields, ___s_Instance_4)); }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMANAGER_T67152D1A926351222F6AD0C0F2442EAE024C7D23_H
#ifndef BASEROTATE_T66CD927C20FD5B40C08A788B6578D220A0B11377_H
#define BASEROTATE_T66CD927C20FD5B40C08A788B6578D220A0B11377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseRotate
struct  BaseRotate_t66CD927C20FD5B40C08A788B6578D220A0B11377  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean BaseRotate::m_bRotating
	bool ___m_bRotating_4;
	// System.Single BaseRotate::m_fRotateSpeed
	float ___m_fRotateSpeed_5;
	// System.Single BaseRotate::m_fCurAngle
	float ___m_fCurAngle_6;

public:
	inline static int32_t get_offset_of_m_bRotating_4() { return static_cast<int32_t>(offsetof(BaseRotate_t66CD927C20FD5B40C08A788B6578D220A0B11377, ___m_bRotating_4)); }
	inline bool get_m_bRotating_4() const { return ___m_bRotating_4; }
	inline bool* get_address_of_m_bRotating_4() { return &___m_bRotating_4; }
	inline void set_m_bRotating_4(bool value)
	{
		___m_bRotating_4 = value;
	}

	inline static int32_t get_offset_of_m_fRotateSpeed_5() { return static_cast<int32_t>(offsetof(BaseRotate_t66CD927C20FD5B40C08A788B6578D220A0B11377, ___m_fRotateSpeed_5)); }
	inline float get_m_fRotateSpeed_5() const { return ___m_fRotateSpeed_5; }
	inline float* get_address_of_m_fRotateSpeed_5() { return &___m_fRotateSpeed_5; }
	inline void set_m_fRotateSpeed_5(float value)
	{
		___m_fRotateSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_fCurAngle_6() { return static_cast<int32_t>(offsetof(BaseRotate_t66CD927C20FD5B40C08A788B6578D220A0B11377, ___m_fCurAngle_6)); }
	inline float get_m_fCurAngle_6() const { return ___m_fCurAngle_6; }
	inline float* get_address_of_m_fCurAngle_6() { return &___m_fCurAngle_6; }
	inline void set_m_fCurAngle_6(float value)
	{
		___m_fCurAngle_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEROTATE_T66CD927C20FD5B40C08A788B6578D220A0B11377_H
#ifndef BASESCALE_TA9DBA1A65C015253341E09F34B301CB704B44063_H
#define BASESCALE_TA9DBA1A65C015253341E09F34B301CB704B44063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseScale
struct  BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// BaseScale/delegateScaleEnd BaseScale::eventScaleEnd
	delegateScaleEnd_t697D8B0D83495ED3E1853627A6FCACD68409376F * ___eventScaleEnd_4;
	// UnityEngine.Vector3 BaseScale::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_5;
	// System.Boolean BaseScale::m_bAutoaPlay
	bool ___m_bAutoaPlay_6;
	// System.Boolean BaseScale::m_bLoop
	bool ___m_bLoop_7;
	// System.Boolean BaseScale::_bHideWhenEnd
	bool ____bHideWhenEnd_8;
	// System.Single BaseScale::m_fScaleTime
	float ___m_fScaleTime_9;
	// System.Single BaseScale::m_fWaitTime
	float ___m_fWaitTime_10;
	// System.Single BaseScale::m_fMaxScale
	float ___m_fMaxScale_11;
	// System.Int32 BaseScale::m_nStatus
	int32_t ___m_nStatus_12;
	// System.Single BaseScale::m_fSpeed
	float ___m_fSpeed_13;
	// System.Single BaseScale::m_fRotateSpeed
	float ___m_fRotateSpeed_14;
	// System.Single BaseScale::m_fWaitTimeElapse
	float ___m_fWaitTimeElapse_15;
	// System.Single BaseScale::m_fInitScale
	float ___m_fInitScale_16;
	// System.Single BaseScale::m_fDestScale
	float ___m_fDestScale_17;
	// System.Boolean BaseScale::_bWithRotate
	bool ____bWithRotate_18;
	// System.Single BaseScale::_fMaxRotateAngle
	float ____fMaxRotateAngle_19;
	// System.Single BaseScale::m_fAngle
	float ___m_fAngle_20;
	// UnityEngine.GameObject BaseScale::_goShadow
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goShadow_21;
	// System.Boolean BaseScale::m_bShadowBegin
	bool ___m_bShadowBegin_22;
	// BaseScale/eScaleAxis BaseScale::m_eAxis
	int32_t ___m_eAxis_23;

public:
	inline static int32_t get_offset_of_eventScaleEnd_4() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___eventScaleEnd_4)); }
	inline delegateScaleEnd_t697D8B0D83495ED3E1853627A6FCACD68409376F * get_eventScaleEnd_4() const { return ___eventScaleEnd_4; }
	inline delegateScaleEnd_t697D8B0D83495ED3E1853627A6FCACD68409376F ** get_address_of_eventScaleEnd_4() { return &___eventScaleEnd_4; }
	inline void set_eventScaleEnd_4(delegateScaleEnd_t697D8B0D83495ED3E1853627A6FCACD68409376F * value)
	{
		___eventScaleEnd_4 = value;
		Il2CppCodeGenWriteBarrier((&___eventScaleEnd_4), value);
	}

	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___vecTempScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_5 = value;
	}

	inline static int32_t get_offset_of_m_bAutoaPlay_6() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_bAutoaPlay_6)); }
	inline bool get_m_bAutoaPlay_6() const { return ___m_bAutoaPlay_6; }
	inline bool* get_address_of_m_bAutoaPlay_6() { return &___m_bAutoaPlay_6; }
	inline void set_m_bAutoaPlay_6(bool value)
	{
		___m_bAutoaPlay_6 = value;
	}

	inline static int32_t get_offset_of_m_bLoop_7() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_bLoop_7)); }
	inline bool get_m_bLoop_7() const { return ___m_bLoop_7; }
	inline bool* get_address_of_m_bLoop_7() { return &___m_bLoop_7; }
	inline void set_m_bLoop_7(bool value)
	{
		___m_bLoop_7 = value;
	}

	inline static int32_t get_offset_of__bHideWhenEnd_8() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ____bHideWhenEnd_8)); }
	inline bool get__bHideWhenEnd_8() const { return ____bHideWhenEnd_8; }
	inline bool* get_address_of__bHideWhenEnd_8() { return &____bHideWhenEnd_8; }
	inline void set__bHideWhenEnd_8(bool value)
	{
		____bHideWhenEnd_8 = value;
	}

	inline static int32_t get_offset_of_m_fScaleTime_9() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fScaleTime_9)); }
	inline float get_m_fScaleTime_9() const { return ___m_fScaleTime_9; }
	inline float* get_address_of_m_fScaleTime_9() { return &___m_fScaleTime_9; }
	inline void set_m_fScaleTime_9(float value)
	{
		___m_fScaleTime_9 = value;
	}

	inline static int32_t get_offset_of_m_fWaitTime_10() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fWaitTime_10)); }
	inline float get_m_fWaitTime_10() const { return ___m_fWaitTime_10; }
	inline float* get_address_of_m_fWaitTime_10() { return &___m_fWaitTime_10; }
	inline void set_m_fWaitTime_10(float value)
	{
		___m_fWaitTime_10 = value;
	}

	inline static int32_t get_offset_of_m_fMaxScale_11() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fMaxScale_11)); }
	inline float get_m_fMaxScale_11() const { return ___m_fMaxScale_11; }
	inline float* get_address_of_m_fMaxScale_11() { return &___m_fMaxScale_11; }
	inline void set_m_fMaxScale_11(float value)
	{
		___m_fMaxScale_11 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_12() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_nStatus_12)); }
	inline int32_t get_m_nStatus_12() const { return ___m_nStatus_12; }
	inline int32_t* get_address_of_m_nStatus_12() { return &___m_nStatus_12; }
	inline void set_m_nStatus_12(int32_t value)
	{
		___m_nStatus_12 = value;
	}

	inline static int32_t get_offset_of_m_fSpeed_13() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fSpeed_13)); }
	inline float get_m_fSpeed_13() const { return ___m_fSpeed_13; }
	inline float* get_address_of_m_fSpeed_13() { return &___m_fSpeed_13; }
	inline void set_m_fSpeed_13(float value)
	{
		___m_fSpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_fRotateSpeed_14() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fRotateSpeed_14)); }
	inline float get_m_fRotateSpeed_14() const { return ___m_fRotateSpeed_14; }
	inline float* get_address_of_m_fRotateSpeed_14() { return &___m_fRotateSpeed_14; }
	inline void set_m_fRotateSpeed_14(float value)
	{
		___m_fRotateSpeed_14 = value;
	}

	inline static int32_t get_offset_of_m_fWaitTimeElapse_15() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fWaitTimeElapse_15)); }
	inline float get_m_fWaitTimeElapse_15() const { return ___m_fWaitTimeElapse_15; }
	inline float* get_address_of_m_fWaitTimeElapse_15() { return &___m_fWaitTimeElapse_15; }
	inline void set_m_fWaitTimeElapse_15(float value)
	{
		___m_fWaitTimeElapse_15 = value;
	}

	inline static int32_t get_offset_of_m_fInitScale_16() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fInitScale_16)); }
	inline float get_m_fInitScale_16() const { return ___m_fInitScale_16; }
	inline float* get_address_of_m_fInitScale_16() { return &___m_fInitScale_16; }
	inline void set_m_fInitScale_16(float value)
	{
		___m_fInitScale_16 = value;
	}

	inline static int32_t get_offset_of_m_fDestScale_17() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fDestScale_17)); }
	inline float get_m_fDestScale_17() const { return ___m_fDestScale_17; }
	inline float* get_address_of_m_fDestScale_17() { return &___m_fDestScale_17; }
	inline void set_m_fDestScale_17(float value)
	{
		___m_fDestScale_17 = value;
	}

	inline static int32_t get_offset_of__bWithRotate_18() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ____bWithRotate_18)); }
	inline bool get__bWithRotate_18() const { return ____bWithRotate_18; }
	inline bool* get_address_of__bWithRotate_18() { return &____bWithRotate_18; }
	inline void set__bWithRotate_18(bool value)
	{
		____bWithRotate_18 = value;
	}

	inline static int32_t get_offset_of__fMaxRotateAngle_19() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ____fMaxRotateAngle_19)); }
	inline float get__fMaxRotateAngle_19() const { return ____fMaxRotateAngle_19; }
	inline float* get_address_of__fMaxRotateAngle_19() { return &____fMaxRotateAngle_19; }
	inline void set__fMaxRotateAngle_19(float value)
	{
		____fMaxRotateAngle_19 = value;
	}

	inline static int32_t get_offset_of_m_fAngle_20() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fAngle_20)); }
	inline float get_m_fAngle_20() const { return ___m_fAngle_20; }
	inline float* get_address_of_m_fAngle_20() { return &___m_fAngle_20; }
	inline void set_m_fAngle_20(float value)
	{
		___m_fAngle_20 = value;
	}

	inline static int32_t get_offset_of__goShadow_21() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ____goShadow_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goShadow_21() const { return ____goShadow_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goShadow_21() { return &____goShadow_21; }
	inline void set__goShadow_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goShadow_21 = value;
		Il2CppCodeGenWriteBarrier((&____goShadow_21), value);
	}

	inline static int32_t get_offset_of_m_bShadowBegin_22() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_bShadowBegin_22)); }
	inline bool get_m_bShadowBegin_22() const { return ___m_bShadowBegin_22; }
	inline bool* get_address_of_m_bShadowBegin_22() { return &___m_bShadowBegin_22; }
	inline void set_m_bShadowBegin_22(bool value)
	{
		___m_bShadowBegin_22 = value;
	}

	inline static int32_t get_offset_of_m_eAxis_23() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_eAxis_23)); }
	inline int32_t get_m_eAxis_23() const { return ___m_eAxis_23; }
	inline int32_t* get_address_of_m_eAxis_23() { return &___m_eAxis_23; }
	inline void set_m_eAxis_23(int32_t value)
	{
		___m_eAxis_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESCALE_TA9DBA1A65C015253341E09F34B301CB704B44063_H
#ifndef BASESHAKE_TC67FA3B616E91A9019CAF1C6FD15EFEFE863930A_H
#define BASESHAKE_TC67FA3B616E91A9019CAF1C6FD15EFEFE863930A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseShake
struct  BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 BaseShake::m_nTotalTimes
	int32_t ___m_nTotalTimes_4;
	// System.Single BaseShake::m_fShakeInterval
	float ___m_fShakeInterval_5;
	// System.Single BaseShake::m_fShakeAngle
	float ___m_fShakeAngle_6;
	// System.Int32 BaseShake::m_nStatus
	int32_t ___m_nStatus_7;
	// System.Int32 BaseShake::m_nCount
	int32_t ___m_nCount_8;
	// System.Single BaseShake::m_fTimeElapse
	float ___m_fTimeElapse_9;
	// System.Boolean BaseShake::m_bAutoPlay
	bool ___m_bAutoPlay_10;
	// System.Boolean BaseShake::m_bLoop
	bool ___m_bLoop_11;
	// System.Single BaseShake::m_fLoopWaitTime
	float ___m_fLoopWaitTime_12;

public:
	inline static int32_t get_offset_of_m_nTotalTimes_4() { return static_cast<int32_t>(offsetof(BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A, ___m_nTotalTimes_4)); }
	inline int32_t get_m_nTotalTimes_4() const { return ___m_nTotalTimes_4; }
	inline int32_t* get_address_of_m_nTotalTimes_4() { return &___m_nTotalTimes_4; }
	inline void set_m_nTotalTimes_4(int32_t value)
	{
		___m_nTotalTimes_4 = value;
	}

	inline static int32_t get_offset_of_m_fShakeInterval_5() { return static_cast<int32_t>(offsetof(BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A, ___m_fShakeInterval_5)); }
	inline float get_m_fShakeInterval_5() const { return ___m_fShakeInterval_5; }
	inline float* get_address_of_m_fShakeInterval_5() { return &___m_fShakeInterval_5; }
	inline void set_m_fShakeInterval_5(float value)
	{
		___m_fShakeInterval_5 = value;
	}

	inline static int32_t get_offset_of_m_fShakeAngle_6() { return static_cast<int32_t>(offsetof(BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A, ___m_fShakeAngle_6)); }
	inline float get_m_fShakeAngle_6() const { return ___m_fShakeAngle_6; }
	inline float* get_address_of_m_fShakeAngle_6() { return &___m_fShakeAngle_6; }
	inline void set_m_fShakeAngle_6(float value)
	{
		___m_fShakeAngle_6 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_7() { return static_cast<int32_t>(offsetof(BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A, ___m_nStatus_7)); }
	inline int32_t get_m_nStatus_7() const { return ___m_nStatus_7; }
	inline int32_t* get_address_of_m_nStatus_7() { return &___m_nStatus_7; }
	inline void set_m_nStatus_7(int32_t value)
	{
		___m_nStatus_7 = value;
	}

	inline static int32_t get_offset_of_m_nCount_8() { return static_cast<int32_t>(offsetof(BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A, ___m_nCount_8)); }
	inline int32_t get_m_nCount_8() const { return ___m_nCount_8; }
	inline int32_t* get_address_of_m_nCount_8() { return &___m_nCount_8; }
	inline void set_m_nCount_8(int32_t value)
	{
		___m_nCount_8 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_9() { return static_cast<int32_t>(offsetof(BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A, ___m_fTimeElapse_9)); }
	inline float get_m_fTimeElapse_9() const { return ___m_fTimeElapse_9; }
	inline float* get_address_of_m_fTimeElapse_9() { return &___m_fTimeElapse_9; }
	inline void set_m_fTimeElapse_9(float value)
	{
		___m_fTimeElapse_9 = value;
	}

	inline static int32_t get_offset_of_m_bAutoPlay_10() { return static_cast<int32_t>(offsetof(BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A, ___m_bAutoPlay_10)); }
	inline bool get_m_bAutoPlay_10() const { return ___m_bAutoPlay_10; }
	inline bool* get_address_of_m_bAutoPlay_10() { return &___m_bAutoPlay_10; }
	inline void set_m_bAutoPlay_10(bool value)
	{
		___m_bAutoPlay_10 = value;
	}

	inline static int32_t get_offset_of_m_bLoop_11() { return static_cast<int32_t>(offsetof(BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A, ___m_bLoop_11)); }
	inline bool get_m_bLoop_11() const { return ___m_bLoop_11; }
	inline bool* get_address_of_m_bLoop_11() { return &___m_bLoop_11; }
	inline void set_m_bLoop_11(bool value)
	{
		___m_bLoop_11 = value;
	}

	inline static int32_t get_offset_of_m_fLoopWaitTime_12() { return static_cast<int32_t>(offsetof(BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A, ___m_fLoopWaitTime_12)); }
	inline float get_m_fLoopWaitTime_12() const { return ___m_fLoopWaitTime_12; }
	inline float* get_address_of_m_fLoopWaitTime_12() { return &___m_fLoopWaitTime_12; }
	inline void set_m_fLoopWaitTime_12(float value)
	{
		___m_fLoopWaitTime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESHAKE_TC67FA3B616E91A9019CAF1C6FD15EFEFE863930A_H
#ifndef BOXMANAGER_T36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE_H
#define BOXMANAGER_T36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoxManager
struct  BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Sprite[] BoxManager::m_aryBoxSpr
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryBoxSpr_5;
	// System.Single BoxManager::m_fGenerateBoxInterval
	float ___m_fGenerateBoxInterval_7;
	// System.Single BoxManager::m_fTimeElapse
	float ___m_fTimeElapse_8;
	// System.Single BoxManager::m_fDropStartPosY
	float ___m_fDropStartPosY_9;
	// System.Single BoxManager::m_fDropSpeed
	float ___m_fDropSpeed_10;
	// System.Single BoxManager::m_fDropTime
	float ___m_fDropTime_11;

public:
	inline static int32_t get_offset_of_m_aryBoxSpr_5() { return static_cast<int32_t>(offsetof(BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE, ___m_aryBoxSpr_5)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryBoxSpr_5() const { return ___m_aryBoxSpr_5; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryBoxSpr_5() { return &___m_aryBoxSpr_5; }
	inline void set_m_aryBoxSpr_5(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryBoxSpr_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBoxSpr_5), value);
	}

	inline static int32_t get_offset_of_m_fGenerateBoxInterval_7() { return static_cast<int32_t>(offsetof(BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE, ___m_fGenerateBoxInterval_7)); }
	inline float get_m_fGenerateBoxInterval_7() const { return ___m_fGenerateBoxInterval_7; }
	inline float* get_address_of_m_fGenerateBoxInterval_7() { return &___m_fGenerateBoxInterval_7; }
	inline void set_m_fGenerateBoxInterval_7(float value)
	{
		___m_fGenerateBoxInterval_7 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_8() { return static_cast<int32_t>(offsetof(BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE, ___m_fTimeElapse_8)); }
	inline float get_m_fTimeElapse_8() const { return ___m_fTimeElapse_8; }
	inline float* get_address_of_m_fTimeElapse_8() { return &___m_fTimeElapse_8; }
	inline void set_m_fTimeElapse_8(float value)
	{
		___m_fTimeElapse_8 = value;
	}

	inline static int32_t get_offset_of_m_fDropStartPosY_9() { return static_cast<int32_t>(offsetof(BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE, ___m_fDropStartPosY_9)); }
	inline float get_m_fDropStartPosY_9() const { return ___m_fDropStartPosY_9; }
	inline float* get_address_of_m_fDropStartPosY_9() { return &___m_fDropStartPosY_9; }
	inline void set_m_fDropStartPosY_9(float value)
	{
		___m_fDropStartPosY_9 = value;
	}

	inline static int32_t get_offset_of_m_fDropSpeed_10() { return static_cast<int32_t>(offsetof(BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE, ___m_fDropSpeed_10)); }
	inline float get_m_fDropSpeed_10() const { return ___m_fDropSpeed_10; }
	inline float* get_address_of_m_fDropSpeed_10() { return &___m_fDropSpeed_10; }
	inline void set_m_fDropSpeed_10(float value)
	{
		___m_fDropSpeed_10 = value;
	}

	inline static int32_t get_offset_of_m_fDropTime_11() { return static_cast<int32_t>(offsetof(BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE, ___m_fDropTime_11)); }
	inline float get_m_fDropTime_11() const { return ___m_fDropTime_11; }
	inline float* get_address_of_m_fDropTime_11() { return &___m_fDropTime_11; }
	inline void set_m_fDropTime_11(float value)
	{
		___m_fDropTime_11 = value;
	}
};

struct BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE_StaticFields
{
public:
	// BoxManager BoxManager::s_Instance
	BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE * ___s_Instance_4;
	// UnityEngine.Vector3 BoxManager::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_6;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE_StaticFields, ___s_Instance_4)); }
	inline BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE * get_s_Instance_4() const { return ___s_Instance_4; }
	inline BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_vecTempPos_6() { return static_cast<int32_t>(offsetof(BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE_StaticFields, ___vecTempPos_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_6() const { return ___vecTempPos_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_6() { return &___vecTempPos_6; }
	inline void set_vecTempPos_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXMANAGER_T36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE_H
#ifndef BUFFMANAGER_T085DFCE00CD02411A9D36236C7A685E150395C5B_H
#define BUFFMANAGER_T085DFCE00CD02411A9D36236C7A685E150395C5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuffManager
struct  BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UIBuffCounter[] BuffManager::m_aryBuffCounter
	UIBuffCounterU5BU5D_t8C5FC9F38732088DCC192DD53CEF2E923FDA4645* ___m_aryBuffCounter_6;
	// System.Single BuffManager::m_fGap
	float ___m_fGap_7;
	// System.Single BuffManager::m_fTimeElapse
	float ___m_fTimeElapse_8;

public:
	inline static int32_t get_offset_of_m_aryBuffCounter_6() { return static_cast<int32_t>(offsetof(BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B, ___m_aryBuffCounter_6)); }
	inline UIBuffCounterU5BU5D_t8C5FC9F38732088DCC192DD53CEF2E923FDA4645* get_m_aryBuffCounter_6() const { return ___m_aryBuffCounter_6; }
	inline UIBuffCounterU5BU5D_t8C5FC9F38732088DCC192DD53CEF2E923FDA4645** get_address_of_m_aryBuffCounter_6() { return &___m_aryBuffCounter_6; }
	inline void set_m_aryBuffCounter_6(UIBuffCounterU5BU5D_t8C5FC9F38732088DCC192DD53CEF2E923FDA4645* value)
	{
		___m_aryBuffCounter_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBuffCounter_6), value);
	}

	inline static int32_t get_offset_of_m_fGap_7() { return static_cast<int32_t>(offsetof(BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B, ___m_fGap_7)); }
	inline float get_m_fGap_7() const { return ___m_fGap_7; }
	inline float* get_address_of_m_fGap_7() { return &___m_fGap_7; }
	inline void set_m_fGap_7(float value)
	{
		___m_fGap_7 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_8() { return static_cast<int32_t>(offsetof(BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B, ___m_fTimeElapse_8)); }
	inline float get_m_fTimeElapse_8() const { return ___m_fTimeElapse_8; }
	inline float* get_address_of_m_fTimeElapse_8() { return &___m_fTimeElapse_8; }
	inline void set_m_fTimeElapse_8(float value)
	{
		___m_fTimeElapse_8 = value;
	}
};

struct BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B_StaticFields
{
public:
	// BuffManager BuffManager::s_Instance
	BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B * ___s_Instance_4;
	// UnityEngine.Vector3 BuffManager::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_5;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B_StaticFields, ___s_Instance_4)); }
	inline BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B * get_s_Instance_4() const { return ___s_Instance_4; }
	inline BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B_StaticFields, ___vecTempPos_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFMANAGER_T085DFCE00CD02411A9D36236C7A685E150395C5B_H
#ifndef CFRAMEANIMATIONEFFECT_TB09C367E8E6954F89BDD8004C69990C722AC2E10_H
#define CFRAMEANIMATIONEFFECT_TB09C367E8E6954F89BDD8004C69990C722AC2E10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFrameAnimationEffect
struct  CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Sprite[] CFrameAnimationEffect::m_aryFrameSprites
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryFrameSprites_4;
	// UnityEngine.UI.Image CFrameAnimationEffect::_imgMain
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgMain_5;
	// UnityEngine.SpriteRenderer CFrameAnimationEffect::_sprMain
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____sprMain_6;
	// System.Boolean CFrameAnimationEffect::m_bUI
	bool ___m_bUI_7;
	// System.Single CFrameAnimationEffect::m_fFrameInterval
	float ___m_fFrameInterval_8;
	// System.Single CFrameAnimationEffect::m_fTimeCount
	float ___m_fTimeCount_9;
	// System.Boolean CFrameAnimationEffect::m_bPlaying
	bool ___m_bPlaying_10;
	// System.Int32 CFrameAnimationEffect::m_nFrameIndex
	int32_t ___m_nFrameIndex_11;
	// System.Boolean CFrameAnimationEffect::m_bLoop
	bool ___m_bLoop_12;
	// System.Boolean CFrameAnimationEffect::m_bReverse
	bool ___m_bReverse_13;
	// System.Boolean CFrameAnimationEffect::m_bCurDir
	bool ___m_bCurDir_14;
	// System.Single CFrameAnimationEffect::m_fLoopInterval
	float ___m_fLoopInterval_15;
	// System.Boolean CFrameAnimationEffect::m_bWaiting
	bool ___m_bWaiting_16;
	// System.Single CFrameAnimationEffect::m_fWaitingTimeCount
	float ___m_fWaitingTimeCount_17;
	// System.Boolean CFrameAnimationEffect::m_bPlayAuto
	bool ___m_bPlayAuto_18;
	// System.Int32 CFrameAnimationEffect::m_nLoopTimes
	int32_t ___m_nLoopTimes_19;
	// System.Boolean CFrameAnimationEffect::m_bHideWhenEnd
	bool ___m_bHideWhenEnd_20;
	// System.Boolean CFrameAnimationEffect::m_bDestroyWhenEnd
	bool ___m_bDestroyWhenEnd_21;
	// System.Int32 CFrameAnimationEffect::m_nId
	int32_t ___m_nId_22;

public:
	inline static int32_t get_offset_of_m_aryFrameSprites_4() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_aryFrameSprites_4)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryFrameSprites_4() const { return ___m_aryFrameSprites_4; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryFrameSprites_4() { return &___m_aryFrameSprites_4; }
	inline void set_m_aryFrameSprites_4(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryFrameSprites_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryFrameSprites_4), value);
	}

	inline static int32_t get_offset_of__imgMain_5() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ____imgMain_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgMain_5() const { return ____imgMain_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgMain_5() { return &____imgMain_5; }
	inline void set__imgMain_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgMain_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_5), value);
	}

	inline static int32_t get_offset_of__sprMain_6() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ____sprMain_6)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__sprMain_6() const { return ____sprMain_6; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__sprMain_6() { return &____sprMain_6; }
	inline void set__sprMain_6(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____sprMain_6 = value;
		Il2CppCodeGenWriteBarrier((&____sprMain_6), value);
	}

	inline static int32_t get_offset_of_m_bUI_7() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bUI_7)); }
	inline bool get_m_bUI_7() const { return ___m_bUI_7; }
	inline bool* get_address_of_m_bUI_7() { return &___m_bUI_7; }
	inline void set_m_bUI_7(bool value)
	{
		___m_bUI_7 = value;
	}

	inline static int32_t get_offset_of_m_fFrameInterval_8() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_fFrameInterval_8)); }
	inline float get_m_fFrameInterval_8() const { return ___m_fFrameInterval_8; }
	inline float* get_address_of_m_fFrameInterval_8() { return &___m_fFrameInterval_8; }
	inline void set_m_fFrameInterval_8(float value)
	{
		___m_fFrameInterval_8 = value;
	}

	inline static int32_t get_offset_of_m_fTimeCount_9() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_fTimeCount_9)); }
	inline float get_m_fTimeCount_9() const { return ___m_fTimeCount_9; }
	inline float* get_address_of_m_fTimeCount_9() { return &___m_fTimeCount_9; }
	inline void set_m_fTimeCount_9(float value)
	{
		___m_fTimeCount_9 = value;
	}

	inline static int32_t get_offset_of_m_bPlaying_10() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bPlaying_10)); }
	inline bool get_m_bPlaying_10() const { return ___m_bPlaying_10; }
	inline bool* get_address_of_m_bPlaying_10() { return &___m_bPlaying_10; }
	inline void set_m_bPlaying_10(bool value)
	{
		___m_bPlaying_10 = value;
	}

	inline static int32_t get_offset_of_m_nFrameIndex_11() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_nFrameIndex_11)); }
	inline int32_t get_m_nFrameIndex_11() const { return ___m_nFrameIndex_11; }
	inline int32_t* get_address_of_m_nFrameIndex_11() { return &___m_nFrameIndex_11; }
	inline void set_m_nFrameIndex_11(int32_t value)
	{
		___m_nFrameIndex_11 = value;
	}

	inline static int32_t get_offset_of_m_bLoop_12() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bLoop_12)); }
	inline bool get_m_bLoop_12() const { return ___m_bLoop_12; }
	inline bool* get_address_of_m_bLoop_12() { return &___m_bLoop_12; }
	inline void set_m_bLoop_12(bool value)
	{
		___m_bLoop_12 = value;
	}

	inline static int32_t get_offset_of_m_bReverse_13() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bReverse_13)); }
	inline bool get_m_bReverse_13() const { return ___m_bReverse_13; }
	inline bool* get_address_of_m_bReverse_13() { return &___m_bReverse_13; }
	inline void set_m_bReverse_13(bool value)
	{
		___m_bReverse_13 = value;
	}

	inline static int32_t get_offset_of_m_bCurDir_14() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bCurDir_14)); }
	inline bool get_m_bCurDir_14() const { return ___m_bCurDir_14; }
	inline bool* get_address_of_m_bCurDir_14() { return &___m_bCurDir_14; }
	inline void set_m_bCurDir_14(bool value)
	{
		___m_bCurDir_14 = value;
	}

	inline static int32_t get_offset_of_m_fLoopInterval_15() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_fLoopInterval_15)); }
	inline float get_m_fLoopInterval_15() const { return ___m_fLoopInterval_15; }
	inline float* get_address_of_m_fLoopInterval_15() { return &___m_fLoopInterval_15; }
	inline void set_m_fLoopInterval_15(float value)
	{
		___m_fLoopInterval_15 = value;
	}

	inline static int32_t get_offset_of_m_bWaiting_16() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bWaiting_16)); }
	inline bool get_m_bWaiting_16() const { return ___m_bWaiting_16; }
	inline bool* get_address_of_m_bWaiting_16() { return &___m_bWaiting_16; }
	inline void set_m_bWaiting_16(bool value)
	{
		___m_bWaiting_16 = value;
	}

	inline static int32_t get_offset_of_m_fWaitingTimeCount_17() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_fWaitingTimeCount_17)); }
	inline float get_m_fWaitingTimeCount_17() const { return ___m_fWaitingTimeCount_17; }
	inline float* get_address_of_m_fWaitingTimeCount_17() { return &___m_fWaitingTimeCount_17; }
	inline void set_m_fWaitingTimeCount_17(float value)
	{
		___m_fWaitingTimeCount_17 = value;
	}

	inline static int32_t get_offset_of_m_bPlayAuto_18() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bPlayAuto_18)); }
	inline bool get_m_bPlayAuto_18() const { return ___m_bPlayAuto_18; }
	inline bool* get_address_of_m_bPlayAuto_18() { return &___m_bPlayAuto_18; }
	inline void set_m_bPlayAuto_18(bool value)
	{
		___m_bPlayAuto_18 = value;
	}

	inline static int32_t get_offset_of_m_nLoopTimes_19() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_nLoopTimes_19)); }
	inline int32_t get_m_nLoopTimes_19() const { return ___m_nLoopTimes_19; }
	inline int32_t* get_address_of_m_nLoopTimes_19() { return &___m_nLoopTimes_19; }
	inline void set_m_nLoopTimes_19(int32_t value)
	{
		___m_nLoopTimes_19 = value;
	}

	inline static int32_t get_offset_of_m_bHideWhenEnd_20() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bHideWhenEnd_20)); }
	inline bool get_m_bHideWhenEnd_20() const { return ___m_bHideWhenEnd_20; }
	inline bool* get_address_of_m_bHideWhenEnd_20() { return &___m_bHideWhenEnd_20; }
	inline void set_m_bHideWhenEnd_20(bool value)
	{
		___m_bHideWhenEnd_20 = value;
	}

	inline static int32_t get_offset_of_m_bDestroyWhenEnd_21() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bDestroyWhenEnd_21)); }
	inline bool get_m_bDestroyWhenEnd_21() const { return ___m_bDestroyWhenEnd_21; }
	inline bool* get_address_of_m_bDestroyWhenEnd_21() { return &___m_bDestroyWhenEnd_21; }
	inline void set_m_bDestroyWhenEnd_21(bool value)
	{
		___m_bDestroyWhenEnd_21 = value;
	}

	inline static int32_t get_offset_of_m_nId_22() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_nId_22)); }
	inline int32_t get_m_nId_22() const { return ___m_nId_22; }
	inline int32_t* get_address_of_m_nId_22() { return &___m_nId_22; }
	inline void set_m_nId_22(int32_t value)
	{
		___m_nId_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFRAMEANIMATIONEFFECT_TB09C367E8E6954F89BDD8004C69990C722AC2E10_H
#ifndef CHEAT_T61A56D9B4C9BF342A86C628A46DB0F0BB704E166_H
#define CHEAT_T61A56D9B4C9BF342A86C628A46DB0F0BB704E166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cheat
struct  Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Toggle Cheat::_toggleShowDebugInfo
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ____toggleShowDebugInfo_5;
	// UnityEngine.UI.InputField Cheat::_txtUIAnchorIndex
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____txtUIAnchorIndex_6;
	// UnityEngine.UI.InputField Cheat::_txtSceneAnchorIndex
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____txtSceneAnchorIndex_7;
	// UnityEngine.GameObject[] Cheat::m_aryUiAnchor
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryUiAnchor_8;
	// UnityEngine.GameObject[] Cheat::m_arySceneAnchor
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_arySceneAnchor_9;
	// UnityEngine.UI.Text Cheat::_txtDebugInfo
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDebugInfo_10;
	// UnityEngine.GameObject Cheat::_sprAirline
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____sprAirline_11;
	// UnityEngine.UI.Dropdown Cheat::_dropPlanetId
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ____dropPlanetId_12;
	// UnityEngine.UI.Dropdown Cheat::_dropDistrictId
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ____dropDistrictId_13;
	// UnityEngine.UI.InputField Cheat::_inputCoin0
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____inputCoin0_14;
	// UnityEngine.UI.InputField Cheat::_inputCoin1
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____inputCoin1_15;
	// UnityEngine.UI.InputField Cheat::_inputCoin2
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____inputCoin2_16;
	// UnityEngine.UI.InputField Cheat::_inputGreenCash
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____inputGreenCash_17;
	// UnityEngine.UI.InputField Cheat::_inputSkillPoint0
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____inputSkillPoint0_18;
	// UnityEngine.UI.InputField Cheat::_inputSkillPoint1
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____inputSkillPoint1_19;
	// UnityEngine.UI.InputField Cheat::_inputSkillPoint2
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____inputSkillPoint2_20;
	// UnityEngine.UI.Slider Cheat::_sliderVibrateAmount
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ____sliderVibrateAmount_21;
	// UnityEngine.UI.Text Cheat::_txtVibrateAmount
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtVibrateAmount_22;
	// UnityEngine.GameObject Cheat::uiCheatPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___uiCheatPanel_23;
	// UnityEngine.GameObject Cheat::m_goCurShit
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goCurShit_24;
	// System.Single Cheat::m_fShitSpeed
	float ___m_fShitSpeed_26;
	// System.Int64 Cheat::m_lVibrateAmount
	int64_t ___m_lVibrateAmount_27;
	// System.Boolean Cheat::m_bShowDebugInfo
	bool ___m_bShowDebugInfo_28;

public:
	inline static int32_t get_offset_of__toggleShowDebugInfo_5() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____toggleShowDebugInfo_5)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get__toggleShowDebugInfo_5() const { return ____toggleShowDebugInfo_5; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of__toggleShowDebugInfo_5() { return &____toggleShowDebugInfo_5; }
	inline void set__toggleShowDebugInfo_5(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		____toggleShowDebugInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&____toggleShowDebugInfo_5), value);
	}

	inline static int32_t get_offset_of__txtUIAnchorIndex_6() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____txtUIAnchorIndex_6)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__txtUIAnchorIndex_6() const { return ____txtUIAnchorIndex_6; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__txtUIAnchorIndex_6() { return &____txtUIAnchorIndex_6; }
	inline void set__txtUIAnchorIndex_6(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____txtUIAnchorIndex_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtUIAnchorIndex_6), value);
	}

	inline static int32_t get_offset_of__txtSceneAnchorIndex_7() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____txtSceneAnchorIndex_7)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__txtSceneAnchorIndex_7() const { return ____txtSceneAnchorIndex_7; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__txtSceneAnchorIndex_7() { return &____txtSceneAnchorIndex_7; }
	inline void set__txtSceneAnchorIndex_7(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____txtSceneAnchorIndex_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtSceneAnchorIndex_7), value);
	}

	inline static int32_t get_offset_of_m_aryUiAnchor_8() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ___m_aryUiAnchor_8)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryUiAnchor_8() const { return ___m_aryUiAnchor_8; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryUiAnchor_8() { return &___m_aryUiAnchor_8; }
	inline void set_m_aryUiAnchor_8(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryUiAnchor_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUiAnchor_8), value);
	}

	inline static int32_t get_offset_of_m_arySceneAnchor_9() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ___m_arySceneAnchor_9)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_arySceneAnchor_9() const { return ___m_arySceneAnchor_9; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_arySceneAnchor_9() { return &___m_arySceneAnchor_9; }
	inline void set_m_arySceneAnchor_9(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_arySceneAnchor_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySceneAnchor_9), value);
	}

	inline static int32_t get_offset_of__txtDebugInfo_10() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____txtDebugInfo_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDebugInfo_10() const { return ____txtDebugInfo_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDebugInfo_10() { return &____txtDebugInfo_10; }
	inline void set__txtDebugInfo_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDebugInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&____txtDebugInfo_10), value);
	}

	inline static int32_t get_offset_of__sprAirline_11() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____sprAirline_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__sprAirline_11() const { return ____sprAirline_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__sprAirline_11() { return &____sprAirline_11; }
	inline void set__sprAirline_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____sprAirline_11 = value;
		Il2CppCodeGenWriteBarrier((&____sprAirline_11), value);
	}

	inline static int32_t get_offset_of__dropPlanetId_12() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____dropPlanetId_12)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get__dropPlanetId_12() const { return ____dropPlanetId_12; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of__dropPlanetId_12() { return &____dropPlanetId_12; }
	inline void set__dropPlanetId_12(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		____dropPlanetId_12 = value;
		Il2CppCodeGenWriteBarrier((&____dropPlanetId_12), value);
	}

	inline static int32_t get_offset_of__dropDistrictId_13() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____dropDistrictId_13)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get__dropDistrictId_13() const { return ____dropDistrictId_13; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of__dropDistrictId_13() { return &____dropDistrictId_13; }
	inline void set__dropDistrictId_13(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		____dropDistrictId_13 = value;
		Il2CppCodeGenWriteBarrier((&____dropDistrictId_13), value);
	}

	inline static int32_t get_offset_of__inputCoin0_14() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____inputCoin0_14)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__inputCoin0_14() const { return ____inputCoin0_14; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__inputCoin0_14() { return &____inputCoin0_14; }
	inline void set__inputCoin0_14(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____inputCoin0_14 = value;
		Il2CppCodeGenWriteBarrier((&____inputCoin0_14), value);
	}

	inline static int32_t get_offset_of__inputCoin1_15() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____inputCoin1_15)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__inputCoin1_15() const { return ____inputCoin1_15; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__inputCoin1_15() { return &____inputCoin1_15; }
	inline void set__inputCoin1_15(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____inputCoin1_15 = value;
		Il2CppCodeGenWriteBarrier((&____inputCoin1_15), value);
	}

	inline static int32_t get_offset_of__inputCoin2_16() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____inputCoin2_16)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__inputCoin2_16() const { return ____inputCoin2_16; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__inputCoin2_16() { return &____inputCoin2_16; }
	inline void set__inputCoin2_16(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____inputCoin2_16 = value;
		Il2CppCodeGenWriteBarrier((&____inputCoin2_16), value);
	}

	inline static int32_t get_offset_of__inputGreenCash_17() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____inputGreenCash_17)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__inputGreenCash_17() const { return ____inputGreenCash_17; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__inputGreenCash_17() { return &____inputGreenCash_17; }
	inline void set__inputGreenCash_17(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____inputGreenCash_17 = value;
		Il2CppCodeGenWriteBarrier((&____inputGreenCash_17), value);
	}

	inline static int32_t get_offset_of__inputSkillPoint0_18() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____inputSkillPoint0_18)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__inputSkillPoint0_18() const { return ____inputSkillPoint0_18; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__inputSkillPoint0_18() { return &____inputSkillPoint0_18; }
	inline void set__inputSkillPoint0_18(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____inputSkillPoint0_18 = value;
		Il2CppCodeGenWriteBarrier((&____inputSkillPoint0_18), value);
	}

	inline static int32_t get_offset_of__inputSkillPoint1_19() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____inputSkillPoint1_19)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__inputSkillPoint1_19() const { return ____inputSkillPoint1_19; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__inputSkillPoint1_19() { return &____inputSkillPoint1_19; }
	inline void set__inputSkillPoint1_19(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____inputSkillPoint1_19 = value;
		Il2CppCodeGenWriteBarrier((&____inputSkillPoint1_19), value);
	}

	inline static int32_t get_offset_of__inputSkillPoint2_20() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____inputSkillPoint2_20)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__inputSkillPoint2_20() const { return ____inputSkillPoint2_20; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__inputSkillPoint2_20() { return &____inputSkillPoint2_20; }
	inline void set__inputSkillPoint2_20(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____inputSkillPoint2_20 = value;
		Il2CppCodeGenWriteBarrier((&____inputSkillPoint2_20), value);
	}

	inline static int32_t get_offset_of__sliderVibrateAmount_21() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____sliderVibrateAmount_21)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get__sliderVibrateAmount_21() const { return ____sliderVibrateAmount_21; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of__sliderVibrateAmount_21() { return &____sliderVibrateAmount_21; }
	inline void set__sliderVibrateAmount_21(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		____sliderVibrateAmount_21 = value;
		Il2CppCodeGenWriteBarrier((&____sliderVibrateAmount_21), value);
	}

	inline static int32_t get_offset_of__txtVibrateAmount_22() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ____txtVibrateAmount_22)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtVibrateAmount_22() const { return ____txtVibrateAmount_22; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtVibrateAmount_22() { return &____txtVibrateAmount_22; }
	inline void set__txtVibrateAmount_22(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtVibrateAmount_22 = value;
		Il2CppCodeGenWriteBarrier((&____txtVibrateAmount_22), value);
	}

	inline static int32_t get_offset_of_uiCheatPanel_23() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ___uiCheatPanel_23)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_uiCheatPanel_23() const { return ___uiCheatPanel_23; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_uiCheatPanel_23() { return &___uiCheatPanel_23; }
	inline void set_uiCheatPanel_23(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___uiCheatPanel_23 = value;
		Il2CppCodeGenWriteBarrier((&___uiCheatPanel_23), value);
	}

	inline static int32_t get_offset_of_m_goCurShit_24() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ___m_goCurShit_24)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goCurShit_24() const { return ___m_goCurShit_24; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goCurShit_24() { return &___m_goCurShit_24; }
	inline void set_m_goCurShit_24(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goCurShit_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_goCurShit_24), value);
	}

	inline static int32_t get_offset_of_m_fShitSpeed_26() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ___m_fShitSpeed_26)); }
	inline float get_m_fShitSpeed_26() const { return ___m_fShitSpeed_26; }
	inline float* get_address_of_m_fShitSpeed_26() { return &___m_fShitSpeed_26; }
	inline void set_m_fShitSpeed_26(float value)
	{
		___m_fShitSpeed_26 = value;
	}

	inline static int32_t get_offset_of_m_lVibrateAmount_27() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ___m_lVibrateAmount_27)); }
	inline int64_t get_m_lVibrateAmount_27() const { return ___m_lVibrateAmount_27; }
	inline int64_t* get_address_of_m_lVibrateAmount_27() { return &___m_lVibrateAmount_27; }
	inline void set_m_lVibrateAmount_27(int64_t value)
	{
		___m_lVibrateAmount_27 = value;
	}

	inline static int32_t get_offset_of_m_bShowDebugInfo_28() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166, ___m_bShowDebugInfo_28)); }
	inline bool get_m_bShowDebugInfo_28() const { return ___m_bShowDebugInfo_28; }
	inline bool* get_address_of_m_bShowDebugInfo_28() { return &___m_bShowDebugInfo_28; }
	inline void set_m_bShowDebugInfo_28(bool value)
	{
		___m_bShowDebugInfo_28 = value;
	}
};

struct Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166_StaticFields
{
public:
	// Cheat Cheat::s_Instance
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166 * ___s_Instance_4;
	// UnityEngine.Vector3 Cheat::vec3Temp
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vec3Temp_25;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166_StaticFields, ___s_Instance_4)); }
	inline Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_vec3Temp_25() { return static_cast<int32_t>(offsetof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166_StaticFields, ___vec3Temp_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vec3Temp_25() const { return ___vec3Temp_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vec3Temp_25() { return &___vec3Temp_25; }
	inline void set_vec3Temp_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vec3Temp_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHEAT_T61A56D9B4C9BF342A86C628A46DB0F0BB704E166_H
#ifndef CYBERTREEMATH_T79DA1AC9D544A93B29F77D7E04C438A5E5A98305_H
#define CYBERTREEMATH_T79DA1AC9D544A93B29F77D7E04C438A5E5A98305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CyberTreeMath
struct  CyberTreeMath_t79DA1AC9D544A93B29F77D7E04C438A5E5A98305  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYBERTREEMATH_T79DA1AC9D544A93B29F77D7E04C438A5E5A98305_H
#ifndef DATAMANAGER_TE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_H
#define DATAMANAGER_TE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager
struct  DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean DataManager::m_bOnlineConfig
	bool ___m_bOnlineConfig_5;
	// System.Int32 DataManager::MAX_PLANET_NUM
	int32_t ___MAX_PLANET_NUM_12;
	// System.Int32 DataManager::MAX_SKILL_POINT_NUM
	int32_t ___MAX_SKILL_POINT_NUM_13;
	// System.Int32 DataManager::MAX_TRACK_NUM_OF_PLANET
	int32_t ___MAX_TRACK_NUM_OF_PLANET_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> DataManager::m_dicCoinGainPerRound
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___m_dicCoinGainPerRound_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> DataManager::m_dicCoinVehiclePrice
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___m_dicCoinVehiclePrice_16;
	// System.Single[] DataManager::m_aryRoundTimeByLevel
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryRoundTimeByLevel_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> DataManager::m_dicCoinCostToPrestige
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___m_dicCoinCostToPrestige_18;
	// System.Collections.Generic.Dictionary`2<System.String,DataManager/sAutomobileConfig> DataManager::m_dicAutomobileConfig
	Dictionary_2_t575E591ADF0DE43EE8FBD5432FDB44C619218BBC * ___m_dicAutomobileConfig_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,DataManager/sPlanetConfig> DataManager::m_dicPlanetConfig
	Dictionary_2_t35A7F0D04BB2998E9DD0CB0865E4F0442AE488AF * ___m_dicPlanetConfig_20;
	// System.Collections.Generic.Dictionary`2<System.String,DataManager/sTrackConfig> DataManager::m_dicTrackConfig
	Dictionary_2_t0FCB090B06AD243E1AEFE7A77D6E44B002171473 * ___m_dicTrackConfig_21;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> DataManager::m_dicTrackAndLevel2ResId
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___m_dicTrackAndLevel2ResId_22;
	// DataManager/sAutomobileConfig DataManager::tempAutomobileConfig
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20  ___tempAutomobileConfig_23;
	// DataManager/sPlanetConfig DataManager::tempPlanetConfig
	sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9  ___tempPlanetConfig_24;
	// DataManager/sTrackConfig DataManager::tempTrackConfig
	sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B  ___tempTrackConfig_25;
	// DataManager/sPrestigeConfig DataManager::tempPrestigeConfig
	sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63  ___tempPrestigeConfig_26;
	// System.Collections.Generic.Dictionary`2<System.String,DataManager/sPrestigeConfig> DataManager::m_dicPrestigeConfig
	Dictionary_2_t6927BC972CD2E0E92AF0DF540164BAF9ECB65E17 * ___m_dicPrestigeConfig_27;
	// System.Boolean DataManager::m_bPlanetConfigLoaded
	bool ___m_bPlanetConfigLoaded_28;
	// System.Boolean DataManager::m_bAutomobileConfigLoaded
	bool ___m_bAutomobileConfigLoaded_29;
	// System.Boolean DataManager::m_bTrackConfigLoaded
	bool ___m_bTrackConfigLoaded_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> DataManager::m_dicLoadMyData_String
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___m_dicLoadMyData_String_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Double> DataManager::m_dicLoadMyData
	Dictionary_2_tD9E97F40821434226F92E86AFDAE50D233468602 * ___m_dicLoadMyData_32;
	// System.Boolean DataManager::m_bCanSave
	bool ___m_bCanSave_33;

public:
	inline static int32_t get_offset_of_m_bOnlineConfig_5() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_bOnlineConfig_5)); }
	inline bool get_m_bOnlineConfig_5() const { return ___m_bOnlineConfig_5; }
	inline bool* get_address_of_m_bOnlineConfig_5() { return &___m_bOnlineConfig_5; }
	inline void set_m_bOnlineConfig_5(bool value)
	{
		___m_bOnlineConfig_5 = value;
	}

	inline static int32_t get_offset_of_MAX_PLANET_NUM_12() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___MAX_PLANET_NUM_12)); }
	inline int32_t get_MAX_PLANET_NUM_12() const { return ___MAX_PLANET_NUM_12; }
	inline int32_t* get_address_of_MAX_PLANET_NUM_12() { return &___MAX_PLANET_NUM_12; }
	inline void set_MAX_PLANET_NUM_12(int32_t value)
	{
		___MAX_PLANET_NUM_12 = value;
	}

	inline static int32_t get_offset_of_MAX_SKILL_POINT_NUM_13() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___MAX_SKILL_POINT_NUM_13)); }
	inline int32_t get_MAX_SKILL_POINT_NUM_13() const { return ___MAX_SKILL_POINT_NUM_13; }
	inline int32_t* get_address_of_MAX_SKILL_POINT_NUM_13() { return &___MAX_SKILL_POINT_NUM_13; }
	inline void set_MAX_SKILL_POINT_NUM_13(int32_t value)
	{
		___MAX_SKILL_POINT_NUM_13 = value;
	}

	inline static int32_t get_offset_of_MAX_TRACK_NUM_OF_PLANET_14() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___MAX_TRACK_NUM_OF_PLANET_14)); }
	inline int32_t get_MAX_TRACK_NUM_OF_PLANET_14() const { return ___MAX_TRACK_NUM_OF_PLANET_14; }
	inline int32_t* get_address_of_MAX_TRACK_NUM_OF_PLANET_14() { return &___MAX_TRACK_NUM_OF_PLANET_14; }
	inline void set_MAX_TRACK_NUM_OF_PLANET_14(int32_t value)
	{
		___MAX_TRACK_NUM_OF_PLANET_14 = value;
	}

	inline static int32_t get_offset_of_m_dicCoinGainPerRound_15() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicCoinGainPerRound_15)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_m_dicCoinGainPerRound_15() const { return ___m_dicCoinGainPerRound_15; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_m_dicCoinGainPerRound_15() { return &___m_dicCoinGainPerRound_15; }
	inline void set_m_dicCoinGainPerRound_15(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___m_dicCoinGainPerRound_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicCoinGainPerRound_15), value);
	}

	inline static int32_t get_offset_of_m_dicCoinVehiclePrice_16() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicCoinVehiclePrice_16)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_m_dicCoinVehiclePrice_16() const { return ___m_dicCoinVehiclePrice_16; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_m_dicCoinVehiclePrice_16() { return &___m_dicCoinVehiclePrice_16; }
	inline void set_m_dicCoinVehiclePrice_16(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___m_dicCoinVehiclePrice_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicCoinVehiclePrice_16), value);
	}

	inline static int32_t get_offset_of_m_aryRoundTimeByLevel_17() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_aryRoundTimeByLevel_17)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryRoundTimeByLevel_17() const { return ___m_aryRoundTimeByLevel_17; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryRoundTimeByLevel_17() { return &___m_aryRoundTimeByLevel_17; }
	inline void set_m_aryRoundTimeByLevel_17(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryRoundTimeByLevel_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryRoundTimeByLevel_17), value);
	}

	inline static int32_t get_offset_of_m_dicCoinCostToPrestige_18() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicCoinCostToPrestige_18)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_m_dicCoinCostToPrestige_18() const { return ___m_dicCoinCostToPrestige_18; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_m_dicCoinCostToPrestige_18() { return &___m_dicCoinCostToPrestige_18; }
	inline void set_m_dicCoinCostToPrestige_18(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___m_dicCoinCostToPrestige_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicCoinCostToPrestige_18), value);
	}

	inline static int32_t get_offset_of_m_dicAutomobileConfig_19() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicAutomobileConfig_19)); }
	inline Dictionary_2_t575E591ADF0DE43EE8FBD5432FDB44C619218BBC * get_m_dicAutomobileConfig_19() const { return ___m_dicAutomobileConfig_19; }
	inline Dictionary_2_t575E591ADF0DE43EE8FBD5432FDB44C619218BBC ** get_address_of_m_dicAutomobileConfig_19() { return &___m_dicAutomobileConfig_19; }
	inline void set_m_dicAutomobileConfig_19(Dictionary_2_t575E591ADF0DE43EE8FBD5432FDB44C619218BBC * value)
	{
		___m_dicAutomobileConfig_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAutomobileConfig_19), value);
	}

	inline static int32_t get_offset_of_m_dicPlanetConfig_20() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicPlanetConfig_20)); }
	inline Dictionary_2_t35A7F0D04BB2998E9DD0CB0865E4F0442AE488AF * get_m_dicPlanetConfig_20() const { return ___m_dicPlanetConfig_20; }
	inline Dictionary_2_t35A7F0D04BB2998E9DD0CB0865E4F0442AE488AF ** get_address_of_m_dicPlanetConfig_20() { return &___m_dicPlanetConfig_20; }
	inline void set_m_dicPlanetConfig_20(Dictionary_2_t35A7F0D04BB2998E9DD0CB0865E4F0442AE488AF * value)
	{
		___m_dicPlanetConfig_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPlanetConfig_20), value);
	}

	inline static int32_t get_offset_of_m_dicTrackConfig_21() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicTrackConfig_21)); }
	inline Dictionary_2_t0FCB090B06AD243E1AEFE7A77D6E44B002171473 * get_m_dicTrackConfig_21() const { return ___m_dicTrackConfig_21; }
	inline Dictionary_2_t0FCB090B06AD243E1AEFE7A77D6E44B002171473 ** get_address_of_m_dicTrackConfig_21() { return &___m_dicTrackConfig_21; }
	inline void set_m_dicTrackConfig_21(Dictionary_2_t0FCB090B06AD243E1AEFE7A77D6E44B002171473 * value)
	{
		___m_dicTrackConfig_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicTrackConfig_21), value);
	}

	inline static int32_t get_offset_of_m_dicTrackAndLevel2ResId_22() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicTrackAndLevel2ResId_22)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_m_dicTrackAndLevel2ResId_22() const { return ___m_dicTrackAndLevel2ResId_22; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_m_dicTrackAndLevel2ResId_22() { return &___m_dicTrackAndLevel2ResId_22; }
	inline void set_m_dicTrackAndLevel2ResId_22(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___m_dicTrackAndLevel2ResId_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicTrackAndLevel2ResId_22), value);
	}

	inline static int32_t get_offset_of_tempAutomobileConfig_23() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___tempAutomobileConfig_23)); }
	inline sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20  get_tempAutomobileConfig_23() const { return ___tempAutomobileConfig_23; }
	inline sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20 * get_address_of_tempAutomobileConfig_23() { return &___tempAutomobileConfig_23; }
	inline void set_tempAutomobileConfig_23(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20  value)
	{
		___tempAutomobileConfig_23 = value;
	}

	inline static int32_t get_offset_of_tempPlanetConfig_24() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___tempPlanetConfig_24)); }
	inline sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9  get_tempPlanetConfig_24() const { return ___tempPlanetConfig_24; }
	inline sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9 * get_address_of_tempPlanetConfig_24() { return &___tempPlanetConfig_24; }
	inline void set_tempPlanetConfig_24(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9  value)
	{
		___tempPlanetConfig_24 = value;
	}

	inline static int32_t get_offset_of_tempTrackConfig_25() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___tempTrackConfig_25)); }
	inline sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B  get_tempTrackConfig_25() const { return ___tempTrackConfig_25; }
	inline sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B * get_address_of_tempTrackConfig_25() { return &___tempTrackConfig_25; }
	inline void set_tempTrackConfig_25(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B  value)
	{
		___tempTrackConfig_25 = value;
	}

	inline static int32_t get_offset_of_tempPrestigeConfig_26() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___tempPrestigeConfig_26)); }
	inline sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63  get_tempPrestigeConfig_26() const { return ___tempPrestigeConfig_26; }
	inline sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63 * get_address_of_tempPrestigeConfig_26() { return &___tempPrestigeConfig_26; }
	inline void set_tempPrestigeConfig_26(sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63  value)
	{
		___tempPrestigeConfig_26 = value;
	}

	inline static int32_t get_offset_of_m_dicPrestigeConfig_27() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicPrestigeConfig_27)); }
	inline Dictionary_2_t6927BC972CD2E0E92AF0DF540164BAF9ECB65E17 * get_m_dicPrestigeConfig_27() const { return ___m_dicPrestigeConfig_27; }
	inline Dictionary_2_t6927BC972CD2E0E92AF0DF540164BAF9ECB65E17 ** get_address_of_m_dicPrestigeConfig_27() { return &___m_dicPrestigeConfig_27; }
	inline void set_m_dicPrestigeConfig_27(Dictionary_2_t6927BC972CD2E0E92AF0DF540164BAF9ECB65E17 * value)
	{
		___m_dicPrestigeConfig_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPrestigeConfig_27), value);
	}

	inline static int32_t get_offset_of_m_bPlanetConfigLoaded_28() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_bPlanetConfigLoaded_28)); }
	inline bool get_m_bPlanetConfigLoaded_28() const { return ___m_bPlanetConfigLoaded_28; }
	inline bool* get_address_of_m_bPlanetConfigLoaded_28() { return &___m_bPlanetConfigLoaded_28; }
	inline void set_m_bPlanetConfigLoaded_28(bool value)
	{
		___m_bPlanetConfigLoaded_28 = value;
	}

	inline static int32_t get_offset_of_m_bAutomobileConfigLoaded_29() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_bAutomobileConfigLoaded_29)); }
	inline bool get_m_bAutomobileConfigLoaded_29() const { return ___m_bAutomobileConfigLoaded_29; }
	inline bool* get_address_of_m_bAutomobileConfigLoaded_29() { return &___m_bAutomobileConfigLoaded_29; }
	inline void set_m_bAutomobileConfigLoaded_29(bool value)
	{
		___m_bAutomobileConfigLoaded_29 = value;
	}

	inline static int32_t get_offset_of_m_bTrackConfigLoaded_30() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_bTrackConfigLoaded_30)); }
	inline bool get_m_bTrackConfigLoaded_30() const { return ___m_bTrackConfigLoaded_30; }
	inline bool* get_address_of_m_bTrackConfigLoaded_30() { return &___m_bTrackConfigLoaded_30; }
	inline void set_m_bTrackConfigLoaded_30(bool value)
	{
		___m_bTrackConfigLoaded_30 = value;
	}

	inline static int32_t get_offset_of_m_dicLoadMyData_String_31() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicLoadMyData_String_31)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_m_dicLoadMyData_String_31() const { return ___m_dicLoadMyData_String_31; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_m_dicLoadMyData_String_31() { return &___m_dicLoadMyData_String_31; }
	inline void set_m_dicLoadMyData_String_31(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___m_dicLoadMyData_String_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLoadMyData_String_31), value);
	}

	inline static int32_t get_offset_of_m_dicLoadMyData_32() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicLoadMyData_32)); }
	inline Dictionary_2_tD9E97F40821434226F92E86AFDAE50D233468602 * get_m_dicLoadMyData_32() const { return ___m_dicLoadMyData_32; }
	inline Dictionary_2_tD9E97F40821434226F92E86AFDAE50D233468602 ** get_address_of_m_dicLoadMyData_32() { return &___m_dicLoadMyData_32; }
	inline void set_m_dicLoadMyData_32(Dictionary_2_tD9E97F40821434226F92E86AFDAE50D233468602 * value)
	{
		___m_dicLoadMyData_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLoadMyData_32), value);
	}

	inline static int32_t get_offset_of_m_bCanSave_33() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_bCanSave_33)); }
	inline bool get_m_bCanSave_33() const { return ___m_bCanSave_33; }
	inline bool* get_address_of_m_bCanSave_33() { return &___m_bCanSave_33; }
	inline void set_m_bCanSave_33(bool value)
	{
		___m_bCanSave_33 = value;
	}
};

struct DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields
{
public:
	// DataManager DataManager::s_Instance
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * ___s_Instance_4;
	// System.String DataManager::url
	String_t* ___url_6;
	// System.String DataManager::url_formal
	String_t* ___url_formal_7;
	// System.String DataManager::url_test
	String_t* ___url_test_8;
	// System.String DataManager::url_offline
	String_t* ___url_offline_9;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields, ___s_Instance_4)); }
	inline DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_url_6() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields, ___url_6)); }
	inline String_t* get_url_6() const { return ___url_6; }
	inline String_t** get_address_of_url_6() { return &___url_6; }
	inline void set_url_6(String_t* value)
	{
		___url_6 = value;
		Il2CppCodeGenWriteBarrier((&___url_6), value);
	}

	inline static int32_t get_offset_of_url_formal_7() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields, ___url_formal_7)); }
	inline String_t* get_url_formal_7() const { return ___url_formal_7; }
	inline String_t** get_address_of_url_formal_7() { return &___url_formal_7; }
	inline void set_url_formal_7(String_t* value)
	{
		___url_formal_7 = value;
		Il2CppCodeGenWriteBarrier((&___url_formal_7), value);
	}

	inline static int32_t get_offset_of_url_test_8() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields, ___url_test_8)); }
	inline String_t* get_url_test_8() const { return ___url_test_8; }
	inline String_t** get_address_of_url_test_8() { return &___url_test_8; }
	inline void set_url_test_8(String_t* value)
	{
		___url_test_8 = value;
		Il2CppCodeGenWriteBarrier((&___url_test_8), value);
	}

	inline static int32_t get_offset_of_url_offline_9() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields, ___url_offline_9)); }
	inline String_t* get_url_offline_9() const { return ___url_offline_9; }
	inline String_t** get_address_of_url_offline_9() { return &___url_offline_9; }
	inline void set_url_offline_9(String_t* value)
	{
		___url_offline_9 = value;
		Il2CppCodeGenWriteBarrier((&___url_offline_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAMANAGER_TE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_H
#ifndef DEBUGINFO_T9A4A67660130ACFB573B3479EA5C46A8E739BC56_H
#define DEBUGINFO_T9A4A67660130ACFB573B3479EA5C46A8E739BC56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugInfo
struct  DebugInfo_t9A4A67660130ACFB573B3479EA5C46A8E739BC56  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text DebugInfo::_txtPlanetAndDistrictId
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPlanetAndDistrictId_5;

public:
	inline static int32_t get_offset_of__txtPlanetAndDistrictId_5() { return static_cast<int32_t>(offsetof(DebugInfo_t9A4A67660130ACFB573B3479EA5C46A8E739BC56, ____txtPlanetAndDistrictId_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPlanetAndDistrictId_5() const { return ____txtPlanetAndDistrictId_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPlanetAndDistrictId_5() { return &____txtPlanetAndDistrictId_5; }
	inline void set__txtPlanetAndDistrictId_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPlanetAndDistrictId_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlanetAndDistrictId_5), value);
	}
};

struct DebugInfo_t9A4A67660130ACFB573B3479EA5C46A8E739BC56_StaticFields
{
public:
	// DebugInfo DebugInfo::s_Instance
	DebugInfo_t9A4A67660130ACFB573B3479EA5C46A8E739BC56 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(DebugInfo_t9A4A67660130ACFB573B3479EA5C46A8E739BC56_StaticFields, ___s_Instance_4)); }
	inline DebugInfo_t9A4A67660130ACFB573B3479EA5C46A8E739BC56 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline DebugInfo_t9A4A67660130ACFB573B3479EA5C46A8E739BC56 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(DebugInfo_t9A4A67660130ACFB573B3479EA5C46A8E739BC56 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGINFO_T9A4A67660130ACFB573B3479EA5C46A8E739BC56_H
#ifndef DISTRICT_T773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A_H
#define DISTRICT_T773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// District
struct  District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.DateTime District::m_dtLastWatchAdsVehicleFreeTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dtLastWatchAdsVehicleFreeTime_4;
	// Planet District::m_BoundPlanet
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * ___m_BoundPlanet_5;
	// MapManager/eDistrictStatus District::m_eStatus
	int32_t ___m_eStatus_6;
	// System.Int32 District::m_nId
	int32_t ___m_nId_7;
	// System.Int32 District::m_nUnlockPrice
	int32_t ___m_nUnlockPrice_8;
	// System.String District::m_szData
	String_t* ___m_szData_9;
	// System.Int32 District::m_nPrestigeTimes
	int32_t ___m_nPrestigeTimes_10;
	// System.Int32[] District::m_aryVehicleBuyTimes
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_aryVehicleBuyTimes_11;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> District::m_dicVehicleBuyTimes
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_dicVehicleBuyTimes_12;
	// System.Int32 District::m_nAdsRaiseTime
	int32_t ___m_nAdsRaiseTime_13;
	// System.Collections.Generic.Dictionary`2<SkillManager/eSkillType,Skill> District::m_dicSkill
	Dictionary_2_t5BEFDBA4420F57163BF6DF9697D75434371B6E60 * ___m_dicSkill_14;
	// SkillManager/sSkillConfig District::config
	sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E  ___config_15;
	// System.Int32 District::m_nAdsBaseTime
	int32_t ___m_nAdsBaseTime_16;
	// System.Collections.Generic.List`1<Plane> District::m_lstRunningPlanes
	List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * ___m_lstRunningPlanes_17;
	// System.Int32 District::m_nLevel
	int32_t ___m_nLevel_18;
	// System.Int32 District::m_nBuyAdminTimes
	int32_t ___m_nBuyAdminTimes_19;
	// UIAdministratorCounter District::m_CurUsingAdmin
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78 * ___m_CurUsingAdmin_20;
	// System.Collections.Generic.List`1<Admin> District::m_lstAdmins
	List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34 * ___m_lstAdmins_21;
	// ResearchCounter District::m_ResearchCounter
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * ___m_ResearchCounter_22;
	// System.Int32 District::m_nDropLevel
	int32_t ___m_nDropLevel_23;
	// System.Int32 District::m_nDropInterval
	int32_t ___m_nDropInterval_24;
	// System.Int32 District::m_nMaxCoinBuyLevel
	int32_t ___m_nMaxCoinBuyLevel_25;
	// System.Int32 District::m_nCurLotNum
	int32_t ___m_nCurLotNum_26;
	// System.DateTime District::m_dtAdsStartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dtAdsStartTime_27;
	// System.Int32 District::m_nAdsLeftTime
	int32_t ___m_nAdsLeftTime_28;
	// System.DateTime District::m_fStartOfflineTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_fStartOfflineTime_29;
	// System.DateTime District::m_fLastOnlineTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_fLastOnlineTime_30;
	// System.Double District::m_fOfflineGainPerSecondBase
	double ___m_fOfflineGainPerSecondBase_31;
	// System.Double District::m_fOfflineGainPerSecondReal
	double ___m_fOfflineGainPerSecondReal_32;
	// System.Boolean District::m_bOffline
	bool ___m_bOffline_33;
	// System.Double District::m_fDPS
	double ___m_fDPS_34;
	// System.Single District::m_fSpeedAccelerateRate
	float ___m_fSpeedAccelerateRate_35;
	// System.Single District::m_fTimeElapseOffline
	float ___m_fTimeElapseOffline_37;
	// System.Double District::m_fCurTotalOfflineGain
	double ___m_fCurTotalOfflineGain_38;
	// System.Collections.Generic.List`1<District/sLevelAndCost> District::m_lstLevelAndCost
	List_1_tB9FA86EA4317FBDAC182273F5D02A08CB5B68D86 * ___m_lstLevelAndCost_39;
	// District/sLevelAndCost District::m_TheSelectedOne
	sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA  ___m_TheSelectedOne_40;
	// System.Collections.Generic.Dictionary`2<System.Int32,ResearchCounter> District::m_dicEnergyResearchCounters
	Dictionary_2_t1D475F8089C35FF3718700296FB38C143E09149F * ___m_dicEnergyResearchCounters_41;
	// System.Single District::m_fTrackCoinRaise
	float ___m_fTrackCoinRaise_42;
	// System.Single District::m_fSpeedAccelerate
	float ___m_fSpeedAccelerate_43;
	// System.Single[] District::m_aryAccelerate
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryAccelerate_45;

public:
	inline static int32_t get_offset_of_m_dtLastWatchAdsVehicleFreeTime_4() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_dtLastWatchAdsVehicleFreeTime_4)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dtLastWatchAdsVehicleFreeTime_4() const { return ___m_dtLastWatchAdsVehicleFreeTime_4; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dtLastWatchAdsVehicleFreeTime_4() { return &___m_dtLastWatchAdsVehicleFreeTime_4; }
	inline void set_m_dtLastWatchAdsVehicleFreeTime_4(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dtLastWatchAdsVehicleFreeTime_4 = value;
	}

	inline static int32_t get_offset_of_m_BoundPlanet_5() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_BoundPlanet_5)); }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * get_m_BoundPlanet_5() const { return ___m_BoundPlanet_5; }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B ** get_address_of_m_BoundPlanet_5() { return &___m_BoundPlanet_5; }
	inline void set_m_BoundPlanet_5(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * value)
	{
		___m_BoundPlanet_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundPlanet_5), value);
	}

	inline static int32_t get_offset_of_m_eStatus_6() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_eStatus_6)); }
	inline int32_t get_m_eStatus_6() const { return ___m_eStatus_6; }
	inline int32_t* get_address_of_m_eStatus_6() { return &___m_eStatus_6; }
	inline void set_m_eStatus_6(int32_t value)
	{
		___m_eStatus_6 = value;
	}

	inline static int32_t get_offset_of_m_nId_7() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nId_7)); }
	inline int32_t get_m_nId_7() const { return ___m_nId_7; }
	inline int32_t* get_address_of_m_nId_7() { return &___m_nId_7; }
	inline void set_m_nId_7(int32_t value)
	{
		___m_nId_7 = value;
	}

	inline static int32_t get_offset_of_m_nUnlockPrice_8() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nUnlockPrice_8)); }
	inline int32_t get_m_nUnlockPrice_8() const { return ___m_nUnlockPrice_8; }
	inline int32_t* get_address_of_m_nUnlockPrice_8() { return &___m_nUnlockPrice_8; }
	inline void set_m_nUnlockPrice_8(int32_t value)
	{
		___m_nUnlockPrice_8 = value;
	}

	inline static int32_t get_offset_of_m_szData_9() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_szData_9)); }
	inline String_t* get_m_szData_9() const { return ___m_szData_9; }
	inline String_t** get_address_of_m_szData_9() { return &___m_szData_9; }
	inline void set_m_szData_9(String_t* value)
	{
		___m_szData_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_szData_9), value);
	}

	inline static int32_t get_offset_of_m_nPrestigeTimes_10() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nPrestigeTimes_10)); }
	inline int32_t get_m_nPrestigeTimes_10() const { return ___m_nPrestigeTimes_10; }
	inline int32_t* get_address_of_m_nPrestigeTimes_10() { return &___m_nPrestigeTimes_10; }
	inline void set_m_nPrestigeTimes_10(int32_t value)
	{
		___m_nPrestigeTimes_10 = value;
	}

	inline static int32_t get_offset_of_m_aryVehicleBuyTimes_11() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_aryVehicleBuyTimes_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_aryVehicleBuyTimes_11() const { return ___m_aryVehicleBuyTimes_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_aryVehicleBuyTimes_11() { return &___m_aryVehicleBuyTimes_11; }
	inline void set_m_aryVehicleBuyTimes_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_aryVehicleBuyTimes_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryVehicleBuyTimes_11), value);
	}

	inline static int32_t get_offset_of_m_dicVehicleBuyTimes_12() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_dicVehicleBuyTimes_12)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_dicVehicleBuyTimes_12() const { return ___m_dicVehicleBuyTimes_12; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_dicVehicleBuyTimes_12() { return &___m_dicVehicleBuyTimes_12; }
	inline void set_m_dicVehicleBuyTimes_12(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_dicVehicleBuyTimes_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicVehicleBuyTimes_12), value);
	}

	inline static int32_t get_offset_of_m_nAdsRaiseTime_13() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nAdsRaiseTime_13)); }
	inline int32_t get_m_nAdsRaiseTime_13() const { return ___m_nAdsRaiseTime_13; }
	inline int32_t* get_address_of_m_nAdsRaiseTime_13() { return &___m_nAdsRaiseTime_13; }
	inline void set_m_nAdsRaiseTime_13(int32_t value)
	{
		___m_nAdsRaiseTime_13 = value;
	}

	inline static int32_t get_offset_of_m_dicSkill_14() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_dicSkill_14)); }
	inline Dictionary_2_t5BEFDBA4420F57163BF6DF9697D75434371B6E60 * get_m_dicSkill_14() const { return ___m_dicSkill_14; }
	inline Dictionary_2_t5BEFDBA4420F57163BF6DF9697D75434371B6E60 ** get_address_of_m_dicSkill_14() { return &___m_dicSkill_14; }
	inline void set_m_dicSkill_14(Dictionary_2_t5BEFDBA4420F57163BF6DF9697D75434371B6E60 * value)
	{
		___m_dicSkill_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSkill_14), value);
	}

	inline static int32_t get_offset_of_config_15() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___config_15)); }
	inline sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E  get_config_15() const { return ___config_15; }
	inline sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E * get_address_of_config_15() { return &___config_15; }
	inline void set_config_15(sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E  value)
	{
		___config_15 = value;
	}

	inline static int32_t get_offset_of_m_nAdsBaseTime_16() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nAdsBaseTime_16)); }
	inline int32_t get_m_nAdsBaseTime_16() const { return ___m_nAdsBaseTime_16; }
	inline int32_t* get_address_of_m_nAdsBaseTime_16() { return &___m_nAdsBaseTime_16; }
	inline void set_m_nAdsBaseTime_16(int32_t value)
	{
		___m_nAdsBaseTime_16 = value;
	}

	inline static int32_t get_offset_of_m_lstRunningPlanes_17() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_lstRunningPlanes_17)); }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * get_m_lstRunningPlanes_17() const { return ___m_lstRunningPlanes_17; }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 ** get_address_of_m_lstRunningPlanes_17() { return &___m_lstRunningPlanes_17; }
	inline void set_m_lstRunningPlanes_17(List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * value)
	{
		___m_lstRunningPlanes_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRunningPlanes_17), value);
	}

	inline static int32_t get_offset_of_m_nLevel_18() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nLevel_18)); }
	inline int32_t get_m_nLevel_18() const { return ___m_nLevel_18; }
	inline int32_t* get_address_of_m_nLevel_18() { return &___m_nLevel_18; }
	inline void set_m_nLevel_18(int32_t value)
	{
		___m_nLevel_18 = value;
	}

	inline static int32_t get_offset_of_m_nBuyAdminTimes_19() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nBuyAdminTimes_19)); }
	inline int32_t get_m_nBuyAdminTimes_19() const { return ___m_nBuyAdminTimes_19; }
	inline int32_t* get_address_of_m_nBuyAdminTimes_19() { return &___m_nBuyAdminTimes_19; }
	inline void set_m_nBuyAdminTimes_19(int32_t value)
	{
		___m_nBuyAdminTimes_19 = value;
	}

	inline static int32_t get_offset_of_m_CurUsingAdmin_20() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_CurUsingAdmin_20)); }
	inline UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78 * get_m_CurUsingAdmin_20() const { return ___m_CurUsingAdmin_20; }
	inline UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78 ** get_address_of_m_CurUsingAdmin_20() { return &___m_CurUsingAdmin_20; }
	inline void set_m_CurUsingAdmin_20(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78 * value)
	{
		___m_CurUsingAdmin_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurUsingAdmin_20), value);
	}

	inline static int32_t get_offset_of_m_lstAdmins_21() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_lstAdmins_21)); }
	inline List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34 * get_m_lstAdmins_21() const { return ___m_lstAdmins_21; }
	inline List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34 ** get_address_of_m_lstAdmins_21() { return &___m_lstAdmins_21; }
	inline void set_m_lstAdmins_21(List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34 * value)
	{
		___m_lstAdmins_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstAdmins_21), value);
	}

	inline static int32_t get_offset_of_m_ResearchCounter_22() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_ResearchCounter_22)); }
	inline ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * get_m_ResearchCounter_22() const { return ___m_ResearchCounter_22; }
	inline ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD ** get_address_of_m_ResearchCounter_22() { return &___m_ResearchCounter_22; }
	inline void set_m_ResearchCounter_22(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * value)
	{
		___m_ResearchCounter_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ResearchCounter_22), value);
	}

	inline static int32_t get_offset_of_m_nDropLevel_23() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nDropLevel_23)); }
	inline int32_t get_m_nDropLevel_23() const { return ___m_nDropLevel_23; }
	inline int32_t* get_address_of_m_nDropLevel_23() { return &___m_nDropLevel_23; }
	inline void set_m_nDropLevel_23(int32_t value)
	{
		___m_nDropLevel_23 = value;
	}

	inline static int32_t get_offset_of_m_nDropInterval_24() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nDropInterval_24)); }
	inline int32_t get_m_nDropInterval_24() const { return ___m_nDropInterval_24; }
	inline int32_t* get_address_of_m_nDropInterval_24() { return &___m_nDropInterval_24; }
	inline void set_m_nDropInterval_24(int32_t value)
	{
		___m_nDropInterval_24 = value;
	}

	inline static int32_t get_offset_of_m_nMaxCoinBuyLevel_25() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nMaxCoinBuyLevel_25)); }
	inline int32_t get_m_nMaxCoinBuyLevel_25() const { return ___m_nMaxCoinBuyLevel_25; }
	inline int32_t* get_address_of_m_nMaxCoinBuyLevel_25() { return &___m_nMaxCoinBuyLevel_25; }
	inline void set_m_nMaxCoinBuyLevel_25(int32_t value)
	{
		___m_nMaxCoinBuyLevel_25 = value;
	}

	inline static int32_t get_offset_of_m_nCurLotNum_26() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nCurLotNum_26)); }
	inline int32_t get_m_nCurLotNum_26() const { return ___m_nCurLotNum_26; }
	inline int32_t* get_address_of_m_nCurLotNum_26() { return &___m_nCurLotNum_26; }
	inline void set_m_nCurLotNum_26(int32_t value)
	{
		___m_nCurLotNum_26 = value;
	}

	inline static int32_t get_offset_of_m_dtAdsStartTime_27() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_dtAdsStartTime_27)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dtAdsStartTime_27() const { return ___m_dtAdsStartTime_27; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dtAdsStartTime_27() { return &___m_dtAdsStartTime_27; }
	inline void set_m_dtAdsStartTime_27(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dtAdsStartTime_27 = value;
	}

	inline static int32_t get_offset_of_m_nAdsLeftTime_28() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nAdsLeftTime_28)); }
	inline int32_t get_m_nAdsLeftTime_28() const { return ___m_nAdsLeftTime_28; }
	inline int32_t* get_address_of_m_nAdsLeftTime_28() { return &___m_nAdsLeftTime_28; }
	inline void set_m_nAdsLeftTime_28(int32_t value)
	{
		___m_nAdsLeftTime_28 = value;
	}

	inline static int32_t get_offset_of_m_fStartOfflineTime_29() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fStartOfflineTime_29)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_fStartOfflineTime_29() const { return ___m_fStartOfflineTime_29; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_fStartOfflineTime_29() { return &___m_fStartOfflineTime_29; }
	inline void set_m_fStartOfflineTime_29(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_fStartOfflineTime_29 = value;
	}

	inline static int32_t get_offset_of_m_fLastOnlineTime_30() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fLastOnlineTime_30)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_fLastOnlineTime_30() const { return ___m_fLastOnlineTime_30; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_fLastOnlineTime_30() { return &___m_fLastOnlineTime_30; }
	inline void set_m_fLastOnlineTime_30(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_fLastOnlineTime_30 = value;
	}

	inline static int32_t get_offset_of_m_fOfflineGainPerSecondBase_31() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fOfflineGainPerSecondBase_31)); }
	inline double get_m_fOfflineGainPerSecondBase_31() const { return ___m_fOfflineGainPerSecondBase_31; }
	inline double* get_address_of_m_fOfflineGainPerSecondBase_31() { return &___m_fOfflineGainPerSecondBase_31; }
	inline void set_m_fOfflineGainPerSecondBase_31(double value)
	{
		___m_fOfflineGainPerSecondBase_31 = value;
	}

	inline static int32_t get_offset_of_m_fOfflineGainPerSecondReal_32() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fOfflineGainPerSecondReal_32)); }
	inline double get_m_fOfflineGainPerSecondReal_32() const { return ___m_fOfflineGainPerSecondReal_32; }
	inline double* get_address_of_m_fOfflineGainPerSecondReal_32() { return &___m_fOfflineGainPerSecondReal_32; }
	inline void set_m_fOfflineGainPerSecondReal_32(double value)
	{
		___m_fOfflineGainPerSecondReal_32 = value;
	}

	inline static int32_t get_offset_of_m_bOffline_33() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_bOffline_33)); }
	inline bool get_m_bOffline_33() const { return ___m_bOffline_33; }
	inline bool* get_address_of_m_bOffline_33() { return &___m_bOffline_33; }
	inline void set_m_bOffline_33(bool value)
	{
		___m_bOffline_33 = value;
	}

	inline static int32_t get_offset_of_m_fDPS_34() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fDPS_34)); }
	inline double get_m_fDPS_34() const { return ___m_fDPS_34; }
	inline double* get_address_of_m_fDPS_34() { return &___m_fDPS_34; }
	inline void set_m_fDPS_34(double value)
	{
		___m_fDPS_34 = value;
	}

	inline static int32_t get_offset_of_m_fSpeedAccelerateRate_35() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fSpeedAccelerateRate_35)); }
	inline float get_m_fSpeedAccelerateRate_35() const { return ___m_fSpeedAccelerateRate_35; }
	inline float* get_address_of_m_fSpeedAccelerateRate_35() { return &___m_fSpeedAccelerateRate_35; }
	inline void set_m_fSpeedAccelerateRate_35(float value)
	{
		___m_fSpeedAccelerateRate_35 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapseOffline_37() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fTimeElapseOffline_37)); }
	inline float get_m_fTimeElapseOffline_37() const { return ___m_fTimeElapseOffline_37; }
	inline float* get_address_of_m_fTimeElapseOffline_37() { return &___m_fTimeElapseOffline_37; }
	inline void set_m_fTimeElapseOffline_37(float value)
	{
		___m_fTimeElapseOffline_37 = value;
	}

	inline static int32_t get_offset_of_m_fCurTotalOfflineGain_38() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fCurTotalOfflineGain_38)); }
	inline double get_m_fCurTotalOfflineGain_38() const { return ___m_fCurTotalOfflineGain_38; }
	inline double* get_address_of_m_fCurTotalOfflineGain_38() { return &___m_fCurTotalOfflineGain_38; }
	inline void set_m_fCurTotalOfflineGain_38(double value)
	{
		___m_fCurTotalOfflineGain_38 = value;
	}

	inline static int32_t get_offset_of_m_lstLevelAndCost_39() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_lstLevelAndCost_39)); }
	inline List_1_tB9FA86EA4317FBDAC182273F5D02A08CB5B68D86 * get_m_lstLevelAndCost_39() const { return ___m_lstLevelAndCost_39; }
	inline List_1_tB9FA86EA4317FBDAC182273F5D02A08CB5B68D86 ** get_address_of_m_lstLevelAndCost_39() { return &___m_lstLevelAndCost_39; }
	inline void set_m_lstLevelAndCost_39(List_1_tB9FA86EA4317FBDAC182273F5D02A08CB5B68D86 * value)
	{
		___m_lstLevelAndCost_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstLevelAndCost_39), value);
	}

	inline static int32_t get_offset_of_m_TheSelectedOne_40() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_TheSelectedOne_40)); }
	inline sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA  get_m_TheSelectedOne_40() const { return ___m_TheSelectedOne_40; }
	inline sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA * get_address_of_m_TheSelectedOne_40() { return &___m_TheSelectedOne_40; }
	inline void set_m_TheSelectedOne_40(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA  value)
	{
		___m_TheSelectedOne_40 = value;
	}

	inline static int32_t get_offset_of_m_dicEnergyResearchCounters_41() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_dicEnergyResearchCounters_41)); }
	inline Dictionary_2_t1D475F8089C35FF3718700296FB38C143E09149F * get_m_dicEnergyResearchCounters_41() const { return ___m_dicEnergyResearchCounters_41; }
	inline Dictionary_2_t1D475F8089C35FF3718700296FB38C143E09149F ** get_address_of_m_dicEnergyResearchCounters_41() { return &___m_dicEnergyResearchCounters_41; }
	inline void set_m_dicEnergyResearchCounters_41(Dictionary_2_t1D475F8089C35FF3718700296FB38C143E09149F * value)
	{
		___m_dicEnergyResearchCounters_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicEnergyResearchCounters_41), value);
	}

	inline static int32_t get_offset_of_m_fTrackCoinRaise_42() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fTrackCoinRaise_42)); }
	inline float get_m_fTrackCoinRaise_42() const { return ___m_fTrackCoinRaise_42; }
	inline float* get_address_of_m_fTrackCoinRaise_42() { return &___m_fTrackCoinRaise_42; }
	inline void set_m_fTrackCoinRaise_42(float value)
	{
		___m_fTrackCoinRaise_42 = value;
	}

	inline static int32_t get_offset_of_m_fSpeedAccelerate_43() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fSpeedAccelerate_43)); }
	inline float get_m_fSpeedAccelerate_43() const { return ___m_fSpeedAccelerate_43; }
	inline float* get_address_of_m_fSpeedAccelerate_43() { return &___m_fSpeedAccelerate_43; }
	inline void set_m_fSpeedAccelerate_43(float value)
	{
		___m_fSpeedAccelerate_43 = value;
	}

	inline static int32_t get_offset_of_m_aryAccelerate_45() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_aryAccelerate_45)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryAccelerate_45() const { return ___m_aryAccelerate_45; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryAccelerate_45() { return &___m_aryAccelerate_45; }
	inline void set_m_aryAccelerate_45(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryAccelerate_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAccelerate_45), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTRICT_T773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A_H
#ifndef ECONOMYSYSTEM_T6205DAAC929785220BA01D0AF128C856AE09F0B1_H
#define ECONOMYSYSTEM_T6205DAAC929785220BA01D0AF128C856AE09F0B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EconomySystem
struct  EconomySystem_t6205DAAC929785220BA01D0AF128C856AE09F0B1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single[] EconomySystem::m_aryCoin
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryCoin_4;
	// System.Single EconomySystem::m_fGreenCash
	float ___m_fGreenCash_5;

public:
	inline static int32_t get_offset_of_m_aryCoin_4() { return static_cast<int32_t>(offsetof(EconomySystem_t6205DAAC929785220BA01D0AF128C856AE09F0B1, ___m_aryCoin_4)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryCoin_4() const { return ___m_aryCoin_4; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryCoin_4() { return &___m_aryCoin_4; }
	inline void set_m_aryCoin_4(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryCoin_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoin_4), value);
	}

	inline static int32_t get_offset_of_m_fGreenCash_5() { return static_cast<int32_t>(offsetof(EconomySystem_t6205DAAC929785220BA01D0AF128C856AE09F0B1, ___m_fGreenCash_5)); }
	inline float get_m_fGreenCash_5() const { return ___m_fGreenCash_5; }
	inline float* get_address_of_m_fGreenCash_5() { return &___m_fGreenCash_5; }
	inline void set_m_fGreenCash_5(float value)
	{
		___m_fGreenCash_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECONOMYSYSTEM_T6205DAAC929785220BA01D0AF128C856AE09F0B1_H
#ifndef EFFECTMANAGER_T6FBC4ED79E1183ACFFB423CF287C653C14B32496_H
#define EFFECTMANAGER_T6FBC4ED79E1183ACFFB423CF287C653C14B32496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectManager
struct  EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject[] EffectManager::m_aryEffectPrefabs
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryEffectPrefabs_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> EffectManager::m_lstMergeAnimation
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___m_lstMergeAnimation_6;
	// System.Int32 EffectManager::m_nMergeEffectIndex
	int32_t ___m_nMergeEffectIndex_7;

public:
	inline static int32_t get_offset_of_m_aryEffectPrefabs_5() { return static_cast<int32_t>(offsetof(EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496, ___m_aryEffectPrefabs_5)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryEffectPrefabs_5() const { return ___m_aryEffectPrefabs_5; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryEffectPrefabs_5() { return &___m_aryEffectPrefabs_5; }
	inline void set_m_aryEffectPrefabs_5(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryEffectPrefabs_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryEffectPrefabs_5), value);
	}

	inline static int32_t get_offset_of_m_lstMergeAnimation_6() { return static_cast<int32_t>(offsetof(EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496, ___m_lstMergeAnimation_6)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_m_lstMergeAnimation_6() const { return ___m_lstMergeAnimation_6; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_m_lstMergeAnimation_6() { return &___m_lstMergeAnimation_6; }
	inline void set_m_lstMergeAnimation_6(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___m_lstMergeAnimation_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstMergeAnimation_6), value);
	}

	inline static int32_t get_offset_of_m_nMergeEffectIndex_7() { return static_cast<int32_t>(offsetof(EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496, ___m_nMergeEffectIndex_7)); }
	inline int32_t get_m_nMergeEffectIndex_7() const { return ___m_nMergeEffectIndex_7; }
	inline int32_t* get_address_of_m_nMergeEffectIndex_7() { return &___m_nMergeEffectIndex_7; }
	inline void set_m_nMergeEffectIndex_7(int32_t value)
	{
		___m_nMergeEffectIndex_7 = value;
	}
};

struct EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496_StaticFields
{
public:
	// EffectManager EffectManager::s_Instance
	EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496_StaticFields, ___s_Instance_4)); }
	inline EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTMANAGER_T6FBC4ED79E1183ACFFB423CF287C653C14B32496_H
#ifndef ENVIROMENTMASK_T88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73_H
#define ENVIROMENTMASK_T88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnviromentMask
struct  EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.SpriteMask EnviromentMask::_sm
	SpriteMask_tE6D9EAE54A65A5207E4635728D2D01385B5D1804 * ____sm_4;
	// UnityEngine.SpriteRenderer EnviromentMask::_srEnviromnet
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srEnviromnet_5;
	// UnityEngine.Color EnviromentMask::_colorBlack
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____colorBlack_6;
	// UnityEngine.Color EnviromentMask::_colorEnviroment
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ____colorEnviroment_7;

public:
	inline static int32_t get_offset_of__sm_4() { return static_cast<int32_t>(offsetof(EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73, ____sm_4)); }
	inline SpriteMask_tE6D9EAE54A65A5207E4635728D2D01385B5D1804 * get__sm_4() const { return ____sm_4; }
	inline SpriteMask_tE6D9EAE54A65A5207E4635728D2D01385B5D1804 ** get_address_of__sm_4() { return &____sm_4; }
	inline void set__sm_4(SpriteMask_tE6D9EAE54A65A5207E4635728D2D01385B5D1804 * value)
	{
		____sm_4 = value;
		Il2CppCodeGenWriteBarrier((&____sm_4), value);
	}

	inline static int32_t get_offset_of__srEnviromnet_5() { return static_cast<int32_t>(offsetof(EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73, ____srEnviromnet_5)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srEnviromnet_5() const { return ____srEnviromnet_5; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srEnviromnet_5() { return &____srEnviromnet_5; }
	inline void set__srEnviromnet_5(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srEnviromnet_5 = value;
		Il2CppCodeGenWriteBarrier((&____srEnviromnet_5), value);
	}

	inline static int32_t get_offset_of__colorBlack_6() { return static_cast<int32_t>(offsetof(EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73, ____colorBlack_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__colorBlack_6() const { return ____colorBlack_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__colorBlack_6() { return &____colorBlack_6; }
	inline void set__colorBlack_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____colorBlack_6 = value;
	}

	inline static int32_t get_offset_of__colorEnviroment_7() { return static_cast<int32_t>(offsetof(EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73, ____colorEnviroment_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get__colorEnviroment_7() const { return ____colorEnviroment_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of__colorEnviroment_7() { return &____colorEnviroment_7; }
	inline void set__colorEnviroment_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		____colorEnviroment_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVIROMENTMASK_T88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73_H
#ifndef FRESHGUIDE_T840B865F5BBA1373DC91F27C5939E88FD6095DD3_H
#define FRESHGUIDE_T840B865F5BBA1373DC91F27C5939E88FD6095DD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FreshGuide
struct  FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject FreshGuide::_containerScene
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerScene_6;
	// UnityEngine.GameObject FreshGuide::_goMask
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goMask_7;
	// System.Boolean FreshGuide::m_bMaskVisible
	bool ___m_bMaskVisible_8;
	// UnityEngine.UI.Text FreshGuide::_txtX
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtX_9;
	// UnityEngine.UI.Text FreshGuide::_txtY
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtY_10;
	// UnityEngine.UI.Text FreshGuide::_txtRadius
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtRadius_11;
	// UnityEngine.UI.Text FreshGuide::_txtPawX
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPawX_12;
	// UnityEngine.UI.Text FreshGuide::_txtPawY
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPawY_13;
	// UnityEngine.Material FreshGuide::m_matMask
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_matMask_14;
	// System.Single FreshGuide::m_fX
	float ___m_fX_15;
	// System.Single FreshGuide::m_fY
	float ___m_fY_16;
	// System.Single FreshGuide::m_fRadius
	float ___m_fRadius_17;
	// UnityEngine.GameObject FreshGuide::_uiRecommend
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiRecommend_18;
	// UnityEngine.GameObject FreshGuide::_lot0
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____lot0_19;
	// UnityEngine.GameObject FreshGuide::_lot1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____lot1_20;
	// UnityEngine.GameObject FreshGuide::_lot2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____lot2_21;
	// UnityEngine.GameObject FreshGuide::_lot3
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____lot3_22;
	// System.Collections.Generic.Dictionary`2<FreshGuide/eGuideType,System.Boolean> FreshGuide::m_dicDone
	Dictionary_2_tE758D927E90AD1ECFDE01DDC3DB458104EDA3D92 * ___m_dicDone_26;
	// Paw FreshGuide::_paw
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * ____paw_27;
	// CFrameAnimationEffect FreshGuide::_faClick
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * ____faClick_28;
	// FreshGuide/eGuideType FreshGuide::m_eCurType
	int32_t ___m_eCurType_29;
	// System.Int32 FreshGuide::m_nStatus
	int32_t ___m_nStatus_30;
	// System.Int32 FreshGuide::m_nSubStatus
	int32_t ___m_nSubStatus_31;
	// System.Int32[] FreshGuide::m_aryIntParams
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_aryIntParams_32;
	// System.Single[] FreshGuide::m_aryFloatParams
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryFloatParams_33;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> FreshGuide::m_dicFloatParams
	Dictionary_2_t89F484A77FD6673A172CE80BD0A37031FBD1FD6A * ___m_dicFloatParams_34;
	// System.Single FreshGuide::m_fTimeElapse
	float ___m_fTimeElapse_35;
	// System.Boolean FreshGuide::m_bMasking
	bool ___m_bMasking_36;

public:
	inline static int32_t get_offset_of__containerScene_6() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____containerScene_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerScene_6() const { return ____containerScene_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerScene_6() { return &____containerScene_6; }
	inline void set__containerScene_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerScene_6 = value;
		Il2CppCodeGenWriteBarrier((&____containerScene_6), value);
	}

	inline static int32_t get_offset_of__goMask_7() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____goMask_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goMask_7() const { return ____goMask_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goMask_7() { return &____goMask_7; }
	inline void set__goMask_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goMask_7 = value;
		Il2CppCodeGenWriteBarrier((&____goMask_7), value);
	}

	inline static int32_t get_offset_of_m_bMaskVisible_8() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_bMaskVisible_8)); }
	inline bool get_m_bMaskVisible_8() const { return ___m_bMaskVisible_8; }
	inline bool* get_address_of_m_bMaskVisible_8() { return &___m_bMaskVisible_8; }
	inline void set_m_bMaskVisible_8(bool value)
	{
		___m_bMaskVisible_8 = value;
	}

	inline static int32_t get_offset_of__txtX_9() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____txtX_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtX_9() const { return ____txtX_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtX_9() { return &____txtX_9; }
	inline void set__txtX_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtX_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtX_9), value);
	}

	inline static int32_t get_offset_of__txtY_10() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____txtY_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtY_10() const { return ____txtY_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtY_10() { return &____txtY_10; }
	inline void set__txtY_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtY_10 = value;
		Il2CppCodeGenWriteBarrier((&____txtY_10), value);
	}

	inline static int32_t get_offset_of__txtRadius_11() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____txtRadius_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtRadius_11() const { return ____txtRadius_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtRadius_11() { return &____txtRadius_11; }
	inline void set__txtRadius_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtRadius_11 = value;
		Il2CppCodeGenWriteBarrier((&____txtRadius_11), value);
	}

	inline static int32_t get_offset_of__txtPawX_12() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____txtPawX_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPawX_12() const { return ____txtPawX_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPawX_12() { return &____txtPawX_12; }
	inline void set__txtPawX_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPawX_12 = value;
		Il2CppCodeGenWriteBarrier((&____txtPawX_12), value);
	}

	inline static int32_t get_offset_of__txtPawY_13() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____txtPawY_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPawY_13() const { return ____txtPawY_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPawY_13() { return &____txtPawY_13; }
	inline void set__txtPawY_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPawY_13 = value;
		Il2CppCodeGenWriteBarrier((&____txtPawY_13), value);
	}

	inline static int32_t get_offset_of_m_matMask_14() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_matMask_14)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_matMask_14() const { return ___m_matMask_14; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_matMask_14() { return &___m_matMask_14; }
	inline void set_m_matMask_14(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_matMask_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_matMask_14), value);
	}

	inline static int32_t get_offset_of_m_fX_15() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_fX_15)); }
	inline float get_m_fX_15() const { return ___m_fX_15; }
	inline float* get_address_of_m_fX_15() { return &___m_fX_15; }
	inline void set_m_fX_15(float value)
	{
		___m_fX_15 = value;
	}

	inline static int32_t get_offset_of_m_fY_16() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_fY_16)); }
	inline float get_m_fY_16() const { return ___m_fY_16; }
	inline float* get_address_of_m_fY_16() { return &___m_fY_16; }
	inline void set_m_fY_16(float value)
	{
		___m_fY_16 = value;
	}

	inline static int32_t get_offset_of_m_fRadius_17() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_fRadius_17)); }
	inline float get_m_fRadius_17() const { return ___m_fRadius_17; }
	inline float* get_address_of_m_fRadius_17() { return &___m_fRadius_17; }
	inline void set_m_fRadius_17(float value)
	{
		___m_fRadius_17 = value;
	}

	inline static int32_t get_offset_of__uiRecommend_18() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____uiRecommend_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiRecommend_18() const { return ____uiRecommend_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiRecommend_18() { return &____uiRecommend_18; }
	inline void set__uiRecommend_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiRecommend_18 = value;
		Il2CppCodeGenWriteBarrier((&____uiRecommend_18), value);
	}

	inline static int32_t get_offset_of__lot0_19() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____lot0_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__lot0_19() const { return ____lot0_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__lot0_19() { return &____lot0_19; }
	inline void set__lot0_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____lot0_19 = value;
		Il2CppCodeGenWriteBarrier((&____lot0_19), value);
	}

	inline static int32_t get_offset_of__lot1_20() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____lot1_20)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__lot1_20() const { return ____lot1_20; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__lot1_20() { return &____lot1_20; }
	inline void set__lot1_20(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____lot1_20 = value;
		Il2CppCodeGenWriteBarrier((&____lot1_20), value);
	}

	inline static int32_t get_offset_of__lot2_21() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____lot2_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__lot2_21() const { return ____lot2_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__lot2_21() { return &____lot2_21; }
	inline void set__lot2_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____lot2_21 = value;
		Il2CppCodeGenWriteBarrier((&____lot2_21), value);
	}

	inline static int32_t get_offset_of__lot3_22() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____lot3_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__lot3_22() const { return ____lot3_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__lot3_22() { return &____lot3_22; }
	inline void set__lot3_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____lot3_22 = value;
		Il2CppCodeGenWriteBarrier((&____lot3_22), value);
	}

	inline static int32_t get_offset_of_m_dicDone_26() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_dicDone_26)); }
	inline Dictionary_2_tE758D927E90AD1ECFDE01DDC3DB458104EDA3D92 * get_m_dicDone_26() const { return ___m_dicDone_26; }
	inline Dictionary_2_tE758D927E90AD1ECFDE01DDC3DB458104EDA3D92 ** get_address_of_m_dicDone_26() { return &___m_dicDone_26; }
	inline void set_m_dicDone_26(Dictionary_2_tE758D927E90AD1ECFDE01DDC3DB458104EDA3D92 * value)
	{
		___m_dicDone_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicDone_26), value);
	}

	inline static int32_t get_offset_of__paw_27() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____paw_27)); }
	inline Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * get__paw_27() const { return ____paw_27; }
	inline Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 ** get_address_of__paw_27() { return &____paw_27; }
	inline void set__paw_27(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * value)
	{
		____paw_27 = value;
		Il2CppCodeGenWriteBarrier((&____paw_27), value);
	}

	inline static int32_t get_offset_of__faClick_28() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____faClick_28)); }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * get__faClick_28() const { return ____faClick_28; }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 ** get_address_of__faClick_28() { return &____faClick_28; }
	inline void set__faClick_28(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * value)
	{
		____faClick_28 = value;
		Il2CppCodeGenWriteBarrier((&____faClick_28), value);
	}

	inline static int32_t get_offset_of_m_eCurType_29() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_eCurType_29)); }
	inline int32_t get_m_eCurType_29() const { return ___m_eCurType_29; }
	inline int32_t* get_address_of_m_eCurType_29() { return &___m_eCurType_29; }
	inline void set_m_eCurType_29(int32_t value)
	{
		___m_eCurType_29 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_30() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_nStatus_30)); }
	inline int32_t get_m_nStatus_30() const { return ___m_nStatus_30; }
	inline int32_t* get_address_of_m_nStatus_30() { return &___m_nStatus_30; }
	inline void set_m_nStatus_30(int32_t value)
	{
		___m_nStatus_30 = value;
	}

	inline static int32_t get_offset_of_m_nSubStatus_31() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_nSubStatus_31)); }
	inline int32_t get_m_nSubStatus_31() const { return ___m_nSubStatus_31; }
	inline int32_t* get_address_of_m_nSubStatus_31() { return &___m_nSubStatus_31; }
	inline void set_m_nSubStatus_31(int32_t value)
	{
		___m_nSubStatus_31 = value;
	}

	inline static int32_t get_offset_of_m_aryIntParams_32() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_aryIntParams_32)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_aryIntParams_32() const { return ___m_aryIntParams_32; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_aryIntParams_32() { return &___m_aryIntParams_32; }
	inline void set_m_aryIntParams_32(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_aryIntParams_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryIntParams_32), value);
	}

	inline static int32_t get_offset_of_m_aryFloatParams_33() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_aryFloatParams_33)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryFloatParams_33() const { return ___m_aryFloatParams_33; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryFloatParams_33() { return &___m_aryFloatParams_33; }
	inline void set_m_aryFloatParams_33(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryFloatParams_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryFloatParams_33), value);
	}

	inline static int32_t get_offset_of_m_dicFloatParams_34() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_dicFloatParams_34)); }
	inline Dictionary_2_t89F484A77FD6673A172CE80BD0A37031FBD1FD6A * get_m_dicFloatParams_34() const { return ___m_dicFloatParams_34; }
	inline Dictionary_2_t89F484A77FD6673A172CE80BD0A37031FBD1FD6A ** get_address_of_m_dicFloatParams_34() { return &___m_dicFloatParams_34; }
	inline void set_m_dicFloatParams_34(Dictionary_2_t89F484A77FD6673A172CE80BD0A37031FBD1FD6A * value)
	{
		___m_dicFloatParams_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicFloatParams_34), value);
	}

	inline static int32_t get_offset_of_m_fTimeElapse_35() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_fTimeElapse_35)); }
	inline float get_m_fTimeElapse_35() const { return ___m_fTimeElapse_35; }
	inline float* get_address_of_m_fTimeElapse_35() { return &___m_fTimeElapse_35; }
	inline void set_m_fTimeElapse_35(float value)
	{
		___m_fTimeElapse_35 = value;
	}

	inline static int32_t get_offset_of_m_bMasking_36() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_bMasking_36)); }
	inline bool get_m_bMasking_36() const { return ___m_bMasking_36; }
	inline bool* get_address_of_m_bMasking_36() { return &___m_bMasking_36; }
	inline void set_m_bMasking_36(bool value)
	{
		___m_bMasking_36 = value;
	}
};

struct FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields
{
public:
	// UnityEngine.Vector3 FreshGuide::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_23;
	// UnityEngine.Vector3 FreshGuide::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_24;
	// FreshGuide FreshGuide::s_Instance
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * ___s_Instance_25;

public:
	inline static int32_t get_offset_of_vecTempPos_23() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields, ___vecTempPos_23)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_23() const { return ___vecTempPos_23; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_23() { return &___vecTempPos_23; }
	inline void set_vecTempPos_23(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_23 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_24() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields, ___vecTempScale_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_24() const { return ___vecTempScale_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_24() { return &___vecTempScale_24; }
	inline void set_vecTempScale_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_24 = value;
	}

	inline static int32_t get_offset_of_s_Instance_25() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields, ___s_Instance_25)); }
	inline FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * get_s_Instance_25() const { return ___s_Instance_25; }
	inline FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 ** get_address_of_s_Instance_25() { return &___s_Instance_25; }
	inline void set_s_Instance_25(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * value)
	{
		___s_Instance_25 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRESHGUIDE_T840B865F5BBA1373DC91F27C5939E88FD6095DD3_H
#ifndef GYRO_T3FD88CAC3210A8FEDD89E4D27329A678A2254BF3_H
#define GYRO_T3FD88CAC3210A8FEDD89E4D27329A678A2254BF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gyro
struct  Gyro_t3FD88CAC3210A8FEDD89E4D27329A678A2254BF3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Gyroscope Gyro::go
	Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC * ___go_5;
	// System.Boolean Gyro::gyinfo
	bool ___gyinfo_6;

public:
	inline static int32_t get_offset_of_go_5() { return static_cast<int32_t>(offsetof(Gyro_t3FD88CAC3210A8FEDD89E4D27329A678A2254BF3, ___go_5)); }
	inline Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC * get_go_5() const { return ___go_5; }
	inline Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC ** get_address_of_go_5() { return &___go_5; }
	inline void set_go_5(Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC * value)
	{
		___go_5 = value;
		Il2CppCodeGenWriteBarrier((&___go_5), value);
	}

	inline static int32_t get_offset_of_gyinfo_6() { return static_cast<int32_t>(offsetof(Gyro_t3FD88CAC3210A8FEDD89E4D27329A678A2254BF3, ___gyinfo_6)); }
	inline bool get_gyinfo_6() const { return ___gyinfo_6; }
	inline bool* get_address_of_gyinfo_6() { return &___gyinfo_6; }
	inline void set_gyinfo_6(bool value)
	{
		___gyinfo_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYRO_T3FD88CAC3210A8FEDD89E4D27329A678A2254BF3_H
#ifndef ITEM_T1FD274D610B07F7F6E6AA093A93FB9E32210D36E_H
#define ITEM_T1FD274D610B07F7F6E6AA093A93FB9E32210D36E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Item
struct  Item_t1FD274D610B07F7F6E6AA093A93FB9E32210D36E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEM_T1FD274D610B07F7F6E6AA093A93FB9E32210D36E_H
#ifndef ITEMSYSTEM_TBFA2A8170ACDDEB8C5F9B9F059761900F5668677_H
#define ITEMSYSTEM_TBFA2A8170ACDDEB8C5F9B9F059761900F5668677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemSystem
struct  ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Sprite[] ItemSystem::m_aryItemIconSprite_Cash
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryItemIconSprite_Cash_5;
	// UnityEngine.Sprite[] ItemSystem::m_aryItemIconSprite_CoinPromote
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryItemIconSprite_CoinPromote_6;
	// UnityEngine.Sprite[] ItemSystem::m_aryItemIconSprite_SpeedAccelerate
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryItemIconSprite_SpeedAccelerate_7;
	// UnityEngine.Sprite[] ItemSystem::m_aryItemIconSprite_SkillPoint
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryItemIconSprite_SkillPoint_8;
	// System.Collections.Generic.Dictionary`2<ShoppinMall/eItemType,UnityEngine.Sprite[]> ItemSystem::m_dicItemIconSpritesList
	Dictionary_2_t32BE7FCA071A61538F1D015AB5D7D62E70FA4CAA * ___m_dicItemIconSpritesList_9;
	// UIItemBagContainer ItemSystem::_theBag
	UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6 * ____theBag_10;
	// UnityEngine.Sprite[] ItemSystem::m_aryItemIcon
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryItemIcon_11;
	// UIShoppinAndItemCounter[] ItemSystem::m_aryShoppinCounter
	UIShoppinAndItemCounterU5BU5D_t414AC543955220267F28047390018B8E86CB56C8* ___m_aryShoppinCounter_12;
	// UnityEngine.GameObject ItemSystem::_containerRecycledItems
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerRecycledItems_14;
	// UnityEngine.GameObject ItemSystem::_panelItemBag
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelItemBag_15;
	// UnityEngine.GameObject ItemSystem::_containerUsing
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerUsing_16;
	// UnityEngine.GameObject ItemSystem::_containerNotUsing
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerNotUsing_17;
	// System.Collections.Generic.List`1<UIItemInBag> ItemSystem::m_lstUsingItems
	List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB * ___m_lstUsingItems_18;
	// System.Collections.Generic.List`1<UIItem> ItemSystem::m_lstNowUsingItems
	List_1_t7302F8A5155D7A00D77040895186DF4F7D8A896D * ___m_lstNowUsingItems_19;
	// ItemSystem/sItemConfig ItemSystem::tempItemConfig
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C  ___tempItemConfig_20;
	// System.Collections.Generic.Dictionary`2<System.Int32,ItemSystem/sItemConfig> ItemSystem::m_dicItemConfig
	Dictionary_2_tE48FC1442A682263A6270B86D3E7E0A91D86F986 * ___m_dicItemConfig_21;
	// System.Boolean ItemSystem::m_bItemConfigLoaded
	bool ___m_bItemConfigLoaded_22;
	// System.Single ItemSystem::m_fTimeElapse
	float ___m_fTimeElapse_23;
	// System.Boolean ItemSystem::m_bIsVisible
	bool ___m_bIsVisible_24;
	// System.Single ItemSystem::m_fAutomobileSpeedAccelerate
	float ___m_fAutomobileSpeedAccelerate_25;
	// System.Collections.Generic.List`1<UIShoppinAndItemCounter> ItemSystem::m_lstUsingItems_New
	List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * ___m_lstUsingItems_New_26;
	// System.Collections.Generic.List`1<UIShoppinAndItemCounter> ItemSystem::lstTemp
	List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * ___lstTemp_27;

public:
	inline static int32_t get_offset_of_m_aryItemIconSprite_Cash_5() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_aryItemIconSprite_Cash_5)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryItemIconSprite_Cash_5() const { return ___m_aryItemIconSprite_Cash_5; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryItemIconSprite_Cash_5() { return &___m_aryItemIconSprite_Cash_5; }
	inline void set_m_aryItemIconSprite_Cash_5(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryItemIconSprite_Cash_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryItemIconSprite_Cash_5), value);
	}

	inline static int32_t get_offset_of_m_aryItemIconSprite_CoinPromote_6() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_aryItemIconSprite_CoinPromote_6)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryItemIconSprite_CoinPromote_6() const { return ___m_aryItemIconSprite_CoinPromote_6; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryItemIconSprite_CoinPromote_6() { return &___m_aryItemIconSprite_CoinPromote_6; }
	inline void set_m_aryItemIconSprite_CoinPromote_6(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryItemIconSprite_CoinPromote_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryItemIconSprite_CoinPromote_6), value);
	}

	inline static int32_t get_offset_of_m_aryItemIconSprite_SpeedAccelerate_7() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_aryItemIconSprite_SpeedAccelerate_7)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryItemIconSprite_SpeedAccelerate_7() const { return ___m_aryItemIconSprite_SpeedAccelerate_7; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryItemIconSprite_SpeedAccelerate_7() { return &___m_aryItemIconSprite_SpeedAccelerate_7; }
	inline void set_m_aryItemIconSprite_SpeedAccelerate_7(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryItemIconSprite_SpeedAccelerate_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryItemIconSprite_SpeedAccelerate_7), value);
	}

	inline static int32_t get_offset_of_m_aryItemIconSprite_SkillPoint_8() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_aryItemIconSprite_SkillPoint_8)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryItemIconSprite_SkillPoint_8() const { return ___m_aryItemIconSprite_SkillPoint_8; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryItemIconSprite_SkillPoint_8() { return &___m_aryItemIconSprite_SkillPoint_8; }
	inline void set_m_aryItemIconSprite_SkillPoint_8(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryItemIconSprite_SkillPoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryItemIconSprite_SkillPoint_8), value);
	}

	inline static int32_t get_offset_of_m_dicItemIconSpritesList_9() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_dicItemIconSpritesList_9)); }
	inline Dictionary_2_t32BE7FCA071A61538F1D015AB5D7D62E70FA4CAA * get_m_dicItemIconSpritesList_9() const { return ___m_dicItemIconSpritesList_9; }
	inline Dictionary_2_t32BE7FCA071A61538F1D015AB5D7D62E70FA4CAA ** get_address_of_m_dicItemIconSpritesList_9() { return &___m_dicItemIconSpritesList_9; }
	inline void set_m_dicItemIconSpritesList_9(Dictionary_2_t32BE7FCA071A61538F1D015AB5D7D62E70FA4CAA * value)
	{
		___m_dicItemIconSpritesList_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicItemIconSpritesList_9), value);
	}

	inline static int32_t get_offset_of__theBag_10() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ____theBag_10)); }
	inline UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6 * get__theBag_10() const { return ____theBag_10; }
	inline UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6 ** get_address_of__theBag_10() { return &____theBag_10; }
	inline void set__theBag_10(UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6 * value)
	{
		____theBag_10 = value;
		Il2CppCodeGenWriteBarrier((&____theBag_10), value);
	}

	inline static int32_t get_offset_of_m_aryItemIcon_11() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_aryItemIcon_11)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryItemIcon_11() const { return ___m_aryItemIcon_11; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryItemIcon_11() { return &___m_aryItemIcon_11; }
	inline void set_m_aryItemIcon_11(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryItemIcon_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryItemIcon_11), value);
	}

	inline static int32_t get_offset_of_m_aryShoppinCounter_12() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_aryShoppinCounter_12)); }
	inline UIShoppinAndItemCounterU5BU5D_t414AC543955220267F28047390018B8E86CB56C8* get_m_aryShoppinCounter_12() const { return ___m_aryShoppinCounter_12; }
	inline UIShoppinAndItemCounterU5BU5D_t414AC543955220267F28047390018B8E86CB56C8** get_address_of_m_aryShoppinCounter_12() { return &___m_aryShoppinCounter_12; }
	inline void set_m_aryShoppinCounter_12(UIShoppinAndItemCounterU5BU5D_t414AC543955220267F28047390018B8E86CB56C8* value)
	{
		___m_aryShoppinCounter_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryShoppinCounter_12), value);
	}

	inline static int32_t get_offset_of__containerRecycledItems_14() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ____containerRecycledItems_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerRecycledItems_14() const { return ____containerRecycledItems_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerRecycledItems_14() { return &____containerRecycledItems_14; }
	inline void set__containerRecycledItems_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerRecycledItems_14 = value;
		Il2CppCodeGenWriteBarrier((&____containerRecycledItems_14), value);
	}

	inline static int32_t get_offset_of__panelItemBag_15() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ____panelItemBag_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelItemBag_15() const { return ____panelItemBag_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelItemBag_15() { return &____panelItemBag_15; }
	inline void set__panelItemBag_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelItemBag_15 = value;
		Il2CppCodeGenWriteBarrier((&____panelItemBag_15), value);
	}

	inline static int32_t get_offset_of__containerUsing_16() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ____containerUsing_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerUsing_16() const { return ____containerUsing_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerUsing_16() { return &____containerUsing_16; }
	inline void set__containerUsing_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerUsing_16 = value;
		Il2CppCodeGenWriteBarrier((&____containerUsing_16), value);
	}

	inline static int32_t get_offset_of__containerNotUsing_17() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ____containerNotUsing_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerNotUsing_17() const { return ____containerNotUsing_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerNotUsing_17() { return &____containerNotUsing_17; }
	inline void set__containerNotUsing_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerNotUsing_17 = value;
		Il2CppCodeGenWriteBarrier((&____containerNotUsing_17), value);
	}

	inline static int32_t get_offset_of_m_lstUsingItems_18() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_lstUsingItems_18)); }
	inline List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB * get_m_lstUsingItems_18() const { return ___m_lstUsingItems_18; }
	inline List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB ** get_address_of_m_lstUsingItems_18() { return &___m_lstUsingItems_18; }
	inline void set_m_lstUsingItems_18(List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB * value)
	{
		___m_lstUsingItems_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstUsingItems_18), value);
	}

	inline static int32_t get_offset_of_m_lstNowUsingItems_19() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_lstNowUsingItems_19)); }
	inline List_1_t7302F8A5155D7A00D77040895186DF4F7D8A896D * get_m_lstNowUsingItems_19() const { return ___m_lstNowUsingItems_19; }
	inline List_1_t7302F8A5155D7A00D77040895186DF4F7D8A896D ** get_address_of_m_lstNowUsingItems_19() { return &___m_lstNowUsingItems_19; }
	inline void set_m_lstNowUsingItems_19(List_1_t7302F8A5155D7A00D77040895186DF4F7D8A896D * value)
	{
		___m_lstNowUsingItems_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstNowUsingItems_19), value);
	}

	inline static int32_t get_offset_of_tempItemConfig_20() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___tempItemConfig_20)); }
	inline sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C  get_tempItemConfig_20() const { return ___tempItemConfig_20; }
	inline sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C * get_address_of_tempItemConfig_20() { return &___tempItemConfig_20; }
	inline void set_tempItemConfig_20(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C  value)
	{
		___tempItemConfig_20 = value;
	}

	inline static int32_t get_offset_of_m_dicItemConfig_21() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_dicItemConfig_21)); }
	inline Dictionary_2_tE48FC1442A682263A6270B86D3E7E0A91D86F986 * get_m_dicItemConfig_21() const { return ___m_dicItemConfig_21; }
	inline Dictionary_2_tE48FC1442A682263A6270B86D3E7E0A91D86F986 ** get_address_of_m_dicItemConfig_21() { return &___m_dicItemConfig_21; }
	inline void set_m_dicItemConfig_21(Dictionary_2_tE48FC1442A682263A6270B86D3E7E0A91D86F986 * value)
	{
		___m_dicItemConfig_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicItemConfig_21), value);
	}

	inline static int32_t get_offset_of_m_bItemConfigLoaded_22() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_bItemConfigLoaded_22)); }
	inline bool get_m_bItemConfigLoaded_22() const { return ___m_bItemConfigLoaded_22; }
	inline bool* get_address_of_m_bItemConfigLoaded_22() { return &___m_bItemConfigLoaded_22; }
	inline void set_m_bItemConfigLoaded_22(bool value)
	{
		___m_bItemConfigLoaded_22 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_23() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_fTimeElapse_23)); }
	inline float get_m_fTimeElapse_23() const { return ___m_fTimeElapse_23; }
	inline float* get_address_of_m_fTimeElapse_23() { return &___m_fTimeElapse_23; }
	inline void set_m_fTimeElapse_23(float value)
	{
		___m_fTimeElapse_23 = value;
	}

	inline static int32_t get_offset_of_m_bIsVisible_24() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_bIsVisible_24)); }
	inline bool get_m_bIsVisible_24() const { return ___m_bIsVisible_24; }
	inline bool* get_address_of_m_bIsVisible_24() { return &___m_bIsVisible_24; }
	inline void set_m_bIsVisible_24(bool value)
	{
		___m_bIsVisible_24 = value;
	}

	inline static int32_t get_offset_of_m_fAutomobileSpeedAccelerate_25() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_fAutomobileSpeedAccelerate_25)); }
	inline float get_m_fAutomobileSpeedAccelerate_25() const { return ___m_fAutomobileSpeedAccelerate_25; }
	inline float* get_address_of_m_fAutomobileSpeedAccelerate_25() { return &___m_fAutomobileSpeedAccelerate_25; }
	inline void set_m_fAutomobileSpeedAccelerate_25(float value)
	{
		___m_fAutomobileSpeedAccelerate_25 = value;
	}

	inline static int32_t get_offset_of_m_lstUsingItems_New_26() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___m_lstUsingItems_New_26)); }
	inline List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * get_m_lstUsingItems_New_26() const { return ___m_lstUsingItems_New_26; }
	inline List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B ** get_address_of_m_lstUsingItems_New_26() { return &___m_lstUsingItems_New_26; }
	inline void set_m_lstUsingItems_New_26(List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * value)
	{
		___m_lstUsingItems_New_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstUsingItems_New_26), value);
	}

	inline static int32_t get_offset_of_lstTemp_27() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677, ___lstTemp_27)); }
	inline List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * get_lstTemp_27() const { return ___lstTemp_27; }
	inline List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B ** get_address_of_lstTemp_27() { return &___lstTemp_27; }
	inline void set_lstTemp_27(List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * value)
	{
		___lstTemp_27 = value;
		Il2CppCodeGenWriteBarrier((&___lstTemp_27), value);
	}
};

struct ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677_StaticFields
{
public:
	// UnityEngine.Vector3 ItemSystem::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_4;
	// ItemSystem ItemSystem::s_Instance
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677 * ___s_Instance_13;

public:
	inline static int32_t get_offset_of_vecTempScale_4() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677_StaticFields, ___vecTempScale_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_4() const { return ___vecTempScale_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_4() { return &___vecTempScale_4; }
	inline void set_vecTempScale_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_4 = value;
	}

	inline static int32_t get_offset_of_s_Instance_13() { return static_cast<int32_t>(offsetof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677_StaticFields, ___s_Instance_13)); }
	inline ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677 * get_s_Instance_13() const { return ___s_Instance_13; }
	inline ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677 ** get_address_of_s_Instance_13() { return &___s_Instance_13; }
	inline void set_s_Instance_13(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677 * value)
	{
		___s_Instance_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMSYSTEM_TBFA2A8170ACDDEB8C5F9B9F059761900F5668677_H
#ifndef JTDHSCENEMANAGER_TEE575F733C779BF24643585515AFFC7AFCC97108_H
#define JTDHSCENEMANAGER_TEE575F733C779BF24643585515AFFC7AFCC97108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JTDHSceneManager
struct  JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single JTDHSceneManager::LOT_X_GAP
	float ___LOT_X_GAP_4;
	// System.Single JTDHSceneManager::LOT_Y_GAP
	float ___LOT_Y_GAP_5;
	// UnityEngine.GameObject JTDHSceneManager::_containerLocations
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerLocations_8;
	// System.Int32 JTDHSceneManager::m_nSceneId
	int32_t ___m_nSceneId_12;
	// UnityEngine.Color[] JTDHSceneManager::m_aryEnvironmentColor
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___m_aryEnvironmentColor_13;
	// UnityEngine.Color[] JTDHSceneManager::m_aryEnvironmentColor_Temp
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___m_aryEnvironmentColor_Temp_14;
	// UnityEngine.GameObject JTDHSceneManager::m_preSnowFlake
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preSnowFlake_15;
	// UnityEngine.Sprite[] JTDHSceneManager::m_aryMainBg
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryMainBg_16;
	// UnityEngine.Sprite[] JTDHSceneManager::m_aryLotSpr
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryLotSpr_17;
	// UnityEngine.Sprite[] JTDHSceneManager::m_aryAirline
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAirline_18;
	// UnityEngine.Sprite[] JTDHSceneManager::m_aryLot
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryLot_19;
	// UnityEngine.Color[] JTDHSceneManager::m_aryLotMaskColor
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___m_aryLotMaskColor_20;
	// UnityEngine.GameObject[] JTDHSceneManager::m_arySceneAccessoriesContainers
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_arySceneAccessoriesContainers_21;
	// UnityEngine.GameObject JTDHSceneManager::m_containerSnowFlakes
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_containerSnowFlakes_22;
	// UnityEngine.SpriteRenderer JTDHSceneManager::_srMainBg
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srMainBg_23;
	// UnityEngine.SpriteRenderer JTDHSceneManager::_srAirline
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srAirline_24;
	// System.Single JTDHSceneManager::m_fSnowMoveSpeed
	float ___m_fSnowMoveSpeed_25;
	// System.Single JTDHSceneManager::m_fSnowMoveStartPosLeft
	float ___m_fSnowMoveStartPosLeft_26;
	// System.Single JTDHSceneManager::m_fSnowMoveStartPosRight
	float ___m_fSnowMoveStartPosRight_27;
	// System.Single JTDHSceneManager::m_fSnowMoveEndPosHigh
	float ___m_fSnowMoveEndPosHigh_28;
	// System.Single JTDHSceneManager::m_fSnowMoveEndPosLow
	float ___m_fSnowMoveEndPosLow_29;
	// System.Single JTDHSceneManager::m_fSnowGenerateInterval
	float ___m_fSnowGenerateInterval_30;
	// System.Single JTDHSceneManager::m_fSnowStartPosY
	float ___m_fSnowStartPosY_31;
	// System.Single JTDHSceneManager::m_fSnowBeginFadePosY
	float ___m_fSnowBeginFadePosY_32;
	// System.Single JTDHSceneManager::m_fSnowStartPosX
	float ___m_fSnowStartPosX_33;
	// System.Single JTDHSceneManager::m_fSnowEndPosY
	float ___m_fSnowEndPosY_34;
	// System.Single JTDHSceneManager::m_fSnowDropMovementPerFrame
	float ___m_fSnowDropMovementPerFrame_35;
	// System.Single JTDHSceneManager::m_fSandStormMoveSpeed
	float ___m_fSandStormMoveSpeed_36;
	// System.Single JTDHSceneManager::m_fSandStormMoveStartPos
	float ___m_fSandStormMoveStartPos_37;
	// System.Single JTDHSceneManager::m_fSandStormMoveEndPos
	float ___m_fSandStormMoveEndPos_38;
	// UnityEngine.SpriteRenderer[] JTDHSceneManager::m_arySandStorm
	SpriteRendererU5BU5D_tA8FE422195BB4C0A7ABA1BFC136CB8D1F174FA32* ___m_arySandStorm_39;
	// System.Single JTDHSceneManager::m_fSandStormMovementPerFrame
	float ___m_fSandStormMovementPerFrame_40;
	// System.Single JTDHSceneManager::m_fAdjustValue
	float ___m_fAdjustValue_41;
	// System.Single JTDHSceneManager::m_fAdjustShit
	float ___m_fAdjustShit_42;
	// System.Single JTDHSceneManager::m_fSnowGenerateTimeElapse
	float ___m_fSnowGenerateTimeElapse_43;
	// System.Collections.Generic.List`1<SnowFlake> JTDHSceneManager::m_lstSnowFlakes
	List_1_t0EA6D74B5B5964105FF9E918795F54CBDC306BD9 * ___m_lstSnowFlakes_44;

public:
	inline static int32_t get_offset_of_LOT_X_GAP_4() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___LOT_X_GAP_4)); }
	inline float get_LOT_X_GAP_4() const { return ___LOT_X_GAP_4; }
	inline float* get_address_of_LOT_X_GAP_4() { return &___LOT_X_GAP_4; }
	inline void set_LOT_X_GAP_4(float value)
	{
		___LOT_X_GAP_4 = value;
	}

	inline static int32_t get_offset_of_LOT_Y_GAP_5() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___LOT_Y_GAP_5)); }
	inline float get_LOT_Y_GAP_5() const { return ___LOT_Y_GAP_5; }
	inline float* get_address_of_LOT_Y_GAP_5() { return &___LOT_Y_GAP_5; }
	inline void set_LOT_Y_GAP_5(float value)
	{
		___LOT_Y_GAP_5 = value;
	}

	inline static int32_t get_offset_of__containerLocations_8() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ____containerLocations_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerLocations_8() const { return ____containerLocations_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerLocations_8() { return &____containerLocations_8; }
	inline void set__containerLocations_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerLocations_8 = value;
		Il2CppCodeGenWriteBarrier((&____containerLocations_8), value);
	}

	inline static int32_t get_offset_of_m_nSceneId_12() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_nSceneId_12)); }
	inline int32_t get_m_nSceneId_12() const { return ___m_nSceneId_12; }
	inline int32_t* get_address_of_m_nSceneId_12() { return &___m_nSceneId_12; }
	inline void set_m_nSceneId_12(int32_t value)
	{
		___m_nSceneId_12 = value;
	}

	inline static int32_t get_offset_of_m_aryEnvironmentColor_13() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_aryEnvironmentColor_13)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_m_aryEnvironmentColor_13() const { return ___m_aryEnvironmentColor_13; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_m_aryEnvironmentColor_13() { return &___m_aryEnvironmentColor_13; }
	inline void set_m_aryEnvironmentColor_13(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___m_aryEnvironmentColor_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryEnvironmentColor_13), value);
	}

	inline static int32_t get_offset_of_m_aryEnvironmentColor_Temp_14() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_aryEnvironmentColor_Temp_14)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_m_aryEnvironmentColor_Temp_14() const { return ___m_aryEnvironmentColor_Temp_14; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_m_aryEnvironmentColor_Temp_14() { return &___m_aryEnvironmentColor_Temp_14; }
	inline void set_m_aryEnvironmentColor_Temp_14(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___m_aryEnvironmentColor_Temp_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryEnvironmentColor_Temp_14), value);
	}

	inline static int32_t get_offset_of_m_preSnowFlake_15() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_preSnowFlake_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preSnowFlake_15() const { return ___m_preSnowFlake_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preSnowFlake_15() { return &___m_preSnowFlake_15; }
	inline void set_m_preSnowFlake_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preSnowFlake_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_preSnowFlake_15), value);
	}

	inline static int32_t get_offset_of_m_aryMainBg_16() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_aryMainBg_16)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryMainBg_16() const { return ___m_aryMainBg_16; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryMainBg_16() { return &___m_aryMainBg_16; }
	inline void set_m_aryMainBg_16(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryMainBg_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMainBg_16), value);
	}

	inline static int32_t get_offset_of_m_aryLotSpr_17() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_aryLotSpr_17)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryLotSpr_17() const { return ___m_aryLotSpr_17; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryLotSpr_17() { return &___m_aryLotSpr_17; }
	inline void set_m_aryLotSpr_17(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryLotSpr_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryLotSpr_17), value);
	}

	inline static int32_t get_offset_of_m_aryAirline_18() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_aryAirline_18)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAirline_18() const { return ___m_aryAirline_18; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAirline_18() { return &___m_aryAirline_18; }
	inline void set_m_aryAirline_18(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAirline_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAirline_18), value);
	}

	inline static int32_t get_offset_of_m_aryLot_19() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_aryLot_19)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryLot_19() const { return ___m_aryLot_19; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryLot_19() { return &___m_aryLot_19; }
	inline void set_m_aryLot_19(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryLot_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryLot_19), value);
	}

	inline static int32_t get_offset_of_m_aryLotMaskColor_20() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_aryLotMaskColor_20)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_m_aryLotMaskColor_20() const { return ___m_aryLotMaskColor_20; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_m_aryLotMaskColor_20() { return &___m_aryLotMaskColor_20; }
	inline void set_m_aryLotMaskColor_20(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___m_aryLotMaskColor_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryLotMaskColor_20), value);
	}

	inline static int32_t get_offset_of_m_arySceneAccessoriesContainers_21() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_arySceneAccessoriesContainers_21)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_arySceneAccessoriesContainers_21() const { return ___m_arySceneAccessoriesContainers_21; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_arySceneAccessoriesContainers_21() { return &___m_arySceneAccessoriesContainers_21; }
	inline void set_m_arySceneAccessoriesContainers_21(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_arySceneAccessoriesContainers_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySceneAccessoriesContainers_21), value);
	}

	inline static int32_t get_offset_of_m_containerSnowFlakes_22() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_containerSnowFlakes_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_containerSnowFlakes_22() const { return ___m_containerSnowFlakes_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_containerSnowFlakes_22() { return &___m_containerSnowFlakes_22; }
	inline void set_m_containerSnowFlakes_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_containerSnowFlakes_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_containerSnowFlakes_22), value);
	}

	inline static int32_t get_offset_of__srMainBg_23() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ____srMainBg_23)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srMainBg_23() const { return ____srMainBg_23; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srMainBg_23() { return &____srMainBg_23; }
	inline void set__srMainBg_23(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srMainBg_23 = value;
		Il2CppCodeGenWriteBarrier((&____srMainBg_23), value);
	}

	inline static int32_t get_offset_of__srAirline_24() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ____srAirline_24)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srAirline_24() const { return ____srAirline_24; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srAirline_24() { return &____srAirline_24; }
	inline void set__srAirline_24(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srAirline_24 = value;
		Il2CppCodeGenWriteBarrier((&____srAirline_24), value);
	}

	inline static int32_t get_offset_of_m_fSnowMoveSpeed_25() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSnowMoveSpeed_25)); }
	inline float get_m_fSnowMoveSpeed_25() const { return ___m_fSnowMoveSpeed_25; }
	inline float* get_address_of_m_fSnowMoveSpeed_25() { return &___m_fSnowMoveSpeed_25; }
	inline void set_m_fSnowMoveSpeed_25(float value)
	{
		___m_fSnowMoveSpeed_25 = value;
	}

	inline static int32_t get_offset_of_m_fSnowMoveStartPosLeft_26() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSnowMoveStartPosLeft_26)); }
	inline float get_m_fSnowMoveStartPosLeft_26() const { return ___m_fSnowMoveStartPosLeft_26; }
	inline float* get_address_of_m_fSnowMoveStartPosLeft_26() { return &___m_fSnowMoveStartPosLeft_26; }
	inline void set_m_fSnowMoveStartPosLeft_26(float value)
	{
		___m_fSnowMoveStartPosLeft_26 = value;
	}

	inline static int32_t get_offset_of_m_fSnowMoveStartPosRight_27() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSnowMoveStartPosRight_27)); }
	inline float get_m_fSnowMoveStartPosRight_27() const { return ___m_fSnowMoveStartPosRight_27; }
	inline float* get_address_of_m_fSnowMoveStartPosRight_27() { return &___m_fSnowMoveStartPosRight_27; }
	inline void set_m_fSnowMoveStartPosRight_27(float value)
	{
		___m_fSnowMoveStartPosRight_27 = value;
	}

	inline static int32_t get_offset_of_m_fSnowMoveEndPosHigh_28() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSnowMoveEndPosHigh_28)); }
	inline float get_m_fSnowMoveEndPosHigh_28() const { return ___m_fSnowMoveEndPosHigh_28; }
	inline float* get_address_of_m_fSnowMoveEndPosHigh_28() { return &___m_fSnowMoveEndPosHigh_28; }
	inline void set_m_fSnowMoveEndPosHigh_28(float value)
	{
		___m_fSnowMoveEndPosHigh_28 = value;
	}

	inline static int32_t get_offset_of_m_fSnowMoveEndPosLow_29() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSnowMoveEndPosLow_29)); }
	inline float get_m_fSnowMoveEndPosLow_29() const { return ___m_fSnowMoveEndPosLow_29; }
	inline float* get_address_of_m_fSnowMoveEndPosLow_29() { return &___m_fSnowMoveEndPosLow_29; }
	inline void set_m_fSnowMoveEndPosLow_29(float value)
	{
		___m_fSnowMoveEndPosLow_29 = value;
	}

	inline static int32_t get_offset_of_m_fSnowGenerateInterval_30() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSnowGenerateInterval_30)); }
	inline float get_m_fSnowGenerateInterval_30() const { return ___m_fSnowGenerateInterval_30; }
	inline float* get_address_of_m_fSnowGenerateInterval_30() { return &___m_fSnowGenerateInterval_30; }
	inline void set_m_fSnowGenerateInterval_30(float value)
	{
		___m_fSnowGenerateInterval_30 = value;
	}

	inline static int32_t get_offset_of_m_fSnowStartPosY_31() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSnowStartPosY_31)); }
	inline float get_m_fSnowStartPosY_31() const { return ___m_fSnowStartPosY_31; }
	inline float* get_address_of_m_fSnowStartPosY_31() { return &___m_fSnowStartPosY_31; }
	inline void set_m_fSnowStartPosY_31(float value)
	{
		___m_fSnowStartPosY_31 = value;
	}

	inline static int32_t get_offset_of_m_fSnowBeginFadePosY_32() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSnowBeginFadePosY_32)); }
	inline float get_m_fSnowBeginFadePosY_32() const { return ___m_fSnowBeginFadePosY_32; }
	inline float* get_address_of_m_fSnowBeginFadePosY_32() { return &___m_fSnowBeginFadePosY_32; }
	inline void set_m_fSnowBeginFadePosY_32(float value)
	{
		___m_fSnowBeginFadePosY_32 = value;
	}

	inline static int32_t get_offset_of_m_fSnowStartPosX_33() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSnowStartPosX_33)); }
	inline float get_m_fSnowStartPosX_33() const { return ___m_fSnowStartPosX_33; }
	inline float* get_address_of_m_fSnowStartPosX_33() { return &___m_fSnowStartPosX_33; }
	inline void set_m_fSnowStartPosX_33(float value)
	{
		___m_fSnowStartPosX_33 = value;
	}

	inline static int32_t get_offset_of_m_fSnowEndPosY_34() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSnowEndPosY_34)); }
	inline float get_m_fSnowEndPosY_34() const { return ___m_fSnowEndPosY_34; }
	inline float* get_address_of_m_fSnowEndPosY_34() { return &___m_fSnowEndPosY_34; }
	inline void set_m_fSnowEndPosY_34(float value)
	{
		___m_fSnowEndPosY_34 = value;
	}

	inline static int32_t get_offset_of_m_fSnowDropMovementPerFrame_35() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSnowDropMovementPerFrame_35)); }
	inline float get_m_fSnowDropMovementPerFrame_35() const { return ___m_fSnowDropMovementPerFrame_35; }
	inline float* get_address_of_m_fSnowDropMovementPerFrame_35() { return &___m_fSnowDropMovementPerFrame_35; }
	inline void set_m_fSnowDropMovementPerFrame_35(float value)
	{
		___m_fSnowDropMovementPerFrame_35 = value;
	}

	inline static int32_t get_offset_of_m_fSandStormMoveSpeed_36() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSandStormMoveSpeed_36)); }
	inline float get_m_fSandStormMoveSpeed_36() const { return ___m_fSandStormMoveSpeed_36; }
	inline float* get_address_of_m_fSandStormMoveSpeed_36() { return &___m_fSandStormMoveSpeed_36; }
	inline void set_m_fSandStormMoveSpeed_36(float value)
	{
		___m_fSandStormMoveSpeed_36 = value;
	}

	inline static int32_t get_offset_of_m_fSandStormMoveStartPos_37() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSandStormMoveStartPos_37)); }
	inline float get_m_fSandStormMoveStartPos_37() const { return ___m_fSandStormMoveStartPos_37; }
	inline float* get_address_of_m_fSandStormMoveStartPos_37() { return &___m_fSandStormMoveStartPos_37; }
	inline void set_m_fSandStormMoveStartPos_37(float value)
	{
		___m_fSandStormMoveStartPos_37 = value;
	}

	inline static int32_t get_offset_of_m_fSandStormMoveEndPos_38() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSandStormMoveEndPos_38)); }
	inline float get_m_fSandStormMoveEndPos_38() const { return ___m_fSandStormMoveEndPos_38; }
	inline float* get_address_of_m_fSandStormMoveEndPos_38() { return &___m_fSandStormMoveEndPos_38; }
	inline void set_m_fSandStormMoveEndPos_38(float value)
	{
		___m_fSandStormMoveEndPos_38 = value;
	}

	inline static int32_t get_offset_of_m_arySandStorm_39() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_arySandStorm_39)); }
	inline SpriteRendererU5BU5D_tA8FE422195BB4C0A7ABA1BFC136CB8D1F174FA32* get_m_arySandStorm_39() const { return ___m_arySandStorm_39; }
	inline SpriteRendererU5BU5D_tA8FE422195BB4C0A7ABA1BFC136CB8D1F174FA32** get_address_of_m_arySandStorm_39() { return &___m_arySandStorm_39; }
	inline void set_m_arySandStorm_39(SpriteRendererU5BU5D_tA8FE422195BB4C0A7ABA1BFC136CB8D1F174FA32* value)
	{
		___m_arySandStorm_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySandStorm_39), value);
	}

	inline static int32_t get_offset_of_m_fSandStormMovementPerFrame_40() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSandStormMovementPerFrame_40)); }
	inline float get_m_fSandStormMovementPerFrame_40() const { return ___m_fSandStormMovementPerFrame_40; }
	inline float* get_address_of_m_fSandStormMovementPerFrame_40() { return &___m_fSandStormMovementPerFrame_40; }
	inline void set_m_fSandStormMovementPerFrame_40(float value)
	{
		___m_fSandStormMovementPerFrame_40 = value;
	}

	inline static int32_t get_offset_of_m_fAdjustValue_41() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fAdjustValue_41)); }
	inline float get_m_fAdjustValue_41() const { return ___m_fAdjustValue_41; }
	inline float* get_address_of_m_fAdjustValue_41() { return &___m_fAdjustValue_41; }
	inline void set_m_fAdjustValue_41(float value)
	{
		___m_fAdjustValue_41 = value;
	}

	inline static int32_t get_offset_of_m_fAdjustShit_42() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fAdjustShit_42)); }
	inline float get_m_fAdjustShit_42() const { return ___m_fAdjustShit_42; }
	inline float* get_address_of_m_fAdjustShit_42() { return &___m_fAdjustShit_42; }
	inline void set_m_fAdjustShit_42(float value)
	{
		___m_fAdjustShit_42 = value;
	}

	inline static int32_t get_offset_of_m_fSnowGenerateTimeElapse_43() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_fSnowGenerateTimeElapse_43)); }
	inline float get_m_fSnowGenerateTimeElapse_43() const { return ___m_fSnowGenerateTimeElapse_43; }
	inline float* get_address_of_m_fSnowGenerateTimeElapse_43() { return &___m_fSnowGenerateTimeElapse_43; }
	inline void set_m_fSnowGenerateTimeElapse_43(float value)
	{
		___m_fSnowGenerateTimeElapse_43 = value;
	}

	inline static int32_t get_offset_of_m_lstSnowFlakes_44() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108, ___m_lstSnowFlakes_44)); }
	inline List_1_t0EA6D74B5B5964105FF9E918795F54CBDC306BD9 * get_m_lstSnowFlakes_44() const { return ___m_lstSnowFlakes_44; }
	inline List_1_t0EA6D74B5B5964105FF9E918795F54CBDC306BD9 ** get_address_of_m_lstSnowFlakes_44() { return &___m_lstSnowFlakes_44; }
	inline void set_m_lstSnowFlakes_44(List_1_t0EA6D74B5B5964105FF9E918795F54CBDC306BD9 * value)
	{
		___m_lstSnowFlakes_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstSnowFlakes_44), value);
	}
};

struct JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108_StaticFields
{
public:
	// JTDHSceneManager JTDHSceneManager::s_Instance
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108 * ___s_Instance_9;
	// UnityEngine.Vector3 JTDHSceneManager::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_10;
	// UnityEngine.Vector3 JTDHSceneManager::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_11;

public:
	inline static int32_t get_offset_of_s_Instance_9() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108_StaticFields, ___s_Instance_9)); }
	inline JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108 * get_s_Instance_9() const { return ___s_Instance_9; }
	inline JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108 ** get_address_of_s_Instance_9() { return &___s_Instance_9; }
	inline void set_s_Instance_9(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108 * value)
	{
		___s_Instance_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_9), value);
	}

	inline static int32_t get_offset_of_vecTempPos_10() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108_StaticFields, ___vecTempPos_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_10() const { return ___vecTempPos_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_10() { return &___vecTempPos_10; }
	inline void set_vecTempPos_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_10 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_11() { return static_cast<int32_t>(offsetof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108_StaticFields, ___vecTempScale_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_11() const { return ___vecTempScale_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_11() { return &___vecTempScale_11; }
	inline void set_vecTempScale_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTDHSCENEMANAGER_TEE575F733C779BF24643585515AFFC7AFCC97108_H
#ifndef JETTRAIL_TCBAAF5CD558CAC61ED2859A36CF54E8786F37A46_H
#define JETTRAIL_TCBAAF5CD558CAC61ED2859A36CF54E8786F37A46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JetTrail
struct  JetTrail_tCBAAF5CD558CAC61ED2859A36CF54E8786F37A46  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JETTRAIL_TCBAAF5CD558CAC61ED2859A36CF54E8786F37A46_H
#ifndef LOT_T51B999792800E5A264A2CA51A4C521311958F874_H
#define LOT_T51B999792800E5A264A2CA51A4C521311958F874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lot
struct  Lot_t51B999792800E5A264A2CA51A4C521311958F874  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Lot::m_goMain
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goMain_6;
	// UnityEngine.SpriteRenderer Lot::_srPlusSign
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srPlusSign_7;
	// System.Boolean Lot::m_bShowingPlusSign
	bool ___m_bShowingPlusSign_8;
	// BaseScale Lot::_basescalePeng
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____basescalePeng_9;
	// Plane Lot::m_Plane
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273 * ___m_Plane_10;
	// UnityEngine.SpriteRenderer Lot::m_srMain
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___m_srMain_11;
	// UnityEngine.SpriteRenderer Lot::m_srBoundPlaneAvatar
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___m_srBoundPlaneAvatar_12;
	// UnityEngine.SpriteRenderer Lot::m_srBoundPlaneAvatarMask
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___m_srBoundPlaneAvatarMask_13;
	// UnityEngine.SpriteRenderer Lot::m_srFlyingMark
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___m_srFlyingMark_14;
	// UnityEngine.GameObject Lot::m_goEffectCanMerge
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goEffectCanMerge_15;
	// UnityEngine.Collider2D Lot::_Trigger
	Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * ____Trigger_16;
	// UnityEngine.SpriteRenderer Lot::_srShadow
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srShadow_17;
	// System.Int32 Lot::m_nId
	int32_t ___m_nId_18;
	// EnviromentMask Lot::_enviromentMask
	EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73 * ____enviromentMask_19;

public:
	inline static int32_t get_offset_of_m_goMain_6() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ___m_goMain_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goMain_6() const { return ___m_goMain_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goMain_6() { return &___m_goMain_6; }
	inline void set_m_goMain_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goMain_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_goMain_6), value);
	}

	inline static int32_t get_offset_of__srPlusSign_7() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ____srPlusSign_7)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srPlusSign_7() const { return ____srPlusSign_7; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srPlusSign_7() { return &____srPlusSign_7; }
	inline void set__srPlusSign_7(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srPlusSign_7 = value;
		Il2CppCodeGenWriteBarrier((&____srPlusSign_7), value);
	}

	inline static int32_t get_offset_of_m_bShowingPlusSign_8() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ___m_bShowingPlusSign_8)); }
	inline bool get_m_bShowingPlusSign_8() const { return ___m_bShowingPlusSign_8; }
	inline bool* get_address_of_m_bShowingPlusSign_8() { return &___m_bShowingPlusSign_8; }
	inline void set_m_bShowingPlusSign_8(bool value)
	{
		___m_bShowingPlusSign_8 = value;
	}

	inline static int32_t get_offset_of__basescalePeng_9() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ____basescalePeng_9)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__basescalePeng_9() const { return ____basescalePeng_9; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__basescalePeng_9() { return &____basescalePeng_9; }
	inline void set__basescalePeng_9(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____basescalePeng_9 = value;
		Il2CppCodeGenWriteBarrier((&____basescalePeng_9), value);
	}

	inline static int32_t get_offset_of_m_Plane_10() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ___m_Plane_10)); }
	inline Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273 * get_m_Plane_10() const { return ___m_Plane_10; }
	inline Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273 ** get_address_of_m_Plane_10() { return &___m_Plane_10; }
	inline void set_m_Plane_10(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273 * value)
	{
		___m_Plane_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Plane_10), value);
	}

	inline static int32_t get_offset_of_m_srMain_11() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ___m_srMain_11)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_m_srMain_11() const { return ___m_srMain_11; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_m_srMain_11() { return &___m_srMain_11; }
	inline void set_m_srMain_11(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___m_srMain_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_srMain_11), value);
	}

	inline static int32_t get_offset_of_m_srBoundPlaneAvatar_12() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ___m_srBoundPlaneAvatar_12)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_m_srBoundPlaneAvatar_12() const { return ___m_srBoundPlaneAvatar_12; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_m_srBoundPlaneAvatar_12() { return &___m_srBoundPlaneAvatar_12; }
	inline void set_m_srBoundPlaneAvatar_12(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___m_srBoundPlaneAvatar_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_srBoundPlaneAvatar_12), value);
	}

	inline static int32_t get_offset_of_m_srBoundPlaneAvatarMask_13() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ___m_srBoundPlaneAvatarMask_13)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_m_srBoundPlaneAvatarMask_13() const { return ___m_srBoundPlaneAvatarMask_13; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_m_srBoundPlaneAvatarMask_13() { return &___m_srBoundPlaneAvatarMask_13; }
	inline void set_m_srBoundPlaneAvatarMask_13(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___m_srBoundPlaneAvatarMask_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_srBoundPlaneAvatarMask_13), value);
	}

	inline static int32_t get_offset_of_m_srFlyingMark_14() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ___m_srFlyingMark_14)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_m_srFlyingMark_14() const { return ___m_srFlyingMark_14; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_m_srFlyingMark_14() { return &___m_srFlyingMark_14; }
	inline void set_m_srFlyingMark_14(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___m_srFlyingMark_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_srFlyingMark_14), value);
	}

	inline static int32_t get_offset_of_m_goEffectCanMerge_15() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ___m_goEffectCanMerge_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goEffectCanMerge_15() const { return ___m_goEffectCanMerge_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goEffectCanMerge_15() { return &___m_goEffectCanMerge_15; }
	inline void set_m_goEffectCanMerge_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goEffectCanMerge_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_goEffectCanMerge_15), value);
	}

	inline static int32_t get_offset_of__Trigger_16() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ____Trigger_16)); }
	inline Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * get__Trigger_16() const { return ____Trigger_16; }
	inline Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 ** get_address_of__Trigger_16() { return &____Trigger_16; }
	inline void set__Trigger_16(Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * value)
	{
		____Trigger_16 = value;
		Il2CppCodeGenWriteBarrier((&____Trigger_16), value);
	}

	inline static int32_t get_offset_of__srShadow_17() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ____srShadow_17)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srShadow_17() const { return ____srShadow_17; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srShadow_17() { return &____srShadow_17; }
	inline void set__srShadow_17(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srShadow_17 = value;
		Il2CppCodeGenWriteBarrier((&____srShadow_17), value);
	}

	inline static int32_t get_offset_of_m_nId_18() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ___m_nId_18)); }
	inline int32_t get_m_nId_18() const { return ___m_nId_18; }
	inline int32_t* get_address_of_m_nId_18() { return &___m_nId_18; }
	inline void set_m_nId_18(int32_t value)
	{
		___m_nId_18 = value;
	}

	inline static int32_t get_offset_of__enviromentMask_19() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874, ____enviromentMask_19)); }
	inline EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73 * get__enviromentMask_19() const { return ____enviromentMask_19; }
	inline EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73 ** get_address_of__enviromentMask_19() { return &____enviromentMask_19; }
	inline void set__enviromentMask_19(EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73 * value)
	{
		____enviromentMask_19 = value;
		Il2CppCodeGenWriteBarrier((&____enviromentMask_19), value);
	}
};

struct Lot_t51B999792800E5A264A2CA51A4C521311958F874_StaticFields
{
public:
	// UnityEngine.Vector3 Lot::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_4;
	// UnityEngine.Vector3 Lot::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_5;

public:
	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874_StaticFields, ___vecTempPos_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(Lot_t51B999792800E5A264A2CA51A4C521311958F874_StaticFields, ___vecTempScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOT_T51B999792800E5A264A2CA51A4C521311958F874_H
#ifndef MAIN_T7BD87A0CB813F834A2F93006206A3CE79188D0C1_H
#define MAIN_T7BD87A0CB813F834A2F93006206A3CE79188D0C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main
struct  Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Main::m_goContainerEffects
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goContainerEffects_4;
	// UnityEngine.GameObject Main::_goStartPos
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goStartPos_5;
	// UnityEngine.GameObject[] Main::m_aryNodeStart
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryNodeStart_6;
	// UnityEngine.GameObject[] Main::m_aryNodeCenter
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryNodeCenter_7;
	// UnityEngine.GameObject[] Main::m_aryNodeEnd
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryNodeEnd_8;
	// System.Collections.Generic.Dictionary`2<Main/ePosType,System.Collections.Generic.List`1<Main/sAutoRunLocation>> Main::m_dicLocationPoints
	Dictionary_2_t8BBCAEE5B6551D2028E4CE6BBACCA1D12D8070C0 * ___m_dicLocationPoints_9;
	// UnityEngine.GameObject[] Main::m_aryStartLocationPointOfEachSeg
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryStartLocationPointOfEachSeg_10;
	// Main/sAutoRunLocation[] Main::m_aryStartLocationInfoPointOfEachSeg
	sAutoRunLocationU5BU5D_t1B4B4B1E8F7FDC3043F544A45744E0D6A98B8864* ___m_aryStartLocationInfoPointOfEachSeg_11;
	// UnityEngine.GameObject Main::m_containerLocations
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_containerLocations_12;
	// UnityEngine.UI.Text Main::_txtDebugInfo
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDebugInfo_13;
	// UnityEngine.UI.Text Main::_txtOfflineDetailDebug
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtOfflineDetailDebug_14;
	// MoneyTrigger Main::_moneyTrigger
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464 * ____moneyTrigger_15;
	// UnityEngine.Sprite[] Main::m_aryMoneyTrigger
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryMoneyTrigger_16;
	// BaseScale Main::_basescaleCPosCoinIcon
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____basescaleCPosCoinIcon_17;
	// UnityEngine.Vector3 Main::m_vecLocalPosOnLot
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecLocalPosOnLot_22;
	// System.Single Main::m_nCurRaise
	float ___m_nCurRaise_23;
	// MoneyCounter[] Main::m_aryCoin0
	MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* ___m_aryCoin0_24;
	// MoneyCounter[] Main::m_aryCoin1
	MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* ___m_aryCoin1_25;
	// MoneyCounter[] Main::m_aryCoin2
	MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* ___m_aryCoin2_26;
	// UnityEngine.UI.Text Main::_txtPrestigeTimes
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPrestigeTimes_27;
	// UnityEngine.UI.Text Main::_txtPrestigeTimes_Shadow
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPrestigeTimes_Shadow_28;
	// UIStars Main::_starsPrestigeTimes
	UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * ____starsPrestigeTimes_29;
	// UnityEngine.UI.Text Main::_txtTotalCoinOfThisPlanet
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTotalCoinOfThisPlanet_30;
	// UnityEngine.TextMesh Main::_tmTotalCoinOfThisPlanet
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ____tmTotalCoinOfThisPlanet_31;
	// UnityEngine.GameObject Main::_panelCollectOfflineProfit
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelCollectOfflineProfit_32;
	// UnityEngine.UI.Text Main::_txtOfflineProfitOfThisDistrict
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtOfflineProfitOfThisDistrict_33;
	// UnityEngine.UI.Button Main::_btnOpenBigMap
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnOpenBigMap_34;
	// UnityEngine.UI.Button Main::_btnCloseBigMap
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnCloseBigMap_35;
	// MoneyCounter Main::_moneyCoin
	MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * ____moneyCoin_36;
	// MoneyCounter[] Main::m_aryActivePlanetCoin
	MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* ___m_aryActivePlanetCoin_37;
	// MoneyCounter Main::_moneyGreenCash
	MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * ____moneyGreenCash_38;
	// UnityEngine.UI.Text Main::_txtDiamond
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDiamond_39;
	// UnityEngine.UI.Text Main::_txtDiamond_Shadow
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDiamond_Shadow_40;
	// UnityEngine.GameObject Main::_panelRaiseDetail
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelRaiseDetail_41;
	// UnityEngine.UI.Text Main::_txtRaiseDetail
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtRaiseDetail_42;
	// SceneUiButton Main::_btnBuy
	SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507 * ____btnBuy_43;
	// UnityEngine.GameObject Main::_BigMap
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____BigMap_44;
	// UnityEngine.GameObject Main::_panelPrestige
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelPrestige_45;
	// UIPrestige Main::_Prestige
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C * ____Prestige_46;
	// UnityEngine.TextMesh Main::_txtNumOfRunningPlane
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ____txtNumOfRunningPlane_47;
	// UnityEngine.UI.Text Main::_txtDPS
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDPS_48;
	// UnityEngine.GameObject Main::_goRecycleBox
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goRecycleBox_49;
	// System.Single Main::DISTANCE_UNIT
	float ___DISTANCE_UNIT_50;
	// System.Single Main::TURN_RADIUS
	float ___TURN_RADIUS_51;
	// UnityEngine.Vector2 Main::START_POS
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___START_POS_52;
	// System.Single Main::LENGTH_OF_TURN
	float ___LENGTH_OF_TURN_53;
	// System.Single Main::PERIMETER
	float ___PERIMETER_54;
	// System.Single Main::MERGE_OFFSET
	float ___MERGE_OFFSET_55;
	// System.Single Main::MERGE_TIME
	float ___MERGE_TIME_56;
	// System.Single Main::MERGE_WAIT_TIME
	float ___MERGE_WAIT_TIME_57;
	// UnityEngine.GameObject Main::_containerRunningPlanes
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerRunningPlanes_58;
	// System.Collections.Generic.List`1<Plane> Main::m_lstRunningPlanes
	List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * ___m_lstRunningPlanes_59;
	// System.Single[] Main::m_aryPosThreshold
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryPosThreshold_60;
	// UnityEngine.Vector2[] Main::m_aryStartPosOfEachSegment
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_aryStartPosOfEachSegment_61;
	// UnityEngine.Vector2[] Main::m_aryCircleCenterOfTurn
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_aryCircleCenterOfTurn_62;
	// System.Single Main::m_fMergeMoveSpeed
	float ___m_fMergeMoveSpeed_63;
	// Lot[] Main::m_aryLots
	LotU5BU5D_tDF39035F8B8E0E8A8915E650FAC90FE38BFD335F* ___m_aryLots_67;
	// UnityEngine.GameObject Main::m_aryLotsPositionPointsContainers
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_aryLotsPositionPointsContainers_68;
	// StartBelt Main::m_StartBelt
	StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD * ___m_StartBelt_69;
	// System.Single[] Main::m_aryRoundTimeByLevel
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryRoundTimeByLevel_70;
	// System.Collections.Generic.List`1<Plane> Main::m_lstAllPlanes
	List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * ___m_lstAllPlanes_71;
	// UnityEngine.Vector3 Main::m_vecStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecStartPos_72;
	// UnityEngine.Vector3[] Main::m_vecNodeStartPos
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_vecNodeStartPos_73;
	// UnityEngine.Vector3[] Main::m_vecNodeCenterPos
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_vecNodeCenterPos_74;
	// UnityEngine.Vector3[] Main::m_vecNodeEndPos
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_vecNodeEndPos_75;
	// System.Single Main::m_fRadius
	float ___m_fRadius_76;
	// System.Single Main::m_fVertLineLen
	float ___m_fVertLineLen_77;
	// System.Single Main::m_fHoriLineLen
	float ___m_fHoriLineLen_78;
	// System.Single Main::m_fRoundLen
	float ___m_fRoundLen_79;
	// System.Single Main::m_fPosAdjustDistance
	float ___m_fPosAdjustDistance_80;
	// System.Single Main::m_f10SecLoopElapse
	float ___m_f10SecLoopElapse_82;
	// System.Boolean Main::m_bIsVisible
	bool ___m_bIsVisible_83;
	// System.Collections.Generic.List`1<Plane> Main::lstTempPLanes
	List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * ___lstTempPLanes_84;
	// System.Single Main::m_fAccelerate
	float ___m_fAccelerate_85;
	// System.Single Main::m_fPreAccerlateTimeLeft
	float ___m_fPreAccerlateTimeLeft_86;
	// System.Single Main::m_fSpeedAccelerate
	float ___m_fSpeedAccelerate_87;
	// System.Single[] Main::m_aryAccelerate
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryAccelerate_89;
	// System.Boolean Main::m_bLostFocus
	bool ___m_bLostFocus_90;
	// System.Object Main::_panelAds
	RuntimeObject * ____panelAds_91;
	// System.Double Main::m_fOfflineProfit
	double ___m_fOfflineProfit_92;
	// UnityEngine.Vector3 Main::vecTempPosLeft
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPosLeft_93;
	// UnityEngine.Vector3 Main::vecTempPosRight
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPosRight_94;
	// System.Collections.Generic.List`1<Main/sAutoRunLocation> Main::m_lstRunLocations
	List_1_t1517B4D31CFE0D5A31B279B7CC226F6BA9FA9E43 * ___m_lstRunLocations_95;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>> Main::m_dicLotPosOfLevel
	Dictionary_2_t27BCF11A3E40FEB6B728C0EA39A63FAB1AE16D5C * ___m_dicLotPosOfLevel_96;
	// Main/sAutoRunLocation Main::selected_one
	sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949  ___selected_one_97;
	// System.Double Main::m_dShowingNumber
	double ___m_dShowingNumber_98;
	// System.Double Main::m_dNumberChangeSpeed
	double ___m_dNumberChangeSpeed_99;
	// System.Single Main::m_fCoinChangeTime
	float ___m_fCoinChangeTime_100;
	// System.Double Main::m_dDestCoinNumber
	double ___m_dDestCoinNumber_101;

public:
	inline static int32_t get_offset_of_m_goContainerEffects_4() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_goContainerEffects_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goContainerEffects_4() const { return ___m_goContainerEffects_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goContainerEffects_4() { return &___m_goContainerEffects_4; }
	inline void set_m_goContainerEffects_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goContainerEffects_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainerEffects_4), value);
	}

	inline static int32_t get_offset_of__goStartPos_5() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____goStartPos_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goStartPos_5() const { return ____goStartPos_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goStartPos_5() { return &____goStartPos_5; }
	inline void set__goStartPos_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goStartPos_5 = value;
		Il2CppCodeGenWriteBarrier((&____goStartPos_5), value);
	}

	inline static int32_t get_offset_of_m_aryNodeStart_6() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryNodeStart_6)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryNodeStart_6() const { return ___m_aryNodeStart_6; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryNodeStart_6() { return &___m_aryNodeStart_6; }
	inline void set_m_aryNodeStart_6(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryNodeStart_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryNodeStart_6), value);
	}

	inline static int32_t get_offset_of_m_aryNodeCenter_7() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryNodeCenter_7)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryNodeCenter_7() const { return ___m_aryNodeCenter_7; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryNodeCenter_7() { return &___m_aryNodeCenter_7; }
	inline void set_m_aryNodeCenter_7(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryNodeCenter_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryNodeCenter_7), value);
	}

	inline static int32_t get_offset_of_m_aryNodeEnd_8() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryNodeEnd_8)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryNodeEnd_8() const { return ___m_aryNodeEnd_8; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryNodeEnd_8() { return &___m_aryNodeEnd_8; }
	inline void set_m_aryNodeEnd_8(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryNodeEnd_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryNodeEnd_8), value);
	}

	inline static int32_t get_offset_of_m_dicLocationPoints_9() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_dicLocationPoints_9)); }
	inline Dictionary_2_t8BBCAEE5B6551D2028E4CE6BBACCA1D12D8070C0 * get_m_dicLocationPoints_9() const { return ___m_dicLocationPoints_9; }
	inline Dictionary_2_t8BBCAEE5B6551D2028E4CE6BBACCA1D12D8070C0 ** get_address_of_m_dicLocationPoints_9() { return &___m_dicLocationPoints_9; }
	inline void set_m_dicLocationPoints_9(Dictionary_2_t8BBCAEE5B6551D2028E4CE6BBACCA1D12D8070C0 * value)
	{
		___m_dicLocationPoints_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLocationPoints_9), value);
	}

	inline static int32_t get_offset_of_m_aryStartLocationPointOfEachSeg_10() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryStartLocationPointOfEachSeg_10)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryStartLocationPointOfEachSeg_10() const { return ___m_aryStartLocationPointOfEachSeg_10; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryStartLocationPointOfEachSeg_10() { return &___m_aryStartLocationPointOfEachSeg_10; }
	inline void set_m_aryStartLocationPointOfEachSeg_10(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryStartLocationPointOfEachSeg_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryStartLocationPointOfEachSeg_10), value);
	}

	inline static int32_t get_offset_of_m_aryStartLocationInfoPointOfEachSeg_11() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryStartLocationInfoPointOfEachSeg_11)); }
	inline sAutoRunLocationU5BU5D_t1B4B4B1E8F7FDC3043F544A45744E0D6A98B8864* get_m_aryStartLocationInfoPointOfEachSeg_11() const { return ___m_aryStartLocationInfoPointOfEachSeg_11; }
	inline sAutoRunLocationU5BU5D_t1B4B4B1E8F7FDC3043F544A45744E0D6A98B8864** get_address_of_m_aryStartLocationInfoPointOfEachSeg_11() { return &___m_aryStartLocationInfoPointOfEachSeg_11; }
	inline void set_m_aryStartLocationInfoPointOfEachSeg_11(sAutoRunLocationU5BU5D_t1B4B4B1E8F7FDC3043F544A45744E0D6A98B8864* value)
	{
		___m_aryStartLocationInfoPointOfEachSeg_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryStartLocationInfoPointOfEachSeg_11), value);
	}

	inline static int32_t get_offset_of_m_containerLocations_12() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_containerLocations_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_containerLocations_12() const { return ___m_containerLocations_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_containerLocations_12() { return &___m_containerLocations_12; }
	inline void set_m_containerLocations_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_containerLocations_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_containerLocations_12), value);
	}

	inline static int32_t get_offset_of__txtDebugInfo_13() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtDebugInfo_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDebugInfo_13() const { return ____txtDebugInfo_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDebugInfo_13() { return &____txtDebugInfo_13; }
	inline void set__txtDebugInfo_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDebugInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&____txtDebugInfo_13), value);
	}

	inline static int32_t get_offset_of__txtOfflineDetailDebug_14() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtOfflineDetailDebug_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtOfflineDetailDebug_14() const { return ____txtOfflineDetailDebug_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtOfflineDetailDebug_14() { return &____txtOfflineDetailDebug_14; }
	inline void set__txtOfflineDetailDebug_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtOfflineDetailDebug_14 = value;
		Il2CppCodeGenWriteBarrier((&____txtOfflineDetailDebug_14), value);
	}

	inline static int32_t get_offset_of__moneyTrigger_15() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____moneyTrigger_15)); }
	inline MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464 * get__moneyTrigger_15() const { return ____moneyTrigger_15; }
	inline MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464 ** get_address_of__moneyTrigger_15() { return &____moneyTrigger_15; }
	inline void set__moneyTrigger_15(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464 * value)
	{
		____moneyTrigger_15 = value;
		Il2CppCodeGenWriteBarrier((&____moneyTrigger_15), value);
	}

	inline static int32_t get_offset_of_m_aryMoneyTrigger_16() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryMoneyTrigger_16)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryMoneyTrigger_16() const { return ___m_aryMoneyTrigger_16; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryMoneyTrigger_16() { return &___m_aryMoneyTrigger_16; }
	inline void set_m_aryMoneyTrigger_16(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryMoneyTrigger_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMoneyTrigger_16), value);
	}

	inline static int32_t get_offset_of__basescaleCPosCoinIcon_17() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____basescaleCPosCoinIcon_17)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__basescaleCPosCoinIcon_17() const { return ____basescaleCPosCoinIcon_17; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__basescaleCPosCoinIcon_17() { return &____basescaleCPosCoinIcon_17; }
	inline void set__basescaleCPosCoinIcon_17(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____basescaleCPosCoinIcon_17 = value;
		Il2CppCodeGenWriteBarrier((&____basescaleCPosCoinIcon_17), value);
	}

	inline static int32_t get_offset_of_m_vecLocalPosOnLot_22() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_vecLocalPosOnLot_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecLocalPosOnLot_22() const { return ___m_vecLocalPosOnLot_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecLocalPosOnLot_22() { return &___m_vecLocalPosOnLot_22; }
	inline void set_m_vecLocalPosOnLot_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecLocalPosOnLot_22 = value;
	}

	inline static int32_t get_offset_of_m_nCurRaise_23() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_nCurRaise_23)); }
	inline float get_m_nCurRaise_23() const { return ___m_nCurRaise_23; }
	inline float* get_address_of_m_nCurRaise_23() { return &___m_nCurRaise_23; }
	inline void set_m_nCurRaise_23(float value)
	{
		___m_nCurRaise_23 = value;
	}

	inline static int32_t get_offset_of_m_aryCoin0_24() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryCoin0_24)); }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* get_m_aryCoin0_24() const { return ___m_aryCoin0_24; }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A** get_address_of_m_aryCoin0_24() { return &___m_aryCoin0_24; }
	inline void set_m_aryCoin0_24(MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* value)
	{
		___m_aryCoin0_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoin0_24), value);
	}

	inline static int32_t get_offset_of_m_aryCoin1_25() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryCoin1_25)); }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* get_m_aryCoin1_25() const { return ___m_aryCoin1_25; }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A** get_address_of_m_aryCoin1_25() { return &___m_aryCoin1_25; }
	inline void set_m_aryCoin1_25(MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* value)
	{
		___m_aryCoin1_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoin1_25), value);
	}

	inline static int32_t get_offset_of_m_aryCoin2_26() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryCoin2_26)); }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* get_m_aryCoin2_26() const { return ___m_aryCoin2_26; }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A** get_address_of_m_aryCoin2_26() { return &___m_aryCoin2_26; }
	inline void set_m_aryCoin2_26(MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* value)
	{
		___m_aryCoin2_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoin2_26), value);
	}

	inline static int32_t get_offset_of__txtPrestigeTimes_27() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtPrestigeTimes_27)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPrestigeTimes_27() const { return ____txtPrestigeTimes_27; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPrestigeTimes_27() { return &____txtPrestigeTimes_27; }
	inline void set__txtPrestigeTimes_27(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPrestigeTimes_27 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrestigeTimes_27), value);
	}

	inline static int32_t get_offset_of__txtPrestigeTimes_Shadow_28() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtPrestigeTimes_Shadow_28)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPrestigeTimes_Shadow_28() const { return ____txtPrestigeTimes_Shadow_28; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPrestigeTimes_Shadow_28() { return &____txtPrestigeTimes_Shadow_28; }
	inline void set__txtPrestigeTimes_Shadow_28(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPrestigeTimes_Shadow_28 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrestigeTimes_Shadow_28), value);
	}

	inline static int32_t get_offset_of__starsPrestigeTimes_29() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____starsPrestigeTimes_29)); }
	inline UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * get__starsPrestigeTimes_29() const { return ____starsPrestigeTimes_29; }
	inline UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B ** get_address_of__starsPrestigeTimes_29() { return &____starsPrestigeTimes_29; }
	inline void set__starsPrestigeTimes_29(UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * value)
	{
		____starsPrestigeTimes_29 = value;
		Il2CppCodeGenWriteBarrier((&____starsPrestigeTimes_29), value);
	}

	inline static int32_t get_offset_of__txtTotalCoinOfThisPlanet_30() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtTotalCoinOfThisPlanet_30)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTotalCoinOfThisPlanet_30() const { return ____txtTotalCoinOfThisPlanet_30; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTotalCoinOfThisPlanet_30() { return &____txtTotalCoinOfThisPlanet_30; }
	inline void set__txtTotalCoinOfThisPlanet_30(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTotalCoinOfThisPlanet_30 = value;
		Il2CppCodeGenWriteBarrier((&____txtTotalCoinOfThisPlanet_30), value);
	}

	inline static int32_t get_offset_of__tmTotalCoinOfThisPlanet_31() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____tmTotalCoinOfThisPlanet_31)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get__tmTotalCoinOfThisPlanet_31() const { return ____tmTotalCoinOfThisPlanet_31; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of__tmTotalCoinOfThisPlanet_31() { return &____tmTotalCoinOfThisPlanet_31; }
	inline void set__tmTotalCoinOfThisPlanet_31(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		____tmTotalCoinOfThisPlanet_31 = value;
		Il2CppCodeGenWriteBarrier((&____tmTotalCoinOfThisPlanet_31), value);
	}

	inline static int32_t get_offset_of__panelCollectOfflineProfit_32() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____panelCollectOfflineProfit_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelCollectOfflineProfit_32() const { return ____panelCollectOfflineProfit_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelCollectOfflineProfit_32() { return &____panelCollectOfflineProfit_32; }
	inline void set__panelCollectOfflineProfit_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelCollectOfflineProfit_32 = value;
		Il2CppCodeGenWriteBarrier((&____panelCollectOfflineProfit_32), value);
	}

	inline static int32_t get_offset_of__txtOfflineProfitOfThisDistrict_33() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtOfflineProfitOfThisDistrict_33)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtOfflineProfitOfThisDistrict_33() const { return ____txtOfflineProfitOfThisDistrict_33; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtOfflineProfitOfThisDistrict_33() { return &____txtOfflineProfitOfThisDistrict_33; }
	inline void set__txtOfflineProfitOfThisDistrict_33(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtOfflineProfitOfThisDistrict_33 = value;
		Il2CppCodeGenWriteBarrier((&____txtOfflineProfitOfThisDistrict_33), value);
	}

	inline static int32_t get_offset_of__btnOpenBigMap_34() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____btnOpenBigMap_34)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnOpenBigMap_34() const { return ____btnOpenBigMap_34; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnOpenBigMap_34() { return &____btnOpenBigMap_34; }
	inline void set__btnOpenBigMap_34(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnOpenBigMap_34 = value;
		Il2CppCodeGenWriteBarrier((&____btnOpenBigMap_34), value);
	}

	inline static int32_t get_offset_of__btnCloseBigMap_35() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____btnCloseBigMap_35)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnCloseBigMap_35() const { return ____btnCloseBigMap_35; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnCloseBigMap_35() { return &____btnCloseBigMap_35; }
	inline void set__btnCloseBigMap_35(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnCloseBigMap_35 = value;
		Il2CppCodeGenWriteBarrier((&____btnCloseBigMap_35), value);
	}

	inline static int32_t get_offset_of__moneyCoin_36() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____moneyCoin_36)); }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * get__moneyCoin_36() const { return ____moneyCoin_36; }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF ** get_address_of__moneyCoin_36() { return &____moneyCoin_36; }
	inline void set__moneyCoin_36(MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * value)
	{
		____moneyCoin_36 = value;
		Il2CppCodeGenWriteBarrier((&____moneyCoin_36), value);
	}

	inline static int32_t get_offset_of_m_aryActivePlanetCoin_37() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryActivePlanetCoin_37)); }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* get_m_aryActivePlanetCoin_37() const { return ___m_aryActivePlanetCoin_37; }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A** get_address_of_m_aryActivePlanetCoin_37() { return &___m_aryActivePlanetCoin_37; }
	inline void set_m_aryActivePlanetCoin_37(MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* value)
	{
		___m_aryActivePlanetCoin_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryActivePlanetCoin_37), value);
	}

	inline static int32_t get_offset_of__moneyGreenCash_38() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____moneyGreenCash_38)); }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * get__moneyGreenCash_38() const { return ____moneyGreenCash_38; }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF ** get_address_of__moneyGreenCash_38() { return &____moneyGreenCash_38; }
	inline void set__moneyGreenCash_38(MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * value)
	{
		____moneyGreenCash_38 = value;
		Il2CppCodeGenWriteBarrier((&____moneyGreenCash_38), value);
	}

	inline static int32_t get_offset_of__txtDiamond_39() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtDiamond_39)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDiamond_39() const { return ____txtDiamond_39; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDiamond_39() { return &____txtDiamond_39; }
	inline void set__txtDiamond_39(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDiamond_39 = value;
		Il2CppCodeGenWriteBarrier((&____txtDiamond_39), value);
	}

	inline static int32_t get_offset_of__txtDiamond_Shadow_40() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtDiamond_Shadow_40)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDiamond_Shadow_40() const { return ____txtDiamond_Shadow_40; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDiamond_Shadow_40() { return &____txtDiamond_Shadow_40; }
	inline void set__txtDiamond_Shadow_40(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDiamond_Shadow_40 = value;
		Il2CppCodeGenWriteBarrier((&____txtDiamond_Shadow_40), value);
	}

	inline static int32_t get_offset_of__panelRaiseDetail_41() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____panelRaiseDetail_41)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelRaiseDetail_41() const { return ____panelRaiseDetail_41; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelRaiseDetail_41() { return &____panelRaiseDetail_41; }
	inline void set__panelRaiseDetail_41(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelRaiseDetail_41 = value;
		Il2CppCodeGenWriteBarrier((&____panelRaiseDetail_41), value);
	}

	inline static int32_t get_offset_of__txtRaiseDetail_42() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtRaiseDetail_42)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtRaiseDetail_42() const { return ____txtRaiseDetail_42; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtRaiseDetail_42() { return &____txtRaiseDetail_42; }
	inline void set__txtRaiseDetail_42(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtRaiseDetail_42 = value;
		Il2CppCodeGenWriteBarrier((&____txtRaiseDetail_42), value);
	}

	inline static int32_t get_offset_of__btnBuy_43() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____btnBuy_43)); }
	inline SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507 * get__btnBuy_43() const { return ____btnBuy_43; }
	inline SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507 ** get_address_of__btnBuy_43() { return &____btnBuy_43; }
	inline void set__btnBuy_43(SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507 * value)
	{
		____btnBuy_43 = value;
		Il2CppCodeGenWriteBarrier((&____btnBuy_43), value);
	}

	inline static int32_t get_offset_of__BigMap_44() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____BigMap_44)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__BigMap_44() const { return ____BigMap_44; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__BigMap_44() { return &____BigMap_44; }
	inline void set__BigMap_44(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____BigMap_44 = value;
		Il2CppCodeGenWriteBarrier((&____BigMap_44), value);
	}

	inline static int32_t get_offset_of__panelPrestige_45() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____panelPrestige_45)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelPrestige_45() const { return ____panelPrestige_45; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelPrestige_45() { return &____panelPrestige_45; }
	inline void set__panelPrestige_45(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelPrestige_45 = value;
		Il2CppCodeGenWriteBarrier((&____panelPrestige_45), value);
	}

	inline static int32_t get_offset_of__Prestige_46() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____Prestige_46)); }
	inline UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C * get__Prestige_46() const { return ____Prestige_46; }
	inline UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C ** get_address_of__Prestige_46() { return &____Prestige_46; }
	inline void set__Prestige_46(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C * value)
	{
		____Prestige_46 = value;
		Il2CppCodeGenWriteBarrier((&____Prestige_46), value);
	}

	inline static int32_t get_offset_of__txtNumOfRunningPlane_47() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtNumOfRunningPlane_47)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get__txtNumOfRunningPlane_47() const { return ____txtNumOfRunningPlane_47; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of__txtNumOfRunningPlane_47() { return &____txtNumOfRunningPlane_47; }
	inline void set__txtNumOfRunningPlane_47(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		____txtNumOfRunningPlane_47 = value;
		Il2CppCodeGenWriteBarrier((&____txtNumOfRunningPlane_47), value);
	}

	inline static int32_t get_offset_of__txtDPS_48() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtDPS_48)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDPS_48() const { return ____txtDPS_48; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDPS_48() { return &____txtDPS_48; }
	inline void set__txtDPS_48(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDPS_48 = value;
		Il2CppCodeGenWriteBarrier((&____txtDPS_48), value);
	}

	inline static int32_t get_offset_of__goRecycleBox_49() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____goRecycleBox_49)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goRecycleBox_49() const { return ____goRecycleBox_49; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goRecycleBox_49() { return &____goRecycleBox_49; }
	inline void set__goRecycleBox_49(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goRecycleBox_49 = value;
		Il2CppCodeGenWriteBarrier((&____goRecycleBox_49), value);
	}

	inline static int32_t get_offset_of_DISTANCE_UNIT_50() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___DISTANCE_UNIT_50)); }
	inline float get_DISTANCE_UNIT_50() const { return ___DISTANCE_UNIT_50; }
	inline float* get_address_of_DISTANCE_UNIT_50() { return &___DISTANCE_UNIT_50; }
	inline void set_DISTANCE_UNIT_50(float value)
	{
		___DISTANCE_UNIT_50 = value;
	}

	inline static int32_t get_offset_of_TURN_RADIUS_51() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___TURN_RADIUS_51)); }
	inline float get_TURN_RADIUS_51() const { return ___TURN_RADIUS_51; }
	inline float* get_address_of_TURN_RADIUS_51() { return &___TURN_RADIUS_51; }
	inline void set_TURN_RADIUS_51(float value)
	{
		___TURN_RADIUS_51 = value;
	}

	inline static int32_t get_offset_of_START_POS_52() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___START_POS_52)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_START_POS_52() const { return ___START_POS_52; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_START_POS_52() { return &___START_POS_52; }
	inline void set_START_POS_52(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___START_POS_52 = value;
	}

	inline static int32_t get_offset_of_LENGTH_OF_TURN_53() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___LENGTH_OF_TURN_53)); }
	inline float get_LENGTH_OF_TURN_53() const { return ___LENGTH_OF_TURN_53; }
	inline float* get_address_of_LENGTH_OF_TURN_53() { return &___LENGTH_OF_TURN_53; }
	inline void set_LENGTH_OF_TURN_53(float value)
	{
		___LENGTH_OF_TURN_53 = value;
	}

	inline static int32_t get_offset_of_PERIMETER_54() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___PERIMETER_54)); }
	inline float get_PERIMETER_54() const { return ___PERIMETER_54; }
	inline float* get_address_of_PERIMETER_54() { return &___PERIMETER_54; }
	inline void set_PERIMETER_54(float value)
	{
		___PERIMETER_54 = value;
	}

	inline static int32_t get_offset_of_MERGE_OFFSET_55() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___MERGE_OFFSET_55)); }
	inline float get_MERGE_OFFSET_55() const { return ___MERGE_OFFSET_55; }
	inline float* get_address_of_MERGE_OFFSET_55() { return &___MERGE_OFFSET_55; }
	inline void set_MERGE_OFFSET_55(float value)
	{
		___MERGE_OFFSET_55 = value;
	}

	inline static int32_t get_offset_of_MERGE_TIME_56() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___MERGE_TIME_56)); }
	inline float get_MERGE_TIME_56() const { return ___MERGE_TIME_56; }
	inline float* get_address_of_MERGE_TIME_56() { return &___MERGE_TIME_56; }
	inline void set_MERGE_TIME_56(float value)
	{
		___MERGE_TIME_56 = value;
	}

	inline static int32_t get_offset_of_MERGE_WAIT_TIME_57() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___MERGE_WAIT_TIME_57)); }
	inline float get_MERGE_WAIT_TIME_57() const { return ___MERGE_WAIT_TIME_57; }
	inline float* get_address_of_MERGE_WAIT_TIME_57() { return &___MERGE_WAIT_TIME_57; }
	inline void set_MERGE_WAIT_TIME_57(float value)
	{
		___MERGE_WAIT_TIME_57 = value;
	}

	inline static int32_t get_offset_of__containerRunningPlanes_58() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____containerRunningPlanes_58)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerRunningPlanes_58() const { return ____containerRunningPlanes_58; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerRunningPlanes_58() { return &____containerRunningPlanes_58; }
	inline void set__containerRunningPlanes_58(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerRunningPlanes_58 = value;
		Il2CppCodeGenWriteBarrier((&____containerRunningPlanes_58), value);
	}

	inline static int32_t get_offset_of_m_lstRunningPlanes_59() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_lstRunningPlanes_59)); }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * get_m_lstRunningPlanes_59() const { return ___m_lstRunningPlanes_59; }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 ** get_address_of_m_lstRunningPlanes_59() { return &___m_lstRunningPlanes_59; }
	inline void set_m_lstRunningPlanes_59(List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * value)
	{
		___m_lstRunningPlanes_59 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRunningPlanes_59), value);
	}

	inline static int32_t get_offset_of_m_aryPosThreshold_60() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryPosThreshold_60)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryPosThreshold_60() const { return ___m_aryPosThreshold_60; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryPosThreshold_60() { return &___m_aryPosThreshold_60; }
	inline void set_m_aryPosThreshold_60(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryPosThreshold_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPosThreshold_60), value);
	}

	inline static int32_t get_offset_of_m_aryStartPosOfEachSegment_61() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryStartPosOfEachSegment_61)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_aryStartPosOfEachSegment_61() const { return ___m_aryStartPosOfEachSegment_61; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_aryStartPosOfEachSegment_61() { return &___m_aryStartPosOfEachSegment_61; }
	inline void set_m_aryStartPosOfEachSegment_61(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_aryStartPosOfEachSegment_61 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryStartPosOfEachSegment_61), value);
	}

	inline static int32_t get_offset_of_m_aryCircleCenterOfTurn_62() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryCircleCenterOfTurn_62)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_aryCircleCenterOfTurn_62() const { return ___m_aryCircleCenterOfTurn_62; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_aryCircleCenterOfTurn_62() { return &___m_aryCircleCenterOfTurn_62; }
	inline void set_m_aryCircleCenterOfTurn_62(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_aryCircleCenterOfTurn_62 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCircleCenterOfTurn_62), value);
	}

	inline static int32_t get_offset_of_m_fMergeMoveSpeed_63() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fMergeMoveSpeed_63)); }
	inline float get_m_fMergeMoveSpeed_63() const { return ___m_fMergeMoveSpeed_63; }
	inline float* get_address_of_m_fMergeMoveSpeed_63() { return &___m_fMergeMoveSpeed_63; }
	inline void set_m_fMergeMoveSpeed_63(float value)
	{
		___m_fMergeMoveSpeed_63 = value;
	}

	inline static int32_t get_offset_of_m_aryLots_67() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryLots_67)); }
	inline LotU5BU5D_tDF39035F8B8E0E8A8915E650FAC90FE38BFD335F* get_m_aryLots_67() const { return ___m_aryLots_67; }
	inline LotU5BU5D_tDF39035F8B8E0E8A8915E650FAC90FE38BFD335F** get_address_of_m_aryLots_67() { return &___m_aryLots_67; }
	inline void set_m_aryLots_67(LotU5BU5D_tDF39035F8B8E0E8A8915E650FAC90FE38BFD335F* value)
	{
		___m_aryLots_67 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryLots_67), value);
	}

	inline static int32_t get_offset_of_m_aryLotsPositionPointsContainers_68() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryLotsPositionPointsContainers_68)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_aryLotsPositionPointsContainers_68() const { return ___m_aryLotsPositionPointsContainers_68; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_aryLotsPositionPointsContainers_68() { return &___m_aryLotsPositionPointsContainers_68; }
	inline void set_m_aryLotsPositionPointsContainers_68(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_aryLotsPositionPointsContainers_68 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryLotsPositionPointsContainers_68), value);
	}

	inline static int32_t get_offset_of_m_StartBelt_69() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_StartBelt_69)); }
	inline StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD * get_m_StartBelt_69() const { return ___m_StartBelt_69; }
	inline StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD ** get_address_of_m_StartBelt_69() { return &___m_StartBelt_69; }
	inline void set_m_StartBelt_69(StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD * value)
	{
		___m_StartBelt_69 = value;
		Il2CppCodeGenWriteBarrier((&___m_StartBelt_69), value);
	}

	inline static int32_t get_offset_of_m_aryRoundTimeByLevel_70() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryRoundTimeByLevel_70)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryRoundTimeByLevel_70() const { return ___m_aryRoundTimeByLevel_70; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryRoundTimeByLevel_70() { return &___m_aryRoundTimeByLevel_70; }
	inline void set_m_aryRoundTimeByLevel_70(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryRoundTimeByLevel_70 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryRoundTimeByLevel_70), value);
	}

	inline static int32_t get_offset_of_m_lstAllPlanes_71() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_lstAllPlanes_71)); }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * get_m_lstAllPlanes_71() const { return ___m_lstAllPlanes_71; }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 ** get_address_of_m_lstAllPlanes_71() { return &___m_lstAllPlanes_71; }
	inline void set_m_lstAllPlanes_71(List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * value)
	{
		___m_lstAllPlanes_71 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstAllPlanes_71), value);
	}

	inline static int32_t get_offset_of_m_vecStartPos_72() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_vecStartPos_72)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecStartPos_72() const { return ___m_vecStartPos_72; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecStartPos_72() { return &___m_vecStartPos_72; }
	inline void set_m_vecStartPos_72(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecStartPos_72 = value;
	}

	inline static int32_t get_offset_of_m_vecNodeStartPos_73() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_vecNodeStartPos_73)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_vecNodeStartPos_73() const { return ___m_vecNodeStartPos_73; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_vecNodeStartPos_73() { return &___m_vecNodeStartPos_73; }
	inline void set_m_vecNodeStartPos_73(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_vecNodeStartPos_73 = value;
		Il2CppCodeGenWriteBarrier((&___m_vecNodeStartPos_73), value);
	}

	inline static int32_t get_offset_of_m_vecNodeCenterPos_74() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_vecNodeCenterPos_74)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_vecNodeCenterPos_74() const { return ___m_vecNodeCenterPos_74; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_vecNodeCenterPos_74() { return &___m_vecNodeCenterPos_74; }
	inline void set_m_vecNodeCenterPos_74(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_vecNodeCenterPos_74 = value;
		Il2CppCodeGenWriteBarrier((&___m_vecNodeCenterPos_74), value);
	}

	inline static int32_t get_offset_of_m_vecNodeEndPos_75() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_vecNodeEndPos_75)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_vecNodeEndPos_75() const { return ___m_vecNodeEndPos_75; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_vecNodeEndPos_75() { return &___m_vecNodeEndPos_75; }
	inline void set_m_vecNodeEndPos_75(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_vecNodeEndPos_75 = value;
		Il2CppCodeGenWriteBarrier((&___m_vecNodeEndPos_75), value);
	}

	inline static int32_t get_offset_of_m_fRadius_76() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fRadius_76)); }
	inline float get_m_fRadius_76() const { return ___m_fRadius_76; }
	inline float* get_address_of_m_fRadius_76() { return &___m_fRadius_76; }
	inline void set_m_fRadius_76(float value)
	{
		___m_fRadius_76 = value;
	}

	inline static int32_t get_offset_of_m_fVertLineLen_77() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fVertLineLen_77)); }
	inline float get_m_fVertLineLen_77() const { return ___m_fVertLineLen_77; }
	inline float* get_address_of_m_fVertLineLen_77() { return &___m_fVertLineLen_77; }
	inline void set_m_fVertLineLen_77(float value)
	{
		___m_fVertLineLen_77 = value;
	}

	inline static int32_t get_offset_of_m_fHoriLineLen_78() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fHoriLineLen_78)); }
	inline float get_m_fHoriLineLen_78() const { return ___m_fHoriLineLen_78; }
	inline float* get_address_of_m_fHoriLineLen_78() { return &___m_fHoriLineLen_78; }
	inline void set_m_fHoriLineLen_78(float value)
	{
		___m_fHoriLineLen_78 = value;
	}

	inline static int32_t get_offset_of_m_fRoundLen_79() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fRoundLen_79)); }
	inline float get_m_fRoundLen_79() const { return ___m_fRoundLen_79; }
	inline float* get_address_of_m_fRoundLen_79() { return &___m_fRoundLen_79; }
	inline void set_m_fRoundLen_79(float value)
	{
		___m_fRoundLen_79 = value;
	}

	inline static int32_t get_offset_of_m_fPosAdjustDistance_80() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fPosAdjustDistance_80)); }
	inline float get_m_fPosAdjustDistance_80() const { return ___m_fPosAdjustDistance_80; }
	inline float* get_address_of_m_fPosAdjustDistance_80() { return &___m_fPosAdjustDistance_80; }
	inline void set_m_fPosAdjustDistance_80(float value)
	{
		___m_fPosAdjustDistance_80 = value;
	}

	inline static int32_t get_offset_of_m_f10SecLoopElapse_82() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_f10SecLoopElapse_82)); }
	inline float get_m_f10SecLoopElapse_82() const { return ___m_f10SecLoopElapse_82; }
	inline float* get_address_of_m_f10SecLoopElapse_82() { return &___m_f10SecLoopElapse_82; }
	inline void set_m_f10SecLoopElapse_82(float value)
	{
		___m_f10SecLoopElapse_82 = value;
	}

	inline static int32_t get_offset_of_m_bIsVisible_83() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_bIsVisible_83)); }
	inline bool get_m_bIsVisible_83() const { return ___m_bIsVisible_83; }
	inline bool* get_address_of_m_bIsVisible_83() { return &___m_bIsVisible_83; }
	inline void set_m_bIsVisible_83(bool value)
	{
		___m_bIsVisible_83 = value;
	}

	inline static int32_t get_offset_of_lstTempPLanes_84() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___lstTempPLanes_84)); }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * get_lstTempPLanes_84() const { return ___lstTempPLanes_84; }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 ** get_address_of_lstTempPLanes_84() { return &___lstTempPLanes_84; }
	inline void set_lstTempPLanes_84(List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * value)
	{
		___lstTempPLanes_84 = value;
		Il2CppCodeGenWriteBarrier((&___lstTempPLanes_84), value);
	}

	inline static int32_t get_offset_of_m_fAccelerate_85() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fAccelerate_85)); }
	inline float get_m_fAccelerate_85() const { return ___m_fAccelerate_85; }
	inline float* get_address_of_m_fAccelerate_85() { return &___m_fAccelerate_85; }
	inline void set_m_fAccelerate_85(float value)
	{
		___m_fAccelerate_85 = value;
	}

	inline static int32_t get_offset_of_m_fPreAccerlateTimeLeft_86() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fPreAccerlateTimeLeft_86)); }
	inline float get_m_fPreAccerlateTimeLeft_86() const { return ___m_fPreAccerlateTimeLeft_86; }
	inline float* get_address_of_m_fPreAccerlateTimeLeft_86() { return &___m_fPreAccerlateTimeLeft_86; }
	inline void set_m_fPreAccerlateTimeLeft_86(float value)
	{
		___m_fPreAccerlateTimeLeft_86 = value;
	}

	inline static int32_t get_offset_of_m_fSpeedAccelerate_87() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fSpeedAccelerate_87)); }
	inline float get_m_fSpeedAccelerate_87() const { return ___m_fSpeedAccelerate_87; }
	inline float* get_address_of_m_fSpeedAccelerate_87() { return &___m_fSpeedAccelerate_87; }
	inline void set_m_fSpeedAccelerate_87(float value)
	{
		___m_fSpeedAccelerate_87 = value;
	}

	inline static int32_t get_offset_of_m_aryAccelerate_89() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryAccelerate_89)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryAccelerate_89() const { return ___m_aryAccelerate_89; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryAccelerate_89() { return &___m_aryAccelerate_89; }
	inline void set_m_aryAccelerate_89(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryAccelerate_89 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAccelerate_89), value);
	}

	inline static int32_t get_offset_of_m_bLostFocus_90() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_bLostFocus_90)); }
	inline bool get_m_bLostFocus_90() const { return ___m_bLostFocus_90; }
	inline bool* get_address_of_m_bLostFocus_90() { return &___m_bLostFocus_90; }
	inline void set_m_bLostFocus_90(bool value)
	{
		___m_bLostFocus_90 = value;
	}

	inline static int32_t get_offset_of__panelAds_91() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____panelAds_91)); }
	inline RuntimeObject * get__panelAds_91() const { return ____panelAds_91; }
	inline RuntimeObject ** get_address_of__panelAds_91() { return &____panelAds_91; }
	inline void set__panelAds_91(RuntimeObject * value)
	{
		____panelAds_91 = value;
		Il2CppCodeGenWriteBarrier((&____panelAds_91), value);
	}

	inline static int32_t get_offset_of_m_fOfflineProfit_92() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fOfflineProfit_92)); }
	inline double get_m_fOfflineProfit_92() const { return ___m_fOfflineProfit_92; }
	inline double* get_address_of_m_fOfflineProfit_92() { return &___m_fOfflineProfit_92; }
	inline void set_m_fOfflineProfit_92(double value)
	{
		___m_fOfflineProfit_92 = value;
	}

	inline static int32_t get_offset_of_vecTempPosLeft_93() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___vecTempPosLeft_93)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPosLeft_93() const { return ___vecTempPosLeft_93; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPosLeft_93() { return &___vecTempPosLeft_93; }
	inline void set_vecTempPosLeft_93(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPosLeft_93 = value;
	}

	inline static int32_t get_offset_of_vecTempPosRight_94() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___vecTempPosRight_94)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPosRight_94() const { return ___vecTempPosRight_94; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPosRight_94() { return &___vecTempPosRight_94; }
	inline void set_vecTempPosRight_94(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPosRight_94 = value;
	}

	inline static int32_t get_offset_of_m_lstRunLocations_95() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_lstRunLocations_95)); }
	inline List_1_t1517B4D31CFE0D5A31B279B7CC226F6BA9FA9E43 * get_m_lstRunLocations_95() const { return ___m_lstRunLocations_95; }
	inline List_1_t1517B4D31CFE0D5A31B279B7CC226F6BA9FA9E43 ** get_address_of_m_lstRunLocations_95() { return &___m_lstRunLocations_95; }
	inline void set_m_lstRunLocations_95(List_1_t1517B4D31CFE0D5A31B279B7CC226F6BA9FA9E43 * value)
	{
		___m_lstRunLocations_95 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRunLocations_95), value);
	}

	inline static int32_t get_offset_of_m_dicLotPosOfLevel_96() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_dicLotPosOfLevel_96)); }
	inline Dictionary_2_t27BCF11A3E40FEB6B728C0EA39A63FAB1AE16D5C * get_m_dicLotPosOfLevel_96() const { return ___m_dicLotPosOfLevel_96; }
	inline Dictionary_2_t27BCF11A3E40FEB6B728C0EA39A63FAB1AE16D5C ** get_address_of_m_dicLotPosOfLevel_96() { return &___m_dicLotPosOfLevel_96; }
	inline void set_m_dicLotPosOfLevel_96(Dictionary_2_t27BCF11A3E40FEB6B728C0EA39A63FAB1AE16D5C * value)
	{
		___m_dicLotPosOfLevel_96 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLotPosOfLevel_96), value);
	}

	inline static int32_t get_offset_of_selected_one_97() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___selected_one_97)); }
	inline sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949  get_selected_one_97() const { return ___selected_one_97; }
	inline sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949 * get_address_of_selected_one_97() { return &___selected_one_97; }
	inline void set_selected_one_97(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949  value)
	{
		___selected_one_97 = value;
	}

	inline static int32_t get_offset_of_m_dShowingNumber_98() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_dShowingNumber_98)); }
	inline double get_m_dShowingNumber_98() const { return ___m_dShowingNumber_98; }
	inline double* get_address_of_m_dShowingNumber_98() { return &___m_dShowingNumber_98; }
	inline void set_m_dShowingNumber_98(double value)
	{
		___m_dShowingNumber_98 = value;
	}

	inline static int32_t get_offset_of_m_dNumberChangeSpeed_99() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_dNumberChangeSpeed_99)); }
	inline double get_m_dNumberChangeSpeed_99() const { return ___m_dNumberChangeSpeed_99; }
	inline double* get_address_of_m_dNumberChangeSpeed_99() { return &___m_dNumberChangeSpeed_99; }
	inline void set_m_dNumberChangeSpeed_99(double value)
	{
		___m_dNumberChangeSpeed_99 = value;
	}

	inline static int32_t get_offset_of_m_fCoinChangeTime_100() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fCoinChangeTime_100)); }
	inline float get_m_fCoinChangeTime_100() const { return ___m_fCoinChangeTime_100; }
	inline float* get_address_of_m_fCoinChangeTime_100() { return &___m_fCoinChangeTime_100; }
	inline void set_m_fCoinChangeTime_100(float value)
	{
		___m_fCoinChangeTime_100 = value;
	}

	inline static int32_t get_offset_of_m_dDestCoinNumber_101() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_dDestCoinNumber_101)); }
	inline double get_m_dDestCoinNumber_101() const { return ___m_dDestCoinNumber_101; }
	inline double* get_address_of_m_dDestCoinNumber_101() { return &___m_dDestCoinNumber_101; }
	inline void set_m_dDestCoinNumber_101(double value)
	{
		___m_dDestCoinNumber_101 = value;
	}
};

struct Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields
{
public:
	// UnityEngine.Vector3 Main::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_18;
	// UnityEngine.Vector3 Main::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_19;
	// UnityEngine.Color Main::colorTemp
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___colorTemp_20;
	// Main Main::s_Instance
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1 * ___s_Instance_21;

public:
	inline static int32_t get_offset_of_vecTempPos_18() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields, ___vecTempPos_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_18() const { return ___vecTempPos_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_18() { return &___vecTempPos_18; }
	inline void set_vecTempPos_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_18 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_19() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields, ___vecTempScale_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_19() const { return ___vecTempScale_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_19() { return &___vecTempScale_19; }
	inline void set_vecTempScale_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_19 = value;
	}

	inline static int32_t get_offset_of_colorTemp_20() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields, ___colorTemp_20)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_colorTemp_20() const { return ___colorTemp_20; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_colorTemp_20() { return &___colorTemp_20; }
	inline void set_colorTemp_20(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___colorTemp_20 = value;
	}

	inline static int32_t get_offset_of_s_Instance_21() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields, ___s_Instance_21)); }
	inline Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1 * get_s_Instance_21() const { return ___s_Instance_21; }
	inline Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1 ** get_address_of_s_Instance_21() { return &___s_Instance_21; }
	inline void set_s_Instance_21(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1 * value)
	{
		___s_Instance_21 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAIN_T7BD87A0CB813F834A2F93006206A3CE79188D0C1_H
#ifndef MAPMANAGER_T76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_H
#define MAPMANAGER_T76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapManager
struct  MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image MapManager::_imgCPosCoinIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCPosCoinIcon_4;
	// UnityEngine.UI.Text MapManager::_txtCurPlanetName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCurPlanetName_5;
	// UnityEngine.GameObject MapManager::_skeletonEffectCurTrack_Container
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____skeletonEffectCurTrack_Container_6;
	// Spine.Unity.SkeletonGraphic MapManager::_skeletonEffectCurTrack
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * ____skeletonEffectCurTrack_7;
	// UnityEngine.Color MapManager::m_colorPlanetUnlockBlack
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorPlanetUnlockBlack_8;
	// UnityEngine.Color MapManager::m_colorBlackWhite
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorBlackWhite_9;
	// UnityEngine.Color MapManager::m_colorOutlineNormal
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorOutlineNormal_10;
	// UnityEngine.Color MapManager::m_colorOutlineCurTrack
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorOutlineCurTrack_11;
	// UnityEngine.Material MapManager::m_matBlackWhite
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_matBlackWhite_12;
	// UnityEngine.Sprite MapManager::m_sprBlockBgCurTrack
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBlockBgCurTrack_13;
	// UnityEngine.Sprite MapManager::m_sprBlockBgNormal
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBlockBgNormal_14;
	// UnityEngine.Sprite MapManager::m_sprBlockBgCanNotUnlock
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBlockBgCanNotUnlock_15;
	// System.Single MapManager::m_fSlideTime
	float ___m_fSlideTime_16;
	// System.Single[] MapManager::m_aryTheMainlandCenters
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryTheMainlandCenters_17;
	// System.String[] MapManager::m_aryDistrictName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_aryDistrictName_21;
	// UnityEngine.Sprite MapManager::m_sprBg_Locked_Left
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBg_Locked_Left_22;
	// UnityEngine.Sprite MapManager::m_sprBg_Locked_Right
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBg_Locked_Right_23;
	// UnityEngine.Sprite MapManager::m_sprBgUnlocked_Left
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBgUnlocked_Left_24;
	// UnityEngine.Sprite MapManager::m_sprBgUnlocked_Right
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBgUnlocked_Right_25;
	// MoneyCounter[] MapManager::m_aryMoneyCounters
	MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* ___m_aryMoneyCounters_26;
	// UnityEngine.UI.Image[] MapManager::m_aryVariousCoinIcons
	ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* ___m_aryVariousCoinIcons_27;
	// UnityEngine.UI.Button MapManager::_btnAdventure
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnAdventure_28;
	// UnityEngine.GameObject MapManager::_containerZengShouCounters
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerZengShouCounters_29;
	// UnityEngine.GameObject MapManager::_containerRecycledCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerRecycledCounter_30;
	// UnityEngine.GameObject MapManager::_panelZengShou
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelZengShou_31;
	// UnityEngine.GameObject MapManager::_panelPanelDetails
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelPanelDetails_32;
	// UnityEngine.UI.Text MapManager::_txtPanelDetailstitle
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPanelDetailstitle_33;
	// UIDistrict[] MapManager::m_aryUIDistricts
	UIDistrictU5BU5D_tDD148C70027D94D63D232B5C9FBB142D5EA721CB* ___m_aryUIDistricts_34;
	// UIPlanet[] MapManager::m_aryUIPlanets
	UIPlanetU5BU5D_t8587E1E90D0FB3095F0BBAA9DF7CE4A5A6A94298* ___m_aryUIPlanets_35;
	// UnityEngine.GameObject MapManager::_panelUnlockDistrict
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelUnlockDistrict_36;
	// UnityEngine.UI.Text MapManager::_txtUnlockDistrictCost
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtUnlockDistrictCost_37;
	// UnityEngine.UI.Image MapManager::_imgUnlockTrackPriceIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgUnlockTrackPriceIcon_38;
	// UnityEngine.GameObject MapManager::_panelUnlockPlanet
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelUnlockPlanet_39;
	// UnityEngine.UI.Text MapManager::_txtUnlockPlanetCost
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtUnlockPlanetCost_40;
	// UnityEngine.UI.Image MapManager::_imgUnlockPlanetPriceIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgUnlockPlanetPriceIcon_41;
	// UnityEngine.GameObject MapManager::_panelEntering
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelEntering_42;
	// UnityEngine.GameObject MapManager::_containerPlanets
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerPlanets_43;
	// UnityEngine.GameObject MapManager::_containerMainPlanets
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerMainPlanets_44;
	// UnityEngine.GameObject MapManager::_containerOtherPlanets
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerOtherPlanets_45;
	// UnityEngine.GameObject MapManager::_panelScienceTree
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelScienceTree_46;
	// UnityEngine.UI.Text MapManager::_txtCurPlanetAndTrack
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCurPlanetAndTrack_47;
	// UnityEngine.UI.Text MapManager::_txtCurTrackLevel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCurTrackLevel_48;
	// UnityEngine.GameObject[] MapManager::m_aryPlanetTitle
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryPlanetTitle_49;
	// UnityEngine.GameObject MapManager::_subpanelBatCollectOffline
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____subpanelBatCollectOffline_50;
	// UnityEngine.UI.Text MapManager::_txtBatOfflineGain
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtBatOfflineGain_51;
	// UnityEngine.UI.Text MapManager::_txtBatOfflineDetail
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtBatOfflineDetail_52;
	// UnityEngine.UI.Text MapManager::_txtBatOfflineTitle
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtBatOfflineTitle_53;
	// UnityEngine.GameObject MapManager::_goStarSky
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goStarSky_54;
	// System.Single MapManager::m_fStarSkyMoveSpeed
	float ___m_fStarSkyMoveSpeed_55;
	// Planet[] MapManager::m_aryPlanets
	PlanetU5BU5D_tFAD0DE71ED69B99F92FC1525DDAE53548F72893D* ___m_aryPlanets_56;
	// Planet MapManager::m_CurPlanet
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * ___m_CurPlanet_57;
	// District MapManager::m_CurDistrict
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * ___m_CurDistrict_58;
	// System.Int32 MapManager::m_nCurShowPlanetDetialIdOnUI
	int32_t ___m_nCurShowPlanetDetialIdOnUI_59;
	// System.Collections.Generic.Dictionary`2<System.String,UIZengShouCounter> MapManager::m_dicZengShouCounters
	Dictionary_2_tA42C789C20DBB051A4F863D661877067E6AE8081 * ___m_dicZengShouCounters_60;
	// UnityEngine.UI.Text[] MapManager::m_aryCoin0Value
	TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* ___m_aryCoin0Value_61;
	// UnityEngine.UI.Text[] MapManager::m_aryCoin1Value
	TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* ___m_aryCoin1Value_62;
	// UnityEngine.UI.Text[] MapManager::m_aryCoin2Value
	TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* ___m_aryCoin2Value_63;
	// UnityEngine.UI.Text[] MapManager::m_aryDiamondValue
	TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* ___m_aryDiamondValue_64;
	// UnityEngine.UI.Button MapManager::_btnCurEnterButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnCurEnterButton_65;
	// System.Single MapManager::m_fUpdatePlanetInfoLoopTimeElapse
	float ___m_fUpdatePlanetInfoLoopTimeElapse_66;
	// Planet MapManager::m_PlanetToPrestige
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * ___m_PlanetToPrestige_67;
	// District MapManager::m_DistrictToPrestige
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * ___m_DistrictToPrestige_68;
	// System.Boolean MapManager::m_bPlanetDetailPanelOpen
	bool ___m_bPlanetDetailPanelOpen_69;
	// District MapManager::m_districtToUnlock
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * ___m_districtToUnlock_70;
	// Planet MapManager::m_planetToUnlock
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * ___m_planetToUnlock_71;
	// System.Int32 MapManager::m_nToEnterRace_PlanetId
	int32_t ___m_nToEnterRace_PlanetId_72;
	// System.Int32 MapManager::m_nToEnterRace_DistrictId
	int32_t ___m_nToEnterRace_DistrictId_73;
	// System.Single MapManager::m_fPreEnterRaceLoopTime
	float ___m_fPreEnterRaceLoopTime_74;
	// System.Boolean MapManager::m_bStarSkyMoving
	bool ___m_bStarSkyMoving_75;
	// System.Boolean MapManager::m_bIsShowingZengShouPanel
	bool ___m_bIsShowingZengShouPanel_76;
	// System.Boolean MapManager::m_bTestShow
	bool ___m_bTestShow_77;
	// System.Boolean MapManager::m_bSliding
	bool ___m_bSliding_78;
	// System.Boolean MapManager::m_bBigMapShowing
	bool ___m_bBigMapShowing_79;
	// System.Single MapManager::m_fSlideSpeed
	float ___m_fSlideSpeed_80;
	// System.Single MapManager::m_fSlideA
	float ___m_fSlideA_81;
	// System.Single MapManager::m_fV0
	float ___m_fV0_82;
	// UnityEngine.GameObject MapManager::_goSVContent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goSVContent_83;
	// System.Single MapManager::m_fDestPos
	float ___m_fDestPos_84;
	// System.Single MapManager::m_fStartDragPos
	float ___m_fStartDragPos_85;
	// System.Single MapManager::m_fEndDragPos
	float ___m_fEndDragPos_86;
	// System.Boolean MapManager::m_bDragging
	bool ___m_bDragging_87;
	// System.Single MapManager::m_fMouseStartPos
	float ___m_fMouseStartPos_88;
	// System.Boolean MapManager::m_bClickProhibit
	bool ___m_bClickProhibit_89;
	// System.Single MapManager::m_fClickProhibitTimeLeft
	float ___m_fClickProhibitTimeLeft_90;
	// System.Int32 MapManager::m_nCurSelctedIndex
	int32_t ___m_nCurSelctedIndex_91;
	// UIMainLand[] MapManager::m_aryMainLands
	UIMainLandU5BU5D_tF159C8131B1BA4E2EEDF316C136F4BA6CE07B280* ___m_aryMainLands_92;

public:
	inline static int32_t get_offset_of__imgCPosCoinIcon_4() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____imgCPosCoinIcon_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCPosCoinIcon_4() const { return ____imgCPosCoinIcon_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCPosCoinIcon_4() { return &____imgCPosCoinIcon_4; }
	inline void set__imgCPosCoinIcon_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCPosCoinIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&____imgCPosCoinIcon_4), value);
	}

	inline static int32_t get_offset_of__txtCurPlanetName_5() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtCurPlanetName_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCurPlanetName_5() const { return ____txtCurPlanetName_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCurPlanetName_5() { return &____txtCurPlanetName_5; }
	inline void set__txtCurPlanetName_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCurPlanetName_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurPlanetName_5), value);
	}

	inline static int32_t get_offset_of__skeletonEffectCurTrack_Container_6() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____skeletonEffectCurTrack_Container_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__skeletonEffectCurTrack_Container_6() const { return ____skeletonEffectCurTrack_Container_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__skeletonEffectCurTrack_Container_6() { return &____skeletonEffectCurTrack_Container_6; }
	inline void set__skeletonEffectCurTrack_Container_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____skeletonEffectCurTrack_Container_6 = value;
		Il2CppCodeGenWriteBarrier((&____skeletonEffectCurTrack_Container_6), value);
	}

	inline static int32_t get_offset_of__skeletonEffectCurTrack_7() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____skeletonEffectCurTrack_7)); }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * get__skeletonEffectCurTrack_7() const { return ____skeletonEffectCurTrack_7; }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A ** get_address_of__skeletonEffectCurTrack_7() { return &____skeletonEffectCurTrack_7; }
	inline void set__skeletonEffectCurTrack_7(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * value)
	{
		____skeletonEffectCurTrack_7 = value;
		Il2CppCodeGenWriteBarrier((&____skeletonEffectCurTrack_7), value);
	}

	inline static int32_t get_offset_of_m_colorPlanetUnlockBlack_8() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_colorPlanetUnlockBlack_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorPlanetUnlockBlack_8() const { return ___m_colorPlanetUnlockBlack_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorPlanetUnlockBlack_8() { return &___m_colorPlanetUnlockBlack_8; }
	inline void set_m_colorPlanetUnlockBlack_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorPlanetUnlockBlack_8 = value;
	}

	inline static int32_t get_offset_of_m_colorBlackWhite_9() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_colorBlackWhite_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorBlackWhite_9() const { return ___m_colorBlackWhite_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorBlackWhite_9() { return &___m_colorBlackWhite_9; }
	inline void set_m_colorBlackWhite_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorBlackWhite_9 = value;
	}

	inline static int32_t get_offset_of_m_colorOutlineNormal_10() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_colorOutlineNormal_10)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorOutlineNormal_10() const { return ___m_colorOutlineNormal_10; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorOutlineNormal_10() { return &___m_colorOutlineNormal_10; }
	inline void set_m_colorOutlineNormal_10(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorOutlineNormal_10 = value;
	}

	inline static int32_t get_offset_of_m_colorOutlineCurTrack_11() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_colorOutlineCurTrack_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorOutlineCurTrack_11() const { return ___m_colorOutlineCurTrack_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorOutlineCurTrack_11() { return &___m_colorOutlineCurTrack_11; }
	inline void set_m_colorOutlineCurTrack_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorOutlineCurTrack_11 = value;
	}

	inline static int32_t get_offset_of_m_matBlackWhite_12() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_matBlackWhite_12)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_matBlackWhite_12() const { return ___m_matBlackWhite_12; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_matBlackWhite_12() { return &___m_matBlackWhite_12; }
	inline void set_m_matBlackWhite_12(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_matBlackWhite_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_matBlackWhite_12), value);
	}

	inline static int32_t get_offset_of_m_sprBlockBgCurTrack_13() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBlockBgCurTrack_13)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBlockBgCurTrack_13() const { return ___m_sprBlockBgCurTrack_13; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBlockBgCurTrack_13() { return &___m_sprBlockBgCurTrack_13; }
	inline void set_m_sprBlockBgCurTrack_13(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBlockBgCurTrack_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBlockBgCurTrack_13), value);
	}

	inline static int32_t get_offset_of_m_sprBlockBgNormal_14() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBlockBgNormal_14)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBlockBgNormal_14() const { return ___m_sprBlockBgNormal_14; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBlockBgNormal_14() { return &___m_sprBlockBgNormal_14; }
	inline void set_m_sprBlockBgNormal_14(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBlockBgNormal_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBlockBgNormal_14), value);
	}

	inline static int32_t get_offset_of_m_sprBlockBgCanNotUnlock_15() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBlockBgCanNotUnlock_15)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBlockBgCanNotUnlock_15() const { return ___m_sprBlockBgCanNotUnlock_15; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBlockBgCanNotUnlock_15() { return &___m_sprBlockBgCanNotUnlock_15; }
	inline void set_m_sprBlockBgCanNotUnlock_15(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBlockBgCanNotUnlock_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBlockBgCanNotUnlock_15), value);
	}

	inline static int32_t get_offset_of_m_fSlideTime_16() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fSlideTime_16)); }
	inline float get_m_fSlideTime_16() const { return ___m_fSlideTime_16; }
	inline float* get_address_of_m_fSlideTime_16() { return &___m_fSlideTime_16; }
	inline void set_m_fSlideTime_16(float value)
	{
		___m_fSlideTime_16 = value;
	}

	inline static int32_t get_offset_of_m_aryTheMainlandCenters_17() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryTheMainlandCenters_17)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryTheMainlandCenters_17() const { return ___m_aryTheMainlandCenters_17; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryTheMainlandCenters_17() { return &___m_aryTheMainlandCenters_17; }
	inline void set_m_aryTheMainlandCenters_17(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryTheMainlandCenters_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryTheMainlandCenters_17), value);
	}

	inline static int32_t get_offset_of_m_aryDistrictName_21() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryDistrictName_21)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_aryDistrictName_21() const { return ___m_aryDistrictName_21; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_aryDistrictName_21() { return &___m_aryDistrictName_21; }
	inline void set_m_aryDistrictName_21(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_aryDistrictName_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryDistrictName_21), value);
	}

	inline static int32_t get_offset_of_m_sprBg_Locked_Left_22() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBg_Locked_Left_22)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBg_Locked_Left_22() const { return ___m_sprBg_Locked_Left_22; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBg_Locked_Left_22() { return &___m_sprBg_Locked_Left_22; }
	inline void set_m_sprBg_Locked_Left_22(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBg_Locked_Left_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBg_Locked_Left_22), value);
	}

	inline static int32_t get_offset_of_m_sprBg_Locked_Right_23() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBg_Locked_Right_23)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBg_Locked_Right_23() const { return ___m_sprBg_Locked_Right_23; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBg_Locked_Right_23() { return &___m_sprBg_Locked_Right_23; }
	inline void set_m_sprBg_Locked_Right_23(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBg_Locked_Right_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBg_Locked_Right_23), value);
	}

	inline static int32_t get_offset_of_m_sprBgUnlocked_Left_24() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBgUnlocked_Left_24)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBgUnlocked_Left_24() const { return ___m_sprBgUnlocked_Left_24; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBgUnlocked_Left_24() { return &___m_sprBgUnlocked_Left_24; }
	inline void set_m_sprBgUnlocked_Left_24(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBgUnlocked_Left_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBgUnlocked_Left_24), value);
	}

	inline static int32_t get_offset_of_m_sprBgUnlocked_Right_25() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBgUnlocked_Right_25)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBgUnlocked_Right_25() const { return ___m_sprBgUnlocked_Right_25; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBgUnlocked_Right_25() { return &___m_sprBgUnlocked_Right_25; }
	inline void set_m_sprBgUnlocked_Right_25(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBgUnlocked_Right_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBgUnlocked_Right_25), value);
	}

	inline static int32_t get_offset_of_m_aryMoneyCounters_26() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryMoneyCounters_26)); }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* get_m_aryMoneyCounters_26() const { return ___m_aryMoneyCounters_26; }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A** get_address_of_m_aryMoneyCounters_26() { return &___m_aryMoneyCounters_26; }
	inline void set_m_aryMoneyCounters_26(MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* value)
	{
		___m_aryMoneyCounters_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMoneyCounters_26), value);
	}

	inline static int32_t get_offset_of_m_aryVariousCoinIcons_27() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryVariousCoinIcons_27)); }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* get_m_aryVariousCoinIcons_27() const { return ___m_aryVariousCoinIcons_27; }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D** get_address_of_m_aryVariousCoinIcons_27() { return &___m_aryVariousCoinIcons_27; }
	inline void set_m_aryVariousCoinIcons_27(ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* value)
	{
		___m_aryVariousCoinIcons_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryVariousCoinIcons_27), value);
	}

	inline static int32_t get_offset_of__btnAdventure_28() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____btnAdventure_28)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnAdventure_28() const { return ____btnAdventure_28; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnAdventure_28() { return &____btnAdventure_28; }
	inline void set__btnAdventure_28(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnAdventure_28 = value;
		Il2CppCodeGenWriteBarrier((&____btnAdventure_28), value);
	}

	inline static int32_t get_offset_of__containerZengShouCounters_29() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____containerZengShouCounters_29)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerZengShouCounters_29() const { return ____containerZengShouCounters_29; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerZengShouCounters_29() { return &____containerZengShouCounters_29; }
	inline void set__containerZengShouCounters_29(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerZengShouCounters_29 = value;
		Il2CppCodeGenWriteBarrier((&____containerZengShouCounters_29), value);
	}

	inline static int32_t get_offset_of__containerRecycledCounter_30() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____containerRecycledCounter_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerRecycledCounter_30() const { return ____containerRecycledCounter_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerRecycledCounter_30() { return &____containerRecycledCounter_30; }
	inline void set__containerRecycledCounter_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerRecycledCounter_30 = value;
		Il2CppCodeGenWriteBarrier((&____containerRecycledCounter_30), value);
	}

	inline static int32_t get_offset_of__panelZengShou_31() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____panelZengShou_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelZengShou_31() const { return ____panelZengShou_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelZengShou_31() { return &____panelZengShou_31; }
	inline void set__panelZengShou_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelZengShou_31 = value;
		Il2CppCodeGenWriteBarrier((&____panelZengShou_31), value);
	}

	inline static int32_t get_offset_of__panelPanelDetails_32() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____panelPanelDetails_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelPanelDetails_32() const { return ____panelPanelDetails_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelPanelDetails_32() { return &____panelPanelDetails_32; }
	inline void set__panelPanelDetails_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelPanelDetails_32 = value;
		Il2CppCodeGenWriteBarrier((&____panelPanelDetails_32), value);
	}

	inline static int32_t get_offset_of__txtPanelDetailstitle_33() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtPanelDetailstitle_33)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPanelDetailstitle_33() const { return ____txtPanelDetailstitle_33; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPanelDetailstitle_33() { return &____txtPanelDetailstitle_33; }
	inline void set__txtPanelDetailstitle_33(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPanelDetailstitle_33 = value;
		Il2CppCodeGenWriteBarrier((&____txtPanelDetailstitle_33), value);
	}

	inline static int32_t get_offset_of_m_aryUIDistricts_34() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryUIDistricts_34)); }
	inline UIDistrictU5BU5D_tDD148C70027D94D63D232B5C9FBB142D5EA721CB* get_m_aryUIDistricts_34() const { return ___m_aryUIDistricts_34; }
	inline UIDistrictU5BU5D_tDD148C70027D94D63D232B5C9FBB142D5EA721CB** get_address_of_m_aryUIDistricts_34() { return &___m_aryUIDistricts_34; }
	inline void set_m_aryUIDistricts_34(UIDistrictU5BU5D_tDD148C70027D94D63D232B5C9FBB142D5EA721CB* value)
	{
		___m_aryUIDistricts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUIDistricts_34), value);
	}

	inline static int32_t get_offset_of_m_aryUIPlanets_35() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryUIPlanets_35)); }
	inline UIPlanetU5BU5D_t8587E1E90D0FB3095F0BBAA9DF7CE4A5A6A94298* get_m_aryUIPlanets_35() const { return ___m_aryUIPlanets_35; }
	inline UIPlanetU5BU5D_t8587E1E90D0FB3095F0BBAA9DF7CE4A5A6A94298** get_address_of_m_aryUIPlanets_35() { return &___m_aryUIPlanets_35; }
	inline void set_m_aryUIPlanets_35(UIPlanetU5BU5D_t8587E1E90D0FB3095F0BBAA9DF7CE4A5A6A94298* value)
	{
		___m_aryUIPlanets_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUIPlanets_35), value);
	}

	inline static int32_t get_offset_of__panelUnlockDistrict_36() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____panelUnlockDistrict_36)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelUnlockDistrict_36() const { return ____panelUnlockDistrict_36; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelUnlockDistrict_36() { return &____panelUnlockDistrict_36; }
	inline void set__panelUnlockDistrict_36(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelUnlockDistrict_36 = value;
		Il2CppCodeGenWriteBarrier((&____panelUnlockDistrict_36), value);
	}

	inline static int32_t get_offset_of__txtUnlockDistrictCost_37() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtUnlockDistrictCost_37)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtUnlockDistrictCost_37() const { return ____txtUnlockDistrictCost_37; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtUnlockDistrictCost_37() { return &____txtUnlockDistrictCost_37; }
	inline void set__txtUnlockDistrictCost_37(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtUnlockDistrictCost_37 = value;
		Il2CppCodeGenWriteBarrier((&____txtUnlockDistrictCost_37), value);
	}

	inline static int32_t get_offset_of__imgUnlockTrackPriceIcon_38() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____imgUnlockTrackPriceIcon_38)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgUnlockTrackPriceIcon_38() const { return ____imgUnlockTrackPriceIcon_38; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgUnlockTrackPriceIcon_38() { return &____imgUnlockTrackPriceIcon_38; }
	inline void set__imgUnlockTrackPriceIcon_38(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgUnlockTrackPriceIcon_38 = value;
		Il2CppCodeGenWriteBarrier((&____imgUnlockTrackPriceIcon_38), value);
	}

	inline static int32_t get_offset_of__panelUnlockPlanet_39() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____panelUnlockPlanet_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelUnlockPlanet_39() const { return ____panelUnlockPlanet_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelUnlockPlanet_39() { return &____panelUnlockPlanet_39; }
	inline void set__panelUnlockPlanet_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelUnlockPlanet_39 = value;
		Il2CppCodeGenWriteBarrier((&____panelUnlockPlanet_39), value);
	}

	inline static int32_t get_offset_of__txtUnlockPlanetCost_40() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtUnlockPlanetCost_40)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtUnlockPlanetCost_40() const { return ____txtUnlockPlanetCost_40; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtUnlockPlanetCost_40() { return &____txtUnlockPlanetCost_40; }
	inline void set__txtUnlockPlanetCost_40(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtUnlockPlanetCost_40 = value;
		Il2CppCodeGenWriteBarrier((&____txtUnlockPlanetCost_40), value);
	}

	inline static int32_t get_offset_of__imgUnlockPlanetPriceIcon_41() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____imgUnlockPlanetPriceIcon_41)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgUnlockPlanetPriceIcon_41() const { return ____imgUnlockPlanetPriceIcon_41; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgUnlockPlanetPriceIcon_41() { return &____imgUnlockPlanetPriceIcon_41; }
	inline void set__imgUnlockPlanetPriceIcon_41(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgUnlockPlanetPriceIcon_41 = value;
		Il2CppCodeGenWriteBarrier((&____imgUnlockPlanetPriceIcon_41), value);
	}

	inline static int32_t get_offset_of__panelEntering_42() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____panelEntering_42)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelEntering_42() const { return ____panelEntering_42; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelEntering_42() { return &____panelEntering_42; }
	inline void set__panelEntering_42(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelEntering_42 = value;
		Il2CppCodeGenWriteBarrier((&____panelEntering_42), value);
	}

	inline static int32_t get_offset_of__containerPlanets_43() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____containerPlanets_43)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerPlanets_43() const { return ____containerPlanets_43; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerPlanets_43() { return &____containerPlanets_43; }
	inline void set__containerPlanets_43(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerPlanets_43 = value;
		Il2CppCodeGenWriteBarrier((&____containerPlanets_43), value);
	}

	inline static int32_t get_offset_of__containerMainPlanets_44() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____containerMainPlanets_44)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerMainPlanets_44() const { return ____containerMainPlanets_44; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerMainPlanets_44() { return &____containerMainPlanets_44; }
	inline void set__containerMainPlanets_44(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerMainPlanets_44 = value;
		Il2CppCodeGenWriteBarrier((&____containerMainPlanets_44), value);
	}

	inline static int32_t get_offset_of__containerOtherPlanets_45() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____containerOtherPlanets_45)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerOtherPlanets_45() const { return ____containerOtherPlanets_45; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerOtherPlanets_45() { return &____containerOtherPlanets_45; }
	inline void set__containerOtherPlanets_45(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerOtherPlanets_45 = value;
		Il2CppCodeGenWriteBarrier((&____containerOtherPlanets_45), value);
	}

	inline static int32_t get_offset_of__panelScienceTree_46() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____panelScienceTree_46)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelScienceTree_46() const { return ____panelScienceTree_46; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelScienceTree_46() { return &____panelScienceTree_46; }
	inline void set__panelScienceTree_46(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelScienceTree_46 = value;
		Il2CppCodeGenWriteBarrier((&____panelScienceTree_46), value);
	}

	inline static int32_t get_offset_of__txtCurPlanetAndTrack_47() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtCurPlanetAndTrack_47)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCurPlanetAndTrack_47() const { return ____txtCurPlanetAndTrack_47; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCurPlanetAndTrack_47() { return &____txtCurPlanetAndTrack_47; }
	inline void set__txtCurPlanetAndTrack_47(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCurPlanetAndTrack_47 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurPlanetAndTrack_47), value);
	}

	inline static int32_t get_offset_of__txtCurTrackLevel_48() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtCurTrackLevel_48)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCurTrackLevel_48() const { return ____txtCurTrackLevel_48; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCurTrackLevel_48() { return &____txtCurTrackLevel_48; }
	inline void set__txtCurTrackLevel_48(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCurTrackLevel_48 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurTrackLevel_48), value);
	}

	inline static int32_t get_offset_of_m_aryPlanetTitle_49() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryPlanetTitle_49)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryPlanetTitle_49() const { return ___m_aryPlanetTitle_49; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryPlanetTitle_49() { return &___m_aryPlanetTitle_49; }
	inline void set_m_aryPlanetTitle_49(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryPlanetTitle_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPlanetTitle_49), value);
	}

	inline static int32_t get_offset_of__subpanelBatCollectOffline_50() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____subpanelBatCollectOffline_50)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__subpanelBatCollectOffline_50() const { return ____subpanelBatCollectOffline_50; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__subpanelBatCollectOffline_50() { return &____subpanelBatCollectOffline_50; }
	inline void set__subpanelBatCollectOffline_50(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____subpanelBatCollectOffline_50 = value;
		Il2CppCodeGenWriteBarrier((&____subpanelBatCollectOffline_50), value);
	}

	inline static int32_t get_offset_of__txtBatOfflineGain_51() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtBatOfflineGain_51)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtBatOfflineGain_51() const { return ____txtBatOfflineGain_51; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtBatOfflineGain_51() { return &____txtBatOfflineGain_51; }
	inline void set__txtBatOfflineGain_51(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtBatOfflineGain_51 = value;
		Il2CppCodeGenWriteBarrier((&____txtBatOfflineGain_51), value);
	}

	inline static int32_t get_offset_of__txtBatOfflineDetail_52() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtBatOfflineDetail_52)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtBatOfflineDetail_52() const { return ____txtBatOfflineDetail_52; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtBatOfflineDetail_52() { return &____txtBatOfflineDetail_52; }
	inline void set__txtBatOfflineDetail_52(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtBatOfflineDetail_52 = value;
		Il2CppCodeGenWriteBarrier((&____txtBatOfflineDetail_52), value);
	}

	inline static int32_t get_offset_of__txtBatOfflineTitle_53() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtBatOfflineTitle_53)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtBatOfflineTitle_53() const { return ____txtBatOfflineTitle_53; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtBatOfflineTitle_53() { return &____txtBatOfflineTitle_53; }
	inline void set__txtBatOfflineTitle_53(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtBatOfflineTitle_53 = value;
		Il2CppCodeGenWriteBarrier((&____txtBatOfflineTitle_53), value);
	}

	inline static int32_t get_offset_of__goStarSky_54() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____goStarSky_54)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goStarSky_54() const { return ____goStarSky_54; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goStarSky_54() { return &____goStarSky_54; }
	inline void set__goStarSky_54(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goStarSky_54 = value;
		Il2CppCodeGenWriteBarrier((&____goStarSky_54), value);
	}

	inline static int32_t get_offset_of_m_fStarSkyMoveSpeed_55() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fStarSkyMoveSpeed_55)); }
	inline float get_m_fStarSkyMoveSpeed_55() const { return ___m_fStarSkyMoveSpeed_55; }
	inline float* get_address_of_m_fStarSkyMoveSpeed_55() { return &___m_fStarSkyMoveSpeed_55; }
	inline void set_m_fStarSkyMoveSpeed_55(float value)
	{
		___m_fStarSkyMoveSpeed_55 = value;
	}

	inline static int32_t get_offset_of_m_aryPlanets_56() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryPlanets_56)); }
	inline PlanetU5BU5D_tFAD0DE71ED69B99F92FC1525DDAE53548F72893D* get_m_aryPlanets_56() const { return ___m_aryPlanets_56; }
	inline PlanetU5BU5D_tFAD0DE71ED69B99F92FC1525DDAE53548F72893D** get_address_of_m_aryPlanets_56() { return &___m_aryPlanets_56; }
	inline void set_m_aryPlanets_56(PlanetU5BU5D_tFAD0DE71ED69B99F92FC1525DDAE53548F72893D* value)
	{
		___m_aryPlanets_56 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPlanets_56), value);
	}

	inline static int32_t get_offset_of_m_CurPlanet_57() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_CurPlanet_57)); }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * get_m_CurPlanet_57() const { return ___m_CurPlanet_57; }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B ** get_address_of_m_CurPlanet_57() { return &___m_CurPlanet_57; }
	inline void set_m_CurPlanet_57(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * value)
	{
		___m_CurPlanet_57 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurPlanet_57), value);
	}

	inline static int32_t get_offset_of_m_CurDistrict_58() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_CurDistrict_58)); }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * get_m_CurDistrict_58() const { return ___m_CurDistrict_58; }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A ** get_address_of_m_CurDistrict_58() { return &___m_CurDistrict_58; }
	inline void set_m_CurDistrict_58(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * value)
	{
		___m_CurDistrict_58 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurDistrict_58), value);
	}

	inline static int32_t get_offset_of_m_nCurShowPlanetDetialIdOnUI_59() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_nCurShowPlanetDetialIdOnUI_59)); }
	inline int32_t get_m_nCurShowPlanetDetialIdOnUI_59() const { return ___m_nCurShowPlanetDetialIdOnUI_59; }
	inline int32_t* get_address_of_m_nCurShowPlanetDetialIdOnUI_59() { return &___m_nCurShowPlanetDetialIdOnUI_59; }
	inline void set_m_nCurShowPlanetDetialIdOnUI_59(int32_t value)
	{
		___m_nCurShowPlanetDetialIdOnUI_59 = value;
	}

	inline static int32_t get_offset_of_m_dicZengShouCounters_60() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_dicZengShouCounters_60)); }
	inline Dictionary_2_tA42C789C20DBB051A4F863D661877067E6AE8081 * get_m_dicZengShouCounters_60() const { return ___m_dicZengShouCounters_60; }
	inline Dictionary_2_tA42C789C20DBB051A4F863D661877067E6AE8081 ** get_address_of_m_dicZengShouCounters_60() { return &___m_dicZengShouCounters_60; }
	inline void set_m_dicZengShouCounters_60(Dictionary_2_tA42C789C20DBB051A4F863D661877067E6AE8081 * value)
	{
		___m_dicZengShouCounters_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicZengShouCounters_60), value);
	}

	inline static int32_t get_offset_of_m_aryCoin0Value_61() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryCoin0Value_61)); }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* get_m_aryCoin0Value_61() const { return ___m_aryCoin0Value_61; }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188** get_address_of_m_aryCoin0Value_61() { return &___m_aryCoin0Value_61; }
	inline void set_m_aryCoin0Value_61(TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* value)
	{
		___m_aryCoin0Value_61 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoin0Value_61), value);
	}

	inline static int32_t get_offset_of_m_aryCoin1Value_62() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryCoin1Value_62)); }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* get_m_aryCoin1Value_62() const { return ___m_aryCoin1Value_62; }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188** get_address_of_m_aryCoin1Value_62() { return &___m_aryCoin1Value_62; }
	inline void set_m_aryCoin1Value_62(TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* value)
	{
		___m_aryCoin1Value_62 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoin1Value_62), value);
	}

	inline static int32_t get_offset_of_m_aryCoin2Value_63() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryCoin2Value_63)); }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* get_m_aryCoin2Value_63() const { return ___m_aryCoin2Value_63; }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188** get_address_of_m_aryCoin2Value_63() { return &___m_aryCoin2Value_63; }
	inline void set_m_aryCoin2Value_63(TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* value)
	{
		___m_aryCoin2Value_63 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoin2Value_63), value);
	}

	inline static int32_t get_offset_of_m_aryDiamondValue_64() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryDiamondValue_64)); }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* get_m_aryDiamondValue_64() const { return ___m_aryDiamondValue_64; }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188** get_address_of_m_aryDiamondValue_64() { return &___m_aryDiamondValue_64; }
	inline void set_m_aryDiamondValue_64(TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* value)
	{
		___m_aryDiamondValue_64 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryDiamondValue_64), value);
	}

	inline static int32_t get_offset_of__btnCurEnterButton_65() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____btnCurEnterButton_65)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnCurEnterButton_65() const { return ____btnCurEnterButton_65; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnCurEnterButton_65() { return &____btnCurEnterButton_65; }
	inline void set__btnCurEnterButton_65(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnCurEnterButton_65 = value;
		Il2CppCodeGenWriteBarrier((&____btnCurEnterButton_65), value);
	}

	inline static int32_t get_offset_of_m_fUpdatePlanetInfoLoopTimeElapse_66() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fUpdatePlanetInfoLoopTimeElapse_66)); }
	inline float get_m_fUpdatePlanetInfoLoopTimeElapse_66() const { return ___m_fUpdatePlanetInfoLoopTimeElapse_66; }
	inline float* get_address_of_m_fUpdatePlanetInfoLoopTimeElapse_66() { return &___m_fUpdatePlanetInfoLoopTimeElapse_66; }
	inline void set_m_fUpdatePlanetInfoLoopTimeElapse_66(float value)
	{
		___m_fUpdatePlanetInfoLoopTimeElapse_66 = value;
	}

	inline static int32_t get_offset_of_m_PlanetToPrestige_67() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_PlanetToPrestige_67)); }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * get_m_PlanetToPrestige_67() const { return ___m_PlanetToPrestige_67; }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B ** get_address_of_m_PlanetToPrestige_67() { return &___m_PlanetToPrestige_67; }
	inline void set_m_PlanetToPrestige_67(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * value)
	{
		___m_PlanetToPrestige_67 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlanetToPrestige_67), value);
	}

	inline static int32_t get_offset_of_m_DistrictToPrestige_68() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_DistrictToPrestige_68)); }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * get_m_DistrictToPrestige_68() const { return ___m_DistrictToPrestige_68; }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A ** get_address_of_m_DistrictToPrestige_68() { return &___m_DistrictToPrestige_68; }
	inline void set_m_DistrictToPrestige_68(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * value)
	{
		___m_DistrictToPrestige_68 = value;
		Il2CppCodeGenWriteBarrier((&___m_DistrictToPrestige_68), value);
	}

	inline static int32_t get_offset_of_m_bPlanetDetailPanelOpen_69() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bPlanetDetailPanelOpen_69)); }
	inline bool get_m_bPlanetDetailPanelOpen_69() const { return ___m_bPlanetDetailPanelOpen_69; }
	inline bool* get_address_of_m_bPlanetDetailPanelOpen_69() { return &___m_bPlanetDetailPanelOpen_69; }
	inline void set_m_bPlanetDetailPanelOpen_69(bool value)
	{
		___m_bPlanetDetailPanelOpen_69 = value;
	}

	inline static int32_t get_offset_of_m_districtToUnlock_70() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_districtToUnlock_70)); }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * get_m_districtToUnlock_70() const { return ___m_districtToUnlock_70; }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A ** get_address_of_m_districtToUnlock_70() { return &___m_districtToUnlock_70; }
	inline void set_m_districtToUnlock_70(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * value)
	{
		___m_districtToUnlock_70 = value;
		Il2CppCodeGenWriteBarrier((&___m_districtToUnlock_70), value);
	}

	inline static int32_t get_offset_of_m_planetToUnlock_71() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_planetToUnlock_71)); }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * get_m_planetToUnlock_71() const { return ___m_planetToUnlock_71; }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B ** get_address_of_m_planetToUnlock_71() { return &___m_planetToUnlock_71; }
	inline void set_m_planetToUnlock_71(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * value)
	{
		___m_planetToUnlock_71 = value;
		Il2CppCodeGenWriteBarrier((&___m_planetToUnlock_71), value);
	}

	inline static int32_t get_offset_of_m_nToEnterRace_PlanetId_72() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_nToEnterRace_PlanetId_72)); }
	inline int32_t get_m_nToEnterRace_PlanetId_72() const { return ___m_nToEnterRace_PlanetId_72; }
	inline int32_t* get_address_of_m_nToEnterRace_PlanetId_72() { return &___m_nToEnterRace_PlanetId_72; }
	inline void set_m_nToEnterRace_PlanetId_72(int32_t value)
	{
		___m_nToEnterRace_PlanetId_72 = value;
	}

	inline static int32_t get_offset_of_m_nToEnterRace_DistrictId_73() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_nToEnterRace_DistrictId_73)); }
	inline int32_t get_m_nToEnterRace_DistrictId_73() const { return ___m_nToEnterRace_DistrictId_73; }
	inline int32_t* get_address_of_m_nToEnterRace_DistrictId_73() { return &___m_nToEnterRace_DistrictId_73; }
	inline void set_m_nToEnterRace_DistrictId_73(int32_t value)
	{
		___m_nToEnterRace_DistrictId_73 = value;
	}

	inline static int32_t get_offset_of_m_fPreEnterRaceLoopTime_74() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fPreEnterRaceLoopTime_74)); }
	inline float get_m_fPreEnterRaceLoopTime_74() const { return ___m_fPreEnterRaceLoopTime_74; }
	inline float* get_address_of_m_fPreEnterRaceLoopTime_74() { return &___m_fPreEnterRaceLoopTime_74; }
	inline void set_m_fPreEnterRaceLoopTime_74(float value)
	{
		___m_fPreEnterRaceLoopTime_74 = value;
	}

	inline static int32_t get_offset_of_m_bStarSkyMoving_75() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bStarSkyMoving_75)); }
	inline bool get_m_bStarSkyMoving_75() const { return ___m_bStarSkyMoving_75; }
	inline bool* get_address_of_m_bStarSkyMoving_75() { return &___m_bStarSkyMoving_75; }
	inline void set_m_bStarSkyMoving_75(bool value)
	{
		___m_bStarSkyMoving_75 = value;
	}

	inline static int32_t get_offset_of_m_bIsShowingZengShouPanel_76() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bIsShowingZengShouPanel_76)); }
	inline bool get_m_bIsShowingZengShouPanel_76() const { return ___m_bIsShowingZengShouPanel_76; }
	inline bool* get_address_of_m_bIsShowingZengShouPanel_76() { return &___m_bIsShowingZengShouPanel_76; }
	inline void set_m_bIsShowingZengShouPanel_76(bool value)
	{
		___m_bIsShowingZengShouPanel_76 = value;
	}

	inline static int32_t get_offset_of_m_bTestShow_77() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bTestShow_77)); }
	inline bool get_m_bTestShow_77() const { return ___m_bTestShow_77; }
	inline bool* get_address_of_m_bTestShow_77() { return &___m_bTestShow_77; }
	inline void set_m_bTestShow_77(bool value)
	{
		___m_bTestShow_77 = value;
	}

	inline static int32_t get_offset_of_m_bSliding_78() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bSliding_78)); }
	inline bool get_m_bSliding_78() const { return ___m_bSliding_78; }
	inline bool* get_address_of_m_bSliding_78() { return &___m_bSliding_78; }
	inline void set_m_bSliding_78(bool value)
	{
		___m_bSliding_78 = value;
	}

	inline static int32_t get_offset_of_m_bBigMapShowing_79() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bBigMapShowing_79)); }
	inline bool get_m_bBigMapShowing_79() const { return ___m_bBigMapShowing_79; }
	inline bool* get_address_of_m_bBigMapShowing_79() { return &___m_bBigMapShowing_79; }
	inline void set_m_bBigMapShowing_79(bool value)
	{
		___m_bBigMapShowing_79 = value;
	}

	inline static int32_t get_offset_of_m_fSlideSpeed_80() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fSlideSpeed_80)); }
	inline float get_m_fSlideSpeed_80() const { return ___m_fSlideSpeed_80; }
	inline float* get_address_of_m_fSlideSpeed_80() { return &___m_fSlideSpeed_80; }
	inline void set_m_fSlideSpeed_80(float value)
	{
		___m_fSlideSpeed_80 = value;
	}

	inline static int32_t get_offset_of_m_fSlideA_81() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fSlideA_81)); }
	inline float get_m_fSlideA_81() const { return ___m_fSlideA_81; }
	inline float* get_address_of_m_fSlideA_81() { return &___m_fSlideA_81; }
	inline void set_m_fSlideA_81(float value)
	{
		___m_fSlideA_81 = value;
	}

	inline static int32_t get_offset_of_m_fV0_82() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fV0_82)); }
	inline float get_m_fV0_82() const { return ___m_fV0_82; }
	inline float* get_address_of_m_fV0_82() { return &___m_fV0_82; }
	inline void set_m_fV0_82(float value)
	{
		___m_fV0_82 = value;
	}

	inline static int32_t get_offset_of__goSVContent_83() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____goSVContent_83)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goSVContent_83() const { return ____goSVContent_83; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goSVContent_83() { return &____goSVContent_83; }
	inline void set__goSVContent_83(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goSVContent_83 = value;
		Il2CppCodeGenWriteBarrier((&____goSVContent_83), value);
	}

	inline static int32_t get_offset_of_m_fDestPos_84() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fDestPos_84)); }
	inline float get_m_fDestPos_84() const { return ___m_fDestPos_84; }
	inline float* get_address_of_m_fDestPos_84() { return &___m_fDestPos_84; }
	inline void set_m_fDestPos_84(float value)
	{
		___m_fDestPos_84 = value;
	}

	inline static int32_t get_offset_of_m_fStartDragPos_85() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fStartDragPos_85)); }
	inline float get_m_fStartDragPos_85() const { return ___m_fStartDragPos_85; }
	inline float* get_address_of_m_fStartDragPos_85() { return &___m_fStartDragPos_85; }
	inline void set_m_fStartDragPos_85(float value)
	{
		___m_fStartDragPos_85 = value;
	}

	inline static int32_t get_offset_of_m_fEndDragPos_86() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fEndDragPos_86)); }
	inline float get_m_fEndDragPos_86() const { return ___m_fEndDragPos_86; }
	inline float* get_address_of_m_fEndDragPos_86() { return &___m_fEndDragPos_86; }
	inline void set_m_fEndDragPos_86(float value)
	{
		___m_fEndDragPos_86 = value;
	}

	inline static int32_t get_offset_of_m_bDragging_87() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bDragging_87)); }
	inline bool get_m_bDragging_87() const { return ___m_bDragging_87; }
	inline bool* get_address_of_m_bDragging_87() { return &___m_bDragging_87; }
	inline void set_m_bDragging_87(bool value)
	{
		___m_bDragging_87 = value;
	}

	inline static int32_t get_offset_of_m_fMouseStartPos_88() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fMouseStartPos_88)); }
	inline float get_m_fMouseStartPos_88() const { return ___m_fMouseStartPos_88; }
	inline float* get_address_of_m_fMouseStartPos_88() { return &___m_fMouseStartPos_88; }
	inline void set_m_fMouseStartPos_88(float value)
	{
		___m_fMouseStartPos_88 = value;
	}

	inline static int32_t get_offset_of_m_bClickProhibit_89() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bClickProhibit_89)); }
	inline bool get_m_bClickProhibit_89() const { return ___m_bClickProhibit_89; }
	inline bool* get_address_of_m_bClickProhibit_89() { return &___m_bClickProhibit_89; }
	inline void set_m_bClickProhibit_89(bool value)
	{
		___m_bClickProhibit_89 = value;
	}

	inline static int32_t get_offset_of_m_fClickProhibitTimeLeft_90() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fClickProhibitTimeLeft_90)); }
	inline float get_m_fClickProhibitTimeLeft_90() const { return ___m_fClickProhibitTimeLeft_90; }
	inline float* get_address_of_m_fClickProhibitTimeLeft_90() { return &___m_fClickProhibitTimeLeft_90; }
	inline void set_m_fClickProhibitTimeLeft_90(float value)
	{
		___m_fClickProhibitTimeLeft_90 = value;
	}

	inline static int32_t get_offset_of_m_nCurSelctedIndex_91() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_nCurSelctedIndex_91)); }
	inline int32_t get_m_nCurSelctedIndex_91() const { return ___m_nCurSelctedIndex_91; }
	inline int32_t* get_address_of_m_nCurSelctedIndex_91() { return &___m_nCurSelctedIndex_91; }
	inline void set_m_nCurSelctedIndex_91(int32_t value)
	{
		___m_nCurSelctedIndex_91 = value;
	}

	inline static int32_t get_offset_of_m_aryMainLands_92() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryMainLands_92)); }
	inline UIMainLandU5BU5D_tF159C8131B1BA4E2EEDF316C136F4BA6CE07B280* get_m_aryMainLands_92() const { return ___m_aryMainLands_92; }
	inline UIMainLandU5BU5D_tF159C8131B1BA4E2EEDF316C136F4BA6CE07B280** get_address_of_m_aryMainLands_92() { return &___m_aryMainLands_92; }
	inline void set_m_aryMainLands_92(UIMainLandU5BU5D_tF159C8131B1BA4E2EEDF316C136F4BA6CE07B280* value)
	{
		___m_aryMainLands_92 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMainLands_92), value);
	}
};

struct MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields
{
public:
	// UnityEngine.Vector3 MapManager::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_18;
	// UnityEngine.Vector3 MapManager::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_19;
	// MapManager MapManager::s_Instance
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C * ___s_Instance_20;

public:
	inline static int32_t get_offset_of_vecTempPos_18() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields, ___vecTempPos_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_18() const { return ___vecTempPos_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_18() { return &___vecTempPos_18; }
	inline void set_vecTempPos_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_18 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_19() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields, ___vecTempScale_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_19() const { return ___vecTempScale_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_19() { return &___vecTempScale_19; }
	inline void set_vecTempScale_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_19 = value;
	}

	inline static int32_t get_offset_of_s_Instance_20() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields, ___s_Instance_20)); }
	inline MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C * get_s_Instance_20() const { return ___s_Instance_20; }
	inline MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C ** get_address_of_s_Instance_20() { return &___s_Instance_20; }
	inline void set_s_Instance_20(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C * value)
	{
		___s_Instance_20 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPMANAGER_T76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_H
#ifndef MONEYTRIGGER_T3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464_H
#define MONEYTRIGGER_T3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoneyTrigger
struct  MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.SpriteRenderer MoneyTrigger::_srMain
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srMain_4;
	// UnityEngine.Vector2 MoneyTrigger::m_vecTiaoZiPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecTiaoZiPos_7;
	// BaseScale MoneyTrigger::_baseScale
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____baseScale_9;
	// BaseScale MoneyTrigger::_baseScaleCointCounter
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____baseScaleCointCounter_10;
	// UnityEngine.GameObject MoneyTrigger::_goContainerJinBi
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goContainerJinBi_11;
	// UnityEngine.GameObject MoneyTrigger::_goContainerRichJinBi
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goContainerRichJinBi_12;
	// System.Int32 MoneyTrigger::m_nJinBiNumToGenerate
	int32_t ___m_nJinBiNumToGenerate_13;
	// System.Single MoneyTrigger::m_fTimeElapse
	float ___m_fTimeElapse_14;

public:
	inline static int32_t get_offset_of__srMain_4() { return static_cast<int32_t>(offsetof(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464, ____srMain_4)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srMain_4() const { return ____srMain_4; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srMain_4() { return &____srMain_4; }
	inline void set__srMain_4(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srMain_4 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_4), value);
	}

	inline static int32_t get_offset_of_m_vecTiaoZiPos_7() { return static_cast<int32_t>(offsetof(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464, ___m_vecTiaoZiPos_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecTiaoZiPos_7() const { return ___m_vecTiaoZiPos_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecTiaoZiPos_7() { return &___m_vecTiaoZiPos_7; }
	inline void set_m_vecTiaoZiPos_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecTiaoZiPos_7 = value;
	}

	inline static int32_t get_offset_of__baseScale_9() { return static_cast<int32_t>(offsetof(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464, ____baseScale_9)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__baseScale_9() const { return ____baseScale_9; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__baseScale_9() { return &____baseScale_9; }
	inline void set__baseScale_9(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____baseScale_9 = value;
		Il2CppCodeGenWriteBarrier((&____baseScale_9), value);
	}

	inline static int32_t get_offset_of__baseScaleCointCounter_10() { return static_cast<int32_t>(offsetof(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464, ____baseScaleCointCounter_10)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__baseScaleCointCounter_10() const { return ____baseScaleCointCounter_10; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__baseScaleCointCounter_10() { return &____baseScaleCointCounter_10; }
	inline void set__baseScaleCointCounter_10(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____baseScaleCointCounter_10 = value;
		Il2CppCodeGenWriteBarrier((&____baseScaleCointCounter_10), value);
	}

	inline static int32_t get_offset_of__goContainerJinBi_11() { return static_cast<int32_t>(offsetof(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464, ____goContainerJinBi_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goContainerJinBi_11() const { return ____goContainerJinBi_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goContainerJinBi_11() { return &____goContainerJinBi_11; }
	inline void set__goContainerJinBi_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goContainerJinBi_11 = value;
		Il2CppCodeGenWriteBarrier((&____goContainerJinBi_11), value);
	}

	inline static int32_t get_offset_of__goContainerRichJinBi_12() { return static_cast<int32_t>(offsetof(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464, ____goContainerRichJinBi_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goContainerRichJinBi_12() const { return ____goContainerRichJinBi_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goContainerRichJinBi_12() { return &____goContainerRichJinBi_12; }
	inline void set__goContainerRichJinBi_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goContainerRichJinBi_12 = value;
		Il2CppCodeGenWriteBarrier((&____goContainerRichJinBi_12), value);
	}

	inline static int32_t get_offset_of_m_nJinBiNumToGenerate_13() { return static_cast<int32_t>(offsetof(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464, ___m_nJinBiNumToGenerate_13)); }
	inline int32_t get_m_nJinBiNumToGenerate_13() const { return ___m_nJinBiNumToGenerate_13; }
	inline int32_t* get_address_of_m_nJinBiNumToGenerate_13() { return &___m_nJinBiNumToGenerate_13; }
	inline void set_m_nJinBiNumToGenerate_13(int32_t value)
	{
		___m_nJinBiNumToGenerate_13 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_14() { return static_cast<int32_t>(offsetof(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464, ___m_fTimeElapse_14)); }
	inline float get_m_fTimeElapse_14() const { return ___m_fTimeElapse_14; }
	inline float* get_address_of_m_fTimeElapse_14() { return &___m_fTimeElapse_14; }
	inline void set_m_fTimeElapse_14(float value)
	{
		___m_fTimeElapse_14 = value;
	}
};

struct MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464_StaticFields
{
public:
	// UnityEngine.Vector3 MoneyTrigger::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_5;
	// UnityEngine.Vector3 MoneyTrigger::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_6;
	// MoneyTrigger MoneyTrigger::s_Instance
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464 * ___s_Instance_8;

public:
	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464_StaticFields, ___vecTempScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_5 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_6() { return static_cast<int32_t>(offsetof(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464_StaticFields, ___vecTempPos_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_6() const { return ___vecTempPos_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_6() { return &___vecTempPos_6; }
	inline void set_vecTempPos_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_6 = value;
	}

	inline static int32_t get_offset_of_s_Instance_8() { return static_cast<int32_t>(offsetof(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464_StaticFields, ___s_Instance_8)); }
	inline MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464 * get_s_Instance_8() const { return ___s_Instance_8; }
	inline MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464 ** get_address_of_s_Instance_8() { return &___s_Instance_8; }
	inline void set_s_Instance_8(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464 * value)
	{
		___s_Instance_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONEYTRIGGER_T3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464_H
#ifndef MONTHCARD_TF6C171DC32E6B01734A000C891B88DE0FA09CA6A_H
#define MONTHCARD_TF6C171DC32E6B01734A000C891B88DE0FA09CA6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonthCard
struct  MonthCard_tF6C171DC32E6B01734A000C891B88DE0FA09CA6A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONTHCARD_TF6C171DC32E6B01734A000C891B88DE0FA09CA6A_H
#ifndef OFFLINEMANAGER_T9C8199967E6FA28551A1FDFC1AFCEBAE15A48860_H
#define OFFLINEMANAGER_T9C8199967E6FA28551A1FDFC1AFCEBAE15A48860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfflineManager
struct  OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text OfflineManager::_txtWatchAdsPromote
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtWatchAdsPromote_5;
	// UnityEngine.UI.Text OfflineManager::_txtDiamondPromote
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDiamondPromote_6;
	// UnityEngine.UI.Image OfflineManager::_imgCoinIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCoinIcon_7;
	// UnityEngine.UI.Text OfflineManager::_txtPromotePercent
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPromotePercent_8;
	// System.Single OfflineManager::m_fWatchAdsPromote_Initial
	float ___m_fWatchAdsPromote_Initial_9;
	// System.Single OfflineManager::m_fDiamondPromote_Initial
	float ___m_fDiamondPromote_Initial_10;
	// System.Int32 OfflineManager::m_nDiamondCost_Initial
	int32_t ___m_nDiamondCost_Initial_11;
	// System.Single OfflineManager::m_fWatchAdsPromote_Real
	float ___m_fWatchAdsPromote_Real_12;
	// System.Single OfflineManager::m_fDiamondPromote_Real
	float ___m_fDiamondPromote_Real_13;

public:
	inline static int32_t get_offset_of__txtWatchAdsPromote_5() { return static_cast<int32_t>(offsetof(OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860, ____txtWatchAdsPromote_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtWatchAdsPromote_5() const { return ____txtWatchAdsPromote_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtWatchAdsPromote_5() { return &____txtWatchAdsPromote_5; }
	inline void set__txtWatchAdsPromote_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtWatchAdsPromote_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtWatchAdsPromote_5), value);
	}

	inline static int32_t get_offset_of__txtDiamondPromote_6() { return static_cast<int32_t>(offsetof(OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860, ____txtDiamondPromote_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDiamondPromote_6() const { return ____txtDiamondPromote_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDiamondPromote_6() { return &____txtDiamondPromote_6; }
	inline void set__txtDiamondPromote_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDiamondPromote_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtDiamondPromote_6), value);
	}

	inline static int32_t get_offset_of__imgCoinIcon_7() { return static_cast<int32_t>(offsetof(OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860, ____imgCoinIcon_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCoinIcon_7() const { return ____imgCoinIcon_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCoinIcon_7() { return &____imgCoinIcon_7; }
	inline void set__imgCoinIcon_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCoinIcon_7 = value;
		Il2CppCodeGenWriteBarrier((&____imgCoinIcon_7), value);
	}

	inline static int32_t get_offset_of__txtPromotePercent_8() { return static_cast<int32_t>(offsetof(OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860, ____txtPromotePercent_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPromotePercent_8() const { return ____txtPromotePercent_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPromotePercent_8() { return &____txtPromotePercent_8; }
	inline void set__txtPromotePercent_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPromotePercent_8 = value;
		Il2CppCodeGenWriteBarrier((&____txtPromotePercent_8), value);
	}

	inline static int32_t get_offset_of_m_fWatchAdsPromote_Initial_9() { return static_cast<int32_t>(offsetof(OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860, ___m_fWatchAdsPromote_Initial_9)); }
	inline float get_m_fWatchAdsPromote_Initial_9() const { return ___m_fWatchAdsPromote_Initial_9; }
	inline float* get_address_of_m_fWatchAdsPromote_Initial_9() { return &___m_fWatchAdsPromote_Initial_9; }
	inline void set_m_fWatchAdsPromote_Initial_9(float value)
	{
		___m_fWatchAdsPromote_Initial_9 = value;
	}

	inline static int32_t get_offset_of_m_fDiamondPromote_Initial_10() { return static_cast<int32_t>(offsetof(OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860, ___m_fDiamondPromote_Initial_10)); }
	inline float get_m_fDiamondPromote_Initial_10() const { return ___m_fDiamondPromote_Initial_10; }
	inline float* get_address_of_m_fDiamondPromote_Initial_10() { return &___m_fDiamondPromote_Initial_10; }
	inline void set_m_fDiamondPromote_Initial_10(float value)
	{
		___m_fDiamondPromote_Initial_10 = value;
	}

	inline static int32_t get_offset_of_m_nDiamondCost_Initial_11() { return static_cast<int32_t>(offsetof(OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860, ___m_nDiamondCost_Initial_11)); }
	inline int32_t get_m_nDiamondCost_Initial_11() const { return ___m_nDiamondCost_Initial_11; }
	inline int32_t* get_address_of_m_nDiamondCost_Initial_11() { return &___m_nDiamondCost_Initial_11; }
	inline void set_m_nDiamondCost_Initial_11(int32_t value)
	{
		___m_nDiamondCost_Initial_11 = value;
	}

	inline static int32_t get_offset_of_m_fWatchAdsPromote_Real_12() { return static_cast<int32_t>(offsetof(OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860, ___m_fWatchAdsPromote_Real_12)); }
	inline float get_m_fWatchAdsPromote_Real_12() const { return ___m_fWatchAdsPromote_Real_12; }
	inline float* get_address_of_m_fWatchAdsPromote_Real_12() { return &___m_fWatchAdsPromote_Real_12; }
	inline void set_m_fWatchAdsPromote_Real_12(float value)
	{
		___m_fWatchAdsPromote_Real_12 = value;
	}

	inline static int32_t get_offset_of_m_fDiamondPromote_Real_13() { return static_cast<int32_t>(offsetof(OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860, ___m_fDiamondPromote_Real_13)); }
	inline float get_m_fDiamondPromote_Real_13() const { return ___m_fDiamondPromote_Real_13; }
	inline float* get_address_of_m_fDiamondPromote_Real_13() { return &___m_fDiamondPromote_Real_13; }
	inline void set_m_fDiamondPromote_Real_13(float value)
	{
		___m_fDiamondPromote_Real_13 = value;
	}
};

struct OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860_StaticFields
{
public:
	// OfflineManager OfflineManager::s_Instance
	OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860_StaticFields, ___s_Instance_4)); }
	inline OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFFLINEMANAGER_T9C8199967E6FA28551A1FDFC1AFCEBAE15A48860_H
#ifndef PLANETRIGGER_T26A91CD049429A2694001257C5B0F2EB7D5BE993_H
#define PLANETRIGGER_T26A91CD049429A2694001257C5B0F2EB7D5BE993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PLaneTrigger
struct  PLaneTrigger_t26A91CD049429A2694001257C5B0F2EB7D5BE993  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANETRIGGER_T26A91CD049429A2694001257C5B0F2EB7D5BE993_H
#ifndef PAW_T9302465120C7C5579ADFF12950B7E6F59B4DF011_H
#define PAW_T9302465120C7C5579ADFF12950B7E6F59B4DF011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Paw
struct  Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// CFrameAnimationEffect Paw::_frameaniClick
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * ____frameaniClick_7;
	// BaseScale Paw::_basescalePaw
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____basescalePaw_8;
	// UnityEngine.UI.Image Paw::_imgPaw
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgPaw_9;
	// UnityEngine.SpriteRenderer Paw::_srPaw
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srPaw_10;
	// System.Single Paw::m_fCurAlpha
	float ___m_fCurAlpha_11;
	// System.Boolean Paw::m_bFading
	bool ___m_bFading_12;
	// System.Boolean Paw::m_bMoving
	bool ___m_bMoving_13;
	// UnityEngine.Vector3 Paw::m_vecStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecStartPos_14;
	// UnityEngine.Vector3 Paw::m_vecEndPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecEndPos_15;
	// UnityEngine.Vector3 Paw::m_vecMoveSpeed
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecMoveSpeed_16;
	// System.Single Paw::m_fWaitTime
	float ___m_fWaitTime_17;
	// System.Int32 Paw::m_nMoveStatus
	int32_t ___m_nMoveStatus_18;
	// System.Single Paw::m_fTimeElapse
	float ___m_fTimeElapse_19;
	// Paw/delegateMoveEnd Paw::eventMoveEnd
	delegateMoveEnd_t1F76CEA26063F0B11A971CAB2747FA1BFA8241F7 * ___eventMoveEnd_20;

public:
	inline static int32_t get_offset_of__frameaniClick_7() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ____frameaniClick_7)); }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * get__frameaniClick_7() const { return ____frameaniClick_7; }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 ** get_address_of__frameaniClick_7() { return &____frameaniClick_7; }
	inline void set__frameaniClick_7(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * value)
	{
		____frameaniClick_7 = value;
		Il2CppCodeGenWriteBarrier((&____frameaniClick_7), value);
	}

	inline static int32_t get_offset_of__basescalePaw_8() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ____basescalePaw_8)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__basescalePaw_8() const { return ____basescalePaw_8; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__basescalePaw_8() { return &____basescalePaw_8; }
	inline void set__basescalePaw_8(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____basescalePaw_8 = value;
		Il2CppCodeGenWriteBarrier((&____basescalePaw_8), value);
	}

	inline static int32_t get_offset_of__imgPaw_9() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ____imgPaw_9)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgPaw_9() const { return ____imgPaw_9; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgPaw_9() { return &____imgPaw_9; }
	inline void set__imgPaw_9(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgPaw_9 = value;
		Il2CppCodeGenWriteBarrier((&____imgPaw_9), value);
	}

	inline static int32_t get_offset_of__srPaw_10() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ____srPaw_10)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srPaw_10() const { return ____srPaw_10; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srPaw_10() { return &____srPaw_10; }
	inline void set__srPaw_10(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srPaw_10 = value;
		Il2CppCodeGenWriteBarrier((&____srPaw_10), value);
	}

	inline static int32_t get_offset_of_m_fCurAlpha_11() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_fCurAlpha_11)); }
	inline float get_m_fCurAlpha_11() const { return ___m_fCurAlpha_11; }
	inline float* get_address_of_m_fCurAlpha_11() { return &___m_fCurAlpha_11; }
	inline void set_m_fCurAlpha_11(float value)
	{
		___m_fCurAlpha_11 = value;
	}

	inline static int32_t get_offset_of_m_bFading_12() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_bFading_12)); }
	inline bool get_m_bFading_12() const { return ___m_bFading_12; }
	inline bool* get_address_of_m_bFading_12() { return &___m_bFading_12; }
	inline void set_m_bFading_12(bool value)
	{
		___m_bFading_12 = value;
	}

	inline static int32_t get_offset_of_m_bMoving_13() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_bMoving_13)); }
	inline bool get_m_bMoving_13() const { return ___m_bMoving_13; }
	inline bool* get_address_of_m_bMoving_13() { return &___m_bMoving_13; }
	inline void set_m_bMoving_13(bool value)
	{
		___m_bMoving_13 = value;
	}

	inline static int32_t get_offset_of_m_vecStartPos_14() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_vecStartPos_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecStartPos_14() const { return ___m_vecStartPos_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecStartPos_14() { return &___m_vecStartPos_14; }
	inline void set_m_vecStartPos_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecStartPos_14 = value;
	}

	inline static int32_t get_offset_of_m_vecEndPos_15() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_vecEndPos_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecEndPos_15() const { return ___m_vecEndPos_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecEndPos_15() { return &___m_vecEndPos_15; }
	inline void set_m_vecEndPos_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecEndPos_15 = value;
	}

	inline static int32_t get_offset_of_m_vecMoveSpeed_16() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_vecMoveSpeed_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecMoveSpeed_16() const { return ___m_vecMoveSpeed_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecMoveSpeed_16() { return &___m_vecMoveSpeed_16; }
	inline void set_m_vecMoveSpeed_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecMoveSpeed_16 = value;
	}

	inline static int32_t get_offset_of_m_fWaitTime_17() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_fWaitTime_17)); }
	inline float get_m_fWaitTime_17() const { return ___m_fWaitTime_17; }
	inline float* get_address_of_m_fWaitTime_17() { return &___m_fWaitTime_17; }
	inline void set_m_fWaitTime_17(float value)
	{
		___m_fWaitTime_17 = value;
	}

	inline static int32_t get_offset_of_m_nMoveStatus_18() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_nMoveStatus_18)); }
	inline int32_t get_m_nMoveStatus_18() const { return ___m_nMoveStatus_18; }
	inline int32_t* get_address_of_m_nMoveStatus_18() { return &___m_nMoveStatus_18; }
	inline void set_m_nMoveStatus_18(int32_t value)
	{
		___m_nMoveStatus_18 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_19() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_fTimeElapse_19)); }
	inline float get_m_fTimeElapse_19() const { return ___m_fTimeElapse_19; }
	inline float* get_address_of_m_fTimeElapse_19() { return &___m_fTimeElapse_19; }
	inline void set_m_fTimeElapse_19(float value)
	{
		___m_fTimeElapse_19 = value;
	}

	inline static int32_t get_offset_of_eventMoveEnd_20() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___eventMoveEnd_20)); }
	inline delegateMoveEnd_t1F76CEA26063F0B11A971CAB2747FA1BFA8241F7 * get_eventMoveEnd_20() const { return ___eventMoveEnd_20; }
	inline delegateMoveEnd_t1F76CEA26063F0B11A971CAB2747FA1BFA8241F7 ** get_address_of_eventMoveEnd_20() { return &___eventMoveEnd_20; }
	inline void set_eventMoveEnd_20(delegateMoveEnd_t1F76CEA26063F0B11A971CAB2747FA1BFA8241F7 * value)
	{
		___eventMoveEnd_20 = value;
		Il2CppCodeGenWriteBarrier((&___eventMoveEnd_20), value);
	}
};

struct Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011_StaticFields
{
public:
	// UnityEngine.Color Paw::tempColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___tempColor_4;
	// UnityEngine.Vector3 Paw::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_5;
	// UnityEngine.Vector3 Paw::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_6;

public:
	inline static int32_t get_offset_of_tempColor_4() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011_StaticFields, ___tempColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_tempColor_4() const { return ___tempColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_tempColor_4() { return &___tempColor_4; }
	inline void set_tempColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___tempColor_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011_StaticFields, ___vecTempPos_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_5 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_6() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011_StaticFields, ___vecTempScale_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_6() const { return ___vecTempScale_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_6() { return &___vecTempScale_6; }
	inline void set_vecTempScale_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAW_T9302465120C7C5579ADFF12950B7E6F59B4DF011_H
#ifndef PLANE_T41E94F32055C9505FD4C25C0B01807EF204A7273_H
#define PLANE_T41E94F32055C9505FD4C25C0B01807EF204A7273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Plane
struct  Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject[] Plane::m_aryTailSlow
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryTailSlow_5;
	// UnityEngine.GameObject[] Plane::m_aryTailFast
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryTailFast_6;
	// UnityEngine.GameObject Plane::_tailSlow
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____tailSlow_7;
	// UnityEngine.GameObject Plane::_tailFast
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____tailFast_8;
	// UnityEngine.TextMesh Plane::_txtStatus
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ____txtStatus_9;
	// UnityEngine.Rendering.SortingGroup Plane::_SortingMain
	SortingGroup_tB115E99DFEC155BA39717DC7970E92586F995AED * ____SortingMain_13;
	// UnityEngine.GameObject Plane::_jetRailNormal0
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____jetRailNormal0_14;
	// UnityEngine.GameObject Plane::_jetRailNormal1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____jetRailNormal1_15;
	// UnityEngine.GameObject Plane::_jetRailCrazy
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____jetRailCrazy_16;
	// UnityEngine.Rendering.SortingGroup Plane::_sortingGroup
	SortingGroup_tB115E99DFEC155BA39717DC7970E92586F995AED * ____sortingGroup_17;
	// BaseScale Plane::_basescaleBox
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____basescaleBox_18;
	// BaseScale Plane::_BaseScale
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____BaseScale_19;
	// BaseShake Plane::_BaseShake
	BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A * ____BaseShake_20;
	// Main/ePlaneStatus Plane::m_ePlaneStatus
	int32_t ___m_ePlaneStatus_21;
	// Main/ePosType Plane::m_ePosType
	int32_t ___m_ePosType_22;
	// System.Single Plane::m_fRunningSpeed
	float ___m_fRunningSpeed_23;
	// System.Single Plane::m_fRunningSpeedPerFrame
	float ___m_fRunningSpeedPerFrame_24;
	// System.Single Plane::fRunningSpeedPerFrame
	float ___fRunningSpeedPerFrame_25;
	// System.Single Plane::fAngleSpeedOfTheTurnPerFrame
	float ___fAngleSpeedOfTheTurnPerFrame_26;
	// System.Single Plane::m_fAngleSpeedOfTheTurnPerFrame
	float ___m_fAngleSpeedOfTheTurnPerFrame_27;
	// System.Single Plane::m_fCurAngle
	float ___m_fCurAngle_28;
	// System.Int32 Plane::m_nLevel
	int32_t ___m_nLevel_29;
	// UnityEngine.Vector3 Plane::m_vecStartPosOfSegment
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecStartPosOfSegment_30;
	// StartBelt Plane::m_CollisionStartBelt
	StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD * ___m_CollisionStartBelt_31;
	// Lot Plane::m_CollisionLot
	Lot_t51B999792800E5A264A2CA51A4C521311958F874 * ___m_CollisionLot_32;
	// Lot Plane::m_Lot
	Lot_t51B999792800E5A264A2CA51A4C521311958F874 * ___m_Lot_33;
	// Lot Plane::m_FlyingDestLot
	Lot_t51B999792800E5A264A2CA51A4C521311958F874 * ___m_FlyingDestLot_34;
	// UnityEngine.SpriteRenderer Plane::m_srMain
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___m_srMain_35;
	// UnityEngine.SpriteRenderer Plane::m_srForMergeAnimation
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___m_srForMergeAnimation_36;
	// UnityEngine.SpriteRenderer Plane::m_srBox
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___m_srBox_37;
	// UnityEngine.Vector2 Plane::m_vecRunningToLotSpeed
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecRunningToLotSpeed_38;
	// UnityEngine.GameObject Plane::m_goEffectJet
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goEffectJet_39;
	// UnityEngine.GameObject Plane::m_goContainerEffects
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goContainerEffects_40;
	// UnityEngine.GameObject Plane::m_gocontainerMain
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_gocontainerMain_41;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Plane::lstTemp
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___lstTemp_42;
	// System.Single Plane::m_fAccelerateTime
	float ___m_fAccelerateTime_43;
	// UnityEngine.GameObject Plane::_goTrail
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goTrail_44;
	// UnityEngine.TrailRenderer Plane::_renderTrail
	TrailRenderer_t9AC23ED5E8A7955A3288A9C37865C11382DA668D * ____renderTrail_45;
	// UnityEngine.Sprite Plane::m_sprRunningAvatar
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprRunningAvatar_46;
	// UnityEngine.Sprite Plane::m_sprParkingAvatar
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprParkingAvatar_47;
	// System.Boolean Plane::m_bRecycling
	bool ___m_bRecycling_48;
	// UnityEngine.GameObject Plane::_containerTreasureBox
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerTreasureBox_49;
	// System.Double Plane::m_nCoinGainPerRound
	double ___m_nCoinGainPerRound_50;
	// System.Single Plane::m_fInitiativeAccelerateRate
	float ___m_fInitiativeAccelerateRate_51;
	// Main/sAutoRunLocation Plane::m_RunLocaitonInfo
	sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949  ___m_RunLocaitonInfo_52;
	// UnityEngine.Vector2 Plane::m_vecCurRunSpeed
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecCurRunSpeed_53;
	// UnityEngine.GameObject Plane::trail0
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___trail0_54;
	// UnityEngine.GameObject Plane::trail1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___trail1_55;
	// UnityEngine.GameObject Plane::_Trail
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____Trail_56;
	// System.Boolean Plane::m_bPreRun
	bool ___m_bPreRun_57;
	// System.Single Plane::m_fPosAdjustSpeed
	float ___m_fPosAdjustSpeed_58;
	// UnityEngine.Vector3 Plane::m_vecBeginDragPlanePos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecBeginDragPlanePos_59;
	// UnityEngine.Vector3 Plane::m_vecBeginDragMousePos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecBeginDragMousePos_60;
	// UnityEngine.Vector3 Plane::m_vecOffsetWhenBeginDrag
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecOffsetWhenBeginDrag_61;
	// System.Single Plane::m_fRunToLotTime
	float ___m_fRunToLotTime_62;
	// Plane Plane::m_ToMergePlane
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273 * ___m_ToMergePlane_63;
	// System.Collections.Generic.Dictionary`2<System.Int32,Plane> Plane::m_dicToMerge
	Dictionary_2_t9EA66ECB4AD0D1857C3BAE8BDD9D84DEC3B499F2 * ___m_dicToMerge_64;
	// System.Int32 Plane::m_nMergingAnimationStatus
	int32_t ___m_nMergingAnimationStatus_65;
	// System.Single Plane::m_fMergingWaitTimeElapse
	float ___m_fMergingWaitTimeElapse_66;
	// System.Int32 Plane::m_nTreasureBoxStatus
	int32_t ___m_nTreasureBoxStatus_67;
	// System.Single Plane::m_fDropDistance
	float ___m_fDropDistance_68;
	// System.Single Plane::m_fTreasureBoxDropSpeed
	float ___m_fTreasureBoxDropSpeed_69;
	// System.Single Plane::m_fWaitingTimeElapse
	float ___m_fWaitingTimeElapse_70;
	// System.Boolean Plane::m_bOpeningTreasureBox
	bool ___m_bOpeningTreasureBox_71;

public:
	inline static int32_t get_offset_of_m_aryTailSlow_5() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_aryTailSlow_5)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryTailSlow_5() const { return ___m_aryTailSlow_5; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryTailSlow_5() { return &___m_aryTailSlow_5; }
	inline void set_m_aryTailSlow_5(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryTailSlow_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryTailSlow_5), value);
	}

	inline static int32_t get_offset_of_m_aryTailFast_6() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_aryTailFast_6)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryTailFast_6() const { return ___m_aryTailFast_6; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryTailFast_6() { return &___m_aryTailFast_6; }
	inline void set_m_aryTailFast_6(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryTailFast_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryTailFast_6), value);
	}

	inline static int32_t get_offset_of__tailSlow_7() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____tailSlow_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__tailSlow_7() const { return ____tailSlow_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__tailSlow_7() { return &____tailSlow_7; }
	inline void set__tailSlow_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____tailSlow_7 = value;
		Il2CppCodeGenWriteBarrier((&____tailSlow_7), value);
	}

	inline static int32_t get_offset_of__tailFast_8() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____tailFast_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__tailFast_8() const { return ____tailFast_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__tailFast_8() { return &____tailFast_8; }
	inline void set__tailFast_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____tailFast_8 = value;
		Il2CppCodeGenWriteBarrier((&____tailFast_8), value);
	}

	inline static int32_t get_offset_of__txtStatus_9() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____txtStatus_9)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get__txtStatus_9() const { return ____txtStatus_9; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of__txtStatus_9() { return &____txtStatus_9; }
	inline void set__txtStatus_9(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		____txtStatus_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtStatus_9), value);
	}

	inline static int32_t get_offset_of__SortingMain_13() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____SortingMain_13)); }
	inline SortingGroup_tB115E99DFEC155BA39717DC7970E92586F995AED * get__SortingMain_13() const { return ____SortingMain_13; }
	inline SortingGroup_tB115E99DFEC155BA39717DC7970E92586F995AED ** get_address_of__SortingMain_13() { return &____SortingMain_13; }
	inline void set__SortingMain_13(SortingGroup_tB115E99DFEC155BA39717DC7970E92586F995AED * value)
	{
		____SortingMain_13 = value;
		Il2CppCodeGenWriteBarrier((&____SortingMain_13), value);
	}

	inline static int32_t get_offset_of__jetRailNormal0_14() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____jetRailNormal0_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__jetRailNormal0_14() const { return ____jetRailNormal0_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__jetRailNormal0_14() { return &____jetRailNormal0_14; }
	inline void set__jetRailNormal0_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____jetRailNormal0_14 = value;
		Il2CppCodeGenWriteBarrier((&____jetRailNormal0_14), value);
	}

	inline static int32_t get_offset_of__jetRailNormal1_15() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____jetRailNormal1_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__jetRailNormal1_15() const { return ____jetRailNormal1_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__jetRailNormal1_15() { return &____jetRailNormal1_15; }
	inline void set__jetRailNormal1_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____jetRailNormal1_15 = value;
		Il2CppCodeGenWriteBarrier((&____jetRailNormal1_15), value);
	}

	inline static int32_t get_offset_of__jetRailCrazy_16() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____jetRailCrazy_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__jetRailCrazy_16() const { return ____jetRailCrazy_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__jetRailCrazy_16() { return &____jetRailCrazy_16; }
	inline void set__jetRailCrazy_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____jetRailCrazy_16 = value;
		Il2CppCodeGenWriteBarrier((&____jetRailCrazy_16), value);
	}

	inline static int32_t get_offset_of__sortingGroup_17() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____sortingGroup_17)); }
	inline SortingGroup_tB115E99DFEC155BA39717DC7970E92586F995AED * get__sortingGroup_17() const { return ____sortingGroup_17; }
	inline SortingGroup_tB115E99DFEC155BA39717DC7970E92586F995AED ** get_address_of__sortingGroup_17() { return &____sortingGroup_17; }
	inline void set__sortingGroup_17(SortingGroup_tB115E99DFEC155BA39717DC7970E92586F995AED * value)
	{
		____sortingGroup_17 = value;
		Il2CppCodeGenWriteBarrier((&____sortingGroup_17), value);
	}

	inline static int32_t get_offset_of__basescaleBox_18() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____basescaleBox_18)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__basescaleBox_18() const { return ____basescaleBox_18; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__basescaleBox_18() { return &____basescaleBox_18; }
	inline void set__basescaleBox_18(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____basescaleBox_18 = value;
		Il2CppCodeGenWriteBarrier((&____basescaleBox_18), value);
	}

	inline static int32_t get_offset_of__BaseScale_19() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____BaseScale_19)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__BaseScale_19() const { return ____BaseScale_19; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__BaseScale_19() { return &____BaseScale_19; }
	inline void set__BaseScale_19(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____BaseScale_19 = value;
		Il2CppCodeGenWriteBarrier((&____BaseScale_19), value);
	}

	inline static int32_t get_offset_of__BaseShake_20() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____BaseShake_20)); }
	inline BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A * get__BaseShake_20() const { return ____BaseShake_20; }
	inline BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A ** get_address_of__BaseShake_20() { return &____BaseShake_20; }
	inline void set__BaseShake_20(BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A * value)
	{
		____BaseShake_20 = value;
		Il2CppCodeGenWriteBarrier((&____BaseShake_20), value);
	}

	inline static int32_t get_offset_of_m_ePlaneStatus_21() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_ePlaneStatus_21)); }
	inline int32_t get_m_ePlaneStatus_21() const { return ___m_ePlaneStatus_21; }
	inline int32_t* get_address_of_m_ePlaneStatus_21() { return &___m_ePlaneStatus_21; }
	inline void set_m_ePlaneStatus_21(int32_t value)
	{
		___m_ePlaneStatus_21 = value;
	}

	inline static int32_t get_offset_of_m_ePosType_22() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_ePosType_22)); }
	inline int32_t get_m_ePosType_22() const { return ___m_ePosType_22; }
	inline int32_t* get_address_of_m_ePosType_22() { return &___m_ePosType_22; }
	inline void set_m_ePosType_22(int32_t value)
	{
		___m_ePosType_22 = value;
	}

	inline static int32_t get_offset_of_m_fRunningSpeed_23() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_fRunningSpeed_23)); }
	inline float get_m_fRunningSpeed_23() const { return ___m_fRunningSpeed_23; }
	inline float* get_address_of_m_fRunningSpeed_23() { return &___m_fRunningSpeed_23; }
	inline void set_m_fRunningSpeed_23(float value)
	{
		___m_fRunningSpeed_23 = value;
	}

	inline static int32_t get_offset_of_m_fRunningSpeedPerFrame_24() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_fRunningSpeedPerFrame_24)); }
	inline float get_m_fRunningSpeedPerFrame_24() const { return ___m_fRunningSpeedPerFrame_24; }
	inline float* get_address_of_m_fRunningSpeedPerFrame_24() { return &___m_fRunningSpeedPerFrame_24; }
	inline void set_m_fRunningSpeedPerFrame_24(float value)
	{
		___m_fRunningSpeedPerFrame_24 = value;
	}

	inline static int32_t get_offset_of_fRunningSpeedPerFrame_25() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___fRunningSpeedPerFrame_25)); }
	inline float get_fRunningSpeedPerFrame_25() const { return ___fRunningSpeedPerFrame_25; }
	inline float* get_address_of_fRunningSpeedPerFrame_25() { return &___fRunningSpeedPerFrame_25; }
	inline void set_fRunningSpeedPerFrame_25(float value)
	{
		___fRunningSpeedPerFrame_25 = value;
	}

	inline static int32_t get_offset_of_fAngleSpeedOfTheTurnPerFrame_26() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___fAngleSpeedOfTheTurnPerFrame_26)); }
	inline float get_fAngleSpeedOfTheTurnPerFrame_26() const { return ___fAngleSpeedOfTheTurnPerFrame_26; }
	inline float* get_address_of_fAngleSpeedOfTheTurnPerFrame_26() { return &___fAngleSpeedOfTheTurnPerFrame_26; }
	inline void set_fAngleSpeedOfTheTurnPerFrame_26(float value)
	{
		___fAngleSpeedOfTheTurnPerFrame_26 = value;
	}

	inline static int32_t get_offset_of_m_fAngleSpeedOfTheTurnPerFrame_27() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_fAngleSpeedOfTheTurnPerFrame_27)); }
	inline float get_m_fAngleSpeedOfTheTurnPerFrame_27() const { return ___m_fAngleSpeedOfTheTurnPerFrame_27; }
	inline float* get_address_of_m_fAngleSpeedOfTheTurnPerFrame_27() { return &___m_fAngleSpeedOfTheTurnPerFrame_27; }
	inline void set_m_fAngleSpeedOfTheTurnPerFrame_27(float value)
	{
		___m_fAngleSpeedOfTheTurnPerFrame_27 = value;
	}

	inline static int32_t get_offset_of_m_fCurAngle_28() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_fCurAngle_28)); }
	inline float get_m_fCurAngle_28() const { return ___m_fCurAngle_28; }
	inline float* get_address_of_m_fCurAngle_28() { return &___m_fCurAngle_28; }
	inline void set_m_fCurAngle_28(float value)
	{
		___m_fCurAngle_28 = value;
	}

	inline static int32_t get_offset_of_m_nLevel_29() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_nLevel_29)); }
	inline int32_t get_m_nLevel_29() const { return ___m_nLevel_29; }
	inline int32_t* get_address_of_m_nLevel_29() { return &___m_nLevel_29; }
	inline void set_m_nLevel_29(int32_t value)
	{
		___m_nLevel_29 = value;
	}

	inline static int32_t get_offset_of_m_vecStartPosOfSegment_30() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_vecStartPosOfSegment_30)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecStartPosOfSegment_30() const { return ___m_vecStartPosOfSegment_30; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecStartPosOfSegment_30() { return &___m_vecStartPosOfSegment_30; }
	inline void set_m_vecStartPosOfSegment_30(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecStartPosOfSegment_30 = value;
	}

	inline static int32_t get_offset_of_m_CollisionStartBelt_31() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_CollisionStartBelt_31)); }
	inline StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD * get_m_CollisionStartBelt_31() const { return ___m_CollisionStartBelt_31; }
	inline StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD ** get_address_of_m_CollisionStartBelt_31() { return &___m_CollisionStartBelt_31; }
	inline void set_m_CollisionStartBelt_31(StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD * value)
	{
		___m_CollisionStartBelt_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_CollisionStartBelt_31), value);
	}

	inline static int32_t get_offset_of_m_CollisionLot_32() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_CollisionLot_32)); }
	inline Lot_t51B999792800E5A264A2CA51A4C521311958F874 * get_m_CollisionLot_32() const { return ___m_CollisionLot_32; }
	inline Lot_t51B999792800E5A264A2CA51A4C521311958F874 ** get_address_of_m_CollisionLot_32() { return &___m_CollisionLot_32; }
	inline void set_m_CollisionLot_32(Lot_t51B999792800E5A264A2CA51A4C521311958F874 * value)
	{
		___m_CollisionLot_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_CollisionLot_32), value);
	}

	inline static int32_t get_offset_of_m_Lot_33() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_Lot_33)); }
	inline Lot_t51B999792800E5A264A2CA51A4C521311958F874 * get_m_Lot_33() const { return ___m_Lot_33; }
	inline Lot_t51B999792800E5A264A2CA51A4C521311958F874 ** get_address_of_m_Lot_33() { return &___m_Lot_33; }
	inline void set_m_Lot_33(Lot_t51B999792800E5A264A2CA51A4C521311958F874 * value)
	{
		___m_Lot_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_Lot_33), value);
	}

	inline static int32_t get_offset_of_m_FlyingDestLot_34() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_FlyingDestLot_34)); }
	inline Lot_t51B999792800E5A264A2CA51A4C521311958F874 * get_m_FlyingDestLot_34() const { return ___m_FlyingDestLot_34; }
	inline Lot_t51B999792800E5A264A2CA51A4C521311958F874 ** get_address_of_m_FlyingDestLot_34() { return &___m_FlyingDestLot_34; }
	inline void set_m_FlyingDestLot_34(Lot_t51B999792800E5A264A2CA51A4C521311958F874 * value)
	{
		___m_FlyingDestLot_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_FlyingDestLot_34), value);
	}

	inline static int32_t get_offset_of_m_srMain_35() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_srMain_35)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_m_srMain_35() const { return ___m_srMain_35; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_m_srMain_35() { return &___m_srMain_35; }
	inline void set_m_srMain_35(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___m_srMain_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_srMain_35), value);
	}

	inline static int32_t get_offset_of_m_srForMergeAnimation_36() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_srForMergeAnimation_36)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_m_srForMergeAnimation_36() const { return ___m_srForMergeAnimation_36; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_m_srForMergeAnimation_36() { return &___m_srForMergeAnimation_36; }
	inline void set_m_srForMergeAnimation_36(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___m_srForMergeAnimation_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_srForMergeAnimation_36), value);
	}

	inline static int32_t get_offset_of_m_srBox_37() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_srBox_37)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_m_srBox_37() const { return ___m_srBox_37; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_m_srBox_37() { return &___m_srBox_37; }
	inline void set_m_srBox_37(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___m_srBox_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_srBox_37), value);
	}

	inline static int32_t get_offset_of_m_vecRunningToLotSpeed_38() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_vecRunningToLotSpeed_38)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecRunningToLotSpeed_38() const { return ___m_vecRunningToLotSpeed_38; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecRunningToLotSpeed_38() { return &___m_vecRunningToLotSpeed_38; }
	inline void set_m_vecRunningToLotSpeed_38(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecRunningToLotSpeed_38 = value;
	}

	inline static int32_t get_offset_of_m_goEffectJet_39() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_goEffectJet_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goEffectJet_39() const { return ___m_goEffectJet_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goEffectJet_39() { return &___m_goEffectJet_39; }
	inline void set_m_goEffectJet_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goEffectJet_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_goEffectJet_39), value);
	}

	inline static int32_t get_offset_of_m_goContainerEffects_40() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_goContainerEffects_40)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goContainerEffects_40() const { return ___m_goContainerEffects_40; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goContainerEffects_40() { return &___m_goContainerEffects_40; }
	inline void set_m_goContainerEffects_40(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goContainerEffects_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainerEffects_40), value);
	}

	inline static int32_t get_offset_of_m_gocontainerMain_41() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_gocontainerMain_41)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_gocontainerMain_41() const { return ___m_gocontainerMain_41; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_gocontainerMain_41() { return &___m_gocontainerMain_41; }
	inline void set_m_gocontainerMain_41(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_gocontainerMain_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_gocontainerMain_41), value);
	}

	inline static int32_t get_offset_of_lstTemp_42() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___lstTemp_42)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_lstTemp_42() const { return ___lstTemp_42; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_lstTemp_42() { return &___lstTemp_42; }
	inline void set_lstTemp_42(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___lstTemp_42 = value;
		Il2CppCodeGenWriteBarrier((&___lstTemp_42), value);
	}

	inline static int32_t get_offset_of_m_fAccelerateTime_43() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_fAccelerateTime_43)); }
	inline float get_m_fAccelerateTime_43() const { return ___m_fAccelerateTime_43; }
	inline float* get_address_of_m_fAccelerateTime_43() { return &___m_fAccelerateTime_43; }
	inline void set_m_fAccelerateTime_43(float value)
	{
		___m_fAccelerateTime_43 = value;
	}

	inline static int32_t get_offset_of__goTrail_44() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____goTrail_44)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goTrail_44() const { return ____goTrail_44; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goTrail_44() { return &____goTrail_44; }
	inline void set__goTrail_44(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goTrail_44 = value;
		Il2CppCodeGenWriteBarrier((&____goTrail_44), value);
	}

	inline static int32_t get_offset_of__renderTrail_45() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____renderTrail_45)); }
	inline TrailRenderer_t9AC23ED5E8A7955A3288A9C37865C11382DA668D * get__renderTrail_45() const { return ____renderTrail_45; }
	inline TrailRenderer_t9AC23ED5E8A7955A3288A9C37865C11382DA668D ** get_address_of__renderTrail_45() { return &____renderTrail_45; }
	inline void set__renderTrail_45(TrailRenderer_t9AC23ED5E8A7955A3288A9C37865C11382DA668D * value)
	{
		____renderTrail_45 = value;
		Il2CppCodeGenWriteBarrier((&____renderTrail_45), value);
	}

	inline static int32_t get_offset_of_m_sprRunningAvatar_46() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_sprRunningAvatar_46)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprRunningAvatar_46() const { return ___m_sprRunningAvatar_46; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprRunningAvatar_46() { return &___m_sprRunningAvatar_46; }
	inline void set_m_sprRunningAvatar_46(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprRunningAvatar_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprRunningAvatar_46), value);
	}

	inline static int32_t get_offset_of_m_sprParkingAvatar_47() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_sprParkingAvatar_47)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprParkingAvatar_47() const { return ___m_sprParkingAvatar_47; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprParkingAvatar_47() { return &___m_sprParkingAvatar_47; }
	inline void set_m_sprParkingAvatar_47(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprParkingAvatar_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprParkingAvatar_47), value);
	}

	inline static int32_t get_offset_of_m_bRecycling_48() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_bRecycling_48)); }
	inline bool get_m_bRecycling_48() const { return ___m_bRecycling_48; }
	inline bool* get_address_of_m_bRecycling_48() { return &___m_bRecycling_48; }
	inline void set_m_bRecycling_48(bool value)
	{
		___m_bRecycling_48 = value;
	}

	inline static int32_t get_offset_of__containerTreasureBox_49() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____containerTreasureBox_49)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerTreasureBox_49() const { return ____containerTreasureBox_49; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerTreasureBox_49() { return &____containerTreasureBox_49; }
	inline void set__containerTreasureBox_49(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerTreasureBox_49 = value;
		Il2CppCodeGenWriteBarrier((&____containerTreasureBox_49), value);
	}

	inline static int32_t get_offset_of_m_nCoinGainPerRound_50() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_nCoinGainPerRound_50)); }
	inline double get_m_nCoinGainPerRound_50() const { return ___m_nCoinGainPerRound_50; }
	inline double* get_address_of_m_nCoinGainPerRound_50() { return &___m_nCoinGainPerRound_50; }
	inline void set_m_nCoinGainPerRound_50(double value)
	{
		___m_nCoinGainPerRound_50 = value;
	}

	inline static int32_t get_offset_of_m_fInitiativeAccelerateRate_51() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_fInitiativeAccelerateRate_51)); }
	inline float get_m_fInitiativeAccelerateRate_51() const { return ___m_fInitiativeAccelerateRate_51; }
	inline float* get_address_of_m_fInitiativeAccelerateRate_51() { return &___m_fInitiativeAccelerateRate_51; }
	inline void set_m_fInitiativeAccelerateRate_51(float value)
	{
		___m_fInitiativeAccelerateRate_51 = value;
	}

	inline static int32_t get_offset_of_m_RunLocaitonInfo_52() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_RunLocaitonInfo_52)); }
	inline sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949  get_m_RunLocaitonInfo_52() const { return ___m_RunLocaitonInfo_52; }
	inline sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949 * get_address_of_m_RunLocaitonInfo_52() { return &___m_RunLocaitonInfo_52; }
	inline void set_m_RunLocaitonInfo_52(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949  value)
	{
		___m_RunLocaitonInfo_52 = value;
	}

	inline static int32_t get_offset_of_m_vecCurRunSpeed_53() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_vecCurRunSpeed_53)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecCurRunSpeed_53() const { return ___m_vecCurRunSpeed_53; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecCurRunSpeed_53() { return &___m_vecCurRunSpeed_53; }
	inline void set_m_vecCurRunSpeed_53(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecCurRunSpeed_53 = value;
	}

	inline static int32_t get_offset_of_trail0_54() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___trail0_54)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_trail0_54() const { return ___trail0_54; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_trail0_54() { return &___trail0_54; }
	inline void set_trail0_54(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___trail0_54 = value;
		Il2CppCodeGenWriteBarrier((&___trail0_54), value);
	}

	inline static int32_t get_offset_of_trail1_55() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___trail1_55)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_trail1_55() const { return ___trail1_55; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_trail1_55() { return &___trail1_55; }
	inline void set_trail1_55(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___trail1_55 = value;
		Il2CppCodeGenWriteBarrier((&___trail1_55), value);
	}

	inline static int32_t get_offset_of__Trail_56() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ____Trail_56)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__Trail_56() const { return ____Trail_56; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__Trail_56() { return &____Trail_56; }
	inline void set__Trail_56(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____Trail_56 = value;
		Il2CppCodeGenWriteBarrier((&____Trail_56), value);
	}

	inline static int32_t get_offset_of_m_bPreRun_57() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_bPreRun_57)); }
	inline bool get_m_bPreRun_57() const { return ___m_bPreRun_57; }
	inline bool* get_address_of_m_bPreRun_57() { return &___m_bPreRun_57; }
	inline void set_m_bPreRun_57(bool value)
	{
		___m_bPreRun_57 = value;
	}

	inline static int32_t get_offset_of_m_fPosAdjustSpeed_58() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_fPosAdjustSpeed_58)); }
	inline float get_m_fPosAdjustSpeed_58() const { return ___m_fPosAdjustSpeed_58; }
	inline float* get_address_of_m_fPosAdjustSpeed_58() { return &___m_fPosAdjustSpeed_58; }
	inline void set_m_fPosAdjustSpeed_58(float value)
	{
		___m_fPosAdjustSpeed_58 = value;
	}

	inline static int32_t get_offset_of_m_vecBeginDragPlanePos_59() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_vecBeginDragPlanePos_59)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecBeginDragPlanePos_59() const { return ___m_vecBeginDragPlanePos_59; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecBeginDragPlanePos_59() { return &___m_vecBeginDragPlanePos_59; }
	inline void set_m_vecBeginDragPlanePos_59(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecBeginDragPlanePos_59 = value;
	}

	inline static int32_t get_offset_of_m_vecBeginDragMousePos_60() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_vecBeginDragMousePos_60)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecBeginDragMousePos_60() const { return ___m_vecBeginDragMousePos_60; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecBeginDragMousePos_60() { return &___m_vecBeginDragMousePos_60; }
	inline void set_m_vecBeginDragMousePos_60(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecBeginDragMousePos_60 = value;
	}

	inline static int32_t get_offset_of_m_vecOffsetWhenBeginDrag_61() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_vecOffsetWhenBeginDrag_61)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecOffsetWhenBeginDrag_61() const { return ___m_vecOffsetWhenBeginDrag_61; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecOffsetWhenBeginDrag_61() { return &___m_vecOffsetWhenBeginDrag_61; }
	inline void set_m_vecOffsetWhenBeginDrag_61(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecOffsetWhenBeginDrag_61 = value;
	}

	inline static int32_t get_offset_of_m_fRunToLotTime_62() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_fRunToLotTime_62)); }
	inline float get_m_fRunToLotTime_62() const { return ___m_fRunToLotTime_62; }
	inline float* get_address_of_m_fRunToLotTime_62() { return &___m_fRunToLotTime_62; }
	inline void set_m_fRunToLotTime_62(float value)
	{
		___m_fRunToLotTime_62 = value;
	}

	inline static int32_t get_offset_of_m_ToMergePlane_63() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_ToMergePlane_63)); }
	inline Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273 * get_m_ToMergePlane_63() const { return ___m_ToMergePlane_63; }
	inline Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273 ** get_address_of_m_ToMergePlane_63() { return &___m_ToMergePlane_63; }
	inline void set_m_ToMergePlane_63(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273 * value)
	{
		___m_ToMergePlane_63 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToMergePlane_63), value);
	}

	inline static int32_t get_offset_of_m_dicToMerge_64() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_dicToMerge_64)); }
	inline Dictionary_2_t9EA66ECB4AD0D1857C3BAE8BDD9D84DEC3B499F2 * get_m_dicToMerge_64() const { return ___m_dicToMerge_64; }
	inline Dictionary_2_t9EA66ECB4AD0D1857C3BAE8BDD9D84DEC3B499F2 ** get_address_of_m_dicToMerge_64() { return &___m_dicToMerge_64; }
	inline void set_m_dicToMerge_64(Dictionary_2_t9EA66ECB4AD0D1857C3BAE8BDD9D84DEC3B499F2 * value)
	{
		___m_dicToMerge_64 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicToMerge_64), value);
	}

	inline static int32_t get_offset_of_m_nMergingAnimationStatus_65() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_nMergingAnimationStatus_65)); }
	inline int32_t get_m_nMergingAnimationStatus_65() const { return ___m_nMergingAnimationStatus_65; }
	inline int32_t* get_address_of_m_nMergingAnimationStatus_65() { return &___m_nMergingAnimationStatus_65; }
	inline void set_m_nMergingAnimationStatus_65(int32_t value)
	{
		___m_nMergingAnimationStatus_65 = value;
	}

	inline static int32_t get_offset_of_m_fMergingWaitTimeElapse_66() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_fMergingWaitTimeElapse_66)); }
	inline float get_m_fMergingWaitTimeElapse_66() const { return ___m_fMergingWaitTimeElapse_66; }
	inline float* get_address_of_m_fMergingWaitTimeElapse_66() { return &___m_fMergingWaitTimeElapse_66; }
	inline void set_m_fMergingWaitTimeElapse_66(float value)
	{
		___m_fMergingWaitTimeElapse_66 = value;
	}

	inline static int32_t get_offset_of_m_nTreasureBoxStatus_67() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_nTreasureBoxStatus_67)); }
	inline int32_t get_m_nTreasureBoxStatus_67() const { return ___m_nTreasureBoxStatus_67; }
	inline int32_t* get_address_of_m_nTreasureBoxStatus_67() { return &___m_nTreasureBoxStatus_67; }
	inline void set_m_nTreasureBoxStatus_67(int32_t value)
	{
		___m_nTreasureBoxStatus_67 = value;
	}

	inline static int32_t get_offset_of_m_fDropDistance_68() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_fDropDistance_68)); }
	inline float get_m_fDropDistance_68() const { return ___m_fDropDistance_68; }
	inline float* get_address_of_m_fDropDistance_68() { return &___m_fDropDistance_68; }
	inline void set_m_fDropDistance_68(float value)
	{
		___m_fDropDistance_68 = value;
	}

	inline static int32_t get_offset_of_m_fTreasureBoxDropSpeed_69() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_fTreasureBoxDropSpeed_69)); }
	inline float get_m_fTreasureBoxDropSpeed_69() const { return ___m_fTreasureBoxDropSpeed_69; }
	inline float* get_address_of_m_fTreasureBoxDropSpeed_69() { return &___m_fTreasureBoxDropSpeed_69; }
	inline void set_m_fTreasureBoxDropSpeed_69(float value)
	{
		___m_fTreasureBoxDropSpeed_69 = value;
	}

	inline static int32_t get_offset_of_m_fWaitingTimeElapse_70() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_fWaitingTimeElapse_70)); }
	inline float get_m_fWaitingTimeElapse_70() const { return ___m_fWaitingTimeElapse_70; }
	inline float* get_address_of_m_fWaitingTimeElapse_70() { return &___m_fWaitingTimeElapse_70; }
	inline void set_m_fWaitingTimeElapse_70(float value)
	{
		___m_fWaitingTimeElapse_70 = value;
	}

	inline static int32_t get_offset_of_m_bOpeningTreasureBox_71() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273, ___m_bOpeningTreasureBox_71)); }
	inline bool get_m_bOpeningTreasureBox_71() const { return ___m_bOpeningTreasureBox_71; }
	inline bool* get_address_of_m_bOpeningTreasureBox_71() { return &___m_bOpeningTreasureBox_71; }
	inline void set_m_bOpeningTreasureBox_71(bool value)
	{
		___m_bOpeningTreasureBox_71 = value;
	}
};

struct Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273_StaticFields
{
public:
	// UnityEngine.Vector3 Plane::vecCenter
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecCenter_4;
	// UnityEngine.Vector3 Plane::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_10;
	// UnityEngine.Vector3 Plane::vecTempPos1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos1_11;
	// UnityEngine.Vector3 Plane::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_12;

public:
	inline static int32_t get_offset_of_vecCenter_4() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273_StaticFields, ___vecCenter_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecCenter_4() const { return ___vecCenter_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecCenter_4() { return &___vecCenter_4; }
	inline void set_vecCenter_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecCenter_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_10() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273_StaticFields, ___vecTempPos_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_10() const { return ___vecTempPos_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_10() { return &___vecTempPos_10; }
	inline void set_vecTempPos_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_10 = value;
	}

	inline static int32_t get_offset_of_vecTempPos1_11() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273_StaticFields, ___vecTempPos1_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos1_11() const { return ___vecTempPos1_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos1_11() { return &___vecTempPos1_11; }
	inline void set_vecTempPos1_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos1_11 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_12() { return static_cast<int32_t>(offsetof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273_StaticFields, ___vecTempScale_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_12() const { return ___vecTempScale_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_12() { return &___vecTempScale_12; }
	inline void set_vecTempScale_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T41E94F32055C9505FD4C25C0B01807EF204A7273_H
#ifndef PLANET_T898D8BC07B9E87CE1EEC734414E5AF9609023C3B_H
#define PLANET_T898D8BC07B9E87CE1EEC734414E5AF9609023C3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Planet
struct  Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// District[] Planet::m_aryDistricts
	DistrictU5BU5D_t4A806ECBA985428889EBCDE245832B5D81274468* ___m_aryDistricts_4;
	// UnityEngine.GameObject Planet::_uiPlanetLock
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiPlanetLock_5;
	// SceneUiPrice Planet::_uiUnlockPlanetPrice
	SceneUiPrice_tDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2 * ____uiUnlockPlanetPrice_6;
	// MapManager/ePlanetStatus Planet::m_eStatus
	int32_t ___m_eStatus_7;
	// System.Int32 Planet::m_nId
	int32_t ___m_nId_8;
	// System.Int32 Planet::m_nUnlockPrice
	int32_t ___m_nUnlockPrice_9;
	// System.Double Planet::m_nCoin
	double ___m_nCoin_10;

public:
	inline static int32_t get_offset_of_m_aryDistricts_4() { return static_cast<int32_t>(offsetof(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B, ___m_aryDistricts_4)); }
	inline DistrictU5BU5D_t4A806ECBA985428889EBCDE245832B5D81274468* get_m_aryDistricts_4() const { return ___m_aryDistricts_4; }
	inline DistrictU5BU5D_t4A806ECBA985428889EBCDE245832B5D81274468** get_address_of_m_aryDistricts_4() { return &___m_aryDistricts_4; }
	inline void set_m_aryDistricts_4(DistrictU5BU5D_t4A806ECBA985428889EBCDE245832B5D81274468* value)
	{
		___m_aryDistricts_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryDistricts_4), value);
	}

	inline static int32_t get_offset_of__uiPlanetLock_5() { return static_cast<int32_t>(offsetof(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B, ____uiPlanetLock_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiPlanetLock_5() const { return ____uiPlanetLock_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiPlanetLock_5() { return &____uiPlanetLock_5; }
	inline void set__uiPlanetLock_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiPlanetLock_5 = value;
		Il2CppCodeGenWriteBarrier((&____uiPlanetLock_5), value);
	}

	inline static int32_t get_offset_of__uiUnlockPlanetPrice_6() { return static_cast<int32_t>(offsetof(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B, ____uiUnlockPlanetPrice_6)); }
	inline SceneUiPrice_tDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2 * get__uiUnlockPlanetPrice_6() const { return ____uiUnlockPlanetPrice_6; }
	inline SceneUiPrice_tDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2 ** get_address_of__uiUnlockPlanetPrice_6() { return &____uiUnlockPlanetPrice_6; }
	inline void set__uiUnlockPlanetPrice_6(SceneUiPrice_tDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2 * value)
	{
		____uiUnlockPlanetPrice_6 = value;
		Il2CppCodeGenWriteBarrier((&____uiUnlockPlanetPrice_6), value);
	}

	inline static int32_t get_offset_of_m_eStatus_7() { return static_cast<int32_t>(offsetof(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B, ___m_eStatus_7)); }
	inline int32_t get_m_eStatus_7() const { return ___m_eStatus_7; }
	inline int32_t* get_address_of_m_eStatus_7() { return &___m_eStatus_7; }
	inline void set_m_eStatus_7(int32_t value)
	{
		___m_eStatus_7 = value;
	}

	inline static int32_t get_offset_of_m_nId_8() { return static_cast<int32_t>(offsetof(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B, ___m_nId_8)); }
	inline int32_t get_m_nId_8() const { return ___m_nId_8; }
	inline int32_t* get_address_of_m_nId_8() { return &___m_nId_8; }
	inline void set_m_nId_8(int32_t value)
	{
		___m_nId_8 = value;
	}

	inline static int32_t get_offset_of_m_nUnlockPrice_9() { return static_cast<int32_t>(offsetof(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B, ___m_nUnlockPrice_9)); }
	inline int32_t get_m_nUnlockPrice_9() const { return ___m_nUnlockPrice_9; }
	inline int32_t* get_address_of_m_nUnlockPrice_9() { return &___m_nUnlockPrice_9; }
	inline void set_m_nUnlockPrice_9(int32_t value)
	{
		___m_nUnlockPrice_9 = value;
	}

	inline static int32_t get_offset_of_m_nCoin_10() { return static_cast<int32_t>(offsetof(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B, ___m_nCoin_10)); }
	inline double get_m_nCoin_10() const { return ___m_nCoin_10; }
	inline double* get_address_of_m_nCoin_10() { return &___m_nCoin_10; }
	inline void set_m_nCoin_10(double value)
	{
		___m_nCoin_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANET_T898D8BC07B9E87CE1EEC734414E5AF9609023C3B_H
#ifndef RESEARCHCOUNTER_T192EF04A2372C9EAD586F86C352CE0742E98AAAD_H
#define RESEARCHCOUNTER_T192EF04A2372C9EAD586F86C352CE0742E98AAAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResearchCounter
struct  ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// ResearchManager/sResearchConfig ResearchCounter::m_Config
	sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9  ___m_Config_6;
	// UnityEngine.UI.Button ResearchCounter::_btnBeginUnlocking
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnBeginUnlocking_7;
	// UnityEngine.GameObject ResearchCounter::_containerBeforePreUnlock
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerBeforePreUnlock_8;
	// UnityEngine.GameObject ResearchCounter::_containerAfterPreUnlock
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerAfterPreUnlock_9;
	// UnityEngine.GameObject ResearchCounter::_containerUnlocked
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerUnlocked_10;
	// UnityEngine.UI.Text ResearchCounter::_txtTitle
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTitle_11;
	// UnityEngine.UI.Image ResearchCounter::_imgTitle
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgTitle_12;
	// UnityEngine.UI.Text ResearchCounter::_txtUnlockTotalTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtUnlockTotalTime_13;
	// UnityEngine.UI.Text ResearchCounter::_txtCoinPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCoinPrice_14;
	// UnityEngine.UI.Image ResearchCounter::_imgCoinIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCoinIcon_15;
	// UnityEngine.UI.Text ResearchCounter::_txtLeftTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtLeftTime_16;
	// UnityEngine.UI.Text ResearchCounter::_txtDiamondPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDiamondPrice_17;
	// UnityEngine.UI.Text ResearchCounter::_txtAdsTimeReduce
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtAdsTimeReduce_18;
	// System.DateTime ResearchCounter::m_dataStartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dataStartTime_19;
	// System.DateTime ResearchCounter::m_dateAdsStartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dateAdsStartTime_20;
	// System.Int32 ResearchCounter::m_nLeftTime
	int32_t ___m_nLeftTime_21;
	// System.Int32 ResearchCounter::m_nAdsLeftTime
	int32_t ___m_nAdsLeftTime_22;
	// ResearchManager/eResearchCounterStatus ResearchCounter::m_eStatus
	int32_t ___m_eStatus_23;
	// System.Int32 ResearchCounter::m_nLevel
	int32_t ___m_nLevel_24;
	// System.Collections.Generic.List`1<System.Int32> ResearchCounter::m_lstToActivate
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_lstToActivate_25;
	// District ResearchCounter::m_BoundTrack
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * ___m_BoundTrack_26;
	// System.Int32 ResearchCounter::m_nRealDiamondCost
	int32_t ___m_nRealDiamondCost_27;
	// System.Single ResearchCounter::m_nRealUnlockTime
	float ___m_nRealUnlockTime_28;
	// System.Double ResearchCounter::m_nRealCoinCost
	double ___m_nRealCoinCost_29;
	// UIResearchCounterContainer ResearchCounter::m_goBoundContainer
	UIResearchCounterContainer_tCD786B78332DEDA681A84956DC136BCFE55100F3 * ___m_goBoundContainer_30;
	// System.Int32 ResearchCounter::m_nPlanetId
	int32_t ___m_nPlanetId_31;
	// System.Int32 ResearchCounter::m_nTrackId
	int32_t ___m_nTrackId_32;
	// System.Single ResearchCounter::m_fAdsTimeElapse
	float ___m_fAdsTimeElapse_33;
	// System.Int32 ResearchCounter::m_nNoAdsCount
	int32_t ___m_nNoAdsCount_34;
	// System.Single ResearchCounter::m_fTimeElaspe
	float ___m_fTimeElaspe_35;

public:
	inline static int32_t get_offset_of_m_Config_6() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_Config_6)); }
	inline sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9  get_m_Config_6() const { return ___m_Config_6; }
	inline sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9 * get_address_of_m_Config_6() { return &___m_Config_6; }
	inline void set_m_Config_6(sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9  value)
	{
		___m_Config_6 = value;
	}

	inline static int32_t get_offset_of__btnBeginUnlocking_7() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ____btnBeginUnlocking_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnBeginUnlocking_7() const { return ____btnBeginUnlocking_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnBeginUnlocking_7() { return &____btnBeginUnlocking_7; }
	inline void set__btnBeginUnlocking_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnBeginUnlocking_7 = value;
		Il2CppCodeGenWriteBarrier((&____btnBeginUnlocking_7), value);
	}

	inline static int32_t get_offset_of__containerBeforePreUnlock_8() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ____containerBeforePreUnlock_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerBeforePreUnlock_8() const { return ____containerBeforePreUnlock_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerBeforePreUnlock_8() { return &____containerBeforePreUnlock_8; }
	inline void set__containerBeforePreUnlock_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerBeforePreUnlock_8 = value;
		Il2CppCodeGenWriteBarrier((&____containerBeforePreUnlock_8), value);
	}

	inline static int32_t get_offset_of__containerAfterPreUnlock_9() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ____containerAfterPreUnlock_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerAfterPreUnlock_9() const { return ____containerAfterPreUnlock_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerAfterPreUnlock_9() { return &____containerAfterPreUnlock_9; }
	inline void set__containerAfterPreUnlock_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerAfterPreUnlock_9 = value;
		Il2CppCodeGenWriteBarrier((&____containerAfterPreUnlock_9), value);
	}

	inline static int32_t get_offset_of__containerUnlocked_10() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ____containerUnlocked_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerUnlocked_10() const { return ____containerUnlocked_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerUnlocked_10() { return &____containerUnlocked_10; }
	inline void set__containerUnlocked_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerUnlocked_10 = value;
		Il2CppCodeGenWriteBarrier((&____containerUnlocked_10), value);
	}

	inline static int32_t get_offset_of__txtTitle_11() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ____txtTitle_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTitle_11() const { return ____txtTitle_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTitle_11() { return &____txtTitle_11; }
	inline void set__txtTitle_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTitle_11 = value;
		Il2CppCodeGenWriteBarrier((&____txtTitle_11), value);
	}

	inline static int32_t get_offset_of__imgTitle_12() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ____imgTitle_12)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgTitle_12() const { return ____imgTitle_12; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgTitle_12() { return &____imgTitle_12; }
	inline void set__imgTitle_12(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgTitle_12 = value;
		Il2CppCodeGenWriteBarrier((&____imgTitle_12), value);
	}

	inline static int32_t get_offset_of__txtUnlockTotalTime_13() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ____txtUnlockTotalTime_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtUnlockTotalTime_13() const { return ____txtUnlockTotalTime_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtUnlockTotalTime_13() { return &____txtUnlockTotalTime_13; }
	inline void set__txtUnlockTotalTime_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtUnlockTotalTime_13 = value;
		Il2CppCodeGenWriteBarrier((&____txtUnlockTotalTime_13), value);
	}

	inline static int32_t get_offset_of__txtCoinPrice_14() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ____txtCoinPrice_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCoinPrice_14() const { return ____txtCoinPrice_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCoinPrice_14() { return &____txtCoinPrice_14; }
	inline void set__txtCoinPrice_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCoinPrice_14 = value;
		Il2CppCodeGenWriteBarrier((&____txtCoinPrice_14), value);
	}

	inline static int32_t get_offset_of__imgCoinIcon_15() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ____imgCoinIcon_15)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCoinIcon_15() const { return ____imgCoinIcon_15; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCoinIcon_15() { return &____imgCoinIcon_15; }
	inline void set__imgCoinIcon_15(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCoinIcon_15 = value;
		Il2CppCodeGenWriteBarrier((&____imgCoinIcon_15), value);
	}

	inline static int32_t get_offset_of__txtLeftTime_16() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ____txtLeftTime_16)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtLeftTime_16() const { return ____txtLeftTime_16; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtLeftTime_16() { return &____txtLeftTime_16; }
	inline void set__txtLeftTime_16(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtLeftTime_16 = value;
		Il2CppCodeGenWriteBarrier((&____txtLeftTime_16), value);
	}

	inline static int32_t get_offset_of__txtDiamondPrice_17() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ____txtDiamondPrice_17)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDiamondPrice_17() const { return ____txtDiamondPrice_17; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDiamondPrice_17() { return &____txtDiamondPrice_17; }
	inline void set__txtDiamondPrice_17(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDiamondPrice_17 = value;
		Il2CppCodeGenWriteBarrier((&____txtDiamondPrice_17), value);
	}

	inline static int32_t get_offset_of__txtAdsTimeReduce_18() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ____txtAdsTimeReduce_18)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtAdsTimeReduce_18() const { return ____txtAdsTimeReduce_18; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtAdsTimeReduce_18() { return &____txtAdsTimeReduce_18; }
	inline void set__txtAdsTimeReduce_18(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtAdsTimeReduce_18 = value;
		Il2CppCodeGenWriteBarrier((&____txtAdsTimeReduce_18), value);
	}

	inline static int32_t get_offset_of_m_dataStartTime_19() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_dataStartTime_19)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dataStartTime_19() const { return ___m_dataStartTime_19; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dataStartTime_19() { return &___m_dataStartTime_19; }
	inline void set_m_dataStartTime_19(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dataStartTime_19 = value;
	}

	inline static int32_t get_offset_of_m_dateAdsStartTime_20() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_dateAdsStartTime_20)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dateAdsStartTime_20() const { return ___m_dateAdsStartTime_20; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dateAdsStartTime_20() { return &___m_dateAdsStartTime_20; }
	inline void set_m_dateAdsStartTime_20(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dateAdsStartTime_20 = value;
	}

	inline static int32_t get_offset_of_m_nLeftTime_21() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_nLeftTime_21)); }
	inline int32_t get_m_nLeftTime_21() const { return ___m_nLeftTime_21; }
	inline int32_t* get_address_of_m_nLeftTime_21() { return &___m_nLeftTime_21; }
	inline void set_m_nLeftTime_21(int32_t value)
	{
		___m_nLeftTime_21 = value;
	}

	inline static int32_t get_offset_of_m_nAdsLeftTime_22() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_nAdsLeftTime_22)); }
	inline int32_t get_m_nAdsLeftTime_22() const { return ___m_nAdsLeftTime_22; }
	inline int32_t* get_address_of_m_nAdsLeftTime_22() { return &___m_nAdsLeftTime_22; }
	inline void set_m_nAdsLeftTime_22(int32_t value)
	{
		___m_nAdsLeftTime_22 = value;
	}

	inline static int32_t get_offset_of_m_eStatus_23() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_eStatus_23)); }
	inline int32_t get_m_eStatus_23() const { return ___m_eStatus_23; }
	inline int32_t* get_address_of_m_eStatus_23() { return &___m_eStatus_23; }
	inline void set_m_eStatus_23(int32_t value)
	{
		___m_eStatus_23 = value;
	}

	inline static int32_t get_offset_of_m_nLevel_24() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_nLevel_24)); }
	inline int32_t get_m_nLevel_24() const { return ___m_nLevel_24; }
	inline int32_t* get_address_of_m_nLevel_24() { return &___m_nLevel_24; }
	inline void set_m_nLevel_24(int32_t value)
	{
		___m_nLevel_24 = value;
	}

	inline static int32_t get_offset_of_m_lstToActivate_25() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_lstToActivate_25)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_lstToActivate_25() const { return ___m_lstToActivate_25; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_lstToActivate_25() { return &___m_lstToActivate_25; }
	inline void set_m_lstToActivate_25(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_lstToActivate_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstToActivate_25), value);
	}

	inline static int32_t get_offset_of_m_BoundTrack_26() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_BoundTrack_26)); }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * get_m_BoundTrack_26() const { return ___m_BoundTrack_26; }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A ** get_address_of_m_BoundTrack_26() { return &___m_BoundTrack_26; }
	inline void set_m_BoundTrack_26(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * value)
	{
		___m_BoundTrack_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundTrack_26), value);
	}

	inline static int32_t get_offset_of_m_nRealDiamondCost_27() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_nRealDiamondCost_27)); }
	inline int32_t get_m_nRealDiamondCost_27() const { return ___m_nRealDiamondCost_27; }
	inline int32_t* get_address_of_m_nRealDiamondCost_27() { return &___m_nRealDiamondCost_27; }
	inline void set_m_nRealDiamondCost_27(int32_t value)
	{
		___m_nRealDiamondCost_27 = value;
	}

	inline static int32_t get_offset_of_m_nRealUnlockTime_28() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_nRealUnlockTime_28)); }
	inline float get_m_nRealUnlockTime_28() const { return ___m_nRealUnlockTime_28; }
	inline float* get_address_of_m_nRealUnlockTime_28() { return &___m_nRealUnlockTime_28; }
	inline void set_m_nRealUnlockTime_28(float value)
	{
		___m_nRealUnlockTime_28 = value;
	}

	inline static int32_t get_offset_of_m_nRealCoinCost_29() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_nRealCoinCost_29)); }
	inline double get_m_nRealCoinCost_29() const { return ___m_nRealCoinCost_29; }
	inline double* get_address_of_m_nRealCoinCost_29() { return &___m_nRealCoinCost_29; }
	inline void set_m_nRealCoinCost_29(double value)
	{
		___m_nRealCoinCost_29 = value;
	}

	inline static int32_t get_offset_of_m_goBoundContainer_30() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_goBoundContainer_30)); }
	inline UIResearchCounterContainer_tCD786B78332DEDA681A84956DC136BCFE55100F3 * get_m_goBoundContainer_30() const { return ___m_goBoundContainer_30; }
	inline UIResearchCounterContainer_tCD786B78332DEDA681A84956DC136BCFE55100F3 ** get_address_of_m_goBoundContainer_30() { return &___m_goBoundContainer_30; }
	inline void set_m_goBoundContainer_30(UIResearchCounterContainer_tCD786B78332DEDA681A84956DC136BCFE55100F3 * value)
	{
		___m_goBoundContainer_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_goBoundContainer_30), value);
	}

	inline static int32_t get_offset_of_m_nPlanetId_31() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_nPlanetId_31)); }
	inline int32_t get_m_nPlanetId_31() const { return ___m_nPlanetId_31; }
	inline int32_t* get_address_of_m_nPlanetId_31() { return &___m_nPlanetId_31; }
	inline void set_m_nPlanetId_31(int32_t value)
	{
		___m_nPlanetId_31 = value;
	}

	inline static int32_t get_offset_of_m_nTrackId_32() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_nTrackId_32)); }
	inline int32_t get_m_nTrackId_32() const { return ___m_nTrackId_32; }
	inline int32_t* get_address_of_m_nTrackId_32() { return &___m_nTrackId_32; }
	inline void set_m_nTrackId_32(int32_t value)
	{
		___m_nTrackId_32 = value;
	}

	inline static int32_t get_offset_of_m_fAdsTimeElapse_33() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_fAdsTimeElapse_33)); }
	inline float get_m_fAdsTimeElapse_33() const { return ___m_fAdsTimeElapse_33; }
	inline float* get_address_of_m_fAdsTimeElapse_33() { return &___m_fAdsTimeElapse_33; }
	inline void set_m_fAdsTimeElapse_33(float value)
	{
		___m_fAdsTimeElapse_33 = value;
	}

	inline static int32_t get_offset_of_m_nNoAdsCount_34() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_nNoAdsCount_34)); }
	inline int32_t get_m_nNoAdsCount_34() const { return ___m_nNoAdsCount_34; }
	inline int32_t* get_address_of_m_nNoAdsCount_34() { return &___m_nNoAdsCount_34; }
	inline void set_m_nNoAdsCount_34(int32_t value)
	{
		___m_nNoAdsCount_34 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElaspe_35() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD, ___m_fTimeElaspe_35)); }
	inline float get_m_fTimeElaspe_35() const { return ___m_fTimeElaspe_35; }
	inline float* get_address_of_m_fTimeElaspe_35() { return &___m_fTimeElaspe_35; }
	inline void set_m_fTimeElaspe_35(float value)
	{
		___m_fTimeElaspe_35 = value;
	}
};

struct ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD_StaticFields
{
public:
	// UnityEngine.Vector3 ResearchCounter::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_4;
	// UnityEngine.Vector3 ResearchCounter::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_5;

public:
	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD_StaticFields, ___vecTempPos_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD_StaticFields, ___vecTempScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESEARCHCOUNTER_T192EF04A2372C9EAD586F86C352CE0742E98AAAD_H
#ifndef RESEARCHMANAGER_T812A2256BEF43E66355964CDEC4E2B9FA524880F_H
#define RESEARCHMANAGER_T812A2256BEF43E66355964CDEC4E2B9FA524880F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResearchManager
struct  ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Sprite ResearchManager::m_sprCanNotUnlockBg
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprCanNotUnlockBg_5;
	// ResearchManager/sResearchConfig ResearchManager::tempResearchConfig
	sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9  ___tempResearchConfig_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.Int32,ResearchManager/sResearchConfig>> ResearchManager::m_dicResearchConfig
	Dictionary_2_t29FAE8F1E76FC04AF92864502318A91DA5A83336 * ___m_dicResearchConfig_7;
	// System.Int32 ResearchManager::m_nWatchAdsReduceTime
	int32_t ___m_nWatchAdsReduceTime_8;
	// System.Int32 ResearchManager::m_nWatchAdsInterval
	int32_t ___m_nWatchAdsInterval_9;
	// UnityEngine.GameObject ResearchManager::m_preResearchCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preResearchCounter_10;
	// UnityEngine.GameObject ResearchManager::m_preResearchCounterContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preResearchCounterContainer_11;

public:
	inline static int32_t get_offset_of_m_sprCanNotUnlockBg_5() { return static_cast<int32_t>(offsetof(ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F, ___m_sprCanNotUnlockBg_5)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprCanNotUnlockBg_5() const { return ___m_sprCanNotUnlockBg_5; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprCanNotUnlockBg_5() { return &___m_sprCanNotUnlockBg_5; }
	inline void set_m_sprCanNotUnlockBg_5(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprCanNotUnlockBg_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprCanNotUnlockBg_5), value);
	}

	inline static int32_t get_offset_of_tempResearchConfig_6() { return static_cast<int32_t>(offsetof(ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F, ___tempResearchConfig_6)); }
	inline sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9  get_tempResearchConfig_6() const { return ___tempResearchConfig_6; }
	inline sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9 * get_address_of_tempResearchConfig_6() { return &___tempResearchConfig_6; }
	inline void set_tempResearchConfig_6(sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9  value)
	{
		___tempResearchConfig_6 = value;
	}

	inline static int32_t get_offset_of_m_dicResearchConfig_7() { return static_cast<int32_t>(offsetof(ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F, ___m_dicResearchConfig_7)); }
	inline Dictionary_2_t29FAE8F1E76FC04AF92864502318A91DA5A83336 * get_m_dicResearchConfig_7() const { return ___m_dicResearchConfig_7; }
	inline Dictionary_2_t29FAE8F1E76FC04AF92864502318A91DA5A83336 ** get_address_of_m_dicResearchConfig_7() { return &___m_dicResearchConfig_7; }
	inline void set_m_dicResearchConfig_7(Dictionary_2_t29FAE8F1E76FC04AF92864502318A91DA5A83336 * value)
	{
		___m_dicResearchConfig_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicResearchConfig_7), value);
	}

	inline static int32_t get_offset_of_m_nWatchAdsReduceTime_8() { return static_cast<int32_t>(offsetof(ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F, ___m_nWatchAdsReduceTime_8)); }
	inline int32_t get_m_nWatchAdsReduceTime_8() const { return ___m_nWatchAdsReduceTime_8; }
	inline int32_t* get_address_of_m_nWatchAdsReduceTime_8() { return &___m_nWatchAdsReduceTime_8; }
	inline void set_m_nWatchAdsReduceTime_8(int32_t value)
	{
		___m_nWatchAdsReduceTime_8 = value;
	}

	inline static int32_t get_offset_of_m_nWatchAdsInterval_9() { return static_cast<int32_t>(offsetof(ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F, ___m_nWatchAdsInterval_9)); }
	inline int32_t get_m_nWatchAdsInterval_9() const { return ___m_nWatchAdsInterval_9; }
	inline int32_t* get_address_of_m_nWatchAdsInterval_9() { return &___m_nWatchAdsInterval_9; }
	inline void set_m_nWatchAdsInterval_9(int32_t value)
	{
		___m_nWatchAdsInterval_9 = value;
	}

	inline static int32_t get_offset_of_m_preResearchCounter_10() { return static_cast<int32_t>(offsetof(ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F, ___m_preResearchCounter_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preResearchCounter_10() const { return ___m_preResearchCounter_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preResearchCounter_10() { return &___m_preResearchCounter_10; }
	inline void set_m_preResearchCounter_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preResearchCounter_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_preResearchCounter_10), value);
	}

	inline static int32_t get_offset_of_m_preResearchCounterContainer_11() { return static_cast<int32_t>(offsetof(ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F, ___m_preResearchCounterContainer_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preResearchCounterContainer_11() const { return ___m_preResearchCounterContainer_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preResearchCounterContainer_11() { return &___m_preResearchCounterContainer_11; }
	inline void set_m_preResearchCounterContainer_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preResearchCounterContainer_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_preResearchCounterContainer_11), value);
	}
};

struct ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F_StaticFields
{
public:
	// ResearchManager ResearchManager::s_Instance
	ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F_StaticFields, ___s_Instance_4)); }
	inline ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F * get_s_Instance_4() const { return ___s_Instance_4; }
	inline ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESEARCHMANAGER_T812A2256BEF43E66355964CDEC4E2B9FA524880F_H
#ifndef RESOLUTIONMANAGER_T387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A_H
#define RESOLUTIONMANAGER_T387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResolutionManager
struct  ResolutionManager_t387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// ResolutionManager/eResolution ResolutionManager::m_eResolution
	int32_t ___m_eResolution_5;

public:
	inline static int32_t get_offset_of_m_eResolution_5() { return static_cast<int32_t>(offsetof(ResolutionManager_t387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A, ___m_eResolution_5)); }
	inline int32_t get_m_eResolution_5() const { return ___m_eResolution_5; }
	inline int32_t* get_address_of_m_eResolution_5() { return &___m_eResolution_5; }
	inline void set_m_eResolution_5(int32_t value)
	{
		___m_eResolution_5 = value;
	}
};

struct ResolutionManager_t387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A_StaticFields
{
public:
	// ResolutionManager ResolutionManager::s_Instance
	ResolutionManager_t387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(ResolutionManager_t387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A_StaticFields, ___s_Instance_4)); }
	inline ResolutionManager_t387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A * get_s_Instance_4() const { return ___s_Instance_4; }
	inline ResolutionManager_t387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(ResolutionManager_t387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTIONMANAGER_T387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A_H
#ifndef RESOURCEMANAGER_TCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_H
#define RESOURCEMANAGER_TCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager
struct  ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Sprite ResourceManager::m_sprGreenCash
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprGreenCash_4;
	// UnityEngine.Sprite[] ResourceManager::m_aryCoinIcon_New
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryCoinIcon_New_5;
	// UnityEngine.Sprite[] ResourceManager::m_aryCoinIcon_Special_New
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryCoinIcon_Special_New_6;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileParking_0
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileParking_0_7;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileParking_1
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileParking_1_8;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileParking_2
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileParking_2_9;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileParking_3
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileParking_3_10;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileParking_4
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileParking_4_11;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileRunning_0
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileRunning_0_12;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileRunning_1
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileRunning_1_13;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileRunning_2
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileRunning_2_14;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileRunning_3
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileRunning_3_15;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileRunning_4
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileRunning_4_16;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Sprite[]> ResourceManager::m_dicAutoSpritesParking
	Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E * ___m_dicAutoSpritesParking_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Sprite[]> ResourceManager::m_dicAutoSpritesRunning
	Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E * ___m_dicAutoSpritesRunning_18;
	// UnityEngine.Font ResourceManager::_font
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ____font_21;
	// UnityEngine.GameObject ResourceManager::m_preEnviromentMask
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preEnviromentMask_22;
	// UnityEngine.GameObject ResourceManager::m_preUIShoppinAndItemCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preUIShoppinAndItemCounter_23;
	// UnityEngine.Sprite[] ResourceManager::m_aryPlanetAvatar
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryPlanetAvatar_24;
	// UnityEngine.Sprite ResourceManager::m_sprArrow
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprArrow_25;
	// UnityEngine.Sprite[] ResourceManager::m_arySkillPointIcon
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_arySkillPointIcon_26;
	// UnityEngine.Sprite[] ResourceManager::m_aryCoinIcon
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryCoinIcon_27;
	// UnityEngine.GameObject ResourceManager::m_goRecycledPlanes
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goRecycledPlanes_28;
	// UnityEngine.Sprite[] ResourceManager::m_aryPlaneSprites
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryPlaneSprites_29;
	// UnityEngine.Sprite[] ResourceManager::m_aryParkingPlaneSprites
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryParkingPlaneSprites_30;
	// UnityEngine.GameObject ResourceManager::m_preZengShouCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preZengShouCounter_31;
	// UnityEngine.GameObject ResourceManager::m_prePlane
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_prePlane_32;
	// UnityEngine.GameObject ResourceManager::m_preJinBi
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preJinBi_33;
	// UnityEngine.GameObject ResourceManager::m_preRichTiaoZi
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preRichTiaoZi_34;
	// UnityEngine.GameObject ResourceManager::m_preVehicleCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preVehicleCounter_35;
	// UnityEngine.Color[] ResourceManager::m_aryTrailColor
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___m_aryTrailColor_36;
	// UnityEngine.GameObject ResourceManager::m_preSkill
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preSkill_37;
	// UnityEngine.GameObject ResourceManager::m_preFlyingCoin
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preFlyingCoin_38;
	// UnityEngine.GameObject ResourceManager::m_preScienceTreeConfig
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preScienceTreeConfig_39;
	// UnityEngine.GameObject ResourceManager::m_preScienceLeaf
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preScienceLeaf_40;
	// UnityEngine.Sprite[] ResourceManager::m_aryItemIcon
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryItemIcon_41;
	// UnityEngine.Sprite[] ResourceManager::m_aryCoinRaiseItemIcon
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryCoinRaiseItemIcon_42;
	// UnityEngine.Sprite[] ResourceManager::m_aryCoinDiamondItemIcon
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryCoinDiamondItemIcon_43;
	// UnityEngine.Sprite ResourceManager::m_sprWatchAdIcon
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprWatchAdIcon_44;
	// System.Collections.Generic.List`1<Plane> ResourceManager::m_lstRecycledPlanes
	List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * ___m_lstRecycledPlanes_46;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ResourceManager::m_lstRecycledJinBi
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___m_lstRecycledJinBi_47;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ResourceManager::m_lstRecycledRichTiaoZi
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___m_lstRecycledRichTiaoZi_48;
	// UnityEngine.GameObject ResourceManager::m_preUiItem
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preUiItem_49;
	// System.Collections.Generic.List`1<UIItemInBag> ResourceManager::m_lstRecylcedUiItems
	List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB * ___m_lstRecylcedUiItems_50;
	// UnityEngine.GameObject ResourceManager::_containerRecycledUIItems
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerRecycledUIItems_51;
	// UnityEngine.GameObject ResourceManager::m_goRecycedVehicleCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goRecycedVehicleCounter_53;
	// UnityEngine.GameObject ResourceManager::m_goRecycedResearchCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goRecycedResearchCounter_54;
	// System.Collections.Generic.List`1<UIFlyingCoin> ResourceManager::m_lstRecycledFlyingCoin
	List_1_tB36688DD81A4ADEF07F2EB8C42C12B19153C0C95 * ___m_lstRecycledFlyingCoin_55;
	// UnityEngine.GameObject ResourceManager::m_preTrail
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preTrail_56;

public:
	inline static int32_t get_offset_of_m_sprGreenCash_4() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_sprGreenCash_4)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprGreenCash_4() const { return ___m_sprGreenCash_4; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprGreenCash_4() { return &___m_sprGreenCash_4; }
	inline void set_m_sprGreenCash_4(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprGreenCash_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprGreenCash_4), value);
	}

	inline static int32_t get_offset_of_m_aryCoinIcon_New_5() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryCoinIcon_New_5)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryCoinIcon_New_5() const { return ___m_aryCoinIcon_New_5; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryCoinIcon_New_5() { return &___m_aryCoinIcon_New_5; }
	inline void set_m_aryCoinIcon_New_5(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryCoinIcon_New_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoinIcon_New_5), value);
	}

	inline static int32_t get_offset_of_m_aryCoinIcon_Special_New_6() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryCoinIcon_Special_New_6)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryCoinIcon_Special_New_6() const { return ___m_aryCoinIcon_Special_New_6; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryCoinIcon_Special_New_6() { return &___m_aryCoinIcon_Special_New_6; }
	inline void set_m_aryCoinIcon_Special_New_6(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryCoinIcon_Special_New_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoinIcon_Special_New_6), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileParking_0_7() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileParking_0_7)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileParking_0_7() const { return ___m_aryAutomobileParking_0_7; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileParking_0_7() { return &___m_aryAutomobileParking_0_7; }
	inline void set_m_aryAutomobileParking_0_7(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileParking_0_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileParking_0_7), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileParking_1_8() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileParking_1_8)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileParking_1_8() const { return ___m_aryAutomobileParking_1_8; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileParking_1_8() { return &___m_aryAutomobileParking_1_8; }
	inline void set_m_aryAutomobileParking_1_8(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileParking_1_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileParking_1_8), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileParking_2_9() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileParking_2_9)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileParking_2_9() const { return ___m_aryAutomobileParking_2_9; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileParking_2_9() { return &___m_aryAutomobileParking_2_9; }
	inline void set_m_aryAutomobileParking_2_9(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileParking_2_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileParking_2_9), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileParking_3_10() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileParking_3_10)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileParking_3_10() const { return ___m_aryAutomobileParking_3_10; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileParking_3_10() { return &___m_aryAutomobileParking_3_10; }
	inline void set_m_aryAutomobileParking_3_10(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileParking_3_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileParking_3_10), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileParking_4_11() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileParking_4_11)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileParking_4_11() const { return ___m_aryAutomobileParking_4_11; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileParking_4_11() { return &___m_aryAutomobileParking_4_11; }
	inline void set_m_aryAutomobileParking_4_11(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileParking_4_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileParking_4_11), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileRunning_0_12() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileRunning_0_12)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileRunning_0_12() const { return ___m_aryAutomobileRunning_0_12; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileRunning_0_12() { return &___m_aryAutomobileRunning_0_12; }
	inline void set_m_aryAutomobileRunning_0_12(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileRunning_0_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileRunning_0_12), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileRunning_1_13() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileRunning_1_13)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileRunning_1_13() const { return ___m_aryAutomobileRunning_1_13; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileRunning_1_13() { return &___m_aryAutomobileRunning_1_13; }
	inline void set_m_aryAutomobileRunning_1_13(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileRunning_1_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileRunning_1_13), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileRunning_2_14() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileRunning_2_14)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileRunning_2_14() const { return ___m_aryAutomobileRunning_2_14; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileRunning_2_14() { return &___m_aryAutomobileRunning_2_14; }
	inline void set_m_aryAutomobileRunning_2_14(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileRunning_2_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileRunning_2_14), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileRunning_3_15() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileRunning_3_15)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileRunning_3_15() const { return ___m_aryAutomobileRunning_3_15; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileRunning_3_15() { return &___m_aryAutomobileRunning_3_15; }
	inline void set_m_aryAutomobileRunning_3_15(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileRunning_3_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileRunning_3_15), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileRunning_4_16() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileRunning_4_16)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileRunning_4_16() const { return ___m_aryAutomobileRunning_4_16; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileRunning_4_16() { return &___m_aryAutomobileRunning_4_16; }
	inline void set_m_aryAutomobileRunning_4_16(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileRunning_4_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileRunning_4_16), value);
	}

	inline static int32_t get_offset_of_m_dicAutoSpritesParking_17() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_dicAutoSpritesParking_17)); }
	inline Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E * get_m_dicAutoSpritesParking_17() const { return ___m_dicAutoSpritesParking_17; }
	inline Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E ** get_address_of_m_dicAutoSpritesParking_17() { return &___m_dicAutoSpritesParking_17; }
	inline void set_m_dicAutoSpritesParking_17(Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E * value)
	{
		___m_dicAutoSpritesParking_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAutoSpritesParking_17), value);
	}

	inline static int32_t get_offset_of_m_dicAutoSpritesRunning_18() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_dicAutoSpritesRunning_18)); }
	inline Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E * get_m_dicAutoSpritesRunning_18() const { return ___m_dicAutoSpritesRunning_18; }
	inline Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E ** get_address_of_m_dicAutoSpritesRunning_18() { return &___m_dicAutoSpritesRunning_18; }
	inline void set_m_dicAutoSpritesRunning_18(Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E * value)
	{
		___m_dicAutoSpritesRunning_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAutoSpritesRunning_18), value);
	}

	inline static int32_t get_offset_of__font_21() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ____font_21)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get__font_21() const { return ____font_21; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of__font_21() { return &____font_21; }
	inline void set__font_21(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		____font_21 = value;
		Il2CppCodeGenWriteBarrier((&____font_21), value);
	}

	inline static int32_t get_offset_of_m_preEnviromentMask_22() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preEnviromentMask_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preEnviromentMask_22() const { return ___m_preEnviromentMask_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preEnviromentMask_22() { return &___m_preEnviromentMask_22; }
	inline void set_m_preEnviromentMask_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preEnviromentMask_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_preEnviromentMask_22), value);
	}

	inline static int32_t get_offset_of_m_preUIShoppinAndItemCounter_23() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preUIShoppinAndItemCounter_23)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preUIShoppinAndItemCounter_23() const { return ___m_preUIShoppinAndItemCounter_23; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preUIShoppinAndItemCounter_23() { return &___m_preUIShoppinAndItemCounter_23; }
	inline void set_m_preUIShoppinAndItemCounter_23(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preUIShoppinAndItemCounter_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_preUIShoppinAndItemCounter_23), value);
	}

	inline static int32_t get_offset_of_m_aryPlanetAvatar_24() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryPlanetAvatar_24)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryPlanetAvatar_24() const { return ___m_aryPlanetAvatar_24; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryPlanetAvatar_24() { return &___m_aryPlanetAvatar_24; }
	inline void set_m_aryPlanetAvatar_24(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryPlanetAvatar_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPlanetAvatar_24), value);
	}

	inline static int32_t get_offset_of_m_sprArrow_25() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_sprArrow_25)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprArrow_25() const { return ___m_sprArrow_25; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprArrow_25() { return &___m_sprArrow_25; }
	inline void set_m_sprArrow_25(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprArrow_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprArrow_25), value);
	}

	inline static int32_t get_offset_of_m_arySkillPointIcon_26() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_arySkillPointIcon_26)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_arySkillPointIcon_26() const { return ___m_arySkillPointIcon_26; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_arySkillPointIcon_26() { return &___m_arySkillPointIcon_26; }
	inline void set_m_arySkillPointIcon_26(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_arySkillPointIcon_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillPointIcon_26), value);
	}

	inline static int32_t get_offset_of_m_aryCoinIcon_27() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryCoinIcon_27)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryCoinIcon_27() const { return ___m_aryCoinIcon_27; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryCoinIcon_27() { return &___m_aryCoinIcon_27; }
	inline void set_m_aryCoinIcon_27(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryCoinIcon_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoinIcon_27), value);
	}

	inline static int32_t get_offset_of_m_goRecycledPlanes_28() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_goRecycledPlanes_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goRecycledPlanes_28() const { return ___m_goRecycledPlanes_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goRecycledPlanes_28() { return &___m_goRecycledPlanes_28; }
	inline void set_m_goRecycledPlanes_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goRecycledPlanes_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_goRecycledPlanes_28), value);
	}

	inline static int32_t get_offset_of_m_aryPlaneSprites_29() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryPlaneSprites_29)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryPlaneSprites_29() const { return ___m_aryPlaneSprites_29; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryPlaneSprites_29() { return &___m_aryPlaneSprites_29; }
	inline void set_m_aryPlaneSprites_29(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryPlaneSprites_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPlaneSprites_29), value);
	}

	inline static int32_t get_offset_of_m_aryParkingPlaneSprites_30() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryParkingPlaneSprites_30)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryParkingPlaneSprites_30() const { return ___m_aryParkingPlaneSprites_30; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryParkingPlaneSprites_30() { return &___m_aryParkingPlaneSprites_30; }
	inline void set_m_aryParkingPlaneSprites_30(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryParkingPlaneSprites_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryParkingPlaneSprites_30), value);
	}

	inline static int32_t get_offset_of_m_preZengShouCounter_31() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preZengShouCounter_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preZengShouCounter_31() const { return ___m_preZengShouCounter_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preZengShouCounter_31() { return &___m_preZengShouCounter_31; }
	inline void set_m_preZengShouCounter_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preZengShouCounter_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_preZengShouCounter_31), value);
	}

	inline static int32_t get_offset_of_m_prePlane_32() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_prePlane_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_prePlane_32() const { return ___m_prePlane_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_prePlane_32() { return &___m_prePlane_32; }
	inline void set_m_prePlane_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_prePlane_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_prePlane_32), value);
	}

	inline static int32_t get_offset_of_m_preJinBi_33() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preJinBi_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preJinBi_33() const { return ___m_preJinBi_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preJinBi_33() { return &___m_preJinBi_33; }
	inline void set_m_preJinBi_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preJinBi_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_preJinBi_33), value);
	}

	inline static int32_t get_offset_of_m_preRichTiaoZi_34() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preRichTiaoZi_34)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preRichTiaoZi_34() const { return ___m_preRichTiaoZi_34; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preRichTiaoZi_34() { return &___m_preRichTiaoZi_34; }
	inline void set_m_preRichTiaoZi_34(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preRichTiaoZi_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_preRichTiaoZi_34), value);
	}

	inline static int32_t get_offset_of_m_preVehicleCounter_35() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preVehicleCounter_35)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preVehicleCounter_35() const { return ___m_preVehicleCounter_35; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preVehicleCounter_35() { return &___m_preVehicleCounter_35; }
	inline void set_m_preVehicleCounter_35(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preVehicleCounter_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_preVehicleCounter_35), value);
	}

	inline static int32_t get_offset_of_m_aryTrailColor_36() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryTrailColor_36)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_m_aryTrailColor_36() const { return ___m_aryTrailColor_36; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_m_aryTrailColor_36() { return &___m_aryTrailColor_36; }
	inline void set_m_aryTrailColor_36(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___m_aryTrailColor_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryTrailColor_36), value);
	}

	inline static int32_t get_offset_of_m_preSkill_37() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preSkill_37)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preSkill_37() const { return ___m_preSkill_37; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preSkill_37() { return &___m_preSkill_37; }
	inline void set_m_preSkill_37(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preSkill_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_preSkill_37), value);
	}

	inline static int32_t get_offset_of_m_preFlyingCoin_38() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preFlyingCoin_38)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preFlyingCoin_38() const { return ___m_preFlyingCoin_38; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preFlyingCoin_38() { return &___m_preFlyingCoin_38; }
	inline void set_m_preFlyingCoin_38(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preFlyingCoin_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_preFlyingCoin_38), value);
	}

	inline static int32_t get_offset_of_m_preScienceTreeConfig_39() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preScienceTreeConfig_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preScienceTreeConfig_39() const { return ___m_preScienceTreeConfig_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preScienceTreeConfig_39() { return &___m_preScienceTreeConfig_39; }
	inline void set_m_preScienceTreeConfig_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preScienceTreeConfig_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_preScienceTreeConfig_39), value);
	}

	inline static int32_t get_offset_of_m_preScienceLeaf_40() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preScienceLeaf_40)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preScienceLeaf_40() const { return ___m_preScienceLeaf_40; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preScienceLeaf_40() { return &___m_preScienceLeaf_40; }
	inline void set_m_preScienceLeaf_40(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preScienceLeaf_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_preScienceLeaf_40), value);
	}

	inline static int32_t get_offset_of_m_aryItemIcon_41() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryItemIcon_41)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryItemIcon_41() const { return ___m_aryItemIcon_41; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryItemIcon_41() { return &___m_aryItemIcon_41; }
	inline void set_m_aryItemIcon_41(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryItemIcon_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryItemIcon_41), value);
	}

	inline static int32_t get_offset_of_m_aryCoinRaiseItemIcon_42() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryCoinRaiseItemIcon_42)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryCoinRaiseItemIcon_42() const { return ___m_aryCoinRaiseItemIcon_42; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryCoinRaiseItemIcon_42() { return &___m_aryCoinRaiseItemIcon_42; }
	inline void set_m_aryCoinRaiseItemIcon_42(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryCoinRaiseItemIcon_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoinRaiseItemIcon_42), value);
	}

	inline static int32_t get_offset_of_m_aryCoinDiamondItemIcon_43() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryCoinDiamondItemIcon_43)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryCoinDiamondItemIcon_43() const { return ___m_aryCoinDiamondItemIcon_43; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryCoinDiamondItemIcon_43() { return &___m_aryCoinDiamondItemIcon_43; }
	inline void set_m_aryCoinDiamondItemIcon_43(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryCoinDiamondItemIcon_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoinDiamondItemIcon_43), value);
	}

	inline static int32_t get_offset_of_m_sprWatchAdIcon_44() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_sprWatchAdIcon_44)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprWatchAdIcon_44() const { return ___m_sprWatchAdIcon_44; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprWatchAdIcon_44() { return &___m_sprWatchAdIcon_44; }
	inline void set_m_sprWatchAdIcon_44(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprWatchAdIcon_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprWatchAdIcon_44), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledPlanes_46() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_lstRecycledPlanes_46)); }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * get_m_lstRecycledPlanes_46() const { return ___m_lstRecycledPlanes_46; }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 ** get_address_of_m_lstRecycledPlanes_46() { return &___m_lstRecycledPlanes_46; }
	inline void set_m_lstRecycledPlanes_46(List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * value)
	{
		___m_lstRecycledPlanes_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledPlanes_46), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledJinBi_47() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_lstRecycledJinBi_47)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_m_lstRecycledJinBi_47() const { return ___m_lstRecycledJinBi_47; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_m_lstRecycledJinBi_47() { return &___m_lstRecycledJinBi_47; }
	inline void set_m_lstRecycledJinBi_47(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___m_lstRecycledJinBi_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledJinBi_47), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledRichTiaoZi_48() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_lstRecycledRichTiaoZi_48)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_m_lstRecycledRichTiaoZi_48() const { return ___m_lstRecycledRichTiaoZi_48; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_m_lstRecycledRichTiaoZi_48() { return &___m_lstRecycledRichTiaoZi_48; }
	inline void set_m_lstRecycledRichTiaoZi_48(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___m_lstRecycledRichTiaoZi_48 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledRichTiaoZi_48), value);
	}

	inline static int32_t get_offset_of_m_preUiItem_49() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preUiItem_49)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preUiItem_49() const { return ___m_preUiItem_49; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preUiItem_49() { return &___m_preUiItem_49; }
	inline void set_m_preUiItem_49(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preUiItem_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_preUiItem_49), value);
	}

	inline static int32_t get_offset_of_m_lstRecylcedUiItems_50() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_lstRecylcedUiItems_50)); }
	inline List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB * get_m_lstRecylcedUiItems_50() const { return ___m_lstRecylcedUiItems_50; }
	inline List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB ** get_address_of_m_lstRecylcedUiItems_50() { return &___m_lstRecylcedUiItems_50; }
	inline void set_m_lstRecylcedUiItems_50(List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB * value)
	{
		___m_lstRecylcedUiItems_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecylcedUiItems_50), value);
	}

	inline static int32_t get_offset_of__containerRecycledUIItems_51() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ____containerRecycledUIItems_51)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerRecycledUIItems_51() const { return ____containerRecycledUIItems_51; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerRecycledUIItems_51() { return &____containerRecycledUIItems_51; }
	inline void set__containerRecycledUIItems_51(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerRecycledUIItems_51 = value;
		Il2CppCodeGenWriteBarrier((&____containerRecycledUIItems_51), value);
	}

	inline static int32_t get_offset_of_m_goRecycedVehicleCounter_53() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_goRecycedVehicleCounter_53)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goRecycedVehicleCounter_53() const { return ___m_goRecycedVehicleCounter_53; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goRecycedVehicleCounter_53() { return &___m_goRecycedVehicleCounter_53; }
	inline void set_m_goRecycedVehicleCounter_53(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goRecycedVehicleCounter_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_goRecycedVehicleCounter_53), value);
	}

	inline static int32_t get_offset_of_m_goRecycedResearchCounter_54() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_goRecycedResearchCounter_54)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goRecycedResearchCounter_54() const { return ___m_goRecycedResearchCounter_54; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goRecycedResearchCounter_54() { return &___m_goRecycedResearchCounter_54; }
	inline void set_m_goRecycedResearchCounter_54(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goRecycedResearchCounter_54 = value;
		Il2CppCodeGenWriteBarrier((&___m_goRecycedResearchCounter_54), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledFlyingCoin_55() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_lstRecycledFlyingCoin_55)); }
	inline List_1_tB36688DD81A4ADEF07F2EB8C42C12B19153C0C95 * get_m_lstRecycledFlyingCoin_55() const { return ___m_lstRecycledFlyingCoin_55; }
	inline List_1_tB36688DD81A4ADEF07F2EB8C42C12B19153C0C95 ** get_address_of_m_lstRecycledFlyingCoin_55() { return &___m_lstRecycledFlyingCoin_55; }
	inline void set_m_lstRecycledFlyingCoin_55(List_1_tB36688DD81A4ADEF07F2EB8C42C12B19153C0C95 * value)
	{
		___m_lstRecycledFlyingCoin_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledFlyingCoin_55), value);
	}

	inline static int32_t get_offset_of_m_preTrail_56() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preTrail_56)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preTrail_56() const { return ___m_preTrail_56; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preTrail_56() { return &___m_preTrail_56; }
	inline void set_m_preTrail_56(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preTrail_56 = value;
		Il2CppCodeGenWriteBarrier((&___m_preTrail_56), value);
	}
};

struct ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields
{
public:
	// ResourceManager ResourceManager::s_Instance
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78 * ___s_Instance_19;
	// UnityEngine.Vector3 ResourceManager::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_20;
	// System.Int32 ResourceManager::s_TrailColorIndex
	int32_t ___s_TrailColorIndex_45;

public:
	inline static int32_t get_offset_of_s_Instance_19() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields, ___s_Instance_19)); }
	inline ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78 * get_s_Instance_19() const { return ___s_Instance_19; }
	inline ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78 ** get_address_of_s_Instance_19() { return &___s_Instance_19; }
	inline void set_s_Instance_19(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78 * value)
	{
		___s_Instance_19 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_19), value);
	}

	inline static int32_t get_offset_of_vecTempScale_20() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields, ___vecTempScale_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_20() const { return ___vecTempScale_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_20() { return &___vecTempScale_20; }
	inline void set_vecTempScale_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_20 = value;
	}

	inline static int32_t get_offset_of_s_TrailColorIndex_45() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields, ___s_TrailColorIndex_45)); }
	inline int32_t get_s_TrailColorIndex_45() const { return ___s_TrailColorIndex_45; }
	inline int32_t* get_address_of_s_TrailColorIndex_45() { return &___s_TrailColorIndex_45; }
	inline void set_s_TrailColorIndex_45(int32_t value)
	{
		___s_TrailColorIndex_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCEMANAGER_TCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_H
#ifndef RICHTIAOZI_T145B6185E429BFFC2E81D4A69C2C7EEA97198E86_H
#define RICHTIAOZI_T145B6185E429BFFC2E81D4A69C2C7EEA97198E86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RichTiaoZi
struct  RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.SpriteRenderer RichTiaoZi::_srIcon
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srIcon_4;
	// UnityEngine.TextMesh RichTiaoZi::_txtValue
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ____txtValue_5;
	// UnityEngine.TextMesh RichTiaoZi::_txtValue_Shadow
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ____txtValue_Shadow_6;
	// UnityEngine.Vector2 RichTiaoZi::m_vecStartPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecStartPos_10;
	// System.Int32 RichTiaoZi::m_nStatus
	int32_t ___m_nStatus_11;
	// System.Single RichTiaoZi::m_fInitScale
	float ___m_fInitScale_12;
	// System.Single RichTiaoZi::m_fMaxScale
	float ___m_fMaxScale_13;
	// System.Single RichTiaoZi::m_fFirstSegDis
	float ___m_fFirstSegDis_14;
	// System.Single RichTiaoZi::m_fFirstSegTime
	float ___m_fFirstSegTime_15;
	// System.Single RichTiaoZi::m_fSecSegDis
	float ___m_fSecSegDis_16;
	// System.Single RichTiaoZi::m_fSecSegTime
	float ___m_fSecSegTime_17;
	// System.Single RichTiaoZi::m_fScaleSpeed
	float ___m_fScaleSpeed_18;
	// System.Single RichTiaoZi::m_fFadeSpeed
	float ___m_fFadeSpeed_19;
	// System.Single RichTiaoZi::m_fRaiseAccelerate
	float ___m_fRaiseAccelerate_20;
	// System.Single RichTiaoZi::m_fRaiseSpeed
	float ___m_fRaiseSpeed_21;
	// System.Single RichTiaoZi::m_fAlpha
	float ___m_fAlpha_22;

public:
	inline static int32_t get_offset_of__srIcon_4() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ____srIcon_4)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srIcon_4() const { return ____srIcon_4; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srIcon_4() { return &____srIcon_4; }
	inline void set__srIcon_4(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&____srIcon_4), value);
	}

	inline static int32_t get_offset_of__txtValue_5() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ____txtValue_5)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get__txtValue_5() const { return ____txtValue_5; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of__txtValue_5() { return &____txtValue_5; }
	inline void set__txtValue_5(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		____txtValue_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtValue_5), value);
	}

	inline static int32_t get_offset_of__txtValue_Shadow_6() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ____txtValue_Shadow_6)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get__txtValue_Shadow_6() const { return ____txtValue_Shadow_6; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of__txtValue_Shadow_6() { return &____txtValue_Shadow_6; }
	inline void set__txtValue_Shadow_6(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		____txtValue_Shadow_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtValue_Shadow_6), value);
	}

	inline static int32_t get_offset_of_m_vecStartPos_10() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ___m_vecStartPos_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecStartPos_10() const { return ___m_vecStartPos_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecStartPos_10() { return &___m_vecStartPos_10; }
	inline void set_m_vecStartPos_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecStartPos_10 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_11() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ___m_nStatus_11)); }
	inline int32_t get_m_nStatus_11() const { return ___m_nStatus_11; }
	inline int32_t* get_address_of_m_nStatus_11() { return &___m_nStatus_11; }
	inline void set_m_nStatus_11(int32_t value)
	{
		___m_nStatus_11 = value;
	}

	inline static int32_t get_offset_of_m_fInitScale_12() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ___m_fInitScale_12)); }
	inline float get_m_fInitScale_12() const { return ___m_fInitScale_12; }
	inline float* get_address_of_m_fInitScale_12() { return &___m_fInitScale_12; }
	inline void set_m_fInitScale_12(float value)
	{
		___m_fInitScale_12 = value;
	}

	inline static int32_t get_offset_of_m_fMaxScale_13() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ___m_fMaxScale_13)); }
	inline float get_m_fMaxScale_13() const { return ___m_fMaxScale_13; }
	inline float* get_address_of_m_fMaxScale_13() { return &___m_fMaxScale_13; }
	inline void set_m_fMaxScale_13(float value)
	{
		___m_fMaxScale_13 = value;
	}

	inline static int32_t get_offset_of_m_fFirstSegDis_14() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ___m_fFirstSegDis_14)); }
	inline float get_m_fFirstSegDis_14() const { return ___m_fFirstSegDis_14; }
	inline float* get_address_of_m_fFirstSegDis_14() { return &___m_fFirstSegDis_14; }
	inline void set_m_fFirstSegDis_14(float value)
	{
		___m_fFirstSegDis_14 = value;
	}

	inline static int32_t get_offset_of_m_fFirstSegTime_15() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ___m_fFirstSegTime_15)); }
	inline float get_m_fFirstSegTime_15() const { return ___m_fFirstSegTime_15; }
	inline float* get_address_of_m_fFirstSegTime_15() { return &___m_fFirstSegTime_15; }
	inline void set_m_fFirstSegTime_15(float value)
	{
		___m_fFirstSegTime_15 = value;
	}

	inline static int32_t get_offset_of_m_fSecSegDis_16() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ___m_fSecSegDis_16)); }
	inline float get_m_fSecSegDis_16() const { return ___m_fSecSegDis_16; }
	inline float* get_address_of_m_fSecSegDis_16() { return &___m_fSecSegDis_16; }
	inline void set_m_fSecSegDis_16(float value)
	{
		___m_fSecSegDis_16 = value;
	}

	inline static int32_t get_offset_of_m_fSecSegTime_17() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ___m_fSecSegTime_17)); }
	inline float get_m_fSecSegTime_17() const { return ___m_fSecSegTime_17; }
	inline float* get_address_of_m_fSecSegTime_17() { return &___m_fSecSegTime_17; }
	inline void set_m_fSecSegTime_17(float value)
	{
		___m_fSecSegTime_17 = value;
	}

	inline static int32_t get_offset_of_m_fScaleSpeed_18() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ___m_fScaleSpeed_18)); }
	inline float get_m_fScaleSpeed_18() const { return ___m_fScaleSpeed_18; }
	inline float* get_address_of_m_fScaleSpeed_18() { return &___m_fScaleSpeed_18; }
	inline void set_m_fScaleSpeed_18(float value)
	{
		___m_fScaleSpeed_18 = value;
	}

	inline static int32_t get_offset_of_m_fFadeSpeed_19() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ___m_fFadeSpeed_19)); }
	inline float get_m_fFadeSpeed_19() const { return ___m_fFadeSpeed_19; }
	inline float* get_address_of_m_fFadeSpeed_19() { return &___m_fFadeSpeed_19; }
	inline void set_m_fFadeSpeed_19(float value)
	{
		___m_fFadeSpeed_19 = value;
	}

	inline static int32_t get_offset_of_m_fRaiseAccelerate_20() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ___m_fRaiseAccelerate_20)); }
	inline float get_m_fRaiseAccelerate_20() const { return ___m_fRaiseAccelerate_20; }
	inline float* get_address_of_m_fRaiseAccelerate_20() { return &___m_fRaiseAccelerate_20; }
	inline void set_m_fRaiseAccelerate_20(float value)
	{
		___m_fRaiseAccelerate_20 = value;
	}

	inline static int32_t get_offset_of_m_fRaiseSpeed_21() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ___m_fRaiseSpeed_21)); }
	inline float get_m_fRaiseSpeed_21() const { return ___m_fRaiseSpeed_21; }
	inline float* get_address_of_m_fRaiseSpeed_21() { return &___m_fRaiseSpeed_21; }
	inline void set_m_fRaiseSpeed_21(float value)
	{
		___m_fRaiseSpeed_21 = value;
	}

	inline static int32_t get_offset_of_m_fAlpha_22() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86, ___m_fAlpha_22)); }
	inline float get_m_fAlpha_22() const { return ___m_fAlpha_22; }
	inline float* get_address_of_m_fAlpha_22() { return &___m_fAlpha_22; }
	inline void set_m_fAlpha_22(float value)
	{
		___m_fAlpha_22 = value;
	}
};

struct RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86_StaticFields
{
public:
	// UnityEngine.Vector3 RichTiaoZi::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_7;
	// UnityEngine.Vector3 RichTiaoZi::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_8;
	// UnityEngine.Color RichTiaoZi::colorTemp
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___colorTemp_9;

public:
	inline static int32_t get_offset_of_vecTempPos_7() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86_StaticFields, ___vecTempPos_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_7() const { return ___vecTempPos_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_7() { return &___vecTempPos_7; }
	inline void set_vecTempPos_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_7 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_8() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86_StaticFields, ___vecTempScale_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_8() const { return ___vecTempScale_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_8() { return &___vecTempScale_8; }
	inline void set_vecTempScale_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_8 = value;
	}

	inline static int32_t get_offset_of_colorTemp_9() { return static_cast<int32_t>(offsetof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86_StaticFields, ___colorTemp_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_colorTemp_9() const { return ___colorTemp_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_colorTemp_9() { return &___colorTemp_9; }
	inline void set_colorTemp_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___colorTemp_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RICHTIAOZI_T145B6185E429BFFC2E81D4A69C2C7EEA97198E86_H
#ifndef SCIENCELEAF_T6FEDE450E219E622BB094980970D85E8DA434718_H
#define SCIENCELEAF_T6FEDE450E219E622BB094980970D85E8DA434718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScienceLeaf
struct  ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// ScienceTree/eBranchType ScienceLeaf::m_eType
	int32_t ___m_eType_4;
	// System.Int32 ScienceLeaf::m_nIndexOfThisBranch
	int32_t ___m_nIndexOfThisBranch_5;
	// System.Int32 ScienceLeaf::m_nLevel
	int32_t ___m_nLevel_6;
	// ScienceTreeConfig ScienceLeaf::m_Config
	ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766 * ___m_Config_7;
	// UnityEngine.GameObject ScienceLeaf::_goLock
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goLock_8;
	// UnityEngine.UI.Image ScienceLeaf::_imgOutline
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgOutline_9;
	// UnityEngine.UI.Image ScienceLeaf::_imgMain
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgMain_10;
	// System.Int32[] ScienceLeaf::m_aryIntParams
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_aryIntParams_11;
	// System.Single[] ScienceLeaf::m_aryFloatParams
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryFloatParams_12;
	// System.Boolean ScienceLeaf::m_bSwitch
	bool ___m_bSwitch_13;

public:
	inline static int32_t get_offset_of_m_eType_4() { return static_cast<int32_t>(offsetof(ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718, ___m_eType_4)); }
	inline int32_t get_m_eType_4() const { return ___m_eType_4; }
	inline int32_t* get_address_of_m_eType_4() { return &___m_eType_4; }
	inline void set_m_eType_4(int32_t value)
	{
		___m_eType_4 = value;
	}

	inline static int32_t get_offset_of_m_nIndexOfThisBranch_5() { return static_cast<int32_t>(offsetof(ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718, ___m_nIndexOfThisBranch_5)); }
	inline int32_t get_m_nIndexOfThisBranch_5() const { return ___m_nIndexOfThisBranch_5; }
	inline int32_t* get_address_of_m_nIndexOfThisBranch_5() { return &___m_nIndexOfThisBranch_5; }
	inline void set_m_nIndexOfThisBranch_5(int32_t value)
	{
		___m_nIndexOfThisBranch_5 = value;
	}

	inline static int32_t get_offset_of_m_nLevel_6() { return static_cast<int32_t>(offsetof(ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718, ___m_nLevel_6)); }
	inline int32_t get_m_nLevel_6() const { return ___m_nLevel_6; }
	inline int32_t* get_address_of_m_nLevel_6() { return &___m_nLevel_6; }
	inline void set_m_nLevel_6(int32_t value)
	{
		___m_nLevel_6 = value;
	}

	inline static int32_t get_offset_of_m_Config_7() { return static_cast<int32_t>(offsetof(ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718, ___m_Config_7)); }
	inline ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766 * get_m_Config_7() const { return ___m_Config_7; }
	inline ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766 ** get_address_of_m_Config_7() { return &___m_Config_7; }
	inline void set_m_Config_7(ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766 * value)
	{
		___m_Config_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Config_7), value);
	}

	inline static int32_t get_offset_of__goLock_8() { return static_cast<int32_t>(offsetof(ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718, ____goLock_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goLock_8() const { return ____goLock_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goLock_8() { return &____goLock_8; }
	inline void set__goLock_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goLock_8 = value;
		Il2CppCodeGenWriteBarrier((&____goLock_8), value);
	}

	inline static int32_t get_offset_of__imgOutline_9() { return static_cast<int32_t>(offsetof(ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718, ____imgOutline_9)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgOutline_9() const { return ____imgOutline_9; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgOutline_9() { return &____imgOutline_9; }
	inline void set__imgOutline_9(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgOutline_9 = value;
		Il2CppCodeGenWriteBarrier((&____imgOutline_9), value);
	}

	inline static int32_t get_offset_of__imgMain_10() { return static_cast<int32_t>(offsetof(ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718, ____imgMain_10)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgMain_10() const { return ____imgMain_10; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgMain_10() { return &____imgMain_10; }
	inline void set__imgMain_10(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgMain_10 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_10), value);
	}

	inline static int32_t get_offset_of_m_aryIntParams_11() { return static_cast<int32_t>(offsetof(ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718, ___m_aryIntParams_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_aryIntParams_11() const { return ___m_aryIntParams_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_aryIntParams_11() { return &___m_aryIntParams_11; }
	inline void set_m_aryIntParams_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_aryIntParams_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryIntParams_11), value);
	}

	inline static int32_t get_offset_of_m_aryFloatParams_12() { return static_cast<int32_t>(offsetof(ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718, ___m_aryFloatParams_12)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryFloatParams_12() const { return ___m_aryFloatParams_12; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryFloatParams_12() { return &___m_aryFloatParams_12; }
	inline void set_m_aryFloatParams_12(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryFloatParams_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryFloatParams_12), value);
	}

	inline static int32_t get_offset_of_m_bSwitch_13() { return static_cast<int32_t>(offsetof(ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718, ___m_bSwitch_13)); }
	inline bool get_m_bSwitch_13() const { return ___m_bSwitch_13; }
	inline bool* get_address_of_m_bSwitch_13() { return &___m_bSwitch_13; }
	inline void set_m_bSwitch_13(bool value)
	{
		___m_bSwitch_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCIENCELEAF_T6FEDE450E219E622BB094980970D85E8DA434718_H
#ifndef SCIENCETREE_T57927F0C7068DCB090379E0B11DC86D2D88A2D2E_H
#define SCIENCETREE_T57927F0C7068DCB090379E0B11DC86D2D88A2D2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScienceTree
struct  ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image ScienceTree::_imgCurSelectedLeafOutline
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCurSelectedLeafOutline_5;
	// UnityEngine.Color ScienceTree::m_colorCurCanUnlock
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorCurCanUnlock_6;
	// UnityEngine.Color ScienceTree::m_colorLeafLocked
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorLeafLocked_7;
	// UnityEngine.GameObject ScienceTree::m_preLeafOutline
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preLeafOutline_8;
	// UnityEngine.GameObject ScienceTree::m_preLeafLock
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preLeafLock_9;
	// System.Collections.Generic.Dictionary`2<ScienceTree/eBranchType,System.Int32> ScienceTree::m_dicSkillPoint
	Dictionary_2_t5651FDBECE7EFA5777F480852B72CC08092E9231 * ___m_dicSkillPoint_15;
	// ScienceLeaf[] ScienceTree::aryBranch0
	ScienceLeafU5BU5D_t6A4CF4ED08D2B3E79C7D82250A9BECA8687F3C39* ___aryBranch0_17;
	// ScienceLeaf[] ScienceTree::aryBranch1
	ScienceLeafU5BU5D_t6A4CF4ED08D2B3E79C7D82250A9BECA8687F3C39* ___aryBranch1_18;
	// ScienceLeaf[] ScienceTree::aryBranch2
	ScienceLeafU5BU5D_t6A4CF4ED08D2B3E79C7D82250A9BECA8687F3C39* ___aryBranch2_19;
	// System.Collections.Generic.List`1<ScienceLeaf[]> ScienceTree::m_lstBranches
	List_1_t423D8BD0139A80038103E8A487DD03999EA830C8 * ___m_lstBranches_20;
	// UnityEngine.GameObject ScienceTree::_panelScienceTree
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelScienceTree_21;
	// UnityEngine.GameObject ScienceTree::_containerTree
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerTree_22;
	// UnityEngine.GameObject ScienceTree::_subpanelUpgrade
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____subpanelUpgrade_23;
	// UnityEngine.UI.Text ScienceTree::_txtLevel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtLevel_24;
	// UnityEngine.GameObject ScienceTree::_containerThisLevelInfo
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerThisLevelInfo_25;
	// UnityEngine.GameObject ScienceTree::_containerNextLevelInfo
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerNextLevelInfo_26;
	// UnityEngine.UI.Button ScienceTree::_btnUpgrade
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnUpgrade_27;
	// UnityEngine.UI.Text ScienceTree::_txtPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPrice_28;
	// UnityEngine.UI.Image ScienceTree::_imgMoneyIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgMoneyIcon_29;
	// UnityEngine.UI.Text ScienceTree::_txtOperateType
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtOperateType_30;
	// UnityEngine.UI.Image ScienceTree::_imgCurSelectedLeafAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCurSelectedLeafAvatar_31;
	// UnityEngine.GameObject ScienceTree::_containerLevel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerLevel_32;
	// UnityEngine.UI.Text ScienceTree::_txtCurLevelValue
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCurLevelValue_33;
	// UnityEngine.UI.Text ScienceTree::_txtNextLevelValue
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtNextLevelValue_34;
	// UnityEngine.UI.Text ScienceTree::_txtDesc
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDesc_35;
	// MoneyCounter ScienceTree::_moneyBranch0
	MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * ____moneyBranch0_36;
	// MoneyCounter ScienceTree::_moneyBranch1
	MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * ____moneyBranch1_37;
	// MoneyCounter ScienceTree::_moneyBranch2
	MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * ____moneyBranch2_38;
	// System.Collections.Generic.Dictionary`2<ScienceTree/eBranchType,ScienceLeaf[]> ScienceTree::m_dicTree
	Dictionary_2_t3CFE57C822373C69789FA5C250542814D1C2F75D * ___m_dicTree_39;
	// System.Collections.Generic.Dictionary`2<ScienceTree/eBranchType,System.Collections.Generic.List`1<ScienceTree/sLeafConfig>> ScienceTree::m_dicLeafConfig
	Dictionary_2_t370E87F7C108A681DC68DB080035C48F72ECE0BE * ___m_dicLeafConfig_40;
	// ScienceLeaf ScienceTree::m_CurSelectedLeaf
	ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718 * ___m_CurSelectedLeaf_41;
	// System.Boolean ScienceTree::m_bShowingScienceTree
	bool ___m_bShowingScienceTree_42;
	// ScienceTree/sLeafConfig ScienceTree::tempLeafConfig
	sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11  ___tempLeafConfig_43;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> ScienceTree::m_dicSkillPointBuyTimes
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_dicSkillPointBuyTimes_44;
	// UnityEngine.Vector3 ScienceTree::m_vecDragLastPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecDragLastPos_45;
	// System.Boolean ScienceTree::m_bDragging
	bool ___m_bDragging_46;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> ScienceTree::m_dicPointsForLevels
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_dicPointsForLevels_47;
	// System.Boolean ScienceTree::m_bTalentConfigLoaded
	bool ___m_bTalentConfigLoaded_48;

public:
	inline static int32_t get_offset_of__imgCurSelectedLeafOutline_5() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____imgCurSelectedLeafOutline_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCurSelectedLeafOutline_5() const { return ____imgCurSelectedLeafOutline_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCurSelectedLeafOutline_5() { return &____imgCurSelectedLeafOutline_5; }
	inline void set__imgCurSelectedLeafOutline_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCurSelectedLeafOutline_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgCurSelectedLeafOutline_5), value);
	}

	inline static int32_t get_offset_of_m_colorCurCanUnlock_6() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_colorCurCanUnlock_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorCurCanUnlock_6() const { return ___m_colorCurCanUnlock_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorCurCanUnlock_6() { return &___m_colorCurCanUnlock_6; }
	inline void set_m_colorCurCanUnlock_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorCurCanUnlock_6 = value;
	}

	inline static int32_t get_offset_of_m_colorLeafLocked_7() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_colorLeafLocked_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorLeafLocked_7() const { return ___m_colorLeafLocked_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorLeafLocked_7() { return &___m_colorLeafLocked_7; }
	inline void set_m_colorLeafLocked_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorLeafLocked_7 = value;
	}

	inline static int32_t get_offset_of_m_preLeafOutline_8() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_preLeafOutline_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preLeafOutline_8() const { return ___m_preLeafOutline_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preLeafOutline_8() { return &___m_preLeafOutline_8; }
	inline void set_m_preLeafOutline_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preLeafOutline_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_preLeafOutline_8), value);
	}

	inline static int32_t get_offset_of_m_preLeafLock_9() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_preLeafLock_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preLeafLock_9() const { return ___m_preLeafLock_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preLeafLock_9() { return &___m_preLeafLock_9; }
	inline void set_m_preLeafLock_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preLeafLock_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_preLeafLock_9), value);
	}

	inline static int32_t get_offset_of_m_dicSkillPoint_15() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_dicSkillPoint_15)); }
	inline Dictionary_2_t5651FDBECE7EFA5777F480852B72CC08092E9231 * get_m_dicSkillPoint_15() const { return ___m_dicSkillPoint_15; }
	inline Dictionary_2_t5651FDBECE7EFA5777F480852B72CC08092E9231 ** get_address_of_m_dicSkillPoint_15() { return &___m_dicSkillPoint_15; }
	inline void set_m_dicSkillPoint_15(Dictionary_2_t5651FDBECE7EFA5777F480852B72CC08092E9231 * value)
	{
		___m_dicSkillPoint_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSkillPoint_15), value);
	}

	inline static int32_t get_offset_of_aryBranch0_17() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___aryBranch0_17)); }
	inline ScienceLeafU5BU5D_t6A4CF4ED08D2B3E79C7D82250A9BECA8687F3C39* get_aryBranch0_17() const { return ___aryBranch0_17; }
	inline ScienceLeafU5BU5D_t6A4CF4ED08D2B3E79C7D82250A9BECA8687F3C39** get_address_of_aryBranch0_17() { return &___aryBranch0_17; }
	inline void set_aryBranch0_17(ScienceLeafU5BU5D_t6A4CF4ED08D2B3E79C7D82250A9BECA8687F3C39* value)
	{
		___aryBranch0_17 = value;
		Il2CppCodeGenWriteBarrier((&___aryBranch0_17), value);
	}

	inline static int32_t get_offset_of_aryBranch1_18() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___aryBranch1_18)); }
	inline ScienceLeafU5BU5D_t6A4CF4ED08D2B3E79C7D82250A9BECA8687F3C39* get_aryBranch1_18() const { return ___aryBranch1_18; }
	inline ScienceLeafU5BU5D_t6A4CF4ED08D2B3E79C7D82250A9BECA8687F3C39** get_address_of_aryBranch1_18() { return &___aryBranch1_18; }
	inline void set_aryBranch1_18(ScienceLeafU5BU5D_t6A4CF4ED08D2B3E79C7D82250A9BECA8687F3C39* value)
	{
		___aryBranch1_18 = value;
		Il2CppCodeGenWriteBarrier((&___aryBranch1_18), value);
	}

	inline static int32_t get_offset_of_aryBranch2_19() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___aryBranch2_19)); }
	inline ScienceLeafU5BU5D_t6A4CF4ED08D2B3E79C7D82250A9BECA8687F3C39* get_aryBranch2_19() const { return ___aryBranch2_19; }
	inline ScienceLeafU5BU5D_t6A4CF4ED08D2B3E79C7D82250A9BECA8687F3C39** get_address_of_aryBranch2_19() { return &___aryBranch2_19; }
	inline void set_aryBranch2_19(ScienceLeafU5BU5D_t6A4CF4ED08D2B3E79C7D82250A9BECA8687F3C39* value)
	{
		___aryBranch2_19 = value;
		Il2CppCodeGenWriteBarrier((&___aryBranch2_19), value);
	}

	inline static int32_t get_offset_of_m_lstBranches_20() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_lstBranches_20)); }
	inline List_1_t423D8BD0139A80038103E8A487DD03999EA830C8 * get_m_lstBranches_20() const { return ___m_lstBranches_20; }
	inline List_1_t423D8BD0139A80038103E8A487DD03999EA830C8 ** get_address_of_m_lstBranches_20() { return &___m_lstBranches_20; }
	inline void set_m_lstBranches_20(List_1_t423D8BD0139A80038103E8A487DD03999EA830C8 * value)
	{
		___m_lstBranches_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstBranches_20), value);
	}

	inline static int32_t get_offset_of__panelScienceTree_21() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____panelScienceTree_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelScienceTree_21() const { return ____panelScienceTree_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelScienceTree_21() { return &____panelScienceTree_21; }
	inline void set__panelScienceTree_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelScienceTree_21 = value;
		Il2CppCodeGenWriteBarrier((&____panelScienceTree_21), value);
	}

	inline static int32_t get_offset_of__containerTree_22() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____containerTree_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerTree_22() const { return ____containerTree_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerTree_22() { return &____containerTree_22; }
	inline void set__containerTree_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerTree_22 = value;
		Il2CppCodeGenWriteBarrier((&____containerTree_22), value);
	}

	inline static int32_t get_offset_of__subpanelUpgrade_23() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____subpanelUpgrade_23)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__subpanelUpgrade_23() const { return ____subpanelUpgrade_23; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__subpanelUpgrade_23() { return &____subpanelUpgrade_23; }
	inline void set__subpanelUpgrade_23(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____subpanelUpgrade_23 = value;
		Il2CppCodeGenWriteBarrier((&____subpanelUpgrade_23), value);
	}

	inline static int32_t get_offset_of__txtLevel_24() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____txtLevel_24)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtLevel_24() const { return ____txtLevel_24; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtLevel_24() { return &____txtLevel_24; }
	inline void set__txtLevel_24(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtLevel_24 = value;
		Il2CppCodeGenWriteBarrier((&____txtLevel_24), value);
	}

	inline static int32_t get_offset_of__containerThisLevelInfo_25() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____containerThisLevelInfo_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerThisLevelInfo_25() const { return ____containerThisLevelInfo_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerThisLevelInfo_25() { return &____containerThisLevelInfo_25; }
	inline void set__containerThisLevelInfo_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerThisLevelInfo_25 = value;
		Il2CppCodeGenWriteBarrier((&____containerThisLevelInfo_25), value);
	}

	inline static int32_t get_offset_of__containerNextLevelInfo_26() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____containerNextLevelInfo_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerNextLevelInfo_26() const { return ____containerNextLevelInfo_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerNextLevelInfo_26() { return &____containerNextLevelInfo_26; }
	inline void set__containerNextLevelInfo_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerNextLevelInfo_26 = value;
		Il2CppCodeGenWriteBarrier((&____containerNextLevelInfo_26), value);
	}

	inline static int32_t get_offset_of__btnUpgrade_27() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____btnUpgrade_27)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnUpgrade_27() const { return ____btnUpgrade_27; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnUpgrade_27() { return &____btnUpgrade_27; }
	inline void set__btnUpgrade_27(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnUpgrade_27 = value;
		Il2CppCodeGenWriteBarrier((&____btnUpgrade_27), value);
	}

	inline static int32_t get_offset_of__txtPrice_28() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____txtPrice_28)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPrice_28() const { return ____txtPrice_28; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPrice_28() { return &____txtPrice_28; }
	inline void set__txtPrice_28(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPrice_28 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrice_28), value);
	}

	inline static int32_t get_offset_of__imgMoneyIcon_29() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____imgMoneyIcon_29)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgMoneyIcon_29() const { return ____imgMoneyIcon_29; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgMoneyIcon_29() { return &____imgMoneyIcon_29; }
	inline void set__imgMoneyIcon_29(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgMoneyIcon_29 = value;
		Il2CppCodeGenWriteBarrier((&____imgMoneyIcon_29), value);
	}

	inline static int32_t get_offset_of__txtOperateType_30() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____txtOperateType_30)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtOperateType_30() const { return ____txtOperateType_30; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtOperateType_30() { return &____txtOperateType_30; }
	inline void set__txtOperateType_30(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtOperateType_30 = value;
		Il2CppCodeGenWriteBarrier((&____txtOperateType_30), value);
	}

	inline static int32_t get_offset_of__imgCurSelectedLeafAvatar_31() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____imgCurSelectedLeafAvatar_31)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCurSelectedLeafAvatar_31() const { return ____imgCurSelectedLeafAvatar_31; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCurSelectedLeafAvatar_31() { return &____imgCurSelectedLeafAvatar_31; }
	inline void set__imgCurSelectedLeafAvatar_31(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCurSelectedLeafAvatar_31 = value;
		Il2CppCodeGenWriteBarrier((&____imgCurSelectedLeafAvatar_31), value);
	}

	inline static int32_t get_offset_of__containerLevel_32() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____containerLevel_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerLevel_32() const { return ____containerLevel_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerLevel_32() { return &____containerLevel_32; }
	inline void set__containerLevel_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerLevel_32 = value;
		Il2CppCodeGenWriteBarrier((&____containerLevel_32), value);
	}

	inline static int32_t get_offset_of__txtCurLevelValue_33() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____txtCurLevelValue_33)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCurLevelValue_33() const { return ____txtCurLevelValue_33; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCurLevelValue_33() { return &____txtCurLevelValue_33; }
	inline void set__txtCurLevelValue_33(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCurLevelValue_33 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurLevelValue_33), value);
	}

	inline static int32_t get_offset_of__txtNextLevelValue_34() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____txtNextLevelValue_34)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtNextLevelValue_34() const { return ____txtNextLevelValue_34; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtNextLevelValue_34() { return &____txtNextLevelValue_34; }
	inline void set__txtNextLevelValue_34(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtNextLevelValue_34 = value;
		Il2CppCodeGenWriteBarrier((&____txtNextLevelValue_34), value);
	}

	inline static int32_t get_offset_of__txtDesc_35() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____txtDesc_35)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDesc_35() const { return ____txtDesc_35; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDesc_35() { return &____txtDesc_35; }
	inline void set__txtDesc_35(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDesc_35 = value;
		Il2CppCodeGenWriteBarrier((&____txtDesc_35), value);
	}

	inline static int32_t get_offset_of__moneyBranch0_36() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____moneyBranch0_36)); }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * get__moneyBranch0_36() const { return ____moneyBranch0_36; }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF ** get_address_of__moneyBranch0_36() { return &____moneyBranch0_36; }
	inline void set__moneyBranch0_36(MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * value)
	{
		____moneyBranch0_36 = value;
		Il2CppCodeGenWriteBarrier((&____moneyBranch0_36), value);
	}

	inline static int32_t get_offset_of__moneyBranch1_37() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____moneyBranch1_37)); }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * get__moneyBranch1_37() const { return ____moneyBranch1_37; }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF ** get_address_of__moneyBranch1_37() { return &____moneyBranch1_37; }
	inline void set__moneyBranch1_37(MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * value)
	{
		____moneyBranch1_37 = value;
		Il2CppCodeGenWriteBarrier((&____moneyBranch1_37), value);
	}

	inline static int32_t get_offset_of__moneyBranch2_38() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ____moneyBranch2_38)); }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * get__moneyBranch2_38() const { return ____moneyBranch2_38; }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF ** get_address_of__moneyBranch2_38() { return &____moneyBranch2_38; }
	inline void set__moneyBranch2_38(MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * value)
	{
		____moneyBranch2_38 = value;
		Il2CppCodeGenWriteBarrier((&____moneyBranch2_38), value);
	}

	inline static int32_t get_offset_of_m_dicTree_39() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_dicTree_39)); }
	inline Dictionary_2_t3CFE57C822373C69789FA5C250542814D1C2F75D * get_m_dicTree_39() const { return ___m_dicTree_39; }
	inline Dictionary_2_t3CFE57C822373C69789FA5C250542814D1C2F75D ** get_address_of_m_dicTree_39() { return &___m_dicTree_39; }
	inline void set_m_dicTree_39(Dictionary_2_t3CFE57C822373C69789FA5C250542814D1C2F75D * value)
	{
		___m_dicTree_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicTree_39), value);
	}

	inline static int32_t get_offset_of_m_dicLeafConfig_40() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_dicLeafConfig_40)); }
	inline Dictionary_2_t370E87F7C108A681DC68DB080035C48F72ECE0BE * get_m_dicLeafConfig_40() const { return ___m_dicLeafConfig_40; }
	inline Dictionary_2_t370E87F7C108A681DC68DB080035C48F72ECE0BE ** get_address_of_m_dicLeafConfig_40() { return &___m_dicLeafConfig_40; }
	inline void set_m_dicLeafConfig_40(Dictionary_2_t370E87F7C108A681DC68DB080035C48F72ECE0BE * value)
	{
		___m_dicLeafConfig_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLeafConfig_40), value);
	}

	inline static int32_t get_offset_of_m_CurSelectedLeaf_41() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_CurSelectedLeaf_41)); }
	inline ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718 * get_m_CurSelectedLeaf_41() const { return ___m_CurSelectedLeaf_41; }
	inline ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718 ** get_address_of_m_CurSelectedLeaf_41() { return &___m_CurSelectedLeaf_41; }
	inline void set_m_CurSelectedLeaf_41(ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718 * value)
	{
		___m_CurSelectedLeaf_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurSelectedLeaf_41), value);
	}

	inline static int32_t get_offset_of_m_bShowingScienceTree_42() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_bShowingScienceTree_42)); }
	inline bool get_m_bShowingScienceTree_42() const { return ___m_bShowingScienceTree_42; }
	inline bool* get_address_of_m_bShowingScienceTree_42() { return &___m_bShowingScienceTree_42; }
	inline void set_m_bShowingScienceTree_42(bool value)
	{
		___m_bShowingScienceTree_42 = value;
	}

	inline static int32_t get_offset_of_tempLeafConfig_43() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___tempLeafConfig_43)); }
	inline sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11  get_tempLeafConfig_43() const { return ___tempLeafConfig_43; }
	inline sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11 * get_address_of_tempLeafConfig_43() { return &___tempLeafConfig_43; }
	inline void set_tempLeafConfig_43(sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11  value)
	{
		___tempLeafConfig_43 = value;
	}

	inline static int32_t get_offset_of_m_dicSkillPointBuyTimes_44() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_dicSkillPointBuyTimes_44)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_dicSkillPointBuyTimes_44() const { return ___m_dicSkillPointBuyTimes_44; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_dicSkillPointBuyTimes_44() { return &___m_dicSkillPointBuyTimes_44; }
	inline void set_m_dicSkillPointBuyTimes_44(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_dicSkillPointBuyTimes_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSkillPointBuyTimes_44), value);
	}

	inline static int32_t get_offset_of_m_vecDragLastPos_45() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_vecDragLastPos_45)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecDragLastPos_45() const { return ___m_vecDragLastPos_45; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecDragLastPos_45() { return &___m_vecDragLastPos_45; }
	inline void set_m_vecDragLastPos_45(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecDragLastPos_45 = value;
	}

	inline static int32_t get_offset_of_m_bDragging_46() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_bDragging_46)); }
	inline bool get_m_bDragging_46() const { return ___m_bDragging_46; }
	inline bool* get_address_of_m_bDragging_46() { return &___m_bDragging_46; }
	inline void set_m_bDragging_46(bool value)
	{
		___m_bDragging_46 = value;
	}

	inline static int32_t get_offset_of_m_dicPointsForLevels_47() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_dicPointsForLevels_47)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_dicPointsForLevels_47() const { return ___m_dicPointsForLevels_47; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_dicPointsForLevels_47() { return &___m_dicPointsForLevels_47; }
	inline void set_m_dicPointsForLevels_47(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_dicPointsForLevels_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPointsForLevels_47), value);
	}

	inline static int32_t get_offset_of_m_bTalentConfigLoaded_48() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E, ___m_bTalentConfigLoaded_48)); }
	inline bool get_m_bTalentConfigLoaded_48() const { return ___m_bTalentConfigLoaded_48; }
	inline bool* get_address_of_m_bTalentConfigLoaded_48() { return &___m_bTalentConfigLoaded_48; }
	inline void set_m_bTalentConfigLoaded_48(bool value)
	{
		___m_bTalentConfigLoaded_48 = value;
	}
};

struct ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E_StaticFields
{
public:
	// ScienceTree ScienceTree::s_Instance
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E * ___s_Instance_4;
	// UnityEngine.Vector3 ScienceTree::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_10;
	// UnityEngine.Vector3 ScienceTree::vecTempPos1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos1_11;
	// UnityEngine.Vector3 ScienceTree::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_12;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E_StaticFields, ___s_Instance_4)); }
	inline ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E * get_s_Instance_4() const { return ___s_Instance_4; }
	inline ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_vecTempPos_10() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E_StaticFields, ___vecTempPos_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_10() const { return ___vecTempPos_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_10() { return &___vecTempPos_10; }
	inline void set_vecTempPos_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_10 = value;
	}

	inline static int32_t get_offset_of_vecTempPos1_11() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E_StaticFields, ___vecTempPos1_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos1_11() const { return ___vecTempPos1_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos1_11() { return &___vecTempPos1_11; }
	inline void set_vecTempPos1_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos1_11 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_12() { return static_cast<int32_t>(offsetof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E_StaticFields, ___vecTempScale_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_12() const { return ___vecTempScale_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_12() { return &___vecTempScale_12; }
	inline void set_vecTempScale_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCIENCETREE_T57927F0C7068DCB090379E0B11DC86D2D88A2D2E_H
#ifndef SCIENCETREECONFIG_T01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766_H
#define SCIENCETREECONFIG_T01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScienceTreeConfig
struct  ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single[] ScienceTreeConfig::aryValue
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___aryValue_4;
	// System.Int32[] ScienceTreeConfig::aryPrice
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___aryPrice_5;
	// System.String ScienceTreeConfig::szDesc
	String_t* ___szDesc_6;
	// System.String ScienceTreeConfig::m_szKey
	String_t* ___m_szKey_7;
	// ScienceLeaf ScienceTreeConfig::m_CurSelecedLeaf
	ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718 * ___m_CurSelecedLeaf_8;
	// ScienceTree/eScienceType ScienceTreeConfig::m_eScienceType
	int32_t ___m_eScienceType_9;

public:
	inline static int32_t get_offset_of_aryValue_4() { return static_cast<int32_t>(offsetof(ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766, ___aryValue_4)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_aryValue_4() const { return ___aryValue_4; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_aryValue_4() { return &___aryValue_4; }
	inline void set_aryValue_4(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___aryValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___aryValue_4), value);
	}

	inline static int32_t get_offset_of_aryPrice_5() { return static_cast<int32_t>(offsetof(ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766, ___aryPrice_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_aryPrice_5() const { return ___aryPrice_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_aryPrice_5() { return &___aryPrice_5; }
	inline void set_aryPrice_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___aryPrice_5 = value;
		Il2CppCodeGenWriteBarrier((&___aryPrice_5), value);
	}

	inline static int32_t get_offset_of_szDesc_6() { return static_cast<int32_t>(offsetof(ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766, ___szDesc_6)); }
	inline String_t* get_szDesc_6() const { return ___szDesc_6; }
	inline String_t** get_address_of_szDesc_6() { return &___szDesc_6; }
	inline void set_szDesc_6(String_t* value)
	{
		___szDesc_6 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_6), value);
	}

	inline static int32_t get_offset_of_m_szKey_7() { return static_cast<int32_t>(offsetof(ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766, ___m_szKey_7)); }
	inline String_t* get_m_szKey_7() const { return ___m_szKey_7; }
	inline String_t** get_address_of_m_szKey_7() { return &___m_szKey_7; }
	inline void set_m_szKey_7(String_t* value)
	{
		___m_szKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_szKey_7), value);
	}

	inline static int32_t get_offset_of_m_CurSelecedLeaf_8() { return static_cast<int32_t>(offsetof(ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766, ___m_CurSelecedLeaf_8)); }
	inline ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718 * get_m_CurSelecedLeaf_8() const { return ___m_CurSelecedLeaf_8; }
	inline ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718 ** get_address_of_m_CurSelecedLeaf_8() { return &___m_CurSelecedLeaf_8; }
	inline void set_m_CurSelecedLeaf_8(ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718 * value)
	{
		___m_CurSelecedLeaf_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurSelecedLeaf_8), value);
	}

	inline static int32_t get_offset_of_m_eScienceType_9() { return static_cast<int32_t>(offsetof(ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766, ___m_eScienceType_9)); }
	inline int32_t get_m_eScienceType_9() const { return ___m_eScienceType_9; }
	inline int32_t* get_address_of_m_eScienceType_9() { return &___m_eScienceType_9; }
	inline void set_m_eScienceType_9(int32_t value)
	{
		___m_eScienceType_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCIENCETREECONFIG_T01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766_H
#ifndef SHOPPINMALL_T6389C5391B85521387589717F7A53592DB917811_H
#define SHOPPINMALL_T6389C5391B85521387589717F7A53592DB917811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppinMall
struct  ShoppinMall_t6389C5391B85521387589717F7A53592DB917811  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.VerticalLayoutGroup ShoppinMall::_vlg
	VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 * ____vlg_7;
	// UnityEngine.GameObject ShoppinMall::m_preTypeContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preTypeContainer_8;
	// UnityEngine.GameObject ShoppinMall::m_preItemCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preItemCounter_9;
	// UnityEngine.GameObject[] ShoppinMall::m_aryCounterPrefab
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryCounterPrefab_10;
	// System.Int32 ShoppinMall::m_nStandardCounterNumPerRow
	int32_t ___m_nStandardCounterNumPerRow_11;
	// System.Single ShoppinMall::m_fStandardCounterWidth
	float ___m_fStandardCounterWidth_12;
	// System.Single ShoppinMall::m_fStandardCounterHeight
	float ___m_fStandardCounterHeight_13;
	// System.Single ShoppinMall::m_fStandardCounterHeight_Long
	float ___m_fStandardCounterHeight_Long_14;
	// System.Single ShoppinMall::m_fTypeContainerTitleHeight
	float ___m_fTypeContainerTitleHeight_15;
	// UnityEngine.Vector2 ShoppinMall::m_vecTypeContainerPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecTypeContainerPos_16;
	// UnityEngine.GameObject ShoppinMall::m_goContainerAllType
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goContainerAllType_17;
	// UnityEngine.GameObject ShoppinMall::_panelShoppingMall
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelShoppingMall_18;
	// UnityEngine.GameObject ShoppinMall::_subpanelConfirmBuy
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____subpanelConfirmBuy_19;
	// UnityEngine.GameObject ShoppinMall::_subpanelConfirmUse
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____subpanelConfirmUse_20;
	// UIItem ShoppinMall::_CurProcessingBuyUiItem
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957 * ____CurProcessingBuyUiItem_21;
	// UIItemInBag ShoppinMall::_CurProcessingUsingUiItem
	UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6 * ____CurProcessingUsingUiItem_22;
	// UnityEngine.UI.Text ShoppinMall::_txtConfirmBuy_FuncAndValue
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtConfirmBuy_FuncAndValue_23;
	// UnityEngine.UI.Text ShoppinMall::_txtConfirmBuy_Cost
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtConfirmBuy_Cost_24;
	// UnityEngine.UI.Text ShoppinMall::_txtConfirmBuy_ItemName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtConfirmBuy_ItemName_25;
	// UnityEngine.UI.Text ShoppinMall::_txtConfirmBuy_Value
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtConfirmBuy_Value_26;
	// UnityEngine.UI.Image ShoppinMall::_imgConfirmBuy_Avatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgConfirmBuy_Avatar_27;
	// UnityEngine.UI.Image ShoppinMall::_imgConfirmUse_Avatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgConfirmUse_Avatar_28;
	// ShoppinMall/sShoppingMallItemConfig ShoppinMall::m_ConfirmBuyShoppingConfig
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  ___m_ConfirmBuyShoppingConfig_29;
	// System.Double ShoppinMall::m_nConfirmBuy_Price
	double ___m_nConfirmBuy_Price_30;
	// System.Int32 ShoppinMall::m_nConfirmBuy_Num
	int32_t ___m_nConfirmBuy_Num_31;
	// UIItem ShoppinMall::m_itemConfirmBuy
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957 * ___m_itemConfirmBuy_32;
	// ShoppinMall/sShoppingMallItemConfig ShoppinMall::tempShoppinMallItemConfig
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  ___tempShoppinMallItemConfig_33;
	// System.Collections.Generic.Dictionary`2<ShoppinMall/eItemType,System.Collections.Generic.List`1<ShoppinMall/sShoppingMallItemConfig>> ShoppinMall::m_dicShoppingMallItems
	Dictionary_2_tC88F31954E3D7F66998673CFA25C2B6EB95412DF * ___m_dicShoppingMallItems_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> ShoppinMall::m_dicItemBuyTimes
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_dicItemBuyTimes_35;
	// System.Boolean ShoppinMall::m_bShoppinmallLoaded
	bool ___m_bShoppinmallLoaded_37;
	// System.Collections.Generic.Dictionary`2<System.Int32,ShoppinMall/sShoppingMallItemConfig> ShoppinMall::m_dicAllItems
	Dictionary_2_t85BD9605BBAFF169EBF8E31434102C544A2649ED * ___m_dicAllItems_38;
	// System.Collections.Generic.List`1<ShoppinMallTypeContainer> ShoppinMall::m_lstRecycledTypeContainers
	List_1_t5485117BEBD07C46F8E6C085089A3EF3A8103DB4 * ___m_lstRecycledTypeContainers_39;
	// UnityEngine.GameObject ShoppinMall::m_containerRecycledItems
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_containerRecycledItems_40;
	// System.Collections.Generic.List`1<UIItem> ShoppinMall::m_lstRecycledBuyCounters
	List_1_t7302F8A5155D7A00D77040895186DF4F7D8A896D * ___m_lstRecycledBuyCounters_41;
	// UnityEngine.Sprite[] ShoppinMall::m_aryAvatarBg
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAvatarBg_42;
	// UnityEngine.UI.Image ShoppinMall::_imgCurBuyCounterAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCurBuyCounterAvatar_43;
	// UnityEngine.UI.Image ShoppinMall::_imgCurBuyCounterAvatarBg
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCurBuyCounterAvatarBg_44;
	// UnityEngine.UI.Text ShoppinMall::_txtCurBuyCounterDuration
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCurBuyCounterDuration_45;
	// UnityEngine.UI.Text ShoppinMall::_txtCurBuyCounterValue
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCurBuyCounterValue_46;
	// UIShoppinAndItemCounter ShoppinMall::m_CurBuyCounter
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169 * ___m_CurBuyCounter_47;
	// System.Int32 ShoppinMall::m_nBuyMoneyType
	int32_t ___m_nBuyMoneyType_48;
	// System.Collections.Generic.Dictionary`2<ShoppinMall/eItemType,System.Collections.Generic.List`1<UIShoppinAndItemCounter>> ShoppinMall::m_dicRecycledCounters
	Dictionary_2_tE1ED95465856839DB9DE1550C696F1B7505215AC * ___m_dicRecycledCounters_49;
	// System.Collections.Generic.List`1<UIShoppinAndItemCounter> ShoppinMall::m_lstCurShowingItems
	List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * ___m_lstCurShowingItems_50;
	// System.Collections.Generic.Dictionary`2<ShoppinMall/eItemType,System.Int32> ShoppinMall::m_dicShoppinTypes
	Dictionary_2_tEA60B96640F887360F2D1F134CCCD59FC6C37840 * ___m_dicShoppinTypes_51;
	// System.Collections.Generic.List`1<ShoppinMallTypeContainer> ShoppinMall::m_lstAllShowingTypeContainers
	List_1_t5485117BEBD07C46F8E6C085089A3EF3A8103DB4 * ___m_lstAllShowingTypeContainers_52;
	// ShoppinMallTypeContainer ShoppinMall::m_TypeContainerOfSkillPoint
	ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555 * ___m_TypeContainerOfSkillPoint_53;
	// System.Boolean ShoppinMall::m_bShoppinMallShowing
	bool ___m_bShoppinMallShowing_54;

public:
	inline static int32_t get_offset_of__vlg_7() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____vlg_7)); }
	inline VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 * get__vlg_7() const { return ____vlg_7; }
	inline VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 ** get_address_of__vlg_7() { return &____vlg_7; }
	inline void set__vlg_7(VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 * value)
	{
		____vlg_7 = value;
		Il2CppCodeGenWriteBarrier((&____vlg_7), value);
	}

	inline static int32_t get_offset_of_m_preTypeContainer_8() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_preTypeContainer_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preTypeContainer_8() const { return ___m_preTypeContainer_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preTypeContainer_8() { return &___m_preTypeContainer_8; }
	inline void set_m_preTypeContainer_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preTypeContainer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_preTypeContainer_8), value);
	}

	inline static int32_t get_offset_of_m_preItemCounter_9() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_preItemCounter_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preItemCounter_9() const { return ___m_preItemCounter_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preItemCounter_9() { return &___m_preItemCounter_9; }
	inline void set_m_preItemCounter_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preItemCounter_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_preItemCounter_9), value);
	}

	inline static int32_t get_offset_of_m_aryCounterPrefab_10() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_aryCounterPrefab_10)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryCounterPrefab_10() const { return ___m_aryCounterPrefab_10; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryCounterPrefab_10() { return &___m_aryCounterPrefab_10; }
	inline void set_m_aryCounterPrefab_10(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryCounterPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCounterPrefab_10), value);
	}

	inline static int32_t get_offset_of_m_nStandardCounterNumPerRow_11() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_nStandardCounterNumPerRow_11)); }
	inline int32_t get_m_nStandardCounterNumPerRow_11() const { return ___m_nStandardCounterNumPerRow_11; }
	inline int32_t* get_address_of_m_nStandardCounterNumPerRow_11() { return &___m_nStandardCounterNumPerRow_11; }
	inline void set_m_nStandardCounterNumPerRow_11(int32_t value)
	{
		___m_nStandardCounterNumPerRow_11 = value;
	}

	inline static int32_t get_offset_of_m_fStandardCounterWidth_12() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_fStandardCounterWidth_12)); }
	inline float get_m_fStandardCounterWidth_12() const { return ___m_fStandardCounterWidth_12; }
	inline float* get_address_of_m_fStandardCounterWidth_12() { return &___m_fStandardCounterWidth_12; }
	inline void set_m_fStandardCounterWidth_12(float value)
	{
		___m_fStandardCounterWidth_12 = value;
	}

	inline static int32_t get_offset_of_m_fStandardCounterHeight_13() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_fStandardCounterHeight_13)); }
	inline float get_m_fStandardCounterHeight_13() const { return ___m_fStandardCounterHeight_13; }
	inline float* get_address_of_m_fStandardCounterHeight_13() { return &___m_fStandardCounterHeight_13; }
	inline void set_m_fStandardCounterHeight_13(float value)
	{
		___m_fStandardCounterHeight_13 = value;
	}

	inline static int32_t get_offset_of_m_fStandardCounterHeight_Long_14() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_fStandardCounterHeight_Long_14)); }
	inline float get_m_fStandardCounterHeight_Long_14() const { return ___m_fStandardCounterHeight_Long_14; }
	inline float* get_address_of_m_fStandardCounterHeight_Long_14() { return &___m_fStandardCounterHeight_Long_14; }
	inline void set_m_fStandardCounterHeight_Long_14(float value)
	{
		___m_fStandardCounterHeight_Long_14 = value;
	}

	inline static int32_t get_offset_of_m_fTypeContainerTitleHeight_15() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_fTypeContainerTitleHeight_15)); }
	inline float get_m_fTypeContainerTitleHeight_15() const { return ___m_fTypeContainerTitleHeight_15; }
	inline float* get_address_of_m_fTypeContainerTitleHeight_15() { return &___m_fTypeContainerTitleHeight_15; }
	inline void set_m_fTypeContainerTitleHeight_15(float value)
	{
		___m_fTypeContainerTitleHeight_15 = value;
	}

	inline static int32_t get_offset_of_m_vecTypeContainerPos_16() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_vecTypeContainerPos_16)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecTypeContainerPos_16() const { return ___m_vecTypeContainerPos_16; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecTypeContainerPos_16() { return &___m_vecTypeContainerPos_16; }
	inline void set_m_vecTypeContainerPos_16(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecTypeContainerPos_16 = value;
	}

	inline static int32_t get_offset_of_m_goContainerAllType_17() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_goContainerAllType_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goContainerAllType_17() const { return ___m_goContainerAllType_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goContainerAllType_17() { return &___m_goContainerAllType_17; }
	inline void set_m_goContainerAllType_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goContainerAllType_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainerAllType_17), value);
	}

	inline static int32_t get_offset_of__panelShoppingMall_18() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____panelShoppingMall_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelShoppingMall_18() const { return ____panelShoppingMall_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelShoppingMall_18() { return &____panelShoppingMall_18; }
	inline void set__panelShoppingMall_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelShoppingMall_18 = value;
		Il2CppCodeGenWriteBarrier((&____panelShoppingMall_18), value);
	}

	inline static int32_t get_offset_of__subpanelConfirmBuy_19() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____subpanelConfirmBuy_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__subpanelConfirmBuy_19() const { return ____subpanelConfirmBuy_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__subpanelConfirmBuy_19() { return &____subpanelConfirmBuy_19; }
	inline void set__subpanelConfirmBuy_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____subpanelConfirmBuy_19 = value;
		Il2CppCodeGenWriteBarrier((&____subpanelConfirmBuy_19), value);
	}

	inline static int32_t get_offset_of__subpanelConfirmUse_20() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____subpanelConfirmUse_20)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__subpanelConfirmUse_20() const { return ____subpanelConfirmUse_20; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__subpanelConfirmUse_20() { return &____subpanelConfirmUse_20; }
	inline void set__subpanelConfirmUse_20(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____subpanelConfirmUse_20 = value;
		Il2CppCodeGenWriteBarrier((&____subpanelConfirmUse_20), value);
	}

	inline static int32_t get_offset_of__CurProcessingBuyUiItem_21() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____CurProcessingBuyUiItem_21)); }
	inline UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957 * get__CurProcessingBuyUiItem_21() const { return ____CurProcessingBuyUiItem_21; }
	inline UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957 ** get_address_of__CurProcessingBuyUiItem_21() { return &____CurProcessingBuyUiItem_21; }
	inline void set__CurProcessingBuyUiItem_21(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957 * value)
	{
		____CurProcessingBuyUiItem_21 = value;
		Il2CppCodeGenWriteBarrier((&____CurProcessingBuyUiItem_21), value);
	}

	inline static int32_t get_offset_of__CurProcessingUsingUiItem_22() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____CurProcessingUsingUiItem_22)); }
	inline UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6 * get__CurProcessingUsingUiItem_22() const { return ____CurProcessingUsingUiItem_22; }
	inline UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6 ** get_address_of__CurProcessingUsingUiItem_22() { return &____CurProcessingUsingUiItem_22; }
	inline void set__CurProcessingUsingUiItem_22(UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6 * value)
	{
		____CurProcessingUsingUiItem_22 = value;
		Il2CppCodeGenWriteBarrier((&____CurProcessingUsingUiItem_22), value);
	}

	inline static int32_t get_offset_of__txtConfirmBuy_FuncAndValue_23() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____txtConfirmBuy_FuncAndValue_23)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtConfirmBuy_FuncAndValue_23() const { return ____txtConfirmBuy_FuncAndValue_23; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtConfirmBuy_FuncAndValue_23() { return &____txtConfirmBuy_FuncAndValue_23; }
	inline void set__txtConfirmBuy_FuncAndValue_23(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtConfirmBuy_FuncAndValue_23 = value;
		Il2CppCodeGenWriteBarrier((&____txtConfirmBuy_FuncAndValue_23), value);
	}

	inline static int32_t get_offset_of__txtConfirmBuy_Cost_24() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____txtConfirmBuy_Cost_24)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtConfirmBuy_Cost_24() const { return ____txtConfirmBuy_Cost_24; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtConfirmBuy_Cost_24() { return &____txtConfirmBuy_Cost_24; }
	inline void set__txtConfirmBuy_Cost_24(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtConfirmBuy_Cost_24 = value;
		Il2CppCodeGenWriteBarrier((&____txtConfirmBuy_Cost_24), value);
	}

	inline static int32_t get_offset_of__txtConfirmBuy_ItemName_25() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____txtConfirmBuy_ItemName_25)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtConfirmBuy_ItemName_25() const { return ____txtConfirmBuy_ItemName_25; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtConfirmBuy_ItemName_25() { return &____txtConfirmBuy_ItemName_25; }
	inline void set__txtConfirmBuy_ItemName_25(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtConfirmBuy_ItemName_25 = value;
		Il2CppCodeGenWriteBarrier((&____txtConfirmBuy_ItemName_25), value);
	}

	inline static int32_t get_offset_of__txtConfirmBuy_Value_26() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____txtConfirmBuy_Value_26)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtConfirmBuy_Value_26() const { return ____txtConfirmBuy_Value_26; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtConfirmBuy_Value_26() { return &____txtConfirmBuy_Value_26; }
	inline void set__txtConfirmBuy_Value_26(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtConfirmBuy_Value_26 = value;
		Il2CppCodeGenWriteBarrier((&____txtConfirmBuy_Value_26), value);
	}

	inline static int32_t get_offset_of__imgConfirmBuy_Avatar_27() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____imgConfirmBuy_Avatar_27)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgConfirmBuy_Avatar_27() const { return ____imgConfirmBuy_Avatar_27; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgConfirmBuy_Avatar_27() { return &____imgConfirmBuy_Avatar_27; }
	inline void set__imgConfirmBuy_Avatar_27(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgConfirmBuy_Avatar_27 = value;
		Il2CppCodeGenWriteBarrier((&____imgConfirmBuy_Avatar_27), value);
	}

	inline static int32_t get_offset_of__imgConfirmUse_Avatar_28() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____imgConfirmUse_Avatar_28)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgConfirmUse_Avatar_28() const { return ____imgConfirmUse_Avatar_28; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgConfirmUse_Avatar_28() { return &____imgConfirmUse_Avatar_28; }
	inline void set__imgConfirmUse_Avatar_28(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgConfirmUse_Avatar_28 = value;
		Il2CppCodeGenWriteBarrier((&____imgConfirmUse_Avatar_28), value);
	}

	inline static int32_t get_offset_of_m_ConfirmBuyShoppingConfig_29() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_ConfirmBuyShoppingConfig_29)); }
	inline sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  get_m_ConfirmBuyShoppingConfig_29() const { return ___m_ConfirmBuyShoppingConfig_29; }
	inline sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058 * get_address_of_m_ConfirmBuyShoppingConfig_29() { return &___m_ConfirmBuyShoppingConfig_29; }
	inline void set_m_ConfirmBuyShoppingConfig_29(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  value)
	{
		___m_ConfirmBuyShoppingConfig_29 = value;
	}

	inline static int32_t get_offset_of_m_nConfirmBuy_Price_30() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_nConfirmBuy_Price_30)); }
	inline double get_m_nConfirmBuy_Price_30() const { return ___m_nConfirmBuy_Price_30; }
	inline double* get_address_of_m_nConfirmBuy_Price_30() { return &___m_nConfirmBuy_Price_30; }
	inline void set_m_nConfirmBuy_Price_30(double value)
	{
		___m_nConfirmBuy_Price_30 = value;
	}

	inline static int32_t get_offset_of_m_nConfirmBuy_Num_31() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_nConfirmBuy_Num_31)); }
	inline int32_t get_m_nConfirmBuy_Num_31() const { return ___m_nConfirmBuy_Num_31; }
	inline int32_t* get_address_of_m_nConfirmBuy_Num_31() { return &___m_nConfirmBuy_Num_31; }
	inline void set_m_nConfirmBuy_Num_31(int32_t value)
	{
		___m_nConfirmBuy_Num_31 = value;
	}

	inline static int32_t get_offset_of_m_itemConfirmBuy_32() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_itemConfirmBuy_32)); }
	inline UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957 * get_m_itemConfirmBuy_32() const { return ___m_itemConfirmBuy_32; }
	inline UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957 ** get_address_of_m_itemConfirmBuy_32() { return &___m_itemConfirmBuy_32; }
	inline void set_m_itemConfirmBuy_32(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957 * value)
	{
		___m_itemConfirmBuy_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_itemConfirmBuy_32), value);
	}

	inline static int32_t get_offset_of_tempShoppinMallItemConfig_33() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___tempShoppinMallItemConfig_33)); }
	inline sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  get_tempShoppinMallItemConfig_33() const { return ___tempShoppinMallItemConfig_33; }
	inline sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058 * get_address_of_tempShoppinMallItemConfig_33() { return &___tempShoppinMallItemConfig_33; }
	inline void set_tempShoppinMallItemConfig_33(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  value)
	{
		___tempShoppinMallItemConfig_33 = value;
	}

	inline static int32_t get_offset_of_m_dicShoppingMallItems_34() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_dicShoppingMallItems_34)); }
	inline Dictionary_2_tC88F31954E3D7F66998673CFA25C2B6EB95412DF * get_m_dicShoppingMallItems_34() const { return ___m_dicShoppingMallItems_34; }
	inline Dictionary_2_tC88F31954E3D7F66998673CFA25C2B6EB95412DF ** get_address_of_m_dicShoppingMallItems_34() { return &___m_dicShoppingMallItems_34; }
	inline void set_m_dicShoppingMallItems_34(Dictionary_2_tC88F31954E3D7F66998673CFA25C2B6EB95412DF * value)
	{
		___m_dicShoppingMallItems_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicShoppingMallItems_34), value);
	}

	inline static int32_t get_offset_of_m_dicItemBuyTimes_35() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_dicItemBuyTimes_35)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_dicItemBuyTimes_35() const { return ___m_dicItemBuyTimes_35; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_dicItemBuyTimes_35() { return &___m_dicItemBuyTimes_35; }
	inline void set_m_dicItemBuyTimes_35(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_dicItemBuyTimes_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicItemBuyTimes_35), value);
	}

	inline static int32_t get_offset_of_m_bShoppinmallLoaded_37() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_bShoppinmallLoaded_37)); }
	inline bool get_m_bShoppinmallLoaded_37() const { return ___m_bShoppinmallLoaded_37; }
	inline bool* get_address_of_m_bShoppinmallLoaded_37() { return &___m_bShoppinmallLoaded_37; }
	inline void set_m_bShoppinmallLoaded_37(bool value)
	{
		___m_bShoppinmallLoaded_37 = value;
	}

	inline static int32_t get_offset_of_m_dicAllItems_38() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_dicAllItems_38)); }
	inline Dictionary_2_t85BD9605BBAFF169EBF8E31434102C544A2649ED * get_m_dicAllItems_38() const { return ___m_dicAllItems_38; }
	inline Dictionary_2_t85BD9605BBAFF169EBF8E31434102C544A2649ED ** get_address_of_m_dicAllItems_38() { return &___m_dicAllItems_38; }
	inline void set_m_dicAllItems_38(Dictionary_2_t85BD9605BBAFF169EBF8E31434102C544A2649ED * value)
	{
		___m_dicAllItems_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAllItems_38), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledTypeContainers_39() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_lstRecycledTypeContainers_39)); }
	inline List_1_t5485117BEBD07C46F8E6C085089A3EF3A8103DB4 * get_m_lstRecycledTypeContainers_39() const { return ___m_lstRecycledTypeContainers_39; }
	inline List_1_t5485117BEBD07C46F8E6C085089A3EF3A8103DB4 ** get_address_of_m_lstRecycledTypeContainers_39() { return &___m_lstRecycledTypeContainers_39; }
	inline void set_m_lstRecycledTypeContainers_39(List_1_t5485117BEBD07C46F8E6C085089A3EF3A8103DB4 * value)
	{
		___m_lstRecycledTypeContainers_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledTypeContainers_39), value);
	}

	inline static int32_t get_offset_of_m_containerRecycledItems_40() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_containerRecycledItems_40)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_containerRecycledItems_40() const { return ___m_containerRecycledItems_40; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_containerRecycledItems_40() { return &___m_containerRecycledItems_40; }
	inline void set_m_containerRecycledItems_40(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_containerRecycledItems_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_containerRecycledItems_40), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledBuyCounters_41() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_lstRecycledBuyCounters_41)); }
	inline List_1_t7302F8A5155D7A00D77040895186DF4F7D8A896D * get_m_lstRecycledBuyCounters_41() const { return ___m_lstRecycledBuyCounters_41; }
	inline List_1_t7302F8A5155D7A00D77040895186DF4F7D8A896D ** get_address_of_m_lstRecycledBuyCounters_41() { return &___m_lstRecycledBuyCounters_41; }
	inline void set_m_lstRecycledBuyCounters_41(List_1_t7302F8A5155D7A00D77040895186DF4F7D8A896D * value)
	{
		___m_lstRecycledBuyCounters_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledBuyCounters_41), value);
	}

	inline static int32_t get_offset_of_m_aryAvatarBg_42() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_aryAvatarBg_42)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAvatarBg_42() const { return ___m_aryAvatarBg_42; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAvatarBg_42() { return &___m_aryAvatarBg_42; }
	inline void set_m_aryAvatarBg_42(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAvatarBg_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAvatarBg_42), value);
	}

	inline static int32_t get_offset_of__imgCurBuyCounterAvatar_43() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____imgCurBuyCounterAvatar_43)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCurBuyCounterAvatar_43() const { return ____imgCurBuyCounterAvatar_43; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCurBuyCounterAvatar_43() { return &____imgCurBuyCounterAvatar_43; }
	inline void set__imgCurBuyCounterAvatar_43(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCurBuyCounterAvatar_43 = value;
		Il2CppCodeGenWriteBarrier((&____imgCurBuyCounterAvatar_43), value);
	}

	inline static int32_t get_offset_of__imgCurBuyCounterAvatarBg_44() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____imgCurBuyCounterAvatarBg_44)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCurBuyCounterAvatarBg_44() const { return ____imgCurBuyCounterAvatarBg_44; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCurBuyCounterAvatarBg_44() { return &____imgCurBuyCounterAvatarBg_44; }
	inline void set__imgCurBuyCounterAvatarBg_44(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCurBuyCounterAvatarBg_44 = value;
		Il2CppCodeGenWriteBarrier((&____imgCurBuyCounterAvatarBg_44), value);
	}

	inline static int32_t get_offset_of__txtCurBuyCounterDuration_45() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____txtCurBuyCounterDuration_45)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCurBuyCounterDuration_45() const { return ____txtCurBuyCounterDuration_45; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCurBuyCounterDuration_45() { return &____txtCurBuyCounterDuration_45; }
	inline void set__txtCurBuyCounterDuration_45(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCurBuyCounterDuration_45 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurBuyCounterDuration_45), value);
	}

	inline static int32_t get_offset_of__txtCurBuyCounterValue_46() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ____txtCurBuyCounterValue_46)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCurBuyCounterValue_46() const { return ____txtCurBuyCounterValue_46; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCurBuyCounterValue_46() { return &____txtCurBuyCounterValue_46; }
	inline void set__txtCurBuyCounterValue_46(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCurBuyCounterValue_46 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurBuyCounterValue_46), value);
	}

	inline static int32_t get_offset_of_m_CurBuyCounter_47() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_CurBuyCounter_47)); }
	inline UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169 * get_m_CurBuyCounter_47() const { return ___m_CurBuyCounter_47; }
	inline UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169 ** get_address_of_m_CurBuyCounter_47() { return &___m_CurBuyCounter_47; }
	inline void set_m_CurBuyCounter_47(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169 * value)
	{
		___m_CurBuyCounter_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurBuyCounter_47), value);
	}

	inline static int32_t get_offset_of_m_nBuyMoneyType_48() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_nBuyMoneyType_48)); }
	inline int32_t get_m_nBuyMoneyType_48() const { return ___m_nBuyMoneyType_48; }
	inline int32_t* get_address_of_m_nBuyMoneyType_48() { return &___m_nBuyMoneyType_48; }
	inline void set_m_nBuyMoneyType_48(int32_t value)
	{
		___m_nBuyMoneyType_48 = value;
	}

	inline static int32_t get_offset_of_m_dicRecycledCounters_49() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_dicRecycledCounters_49)); }
	inline Dictionary_2_tE1ED95465856839DB9DE1550C696F1B7505215AC * get_m_dicRecycledCounters_49() const { return ___m_dicRecycledCounters_49; }
	inline Dictionary_2_tE1ED95465856839DB9DE1550C696F1B7505215AC ** get_address_of_m_dicRecycledCounters_49() { return &___m_dicRecycledCounters_49; }
	inline void set_m_dicRecycledCounters_49(Dictionary_2_tE1ED95465856839DB9DE1550C696F1B7505215AC * value)
	{
		___m_dicRecycledCounters_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicRecycledCounters_49), value);
	}

	inline static int32_t get_offset_of_m_lstCurShowingItems_50() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_lstCurShowingItems_50)); }
	inline List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * get_m_lstCurShowingItems_50() const { return ___m_lstCurShowingItems_50; }
	inline List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B ** get_address_of_m_lstCurShowingItems_50() { return &___m_lstCurShowingItems_50; }
	inline void set_m_lstCurShowingItems_50(List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * value)
	{
		___m_lstCurShowingItems_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstCurShowingItems_50), value);
	}

	inline static int32_t get_offset_of_m_dicShoppinTypes_51() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_dicShoppinTypes_51)); }
	inline Dictionary_2_tEA60B96640F887360F2D1F134CCCD59FC6C37840 * get_m_dicShoppinTypes_51() const { return ___m_dicShoppinTypes_51; }
	inline Dictionary_2_tEA60B96640F887360F2D1F134CCCD59FC6C37840 ** get_address_of_m_dicShoppinTypes_51() { return &___m_dicShoppinTypes_51; }
	inline void set_m_dicShoppinTypes_51(Dictionary_2_tEA60B96640F887360F2D1F134CCCD59FC6C37840 * value)
	{
		___m_dicShoppinTypes_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicShoppinTypes_51), value);
	}

	inline static int32_t get_offset_of_m_lstAllShowingTypeContainers_52() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_lstAllShowingTypeContainers_52)); }
	inline List_1_t5485117BEBD07C46F8E6C085089A3EF3A8103DB4 * get_m_lstAllShowingTypeContainers_52() const { return ___m_lstAllShowingTypeContainers_52; }
	inline List_1_t5485117BEBD07C46F8E6C085089A3EF3A8103DB4 ** get_address_of_m_lstAllShowingTypeContainers_52() { return &___m_lstAllShowingTypeContainers_52; }
	inline void set_m_lstAllShowingTypeContainers_52(List_1_t5485117BEBD07C46F8E6C085089A3EF3A8103DB4 * value)
	{
		___m_lstAllShowingTypeContainers_52 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstAllShowingTypeContainers_52), value);
	}

	inline static int32_t get_offset_of_m_TypeContainerOfSkillPoint_53() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_TypeContainerOfSkillPoint_53)); }
	inline ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555 * get_m_TypeContainerOfSkillPoint_53() const { return ___m_TypeContainerOfSkillPoint_53; }
	inline ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555 ** get_address_of_m_TypeContainerOfSkillPoint_53() { return &___m_TypeContainerOfSkillPoint_53; }
	inline void set_m_TypeContainerOfSkillPoint_53(ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555 * value)
	{
		___m_TypeContainerOfSkillPoint_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeContainerOfSkillPoint_53), value);
	}

	inline static int32_t get_offset_of_m_bShoppinMallShowing_54() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811, ___m_bShoppinMallShowing_54)); }
	inline bool get_m_bShoppinMallShowing_54() const { return ___m_bShoppinMallShowing_54; }
	inline bool* get_address_of_m_bShoppinMallShowing_54() { return &___m_bShoppinMallShowing_54; }
	inline void set_m_bShoppinMallShowing_54(bool value)
	{
		___m_bShoppinMallShowing_54 = value;
	}
};

struct ShoppinMall_t6389C5391B85521387589717F7A53592DB917811_StaticFields
{
public:
	// UnityEngine.Vector3 ShoppinMall::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_4;
	// UnityEngine.Vector3 ShoppinMall::vecScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecScale_5;
	// ShoppinMall ShoppinMall::s_Instance
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811 * ___s_Instance_6;

public:
	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811_StaticFields, ___vecTempPos_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_vecScale_5() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811_StaticFields, ___vecScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecScale_5() const { return ___vecScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecScale_5() { return &___vecScale_5; }
	inline void set_vecScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecScale_5 = value;
	}

	inline static int32_t get_offset_of_s_Instance_6() { return static_cast<int32_t>(offsetof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811_StaticFields, ___s_Instance_6)); }
	inline ShoppinMall_t6389C5391B85521387589717F7A53592DB917811 * get_s_Instance_6() const { return ___s_Instance_6; }
	inline ShoppinMall_t6389C5391B85521387589717F7A53592DB917811 ** get_address_of_s_Instance_6() { return &___s_Instance_6; }
	inline void set_s_Instance_6(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811 * value)
	{
		___s_Instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOPPINMALL_T6389C5391B85521387589717F7A53592DB917811_H
#ifndef SKILL_T073059782D8C94AF6552F03A570459CA6FBEB2DF_H
#define SKILL_T073059782D8C94AF6552F03A570459CA6FBEB2DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Skill
struct  Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// SkillManager/eSkillType Skill::m_eType
	int32_t ___m_eType_4;
	// SkillManager/eSkillStatus Skill::m_eStatus
	int32_t ___m_eStatus_5;
	// SkillManager/sSkillConfig Skill::m_Config
	sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E  ___m_Config_6;
	// Planet Skill::m_BoundPlanet
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * ___m_BoundPlanet_7;
	// District Skill::m_BoundDistrict
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * ___m_BoundDistrict_8;
	// UISkillCastCounter Skill::m_BoundUIBUtton
	UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324 * ___m_BoundUIBUtton_9;
	// System.Single Skill::m_fStartTime
	float ___m_fStartTime_10;
	// System.DateTime Skill::m_StartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_StartTime_11;
	// System.Single Skill::m_fRealTimeDuration
	float ___m_fRealTimeDuration_12;
	// System.Single Skill::m_fTimeElapse_1Second
	float ___m_fTimeElapse_1Second_13;

public:
	inline static int32_t get_offset_of_m_eType_4() { return static_cast<int32_t>(offsetof(Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF, ___m_eType_4)); }
	inline int32_t get_m_eType_4() const { return ___m_eType_4; }
	inline int32_t* get_address_of_m_eType_4() { return &___m_eType_4; }
	inline void set_m_eType_4(int32_t value)
	{
		___m_eType_4 = value;
	}

	inline static int32_t get_offset_of_m_eStatus_5() { return static_cast<int32_t>(offsetof(Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF, ___m_eStatus_5)); }
	inline int32_t get_m_eStatus_5() const { return ___m_eStatus_5; }
	inline int32_t* get_address_of_m_eStatus_5() { return &___m_eStatus_5; }
	inline void set_m_eStatus_5(int32_t value)
	{
		___m_eStatus_5 = value;
	}

	inline static int32_t get_offset_of_m_Config_6() { return static_cast<int32_t>(offsetof(Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF, ___m_Config_6)); }
	inline sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E  get_m_Config_6() const { return ___m_Config_6; }
	inline sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E * get_address_of_m_Config_6() { return &___m_Config_6; }
	inline void set_m_Config_6(sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E  value)
	{
		___m_Config_6 = value;
	}

	inline static int32_t get_offset_of_m_BoundPlanet_7() { return static_cast<int32_t>(offsetof(Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF, ___m_BoundPlanet_7)); }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * get_m_BoundPlanet_7() const { return ___m_BoundPlanet_7; }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B ** get_address_of_m_BoundPlanet_7() { return &___m_BoundPlanet_7; }
	inline void set_m_BoundPlanet_7(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * value)
	{
		___m_BoundPlanet_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundPlanet_7), value);
	}

	inline static int32_t get_offset_of_m_BoundDistrict_8() { return static_cast<int32_t>(offsetof(Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF, ___m_BoundDistrict_8)); }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * get_m_BoundDistrict_8() const { return ___m_BoundDistrict_8; }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A ** get_address_of_m_BoundDistrict_8() { return &___m_BoundDistrict_8; }
	inline void set_m_BoundDistrict_8(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * value)
	{
		___m_BoundDistrict_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundDistrict_8), value);
	}

	inline static int32_t get_offset_of_m_BoundUIBUtton_9() { return static_cast<int32_t>(offsetof(Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF, ___m_BoundUIBUtton_9)); }
	inline UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324 * get_m_BoundUIBUtton_9() const { return ___m_BoundUIBUtton_9; }
	inline UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324 ** get_address_of_m_BoundUIBUtton_9() { return &___m_BoundUIBUtton_9; }
	inline void set_m_BoundUIBUtton_9(UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324 * value)
	{
		___m_BoundUIBUtton_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundUIBUtton_9), value);
	}

	inline static int32_t get_offset_of_m_fStartTime_10() { return static_cast<int32_t>(offsetof(Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF, ___m_fStartTime_10)); }
	inline float get_m_fStartTime_10() const { return ___m_fStartTime_10; }
	inline float* get_address_of_m_fStartTime_10() { return &___m_fStartTime_10; }
	inline void set_m_fStartTime_10(float value)
	{
		___m_fStartTime_10 = value;
	}

	inline static int32_t get_offset_of_m_StartTime_11() { return static_cast<int32_t>(offsetof(Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF, ___m_StartTime_11)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_StartTime_11() const { return ___m_StartTime_11; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_StartTime_11() { return &___m_StartTime_11; }
	inline void set_m_StartTime_11(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_StartTime_11 = value;
	}

	inline static int32_t get_offset_of_m_fRealTimeDuration_12() { return static_cast<int32_t>(offsetof(Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF, ___m_fRealTimeDuration_12)); }
	inline float get_m_fRealTimeDuration_12() const { return ___m_fRealTimeDuration_12; }
	inline float* get_address_of_m_fRealTimeDuration_12() { return &___m_fRealTimeDuration_12; }
	inline void set_m_fRealTimeDuration_12(float value)
	{
		___m_fRealTimeDuration_12 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_1Second_13() { return static_cast<int32_t>(offsetof(Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF, ___m_fTimeElapse_1Second_13)); }
	inline float get_m_fTimeElapse_1Second_13() const { return ___m_fTimeElapse_1Second_13; }
	inline float* get_address_of_m_fTimeElapse_1Second_13() { return &___m_fTimeElapse_1Second_13; }
	inline void set_m_fTimeElapse_1Second_13(float value)
	{
		___m_fTimeElapse_1Second_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKILL_T073059782D8C94AF6552F03A570459CA6FBEB2DF_H
#ifndef SKILLMANAGER_TDD7938C310F6B1DD79A1E289163A5569E4C0E231_H
#define SKILLMANAGER_TDD7938C310F6B1DD79A1E289163A5569E4C0E231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkillManager
struct  SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject SkillManager::_containerAcclerateLights
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerAcclerateLights_5;
	// System.Collections.Generic.Dictionary`2<SkillManager/eSkillType,SkillManager/sSkillConfig[]> SkillManager::m_dicSkillConfig
	Dictionary_2_t773BB6E1D26D4A9ACB7CB6040164FD7F4C4F3A6D * ___m_dicSkillConfig_7;
	// UISkillCastCounter[] SkillManager::m_arySkillButton
	UISkillCastCounterU5BU5D_t4E7D995D5ECD704EBC1A808B62174C6F4EA76349* ___m_arySkillButton_8;
	// SkillManager/sSkillConfig SkillManager::tempConfig
	sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E  ___tempConfig_9;

public:
	inline static int32_t get_offset_of__containerAcclerateLights_5() { return static_cast<int32_t>(offsetof(SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231, ____containerAcclerateLights_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerAcclerateLights_5() const { return ____containerAcclerateLights_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerAcclerateLights_5() { return &____containerAcclerateLights_5; }
	inline void set__containerAcclerateLights_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerAcclerateLights_5 = value;
		Il2CppCodeGenWriteBarrier((&____containerAcclerateLights_5), value);
	}

	inline static int32_t get_offset_of_m_dicSkillConfig_7() { return static_cast<int32_t>(offsetof(SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231, ___m_dicSkillConfig_7)); }
	inline Dictionary_2_t773BB6E1D26D4A9ACB7CB6040164FD7F4C4F3A6D * get_m_dicSkillConfig_7() const { return ___m_dicSkillConfig_7; }
	inline Dictionary_2_t773BB6E1D26D4A9ACB7CB6040164FD7F4C4F3A6D ** get_address_of_m_dicSkillConfig_7() { return &___m_dicSkillConfig_7; }
	inline void set_m_dicSkillConfig_7(Dictionary_2_t773BB6E1D26D4A9ACB7CB6040164FD7F4C4F3A6D * value)
	{
		___m_dicSkillConfig_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSkillConfig_7), value);
	}

	inline static int32_t get_offset_of_m_arySkillButton_8() { return static_cast<int32_t>(offsetof(SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231, ___m_arySkillButton_8)); }
	inline UISkillCastCounterU5BU5D_t4E7D995D5ECD704EBC1A808B62174C6F4EA76349* get_m_arySkillButton_8() const { return ___m_arySkillButton_8; }
	inline UISkillCastCounterU5BU5D_t4E7D995D5ECD704EBC1A808B62174C6F4EA76349** get_address_of_m_arySkillButton_8() { return &___m_arySkillButton_8; }
	inline void set_m_arySkillButton_8(UISkillCastCounterU5BU5D_t4E7D995D5ECD704EBC1A808B62174C6F4EA76349* value)
	{
		___m_arySkillButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillButton_8), value);
	}

	inline static int32_t get_offset_of_tempConfig_9() { return static_cast<int32_t>(offsetof(SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231, ___tempConfig_9)); }
	inline sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E  get_tempConfig_9() const { return ___tempConfig_9; }
	inline sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E * get_address_of_tempConfig_9() { return &___tempConfig_9; }
	inline void set_tempConfig_9(sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E  value)
	{
		___tempConfig_9 = value;
	}
};

struct SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231_StaticFields
{
public:
	// SkillManager SkillManager::s_Instance
	SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231_StaticFields, ___s_Instance_4)); }
	inline SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKILLMANAGER_TDD7938C310F6B1DD79A1E289163A5569E4C0E231_H
#ifndef STARTBELT_T8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD_H
#define STARTBELT_T8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartBelt
struct  StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// StartRunArrow[] StartBelt::m_aryStartArrows
	StartRunArrowU5BU5D_t03C581BC0F67FE5F0F67FEED77CA35F307D55353* ___m_aryStartArrows_5;
	// System.Int32 StartBelt::m_nMaxNum
	int32_t ___m_nMaxNum_6;
	// System.Int32 StartBelt::m_nCurNum
	int32_t ___m_nCurNum_7;

public:
	inline static int32_t get_offset_of_m_aryStartArrows_5() { return static_cast<int32_t>(offsetof(StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD, ___m_aryStartArrows_5)); }
	inline StartRunArrowU5BU5D_t03C581BC0F67FE5F0F67FEED77CA35F307D55353* get_m_aryStartArrows_5() const { return ___m_aryStartArrows_5; }
	inline StartRunArrowU5BU5D_t03C581BC0F67FE5F0F67FEED77CA35F307D55353** get_address_of_m_aryStartArrows_5() { return &___m_aryStartArrows_5; }
	inline void set_m_aryStartArrows_5(StartRunArrowU5BU5D_t03C581BC0F67FE5F0F67FEED77CA35F307D55353* value)
	{
		___m_aryStartArrows_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryStartArrows_5), value);
	}

	inline static int32_t get_offset_of_m_nMaxNum_6() { return static_cast<int32_t>(offsetof(StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD, ___m_nMaxNum_6)); }
	inline int32_t get_m_nMaxNum_6() const { return ___m_nMaxNum_6; }
	inline int32_t* get_address_of_m_nMaxNum_6() { return &___m_nMaxNum_6; }
	inline void set_m_nMaxNum_6(int32_t value)
	{
		___m_nMaxNum_6 = value;
	}

	inline static int32_t get_offset_of_m_nCurNum_7() { return static_cast<int32_t>(offsetof(StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD, ___m_nCurNum_7)); }
	inline int32_t get_m_nCurNum_7() const { return ___m_nCurNum_7; }
	inline int32_t* get_address_of_m_nCurNum_7() { return &___m_nCurNum_7; }
	inline void set_m_nCurNum_7(int32_t value)
	{
		___m_nCurNum_7 = value;
	}
};

struct StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD_StaticFields
{
public:
	// StartBelt StartBelt::s_Instance
	StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD_StaticFields, ___s_Instance_4)); }
	inline StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD * get_s_Instance_4() const { return ___s_Instance_4; }
	inline StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTBELT_T8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD_H
#ifndef STARTRUNARROW_T34112B02A95B4E2CF819CE3086EAD5F0AF6257F6_H
#define STARTRUNARROW_T34112B02A95B4E2CF819CE3086EAD5F0AF6257F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartRunArrow
struct  StartRunArrow_t34112B02A95B4E2CF819CE3086EAD5F0AF6257F6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.SpriteRenderer StartRunArrow::m_srMain
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___m_srMain_4;

public:
	inline static int32_t get_offset_of_m_srMain_4() { return static_cast<int32_t>(offsetof(StartRunArrow_t34112B02A95B4E2CF819CE3086EAD5F0AF6257F6, ___m_srMain_4)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_m_srMain_4() const { return ___m_srMain_4; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_m_srMain_4() { return &___m_srMain_4; }
	inline void set_m_srMain_4(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___m_srMain_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_srMain_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTRUNARROW_T34112B02A95B4E2CF819CE3086EAD5F0AF6257F6_H
#ifndef TANGECHE_T8AB5138CD649482ABE434C2571453BE2A5AF0455_H
#define TANGECHE_T8AB5138CD649482ABE434C2571453BE2A5AF0455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TanGeChe
struct  TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 TanGeChe::m_nFreshGuideStatus
	int32_t ___m_nFreshGuideStatus_4;
	// UnityEngine.UI.VerticalLayoutGroup TanGeChe::_vlg
	VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 * ____vlg_5;
	// UnityEngine.GameObject TanGeChe::_goFreeFlag
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goFreeFlag_6;
	// UnityEngine.GameObject TanGeChe::_goBtnTanGeChe
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goBtnTanGeChe_7;
	// UnityEngine.UI.Image TanGeChe::_imgPriceOff
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgPriceOff_8;
	// UnityEngine.Vector2 TanGeChe::m_vecPriceOffStartPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecPriceOffStartPos_9;
	// UnityEngine.Vector2 TanGeChe::m_vecPriceOffSummitPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecPriceOffSummitPos_10;
	// UnityEngine.Vector2 TanGeChe::m_vecPriceOffEndPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecPriceOffEndPos_11;
	// System.Single TanGeChe::m_fPriceOffAniTime
	float ___m_fPriceOffAniTime_12;
	// UnityEngine.Vector2 TanGeChe::m_vecPriceOffAniSpeed
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecPriceOffAniSpeed_13;
	// System.Single TanGeChe::m_fPriceOffAniA
	float ___m_fPriceOffAniA_14;
	// System.Single TanGeChe::m_fPriceOffAniV0
	float ___m_fPriceOffAniV0_15;
	// UnityEngine.UI.Text TanGeChe::_txtRecommendPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtRecommendPrice_19;
	// UnityEngine.UI.Image TanGeChe::_imgRecommendAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgRecommendAvatar_20;
	// UnityEngine.UI.Image TanGeChe::_imgRecommendCoinIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgRecommendCoinIcon_21;
	// UnityEngine.GameObject TanGeChe::_panelRecommendDetail
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelRecommendDetail_22;
	// UnityEngine.UI.Text TanGeChe::_txtRecommendDetail
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtRecommendDetail_23;
	// UnityEngine.UI.Toggle TanGeChe::_toggleNoPlane
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ____toggleNoPlane_24;
	// UnityEngine.UI.Toggle TanGeChe::_toggleNoDrop
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ____toggleNoDrop_25;
	// UnityEngine.UI.InputField TanGeChe::_inputTrackLevel
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____inputTrackLevel_26;
	// UnityEngine.GameObject TanGeChe::_goCounterList
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goCounterList_28;
	// System.Collections.Generic.List`1<UIVehicleCounter> TanGeChe::m_lstVehicleCounters
	List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * ___m_lstVehicleCounters_29;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<UIVehicleCounter>> TanGeChe::m_dicVehicleCounters
	Dictionary_2_t23E80E3DB14B3FC8A2D8537F3FCAC9C8780F1654 * ___m_dicVehicleCounters_30;
	// UnityEngine.GameObject TanGeChe::_panelTanGeChe
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelTanGeChe_31;
	// System.Single TanGeChe::m_fRealTimeDiscount
	float ___m_fRealTimeDiscount_32;
	// UnityEngine.Sprite TanGeChe::m_sprJianYingTemp
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprJianYingTemp_33;
	// UnityEngine.Sprite TanGeChe::m_sprLockedBuyButton
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprLockedBuyButton_34;
	// UnityEngine.Sprite TanGeChe::m_spUnlockedBuyButton
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_spUnlockedBuyButton_35;
	// UnityEngine.Color TanGeChe::m_colorUnlocked
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorUnlocked_36;
	// UnityEngine.Color TanGeChe::m_colorLocked
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorLocked_37;
	// System.Int32 TanGeChe::m_nRecommendLowerAmount
	int32_t ___m_nRecommendLowerAmount_38;
	// System.Int32 TanGeChe::m_nRecommendHigherAmount
	int32_t ___m_nRecommendHigherAmount_39;
	// System.Collections.Generic.List`1<UIVehicleCounter> TanGeChe::lstTempShowingCounters
	List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * ___lstTempShowingCounters_40;
	// System.Collections.Generic.List`1<UIVehicleCounter> TanGeChe::lstTempShowingUnlockedCounters
	List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * ___lstTempShowingUnlockedCounters_41;
	// System.Collections.Generic.Dictionary`2<System.Int32,UIResearchCounterContainer> TanGeChe::m_dicShareUICountersContainer
	Dictionary_2_tC10E085FDFF6C3AB8005D16563E90234EF657A79 * ___m_dicShareUICountersContainer_42;
	// System.Int32 TanGeChe::m_nCurWatchAdsFreeCounterIndex
	int32_t ___m_nCurWatchAdsFreeCounterIndex_43;
	// System.Boolean TanGeChe::m_bIsVisible
	bool ___m_bIsVisible_44;
	// System.Single TanGeChe::m_fRandomEventInterval
	float ___m_fRandomEventInterval_45;
	// System.Single TanGeChe::m_fRandomEventTimeElapse
	float ___m_fRandomEventTimeElapse_46;
	// System.Boolean TanGeChe::m_bRandomEventProcessing
	bool ___m_bRandomEventProcessing_47;
	// System.Int32 TanGeChe::s_nRecommendCount
	int32_t ___s_nRecommendCount_48;
	// District/sLevelAndCost TanGeChe::selected_one
	sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA  ___selected_one_49;
	// System.Boolean TanGeChe::m_bNoPlane
	bool ___m_bNoPlane_50;
	// System.Boolean TanGeChe::m_bNoDrop
	bool ___m_bNoDrop_51;
	// System.Int32 TanGeChe::m_nPriceOffAnimationStatus
	int32_t ___m_nPriceOffAnimationStatus_52;

public:
	inline static int32_t get_offset_of_m_nFreshGuideStatus_4() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_nFreshGuideStatus_4)); }
	inline int32_t get_m_nFreshGuideStatus_4() const { return ___m_nFreshGuideStatus_4; }
	inline int32_t* get_address_of_m_nFreshGuideStatus_4() { return &___m_nFreshGuideStatus_4; }
	inline void set_m_nFreshGuideStatus_4(int32_t value)
	{
		___m_nFreshGuideStatus_4 = value;
	}

	inline static int32_t get_offset_of__vlg_5() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____vlg_5)); }
	inline VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 * get__vlg_5() const { return ____vlg_5; }
	inline VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 ** get_address_of__vlg_5() { return &____vlg_5; }
	inline void set__vlg_5(VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 * value)
	{
		____vlg_5 = value;
		Il2CppCodeGenWriteBarrier((&____vlg_5), value);
	}

	inline static int32_t get_offset_of__goFreeFlag_6() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____goFreeFlag_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goFreeFlag_6() const { return ____goFreeFlag_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goFreeFlag_6() { return &____goFreeFlag_6; }
	inline void set__goFreeFlag_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goFreeFlag_6 = value;
		Il2CppCodeGenWriteBarrier((&____goFreeFlag_6), value);
	}

	inline static int32_t get_offset_of__goBtnTanGeChe_7() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____goBtnTanGeChe_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goBtnTanGeChe_7() const { return ____goBtnTanGeChe_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goBtnTanGeChe_7() { return &____goBtnTanGeChe_7; }
	inline void set__goBtnTanGeChe_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goBtnTanGeChe_7 = value;
		Il2CppCodeGenWriteBarrier((&____goBtnTanGeChe_7), value);
	}

	inline static int32_t get_offset_of__imgPriceOff_8() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____imgPriceOff_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgPriceOff_8() const { return ____imgPriceOff_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgPriceOff_8() { return &____imgPriceOff_8; }
	inline void set__imgPriceOff_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgPriceOff_8 = value;
		Il2CppCodeGenWriteBarrier((&____imgPriceOff_8), value);
	}

	inline static int32_t get_offset_of_m_vecPriceOffStartPos_9() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_vecPriceOffStartPos_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecPriceOffStartPos_9() const { return ___m_vecPriceOffStartPos_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecPriceOffStartPos_9() { return &___m_vecPriceOffStartPos_9; }
	inline void set_m_vecPriceOffStartPos_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecPriceOffStartPos_9 = value;
	}

	inline static int32_t get_offset_of_m_vecPriceOffSummitPos_10() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_vecPriceOffSummitPos_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecPriceOffSummitPos_10() const { return ___m_vecPriceOffSummitPos_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecPriceOffSummitPos_10() { return &___m_vecPriceOffSummitPos_10; }
	inline void set_m_vecPriceOffSummitPos_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecPriceOffSummitPos_10 = value;
	}

	inline static int32_t get_offset_of_m_vecPriceOffEndPos_11() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_vecPriceOffEndPos_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecPriceOffEndPos_11() const { return ___m_vecPriceOffEndPos_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecPriceOffEndPos_11() { return &___m_vecPriceOffEndPos_11; }
	inline void set_m_vecPriceOffEndPos_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecPriceOffEndPos_11 = value;
	}

	inline static int32_t get_offset_of_m_fPriceOffAniTime_12() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_fPriceOffAniTime_12)); }
	inline float get_m_fPriceOffAniTime_12() const { return ___m_fPriceOffAniTime_12; }
	inline float* get_address_of_m_fPriceOffAniTime_12() { return &___m_fPriceOffAniTime_12; }
	inline void set_m_fPriceOffAniTime_12(float value)
	{
		___m_fPriceOffAniTime_12 = value;
	}

	inline static int32_t get_offset_of_m_vecPriceOffAniSpeed_13() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_vecPriceOffAniSpeed_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecPriceOffAniSpeed_13() const { return ___m_vecPriceOffAniSpeed_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecPriceOffAniSpeed_13() { return &___m_vecPriceOffAniSpeed_13; }
	inline void set_m_vecPriceOffAniSpeed_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecPriceOffAniSpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_fPriceOffAniA_14() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_fPriceOffAniA_14)); }
	inline float get_m_fPriceOffAniA_14() const { return ___m_fPriceOffAniA_14; }
	inline float* get_address_of_m_fPriceOffAniA_14() { return &___m_fPriceOffAniA_14; }
	inline void set_m_fPriceOffAniA_14(float value)
	{
		___m_fPriceOffAniA_14 = value;
	}

	inline static int32_t get_offset_of_m_fPriceOffAniV0_15() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_fPriceOffAniV0_15)); }
	inline float get_m_fPriceOffAniV0_15() const { return ___m_fPriceOffAniV0_15; }
	inline float* get_address_of_m_fPriceOffAniV0_15() { return &___m_fPriceOffAniV0_15; }
	inline void set_m_fPriceOffAniV0_15(float value)
	{
		___m_fPriceOffAniV0_15 = value;
	}

	inline static int32_t get_offset_of__txtRecommendPrice_19() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____txtRecommendPrice_19)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtRecommendPrice_19() const { return ____txtRecommendPrice_19; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtRecommendPrice_19() { return &____txtRecommendPrice_19; }
	inline void set__txtRecommendPrice_19(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtRecommendPrice_19 = value;
		Il2CppCodeGenWriteBarrier((&____txtRecommendPrice_19), value);
	}

	inline static int32_t get_offset_of__imgRecommendAvatar_20() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____imgRecommendAvatar_20)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgRecommendAvatar_20() const { return ____imgRecommendAvatar_20; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgRecommendAvatar_20() { return &____imgRecommendAvatar_20; }
	inline void set__imgRecommendAvatar_20(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgRecommendAvatar_20 = value;
		Il2CppCodeGenWriteBarrier((&____imgRecommendAvatar_20), value);
	}

	inline static int32_t get_offset_of__imgRecommendCoinIcon_21() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____imgRecommendCoinIcon_21)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgRecommendCoinIcon_21() const { return ____imgRecommendCoinIcon_21; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgRecommendCoinIcon_21() { return &____imgRecommendCoinIcon_21; }
	inline void set__imgRecommendCoinIcon_21(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgRecommendCoinIcon_21 = value;
		Il2CppCodeGenWriteBarrier((&____imgRecommendCoinIcon_21), value);
	}

	inline static int32_t get_offset_of__panelRecommendDetail_22() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____panelRecommendDetail_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelRecommendDetail_22() const { return ____panelRecommendDetail_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelRecommendDetail_22() { return &____panelRecommendDetail_22; }
	inline void set__panelRecommendDetail_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelRecommendDetail_22 = value;
		Il2CppCodeGenWriteBarrier((&____panelRecommendDetail_22), value);
	}

	inline static int32_t get_offset_of__txtRecommendDetail_23() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____txtRecommendDetail_23)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtRecommendDetail_23() const { return ____txtRecommendDetail_23; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtRecommendDetail_23() { return &____txtRecommendDetail_23; }
	inline void set__txtRecommendDetail_23(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtRecommendDetail_23 = value;
		Il2CppCodeGenWriteBarrier((&____txtRecommendDetail_23), value);
	}

	inline static int32_t get_offset_of__toggleNoPlane_24() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____toggleNoPlane_24)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get__toggleNoPlane_24() const { return ____toggleNoPlane_24; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of__toggleNoPlane_24() { return &____toggleNoPlane_24; }
	inline void set__toggleNoPlane_24(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		____toggleNoPlane_24 = value;
		Il2CppCodeGenWriteBarrier((&____toggleNoPlane_24), value);
	}

	inline static int32_t get_offset_of__toggleNoDrop_25() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____toggleNoDrop_25)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get__toggleNoDrop_25() const { return ____toggleNoDrop_25; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of__toggleNoDrop_25() { return &____toggleNoDrop_25; }
	inline void set__toggleNoDrop_25(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		____toggleNoDrop_25 = value;
		Il2CppCodeGenWriteBarrier((&____toggleNoDrop_25), value);
	}

	inline static int32_t get_offset_of__inputTrackLevel_26() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____inputTrackLevel_26)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__inputTrackLevel_26() const { return ____inputTrackLevel_26; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__inputTrackLevel_26() { return &____inputTrackLevel_26; }
	inline void set__inputTrackLevel_26(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____inputTrackLevel_26 = value;
		Il2CppCodeGenWriteBarrier((&____inputTrackLevel_26), value);
	}

	inline static int32_t get_offset_of__goCounterList_28() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____goCounterList_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goCounterList_28() const { return ____goCounterList_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goCounterList_28() { return &____goCounterList_28; }
	inline void set__goCounterList_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goCounterList_28 = value;
		Il2CppCodeGenWriteBarrier((&____goCounterList_28), value);
	}

	inline static int32_t get_offset_of_m_lstVehicleCounters_29() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_lstVehicleCounters_29)); }
	inline List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * get_m_lstVehicleCounters_29() const { return ___m_lstVehicleCounters_29; }
	inline List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C ** get_address_of_m_lstVehicleCounters_29() { return &___m_lstVehicleCounters_29; }
	inline void set_m_lstVehicleCounters_29(List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * value)
	{
		___m_lstVehicleCounters_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstVehicleCounters_29), value);
	}

	inline static int32_t get_offset_of_m_dicVehicleCounters_30() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_dicVehicleCounters_30)); }
	inline Dictionary_2_t23E80E3DB14B3FC8A2D8537F3FCAC9C8780F1654 * get_m_dicVehicleCounters_30() const { return ___m_dicVehicleCounters_30; }
	inline Dictionary_2_t23E80E3DB14B3FC8A2D8537F3FCAC9C8780F1654 ** get_address_of_m_dicVehicleCounters_30() { return &___m_dicVehicleCounters_30; }
	inline void set_m_dicVehicleCounters_30(Dictionary_2_t23E80E3DB14B3FC8A2D8537F3FCAC9C8780F1654 * value)
	{
		___m_dicVehicleCounters_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicVehicleCounters_30), value);
	}

	inline static int32_t get_offset_of__panelTanGeChe_31() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____panelTanGeChe_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelTanGeChe_31() const { return ____panelTanGeChe_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelTanGeChe_31() { return &____panelTanGeChe_31; }
	inline void set__panelTanGeChe_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelTanGeChe_31 = value;
		Il2CppCodeGenWriteBarrier((&____panelTanGeChe_31), value);
	}

	inline static int32_t get_offset_of_m_fRealTimeDiscount_32() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_fRealTimeDiscount_32)); }
	inline float get_m_fRealTimeDiscount_32() const { return ___m_fRealTimeDiscount_32; }
	inline float* get_address_of_m_fRealTimeDiscount_32() { return &___m_fRealTimeDiscount_32; }
	inline void set_m_fRealTimeDiscount_32(float value)
	{
		___m_fRealTimeDiscount_32 = value;
	}

	inline static int32_t get_offset_of_m_sprJianYingTemp_33() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_sprJianYingTemp_33)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprJianYingTemp_33() const { return ___m_sprJianYingTemp_33; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprJianYingTemp_33() { return &___m_sprJianYingTemp_33; }
	inline void set_m_sprJianYingTemp_33(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprJianYingTemp_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprJianYingTemp_33), value);
	}

	inline static int32_t get_offset_of_m_sprLockedBuyButton_34() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_sprLockedBuyButton_34)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprLockedBuyButton_34() const { return ___m_sprLockedBuyButton_34; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprLockedBuyButton_34() { return &___m_sprLockedBuyButton_34; }
	inline void set_m_sprLockedBuyButton_34(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprLockedBuyButton_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprLockedBuyButton_34), value);
	}

	inline static int32_t get_offset_of_m_spUnlockedBuyButton_35() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_spUnlockedBuyButton_35)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_spUnlockedBuyButton_35() const { return ___m_spUnlockedBuyButton_35; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_spUnlockedBuyButton_35() { return &___m_spUnlockedBuyButton_35; }
	inline void set_m_spUnlockedBuyButton_35(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_spUnlockedBuyButton_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_spUnlockedBuyButton_35), value);
	}

	inline static int32_t get_offset_of_m_colorUnlocked_36() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_colorUnlocked_36)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorUnlocked_36() const { return ___m_colorUnlocked_36; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorUnlocked_36() { return &___m_colorUnlocked_36; }
	inline void set_m_colorUnlocked_36(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorUnlocked_36 = value;
	}

	inline static int32_t get_offset_of_m_colorLocked_37() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_colorLocked_37)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorLocked_37() const { return ___m_colorLocked_37; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorLocked_37() { return &___m_colorLocked_37; }
	inline void set_m_colorLocked_37(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorLocked_37 = value;
	}

	inline static int32_t get_offset_of_m_nRecommendLowerAmount_38() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_nRecommendLowerAmount_38)); }
	inline int32_t get_m_nRecommendLowerAmount_38() const { return ___m_nRecommendLowerAmount_38; }
	inline int32_t* get_address_of_m_nRecommendLowerAmount_38() { return &___m_nRecommendLowerAmount_38; }
	inline void set_m_nRecommendLowerAmount_38(int32_t value)
	{
		___m_nRecommendLowerAmount_38 = value;
	}

	inline static int32_t get_offset_of_m_nRecommendHigherAmount_39() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_nRecommendHigherAmount_39)); }
	inline int32_t get_m_nRecommendHigherAmount_39() const { return ___m_nRecommendHigherAmount_39; }
	inline int32_t* get_address_of_m_nRecommendHigherAmount_39() { return &___m_nRecommendHigherAmount_39; }
	inline void set_m_nRecommendHigherAmount_39(int32_t value)
	{
		___m_nRecommendHigherAmount_39 = value;
	}

	inline static int32_t get_offset_of_lstTempShowingCounters_40() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___lstTempShowingCounters_40)); }
	inline List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * get_lstTempShowingCounters_40() const { return ___lstTempShowingCounters_40; }
	inline List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C ** get_address_of_lstTempShowingCounters_40() { return &___lstTempShowingCounters_40; }
	inline void set_lstTempShowingCounters_40(List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * value)
	{
		___lstTempShowingCounters_40 = value;
		Il2CppCodeGenWriteBarrier((&___lstTempShowingCounters_40), value);
	}

	inline static int32_t get_offset_of_lstTempShowingUnlockedCounters_41() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___lstTempShowingUnlockedCounters_41)); }
	inline List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * get_lstTempShowingUnlockedCounters_41() const { return ___lstTempShowingUnlockedCounters_41; }
	inline List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C ** get_address_of_lstTempShowingUnlockedCounters_41() { return &___lstTempShowingUnlockedCounters_41; }
	inline void set_lstTempShowingUnlockedCounters_41(List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * value)
	{
		___lstTempShowingUnlockedCounters_41 = value;
		Il2CppCodeGenWriteBarrier((&___lstTempShowingUnlockedCounters_41), value);
	}

	inline static int32_t get_offset_of_m_dicShareUICountersContainer_42() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_dicShareUICountersContainer_42)); }
	inline Dictionary_2_tC10E085FDFF6C3AB8005D16563E90234EF657A79 * get_m_dicShareUICountersContainer_42() const { return ___m_dicShareUICountersContainer_42; }
	inline Dictionary_2_tC10E085FDFF6C3AB8005D16563E90234EF657A79 ** get_address_of_m_dicShareUICountersContainer_42() { return &___m_dicShareUICountersContainer_42; }
	inline void set_m_dicShareUICountersContainer_42(Dictionary_2_tC10E085FDFF6C3AB8005D16563E90234EF657A79 * value)
	{
		___m_dicShareUICountersContainer_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicShareUICountersContainer_42), value);
	}

	inline static int32_t get_offset_of_m_nCurWatchAdsFreeCounterIndex_43() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_nCurWatchAdsFreeCounterIndex_43)); }
	inline int32_t get_m_nCurWatchAdsFreeCounterIndex_43() const { return ___m_nCurWatchAdsFreeCounterIndex_43; }
	inline int32_t* get_address_of_m_nCurWatchAdsFreeCounterIndex_43() { return &___m_nCurWatchAdsFreeCounterIndex_43; }
	inline void set_m_nCurWatchAdsFreeCounterIndex_43(int32_t value)
	{
		___m_nCurWatchAdsFreeCounterIndex_43 = value;
	}

	inline static int32_t get_offset_of_m_bIsVisible_44() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_bIsVisible_44)); }
	inline bool get_m_bIsVisible_44() const { return ___m_bIsVisible_44; }
	inline bool* get_address_of_m_bIsVisible_44() { return &___m_bIsVisible_44; }
	inline void set_m_bIsVisible_44(bool value)
	{
		___m_bIsVisible_44 = value;
	}

	inline static int32_t get_offset_of_m_fRandomEventInterval_45() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_fRandomEventInterval_45)); }
	inline float get_m_fRandomEventInterval_45() const { return ___m_fRandomEventInterval_45; }
	inline float* get_address_of_m_fRandomEventInterval_45() { return &___m_fRandomEventInterval_45; }
	inline void set_m_fRandomEventInterval_45(float value)
	{
		___m_fRandomEventInterval_45 = value;
	}

	inline static int32_t get_offset_of_m_fRandomEventTimeElapse_46() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_fRandomEventTimeElapse_46)); }
	inline float get_m_fRandomEventTimeElapse_46() const { return ___m_fRandomEventTimeElapse_46; }
	inline float* get_address_of_m_fRandomEventTimeElapse_46() { return &___m_fRandomEventTimeElapse_46; }
	inline void set_m_fRandomEventTimeElapse_46(float value)
	{
		___m_fRandomEventTimeElapse_46 = value;
	}

	inline static int32_t get_offset_of_m_bRandomEventProcessing_47() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_bRandomEventProcessing_47)); }
	inline bool get_m_bRandomEventProcessing_47() const { return ___m_bRandomEventProcessing_47; }
	inline bool* get_address_of_m_bRandomEventProcessing_47() { return &___m_bRandomEventProcessing_47; }
	inline void set_m_bRandomEventProcessing_47(bool value)
	{
		___m_bRandomEventProcessing_47 = value;
	}

	inline static int32_t get_offset_of_s_nRecommendCount_48() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___s_nRecommendCount_48)); }
	inline int32_t get_s_nRecommendCount_48() const { return ___s_nRecommendCount_48; }
	inline int32_t* get_address_of_s_nRecommendCount_48() { return &___s_nRecommendCount_48; }
	inline void set_s_nRecommendCount_48(int32_t value)
	{
		___s_nRecommendCount_48 = value;
	}

	inline static int32_t get_offset_of_selected_one_49() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___selected_one_49)); }
	inline sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA  get_selected_one_49() const { return ___selected_one_49; }
	inline sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA * get_address_of_selected_one_49() { return &___selected_one_49; }
	inline void set_selected_one_49(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA  value)
	{
		___selected_one_49 = value;
	}

	inline static int32_t get_offset_of_m_bNoPlane_50() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_bNoPlane_50)); }
	inline bool get_m_bNoPlane_50() const { return ___m_bNoPlane_50; }
	inline bool* get_address_of_m_bNoPlane_50() { return &___m_bNoPlane_50; }
	inline void set_m_bNoPlane_50(bool value)
	{
		___m_bNoPlane_50 = value;
	}

	inline static int32_t get_offset_of_m_bNoDrop_51() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_bNoDrop_51)); }
	inline bool get_m_bNoDrop_51() const { return ___m_bNoDrop_51; }
	inline bool* get_address_of_m_bNoDrop_51() { return &___m_bNoDrop_51; }
	inline void set_m_bNoDrop_51(bool value)
	{
		___m_bNoDrop_51 = value;
	}

	inline static int32_t get_offset_of_m_nPriceOffAnimationStatus_52() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_nPriceOffAnimationStatus_52)); }
	inline int32_t get_m_nPriceOffAnimationStatus_52() const { return ___m_nPriceOffAnimationStatus_52; }
	inline int32_t* get_address_of_m_nPriceOffAnimationStatus_52() { return &___m_nPriceOffAnimationStatus_52; }
	inline void set_m_nPriceOffAnimationStatus_52(int32_t value)
	{
		___m_nPriceOffAnimationStatus_52 = value;
	}
};

struct TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields
{
public:
	// UnityEngine.Vector3 TanGeChe::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_16;
	// UnityEngine.Vector3 TanGeChe::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_17;
	// UnityEngine.Color TanGeChe::colorTemp
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___colorTemp_18;
	// TanGeChe TanGeChe::s_Instance
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455 * ___s_Instance_27;

public:
	inline static int32_t get_offset_of_vecTempScale_16() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields, ___vecTempScale_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_16() const { return ___vecTempScale_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_16() { return &___vecTempScale_16; }
	inline void set_vecTempScale_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_16 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_17() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields, ___vecTempPos_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_17() const { return ___vecTempPos_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_17() { return &___vecTempPos_17; }
	inline void set_vecTempPos_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_17 = value;
	}

	inline static int32_t get_offset_of_colorTemp_18() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields, ___colorTemp_18)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_colorTemp_18() const { return ___colorTemp_18; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_colorTemp_18() { return &___colorTemp_18; }
	inline void set_colorTemp_18(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___colorTemp_18 = value;
	}

	inline static int32_t get_offset_of_s_Instance_27() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields, ___s_Instance_27)); }
	inline TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455 * get_s_Instance_27() const { return ___s_Instance_27; }
	inline TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455 ** get_address_of_s_Instance_27() { return &___s_Instance_27; }
	inline void set_s_Instance_27(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455 * value)
	{
		___s_Instance_27 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TANGECHE_T8AB5138CD649482ABE434C2571453BE2A5AF0455_H
#ifndef TIAOZIJINBI_T09250279C8D7D5B011E9ACCE6111D739FD130648_H
#define TIAOZIJINBI_T09250279C8D7D5B011E9ACCE6111D739FD130648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiaoZiJinBi
struct  TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.SpriteRenderer TiaoZiJinBi::_srMain
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srMain_5;
	// System.Single TiaoZiJinBi::m_fT
	float ___m_fT_6;
	// System.Single TiaoZiJinBi::m_fSX
	float ___m_fSX_7;
	// System.Single TiaoZiJinBi::m_fSY
	float ___m_fSY_8;
	// System.Single TiaoZiJinBi::m_fV0X
	float ___m_fV0X_9;
	// System.Single TiaoZiJinBi::m_fV0Y
	float ___m_fV0Y_10;
	// System.Single TiaoZiJinBi::m_fA
	float ___m_fA_11;
	// System.Int32 TiaoZiJinBi::m_nStatus
	int32_t ___m_nStatus_12;
	// System.Single TiaoZiJinBi::m_fDir
	float ___m_fDir_13;

public:
	inline static int32_t get_offset_of__srMain_5() { return static_cast<int32_t>(offsetof(TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648, ____srMain_5)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srMain_5() const { return ____srMain_5; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srMain_5() { return &____srMain_5; }
	inline void set__srMain_5(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srMain_5 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_5), value);
	}

	inline static int32_t get_offset_of_m_fT_6() { return static_cast<int32_t>(offsetof(TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648, ___m_fT_6)); }
	inline float get_m_fT_6() const { return ___m_fT_6; }
	inline float* get_address_of_m_fT_6() { return &___m_fT_6; }
	inline void set_m_fT_6(float value)
	{
		___m_fT_6 = value;
	}

	inline static int32_t get_offset_of_m_fSX_7() { return static_cast<int32_t>(offsetof(TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648, ___m_fSX_7)); }
	inline float get_m_fSX_7() const { return ___m_fSX_7; }
	inline float* get_address_of_m_fSX_7() { return &___m_fSX_7; }
	inline void set_m_fSX_7(float value)
	{
		___m_fSX_7 = value;
	}

	inline static int32_t get_offset_of_m_fSY_8() { return static_cast<int32_t>(offsetof(TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648, ___m_fSY_8)); }
	inline float get_m_fSY_8() const { return ___m_fSY_8; }
	inline float* get_address_of_m_fSY_8() { return &___m_fSY_8; }
	inline void set_m_fSY_8(float value)
	{
		___m_fSY_8 = value;
	}

	inline static int32_t get_offset_of_m_fV0X_9() { return static_cast<int32_t>(offsetof(TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648, ___m_fV0X_9)); }
	inline float get_m_fV0X_9() const { return ___m_fV0X_9; }
	inline float* get_address_of_m_fV0X_9() { return &___m_fV0X_9; }
	inline void set_m_fV0X_9(float value)
	{
		___m_fV0X_9 = value;
	}

	inline static int32_t get_offset_of_m_fV0Y_10() { return static_cast<int32_t>(offsetof(TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648, ___m_fV0Y_10)); }
	inline float get_m_fV0Y_10() const { return ___m_fV0Y_10; }
	inline float* get_address_of_m_fV0Y_10() { return &___m_fV0Y_10; }
	inline void set_m_fV0Y_10(float value)
	{
		___m_fV0Y_10 = value;
	}

	inline static int32_t get_offset_of_m_fA_11() { return static_cast<int32_t>(offsetof(TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648, ___m_fA_11)); }
	inline float get_m_fA_11() const { return ___m_fA_11; }
	inline float* get_address_of_m_fA_11() { return &___m_fA_11; }
	inline void set_m_fA_11(float value)
	{
		___m_fA_11 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_12() { return static_cast<int32_t>(offsetof(TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648, ___m_nStatus_12)); }
	inline int32_t get_m_nStatus_12() const { return ___m_nStatus_12; }
	inline int32_t* get_address_of_m_nStatus_12() { return &___m_nStatus_12; }
	inline void set_m_nStatus_12(int32_t value)
	{
		___m_nStatus_12 = value;
	}

	inline static int32_t get_offset_of_m_fDir_13() { return static_cast<int32_t>(offsetof(TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648, ___m_fDir_13)); }
	inline float get_m_fDir_13() const { return ___m_fDir_13; }
	inline float* get_address_of_m_fDir_13() { return &___m_fDir_13; }
	inline void set_m_fDir_13(float value)
	{
		___m_fDir_13 = value;
	}
};

struct TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648_StaticFields
{
public:
	// UnityEngine.Vector3 TiaoZiJinBi::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_4;

public:
	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648_StaticFields, ___vecTempPos_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIAOZIJINBI_T09250279C8D7D5B011E9ACCE6111D739FD130648_H
#ifndef TRANSFORMMANAGER_T92CE50837CEEDBB3E6495C8BDF8D4C24CF78EBFB_H
#define TRANSFORMMANAGER_T92CE50837CEEDBB3E6495C8BDF8D4C24CF78EBFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransformManager
struct  TransformManager_t92CE50837CEEDBB3E6495C8BDF8D4C24CF78EBFB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMMANAGER_T92CE50837CEEDBB3E6495C8BDF8D4C24CF78EBFB_H
#ifndef TREASUREBOXMANAGER_T23242BEA18FC83D440BB4AFECDF9F40B5209D14A_H
#define TREASUREBOXMANAGER_T23242BEA18FC83D440BB4AFECDF9F40B5209D14A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreasureBoxManager
struct  TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject TreasureBoxManager::_panelMain
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelMain_5;
	// UnityEngine.UI.Text TreasureBoxManager::_txtOpenResult
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtOpenResult_6;
	// System.Collections.Generic.List`1<System.Int32> TreasureBoxManager::m_lstId
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_lstId_7;

public:
	inline static int32_t get_offset_of__panelMain_5() { return static_cast<int32_t>(offsetof(TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A, ____panelMain_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelMain_5() const { return ____panelMain_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelMain_5() { return &____panelMain_5; }
	inline void set__panelMain_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelMain_5 = value;
		Il2CppCodeGenWriteBarrier((&____panelMain_5), value);
	}

	inline static int32_t get_offset_of__txtOpenResult_6() { return static_cast<int32_t>(offsetof(TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A, ____txtOpenResult_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtOpenResult_6() const { return ____txtOpenResult_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtOpenResult_6() { return &____txtOpenResult_6; }
	inline void set__txtOpenResult_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtOpenResult_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtOpenResult_6), value);
	}

	inline static int32_t get_offset_of_m_lstId_7() { return static_cast<int32_t>(offsetof(TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A, ___m_lstId_7)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_lstId_7() const { return ___m_lstId_7; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_lstId_7() { return &___m_lstId_7; }
	inline void set_m_lstId_7(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_lstId_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstId_7), value);
	}
};

struct TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A_StaticFields
{
public:
	// TreasureBoxManager TreasureBoxManager::s_Instance
	TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A_StaticFields, ___s_Instance_4)); }
	inline TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A * get_s_Instance_4() const { return ___s_Instance_4; }
	inline TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TREASUREBOXMANAGER_T23242BEA18FC83D440BB4AFECDF9F40B5209D14A_H
#ifndef UIBLOCK_T34D109BECBD5E8B392500CB078B388ADB954326B_H
#define UIBLOCK_T34D109BECBD5E8B392500CB078B388ADB954326B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBlock
struct  UIBlock_t34D109BECBD5E8B392500CB078B388ADB954326B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image UIBlock::_imgMain
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgMain_4;
	// UnityEngine.UI.Outline UIBlock::_outLine
	Outline_tB750E496976B072E79142D51C0A991AC20183095 * ____outLine_5;
	// UnityEngine.UI.Image UIBlock::_imgBg
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgBg_6;

public:
	inline static int32_t get_offset_of__imgMain_4() { return static_cast<int32_t>(offsetof(UIBlock_t34D109BECBD5E8B392500CB078B388ADB954326B, ____imgMain_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgMain_4() const { return ____imgMain_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgMain_4() { return &____imgMain_4; }
	inline void set__imgMain_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgMain_4 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_4), value);
	}

	inline static int32_t get_offset_of__outLine_5() { return static_cast<int32_t>(offsetof(UIBlock_t34D109BECBD5E8B392500CB078B388ADB954326B, ____outLine_5)); }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 * get__outLine_5() const { return ____outLine_5; }
	inline Outline_tB750E496976B072E79142D51C0A991AC20183095 ** get_address_of__outLine_5() { return &____outLine_5; }
	inline void set__outLine_5(Outline_tB750E496976B072E79142D51C0A991AC20183095 * value)
	{
		____outLine_5 = value;
		Il2CppCodeGenWriteBarrier((&____outLine_5), value);
	}

	inline static int32_t get_offset_of__imgBg_6() { return static_cast<int32_t>(offsetof(UIBlock_t34D109BECBD5E8B392500CB078B388ADB954326B, ____imgBg_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgBg_6() const { return ____imgBg_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgBg_6() { return &____imgBg_6; }
	inline void set__imgBg_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgBg_6 = value;
		Il2CppCodeGenWriteBarrier((&____imgBg_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBLOCK_T34D109BECBD5E8B392500CB078B388ADB954326B_H
#ifndef UIRESEARCHCOUNTERCONTAINER_TCD786B78332DEDA681A84956DC136BCFE55100F3_H
#define UIRESEARCHCOUNTERCONTAINER_TCD786B78332DEDA681A84956DC136BCFE55100F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIResearchCounterContainer
struct  UIResearchCounterContainer_tCD786B78332DEDA681A84956DC136BCFE55100F3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text UIResearchCounterContainer::_txtIntro
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtIntro_4;
	// UnityEngine.UI.Image UIResearchCounterContainer::_imgBg
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgBg_5;
	// ResearchCounter UIResearchCounterContainer::m_BoundResearchCounter
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * ___m_BoundResearchCounter_6;

public:
	inline static int32_t get_offset_of__txtIntro_4() { return static_cast<int32_t>(offsetof(UIResearchCounterContainer_tCD786B78332DEDA681A84956DC136BCFE55100F3, ____txtIntro_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtIntro_4() const { return ____txtIntro_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtIntro_4() { return &____txtIntro_4; }
	inline void set__txtIntro_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtIntro_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtIntro_4), value);
	}

	inline static int32_t get_offset_of__imgBg_5() { return static_cast<int32_t>(offsetof(UIResearchCounterContainer_tCD786B78332DEDA681A84956DC136BCFE55100F3, ____imgBg_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgBg_5() const { return ____imgBg_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgBg_5() { return &____imgBg_5; }
	inline void set__imgBg_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgBg_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgBg_5), value);
	}

	inline static int32_t get_offset_of_m_BoundResearchCounter_6() { return static_cast<int32_t>(offsetof(UIResearchCounterContainer_tCD786B78332DEDA681A84956DC136BCFE55100F3, ___m_BoundResearchCounter_6)); }
	inline ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * get_m_BoundResearchCounter_6() const { return ___m_BoundResearchCounter_6; }
	inline ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD ** get_address_of_m_BoundResearchCounter_6() { return &___m_BoundResearchCounter_6; }
	inline void set_m_BoundResearchCounter_6(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * value)
	{
		___m_BoundResearchCounter_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundResearchCounter_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIRESEARCHCOUNTERCONTAINER_TCD786B78332DEDA681A84956DC136BCFE55100F3_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (eAdventureProfitType_tD199C31799F1616902B9916C0195AE7BBE3D60CC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2700[3] = 
{
	eAdventureProfitType_tD199C31799F1616902B9916C0195AE7BBE3D60CC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47), -1, sizeof(AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2701[15] = 
{
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47_StaticFields::get_offset_of_s_Instance_4(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47_StaticFields::get_offset_of_vecTempPos_5(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47::get_offset_of_m_vecLeft_6(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47::get_offset_of_m_vecLeftTop_7(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47::get_offset_of_m_vecTop_8(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47::get_offset_of_m_vecRightTop_9(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47::get_offset_of_m_vecRight_10(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47::get_offset_of_m_vecRightBottom_11(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47::get_offset_of_m_vecBottom_12(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47::get_offset_of_m_vecLeftBottom_13(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47::get_offset_of_m_vecCenterLeftTop_14(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47::get_offset_of_m_vecCenterRightTop_15(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47::get_offset_of_m_vecCenterRightBottom_16(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47::get_offset_of_m_vecCenterLeftBottom_17(),
	AirlineManager_tB3B70C62CE6925B6930EB9E670E3AD17BF575F47::get_offset_of_m_aryKeyFramePoints_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23), -1, sizeof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2702[10] = 
{
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields::get_offset_of_s_Instance_4(),
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23::get_offset_of_m_aryBGM_5(),
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23::get_offset_of_m_arySE_6(),
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23::get_offset_of_m_arySE_New_7(),
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23::get_offset_of__audioDefaultClickButton_8(),
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23::get_offset_of_m_aryAuidoPrefabs_9(),
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23::get_offset_of__BMG_10(),
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23::get_offset_of_m_dicPlayingSE_11(),
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23::get_offset_of_m_bBGM_12(),
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23::get_offset_of_m_nMusicIndex_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (eSe_New_tED4E1E0AAC6B985EE81D0661351734AC7DFFDFC2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2703[30] = 
{
	eSe_New_tED4E1E0AAC6B985EE81D0661351734AC7DFFDFC2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (eSE_tEBFC5E9CC8CA17804BEE458B4DAE21100B2641FD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2704[11] = 
{
	eSE_tEBFC5E9CC8CA17804BEE458B4DAE21100B2641FD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE), -1, sizeof(BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2705[8] = 
{
	BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE_StaticFields::get_offset_of_s_Instance_4(),
	BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE::get_offset_of_m_aryBoxSpr_5(),
	BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE_StaticFields::get_offset_of_vecTempPos_6(),
	BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE::get_offset_of_m_fGenerateBoxInterval_7(),
	BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE::get_offset_of_m_fTimeElapse_8(),
	BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE::get_offset_of_m_fDropStartPosY_9(),
	BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE::get_offset_of_m_fDropSpeed_10(),
	BoxManager_t36F4387CB5FD8C838CAA9FF8DE089EF6BA11A4FE::get_offset_of_m_fDropTime_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (eBoxType_tC389FEFF432D0270A9F0BF3093CC5603026BE301)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2706[4] = 
{
	eBoxType_tC389FEFF432D0270A9F0BF3093CC5603026BE301::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B), -1, sizeof(BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2707[5] = 
{
	BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B_StaticFields::get_offset_of_s_Instance_4(),
	BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B_StaticFields::get_offset_of_vecTempPos_5(),
	BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B::get_offset_of_m_aryBuffCounter_6(),
	BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B::get_offset_of_m_fGap_7(),
	BuffManager_t085DFCE00CD02411A9D36236C7A685E150395C5B::get_offset_of_m_fTimeElapse_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (eBuffType_t78DAFF47F067B735F9426A12B7585E2FCE124F8E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2708[4] = 
{
	eBuffType_t78DAFF47F067B735F9426A12B7585E2FCE124F8E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (BaseRotate_t66CD927C20FD5B40C08A788B6578D220A0B11377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[3] = 
{
	BaseRotate_t66CD927C20FD5B40C08A788B6578D220A0B11377::get_offset_of_m_bRotating_4(),
	BaseRotate_t66CD927C20FD5B40C08A788B6578D220A0B11377::get_offset_of_m_fRotateSpeed_5(),
	BaseRotate_t66CD927C20FD5B40C08A788B6578D220A0B11377::get_offset_of_m_fCurAngle_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2710[20] = 
{
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_eventScaleEnd_4(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_vecTempScale_5(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_bAutoaPlay_6(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_bLoop_7(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of__bHideWhenEnd_8(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_fScaleTime_9(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_fWaitTime_10(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_fMaxScale_11(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_nStatus_12(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_fSpeed_13(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_fRotateSpeed_14(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_fWaitTimeElapse_15(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_fInitScale_16(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_fDestScale_17(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of__bWithRotate_18(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of__fMaxRotateAngle_19(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_fAngle_20(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of__goShadow_21(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_bShadowBegin_22(),
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063::get_offset_of_m_eAxis_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (delegateScaleEnd_t697D8B0D83495ED3E1853627A6FCACD68409376F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (eScaleAxis_t2049C60FBA33DD8A1C5CAC79ECF499AF981117A2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2712[4] = 
{
	eScaleAxis_t2049C60FBA33DD8A1C5CAC79ECF499AF981117A2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[9] = 
{
	BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A::get_offset_of_m_nTotalTimes_4(),
	BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A::get_offset_of_m_fShakeInterval_5(),
	BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A::get_offset_of_m_fShakeAngle_6(),
	BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A::get_offset_of_m_nStatus_7(),
	BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A::get_offset_of_m_nCount_8(),
	BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A::get_offset_of_m_fTimeElapse_9(),
	BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A::get_offset_of_m_bAutoPlay_10(),
	BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A::get_offset_of_m_bLoop_11(),
	BaseShake_tC67FA3B616E91A9019CAF1C6FD15EFEFE863930A::get_offset_of_m_fLoopWaitTime_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[19] = 
{
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_aryFrameSprites_4(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of__imgMain_5(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of__sprMain_6(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_bUI_7(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_fFrameInterval_8(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_fTimeCount_9(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_bPlaying_10(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_nFrameIndex_11(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_bLoop_12(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_bReverse_13(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_bCurDir_14(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_fLoopInterval_15(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_bWaiting_16(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_fWaitingTimeCount_17(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_bPlayAuto_18(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_nLoopTimes_19(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_bHideWhenEnd_20(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_bDestroyWhenEnd_21(),
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10::get_offset_of_m_nId_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496), -1, sizeof(EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2715[4] = 
{
	EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496_StaticFields::get_offset_of_s_Instance_4(),
	EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496::get_offset_of_m_aryEffectPrefabs_5(),
	EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496::get_offset_of_m_lstMergeAnimation_6(),
	EffectManager_t6FBC4ED79E1183ACFFB423CF287C653C14B32496::get_offset_of_m_nMergeEffectIndex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (eEffectType_t37AF40DF21D81FB2776F142F88D342C51372F583)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2716[12] = 
{
	eEffectType_t37AF40DF21D81FB2776F142F88D342C51372F583::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[4] = 
{
	EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73::get_offset_of__sm_4(),
	EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73::get_offset_of__srEnviromnet_5(),
	EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73::get_offset_of__colorBlack_6(),
	EnviromentMask_t88DCCEE6DC654DA382F8FF1819D2E7DE019B4E73::get_offset_of__colorEnviroment_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (JetTrail_tCBAAF5CD558CAC61ED2859A36CF54E8786F37A46), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (TransformManager_t92CE50837CEEDBB3E6495C8BDF8D4C24CF78EBFB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166), -1, sizeof(Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2720[25] = 
{
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166_StaticFields::get_offset_of_s_Instance_4(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__toggleShowDebugInfo_5(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__txtUIAnchorIndex_6(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__txtSceneAnchorIndex_7(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of_m_aryUiAnchor_8(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of_m_arySceneAnchor_9(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__txtDebugInfo_10(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__sprAirline_11(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__dropPlanetId_12(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__dropDistrictId_13(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__inputCoin0_14(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__inputCoin1_15(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__inputCoin2_16(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__inputGreenCash_17(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__inputSkillPoint0_18(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__inputSkillPoint1_19(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__inputSkillPoint2_20(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__sliderVibrateAmount_21(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of__txtVibrateAmount_22(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of_uiCheatPanel_23(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of_m_goCurShit_24(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166_StaticFields::get_offset_of_vec3Temp_25(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of_m_fShitSpeed_26(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of_m_lVibrateAmount_27(),
	Cheat_t61A56D9B4C9BF342A86C628A46DB0F0BB704E166::get_offset_of_m_bShowDebugInfo_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (CyberTreeMath_t79DA1AC9D544A93B29F77D7E04C438A5E5A98305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30), -1, sizeof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2722[30] = 
{
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields::get_offset_of_s_Instance_4(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_bOnlineConfig_5(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields::get_offset_of_url_6(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields::get_offset_of_url_formal_7(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields::get_offset_of_url_test_8(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields::get_offset_of_url_offline_9(),
	0,
	0,
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_MAX_PLANET_NUM_12(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_MAX_SKILL_POINT_NUM_13(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_MAX_TRACK_NUM_OF_PLANET_14(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_dicCoinGainPerRound_15(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_dicCoinVehiclePrice_16(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_aryRoundTimeByLevel_17(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_dicCoinCostToPrestige_18(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_dicAutomobileConfig_19(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_dicPlanetConfig_20(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_dicTrackConfig_21(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_dicTrackAndLevel2ResId_22(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_tempAutomobileConfig_23(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_tempPlanetConfig_24(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_tempTrackConfig_25(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_tempPrestigeConfig_26(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_dicPrestigeConfig_27(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_bPlanetConfigLoaded_28(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_bAutomobileConfigLoaded_29(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_bTrackConfigLoaded_30(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_dicLoadMyData_String_31(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_dicLoadMyData_32(),
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30::get_offset_of_m_bCanSave_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (eMoneyType_t9A22FBEEECC2803A11A7756315481457B389F4C1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2723[6] = 
{
	eMoneyType_t9A22FBEEECC2803A11A7756315481457B389F4C1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20)+ sizeof (RuntimeObject), sizeof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2724[14] = 
{
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_nId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_nLevel_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_nResId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_szName_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_eCoinType_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_nStartPrice_CoinValue_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_fPriceRaisePercentPerBuy_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_nPriceDiamond_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_nCanUnlockLevel_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_szSuitableTrackId_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_nBaseGain_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_fBaseSpeed_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_nDropLevel_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20::get_offset_of_nDropInterval_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9)+ sizeof (RuntimeObject), sizeof(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2725[6] = 
{
	sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9::get_offset_of_szName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9::get_offset_of_nId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9::get_offset_of_nUnlockPrice_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9::get_offset_of_eUnlockMoneyType_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9::get_offset_of_dInitCoin_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9::get_offset_of_dInitSkillPoint_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B)+ sizeof (RuntimeObject), sizeof(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2726[6] = 
{
	sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B::get_offset_of_nId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B::get_offset_of_szName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B::get_offset_of_nPlanetId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B::get_offset_of_nUnlockPrice_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B::get_offset_of_fEarning_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B::get_offset_of_nBuyAdminBasePrice_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[2] = 
{
	sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63::get_offset_of_aryCoinCost_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63::get_offset_of_aryCoinPromote_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (U3CLoadConfig_PlanetU3Ed__35_tC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[5] = 
{
	U3CLoadConfig_PlanetU3Ed__35_tC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133::get_offset_of_U3CU3E1__state_0(),
	U3CLoadConfig_PlanetU3Ed__35_tC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133::get_offset_of_U3CU3E2__current_1(),
	U3CLoadConfig_PlanetU3Ed__35_tC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133::get_offset_of_szFileName_2(),
	U3CLoadConfig_PlanetU3Ed__35_tC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133::get_offset_of_U3CU3E4__this_3(),
	U3CLoadConfig_PlanetU3Ed__35_tC2DA6ABB59AB4C1614F36AE2E2D39D64C8CDC133::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (U3CLoadConfig_TrackU3Ed__37_tF50D32EEA406D9E38804EEC52B82308D941DCA68), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[5] = 
{
	U3CLoadConfig_TrackU3Ed__37_tF50D32EEA406D9E38804EEC52B82308D941DCA68::get_offset_of_U3CU3E1__state_0(),
	U3CLoadConfig_TrackU3Ed__37_tF50D32EEA406D9E38804EEC52B82308D941DCA68::get_offset_of_U3CU3E2__current_1(),
	U3CLoadConfig_TrackU3Ed__37_tF50D32EEA406D9E38804EEC52B82308D941DCA68::get_offset_of_szFileName_2(),
	U3CLoadConfig_TrackU3Ed__37_tF50D32EEA406D9E38804EEC52B82308D941DCA68::get_offset_of_U3CU3E4__this_3(),
	U3CLoadConfig_TrackU3Ed__37_tF50D32EEA406D9E38804EEC52B82308D941DCA68::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (U3CLoadConfig_AutomobileU3Ed__42_t38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[5] = 
{
	U3CLoadConfig_AutomobileU3Ed__42_t38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0::get_offset_of_U3CU3E1__state_0(),
	U3CLoadConfig_AutomobileU3Ed__42_t38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0::get_offset_of_U3CU3E2__current_1(),
	U3CLoadConfig_AutomobileU3Ed__42_t38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0::get_offset_of_szFileName_2(),
	U3CLoadConfig_AutomobileU3Ed__42_t38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0::get_offset_of_U3CU3E4__this_3(),
	U3CLoadConfig_AutomobileU3Ed__42_t38A0643122DC7A1B76BA2DA2AA196C1C9C3123E0::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (DebugInfo_t9A4A67660130ACFB573B3479EA5C46A8E739BC56), -1, sizeof(DebugInfo_t9A4A67660130ACFB573B3479EA5C46A8E739BC56_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2731[2] = 
{
	DebugInfo_t9A4A67660130ACFB573B3479EA5C46A8E739BC56_StaticFields::get_offset_of_s_Instance_4(),
	DebugInfo_t9A4A67660130ACFB573B3479EA5C46A8E739BC56::get_offset_of__txtPlanetAndDistrictId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[42] = 
{
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_dtLastWatchAdsVehicleFreeTime_4(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_BoundPlanet_5(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_eStatus_6(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_nId_7(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_nUnlockPrice_8(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_szData_9(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_nPrestigeTimes_10(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_aryVehicleBuyTimes_11(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_dicVehicleBuyTimes_12(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_nAdsRaiseTime_13(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_dicSkill_14(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_config_15(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_nAdsBaseTime_16(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_lstRunningPlanes_17(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_nLevel_18(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_nBuyAdminTimes_19(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_CurUsingAdmin_20(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_lstAdmins_21(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_ResearchCounter_22(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_nDropLevel_23(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_nDropInterval_24(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_nMaxCoinBuyLevel_25(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_nCurLotNum_26(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_dtAdsStartTime_27(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_nAdsLeftTime_28(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_fStartOfflineTime_29(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_fLastOnlineTime_30(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_fOfflineGainPerSecondBase_31(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_fOfflineGainPerSecondReal_32(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_bOffline_33(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_fDPS_34(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_fSpeedAccelerateRate_35(),
	0,
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_fTimeElapseOffline_37(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_fCurTotalOfflineGain_38(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_lstLevelAndCost_39(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_TheSelectedOne_40(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_dicEnergyResearchCounters_41(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_fTrackCoinRaise_42(),
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_fSpeedAccelerate_43(),
	0,
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A::get_offset_of_m_aryAccelerate_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA)+ sizeof (RuntimeObject), sizeof(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA ), 0, 0 };
extern const int32_t g_FieldOffsetTable2733[5] = 
{
	sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA::get_offset_of_nLevel_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA::get_offset_of_dCurPrice_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA::get_offset_of_dCost_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA::get_offset_of_nCurBuyTimes_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA::get_offset_of_nToBuyTime_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (dou_t6892A900BE5AC1C3D8B4DFBA12DC4445953E1EFD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (EconomySystem_t6205DAAC929785220BA01D0AF128C856AE09F0B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[2] = 
{
	EconomySystem_t6205DAAC929785220BA01D0AF128C856AE09F0B1::get_offset_of_m_aryCoin_4(),
	EconomySystem_t6205DAAC929785220BA01D0AF128C856AE09F0B1::get_offset_of_m_fGreenCash_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3), -1, sizeof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2736[33] = 
{
	0,
	0,
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__containerScene_6(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__goMask_7(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_bMaskVisible_8(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__txtX_9(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__txtY_10(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__txtRadius_11(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__txtPawX_12(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__txtPawY_13(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_matMask_14(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_fX_15(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_fY_16(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_fRadius_17(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__uiRecommend_18(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__lot0_19(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__lot1_20(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__lot2_21(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__lot3_22(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields::get_offset_of_vecTempPos_23(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields::get_offset_of_vecTempScale_24(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields::get_offset_of_s_Instance_25(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_dicDone_26(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__paw_27(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of__faClick_28(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_eCurType_29(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_nStatus_30(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_nSubStatus_31(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_aryIntParams_32(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_aryFloatParams_33(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_dicFloatParams_34(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_fTimeElapse_35(),
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3::get_offset_of_m_bMasking_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (eGuideType_t90774CD9942800464AB6E6A8D72E0F77452E0288)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2737[3] = 
{
	eGuideType_t90774CD9942800464AB6E6A8D72E0F77452E0288::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (GetDropInterval_tECA142D4B048205911AC241AF307139A969168E5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (Gyro_t3FD88CAC3210A8FEDD89E4D27329A678A2254BF3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[3] = 
{
	0,
	Gyro_t3FD88CAC3210A8FEDD89E4D27329A678A2254BF3::get_offset_of_go_5(),
	Gyro_t3FD88CAC3210A8FEDD89E4D27329A678A2254BF3::get_offset_of_gyinfo_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (Item_t1FD274D610B07F7F6E6AA093A93FB9E32210D36E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (MonthCard_tF6C171DC32E6B01734A000C891B88DE0FA09CA6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677), -1, sizeof(ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2742[24] = 
{
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677_StaticFields::get_offset_of_vecTempScale_4(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_aryItemIconSprite_Cash_5(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_aryItemIconSprite_CoinPromote_6(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_aryItemIconSprite_SpeedAccelerate_7(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_aryItemIconSprite_SkillPoint_8(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_dicItemIconSpritesList_9(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of__theBag_10(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_aryItemIcon_11(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_aryShoppinCounter_12(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677_StaticFields::get_offset_of_s_Instance_13(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of__containerRecycledItems_14(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of__panelItemBag_15(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of__containerUsing_16(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of__containerNotUsing_17(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_lstUsingItems_18(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_lstNowUsingItems_19(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_tempItemConfig_20(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_dicItemConfig_21(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_bItemConfigLoaded_22(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_fTimeElapse_23(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_bIsVisible_24(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_fAutomobileSpeedAccelerate_25(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_m_lstUsingItems_New_26(),
	ItemSystem_tBFA2A8170ACDDEB8C5F9B9F059761900F5668677::get_offset_of_lstTemp_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C)+ sizeof (RuntimeObject), sizeof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2743[9] = 
{
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C::get_offset_of_nId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C::get_offset_of_szName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C::get_offset_of_nType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C::get_offset_of_nResId_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C::get_offset_of_szParams_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C::get_offset_of_nDuration_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C::get_offset_of_szDesc_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C::get_offset_of_aryIntParams_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C::get_offset_of_aryFloatParams_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (U3CLoadConfig_ItemU3Ed__25_t2925A912974D437EC95D48E799EC131D0C0BDF9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[5] = 
{
	U3CLoadConfig_ItemU3Ed__25_t2925A912974D437EC95D48E799EC131D0C0BDF9D::get_offset_of_U3CU3E1__state_0(),
	U3CLoadConfig_ItemU3Ed__25_t2925A912974D437EC95D48E799EC131D0C0BDF9D::get_offset_of_U3CU3E2__current_1(),
	U3CLoadConfig_ItemU3Ed__25_t2925A912974D437EC95D48E799EC131D0C0BDF9D::get_offset_of_szFileName_2(),
	U3CLoadConfig_ItemU3Ed__25_t2925A912974D437EC95D48E799EC131D0C0BDF9D::get_offset_of_U3CU3E4__this_3(),
	U3CLoadConfig_ItemU3Ed__25_t2925A912974D437EC95D48E799EC131D0C0BDF9D::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (Lot_t51B999792800E5A264A2CA51A4C521311958F874), -1, sizeof(Lot_t51B999792800E5A264A2CA51A4C521311958F874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2745[16] = 
{
	Lot_t51B999792800E5A264A2CA51A4C521311958F874_StaticFields::get_offset_of_vecTempPos_4(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874_StaticFields::get_offset_of_vecTempScale_5(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of_m_goMain_6(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of__srPlusSign_7(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of_m_bShowingPlusSign_8(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of__basescalePeng_9(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of_m_Plane_10(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of_m_srMain_11(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of_m_srBoundPlaneAvatar_12(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of_m_srBoundPlaneAvatarMask_13(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of_m_srFlyingMark_14(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of_m_goEffectCanMerge_15(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of__Trigger_16(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of__srShadow_17(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of_m_nId_18(),
	Lot_t51B999792800E5A264A2CA51A4C521311958F874::get_offset_of__enviromentMask_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1), -1, sizeof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2746[98] = 
{
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_goContainerEffects_4(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__goStartPos_5(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryNodeStart_6(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryNodeCenter_7(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryNodeEnd_8(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_dicLocationPoints_9(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryStartLocationPointOfEachSeg_10(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryStartLocationInfoPointOfEachSeg_11(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_containerLocations_12(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__txtDebugInfo_13(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__txtOfflineDetailDebug_14(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__moneyTrigger_15(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryMoneyTrigger_16(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__basescaleCPosCoinIcon_17(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields::get_offset_of_vecTempPos_18(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields::get_offset_of_vecTempScale_19(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields::get_offset_of_colorTemp_20(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields::get_offset_of_s_Instance_21(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_vecLocalPosOnLot_22(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_nCurRaise_23(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryCoin0_24(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryCoin1_25(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryCoin2_26(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__txtPrestigeTimes_27(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__txtPrestigeTimes_Shadow_28(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__starsPrestigeTimes_29(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__txtTotalCoinOfThisPlanet_30(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__tmTotalCoinOfThisPlanet_31(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__panelCollectOfflineProfit_32(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__txtOfflineProfitOfThisDistrict_33(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__btnOpenBigMap_34(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__btnCloseBigMap_35(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__moneyCoin_36(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryActivePlanetCoin_37(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__moneyGreenCash_38(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__txtDiamond_39(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__txtDiamond_Shadow_40(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__panelRaiseDetail_41(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__txtRaiseDetail_42(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__btnBuy_43(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__BigMap_44(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__panelPrestige_45(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__Prestige_46(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__txtNumOfRunningPlane_47(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__txtDPS_48(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__goRecycleBox_49(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_DISTANCE_UNIT_50(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_TURN_RADIUS_51(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_START_POS_52(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_LENGTH_OF_TURN_53(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_PERIMETER_54(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_MERGE_OFFSET_55(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_MERGE_TIME_56(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_MERGE_WAIT_TIME_57(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__containerRunningPlanes_58(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_lstRunningPlanes_59(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryPosThreshold_60(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryStartPosOfEachSegment_61(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryCircleCenterOfTurn_62(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_fMergeMoveSpeed_63(),
	0,
	0,
	0,
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryLots_67(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryLotsPositionPointsContainers_68(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_StartBelt_69(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryRoundTimeByLevel_70(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_lstAllPlanes_71(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_vecStartPos_72(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_vecNodeStartPos_73(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_vecNodeCenterPos_74(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_vecNodeEndPos_75(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_fRadius_76(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_fVertLineLen_77(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_fHoriLineLen_78(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_fRoundLen_79(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_fPosAdjustDistance_80(),
	0,
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_f10SecLoopElapse_82(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_bIsVisible_83(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_lstTempPLanes_84(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_fAccelerate_85(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_fPreAccerlateTimeLeft_86(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_fSpeedAccelerate_87(),
	0,
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_aryAccelerate_89(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_bLostFocus_90(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of__panelAds_91(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_fOfflineProfit_92(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_vecTempPosLeft_93(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_vecTempPosRight_94(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_lstRunLocations_95(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_dicLotPosOfLevel_96(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_selected_one_97(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_dShowingNumber_98(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_dNumberChangeSpeed_99(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_fCoinChangeTime_100(),
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1::get_offset_of_m_dDestCoinNumber_101(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (ePosType_tE6D03887D78F8D40B765866D3E62F40C088C676A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2747[9] = 
{
	ePosType_tE6D03887D78F8D40B765866D3E62F40C088C676A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (ePlaneStatus_tAA6F08AE7DEE870620C7F8E6459B8578DECEC0F4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2748[7] = 
{
	ePlaneStatus_tAA6F08AE7DEE870620C7F8E6459B8578DECEC0F4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949)+ sizeof (RuntimeObject), sizeof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2749[8] = 
{
	sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949::get_offset_of_nLocationIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949::get_offset_of_vecSrc_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949::get_offset_of_vecDest_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949::get_offset_of_vecDir_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949::get_offset_of_vecShowDir_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949::get_offset_of_fRotateAngle_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949::get_offset_of_fDeltaAngleToNextNode_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949::get_offset_of_fDistanceToNextNode_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C), -1, sizeof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2750[89] = 
{
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__imgCPosCoinIcon_4(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__txtCurPlanetName_5(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__skeletonEffectCurTrack_Container_6(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__skeletonEffectCurTrack_7(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_colorPlanetUnlockBlack_8(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_colorBlackWhite_9(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_colorOutlineNormal_10(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_colorOutlineCurTrack_11(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_matBlackWhite_12(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_sprBlockBgCurTrack_13(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_sprBlockBgNormal_14(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_sprBlockBgCanNotUnlock_15(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_fSlideTime_16(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_aryTheMainlandCenters_17(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields::get_offset_of_vecTempPos_18(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields::get_offset_of_vecTempScale_19(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields::get_offset_of_s_Instance_20(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_aryDistrictName_21(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_sprBg_Locked_Left_22(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_sprBg_Locked_Right_23(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_sprBgUnlocked_Left_24(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_sprBgUnlocked_Right_25(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_aryMoneyCounters_26(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_aryVariousCoinIcons_27(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__btnAdventure_28(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__containerZengShouCounters_29(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__containerRecycledCounter_30(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__panelZengShou_31(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__panelPanelDetails_32(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__txtPanelDetailstitle_33(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_aryUIDistricts_34(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_aryUIPlanets_35(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__panelUnlockDistrict_36(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__txtUnlockDistrictCost_37(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__imgUnlockTrackPriceIcon_38(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__panelUnlockPlanet_39(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__txtUnlockPlanetCost_40(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__imgUnlockPlanetPriceIcon_41(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__panelEntering_42(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__containerPlanets_43(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__containerMainPlanets_44(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__containerOtherPlanets_45(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__panelScienceTree_46(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__txtCurPlanetAndTrack_47(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__txtCurTrackLevel_48(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_aryPlanetTitle_49(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__subpanelBatCollectOffline_50(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__txtBatOfflineGain_51(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__txtBatOfflineDetail_52(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__txtBatOfflineTitle_53(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__goStarSky_54(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_fStarSkyMoveSpeed_55(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_aryPlanets_56(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_CurPlanet_57(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_CurDistrict_58(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_nCurShowPlanetDetialIdOnUI_59(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_dicZengShouCounters_60(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_aryCoin0Value_61(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_aryCoin1Value_62(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_aryCoin2Value_63(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_aryDiamondValue_64(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__btnCurEnterButton_65(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_fUpdatePlanetInfoLoopTimeElapse_66(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_PlanetToPrestige_67(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_DistrictToPrestige_68(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_bPlanetDetailPanelOpen_69(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_districtToUnlock_70(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_planetToUnlock_71(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_nToEnterRace_PlanetId_72(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_nToEnterRace_DistrictId_73(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_fPreEnterRaceLoopTime_74(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_bStarSkyMoving_75(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_bIsShowingZengShouPanel_76(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_bTestShow_77(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_bSliding_78(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_bBigMapShowing_79(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_fSlideSpeed_80(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_fSlideA_81(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_fV0_82(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of__goSVContent_83(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_fDestPos_84(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_fStartDragPos_85(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_fEndDragPos_86(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_bDragging_87(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_fMouseStartPos_88(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_bClickProhibit_89(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_fClickProhibitTimeLeft_90(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_nCurSelctedIndex_91(),
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C::get_offset_of_m_aryMainLands_92(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (ePlanetId_t35BB1983FD70DAE2F267AF836478B4561EECF2D4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2751[5] = 
{
	ePlanetId_t35BB1983FD70DAE2F267AF836478B4561EECF2D4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (ePlanetStatus_tC2E1F5015F795EE2F74D5F17749C432ECF4567EE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2752[4] = 
{
	ePlanetStatus_tC2E1F5015F795EE2F74D5F17749C432ECF4567EE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (eDistrictStatus_tA21EDB657F7184F24E00E5C52D26254A33458EC8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2753[4] = 
{
	eDistrictStatus_tA21EDB657F7184F24E00E5C52D26254A33458EC8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464), -1, sizeof(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2754[11] = 
{
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464::get_offset_of__srMain_4(),
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464_StaticFields::get_offset_of_vecTempScale_5(),
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464_StaticFields::get_offset_of_vecTempPos_6(),
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464::get_offset_of_m_vecTiaoZiPos_7(),
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464_StaticFields::get_offset_of_s_Instance_8(),
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464::get_offset_of__baseScale_9(),
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464::get_offset_of__baseScaleCointCounter_10(),
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464::get_offset_of__goContainerJinBi_11(),
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464::get_offset_of__goContainerRichJinBi_12(),
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464::get_offset_of_m_nJinBiNumToGenerate_13(),
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464::get_offset_of_m_fTimeElapse_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860), -1, sizeof(OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2755[10] = 
{
	OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860_StaticFields::get_offset_of_s_Instance_4(),
	OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860::get_offset_of__txtWatchAdsPromote_5(),
	OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860::get_offset_of__txtDiamondPromote_6(),
	OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860::get_offset_of__imgCoinIcon_7(),
	OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860::get_offset_of__txtPromotePercent_8(),
	OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860::get_offset_of_m_fWatchAdsPromote_Initial_9(),
	OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860::get_offset_of_m_fDiamondPromote_Initial_10(),
	OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860::get_offset_of_m_nDiamondCost_Initial_11(),
	OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860::get_offset_of_m_fWatchAdsPromote_Real_12(),
	OfflineManager_t9C8199967E6FA28551A1FDFC1AFCEBAE15A48860::get_offset_of_m_fDiamondPromote_Real_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (U3CLoadConfig_OfflineU3Ed__14_t1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2756[5] = 
{
	U3CLoadConfig_OfflineU3Ed__14_t1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A::get_offset_of_U3CU3E1__state_0(),
	U3CLoadConfig_OfflineU3Ed__14_t1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A::get_offset_of_U3CU3E2__current_1(),
	U3CLoadConfig_OfflineU3Ed__14_t1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A::get_offset_of_szFileName_2(),
	U3CLoadConfig_OfflineU3Ed__14_t1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A::get_offset_of_U3CU3E4__this_3(),
	U3CLoadConfig_OfflineU3Ed__14_t1216BF47E90FAE25C38F3E4B3B5D3D28B985C40A::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273), -1, sizeof(Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2757[68] = 
{
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273_StaticFields::get_offset_of_vecCenter_4(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_aryTailSlow_5(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_aryTailFast_6(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__tailSlow_7(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__tailFast_8(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__txtStatus_9(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273_StaticFields::get_offset_of_vecTempPos_10(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273_StaticFields::get_offset_of_vecTempPos1_11(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273_StaticFields::get_offset_of_vecTempScale_12(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__SortingMain_13(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__jetRailNormal0_14(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__jetRailNormal1_15(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__jetRailCrazy_16(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__sortingGroup_17(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__basescaleBox_18(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__BaseScale_19(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__BaseShake_20(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_ePlaneStatus_21(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_ePosType_22(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_fRunningSpeed_23(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_fRunningSpeedPerFrame_24(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_fRunningSpeedPerFrame_25(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_fAngleSpeedOfTheTurnPerFrame_26(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_fAngleSpeedOfTheTurnPerFrame_27(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_fCurAngle_28(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_nLevel_29(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_vecStartPosOfSegment_30(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_CollisionStartBelt_31(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_CollisionLot_32(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_Lot_33(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_FlyingDestLot_34(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_srMain_35(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_srForMergeAnimation_36(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_srBox_37(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_vecRunningToLotSpeed_38(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_goEffectJet_39(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_goContainerEffects_40(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_gocontainerMain_41(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_lstTemp_42(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_fAccelerateTime_43(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__goTrail_44(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__renderTrail_45(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_sprRunningAvatar_46(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_sprParkingAvatar_47(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_bRecycling_48(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__containerTreasureBox_49(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_nCoinGainPerRound_50(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_fInitiativeAccelerateRate_51(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_RunLocaitonInfo_52(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_vecCurRunSpeed_53(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_trail0_54(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_trail1_55(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of__Trail_56(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_bPreRun_57(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_fPosAdjustSpeed_58(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_vecBeginDragPlanePos_59(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_vecBeginDragMousePos_60(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_vecOffsetWhenBeginDrag_61(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_fRunToLotTime_62(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_ToMergePlane_63(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_dicToMerge_64(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_nMergingAnimationStatus_65(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_fMergingWaitTimeElapse_66(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_nTreasureBoxStatus_67(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_fDropDistance_68(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_fTreasureBoxDropSpeed_69(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_fWaitingTimeElapse_70(),
	Plane_t41E94F32055C9505FD4C25C0B01807EF204A7273::get_offset_of_m_bOpeningTreasureBox_71(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2758[7] = 
{
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B::get_offset_of_m_aryDistricts_4(),
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B::get_offset_of__uiPlanetLock_5(),
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B::get_offset_of__uiUnlockPlanetPrice_6(),
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B::get_offset_of_m_eStatus_7(),
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B::get_offset_of_m_nId_8(),
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B::get_offset_of_m_nUnlockPrice_9(),
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B::get_offset_of_m_nCoin_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (PLaneTrigger_t26A91CD049429A2694001257C5B0F2EB7D5BE993), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD), -1, sizeof(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2760[32] = 
{
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD_StaticFields::get_offset_of_vecTempPos_4(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD_StaticFields::get_offset_of_vecTempScale_5(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_Config_6(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of__btnBeginUnlocking_7(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of__containerBeforePreUnlock_8(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of__containerAfterPreUnlock_9(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of__containerUnlocked_10(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of__txtTitle_11(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of__imgTitle_12(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of__txtUnlockTotalTime_13(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of__txtCoinPrice_14(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of__imgCoinIcon_15(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of__txtLeftTime_16(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of__txtDiamondPrice_17(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of__txtAdsTimeReduce_18(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_dataStartTime_19(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_dateAdsStartTime_20(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_nLeftTime_21(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_nAdsLeftTime_22(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_eStatus_23(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_nLevel_24(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_lstToActivate_25(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_BoundTrack_26(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_nRealDiamondCost_27(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_nRealUnlockTime_28(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_nRealCoinCost_29(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_goBoundContainer_30(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_nPlanetId_31(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_nTrackId_32(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_fAdsTimeElapse_33(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_nNoAdsCount_34(),
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD::get_offset_of_m_fTimeElaspe_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F), -1, sizeof(ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2761[8] = 
{
	ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F_StaticFields::get_offset_of_s_Instance_4(),
	ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F::get_offset_of_m_sprCanNotUnlockBg_5(),
	ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F::get_offset_of_tempResearchConfig_6(),
	ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F::get_offset_of_m_dicResearchConfig_7(),
	ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F::get_offset_of_m_nWatchAdsReduceTime_8(),
	ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F::get_offset_of_m_nWatchAdsInterval_9(),
	ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F::get_offset_of_m_preResearchCounter_10(),
	ResearchManager_t812A2256BEF43E66355964CDEC4E2B9FA524880F::get_offset_of_m_preResearchCounterContainer_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (eResearchCounterStatus_tBBB7AB208D35C7D68CE9FD1CC40A13533A0F325F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2762[5] = 
{
	eResearchCounterStatus_tBBB7AB208D35C7D68CE9FD1CC40A13533A0F325F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9)+ sizeof (RuntimeObject), sizeof(sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2763[3] = 
{
	sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9::get_offset_of_nCoinCost_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9::get_offset_of_nTimeCost_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sResearchConfig_t0C6B8A3EB463658005C7DD0B324869F729AD21C9::get_offset_of_nDiamondCost_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (U3CLoadConfig_ResearchU3Ed__14_tF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[5] = 
{
	U3CLoadConfig_ResearchU3Ed__14_tF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B::get_offset_of_U3CU3E1__state_0(),
	U3CLoadConfig_ResearchU3Ed__14_tF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B::get_offset_of_U3CU3E2__current_1(),
	U3CLoadConfig_ResearchU3Ed__14_tF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B::get_offset_of_szFileName_2(),
	U3CLoadConfig_ResearchU3Ed__14_tF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B::get_offset_of_U3CU3E4__this_3(),
	U3CLoadConfig_ResearchU3Ed__14_tF3FD02A66D4D996E77C8B6CAD7D3AAA049E5B33B::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (ResolutionManager_t387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A), -1, sizeof(ResolutionManager_t387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2765[2] = 
{
	ResolutionManager_t387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A_StaticFields::get_offset_of_s_Instance_4(),
	ResolutionManager_t387ECD8A6E79FCD193EB6761C97F7FAAA38F3C2A::get_offset_of_m_eResolution_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (eResolution_tF2B80E36A8A199AEEF1E37CDF36DFE5F7EE3F0FE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2766[3] = 
{
	eResolution_tF2B80E36A8A199AEEF1E37CDF36DFE5F7EE3F0FE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78), -1, sizeof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2767[53] = 
{
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_sprGreenCash_4(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryCoinIcon_New_5(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryCoinIcon_Special_New_6(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryAutomobileParking_0_7(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryAutomobileParking_1_8(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryAutomobileParking_2_9(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryAutomobileParking_3_10(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryAutomobileParking_4_11(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryAutomobileRunning_0_12(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryAutomobileRunning_1_13(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryAutomobileRunning_2_14(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryAutomobileRunning_3_15(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryAutomobileRunning_4_16(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_dicAutoSpritesParking_17(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_dicAutoSpritesRunning_18(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields::get_offset_of_s_Instance_19(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields::get_offset_of_vecTempScale_20(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of__font_21(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_preEnviromentMask_22(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_preUIShoppinAndItemCounter_23(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryPlanetAvatar_24(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_sprArrow_25(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_arySkillPointIcon_26(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryCoinIcon_27(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_goRecycledPlanes_28(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryPlaneSprites_29(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryParkingPlaneSprites_30(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_preZengShouCounter_31(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_prePlane_32(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_preJinBi_33(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_preRichTiaoZi_34(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_preVehicleCounter_35(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryTrailColor_36(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_preSkill_37(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_preFlyingCoin_38(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_preScienceTreeConfig_39(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_preScienceLeaf_40(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryItemIcon_41(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryCoinRaiseItemIcon_42(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_aryCoinDiamondItemIcon_43(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_sprWatchAdIcon_44(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields::get_offset_of_s_TrailColorIndex_45(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_lstRecycledPlanes_46(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_lstRecycledJinBi_47(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_lstRecycledRichTiaoZi_48(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_preUiItem_49(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_lstRecylcedUiItems_50(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of__containerRecycledUIItems_51(),
	0,
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_goRecycedVehicleCounter_53(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_goRecycedResearchCounter_54(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_lstRecycledFlyingCoin_55(),
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78::get_offset_of_m_preTrail_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (eItemIconType_t1AA9127543953891EBC2D8BC6E1A8E917CED3F49)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2768[3] = 
{
	eItemIconType_t1AA9127543953891EBC2D8BC6E1A8E917CED3F49::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108), -1, sizeof(JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2769[41] = 
{
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_LOT_X_GAP_4(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_LOT_Y_GAP_5(),
	0,
	0,
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of__containerLocations_8(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108_StaticFields::get_offset_of_s_Instance_9(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108_StaticFields::get_offset_of_vecTempPos_10(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108_StaticFields::get_offset_of_vecTempScale_11(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_nSceneId_12(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_aryEnvironmentColor_13(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_aryEnvironmentColor_Temp_14(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_preSnowFlake_15(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_aryMainBg_16(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_aryLotSpr_17(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_aryAirline_18(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_aryLot_19(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_aryLotMaskColor_20(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_arySceneAccessoriesContainers_21(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_containerSnowFlakes_22(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of__srMainBg_23(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of__srAirline_24(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSnowMoveSpeed_25(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSnowMoveStartPosLeft_26(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSnowMoveStartPosRight_27(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSnowMoveEndPosHigh_28(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSnowMoveEndPosLow_29(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSnowGenerateInterval_30(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSnowStartPosY_31(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSnowBeginFadePosY_32(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSnowStartPosX_33(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSnowEndPosY_34(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSnowDropMovementPerFrame_35(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSandStormMoveSpeed_36(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSandStormMoveStartPos_37(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSandStormMoveEndPos_38(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_arySandStorm_39(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSandStormMovementPerFrame_40(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fAdjustValue_41(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fAdjustShit_42(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_fSnowGenerateTimeElapse_43(),
	JTDHSceneManager_tEE575F733C779BF24643585515AFFC7AFCC97108::get_offset_of_m_lstSnowFlakes_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2770[10] = 
{
	ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718::get_offset_of_m_eType_4(),
	ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718::get_offset_of_m_nIndexOfThisBranch_5(),
	ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718::get_offset_of_m_nLevel_6(),
	ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718::get_offset_of_m_Config_7(),
	ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718::get_offset_of__goLock_8(),
	ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718::get_offset_of__imgOutline_9(),
	ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718::get_offset_of__imgMain_10(),
	ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718::get_offset_of_m_aryIntParams_11(),
	ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718::get_offset_of_m_aryFloatParams_12(),
	ScienceLeaf_t6FEDE450E219E622BB094980970D85E8DA434718::get_offset_of_m_bSwitch_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E), -1, sizeof(ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2771[45] = 
{
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E_StaticFields::get_offset_of_s_Instance_4(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__imgCurSelectedLeafOutline_5(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_colorCurCanUnlock_6(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_colorLeafLocked_7(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_preLeafOutline_8(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_preLeafLock_9(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E_StaticFields::get_offset_of_vecTempPos_10(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E_StaticFields::get_offset_of_vecTempPos1_11(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E_StaticFields::get_offset_of_vecTempScale_12(),
	0,
	0,
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_dicSkillPoint_15(),
	0,
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_aryBranch0_17(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_aryBranch1_18(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_aryBranch2_19(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_lstBranches_20(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__panelScienceTree_21(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__containerTree_22(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__subpanelUpgrade_23(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__txtLevel_24(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__containerThisLevelInfo_25(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__containerNextLevelInfo_26(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__btnUpgrade_27(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__txtPrice_28(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__imgMoneyIcon_29(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__txtOperateType_30(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__imgCurSelectedLeafAvatar_31(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__containerLevel_32(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__txtCurLevelValue_33(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__txtNextLevelValue_34(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__txtDesc_35(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__moneyBranch0_36(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__moneyBranch1_37(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of__moneyBranch2_38(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_dicTree_39(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_dicLeafConfig_40(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_CurSelectedLeaf_41(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_bShowingScienceTree_42(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_tempLeafConfig_43(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_dicSkillPointBuyTimes_44(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_vecDragLastPos_45(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_bDragging_46(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_dicPointsForLevels_47(),
	ScienceTree_t57927F0C7068DCB090379E0B11DC86D2D88A2D2E::get_offset_of_m_bTalentConfigLoaded_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (eBranchType_tA7C9751A118EB41EC27607C7387348B868619540)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2772[4] = 
{
	eBranchType_tA7C9751A118EB41EC27607C7387348B868619540::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (eScienceType_t407F29E64187E9619231D131CBD33C05C0E3DB4E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2773[8] = 
{
	eScienceType_t407F29E64187E9619231D131CBD33C05C0E3DB4E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (eLeafConfig_tE6D039658A5FBCA8E009DDCD81DBCE5ECF027A09)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2774[2] = 
{
	eLeafConfig_tE6D039658A5FBCA8E009DDCD81DBCE5ECF027A09::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11)+ sizeof (RuntimeObject), sizeof(sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2775[6] = 
{
	sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11::get_offset_of_eType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11::get_offset_of_nType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11::get_offset_of_aryIntParams_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11::get_offset_of_aryFloatParams_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11::get_offset_of_szDesc_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sLeafConfig_t101684B4AA2B2843705D9A91099D4EA539E12F11::get_offset_of_bSwitch_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (U3CLoadConfig_TalentGeneralU3Ed__61_t7C66D1F4C165678DBE343AEA87115D05CCE67AD9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[5] = 
{
	U3CLoadConfig_TalentGeneralU3Ed__61_t7C66D1F4C165678DBE343AEA87115D05CCE67AD9::get_offset_of_U3CU3E1__state_0(),
	U3CLoadConfig_TalentGeneralU3Ed__61_t7C66D1F4C165678DBE343AEA87115D05CCE67AD9::get_offset_of_U3CU3E2__current_1(),
	U3CLoadConfig_TalentGeneralU3Ed__61_t7C66D1F4C165678DBE343AEA87115D05CCE67AD9::get_offset_of_szFileName_2(),
	U3CLoadConfig_TalentGeneralU3Ed__61_t7C66D1F4C165678DBE343AEA87115D05CCE67AD9::get_offset_of_U3CU3E4__this_3(),
	U3CLoadConfig_TalentGeneralU3Ed__61_t7C66D1F4C165678DBE343AEA87115D05CCE67AD9::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[6] = 
{
	ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766::get_offset_of_aryValue_4(),
	ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766::get_offset_of_aryPrice_5(),
	ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766::get_offset_of_szDesc_6(),
	ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766::get_offset_of_m_szKey_7(),
	ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766::get_offset_of_m_CurSelecedLeaf_8(),
	ScienceTreeConfig_t01FCBA6A5FC2BFB9F0F57560FAF0E0D5F2317766::get_offset_of_m_eScienceType_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (ShoppinMall_t6389C5391B85521387589717F7A53592DB917811), -1, sizeof(ShoppinMall_t6389C5391B85521387589717F7A53592DB917811_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2778[51] = 
{
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811_StaticFields::get_offset_of_vecTempPos_4(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811_StaticFields::get_offset_of_vecScale_5(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811_StaticFields::get_offset_of_s_Instance_6(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__vlg_7(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_preTypeContainer_8(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_preItemCounter_9(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_aryCounterPrefab_10(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_nStandardCounterNumPerRow_11(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_fStandardCounterWidth_12(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_fStandardCounterHeight_13(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_fStandardCounterHeight_Long_14(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_fTypeContainerTitleHeight_15(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_vecTypeContainerPos_16(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_goContainerAllType_17(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__panelShoppingMall_18(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__subpanelConfirmBuy_19(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__subpanelConfirmUse_20(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__CurProcessingBuyUiItem_21(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__CurProcessingUsingUiItem_22(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__txtConfirmBuy_FuncAndValue_23(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__txtConfirmBuy_Cost_24(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__txtConfirmBuy_ItemName_25(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__txtConfirmBuy_Value_26(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__imgConfirmBuy_Avatar_27(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__imgConfirmUse_Avatar_28(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_ConfirmBuyShoppingConfig_29(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_nConfirmBuy_Price_30(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_nConfirmBuy_Num_31(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_itemConfirmBuy_32(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_tempShoppinMallItemConfig_33(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_dicShoppingMallItems_34(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_dicItemBuyTimes_35(),
	0,
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_bShoppinmallLoaded_37(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_dicAllItems_38(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_lstRecycledTypeContainers_39(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_containerRecycledItems_40(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_lstRecycledBuyCounters_41(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_aryAvatarBg_42(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__imgCurBuyCounterAvatar_43(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__imgCurBuyCounterAvatarBg_44(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__txtCurBuyCounterDuration_45(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of__txtCurBuyCounterValue_46(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_CurBuyCounter_47(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_nBuyMoneyType_48(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_dicRecycledCounters_49(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_lstCurShowingItems_50(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_dicShoppinTypes_51(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_lstAllShowingTypeContainers_52(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_TypeContainerOfSkillPoint_53(),
	ShoppinMall_t6389C5391B85521387589717F7A53592DB917811::get_offset_of_m_bShoppinMallShowing_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (eItemType_t796F7E98F0ED3F4BC872A243ECB26EBF2A5A25A4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2779[9] = 
{
	eItemType_t796F7E98F0ED3F4BC872A243ECB26EBF2A5A25A4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (ePriceType_t2A0ABDBAB8B516FAA133FC73FC2F9C0F3B46E4EA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2780[4] = 
{
	ePriceType_t2A0ABDBAB8B516FAA133FC73FC2F9C0F3B46E4EA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (ePriceSubType_t452224C5C4361AD927ABAAB951C185B7D2801B5D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2781[4] = 
{
	ePriceSubType_t452224C5C4361AD927ABAAB951C185B7D2801B5D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058)+ sizeof (RuntimeObject), sizeof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2782[16] = 
{
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_nId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_itemConfig_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_nPriceType1_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_nPriceSubType1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_nPrice1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_nNum1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_nPriceType2_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_nPriceSubType2_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_nPrice2_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_nNum2_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_fRisePricePercent_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_nNum_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_nOn_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_szDesc_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_szIconId_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058::get_offset_of_fPriceRiseEachBuy_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (U3CLoadConfig_Shoppingmall_NewU3Ed__44_t9DD82782DF06252A4AE472EEF05064F8AF6CF0F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2783[5] = 
{
	U3CLoadConfig_Shoppingmall_NewU3Ed__44_t9DD82782DF06252A4AE472EEF05064F8AF6CF0F0::get_offset_of_U3CU3E1__state_0(),
	U3CLoadConfig_Shoppingmall_NewU3Ed__44_t9DD82782DF06252A4AE472EEF05064F8AF6CF0F0::get_offset_of_U3CU3E2__current_1(),
	U3CLoadConfig_Shoppingmall_NewU3Ed__44_t9DD82782DF06252A4AE472EEF05064F8AF6CF0F0::get_offset_of_szFileName_2(),
	U3CLoadConfig_Shoppingmall_NewU3Ed__44_t9DD82782DF06252A4AE472EEF05064F8AF6CF0F0::get_offset_of_U3CU3E4__this_3(),
	U3CLoadConfig_Shoppingmall_NewU3Ed__44_t9DD82782DF06252A4AE472EEF05064F8AF6CF0F0::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (U3CLoadConfig_ShoppingmallU3Ed__49_t6419239B92C5C5B6EC20B06F98A7552A7FE79176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2784[3] = 
{
	U3CLoadConfig_ShoppingmallU3Ed__49_t6419239B92C5C5B6EC20B06F98A7552A7FE79176::get_offset_of_U3CU3E1__state_0(),
	U3CLoadConfig_ShoppingmallU3Ed__49_t6419239B92C5C5B6EC20B06F98A7552A7FE79176::get_offset_of_U3CU3E2__current_1(),
	U3CLoadConfig_ShoppingmallU3Ed__49_t6419239B92C5C5B6EC20B06F98A7552A7FE79176::get_offset_of_szFileName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2785[10] = 
{
	Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF::get_offset_of_m_eType_4(),
	Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF::get_offset_of_m_eStatus_5(),
	Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF::get_offset_of_m_Config_6(),
	Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF::get_offset_of_m_BoundPlanet_7(),
	Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF::get_offset_of_m_BoundDistrict_8(),
	Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF::get_offset_of_m_BoundUIBUtton_9(),
	Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF::get_offset_of_m_fStartTime_10(),
	Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF::get_offset_of_m_StartTime_11(),
	Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF::get_offset_of_m_fRealTimeDuration_12(),
	Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF::get_offset_of_m_fTimeElapse_1Second_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231), -1, sizeof(SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2786[6] = 
{
	SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231_StaticFields::get_offset_of_s_Instance_4(),
	SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231::get_offset_of__containerAcclerateLights_5(),
	0,
	SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231::get_offset_of_m_dicSkillConfig_7(),
	SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231::get_offset_of_m_arySkillButton_8(),
	SkillManager_tDD7938C310F6B1DD79A1E289163A5569E4C0E231::get_offset_of_tempConfig_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (eSkillType_t6B98D3D74E24B3B140959305730C30677538C578)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2787[5] = 
{
	eSkillType_t6B98D3D74E24B3B140959305730C30677538C578::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E)+ sizeof (RuntimeObject), sizeof(sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E ), 0, 0 };
extern const int32_t g_FieldOffsetTable2788[3] = 
{
	sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E::get_offset_of_fValue_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E::get_offset_of_nColdDown_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E::get_offset_of_nDuration_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (eSkillStatus_t7ACD9CEC3A4B4D024083BE0233781CAE1E8DA753)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2789[4] = 
{
	eSkillStatus_t7ACD9CEC3A4B4D024083BE0233781CAE1E8DA753::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD), -1, sizeof(StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2790[4] = 
{
	StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD_StaticFields::get_offset_of_s_Instance_4(),
	StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD::get_offset_of_m_aryStartArrows_5(),
	StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD::get_offset_of_m_nMaxNum_6(),
	StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD::get_offset_of_m_nCurNum_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (StartRunArrow_t34112B02A95B4E2CF819CE3086EAD5F0AF6257F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[1] = 
{
	StartRunArrow_t34112B02A95B4E2CF819CE3086EAD5F0AF6257F6::get_offset_of_m_srMain_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455), -1, sizeof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2792[49] = 
{
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_nFreshGuideStatus_4(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__vlg_5(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__goFreeFlag_6(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__goBtnTanGeChe_7(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__imgPriceOff_8(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_vecPriceOffStartPos_9(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_vecPriceOffSummitPos_10(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_vecPriceOffEndPos_11(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_fPriceOffAniTime_12(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_vecPriceOffAniSpeed_13(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_fPriceOffAniA_14(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_fPriceOffAniV0_15(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields::get_offset_of_vecTempScale_16(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields::get_offset_of_vecTempPos_17(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields::get_offset_of_colorTemp_18(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__txtRecommendPrice_19(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__imgRecommendAvatar_20(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__imgRecommendCoinIcon_21(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__panelRecommendDetail_22(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__txtRecommendDetail_23(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__toggleNoPlane_24(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__toggleNoDrop_25(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__inputTrackLevel_26(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields::get_offset_of_s_Instance_27(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__goCounterList_28(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_lstVehicleCounters_29(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_dicVehicleCounters_30(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of__panelTanGeChe_31(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_fRealTimeDiscount_32(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_sprJianYingTemp_33(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_sprLockedBuyButton_34(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_spUnlockedBuyButton_35(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_colorUnlocked_36(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_colorLocked_37(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_nRecommendLowerAmount_38(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_nRecommendHigherAmount_39(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_lstTempShowingCounters_40(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_lstTempShowingUnlockedCounters_41(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_dicShareUICountersContainer_42(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_nCurWatchAdsFreeCounterIndex_43(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_bIsVisible_44(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_fRandomEventInterval_45(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_fRandomEventTimeElapse_46(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_bRandomEventProcessing_47(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_s_nRecommendCount_48(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_selected_one_49(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_bNoPlane_50(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_bNoDrop_51(),
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455::get_offset_of_m_nPriceOffAnimationStatus_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (eVehicleCoounterStatus_t0D796DADCA513036C03B7F33D32F457BE2C3D475)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2793[4] = 
{
	eVehicleCoounterStatus_t0D796DADCA513036C03B7F33D32F457BE2C3D475::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86), -1, sizeof(RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2794[19] = 
{
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of__srIcon_4(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of__txtValue_5(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of__txtValue_Shadow_6(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86_StaticFields::get_offset_of_vecTempPos_7(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86_StaticFields::get_offset_of_vecTempScale_8(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86_StaticFields::get_offset_of_colorTemp_9(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of_m_vecStartPos_10(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of_m_nStatus_11(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of_m_fInitScale_12(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of_m_fMaxScale_13(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of_m_fFirstSegDis_14(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of_m_fFirstSegTime_15(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of_m_fSecSegDis_16(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of_m_fSecSegTime_17(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of_m_fScaleSpeed_18(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of_m_fFadeSpeed_19(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of_m_fRaiseAccelerate_20(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of_m_fRaiseSpeed_21(),
	RichTiaoZi_t145B6185E429BFFC2E81D4A69C2C7EEA97198E86::get_offset_of_m_fAlpha_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648), -1, sizeof(TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2795[10] = 
{
	TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648_StaticFields::get_offset_of_vecTempPos_4(),
	TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648::get_offset_of__srMain_5(),
	TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648::get_offset_of_m_fT_6(),
	TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648::get_offset_of_m_fSX_7(),
	TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648::get_offset_of_m_fSY_8(),
	TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648::get_offset_of_m_fV0X_9(),
	TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648::get_offset_of_m_fV0Y_10(),
	TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648::get_offset_of_m_fA_11(),
	TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648::get_offset_of_m_nStatus_12(),
	TiaoZiJinBi_t09250279C8D7D5B011E9ACCE6111D739FD130648::get_offset_of_m_fDir_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A), -1, sizeof(TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2796[4] = 
{
	TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A_StaticFields::get_offset_of_s_Instance_4(),
	TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A::get_offset_of__panelMain_5(),
	TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A::get_offset_of__txtOpenResult_6(),
	TreasureBoxManager_t23242BEA18FC83D440BB4AFECDF9F40B5209D14A::get_offset_of_m_lstId_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (UIBlock_t34D109BECBD5E8B392500CB078B388ADB954326B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2797[3] = 
{
	UIBlock_t34D109BECBD5E8B392500CB078B388ADB954326B::get_offset_of__imgMain_4(),
	UIBlock_t34D109BECBD5E8B392500CB078B388ADB954326B::get_offset_of__outLine_5(),
	UIBlock_t34D109BECBD5E8B392500CB078B388ADB954326B::get_offset_of__imgBg_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (UIResearchCounterContainer_tCD786B78332DEDA681A84956DC136BCFE55100F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2798[3] = 
{
	UIResearchCounterContainer_tCD786B78332DEDA681A84956DC136BCFE55100F3::get_offset_of__txtIntro_4(),
	UIResearchCounterContainer_tCD786B78332DEDA681A84956DC136BCFE55100F3::get_offset_of__imgBg_5(),
	UIResearchCounterContainer_tCD786B78332DEDA681A84956DC136BCFE55100F3::get_offset_of_m_BoundResearchCounter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011), -1, sizeof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2799[17] = 
{
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011_StaticFields::get_offset_of_tempColor_4(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011_StaticFields::get_offset_of_vecTempPos_5(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011_StaticFields::get_offset_of_vecTempScale_6(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of__frameaniClick_7(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of__basescalePaw_8(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of__imgPaw_9(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of__srPaw_10(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of_m_fCurAlpha_11(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of_m_bFading_12(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of_m_bMoving_13(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of_m_vecStartPos_14(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of_m_vecEndPos_15(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of_m_vecMoveSpeed_16(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of_m_fWaitTime_17(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of_m_nMoveStatus_18(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of_m_fTimeElapse_19(),
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011::get_offset_of_eventMoveEnd_20(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
