﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cheat : MonoBehaviour {

    public static Cheat s_Instance = null;

    /// <summary>
    /// UI
    public Toggle _toggleShowDebugInfo;

    public InputField _txtUIAnchorIndex;
    public InputField _txtSceneAnchorIndex;
    public GameObject[] m_aryUiAnchor;
    public GameObject[] m_arySceneAnchor;
    public Text _txtDebugInfo;
    public GameObject _sprAirline;

    public Dropdown _dropPlanetId;
    public Dropdown _dropDistrictId;
    public InputField _inputCoin0;
    public InputField _inputCoin1;
    public InputField _inputCoin2;
    public InputField _inputGreenCash;
    public InputField _inputSkillPoint0;
    public InputField _inputSkillPoint1;
    public InputField _inputSkillPoint2;

    public Slider _sliderVibrateAmount;
    public Text _txtVibrateAmount;

    public GameObject uiCheatPanel;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        Init();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Init()
    {
        List<string> showNames = new List<string>();
        showNames.Add("0 - 青铜");
        showNames.Add("1 - 白银");
        showNames.Add("2 - 黄金");
        UIManager.UpdateDropdownView(_dropPlanetId, showNames);

        showNames.Clear();
        showNames.Add("0赛道");
        showNames.Add("1赛道");
        showNames.Add("2赛道");
        showNames.Add("3赛道");
        showNames.Add("4赛道");
        UIManager.UpdateDropdownView(_dropDistrictId, showNames);

        _sliderVibrateAmount.value = m_lVibrateAmount;
    }

    public void OnClick_Close()
    {
        uiCheatPanel.SetActive( false );
    }

    public void OnClick_Open()
    {
        uiCheatPanel.SetActive( true );
    }

    public void OnClick_UnlockPlanet()
    {
        int nPlanetId = _dropPlanetId.value;
        int nDistrictId = _dropDistrictId.value;
        MapManager.s_Instance.UnLockPlanet(nPlanetId);
    }

    public void OnClick_UnlockDistrict()
    {
        int nPlanetId = _dropPlanetId.value;
        int nDistrictId = _dropDistrictId.value;
        MapManager.s_Instance.UnLockDistrict(nPlanetId, nDistrictId);
    }

    public void OnClick_EnterDistrict()
    {
        int nPlanetId = _dropPlanetId.value;
        int nDistrictId = _dropDistrictId.value;

        MapManager.s_Instance.EnterDistrict(nPlanetId, nDistrictId);
    }

    public void OnClick_ModifyCoin_0()
    {
        double nValue = 0;
        if ( !double.TryParse( _inputCoin0.text, out nValue ) )
        {
            nValue = 0;
        }
        /*
        Planet planet = MapManager.s_Instance.GetPlanetById(0);
        planet.SetCoin( nValue );
        */
        AccountSystem.s_Instance.SetCoin( 0, nValue);


       
    }

    public void OnClick_ModifyCoin_1()
    {
        double nValue = 0;
        if (!double.TryParse(_inputCoin1.text, out nValue))
        {
            nValue = 0;
        }
        /*
        Planet planet = MapManager.s_Instance.GetPlanetById(1);
        planet.SetCoin(nValue);
        */
        AccountSystem.s_Instance.SetCoin(1, nValue);
    }

    public void OnClick_ModifyCoin_2()
    {
        double nValue = 0;
        if (!double.TryParse(_inputCoin2.text, out nValue))
        {
            nValue = 0;
        }
        /*
        Planet planet = MapManager.s_Instance.GetPlanetById(2);
        planet.SetCoin(nValue);
        */
        AccountSystem.s_Instance.SetCoin(2, nValue);
    }

    public void OnClick_ModifyGreenCash()
    {
        double nValue = 0;
        if (!double.TryParse(_inputGreenCash.text, out nValue))
        {
            nValue = 0;
        }

        AccountSystem.s_Instance.SetGreenCash(nValue);
    }

    public void OnClick_SetSkillPoint0()
    {
        int nValue = 0;
        if (!int.TryParse(_inputSkillPoint0.text, out nValue))
        {
            nValue = 0;
        }
        ScienceTree.s_Instance.SetSkillPoint(ScienceTree.eBranchType.branch0, nValue);
    }

    public void OnClick_SetSkillPoint1()
    {
        int nValue = 0;
        if (!int.TryParse(_inputSkillPoint1.text, out nValue))
        {
            nValue = 0;
        }
        ScienceTree.s_Instance.SetSkillPoint(ScienceTree.eBranchType.branch1, nValue);
    }

    public void OnClick_SetSkillPoint2()
    {
        int nValue = 0;
        if (!int.TryParse(_inputSkillPoint2.text, out nValue))
        {
            nValue = 0;
        }
        ScienceTree.s_Instance.SetSkillPoint(ScienceTree.eBranchType.branch2, nValue);
    }

    public void OnClick_ClearMyData()
    {
        DataManager.s_Instance.ClearMyData();
    }

    GameObject m_goCurShit = null;
    public void OnInputChanged_UIAnchorIndex()
    {
        /*
        float fPosY = 0;
        if ( float.TryParse(_txtUIAnchorIndex.text, out fPosY) )
        {
            //TanGeChe.s_Instance._vlg.padding.bottom = (int)fPosY;
            vec3Temp = UIManager.s_Instance.m_vecSubContainerForBigMap;
            vec3Temp.y -= fPosY;
            UIManager.s_Instance._uiSubContainerForBigMap.transform.localPosition = vec3Temp;
        }
        */
        int nTotalSec = 0;
        if ( int.TryParse(_txtUIAnchorIndex.text, out nTotalSec) )
        {
            // Debug.Log( CyberTreeMath.FormatTime(nTotalSec) ) ;

            string szShit = CyberTreeMath.GetFormatMoney(nTotalSec);
            Debug.Log("szShit=" + szShit);
        }


        return;

        int nIndex = 0;
        int.TryParse(_txtUIAnchorIndex.text, out nIndex);

        if ( nIndex == 0 )
        {
            m_goCurShit = _sprAirline;
            _txtDebugInfo.text = m_goCurShit.transform.localScale.ToString("f2");
        }
        else
        {
            m_goCurShit = m_aryUiAnchor[nIndex - 1];
            _txtDebugInfo.text = m_goCurShit.transform.localPosition.ToString("f2");
        }

        m_fShitSpeed = 1f;
    }

    public void OnInputChanged_SceneAnchorIndex()
    {
        float fPosY = 0;
        if (float.TryParse(_txtUIAnchorIndex.text, out fPosY))
        {
            //TanGeChe.s_Instance._vlg.padding.bottom = (int)fPosY;
            vec3Temp = UIManager.s_Instance.m_vecAdventureAndScience;
            vec3Temp.y += fPosY;
            UIManager.s_Instance._uiAdventureAndScience.transform.localPosition = vec3Temp;
        }



        return;


        int nIndex = 0;
        int.TryParse(_txtSceneAnchorIndex.text, out nIndex);
        m_goCurShit = m_arySceneAnchor[nIndex - 1];
        _txtDebugInfo.text = m_goCurShit.transform.localPosition.ToString("f2");
        m_fShitSpeed = 0.01f;
    }


    static Vector3 vec3Temp = new Vector3();
    public void OnClick_Up()
    {
        /*
        if (m_goCurShit == _sprAirline)
        {
            vec3Temp = m_goCurShit.transform.localScale;
            vec3Temp.x += 0.01f;
            vec3Temp.y += 0.01f;
            m_goCurShit.transform.localScale = vec3Temp;

        }
        else
        {
            vec3Temp = m_goCurShit.transform.localPosition;
            vec3Temp.y += m_fShitSpeed;
            m_goCurShit.transform.localPosition = vec3Temp;
        }


        _txtDebugInfo.text = vec3Temp.ToString("f2");
        */
        vec3Temp = UIManager.s_Instance._canvasScaler.referenceResolution;
        vec3Temp.y += 1;
        UIManager.s_Instance._canvasScaler.referenceResolution = vec3Temp;

        Main.s_Instance._txtDebugInfo.text = UIManager.s_Instance._canvasScaler.referenceResolution.ToString();
    }

    float m_fShitSpeed = 0.01f;
    public void OnClick_Down()
    {
        /*
        if (m_goCurShit == _sprAirline)
        {
            vec3Temp = m_goCurShit.transform.localScale;
            vec3Temp.x -= 0.01f;
            vec3Temp.y -= 0.01f;
            m_goCurShit.transform.localScale = vec3Temp;

        }
        else
        {
            vec3Temp = m_goCurShit.transform.localPosition;
            vec3Temp.y -= m_fShitSpeed;
            m_goCurShit.transform.localPosition = vec3Temp;
        }

        _txtDebugInfo.text = vec3Temp.ToString("f2");
        */
        vec3Temp = UIManager.s_Instance._canvasScaler.referenceResolution;
        vec3Temp.y += 1;
        UIManager.s_Instance._canvasScaler.referenceResolution = vec3Temp;

        Main.s_Instance._txtDebugInfo.text = UIManager.s_Instance._canvasScaler.referenceResolution.ToString();
    }

    public void OnClick_Left()
    {
        vec3Temp = m_goCurShit.transform.localPosition;
        vec3Temp.x -= m_fShitSpeed;
        m_goCurShit.transform.localPosition = vec3Temp;
        _txtDebugInfo.text = vec3Temp.ToString("f2");
    }

    public void OnClick_Right()
    {
        vec3Temp = m_goCurShit.transform.localPosition;
        vec3Temp.x += m_fShitSpeed;
        m_goCurShit.transform.localPosition = vec3Temp;
        _txtDebugInfo.text = vec3Temp.ToString("f2");
    }

    long m_lVibrateAmount = 1L;
    public void OnClick_Vibrate()
    {
        IntenseVibration.Instance.Vibrate(m_lVibrateAmount);
    }

    public void OnSliderValueChange_VibrateAmount()
    {
        m_lVibrateAmount = (long)_sliderVibrateAmount.value;
        _txtVibrateAmount.text = "震动强度：" + m_lVibrateAmount;
    }

    public bool m_bShowDebugInfo = false;
    public void OnToggleValueChanged_ShowDebugInfo()
    {
        m_bShowDebugInfo = _toggleShowDebugInfo.isOn;
    }


}
