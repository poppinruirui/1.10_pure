﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberTreeMath : MonoBehaviour
{

    public const float ANGLE_TO_RADIAN_XISHU = 0.017453f;

    public const int SECONDS_PER_MIN = 60;
    public const int SECONDS_PER_HOUR = 3600;
    public const int SECONDS_PER_DAY = 86400;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public static float Angle2Radian(float fAngle)
    {
        return ANGLE_TO_RADIAN_XISHU * fAngle;
    }

    public static float Radian2Angel(float fRadian)
    {
        return fRadian / ANGLE_TO_RADIAN_XISHU;
    }

    public static float Cos(float fAngle)
    {
        return Mathf.Cos(Angle2Radian(fAngle));
    }

    public static float Sin(float fAngle)
    {
        return Mathf.Sin(Angle2Radian(fAngle));
    }

    // 已知位移和时间求初速度（末速度为零）
    public static float GetV0(float s, float t)
    {
        return (2.0f * s / t);
    }

    // 已知位移和时间求加速度（末速度为零）
    public static float GetA(float s, float t)
    {
        return -2.0f * s / (t * t);
    }

    public static string FormatMoney(double nValue)
    {
        /*
        if ( nValue < 1000 )
        {
            return nValue.ToString();
        }
        else if (nValue < 1000000)
        {
            float fK = (float)nValue / 1000;
            return fK.ToString( "f2" ) + "K";
        }
        else
        {
            float fM = (float)nValue / 1000000;
            return fM.ToString("f2") + "M";
        }
        */





        return nValue.ToString();
    }

    public static string GetFormatMoney(double nValue)
    {
        return GetFormatMoney_New(nValue);

        double dStep = 1000d;
        double dRet = nValue / dStep;
        double dLastRet = 0d;
        int nCount = 0;
        while (dRet >= 1)
        {
            nCount++;
            dStep *= 1000d;
            dLastRet = dRet;
            dRet = nValue / dStep;
        }

        if (nCount < 2)
        {
            return nValue.ToString("f0");
        }

        //   dLastRet *= 1000d;
        string szLastRet = dLastRet.ToString("f2");
        if (nCount == 1)
        {
            szLastRet += "k";

        }
        else if (nCount == 2)
        {
            szLastRet += "m";
        }
        else if (nCount == 3)
        {
            szLastRet += "b";
        }
        else
        {
            szLastRet += "a";
            szLastRet += (char)(nCount + 93);
        }

        return szLastRet;
    }

    public static string GetFormatMoney_New(double nValue)
    {
        double dStep = 1000d;
        double dRet = nValue / dStep;
        double dLastRet = 0d;
        int nCount = 0;
        while (dRet >= 1)
        {
            nCount++;
            dStep *= 1000d;
            dLastRet = dRet;
            dRet = nValue / dStep;
        }

        if (nCount < 1)
        {
            return nValue.ToString("f0");
        }

        //   dLastRet *= 1000d;
        string szLastRet = "";
        if (dLastRet < 10)
        {
            szLastRet = dLastRet.ToString("f2");

        }
        else if (dLastRet < 100)
        {
            szLastRet = dLastRet.ToString("f2").Substring(0,4);
        }
        else
        {
            szLastRet = dLastRet.ToString("f2").Substring(0, 3);
        }

        if (nCount == 1)
        {
            szLastRet += "k";

        }
        else if (nCount == 2)
        {
            szLastRet += "m";
        }
        else if (nCount == 3)
        {
            szLastRet += "b";
        }
        else
        {
            szLastRet += "a";
            szLastRet += (char)(nCount + 93);
        }

        return szLastRet;
    }


    public static string FormatTime(int nSeconds, bool bShowSec = false)
    {
        string szResult = "";

        if (nSeconds == 0)
        {
            return "0";
        }

        int nType = 0;

        int nDay = 0;
        int nHour = 0;
        int nMin = 0;

        TimeSpan ts = new System.TimeSpan(0, 0, nSeconds);
        string timeFormat = "";
        if (bShowSec)
        {
            if (ts.Days > 0)
            {
                timeFormat = "{0:D1}d {1:D2}h {2:D2}m {3:D2}s";
                szResult = string.Format(timeFormat, ts.Days, ts.Hours, ts.Minutes, ts.Seconds);
            }
            else if (ts.Hours > 0)
            {
                timeFormat = "{0:D1}h {1:D2}m {2:D2}s";
                szResult = string.Format(timeFormat, ts.Hours, ts.Minutes, ts.Seconds);
            }
            else if (ts.Minutes > 0)
            {
                timeFormat = "{0:D1}m {1:D2}s";
                szResult = string.Format(timeFormat, ts.Minutes, ts.Seconds);
            }
            else
            {
                timeFormat = "{0:D1}s";
                szResult = string.Format(timeFormat, ts.Seconds);
            }
        }
        else
        {
            if (ts.Days > 0)
            {
                timeFormat = "{0:D1}d {1:D2}h {2:D2}m";
                szResult = string.Format(timeFormat, ts.Days, ts.Hours, ts.Minutes);
            }
            else if (ts.Hours > 0)
            {
                timeFormat = "{0:D1}h {1:D2}m";
                szResult = string.Format(timeFormat, ts.Hours, ts.Minutes);
            }
            else if (ts.Minutes > 0)
            {
                timeFormat = "{0:D1}m {1:D2}s";
                szResult = string.Format(timeFormat, ts.Minutes, ts.Seconds);
            }
            else
            {
                timeFormat = "{0:D1}s";
                szResult = string.Format(timeFormat, ts.Seconds);
            }

            nDay = nSeconds / SECONDS_PER_DAY;
            nHour = nSeconds / SECONDS_PER_HOUR;
            nMin = nSeconds / SECONDS_PER_MIN;
            if (nHour == 24)
            {
                timeFormat = "{0:D1}h {1:D2}m";
                szResult = string.Format(timeFormat, nHour, ts.Minutes);
            }

            if (nSeconds % SECONDS_PER_MIN == 0 && nMin < 60)
            {
                timeFormat = "{0:D1}m";
                szResult = string.Format(timeFormat, nMin);
            }
            if (nSeconds % SECONDS_PER_HOUR == 0 && nHour < 24)
            {
                timeFormat = "{0:D1}h";
                szResult = string.Format(timeFormat, nHour);
            }
            if (nSeconds % SECONDS_PER_DAY == 0 && nDay > 0)
            {
                timeFormat = "{0:D1}d";
                szResult = string.Format(timeFormat, nDay);
            }
        }
        //szResult = ts.Days + "天" + ts.Hours + "小时" + ts.Minutes + "分钟";
        //if (nSeconds > SECONDS_PER_HOUR && bShowSec)
        //{
        //    szResult += ts.Seconds + "秒";
        //}

        return szResult;

        //int nParam = 0;
        //if (nSeconds >= SECONDS_PER_DAY + SECONDS_PER_HOUR ) // 超过一天
        //{
        //    nDay = nSeconds / SECONDS_PER_DAY;
        //    nSeconds = nSeconds % SECONDS_PER_DAY;
        //    nType = 3;
        //}
        //if (nSeconds >= SECONDS_PER_HOUR) // 超过1小时
        //{
        //    nParam = 1;
        //    nHour = nSeconds / SECONDS_PER_HOUR;
        //    nSeconds = nSeconds % SECONDS_PER_HOUR;
        //    if ( nType < 2 )
        //    {
        //        nType = 2;
        //    }
        //}
        //if (nSeconds > SECONDS_PER_MIN ) // 超过1分钟 
        //{
        //    nMin = nSeconds / SECONDS_PER_MIN;
        //    nSeconds = nSeconds % SECONDS_PER_MIN;
        //    if (nType < 1)
        //    {
        //        nType = 1;
        //    }
        //}


        /*
        if (nType == 3)
        {
            szResult = nDay + "天" + nHour + "时" + nMin + "分" + nSeconds + "秒";
        }
        else if (nType == 2)
        {
            szResult =  nHour + "时" + nMin + "分" + nSeconds + "秒";
        }
        else if (nType == 1)
        {
            szResult =  nMin + "分" + nSeconds + "秒";
        }
        else
        {
            szResult =  nSeconds + "秒";
        }

        */
        //return ProcessDayHourMinSec( nDay, nHour, nMin, nSeconds, nParam, bShowSec);



    }

    public static string ProcessDayHourMinSec(int nDay, int nHour, int nMin, int nSec, int nParam = 0, bool bShowSec = true)
    {
        string szResult = "";

        if (nDay > 0)
        {
            szResult += (nDay + "天");
        }

        if (nHour > 0 || (nDay > 0 && (nMin > 0 || nSec > 0)))
        {
            szResult += (nHour + "小时");
        }

        if (nMin > 0 || ((nDay > 0 || nHour > 0) && nSec > 0))
        {
            if (nParam == 1)
            {
                szResult += (nMin + "分钟");
            }
            else
            {
                szResult += (nMin + "分");
            }
        }


        if (nSec > 0 && (!bShowSec && nParam < 1))
        {
            szResult += (nSec + "秒");
        }

        return szResult;
    }


    // include nStartIndex and EndIndex
    public static int GetRandomIndex(int nStartIndex, int EndIndex)
    {
        return UnityEngine.Random.Range(nStartIndex, EndIndex);
    }


    public static float Dir2Angle(float fDirX, float fDirY)
    {
        float fAngle = 0f;

        if (fDirX == 0)
        {
            if (fDirY > 0)
            {
                fAngle = 90f;
            }
            else
            {
                fAngle = 270;
            }
        }
        else if (fDirY == 0)
        {
            if (fDirX > 0)
            {
                fAngle = 0f;
            }
            else
            {
                fAngle = 180;
            }
        }
        else
        {
            fAngle = Mathf.Abs(Mathf.Atan(fDirY / fDirX));
            fAngle = Radian2Angel(fAngle);

            if (fDirX < 0 && fDirY > 0)
            {
                fAngle = 180 - fAngle;
            }
            else if (fDirX < 0 && fDirY < 0)
            {
                fAngle = 180 + fAngle;
            }
            else if (fDirX > 0 && fDirY < 0)
            {
                fAngle = 360 - fAngle;
            }
        }


        return fAngle;
    }

    public static float GetRandomValue(float fMin, float fMax)
    {
        return UnityEngine.Random.RandomRange(fMin, fMax);
    }

    // not include the nMax
    public static int GetRandomValue(int nMin, int nMax)
    {
        return UnityEngine.Random.RandomRange(nMin, nMax);
    }

} // end class
