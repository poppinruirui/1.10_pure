﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Admin : MonoBehaviour {

    static Vector3 vecTempScale = new Vector3();

    AdministratorManager.sAdminConfig m_Config ;

    int m_nColddownLeftTime = 0;
    int m_nDurationLeftTime = 0;

    int m_nRealDuration = 0;

    bool m_bUsing = false;

    AdministratorManager.eUisngAdminStatus m_eStatus = AdministratorManager.eUisngAdminStatus.idle;

    System.DateTime m_dateSkillStartTime;
    float m_fColddownPercent = 0;

    UIAdministratorCounter m_BoundCounter = null;

    District m_BoundTrack = null;

    double m_nBuyPrice = 0; // 买入价 

    double[] m_aryDoubleParams = new double[8];

    // Use this for initialization
    void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {

        SkillLoop();
      
	}

    public void Reset()
    {
        SetUsing( false );
        m_nColddownLeftTime = 0;
        m_nDurationLeftTime = 0;

        SetStatus(AdministratorManager.eUisngAdminStatus.idle);
    }

    public void SetBoundTrack( District track )
    {
        m_BoundTrack = track;
    }

    public District GetBoundTrack()
    {
        return m_BoundTrack;
    }

    public void SetBoundCounter( UIAdministratorCounter counter )
    {
        m_BoundCounter = counter;
    }


    public UIAdministratorCounter GetBoundCounter()
    {
       return m_BoundCounter;
    }

    public void SetConfig( AdministratorManager.sAdminConfig config )
    {
        m_Config = config;
    }

    public void SetUsing( bool bUsing )
    {
        m_bUsing = bUsing;
    }

    public bool GetUsing()
    {
        return m_bUsing;
    }

    public AdministratorManager.sAdminConfig GetConfig()
    {
        return m_Config;
    }

    public void SetStatus( AdministratorManager.eUisngAdminStatus eStatus )
    {
        m_eStatus = eStatus;

    }

    public AdministratorManager.eUisngAdminStatus GetStatus()
    {
        return m_eStatus;
    }


    public void BeginCastSkill( bool bReallyBegin = true )
    {

        SetStatus(AdministratorManager.eUisngAdminStatus.casting);

        if (bReallyBegin)
        {
            m_dateSkillStartTime = Main.GetSystemTime();
        }
  
        // m_nDurationLeftTime = m_Config.nDuration;
        float fRealValue = m_Config.nDuration;
        ScienceLeaf leaf = ScienceTree.s_Instance.GetLeaf(ScienceTree.eBranchType.branch2, 5);
        if (leaf.GetLevel() > 0)
        {
            fRealValue *= (1f + leaf.GetFloatParam(0));
        }
        m_nRealDuration = (int)fRealValue;

        if ( GetBoundTrack() != MapManager.s_Instance.GetCurDistrict() )
        {
            return;
        }

        switch ( m_Config.eType )
        {
            case AdministratorManager.eAdminFuncType.coin_raise:
                {
                    Main.s_Instance.UpdateRaise();
                }
                break;
            case AdministratorManager.eAdminFuncType.automobile_accelerate:
                {
                    Main.s_Instance.AccelerateAll( 1f + AdministratorManager.s_Instance.GetAutomobileSpeedAccelerate() /*m_Config.fValue*/ );
                }
                break;
            case AdministratorManager.eAdminFuncType.gain_immediately:
                {
                    ReCalculateDPSForGain();
                    // “立即获取一段时间的收益”改成持续技能了
                    /*
                    int nDps = (int)MapManager.s_Instance.GetCurDistrict().CalculateDPS();
                    int nGain = (int)( nDps * m_Config.fValue );
                    int nPlanetId = MapManager.s_Instance.GetCurPlanet().GetId();
                    int nCurCoin = AccountSystem.s_Instance.GetCoin(nPlanetId);
                    AccountSystem.s_Instance.SetCoin(nPlanetId, nCurCoin + nGain);
                    UIMsgBox.s_Instance.ShowMsg( nDps + " DPS x " + m_Config.fValue + " 秒 = " + nGain);
                    EndCastSkill(); // 瞬时技能，一发出就结束
                    */
                }
                break;
            case AdministratorManager.eAdminFuncType.reduce_automobile_price: // 载具打折
                {
                    m_BoundTrack.CalculateDropInfo();
                    if (m_BoundTrack.IsCurTrack())
                    {
                        //  AdministratorManager.s_Instance._imgPriceOff.gameObject.SetActive(true);
                        TanGeChe.s_Instance.BeginPriceOffAnimation();
                    }
                }
                break;
        } // end switch
    }

    float m_fTimeElaspe = 0;
    void SkillLoop()
    {
        m_fTimeElaspe += Time.deltaTime;
        if (m_fTimeElaspe < 1f) // 每秒轮询一次 
        {
            return;
        }
        m_fTimeElaspe = 0f;

        Casting_SkillLoop();
        ColdDown_SkillLoop();

    }

    public void Casting_SkillLoop()
    {
        if (!m_BoundTrack.IsCurTrack())
        {
            return;
        }

            if ( GetStatus() != AdministratorManager.eUisngAdminStatus.casting )
        {
            return;
        }

        // 时间跨度有两种算法，依据游戏逻辑选择合适的算法
        // 1、如果要求必须在线计时的，就用走帧的方法计时
        // 2、如果可以离线计时的，就用当前时间减去起始时间
        int nTimeSpan = (int)((Main.GetSystemTime() - m_dateSkillStartTime).TotalSeconds );

        switch( m_Config.eType )
        {
            case AdministratorManager.eAdminFuncType.gain_immediately:
                {
                    SkillLoop_Gain();
                }
                break;
        } // end switch

        m_nDurationLeftTime =/* m_Config.nDuration*/m_nRealDuration - nTimeSpan;

        if (m_BoundTrack.IsCurTrack())
        {
            m_BoundCounter._progressbarColdDown.SetPercent((float)m_nDurationLeftTime/*nTimeSpan*/ / (float)m_Config.nDuration);
            m_BoundCounter._progressbarColdDown.SetTextContent("施放中:" + CyberTreeMath.FormatTime( m_nDurationLeftTime ));
        }
        if (m_nDurationLeftTime <= 0)
        {
            EndCastSkill();
        }

    }

    void ReCalculateDPSForGain()
    {
        double nDps =  m_BoundTrack.GetDPS() ;//(int)MapManager.s_Instance.GetCurDistrict().GetDPS()/*CalculateDPS()*/;

        double fRealValue = m_Config.fValue;
        // 受科技树影响加成
        ScienceLeaf leaf = ScienceTree.s_Instance.GetLeaf(ScienceTree.eBranchType.branch2, 2);
        if (leaf.GetLevel() > 0)
        {
            fRealValue *= (1f + leaf.GetFloatParam(0));
        }

        // 注意这儿一定要用double型变量，千万不能用int型。因为本游戏的数值体系非常庞大，玩到后期数值远远高于int型的最大值，以至于出问题
        double nTotalGain = (nDps * fRealValue/*m_Config.fValue*/); 
        double nGainPerRound = nTotalGain / (double)m_Config.nDuration; // 在持续时间内，每秒获得多少
        SetDoubleParam(0, nGainPerRound);
    }

    float m_fSkillGainTimeElaspe = 0f;
    void SkillLoop_Gain()
    {
        m_fSkillGainTimeElaspe += 1f;
     
        if (m_fSkillGainTimeElaspe < AdministratorManager.s_Instance.m_fSkillGainInterval)
        {
            return;
        }
        m_fSkillGainTimeElaspe = 0;
        
        ReCalculateDPSForGain();

        double nGainOfThisRound = GetDoubleParam(0);
        nGainOfThisRound *= AdministratorManager.s_Instance.m_fSkillGainInterval;
        AccountSystem.s_Instance.ChangeCoin(1, nGainOfThisRound);

      

        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_bank);

        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        Main.s_Instance.CollectOfflineProfitAnimation(UIManager.s_Instance._goStart_Admin);
        /*
        for (int i = 0; i < 30; i++)
        {
            UIFlyingCoin coin = ResourceManager.s_Instance.NewFlyingCoin();
            coin.transform.SetParent(UIManager.s_Instance._containerFlyingCoins.transform);

            coin.transform.localScale = vecTempScale;
            coin.transform.localPosition = AdministratorManager.s_Instance.m_vecCoinFlyStartPos;

            coin.m_vecSession0End.x = UnityEngine.Random.Range(AdministratorManager.s_Instance.m_vecCoinFlyMiddlePos_Left.x, AdministratorManager.s_Instance.m_vecCoinFlyMiddlePos_Right.x);
            coin.m_vecSession0End.y = UnityEngine.Random.Range(AdministratorManager.s_Instance.m_vecCoinFlyMiddlePos_Left.y, AdministratorManager.s_Instance.m_vecCoinFlyMiddlePos_Left.y);

            coin.m_vecEndPos = AdministratorManager.s_Instance.m_vecCoinFlyEndPos;

            float sX = coin.m_vecSession0End.x - coin.transform.localPosition.x;
            float sY = coin.m_vecSession0End.y - coin.transform.localPosition.y;
            float t = UIManager.s_Instance.m_fSession0Time;
            t = UnityEngine.Random.Range(t, t * 2);
            coin.m_vecSession0_A.x = 2f * sX / (t * t);
            coin.m_vecSession0_A.y = 2f * sY / (t * t);

            sX = AdministratorManager.s_Instance.m_vecCoinFlyEndPos.x - coin.m_vecSession0End.x;
            sY = AdministratorManager.s_Instance.m_vecCoinFlyEndPos.y - coin.m_vecSession0End.y;
            t = UIManager.s_Instance.m_fSession1Time;
            t = UnityEngine.Random.Range(t, t * 2);
            coin.m_vecSession1_A.x = 2f * sX / (t * t);
            coin.m_vecSession1_A.y = 2f * sY / (t * t);


            coin.BeginFly();
        } // end for

        */

    }

    public int GetSkillCastingLeftTime()
    {
        return m_nDurationLeftTime;
    }

    public int GetSkillColdDownLeftTime()
    {
        return m_nColddownLeftTime;
    }

    public void ColdDown_SkillLoop()
    {
        if (GetStatus() != AdministratorManager.eUisngAdminStatus.colddown)
        {
            return;
        }

        int nTimeSpan = (int)(Main.GetSystemTime() - m_dateSkillStartTime).TotalSeconds;
        m_nColddownLeftTime = m_nRealDuration/*m_Config.nColddown*/ - nTimeSpan;
     //   int nColdDownLeftTime = m_Config.nColddown - nTimeSpan;
        m_fColddownPercent = (float)m_nColddownLeftTime / (float)m_Config.nColddown;

        if (m_BoundTrack.IsCurTrack())
        {
            m_BoundCounter._progressbarColdDown.SetPercent(m_fColddownPercent);
            m_BoundCounter._progressbarColdDown.SetTextContent("冷却中:" + CyberTreeMath.FormatTime( m_nColddownLeftTime ));
        }
        if (nTimeSpan >= m_Config.nColddown)
        {
            EndColdDown();
        }

    }

    void EndColdDown()
    {
        SetStatus(AdministratorManager.eUisngAdminStatus.idle);

        AdministratorManager.s_Instance.EndColdDown();
    }

    public float GetColdDownPercent()
    {
        return m_fColddownPercent; 
    }

    public void SetStartTime(System.DateTime dtStartTime)
    {
        m_dateSkillStartTime = dtStartTime;
    }

    public System.DateTime GetStartTime()
    {
        return m_dateSkillStartTime;
    }

    public void EndCastSkill( bool bMannual = false )
    {
        if ( GetStatus() != AdministratorManager.eUisngAdminStatus.casting ) // 不要重复执行
        {
            return;
        }



        // ColdDown的启动有两种时机：一种是在线状态下开始；另一种是处在离线状态，上线之后检测到技能已结束，进入ColdDown状态
        // 因此ColdDown的起始时间不能直接取当前系统时间，因为上线的时刻可能ColdDown已经经历过一段时间了s
        // 
        //  m_dateSkillStartTime = Main.GetSystemTime();
        if (bMannual)
        {
            m_dateSkillStartTime = Main.GetSystemTime();
        }
        else
        {
            m_dateSkillStartTime = m_dateSkillStartTime.AddSeconds(m_nRealDuration);
        }
        BeginColdDown();
        SetStatus(AdministratorManager.eUisngAdminStatus.colddown);



        switch( m_Config.eType )
        {
            case AdministratorManager.eAdminFuncType.coin_raise:
                {
                    Main.s_Instance.UpdateRaise();
                }
                break;
            case AdministratorManager.eAdminFuncType.automobile_accelerate:
                {
                    Main.s_Instance.StopAccelerateAll();
                }
                break;
            case AdministratorManager.eAdminFuncType.reduce_automobile_price: // 载具打折
                {
                    m_BoundTrack.CalculateDropInfo();
                    if (m_BoundTrack.IsCurTrack())
                    {
                        //  AdministratorManager.s_Instance._imgPriceOff.gameObject.SetActive(false);
                        TanGeChe.s_Instance.SetPriceOffPoPoVisible(false);
                    }
                }
                break;
        } // end switch


        AdministratorManager.s_Instance.EndCastSkill();
        ColdDown_SkillLoop();
        AdministratorManager.s_Instance.SaveAdminData();
    }

    public void BeginColdDown()
    {
        SetStatus(AdministratorManager.eUisngAdminStatus.colddown);
        AdministratorManager.s_Instance.BeginColdDown();

        float fRealValue = m_Config.nColddown;
        ScienceLeaf leaf = ScienceTree.s_Instance.GetLeaf(ScienceTree.eBranchType.branch2, 6);
        if (leaf.GetLevel() > 0)
        {
            fRealValue *= (1f - leaf.GetFloatParam(0));
        }
        m_nRealDuration = (int)fRealValue;


    }

    public void CancelCasting()
    {
        if (this.GetStatus() != AdministratorManager.eUisngAdminStatus.casting)
        {
            return;
        }

        this.EndCastSkill( true );
    }


    public void SetBuyPrice( double nValue )
    {
        m_nBuyPrice = nValue;
    }

    public double GetBuyPrice()
    {
        return m_nBuyPrice;
    }

    public void SetDoubleParam( int nIndex, double dValue )
    {
        m_aryDoubleParams[nIndex] = dValue;
    }

    public double GetDoubleParam(int nIndex)
    {
        return m_aryDoubleParams[nIndex];
    }


} // end class
