﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;

public class MapManager : MonoBehaviour {

    public Image _imgCPosCoinIcon;

    public Text _txtCurPlanetName;

    public GameObject _skeletonEffectCurTrack_Container;
    public SkeletonGraphic _skeletonEffectCurTrack;

    // block
    public Color m_colorCurTrack;
    public Color m_colorPlanetUnlockBlack;
    public Color m_colorBlackWhite;
    public Color m_colorOutlineNormal;
    public Color m_colorOutlineCurTrack;
    public Material m_matBlackWhite;

    public Sprite m_sprBlockBgCurTrack;
    public Sprite m_sprBlockBgNormal; // unlocked or Can Unlock
    public Sprite m_sprBlockBgCanNotUnlock;  // can not unlock


    public float m_fSlideTime;
    public float[] m_aryTheMainlandCenters;

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public static MapManager s_Instance = null;

    public string[] m_aryDistrictName;

    public Sprite m_sprBg_Locked_Left;
    public Sprite m_sprBg_Locked_Right;
    public Sprite m_sprBgUnlocked_Left;
    public Sprite m_sprBgUnlocked_Right;

    ///// 各种金币显示UI
    public MoneyCounter[] m_aryMoneyCounters;
    public Image[] m_aryVariousCoinIcons;

    /// end 各种金币显示UI


    /// <summary>
    ///  UI
    /// </summary>
    public Button _btnAdventure;


    public GameObject _containerZengShouCounters;
    public GameObject _containerRecycledCounter;
    public GameObject _panelZengShou;

    public GameObject _panelPanelDetails;
    public Text _txtPanelDetailstitle;
    public UIDistrict[] m_aryUIDistricts;
    public UIPlanet[] m_aryUIPlanets;

    public GameObject _panelUnlockDistrict;
    public Text _txtUnlockDistrictCost;
    public Image _imgUnlockTrackPriceIcon;
      
    public GameObject _panelUnlockPlanet;
    bool m_bUnlockPlanetVisible = false;

    public Text _txtUnlockPlanetCost;
    public Image _imgUnlockPlanetPriceIcon;

    public GameObject _panelEntering;

    public GameObject _containerPlanets;

    public GameObject _containerMainPlanets;
    public GameObject _containerOtherPlanets;

    public GameObject _panelScienceTree;


    public Text _txtCurPlanetAndTrack;
    public Text _txtCurTrackLevel;
    public GameObject[] m_aryPlanetTitle;


    // 离线收益相关
    public GameObject _subpanelBatCollectOffline;
    bool m_bCollectPanelVisible = false;

    public Text _txtBatOfflineGain;
    public Text _txtBatOfflineDetail;
    public Text _txtBatOfflineTitle;
    //// end UI


    public GameObject _goStarSky;
    public float m_fStarSkyMoveSpeed = 1f;

    public enum ePlanetId
    {
        cooper,
        silver,
        gold,
        activity,
    };

    public enum ePlanetStatus
    {
        unlocked,        // 已解锁
        can_unlock,      // 未解锁，当前可以解锁
        can_not_unlock,  // 未解锁，当前不能解锁
    };

    public enum eDistrictStatus
    {
        unlocked,        // 已解锁
        can_unlock,      // 未解锁，当前可以解锁
        can_not_unlock,  // 未解锁，当前不能解锁
    };

    public Planet[] m_aryPlanets;

    public Planet m_CurPlanet = null;
    public District m_CurDistrict = null;

    public int m_nCurShowPlanetDetialIdOnUI = 0;

    Dictionary<string, UIZengShouCounter> m_dicZengShouCounters = new Dictionary<string, UIZengShouCounter>();

    public Text[] m_aryCoin0Value;
    public Text[] m_aryCoin1Value;
    public Text[] m_aryCoin2Value;
    public Text[] m_aryDiamondValue;

    public Button _btnCurEnterButton = null;

    private void Awake()
    {
        s_Instance = this;
    }

    private void FixedUpdate()
    {
        if (ScienceTree.s_Instance == null)
        {
            return;
        }

        if ( ScienceTree.s_Instance.m_bShowingScienceTree )
        {
            return;
        }

        StarSkyMoveLoop();
        DoSliding();
        Dragging();
    }

    public void Init()
    {


    }

    public void LoadMyData_CurTrackPlanesData()
    {

        // 采取读档机制
        m_CurPlanet = null;// m_aryPlanets[0];
        m_CurDistrict = null;// m_CurPlanet.GetDistrictById(0);


            // 大陆解锁情况
        for (int i = 0; i < DataManager.s_Instance.MAX_PLANET_NUM; i++)
        {
            Planet planet = m_aryPlanets[i];

          


            double dUnlock = (int)DataManager.s_Instance.GetMyData("PlanetUnlock" + i);
            if (dUnlock == 1)
            {
                planet.DoUnlock();
              // Debug.Log("大陆已解锁：" + i);
            }


            string szData = DataManager.s_Instance.GetMyData_String("CurPlanetAndTrack");
            if (szData == DataManager.LOAD_MY_DATA_INVALID_STRING)
            {
                // 如果没有存档，则默认进入大陆0、赛道0
                EnterDistrict(0, 0);
                AccountSystem.s_Instance.InitMoney();
                AdventureManager_New.s_Instance.DoRefresh();
                continue;
            }
            string[] aryParmas = szData.Split('_');
            int nPlanetId = int.Parse(aryParmas[0]);
            int nTrackId = int.Parse(aryParmas[1]);

            //            Debug.Log("当前所处赛道:" + szData);


           
            ScienceTree.s_Instance.LoadData(); // 加载科技树的存档。一定要先加载，因为很多系统都会受科技树加成的影响


            // 赛道解锁情况
            District[] aryTracks = planet.GetDistrictsList();
            for (int j = 0; j < DataManager.s_Instance.MAX_TRACK_NUM_OF_PLANET; j++)
            {
                District track = aryTracks[j];
                dUnlock = (int)DataManager.s_Instance.GetMyData("TrackUnlock" + i + "_" + j);
                if (dUnlock == 1)
                {
                    track.DoUnlock();
                    //  Debug.Log("赛道已解锁：" + i + "_" + j);

                    int nTrackLotNum = Main.MIN_LOT_NUM;
                    string szTrackLotNum = DataManager.s_Instance.GetMyData_String("TrackLotNum" + i + "_" + j);
                    if ( !int.TryParse(szTrackLotNum, out nTrackLotNum) )
                    {
                        nTrackLotNum = Main.MIN_LOT_NUM;
                    }
                    track.SetCurLotNum(nTrackLotNum);


                    /*
                    // 读取其离线信息(如有)
                    string szOfflineKey = "Offline" + i + "_" + j;
                    string szOfflineData = DataManager.s_Instance.GetMyData_String(szOfflineKey);
                    if (DataManager.IsDataValid(szOfflineData))
                    {
                        //  Debug.Log( "读取到赛道" + szOfflineKey + "的离线信息：" + szOfflineData);
                        string[] aryOfflineData = szOfflineData.Split('_');
                        System.DateTime dtOfflineStartTime = System.DateTime.Parse(aryOfflineData[1]);
                        track.SetStartOfflineTime(dtOfflineStartTime);
                        track.SetOfflineDPS(double.Parse(aryOfflineData[0]));
                    }
                    */

                    // 读取“最后一次在线时刻及当时的DPS”
                    string szOnlineKey = "LastOnline" + i + "_" + j;
                    string szOnlineData = DataManager.s_Instance.GetMyData_String(szOnlineKey);
                    if (DataManager.IsDataValid(szOnlineData))
                    {
                     
                        string[] aryOnlineData = szOnlineData.Split('_');
                        System.DateTime dtLastOnlineTime = System.DateTime.Parse(aryOnlineData[1]);
                     
                        track.SetOfflineDPS(double.Parse(aryOnlineData[0]));
                        track.SetLastOnlineTime(dtLastOnlineTime);
                    }


                    if (nPlanetId == i && j == nTrackId)
                    {
                        EnterDistrict(nPlanetId, nTrackId, true);
                    }



                    // 在线DPS
                    string szDPSKey = "DPS" + i + "_" + j;
                    string szDPSData = DataManager.s_Instance.GetMyData_String(szDPSKey);
                    if (DataManager.IsDataValid(szDPSData))
                    {
                        track.SetDPS(double.Parse(szDPSData));

                    }

                    // 读取其重生信息
                    string szPrestigeKey = "Prestige" + i + "_" + j;
                    string szPrestigeData = DataManager.s_Instance.GetMyData_String(szPrestigeKey);
                    if (DataManager.IsDataValid(szPrestigeData))
                    {
                        int nPrestigeTime = int.Parse(szPrestigeData);
                        track.SetPrestigeTimes(nPrestigeTime);
                    }

                    // 读取赛道等级
                    string szTrackLevelKey = "TrackLevel" + i + "_" + j;
                    string szTrackLevelData = DataManager.s_Instance.GetMyData_String(szTrackLevelKey);
                    if (DataManager.IsDataValid(szTrackLevelData))
                    {
                        int nTrackLevel = int.Parse(szTrackLevelData);
                        track.UpdateTrackLevel(nTrackLevel);

                    }

                    // 该赛道上的载具已购买的次数 
                    string szBuyTimeKey = "BuyTimes" + i + "_" + j;
                    string szBuyTimesData = DataManager.s_Instance.GetMyData_String(szBuyTimeKey);
                    if (DataManager.IsDataValid(szBuyTimesData))
                    {
                        string[] aryBuyTimesData = szBuyTimesData.Split('_');
                        for (int k = 0; k < aryBuyTimesData.Length; k++)
                        {
                            if (aryBuyTimesData[k] == "")
                            {
                                continue;
                            }

                            string[] aryBuyTimesParams = aryBuyTimesData[k].Split(',');
                            int nLevel = int.Parse(aryBuyTimesParams[0]);
                            int nBuyTimes = int.Parse(aryBuyTimesParams[1]);
                            track.SetVehicleBuyTimeById(nLevel, nBuyTimes);
                        } // end for k
                    }

                    // 能源锁的情况
                    string szEnergyResearchKey = "EnergyResearch" + i + "_" + j;
                    string szEnergyResearchData = DataManager.s_Instance.GetMyData_String(szEnergyResearchKey);
                    if (DataManager.IsDataValid(szEnergyResearchData))
                    {
                        string[] aryEnergyResearchData = szEnergyResearchData.Split('_');
                        for (int k = 0; k < aryEnergyResearchData.Length; k++)
                        {
                            if (aryEnergyResearchData[k] == "")
                            {
                                continue;
                            }

                            int nPointer = 0;
                            string[] aryEnergyResearchParams = aryEnergyResearchData[k].Split(',');
                            int nLevel = int.Parse(aryEnergyResearchParams[nPointer++]);
                            ResearchManager.eResearchCounterStatus eStatus = (ResearchManager.eResearchCounterStatus)int.Parse(aryEnergyResearchParams[nPointer++]);
                            System.DateTime dt = System.DateTime.Parse(aryEnergyResearchParams[nPointer++]);
                            int nLeftTime = int.Parse(aryEnergyResearchParams[nPointer++]);
                            int nAdsLefTime = int.Parse(aryEnergyResearchParams[nPointer++]);
                            System.DateTime dtAds = System.DateTime.Parse(aryEnergyResearchParams[nPointer++]);
                            int nNoColdDownCount = int.Parse(aryEnergyResearchParams[nPointer++]);
                            track.SetResearchCounterInfo(nLevel, eStatus, dt, nLeftTime, nAdsLefTime, dtAds, nNoColdDownCount);


                        } // end for k


                    } //end  szEnergyResearchData


                    // 主管的情况
                    string szBuyAdminTimes = DataManager.s_Instance.GetMyData_String("BuyAdminTimes" + i + "_" + j);
                    int nBuyAdminTime = 0;
                    if ( !int.TryParse(szBuyAdminTimes, out nBuyAdminTime) )
                    {
                        nBuyAdminTime = 0;
                    }
                    track.SetCurBoughtAdminNum(nBuyAdminTime);

                    string szAdminKey = "Admin" + i + "_" + j;
                    string szAdminData = DataManager.s_Instance.GetMyData_String(szAdminKey);
                    if (DataManager.IsDataValid(szAdminData))
                    {
                        string[] aryAdminData = szAdminData.Split('_');
                        for (int k = 0; k < aryAdminData.Length; k++)
                        {
                            if (aryAdminData[k] == "")
                            {
                                continue;
                            }

                            string[] aryAdminParams = aryAdminData[k].Split(',');

                            int nPointer = 0;
                            int nId = int.Parse(aryAdminParams[nPointer++]);
                            int nUsing = int.Parse(aryAdminParams[nPointer++]);
                            AdministratorManager.eUisngAdminStatus eStatus = (AdministratorManager.eUisngAdminStatus)int.Parse(aryAdminParams[nPointer++]);
                            System.DateTime dtStartTime = System.DateTime.Parse(aryAdminParams[nPointer++]);

                            bool bUsing = (nUsing == 1);
                            Admin admin = AdministratorManager.s_Instance.DoBuy(track, nId, 0);
                            if (bUsing)
                            {
                                AdministratorManager.s_Instance.DoUse(admin);
                            }

                            if (eStatus == AdministratorManager.eUisngAdminStatus.casting)
                            {
                                admin.BeginCastSkill(false);
                                admin.SetStartTime(dtStartTime);

                            }
                            else if (eStatus == AdministratorManager.eUisngAdminStatus.colddown)
                            {
                                admin.BeginColdDown();
                                admin.SetStartTime(dtStartTime);
                            }
                        } // end for k
                    } //end  主管的情况


                    //// 广告时间
                    string szAdskey = "AdsTime" + i + "_" + j;
                    string szAdsData = DataManager.s_Instance.GetMyData_String(szAdskey);
                    if (DataManager.IsDataValid(szAdsData))
                    {
                        int nPointer = 0;
                        string[] aryData = szAdsData.Split( ',' );
                        int nRaiseTime = int.Parse(aryData[nPointer++]);
                        System.DateTime dtStartTime = System.DateTime.Parse(aryData[nPointer++]);
                        track.SetAdsRaiseTime(nRaiseTime);
                        track.SetAdsStartTime(dtStartTime);
                        if (nRaiseTime > 0)
                        {
                            track.ShowAdsTime();

                        }

                    }/// end 广告时间


                }// en if (dUnlock == 1)
            } // end for j

        } // end for i


        for (int i = 0; i < DataManager.s_Instance.MAX_PLANET_NUM; i++)
        {

            // 读取该大陆的金币数；如果没有发现存档，则从配置文件中读取初始值
            double dCoin = DataManager.s_Instance.GetMyData("Coin" + i);
            if (dCoin == DataManager.LOAD_MY_DATA_INVALID_VALUE)
            {
                DataManager.sPlanetConfig planet_config = DataManager.s_Instance.GetPlanetConfigById(i);
                AccountSystem.s_Instance.SetCoin(i, planet_config.dInitCoin);
            }
            else
            {
                AccountSystem.s_Instance.SetCoin(i, dCoin);
            }

            // 读取该大陆的技能点数；如果没有发现存档，则从配置文件中读取初始值
            double dSkillPoint = DataManager.s_Instance.GetMyData("SkillPoint" + i);
            if (dSkillPoint == DataManager.LOAD_MY_DATA_INVALID_VALUE)
            {
                DataManager.sPlanetConfig planet_config = DataManager.s_Instance.GetPlanetConfigById(i);
                ScienceTree.s_Instance.SetSkillPoint((ScienceTree.eBranchType)i, (int)planet_config.dInitSkillPoint);
            }
            else
            {
                ScienceTree.s_Instance.SetSkillPoint((ScienceTree.eBranchType)i, (int)dSkillPoint);
            }
        }




        /*
        string szData = DataManager.s_Instance.GetMyData_String("CurPlanetAndTrack");
        if ( szData == DataManager.LOAD_MY_DATA_INVALID_STRING )
        {
            // 如果没有存档，则默认进入大陆0、赛道0
            EnterDistrict(0, 0);
            AccountSystem.s_Instance.InitMoney();
            return;
        }
        string[] aryParmas = szData.Split( '_' );
        int nPlanetId = int.Parse(aryParmas[0]);
        int nTrackId = int.Parse(aryParmas[1]);

        Debug.Log( "当前所处赛道:" + szData);

       EnterDistrict(nPlanetId, nTrackId);
        */


        string szAdsTotalkey = "AdsTotalTime";
        string szAdsTotalData = DataManager.s_Instance.GetMyData_String(szAdsTotalkey);
        if (DataManager.IsDataValid(szAdsTotalData))
        {
            string[] aryAdsData = szAdsTotalData.Split( ',' );
            int nPointer = 0;
            float fAdsTotalTime = float.Parse(aryAdsData[nPointer++]);
            float fAdsRaise = float.Parse(aryAdsData[nPointer++]);
            System.DateTime dtTotalTimeStartTime = System.DateTime.Parse(aryAdsData[nPointer++]);
            AdsManager.s_Instance.SetTotalTimeStartTime(dtTotalTimeStartTime);
            AdsManager.s_Instance.SetTotalTime(fAdsTotalTime);
            AdsManager.s_Instance.SetAdsRaise(fAdsRaise);
            AdsManager.s_Instance.UpdateRealData();
        }

        // load item data
       
       // ItemSystem.s_Instance.LoadData();
        // end load item data

        //// 加载探险系统的存档
      //  AdventureManager_New.s_Instance.LaodData();
        /// end 加载探险系统的存档


        UpdateTrackInfo();
        AccountSystem.s_Instance.InitMoney();

   

        Main.s_Instance.Load( GetCurPlanet(), GetCurDistrict() );


        MapManager.s_Instance.GetCurDistrict().CalculateDropInfo();
        MapManager.s_Instance.GetCurDistrict().CalculateRecommendInfo();


       
    }



    // Use this for initialization
    void Start()
    {

     
    }

    public static bool IsTrackKeyValid(string szKey)
    {
        if (szKey == "")
        {
            return false;
        }
        return true;
    }

    public void NewZengShouCounter( int nPlanetId, int nDistrictId )
    {
        UIZengShouCounter zengshou_counter = ResourceManager.s_Instance.NewZengShouCounter().GetComponent<UIZengShouCounter>();
        //zengshou_counter.transform.SetParent(_containerZengShouCounters.transform);
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        zengshou_counter.transform.localScale = vecTempScale;
        zengshou_counter.SetPlanetIdAndDistrictId(nPlanetId, nDistrictId);

        m_dicZengShouCounters[nPlanetId + "_" + nDistrictId] = zengshou_counter;

     //   int nRate = ( nPlanetId + 1 ) * (nDistrictId + 1);

        float fRate = DataManager.s_Instance.GetTrackConfigById(nPlanetId, nDistrictId).fEarning;

        //zengshou_counter._txtDistrictRaise.text = "x" + fRate/*nRate*/;
    }
	
	// Update is called once per frame
	void Update () {
        PreEnterRaceLoop();

        UpdatePlanetInfoLoop();

        BigMapLoop();

        ClickProhibitLoop();
    }

    float m_fUpdatePlanetInfoLoopTimeElapse = 0;
    void UpdatePlanetInfoLoop()
    {
        if ( !m_bPlanetDetailPanelOpen )
        {
            return;
        }

        m_fUpdatePlanetInfoLoopTimeElapse += Time.deltaTime;
        if (m_fUpdatePlanetInfoLoopTimeElapse< 1f)
        {
            return;
        }
        m_fUpdatePlanetInfoLoopTimeElapse = 0;

       
    UpdateUIDistrictsInfo();
    }



    public void UpdateTrackInfo()
    {
        if (m_CurDistrict == null || m_CurPlanet == null)
        {
            return;
        }

        _txtCurTrackLevel.text = "当前赛道等级：" + m_CurDistrict.GetLevel();


        _txtCurPlanetAndTrack.text = "大陆:" + m_CurPlanet.GetId() + ", 赛道：" + (m_CurDistrict.GetId());

        AdministratorManager.s_Instance.UpdateAdminMainPanelInfo();

        Admin cur_admin = m_CurDistrict.GetCusUsingAdmin();
        if (cur_admin )
        {
            if (cur_admin.GetStatus() == AdministratorManager.eUisngAdminStatus.casting)
            {
                cur_admin.BeginCastSkill(false);
            }
        }

        List<UIShoppinAndItemCounter> lstUsingItem = ItemSystem.s_Instance.GetCurUsingItemsList();
        for (int i = 0; i < lstUsingItem.Count; i++)
        {

            UIShoppinAndItemCounter item = lstUsingItem[i];
            if (item.m_nItemType != (int)ShoppinMall.eItemType.automobile_accelerate)
            {
                continue;
            }

            Main.s_Instance.AccelerateAll(1f + item.m_fValue, 1);
        }
        /// end item
    }

    public void UnLockPlanet( int nPlanetId )
    {
        if ( nPlanetId == 0 )
        {
            //UIMsgBox.s_Instance.ShowMsg( "0号大陆是起始大陆，无需解锁" );
            return;
        }

        Planet planet_to_unlock = MapManager.s_Instance.GetPlanetById(nPlanetId);
        Planet planet_prev = MapManager.s_Instance.GetPlanetById(nPlanetId - 1);

       
        if (planet_prev.GetStatus() != ePlanetStatus.unlocked)
        {
            UIMsgBox.s_Instance.ShowMsg("先解锁上一级大陆");
            return;
        }

        if (!planet_prev.CheckIfAllDistrictsUnlocked())
        {
            UIMsgBox.s_Instance.ShowMsg("先解锁上一大陆所有赛道");
            return;
        }


        double nCoinCost = planet_to_unlock.GetUnlockCoinCost();
        double nCurTotalCoin = planet_prev.GetCoin();
        if (nCurTotalCoin < nCoinCost)
        {
            UIMsgBox.s_Instance.ShowMsg("上一级大陆的金币数量不足:" + CyberTreeMath.GetFormatMoney( nCurTotalCoin ) );
            OnClick_CloseUnlockPlanetPanel();
            return;
        }

        // 解锁
        // planet_prev.CostCoin(nCoinCost);
        double nCurCoin = AccountSystem.s_Instance.GetCoin(planet_prev.GetId());
        AccountSystem.s_Instance.SetCoin(planet_prev.GetId(), nCurCoin - nCoinCost);
        planet_to_unlock.DoUnlock();


        //UIMsgBox.s_Instance.ShowMsg( "大陆解锁成功" );
        MapManager.s_Instance.PreEnterRace(planet_to_unlock.GetId(), 0, false);

        // UIMsgBox.s_Instance.ShowMsg("解锁成功，正在进入...");
        UILoading.s_Instance.BeginProgress(1.5f);


        //_panelUnlockPlanet.SetActive( false );
        OnClick_CloseUnlockPlanetPanel();
        UpdateUIPlanetsInfo();

        //  AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_congratulations);
        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.JieSuoSaiDaoHuoDaLu);
    }

    public void UnLockDistrict(int nPlanetId, int nDistrictId)
    {
        Planet planet = GetPlanetById(nPlanetId);
        if ( planet.GetStatus() != ePlanetStatus.unlocked )
        {
            UIMsgBox.s_Instance.ShowMsg( "这个大陆都还没解锁，谈不上赛道解锁" );
            return;
        }

        District district_to_unlock = planet.GetDistrictById(nDistrictId);
        if (nDistrictId == 0 || district_to_unlock.GetStatus() == eDistrictStatus.unlocked)
        {
            UIMsgBox.s_Instance.ShowMsg( "该赛道已解锁，无需再做此操作" );
            return;
        }

        District district_prev = planet.GetDistrictById(nDistrictId - 1);
        if (district_prev.GetStatus() != eDistrictStatus.unlocked)
        {
            UIMsgBox.s_Instance.ShowMsg( "先解锁上一级赛道" );
            return;
        }


        double nPriceToUnlock = district_to_unlock.GetUnlockPrice();
        double nCurTotalCoin = planet.GetCoin();
        if (nCurTotalCoin < nPriceToUnlock)
        {
            UIMsgBox.s_Instance.ShowMsg( "本大陆金币数量不足");
            OnClick_CloseUnlockDistrictPanel();
            return;
        }

        // 正式解锁
        //planet.CostCoin(nPriceToUnlock);
        double nCurCoint = AccountSystem.s_Instance.GetCoin(planet.GetId());
        AccountSystem.s_Instance.SetCoin(planet.GetId(),  nCurCoint - nPriceToUnlock);
        district_to_unlock.DoUnlock();

       // UIMsgBox.s_Instance.ShowMsg( "赛道解锁成功" );


        // poppin test

        MapManager.s_Instance.PreEnterRace(planet.GetId(), nDistrictId, false);

        //   UIMsgBox.s_Instance.ShowMsg("解锁成功，正在进入...");
        UILoading.s_Instance.BeginProgress(1.5f);
        // end poppin test




        //_panelUnlockDistrict.SetActive(false);
        OnClick_CloseUnlockDistrictPanel();
        UpdateUIDistrictsInfo();


        //  AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_congratulations);
        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.JieSuoSaiDaoHuoDaLu);
    }

    public void DoSomethingWhenChangeTrack()
    {
        UnlockNewPlaneManager.s_Instance.Reset();
    }

    public void EnterDistrict(int nPlanetId, int nDistrictId, bool bNotLoadPlanes = false)
    {
//        Debug.Log( "enter distrcit:" + nPlanetId + "_" + nDistrictId);
      


        Planet planet = MapManager.s_Instance.GetPlanetById(nPlanetId);
        if (planet == null) 
        {
            Debug.LogError("planet == null");
            return;
        }
        if (planet.GetStatus() != MapManager.ePlanetStatus.unlocked)
        {
            Debug.Log("大陆" + (nPlanetId) + "尚未解锁");
            return;
        }

        Planet cur_planet = MapManager.s_Instance.GetCurPlanet();
        District cur_district = MapManager.s_Instance.GetCurDistrict();

        District district = planet.GetDistrictById(nDistrictId);
        if (district == null)
        {
            Debug.LogError("district == null");
        }

        if (district == cur_district)
        {
//            Debug.Log("已经在这个赛道中了");
            return;

        }

        if (district.GetStatus() != MapManager.eDistrictStatus.unlocked)
        {
            Debug.Log("大陆" + (nPlanetId) + "的赛道" + (nDistrictId) + "未解锁");
            return;
        }

        JTDHSceneManager.s_Instance.ChangeScene(nPlanetId, nDistrictId); // 执行切换场景

   //     MapManager.s_Instance.SaveCurDistrictData();


        m_CurPlanet = planet;

        // 离开的这个District变成离线状态
        if (m_CurDistrict)
        {
            m_CurDistrict.OffLine();
        }
        if (m_CurDistrict != district)
        {
            DoSomethingWhenChangeTrack();
        }

        m_CurDistrict = district;

        m_CurDistrict.ClearAllRunningPlane();

        m_CurDistrict.OnLine();


        if (!bNotLoadPlanes)
        {
            Main.s_Instance.Load(m_CurPlanet, m_CurDistrict);
        }

        Main.s_Instance._moneyCoin.SetValue(m_CurPlanet.GetCoin());
        // Main.s_Instance._txtPrestigeTimes.text = m_CurDistrict.GetPrestigeTimes().ToString();
        Main.s_Instance.SetPrestigeTimes(m_CurDistrict.GetPrestigeTimes());


        DebugInfo.s_Instance.SetPlanetAndDistrict(m_CurPlanet.GetId(), m_CurDistrict.GetId());

        Main.s_Instance.UpdateRaise();

        // poppin to do
        // Main.s_Instance.ShowCoin();

        SkillManager.s_Instance.UpdateSkillButtonsStatus();


        // 根据所处的大陆，UI上的金币图标显示本大陆独有的金币图标
        UpdateCoinIcon(m_CurPlanet.GetId());


        UpdateTrackInfo(  );

        Main.s_Instance.UpdateRaise();

        // 存档：当前所在的大陆和赛道
        DataManager.s_Instance.SaveMyData("CurPlanetAndTrack", m_CurPlanet.GetId() + "_" + m_CurDistrict.GetId());


        m_CurDistrict.SetTrackRaise( DataManager.s_Instance.GetTrackConfigById(m_CurPlanet.GetId(), m_CurDistrict.GetId()).fEarning );

        TanGeChe.s_Instance.LoadData( m_CurPlanet.GetId(), m_CurDistrict.GetId() );
        m_CurDistrict.CalculateDropInfo();

        Main.s_Instance._moneyTrigger._srMain.sprite = Main.s_Instance.m_aryMoneyTrigger[m_CurDistrict.GetId()];

        //    Admin cur_admin = m_CurDistrict.GetCurAdmin();
       double nDPS =  m_CurDistrict.CalculateDPS();
        Main.s_Instance. _txtDPS.text = CyberTreeMath.GetFormatMoney(nDPS) + "/秒";

        Main.s_Instance.UpdateLots( m_CurDistrict );

        AdsManager.s_Instance.MainLoop( true );
    }

   

    public void UpdateCoinIcon( int nPlanetId )
    {
        Sprite spr = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(nPlanetId);


        for (int i = 0; i < m_aryMoneyCounters.Length; i++ )
        {
            MoneyCounter counter = m_aryMoneyCounters[i];
            if (counter == null)
            {
                continue;
            }
            counter.SetIcon(spr);
        }

        for (int i = 0; i < m_aryVariousCoinIcons.Length; i++ )
        {
            Image imgIcon = m_aryVariousCoinIcons[i];
            if (imgIcon == null)
            {
                continue;
            }
            imgIcon.sprite = spr;
        }
        _imgCPosCoinIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(nPlanetId, true);


    }



    public Planet GetPlanetById(int nId)
    {
        if (nId < 0 || nId >= m_aryPlanets.Length)
        {
            return null;
        }

        return m_aryPlanets[nId];

    }

    public Planet GetCurPlanet()
    {
        return m_CurPlanet;
    }

    public District GetCurDistrict()
    {
        return m_CurDistrict;
    }

    public void SaveCurDistrictData()
    {
        if (m_CurDistrict == null)
        {
            return;
        }

        string szDistrictData = Main.s_Instance.GenerateData();
        m_CurDistrict.SetData(szDistrictData);


    }

    public Planet m_PlanetToPrestige = null;
    public District m_DistrictToPrestige = null;

    public void OnClick_Prestige()
    {
        if ( UIPrestige.s_Instance.m_bCoinNotEnough)
        {
            return
                ;
        }

        /*
        int nCurTotalCoin = m_CurPlanet.GetCoin();
        int nCost = DataManager.s_Instance.GetPrestigaeCoinCost(m_CurPlanet.GetId(), m_CurDistrict.GetId());
        if (nCurTotalCoin < nCost)
        {
            UIMsgBox.s_Instance.ShowMsg("金币不足");
            return;
        }

        m_CurPlanet.CostCoin(nCost);
        m_CurDistrict.DoPrestige(); // 注意，“重生”是针对赛道的，不是针对大陆的
        */
        DoPrestige(m_PlanetToPrestige, m_DistrictToPrestige);

        Main.s_Instance._panelPrestige.SetActive( false );

        Main.s_Instance.UpdateRaise();

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.ChongSheng);
    }

    public void DoPrestige( Planet planet, District district )
    {
        double nCurTotalCoin = planet.GetCoin();
        double nCost = UIPrestige.s_Instance.GetRealPrestigeCoinCost() ;//DataManager.s_Instance.GetPrestigaeCoinCost(planet.GetId(), district.GetId());
        if (nCurTotalCoin < nCost)
        {
            UIMsgBox.s_Instance.ShowMsg("金币不足");
            return;
        }

        //planet.CostCoin(nCost);
        double nCurCoin = AccountSystem.s_Instance.GetCoin(planet.GetId());
        AccountSystem.s_Instance.SetCoin(planet.GetId(), nCurCoin - nCost);
        district.DoPrestige(); // 注意，“重生”是针对赛道的，不是针对大陆的
    }

    bool m_bPlanetDetailPanelOpen = false;
    public void OpenPlanetDetailPanel( int nPlanetId )
    {
        m_bPlanetDetailPanelOpen = true;

        m_nCurShowPlanetDetialIdOnUI = nPlanetId;

        _panelPanelDetails.SetActive( true );
        Planet cur_planet = GetPlanetById( nPlanetId );
        _txtPanelDetailstitle.text = GetPlanetNameById(nPlanetId);

        UpdateUIDistrictsInfo();
    }

    public void UpdateUIPlanetsInfo()
    {
        for (int i = 0; i <  m_aryPlanets.Length; i++ )
        {
            UIPlanet ui_planet = m_aryUIPlanets[i]; UIMainLand ui_main_land = m_aryMainLands[i];

            Planet planet = m_aryPlanets[i];
            if ( /*i > 0*/true)
            {
                ui_planet.SetUnlockPrice( planet.GetUnlockCoinCost() ); ui_main_land.SetUnlockPrice(planet.GetUnlockCoinCost(), i);
                ui_planet._txtPlanetName.text = DataManager.s_Instance.GetPlanetConfigById( planet.GetId() ).szName;
                if ( planet.GetStatus() == ePlanetStatus.unlocked )
                {
                    ui_planet.SetUnlock( true );
                    ui_main_land.SetUnlock(true);
                }
                else
                {
                    ui_planet.SetUnlock(false);
                    ui_main_land.SetUnlock(false);
                }
            } // end i > 0
        } // end for
    }

    public void UpdateUIDistrictsInfo_New()
    {
      

        Planet cur_show_detail_planet = GetPlanetById(m_nCurShowPlanetDetialIdOnUI);

        UIMainLand uiMainLand = m_aryMainLands[m_nCurShowPlanetDetialIdOnUI];
        UIBigMapTrackInfo[] aryUITrackInfo = uiMainLand.m_aryUITrakcs;

        District[] aryDistrict = cur_show_detail_planet.GetDistrictsList();
        for (int i = 0; i < aryDistrict.Length; i++)
        {
            UIBigMapTrackInfo ui_track = aryUITrackInfo[i];

            District district = aryDistrict[i];
            UIDistrict ui_district = m_aryUIDistricts[i];
            ui_district.SetDistrict(district);
            ui_track.SetTrack(district);

            ui_track._starsPrestige.SetStarLevel(district.GetPrestigeTimes());

            if (true) 
            {
                bool bPrevDistrictUnlock = false;
                bool bThisDistrictUnlock = false;
                District district_prev = null;
                if ( i == 0 )
                {
                    bPrevDistrictUnlock = true;
                    bThisDistrictUnlock = true;
                }
                else{
                    district_prev = aryDistrict[i-1];
                    bPrevDistrictUnlock = (district_prev.GetStatus() == eDistrictStatus.unlocked);
                    bThisDistrictUnlock = (district.GetStatus() == eDistrictStatus.unlocked);

                }



                if (!bThisDistrictUnlock) // 本赛道尚未解锁
                {
                    ui_district.SetLocked(true, i);
                    ui_district.SetCanUnlock(bPrevDistrictUnlock);

                    ui_track.SetLocked(true, i);
                    ui_track.SetCanUnlock(bPrevDistrictUnlock);

                    ui_track._imgDataBg.gameObject.SetActive(bPrevDistrictUnlock);
                    ui_track._imgLock.gameObject.SetActive(bPrevDistrictUnlock);


                    if (bPrevDistrictUnlock)  // 上一级赛道已解锁，本赛道当前可解锁
                    {
                      
                         //  ui_track._block.SetColor( Color.white );
                         //   ui_track._block.SetMaterial( null );
                         //   ui_track._imgQuestionMark.gameObject.SetActive(false);


                       
                    }
                    else // 上一级赛道尚未解锁，以至于本赛道当前不可解锁
                    {
                    
                        //   ui_track._block.SetColor(m_colorBlackWhite);
                        //    ui_track._block.SetMaterial(m_matBlackWhite);
                        //    ui_track._imgQuestionMark.gameObject.SetActive(true);


                    }

                    if (cur_show_detail_planet.GetStatus() == ePlanetStatus.unlocked)
                    {
                        ui_track._block.SetColor(m_colorBlackWhite);
                        ui_track._block.SetMaterial(m_matBlackWhite);
                    }

                    ui_track._Content.SetActive( false );
                    ui_track._block.SetBgSpr(m_sprBlockBgCanNotUnlock);
                    ui_track._txtTrackName.gameObject.SetActive( false );

                }
                else // 本赛道已解锁
                {
                    ui_track._txtTrackName.gameObject.SetActive(true);
                    ui_track._block.SetBgSpr(m_sprBlockBgNormal);
                    ui_track._Content.SetActive(true);
                    ui_track._imgQuestionMark.gameObject.SetActive(false );

                    /*
                    if (ui_track._imgBlock)
                    {
                        ui_track._imgBlock.color = Color.white;
                        ui_track._imgBlock.material = null;
                    }
                    */

                    if (cur_show_detail_planet.GetStatus() == ePlanetStatus.unlocked)
                    {
                        ui_track._block.SetColor(Color.white);
                        ui_track._block.SetMaterial(null);
                    }

                    ui_district.SetLocked(false, i);
                    ui_district._txtUnlockPrice.gameObject.SetActive(false);
                    //    ui_district._goWenHao.SetActive(false);

                    ui_track.SetLocked(false, i);
                    ui_track._containerUnLockPrice.SetActive(false);

                    ui_track._imgDataBg.gameObject.SetActive(true);
                    ui_track._imgLock.gameObject.SetActive(false);

                }
                ui_district.m_bLocked = !bThisDistrictUnlock;
                ui_track.m_bLocked = !bThisDistrictUnlock;
            }

            bool bIsCurDistrict = MapManager.s_Instance.GetCurDistrict() == ui_track.GetTrack();//(i == GetCurDistrict().GetId() );
            ui_district._txtIsCurDistrict.gameObject.SetActive(bIsCurDistrict); ui_track._txtIsCurDistrict.gameObject.SetActive(bIsCurDistrict);
            ui_district._contaainerShouYi.gameObject.SetActive(!bIsCurDistrict); ui_district._contaainerShouYi.gameObject.SetActive(!bIsCurDistrict);

            ui_track.SetTextColorAndSize(bIsCurDistrict);

            if (!bIsCurDistrict) // 离线
            {
                float fTimeElaspe = (float)(Main.GetSystemTime() - district.GetLastOnlineTime()).TotalSeconds;
                double fCurOfflineGain = fTimeElaspe * district.GetOfflineDps();


                ui_track._txtAdsProfitLeftTime.text = fCurOfflineGain.ToString("f0"); 
                ui_track._txtCurOfflineGain.text = "离线收益        " + CyberTreeMath.GetFormatMoney(fCurOfflineGain);

                ui_track._subcontainerProfitAndTime.SetActive(true);

                ui_track._block._outLine.effectColor = m_colorOutlineNormal;
                ui_track._block.gameObject.SetActive( true );

               

            }
            else // 当前赛道
            {
                ui_track._subcontainerProfitAndTime.SetActive(false);
                ui_track._block._outLine.effectColor = m_colorOutlineCurTrack;
                ui_track._block.SetBgSpr( m_sprBlockBgCurTrack );
                /*
                _skeletonEffectCurTrack_Container.transform.SetParent(ui_track._containerEffect.transform);
                vecTempPos.x = 0f;
                vecTempPos.y = 0f;
                vecTempPos.z = 0f;
                _skeletonEffectCurTrack_Container.transform.localPosition = vecTempPos;
                ui_track._block.gameObject.SetActive(false);

            //    _skeletonEffectCurTrack.skeletonDataAsset.atlasAssets[0].materials[0].SetTexture("_MainTex", ui_track._block.GetSprite().texture);
                */
            }





            float fAdsLeftTime = district.GetAdsLeftTime();
            if (fAdsLeftTime > 0)
            {
                ui_district._txtTiSheng.text = "增益时间" + CyberTreeMath.FormatTime((int)fAdsLeftTime);
                ui_track._txtAdsProfitLeftTime.text = "增益时间 " + CyberTreeMath.FormatTime((int)fAdsLeftTime);
            }
            else
            {
                ui_district._txtTiSheng.text = "未提升";
                ui_track._txtAdsProfitLeftTime.text = "未提升";
            }

            ui_district._imgMoneyIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI);//ResourceManager.s_Instance.m_aryCoinIcon[m_nCurShowPlanetDetialIdOnUI];
            ui_district._imgMoneyIcon_ShouYi.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI);

            ui_district._imgMoneyIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI);//ResourceManager.s_Instance.m_aryCoinIcon[m_nCurShowPlanetDetialIdOnUI];
            ui_district._imgMoneyIcon_ShouYi.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI);


        } // end for

    }




    public void UpdateUIDistrictsInfo()
    {
 
            UpdateUIDistrictsInfo_New();
        return;
     

        Planet cur_show_detail_planet = GetPlanetById(m_nCurShowPlanetDetialIdOnUI);

        UIMainLand uiMainLand = m_aryMainLands[m_nCurShowPlanetDetialIdOnUI];
        UIBigMapTrackInfo[] aryUITrackInfo = uiMainLand.m_aryUITrakcs;

        District[] aryDistrict = cur_show_detail_planet.GetDistrictsList();
        for (int i = 0; i < aryDistrict.Length; i++)
        {
            UIBigMapTrackInfo ui_track = aryUITrackInfo[i];

            District district = aryDistrict[i];
            UIDistrict ui_district = m_aryUIDistricts[i];
            ui_district.SetDistrict(district);
            ui_track.SetTrack(district);

            ui_track._starsPrestige.SetStarLevel(district.GetPrestigeTimes());

            if (i > 0) // 从第二个赛道开始
            {
                District district_prev = aryDistrict[i - 1];
                bool bPrevDistrictUnlock = (district_prev.GetStatus() == eDistrictStatus.unlocked);
                bool bThisDistrictUnlock = (district.GetStatus() == eDistrictStatus.unlocked);

                if (!bThisDistrictUnlock) // 本赛道尚未解锁
                {
                    ui_district.SetLocked(true, i);
                    ui_district.SetCanUnlock(bPrevDistrictUnlock);

                    ui_track.SetLocked(true, i);
                    ui_track.SetCanUnlock(bPrevDistrictUnlock);

                    ui_track._imgDataBg.gameObject.SetActive(bPrevDistrictUnlock);
                    ui_track._imgLock.gameObject.SetActive(bPrevDistrictUnlock);

                    if (bPrevDistrictUnlock)
                    {
                        if (ui_track._imgBlock)
                        {
                            ui_track._imgBlock.color = Color.white;
                            ui_track._imgBlock.material = null;
                        }
                    }
                    else
                    {
                        if (ui_track._imgBlock)
                        {
                            ui_track._imgBlock.color = m_colorBlackWhite;
                            ui_track._imgBlock.material = m_matBlackWhite;
                        }
                    }


                }
                else // 本赛道已解锁
                {
                    if (ui_track._imgBlock)
                    {
                        ui_track._imgBlock.color = Color.white;
                        ui_track._imgBlock.material = null;
                    }


                    ui_district.SetLocked(false, i);
                    ui_district._txtUnlockPrice.gameObject.SetActive(false);
                    //    ui_district._goWenHao.SetActive(false);

                    ui_track.SetLocked(false, i);
                    ui_track._containerUnLockPrice.SetActive(false);

                    ui_track._imgDataBg.gameObject.SetActive(true);
                    ui_track._imgLock.gameObject.SetActive(false);

                }
                ui_district.m_bLocked = !bThisDistrictUnlock;
                ui_track.m_bLocked = !bThisDistrictUnlock;
            }

            bool bIsCurDistrict = MapManager.s_Instance.GetCurDistrict() == ui_track.GetTrack();//(i == GetCurDistrict().GetId() );
            ui_district._txtIsCurDistrict.gameObject.SetActive(bIsCurDistrict); ui_track._txtIsCurDistrict.gameObject.SetActive(bIsCurDistrict);
            ui_district._contaainerShouYi.gameObject.SetActive(!bIsCurDistrict); ui_district._contaainerShouYi.gameObject.SetActive(!bIsCurDistrict);
            if (!bIsCurDistrict) // 离线
            {
                float fTimeElaspe = (float)(Main.GetSystemTime() - district.GetStartOfflineTime()).TotalSeconds;
                double fCurOfflineGain = fTimeElaspe * district.GetOfflineDps();
               
                ui_district._txtShouYi.text = fCurOfflineGain.ToString( "f0" ); ui_track._txtCurOfflineGain.text = "获利      " + fCurOfflineGain.ToString("f0");
                ui_track._txtAdsProfitLeftTime.text = fCurOfflineGain.ToString("f0"); ui_track._txtCurOfflineGain.text = "离线收益      " + CyberTreeMath.GetFormatMoney( fCurOfflineGain );

                ui_track._subcontainerProfitAndTime.SetActive(true);
            }
            else // 当前赛道
            {
                ui_track._subcontainerProfitAndTime.SetActive(false);
            }





            float fAdsLeftTime = district.GetAdsLeftTime();
            if (fAdsLeftTime > 0)
            {
                ui_district._txtTiSheng.text = "增益时间" + CyberTreeMath.FormatTime( (int)fAdsLeftTime );
                ui_track._txtAdsProfitLeftTime.text = "增益时间 " + CyberTreeMath.FormatTime((int)fAdsLeftTime);
            }
            else
            {
                ui_district._txtTiSheng.text = "未提升";
                ui_track._txtAdsProfitLeftTime.text = "未提升";
            }

            ui_district._imgMoneyIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI) ;//ResourceManager.s_Instance.m_aryCoinIcon[m_nCurShowPlanetDetialIdOnUI];
            ui_district._imgMoneyIcon_ShouYi.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI);

            ui_district._imgMoneyIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI);//ResourceManager.s_Instance.m_aryCoinIcon[m_nCurShowPlanetDetialIdOnUI];
            ui_district._imgMoneyIcon_ShouYi.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI);


        } // end for

    } // end 

    public string GetPlanetNameById(int nPlanetId)
    {
        DataManager.sPlanetConfig config = DataManager.s_Instance.GetPlanetConfigById(nPlanetId);
        return config.szName;
        switch(nPlanetId)
        {
            case 0:
                {
                    return "青铜大陆";
                }
                break;
            case 1:
                {
                    return "白银大陆";
                }
                break;
            case 2:
                {
                    return "黄金大陆";
                }
                break;
        } // end switch

        return "";
    }

    public void OnClickButton_ClosePlanetDetailPanel()
    {
        _panelPanelDetails.SetActive( false );
        //  MapManager.s_Instance._subpanelBatCollectOffline.SetActive(false);
        MapManager.s_Instance.SetCollectPanelVisible(false);
        m_bPlanetDetailPanelOpen = false;
    }


    public void SetCollectPanelVisible( bool bVisible )
    {
        _subpanelBatCollectOffline.SetActive(bVisible);
        m_bCollectPanelVisible = bVisible;
    }

    public bool IsCollectPanelVisible()
    {
        return m_bCollectPanelVisible;
    }


    District m_districtToUnlock = null;
    bool m_bUnlockDistrictPanelVisible = false;
    public void PreUnLockDistrict( District district )
    {
        m_districtToUnlock = district;
        _panelUnlockDistrict.SetActive( true );
        _txtUnlockDistrictCost.text = CyberTreeMath.GetFormatMoney(district.GetUnlockPrice()) ;//district.GetUnlockPrice().ToString();
        _imgUnlockTrackPriceIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId( district.GetBoundPlanet().GetId() );

        m_bUnlockDistrictPanelVisible = true;
    }

    public void OnClick_CloseUnlockDistrictPanel()
    {
        _panelUnlockDistrict.SetActive( false );

        m_bUnlockDistrictPanelVisible = false;
    }

    public bool IsUnlockDistrictPanelVisible()
    {
        return m_bUnlockDistrictPanelVisible;
    }

    public void OnClick_DoUnlockDistrict()
    {
        UnLockDistrict(m_nCurShowPlanetDetialIdOnUI, m_districtToUnlock.GetId());

      
    }

    Planet m_planetToUnlock = null;
    public void PreUnlockPlanet(int nPlanetId)
    {
        /*s
        if (nPlanetId >= 2)
        {
            UIMsgBox.s_Instance.ShowMsg("本场景研发中，暂不开放.....");
            return;
        }
        */

        int nShit = 123;
        nShit++;
        Debug.Log( nShit );

        Planet planet_prev = GetPlanetById(nPlanetId - 1);
        if ( planet_prev.GetStatus() != ePlanetStatus.unlocked )
        {
            UIMsgBox.s_Instance.ShowMsg( "请先解锁上一级场景" );
            return;
        }

        if (!planet_prev.CheckIfAllDistrictsUnlocked())
        {
            UIMsgBox.s_Instance.ShowMsg("请先把上一级场景所有赛道解锁");
            return;
        }

        m_planetToUnlock = GetPlanetById(nPlanetId);
        _panelUnlockPlanet.SetActive( true );
        _txtUnlockPlanetCost.text = CyberTreeMath.GetFormatMoney(m_planetToUnlock.GetUnlockCoinCost());//m_planetToUnlock.GetUnlockCoinCost().ToString();

        _imgUnlockPlanetPriceIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_planetToUnlock.GetId() - 1);

        m_bUnlockPlanetVisible = true;
    }

    public bool IsUnlockPlanetVisible()
    {
        return m_bUnlockPlanetVisible;
    }

    public void OnClick_CloseUnlockPlanetPanel()
    {
        _panelUnlockPlanet.SetActive( false );

        m_bUnlockPlanetVisible = false;
    }

    public void OnClick_DoUnlockPlanet()
    {
        UnLockPlanet(m_planetToUnlock.GetId());

       
    }

    int m_nToEnterRace_PlanetId = 0;
    int m_nToEnterRace_DistrictId = 0;
    public void PreEnterRace( int nPlanetId, int nDistrictId, bool bCur = false )
    {
        m_nToEnterRace_PlanetId = nPlanetId;
        m_nToEnterRace_DistrictId = nDistrictId;

        if (bCur)
        {
            DoEnterTrack();
        }
        else
        {
            m_fPreEnterRaceLoopTime = 1f;
            _panelEntering.SetActive(true);
        }
    }

    float m_fPreEnterRaceLoopTime = 0;
    void PreEnterRaceLoop()
    {
        if (m_fPreEnterRaceLoopTime <= 0)
        {
            return;
        }
        m_fPreEnterRaceLoopTime -= Time.deltaTime;
        if (m_fPreEnterRaceLoopTime <= 0)
        {
            /*
            _panelEntering.SetActive(false);
            _panelPanelDetails.SetActive( false );
            Main.s_Instance.OnClickButton_CloseBigMap();
            EnterDistrict(m_nToEnterRace_PlanetId, m_nToEnterRace_DistrictId);
            */
            DoEnterTrack();
        }
    }

    public void DoEnterTrack()
    {
        _panelEntering.SetActive(false);
        _panelPanelDetails.SetActive(false);
        Main.s_Instance.OnClickButton_CloseBigMap();
        EnterDistrict(m_nToEnterRace_PlanetId, m_nToEnterRace_DistrictId);
    }

    public Planet[] GetPlanetList()
    {
        return m_aryPlanets;
    }

    bool m_bStarSkyMoving = false;
    public void StartStarSkyEffect()
    {
       // _goStarSky.SetActive( true );
        m_bStarSkyMoving = true;
    }

    public void StopStarSkyEffect()
    {
        _goStarSky.SetActive( false );
        m_bStarSkyMoving = false;

        Camera.main.transform.position = Vector3.zero;;
    }

    // 星空的移动 
    void StarSkyMoveLoop()
    {
        return;

        if (!m_bStarSkyMoving)
        {
            return;
        }

        vecTempPos = Camera.main.transform.position;
        vecTempPos.x -= m_fStarSkyMoveSpeed * Time.fixedDeltaTime;
        Camera.main.transform.position = vecTempPos;


        vecTempPos.x = -Input.gyro.attitude.y * 200f;
        vecTempPos.y = Input.gyro.attitude.x * 200f; ;
        vecTempPos.z = 0;
        _containerMainPlanets.transform.localPosition = vecTempPos;


        vecTempPos.x = -Input.gyro.attitude.y * 50f;
        vecTempPos.y = Input.gyro.attitude.x * 50f;
        vecTempPos.z = 0;
        _containerOtherPlanets.transform.localPosition = vecTempPos;

    }

    public bool IsShowingZengShouPanel()
    {
        return m_bIsShowingZengShouPanel;
    }

    public void OnClick_CloseZengShouPanel()
    {
        _panelZengShou.SetActive( false );
        m_bIsShowingZengShouPanel = false;
    }

    bool m_bIsShowingZengShouPanel = false;
    public void OnClick_OpenZengShouPanel()
    {
        m_bIsShowingZengShouPanel = true;
        _panelZengShou.SetActive(true);

        foreach (KeyValuePair<string, UIZengShouCounter> pair in m_dicZengShouCounters)
        {
            pair.Value.transform.SetParent(_containerRecycledCounter.transform);
        }

        for (int i = 0; i < m_aryPlanetTitle.Length; i++ )
        {
            m_aryPlanetTitle[i].transform.SetParent(_containerRecycledCounter.transform);
        }

 
        int nPlanetId = -1;

        foreach ( KeyValuePair<string, UIZengShouCounter> pair in m_dicZengShouCounters)
        {
            if (nPlanetId != pair.Value.GetPlanetId() )
            {
                nPlanetId = pair.Value.GetPlanetId();
                m_aryPlanetTitle[nPlanetId].transform.SetParent(_containerZengShouCounters.transform); 
            }
          
           
            pair.Value.transform.SetParent( _containerZengShouCounters.transform );
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            pair.Value.transform.localScale = vecTempScale;
        }

        UpdateZengShouCountersOfflineDps();

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.SaiDaoGuanLiAnNiu);
    }

    public void SetZengShouCounterPrestigeTimes( int nPlanetId, int nDistrictId, int nPrestigeTimes )
    {
        UIZengShouCounter counter = m_dicZengShouCounters[nPlanetId + "_" + nDistrictId];
        counter._txtCurPrestigeTimes.text = nPrestigeTimes.ToString();

        /*
        int nPresitageRaise = DataManager.s_Instance.GetPrestigeGain(nPlanetId, nDistrictId, nPrestigeTimes);
        if (nPresitageRaise == 0)
        {
            nPresitageRaise = 1;
        }
        counter._txtPrestigeRaise.text = "x" + nPresitageRaise;
        */

        string szKey = nPlanetId + "_" + nDistrictId;             if (nPrestigeTimes == 0)
        {
            DataManager.sTrackConfig track_config = DataManager.s_Instance.GetTrackConfigById(nPlanetId, nDistrictId);
            counter._txtDistrictRaise.text = (1 + track_config.fEarning).ToString("f1") + "倍";
        }
        else
        {
            DataManager.sPrestigeConfig config = DataManager.s_Instance.GetPrestigeConfig(szKey);
            float fPromote = config.aryCoinPromote[nPrestigeTimes - 1];
            counter._txtDistrictRaise.text = (1 + fPromote).ToString("f1") + "倍";
        }


    }

    public void UpdateZengShouCountersOfflineDps()
    {
        foreach( KeyValuePair<string, UIZengShouCounter> pair in m_dicZengShouCounters )
        {
            UIZengShouCounter counter = pair.Value;
            int nPlanetId = counter.GetPlanetId();
            int nDistrictId = counter.GetDistrictId();
            Planet planet = GetPlanetById(nPlanetId);
            District district = planet.GetDistrictById(nDistrictId);
        //    district.CalculateOfflineDps(); // 这里不能实时计算，因为当前只有一个赛道在线，其余赛道都不在线，无法计算，一计算反而把数据写坏了
            counter._txtOfflineDps.text = CyberTreeMath.GetFormatMoney( district.GetOfflineDps()) + "/秒";


           MapManager.s_Instance.SetZengShouCounterPrestigeTimes( nPlanetId, nDistrictId, district.GetPrestigeTimes());

        } // end foreach
    }

    public string GetDistrictNameByIndex( int nIndex )
    {
        return m_aryDistrictName[nIndex];
    }

    public void SetAllDiamondValueText( double nValue)
    {
        for (int i = 0; i < m_aryDiamondValue.Length; i++ )
        {
            Text txt = m_aryDiamondValue[i];
            if ( txt == null )
            {
                continue;
            }
            txt.text = CyberTreeMath.GetFormatMoney( nValue ) ;//nValue.ToString("f0");
        }
    }

    public void SetAllCoinValueText( int nPlanetId, double nValue )
    {
        Text[] aryCoinValue = null;
        if (nPlanetId == 0)
        {
            aryCoinValue = m_aryCoin0Value;
        }
        else if (nPlanetId == 1)
        {
            aryCoinValue = m_aryCoin1Value;
        }
        else if (nPlanetId == 2)
        {
            aryCoinValue = m_aryCoin2Value;
        }

        if (aryCoinValue == null)
        {
            return;
        }

        for (int i = 0; i < aryCoinValue.Length; i++ )
        {
            Text txt = aryCoinValue[i];
            if ( txt == null )
            {
                return;
            }

            txt.text = CyberTreeMath.GetFormatMoney(nValue  ) ;// nValue.ToString();
        }

    }

    public void CalculateAllPlanesSpeed()
    {
        m_CurDistrict.CalculateAllPlanesSpeed();
    }

    bool m_bTestShow = true;

    public void OnClick_Test()
    {
        _btnAdventure.gameObject.SetActive( !m_bTestShow);
    }

    public void OpenBigMap()
    {

        m_fStartDragPos = _goSVContent.transform.localPosition.x;
        m_fMouseStartPos = Input.mousePosition.x;
        m_bDragging = false;

        LocateTheCurPlanet( MapManager.s_Instance.GetCurPlanet().GetId() );

    }

    bool m_bSliding = false;
    public bool m_bBigMapShowing = false;
    float m_fSlideSpeed = 0;
    float m_fSlideA = 0;
    float m_fV0 = 0;
    public GameObject _goSVContent;
    float m_fDestPos = 0;
    float m_fStartDragPos = 0;
    float m_fEndDragPos = 0;
    bool m_bDragging = false;
    float m_fMouseStartPos = 0;
    void BigMapLoop()
    {
        bool bEndSliding = false;
        if (!m_bBigMapShowing)
        {
            bEndSliding = true;
        }

        if ( AdventureManager_New.s_Instance.IsShowingAdventurePanel() )
        {
            bEndSliding = true;
        }

        if (IsShowingZengShouPanel())
        {
            bEndSliding = true;
        }

        if (bEndSliding)
        {
            m_bDragging = false;
            EndSlide();
            return;
        }


        if ( Input.GetMouseButtonDown(0) )
        {
            if ( !m_bDragging)
            {
                m_fStartDragPos = _goSVContent.transform.localPosition.x;
                m_fMouseStartPos = Input.mousePosition.x;
            }
            m_bDragging = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
        

            m_bDragging = false;

            m_fEndDragPos = _goSVContent.transform.localPosition.x;
            bool bDragDirLeft = ( m_fEndDragPos < m_fStartDragPos );

            float fDragDelta = m_fEndDragPos - m_fStartDragPos ;
            bDragDirLeft = fDragDelta < 0;
            fDragDelta = Mathf.Abs(fDragDelta);//Debug.Log("mpouse up: " + fDragDelta);
           
            if (fDragDelta > 20)
            {
                BeginClickProhibit();
            }

            if (fDragDelta > 20)
            {
                BeginSlide(bDragDirLeft);
            }
      
        }

        if (m_bDragging)
        {
            if ( Mathf.Abs(Input.mousePosition.x - m_fMouseStartPos) > 10 )
            {
               
               
            }
        }


    }


    public bool m_bClickProhibit = false;
    float m_fClickProhibitTimeLeft = 0;
    void BeginClickProhibit()
    {
        m_bClickProhibit = true;
        m_fClickProhibitTimeLeft = 0.5f;

    }

    void ClickProhibitLoop()
    {
        if (m_fClickProhibitTimeLeft <= 0f || m_bClickProhibit == false)
        {
            return;
        }
      
        m_fClickProhibitTimeLeft -= Time.deltaTime;
        if (m_fClickProhibitTimeLeft <= 0)
        {
           m_bClickProhibit = false;
        }

    }


    void Dragging()
    {
        if ( !m_bDragging )
        {
            return;
        }

        if ( IsUnlockDistrictPanelVisible() || IsUnlockPlanetVisible() || IsCollectPanelVisible() )
        {
            return;
        }

        float fDesta = Input.mousePosition.x - m_fMouseStartPos;
        vecTempPos = _goSVContent.transform.localPosition;
        vecTempPos.x = m_fStartDragPos + fDesta;

        if (vecTempPos.x > 300)
        {
            vecTempPos.x = 300;

        }

        if (vecTempPos.x < -2600)
        {
            vecTempPos.x = -2600;

        }


        _goSVContent.transform.localPosition = vecTempPos;
    }

    public int m_nCurSelctedIndex = 0;
    public void BeginSlide( bool bDragDirLeft = true )
    {

        float fMinDis = 0;
        bool bFirst = true;

        float fRealDis = 0;
        int nDestIndex = 0;

        /*
        for (int i = 0; i < 3; i++)
        {
            float fDis = (m_aryTheMainlandCenters[i] - _goSVContent.transform.localPosition.x);
            float fAbsDis = Mathf.Abs( fDis );
            if (bFirst)
            {
                fMinDis = fAbsDis;
                bFirst = false;
                nDestIndex = i;
                continue;
            }
            if (fAbsDis < fMinDis)
            {
                fMinDis = fAbsDis;
                nDestIndex = i;
            }
        }
        */

        switch(m_nCurSelctedIndex)
        {
            case 0:
                {
                    if (bDragDirLeft)
                    {
                        nDestIndex = 1;
                    }
                    else
                    {
                        nDestIndex = 0;
                    }
                }
                break;

            case 1:
                {
                    if (bDragDirLeft)
                    {
                        nDestIndex = 2;
                    }
                    else
                    {
                        nDestIndex = 0;
                    }
                }
                break;

            case 2:
                {
                    if (bDragDirLeft)
                    {
                        nDestIndex = 2;
                    }
                    else
                    {
                        nDestIndex = 1;
                    }
                }
                break;
        } // end switch

        m_nCurSelctedIndex = nDestIndex;

        fRealDis = m_aryTheMainlandCenters[nDestIndex] - _goSVContent.transform.localPosition.x;
        m_fV0 = fRealDis / m_fSlideTime;
        m_bSliding = true;
        m_fDestPos = m_aryTheMainlandCenters[nDestIndex];


        MapManager.s_Instance.OpenPlanetDetailPanel(nDestIndex);
        _txtCurPlanetName.text = GetPlanetNameById(nDestIndex);

    }

    public void LocateTheCurPlanet( int nPlanetId )
    {
        _txtCurPlanetName.text = GetPlanetNameById(nPlanetId);

        vecTempPos = _goSVContent.transform.localPosition;
        vecTempPos.x = m_aryTheMainlandCenters[nPlanetId];
        _goSVContent.transform.localPosition = vecTempPos;
    }

    void DoSliding()
    {
        if ( !m_bSliding)
        {
            return;
        }
        vecTempPos = _goSVContent.transform.localPosition;
        vecTempPos.x += m_fV0 * Time.fixedDeltaTime;
        _goSVContent.transform.localPosition = vecTempPos;
//        Debug.Log( "cur=" + _goSVContent.transform.localPosition.x);

        bool bEnd = false;
        if ( m_fV0 > 0 )
        {
            if ( vecTempPos.x >= m_fDestPos )
            {
                bEnd = true;
            }
        }else if (m_fV0 < 0)
        {
            if (vecTempPos.x <= m_fDestPos)
            {
                bEnd = true;
            }
        }
        else
        {
            bEnd = true;
        }
        if ( bEnd )
        {
            EndSlide();
        }
    }

    void EndSlide()
    {
        m_bSliding = false;

    }

    public UIMainLand[] m_aryMainLands;

   

    public UIZengShouCounter GetZengShouCounter(int nPlanetId, int nTrackId)
    {
        return m_dicZengShouCounters[nPlanetId + "_" + nTrackId];
    }


} // end class
