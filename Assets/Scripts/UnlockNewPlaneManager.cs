﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;

public class UnlockNewPlaneManager : MonoBehaviour {

    public static UnlockNewPlaneManager s_Instance = null;

    public Text _txtTapToExit;
    public Button _btnClose;

    public SkeletonGraphic _skeTwoAuto;
    public SkeletonGraphic _skeNewAuto;
    public SkeletonGraphic _skeEffect1;
    public SkeletonGraphic _skeEffect2;

    public GameObject m_containerEffect;
    public GameObject m_containerLighning;
    public GameObject m_containerFoGuang;

    public GameObject m_preFoGuang;

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public GameObject _containerAll;

    public SpriteRenderer _srPlane1;
    public SpriteRenderer _srPlane2;
    public SpriteRenderer _srNewPlane;

    public float[] m_arySessionTime; 

    public Vector2 m_vecPlane1StartPos = new Vector2();
    public Vector2 m_vecPlane2StartPos= new Vector2();

    public float m_fCrashDistance = 0f;

    public float m_fBackSpeed = 0f;
    public float m_fBackDistance = 0;

    public float m_fCrashA = 0f;
    float m_fCrashSpeed = 0f;

   int m_nStatus = -1;

    public GameObject _goLight;

    public BaseScale _baseScaleNewPlane;

    public GameObject _containerExplosionEffect;

    public GameObject _effectFoGuang;

    public GameObject[] _aryContainerFirework;

    public Sprite m_sprTest;

    private void Awake()
    {
        s_Instance = this;
    }


    // Use this for initialization
    void Start () {

        m_fBackSpeed = m_fBackDistance / m_arySessionTime[1];

        float s = m_fCrashDistance;
        float t = m_arySessionTime[2];
        m_fCrashA = 2 * s / (t * t);

        //    _skeTwoAuto.AnimationState.SetAnimation(0, "xhc", false);
       




    }

    public void Reset()
    {
        m_nCurLevel = 1;
    }

    public void SetCurLevel( int nLevel )
    {
        m_nCurLevel = nLevel;
    }

    int m_nCurLevel = 1;

    void Loop_NewVersion()
    {
        if (m_nStatus == -1)
        {
            return;
        }

        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse > 5f)
        {
            End_NewVersion();
        }

        if ( m_fTimeElapse >= 2 )
        {
           
            _btnClose.gameObject.SetActive(true);
        }

        if ( m_nStatus == 0 )
        {

            if ( m_fTimeElapse >= 1.5f )
            {
                AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_congratulations);
                m_nStatus = 1;
            }
        }



    }

    public void OnClickButton_Exit()
    {
        _containerAll.SetActive(false);
    }

    public void End_NewVersion()
    {
        _containerAll.SetActive(false);
        m_nStatus = -1;
        /*
       
             if ( MapManager.s_Instance.GetCurDistrict().GetLevel() == 6 &&  TanGeChe.s_Instance.m_nFreshGuideStatus == 0)
            {
                // SetSpotlightSrcInfo(0.59f, 0.56f, 0.34f);
                // SetSpotlightMoveInfo(0.59f, 0.56f, 0.09f, 1f);
                FreshGuide.s_Instance.BeginMask(0.62f, 0.25f, 0.3f, 0.62f, 0.22f, 0.09f, 1f);



                FreshGuide.s_Instance._paw.SetIsUI(true);
                FreshGuide.s_Instance._paw.transform.SetParent(TanGeChe.s_Instance._goBtnTanGeChe.transform);
                FreshGuide.s_Instance._paw._basescalePaw.enabled = true;
                FreshGuide.s_Instance._paw.EndMove();
                vecTempPos.x = 30;
                vecTempPos.y = -100;
                vecTempPos.z = 0;
                FreshGuide.s_Instance._paw.transform.localPosition = vecTempPos;

                vecTempScale.x = 0.7f;
                vecTempScale.y = 0.7f;
                FreshGuide.s_Instance._paw.transform.localScale = vecTempScale;

                FreshGuide.s_Instance._faClick.transform.SetParent(TanGeChe.s_Instance._goBtnTanGeChe.transform);
                FreshGuide.s_Instance._faClick.transform.localPosition = FreshGuide.s_Instance._paw.transform.localPosition;
                FreshGuide.s_Instance._paw.SetAlpha(1f);
                vecTempScale.x = 0.8f;
                vecTempScale.y = 0.8f;
                FreshGuide.s_Instance._faClick.transform.localScale = vecTempScale;

            TanGeChe.s_Instance.m_nFreshGuideStatus = 1;

                DataManager.s_Instance.SaveMyData("EngeryGuide", TanGeChe.s_Instance.m_nFreshGuideStatus);
            }// end if (m_nFreshGuideStatus != 1)
        */
    }

    public void Begin( int nNewLevel )
    {
        _txtTapToExit.gameObject.SetActive(true);
        _btnClose.gameObject.SetActive(false);


        m_nStatus = 0;
        m_fTimeElapse = 0;

        _containerAll.SetActive(true );

        //AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_congratulations);
        _skeTwoAuto.AnimationState.SetAnimation(0, "xhc", false);
        _skeNewAuto.AnimationState.SetAnimation(0, "xhc", false);
        _skeEffect1.AnimationState.SetAnimation(0, "xhc", false);
        _skeEffect2.AnimationState.SetAnimation(0, "xhc", false);

        _skeTwoAuto.skeletonDataAsset.atlasAssets[0].materials[0].SetTexture("_MainTex",  ResourceManager.s_Instance.GetParkingPlaneSpriteByLevel(MapManager.s_Instance.GetCurDistrict().GetId(), nNewLevel - 1).texture );
        _skeNewAuto.skeletonDataAsset.atlasAssets[0].materials[0].SetTexture("_MainTex", ResourceManager.s_Instance.GetParkingPlaneSpriteByLevel(MapManager.s_Instance.GetCurDistrict().GetId(), nNewLevel).texture);

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.Great_Merge);

        /*

        int nOldLevel = nNewLevel - 1;
        _srPlane1.sprite = ResourceManager.s_Instance.GetParkingPlaneSpriteByLevel(nOldLevel);
        _srPlane2.sprite = _srPlane1.sprite;
        _srNewPlane.sprite = ResourceManager.s_Instance.GetParkingPlaneSpriteByLevel(nNewLevel);

        m_fTimeElapse = 0;
        _srPlane1.gameObject.SetActive( true );
        _srPlane2.gameObject.SetActive(true);
        _srPlane1.transform.localPosition = m_vecPlane1StartPos;
        _srPlane2.transform.localPosition = m_vecPlane2StartPos;
        _srNewPlane.gameObject.SetActive( false );
//        _goLight.SetActive( true );

        m_fMovement = 0;
        m_nStatus = 0;

        _containerAll.SetActive( true );

        UIManager.s_Instance.SetUiVisible( false );

        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_congratulations);
        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_greate_merge);


        //_effectFoGuang.SetActive(false );

        // GameObject goMerge = GameObject.Instantiate(m_preFoGuang);
        // goMerge.transform.SetParent(m_containerEffect.transform);

        m_containerLighning.SetActive( true );

        m_containerFoGuang.SetActive(false);

        */
    }

    public void End()
    {
        m_nStatus = -1;
        _containerAll.SetActive( false );
        UIManager.s_Instance.SetUiVisible( true);
    }

    void ClearSomething()
    {
        m_fMovement = 0;
        m_fTimeElapse = 0;
    }

    void SetStatus( int nStatus )
    {
        m_nStatus = nStatus;
        ClearSomething();
        m_nCount = 0;
    }

    List<GameObject> m_lstEffects = new List<GameObject>();

    float m_fTimeElapse = 0;
    float m_fMovement = 0f;
    private void FixedUpdate()
    {
        return;

        if ( Input.GetMouseButtonDown(0) )
        {
            End();
        }

        if ( m_nStatus == -1 )
        {
            return;
        }
       
        m_fTimeElapse += Time.fixedDeltaTime;

        if (m_nStatus == 0)
        {
            if (m_fTimeElapse >= m_arySessionTime[0])
            {
                SetStatus(1);

             //   _goLight.SetActive(false);
            }

        } // end 0
        else if (m_nStatus == 1)
        {

            float fMovement = m_fBackSpeed * Time.fixedDeltaTime;

            m_fMovement += fMovement;

            vecTempPos = _srPlane1.transform.localPosition;
            vecTempPos.x -= fMovement;
            _srPlane1.transform.localPosition = vecTempPos;

            vecTempPos = _srPlane2.transform.localPosition;
            vecTempPos.x += fMovement;
            _srPlane2.transform.localPosition = vecTempPos;

            if (m_fMovement >= m_fBackDistance)
            {
                SetStatus(2);
                m_containerLighning.SetActive(false);
            }


        } // end 1
        else if (m_nStatus == 2)
        {
            m_fTimeElapse += Time.fixedDeltaTime;
            if (m_fTimeElapse >= m_arySessionTime[2])
            {
                SetStatus(3);
            }
        }
        else if (m_nStatus == 3)
        {
            float fMovement = m_fCrashSpeed * Time.fixedDeltaTime;
            m_fMovement += fMovement;

            m_fCrashSpeed += m_fCrashA * Time.fixedDeltaTime;

            vecTempPos = _srPlane1.transform.localPosition;
            vecTempPos.x += fMovement;
            _srPlane1.transform.localPosition = vecTempPos;

            vecTempPos = _srPlane2.transform.localPosition;
            vecTempPos.x -= fMovement;
            _srPlane2.transform.localPosition = vecTempPos;

            if (m_fMovement >= m_fCrashDistance)
            {
                SetStatus(4);

                _srPlane1.gameObject.SetActive(false);
                _srPlane2.gameObject.SetActive(false);
                _srNewPlane.gameObject.SetActive(true);
                _baseScaleNewPlane.BeginScale();

                GameObject effect = EffectManager.s_Instance.NewEffect(EffectManager.eEffectType.unlock_new_plane_explosion);
                effect.transform.SetParent(_containerExplosionEffect.transform);
                vecTempPos.x = 0;
                vecTempPos.y = 0;
                vecTempPos.z = 0;
                effect.transform.localPosition = vecTempPos;

                m_lstEffects.Add(effect);

                m_containerFoGuang.SetActive( true );
            }
        }
        else if (m_nStatus == 4)
        {
            m_fTimeElapse += Time.fixedDeltaTime;
            if (m_fTimeElapse >= 1.5f)
            {
//                _effectFoGuang.SetActive(true);
                SetStatus(5);

                AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_firework);
            }
        }
        else if (m_nStatus == 5)
        {
            m_fTimeElapse += Time.fixedDeltaTime;
            if (m_fTimeElapse >= 0.5f)
            {
                m_fTimeElapse = 0;
               



                if ( m_nCount >= 6 )
                {
                    SetStatus(6);
                 
                }
                else
                {
                    int nIndex = m_nCount + (int)EffectManager.eEffectType.firework0;
                    GameObject effect = EffectManager.s_Instance.NewEffect((EffectManager.eEffectType)nIndex);
                    effect.transform.SetParent( _aryContainerFirework[m_nCount].transform );
                    m_nCount++;


                    vecTempPos.x = 0f;
                    vecTempPos.y = 0f;
                    vecTempPos.z = 0f;
                    effect.transform.localPosition = vecTempPos;

                    vecTempScale.x = 0.3f;
                    vecTempScale.y = 0.3f;
                    vecTempScale.z = 0.3f;
                    effect.transform.localScale = vecTempScale;
                }
              

            }
           
        }

        else if (m_nStatus == 6)
        {
            m_fTimeElapse += Time.fixedDeltaTime;

            if (m_fTimeElapse >= 4f)
            {
                End();
            }
        }
    }// end FixedUpdate

    int m_nCount = 0;

    // Update is called once per frame
    void Update () {

        Loop_NewVersion();




    }


} // end class
