﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
 

    public Sprite m_sprGreenCash;
    public Sprite[] m_aryCoinIcon_New;
    public Sprite[] m_aryCoinIcon_Special_New;

    public Sprite[] m_aryAutomobileParking_0;
    public Sprite[] m_aryAutomobileParking_1;
    public Sprite[] m_aryAutomobileParking_2;
    public Sprite[] m_aryAutomobileParking_3;
    public Sprite[] m_aryAutomobileParking_4;

    public Sprite[] m_aryAutomobileRunning_0;
    public Sprite[] m_aryAutomobileRunning_1;
    public Sprite[] m_aryAutomobileRunning_2;
    public Sprite[] m_aryAutomobileRunning_3;
    public Sprite[] m_aryAutomobileRunning_4;

    Dictionary<int, Sprite[]> m_dicAutoSpritesParking = new Dictionary<int, Sprite[]>();
    Dictionary<int, Sprite[]> m_dicAutoSpritesRunning = new Dictionary<int, Sprite[]>();


    public static ResourceManager s_Instance = null;
    public static Vector3 vecTempScale = new Vector3();

    public Font _font;

    public GameObject m_preEnviromentMask;

    public GameObject m_preUIShoppinAndItemCounter;

    public Sprite[] m_aryPlanetAvatar;

    public Sprite m_sprArrow;
    public Sprite[] m_arySkillPointIcon;
    public Sprite[] m_aryCoinIcon;

    public GameObject m_goRecycledPlanes;

    public Sprite[] m_aryPlaneSprites;
    public Sprite[] m_aryParkingPlaneSprites;

    public GameObject m_preZengShouCounter;

    public GameObject m_prePlane; // prefab of plane


    public GameObject m_preJinBi;
    public GameObject m_preRichTiaoZi;

    public GameObject m_preVehicleCounter;

    public Color[] m_aryTrailColor;

    public GameObject m_preSkill;

    public GameObject m_preFlyingCoin;

    /// <summary>
    /// 科技树 
    /// </summary>
    public GameObject m_preScienceTreeConfig;
    public GameObject m_preScienceLeaf;
    //// end 科技树


    public enum eItemIconType
    {
        diamond,     // 现金
        coin_raise,
    };

    public Sprite[] m_aryItemIcon;

    public Sprite[] m_aryCoinRaiseItemIcon;
    public Sprite[] m_aryCoinDiamondItemIcon;

    public Sprite m_sprWatchAdIcon;



    private void Awake()
    {
        s_Instance = this;


        m_dicAutoSpritesParking[0] = m_aryAutomobileParking_0;
        m_dicAutoSpritesParking[1] = m_aryAutomobileParking_1;
        m_dicAutoSpritesParking[2] = m_aryAutomobileParking_2;
        m_dicAutoSpritesParking[3] = m_aryAutomobileParking_3;
        m_dicAutoSpritesParking[4] = m_aryAutomobileParking_4;


        m_dicAutoSpritesRunning[0] = m_aryAutomobileRunning_0;
        m_dicAutoSpritesRunning[1] = m_aryAutomobileRunning_1;
        m_dicAutoSpritesRunning[2] = m_aryAutomobileRunning_2;
        m_dicAutoSpritesRunning[3] = m_aryAutomobileRunning_3;
        m_dicAutoSpritesRunning[4] = m_aryAutomobileRunning_4;
    }

    // Use this for initialization
    void Start () {

       

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public Sprite GetPlaneSpriteByLevel( int nTrackId,  int nLevel )
    {
        // poppin test
        if (nTrackId == 4)
        {
            nTrackId = 1;
        }
        Sprite[] arySprites = null;
        if ( !m_dicAutoSpritesRunning.TryGetValue(nTrackId, out arySprites) )
        {
            Debug.LogError("GetPlaneSpriteByLevel");
            return null;
        }



        int nSprIndex = nLevel - 1;
        if (nSprIndex >= arySprites.Length)
        {
            Debug.LogError("GetPlaneSpriteByLevel");
            return null;
        }

        return arySprites[nSprIndex];

       // return m_aryPlaneSprites[nLevel - 1];
    }

    public Sprite GetParkingPlaneSpriteByLevel( int nTrackId, int nLevel)
    {
        // poppin test
        if (nTrackId == 4)
        {
            nTrackId = 1;
        }

        Sprite[] arySprites = null;
        if (!m_dicAutoSpritesParking.TryGetValue(nTrackId, out arySprites))
        {
            Debug.LogError("GetParkingPlaneSpriteByLevel");
            return null;
        }



        int nSprIndex = nLevel - 1;
        if (nSprIndex >= arySprites.Length)
        {
            Debug.LogError("GetParkingPlaneSpriteByLevel");
            return null;
        }

        return arySprites[nSprIndex];

        // return m_aryParkingPlaneSprites[nLevel - 1];
    }

    static int s_TrailColorIndex = 0;

    List<Plane> m_lstRecycledPlanes = new List<Plane>();
    public Plane NewPlane()
    {
        Plane plane = null;

        if ( m_lstRecycledPlanes.Count > 0 )
        {
            plane = m_lstRecycledPlanes[0];
            plane.gameObject.SetActive( true );
            plane.Reset();
            m_lstRecycledPlanes.RemoveAt(0);
        }
        else
        {
            plane = GameObject.Instantiate(m_prePlane).GetComponent<Plane>();
        }

       
        if (s_TrailColorIndex >= m_aryTrailColor.Length)
        {
            s_TrailColorIndex = 0;
        }
        plane.SetTrailColor(m_aryTrailColor[s_TrailColorIndex++]);



        return plane;
    }

    public void DeletePlane( Plane plane )
    {
        plane.transform.SetParent(m_goRecycledPlanes.transform);
        plane.gameObject.SetActive( false );
        GameObject.Destroy( plane.gameObject );

        return;
        plane.gameObject.SetActive( false );
        m_lstRecycledPlanes.Add( plane );
     

    }

    List<GameObject> m_lstRecycledJinBi = new List<GameObject>();
    public GameObject NewJinBi()
    {
        GameObject goJinBi = null;
        if ( m_lstRecycledJinBi.Count > 0 )
        {
            goJinBi = m_lstRecycledJinBi[0];
            m_lstRecycledJinBi.RemoveAt(0);
            goJinBi.SetActive(true);
        }
        else
        {
            goJinBi = GameObject.Instantiate(m_preJinBi);
        }
        return goJinBi;
    }

    public void DeleteJinBi( GameObject goJinBi )
    {
        goJinBi.SetActive( false );
        m_lstRecycledJinBi.Add( goJinBi );
    }

    List<GameObject> m_lstRecycledRichTiaoZi = new List<GameObject>();
    public GameObject NewRichTiaoZi()
    {
        GameObject tiaozi = null;
        if ( m_lstRecycledRichTiaoZi.Count > 0 )
        {
            tiaozi = m_lstRecycledRichTiaoZi[0];
            m_lstRecycledRichTiaoZi.RemoveAt(0);
            tiaozi.SetActive( true );
        }
        else
        {
            tiaozi = GameObject.Instantiate(m_preRichTiaoZi);
        }
        return tiaozi;
    }

    public void DeleteRichTiaoZi(GameObject tiaozi)
    {
        tiaozi.SetActive( false );
        m_lstRecycledRichTiaoZi.Add( tiaozi);
    }

    ////// 商城道具相关
    public GameObject m_preUiItem;
    List<UIItemInBag> m_lstRecylcedUiItems = new List<UIItemInBag>();
    public GameObject _containerRecycledUIItems;
    public UIItemInBag NewUiItem()
    {
        UIItemInBag item = null;
        if (m_lstRecylcedUiItems.Count > 0)
        {
            item = m_lstRecylcedUiItems[0];
            item.gameObject.SetActive( true );
            m_lstRecylcedUiItems.RemoveAt(0);
        }
        else
        {
            item = GameObject.Instantiate(m_preUiItem).GetComponent<UIItemInBag>();
        }
        return item;
    }

    public void DeleteUiItem(UIItemInBag item)
    {
        item.gameObject.SetActive(false);
        m_lstRecylcedUiItems.Add( item );
        item.transform.SetParent(_containerRecycledUIItems.transform);
    }

    public const int MAX_VEHICLE_NUM = 13;

    public GameObject m_goRecycedVehicleCounter;
    public UIVehicleCounter NewVehicleCounter()
    {
        return GameObject.Instantiate( m_preVehicleCounter ).GetComponent<UIVehicleCounter>();
    }

    public void DeleteVehicleCounter(UIVehicleCounter counter  )
    {
        counter.transform.SetParent(m_goRecycedVehicleCounter.transform);
        GameObject.Destroy( counter.gameObject );
    }

    public GameObject m_goRecycedResearchCounter;



    ////// end 商城道具相关

    public Skill NewSkill()
    {
        return GameObject.Instantiate(m_preSkill).GetComponent<Skill>();
    }

    //// 科技树
    public ScienceTreeConfig NewTreeConfig()
    {
        return GameObject.Instantiate(m_preScienceTreeConfig).GetComponent<ScienceTreeConfig>();
    }

    public ScienceLeaf newScienceLeaf()
    {
        return GameObject.Instantiate(m_preScienceLeaf).GetComponent<ScienceLeaf>();
    }

    /// end 科技树
    List<UIFlyingCoin> m_lstRecycledFlyingCoin = new List<UIFlyingCoin>();
    public UIFlyingCoin NewFlyingCoin()
    {
        UIFlyingCoin coin = null;

        if (m_lstRecycledFlyingCoin.Count > 0)
        {
            coin = m_lstRecycledFlyingCoin[0];
            coin.gameObject.SetActive( true );
            m_lstRecycledFlyingCoin.RemoveAt(0);
        }
        else
        {
            coin = GameObject.Instantiate( m_preFlyingCoin ).GetComponent<UIFlyingCoin>();
        }

        return coin;
    }


    public void DeleteFlyingCoin(UIFlyingCoin coin )
    {
        coin.gameObject.SetActive( false )  ;
        m_lstRecycledFlyingCoin.Add( coin );
    }

    public Sprite GetMoneySpriteByMoneyType( DataManager.eMoneyType eType )
    {
        if (eType == DataManager.eMoneyType.diamond)
        {
            return GetGreenCashSprite();
        }
        else
        {

        }

        return null;
    }

    public Sprite GetCoinSpriteByPlanetId_New(int nPlanetId, bool bSpecial = false)
    {
        if (bSpecial)
        {
            return m_aryCoinIcon_Special_New[nPlanetId];
        }
        else
        {
            return m_aryCoinIcon_New[nPlanetId];
        }
    }

    public Sprite GetCoinSpriteByPlanetId( int nPlanetId, bool bSpecial = false )
    {
        return GetCoinSpriteByPlanetId_New(nPlanetId, bSpecial);


        int nMoneyTypeId = DataManager.s_Instance.PlanetId2CoinId(nPlanetId);
        return m_aryCoinIcon[nMoneyTypeId];
    }

    public Sprite GetGreenCashSprite()
    {
        return m_sprGreenCash;
    }

    public Sprite GetCoinSpriteByType( ShoppinMall.ePriceType eType, int nPlanetId = 0 )
    {
        Sprite spr = null;

        switch( eType )
        {
            case ShoppinMall.ePriceType.legal_tender:
                {
                    spr = m_aryCoinIcon[(int)DataManager.eMoneyType.legal_currency];
                }
                break;

            case ShoppinMall.ePriceType.green_cash:
                {
                    spr = m_aryCoinIcon[(int)DataManager.eMoneyType.diamond]; // 
                }
                break;

            case ShoppinMall.ePriceType.coin:
                {
                    spr =  GetCoinSpriteByPlanetId(nPlanetId);
                }
                break;

        } // end switch

        return spr;
    }

    public GameObject NewZengShouCounter()
    {
        return GameObject.Instantiate( m_preZengShouCounter );
    }

    public Sprite GetPlanetAvatarByIndex( int nIndex )
    {
        return m_aryPlanetAvatar[nIndex];
    }

    public Sprite GetItemIconByType( eItemIconType eType )
    {
        return m_aryItemIcon[(int)eType];
    }

    public UIShoppinAndItemCounter NewShoppinAndItemCounter()
    {
        return GameObject.Instantiate( m_preUIShoppinAndItemCounter ).GetComponent<UIShoppinAndItemCounter>();
    }

    public void DeleteShoppinAndItemCounter(UIShoppinAndItemCounter counter )
    {
        counter.transform.SetParent( ItemSystem.s_Instance._containerRecycledItems.transform );

        GameObject.Destroy(counter.gameObject);


    }

    public Sprite GetSkillPointSprite(int nPlanetId)
    {
        return m_arySkillPointIcon[nPlanetId];
    }

    public GameObject m_preTrail;
    public GameObject NewTrail()
    {
        return GameObject.Instantiate(m_preTrail);
    }

    public void DeleteTrail(GameObject trail)
    {
        GameObject.Destroy(trail);
    }



} // end class
