﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activity : MonoBehaviour
{
    public static Activity s_Instance = null;

    public float m_fWatchAdsBuyVehicleFreeInterval = 300f; // 2分钟
    float m_fWatchAdsBuyVehicleFreeTimeElaspe = 0f;

    private void Awake()
    {
        s_Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        string szData = DataManager.s_Instance.GetMyData_String ("LastWatchAdsBuyVehicleFreeTime");
        if ( DataManager.IsDataValid(szData) )
        {
            System.DateTime.TryParse( szData, out m_dtLastWatchAdsTime );
        }
    }

    // Update is called once per frame
    void Update()
    {
        WatchAdsBuyVehicleFreeLoop();
    }

    int m_nCurWatchAdsLevel = -1;
    int m_nCurPlanetId = -1;
    int m_nTrackId = -1;

    bool HasValidWatchAdsLevel()
    {
        return m_nCurWatchAdsLevel > 1;
    }

    // 看广告得载具免费
    void WatchAdsBuyVehicleFreeLoop()
    {
        m_fWatchAdsBuyVehicleFreeTimeElaspe += Time.deltaTime;
        if (m_fWatchAdsBuyVehicleFreeTimeElaspe < 30f) 
        {
            return;
        }
        m_fWatchAdsBuyVehicleFreeTimeElaspe = 0;
        float fWatchAdsTimeElapse = 0f;
        if ( !CanWatchAds( ref fWatchAdsTimeElapse))
        {
            return;
        }
      /*s
        if (HasValidWatchAdsLevel() && m_nCurPlanetId == MapManager.s_Instance.GetCurPlanet().GetId() && m_nTrackId == MapManager.s_Instance.GetCurDistrict().GetId())
        {
            return;
        }
        */


        TanGeChe.s_Instance.LoadData( MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId() );
        m_nCurWatchAdsLevel = TanGeChe.s_Instance.GetCurWatchAdsFreeCounterIndex();
        if (HasValidWatchAdsLevel()) // 有可以看广告免费买的载具了
        {
            TanGeChe.s_Instance.SetFreeFlagVisible( true );
            m_nCurPlanetId = MapManager.s_Instance.GetCurPlanet().GetId();
            m_nTrackId = MapManager.s_Instance.GetCurDistrict().GetId();
        }
        else
        {
            TanGeChe.s_Instance.SetFreeFlagVisible(false);
        }
    }

    public System.DateTime m_dtLastWatchAdsTime;
    public bool CanWatchAds( ref float fTimeElapse )
    {
        fTimeElapse = (float)(Main.GetSystemTime() - m_dtLastWatchAdsTime).TotalSeconds;
   //    Debug.Log("时间已过了：" + CyberTreeMath.FormatTime((int)fTimeElapse));
        return fTimeElapse > m_fWatchAdsBuyVehicleFreeInterval;
    }

    public void RecordLastWatchAdsFreeVehicleTime()
    {
        //MapManager.s_Instance.GetCurDistrict().SetLastWatchAdsBuyVehicleFreeTime( Main.GetSystemTime() );
        m_dtLastWatchAdsTime = Main.GetSystemTime();
        m_nCurWatchAdsLevel = -1;
        DataManager.s_Instance.SaveMyData("LastWatchAdsBuyVehicleFreeTime", m_dtLastWatchAdsTime.ToString());
    }

    public void SetLastWatchAdsFreeVehicleTime( System.DateTime dtLastTime )
    {
        m_dtLastWatchAdsTime = dtLastTime;
    }

} // end class
