﻿// 新手引导系统的“手掌”

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Paw : MonoBehaviour
{
    static Color tempColor = new Color();
    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public CFrameAnimationEffect _frameaniClick;
    public BaseScale _basescalePaw;

    public Image _imgPaw;
    public SpriteRenderer _srPaw;

    float m_fCurAlpha = 1f;
    bool m_bFading = false;

    bool m_bMoving = false;
    Vector3 m_vecStartPos = new Vector3();
    Vector3 m_vecEndPos = new Vector3();
    Vector3 m_vecMoveSpeed = new Vector3();
    float m_fWaitTime = 0;
    int m_nMoveStatus = 0;

    float m_fTimeElapse = 0;


    //定义一个委托
    public delegate void delegateMoveEnd();
    //定义一个事件
    public event delegateMoveEnd eventMoveEnd;


    // Start is called before the first frame update
    void Start()
    {
        _basescalePaw.eventScaleEnd += OnPawScaleEnd;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        Fading();
        Moving();
    }

    void OnPawScaleEnd()
    {
        _frameaniClick.BeginPlay(false);
    }

    public void SetIsUI( bool bUI )
    {
        _frameaniClick.m_bUI = bUI;
        if ( bUI )
        {
            _imgPaw.gameObject.SetActive( true );
            _srPaw.gameObject.SetActive(false);
        }
        else
        {
            _imgPaw.gameObject.SetActive(false);
            _srPaw.gameObject.SetActive(true);

        }
    }

    public void BeginFade()
    {
        m_bFading = true;
    }

    void Fading()
    {
        if ( !m_bFading )
        {
            return;
        }

        if ( m_fCurAlpha <= 0f )
        {
            EndFade();
            return;
        }

        m_fCurAlpha -= Time.fixedDeltaTime;
        SetAlpha(m_fCurAlpha);

    }

    public void EndFade()
    {
        m_bFading = false;
    }

    public void SetAlpha( float fAlpha )
    {
        m_fCurAlpha = fAlpha;

        tempColor = _srPaw.color;
        tempColor.a = m_fCurAlpha;
        _srPaw.color = tempColor;
        _imgPaw.color = tempColor;



    }

    public void BeginMove( Vector3 vecStartPos, Vector3 vecEndPos, float fMoveTime, float fWaittime, bool bLoop )
    {
        m_fTimeElapse = 0;
        m_bMoving = true;
        m_vecStartPos = vecStartPos;
        m_vecEndPos = vecEndPos;
        m_vecMoveSpeed = (vecEndPos - vecStartPos) / fMoveTime;
        m_fWaitTime = fWaittime;
        m_nMoveStatus = 0;

        this.transform.position = vecStartPos;
    }

    void Moving()
    {
        if ( !m_bMoving )
        {
            return;
        }

        if ( m_nMoveStatus == 0 ) // moving
        {
            vecTempPos = this.transform.position;
            vecTempPos += m_vecMoveSpeed * Time.fixedDeltaTime;
            this.transform.position = vecTempPos;
            bool bEndX = false;
            bool bEndY = false;
            if ( m_vecMoveSpeed.x > 0 )
            {
                if (vecTempPos.x >= m_vecEndPos.x)
                {
                    bEndX = true;
                }
            }else if (m_vecMoveSpeed.x < 0)
            {
                if (vecTempPos.x <= m_vecEndPos.x)
                {
                    bEndX = true;
                }
            }
            else
            {
                bEndX = true;
            }

            if (m_vecMoveSpeed.y > 0)
            {
                if (vecTempPos.y >= m_vecEndPos.y)
                {
                    bEndY = true;
                }
            }
            else if (m_vecMoveSpeed.y < 0)
            {
                if (vecTempPos.y <= m_vecEndPos.y)
                {
                    bEndY = true;
                }
            }
            else
            {
                bEndY = true;
            }

            if ( bEndX || bEndY )
            {
                m_nMoveStatus = 1;
                m_fTimeElapse = 0;
            }

        }
        else if (m_nMoveStatus == 1) // waiting
        {
            m_fTimeElapse += Time.fixedDeltaTime;
            if (m_fTimeElapse >= m_fWaitTime)
            {
                this.transform.position = m_vecStartPos;
                m_nMoveStatus = 0;

                if (eventMoveEnd != null)
                {
                    eventMoveEnd();
                }
            }
        }




    }

    public void EndMove()
    {
        m_bMoving = false;
    }

} // end class
