﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItemBagContainer : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public  GameObject m_containerTemp;
    public VerticalLayoutGroup _vlg;

    public float m_fHoriGap = 348;
    public float m_fVertGap = 524;

    List<UIShoppinAndItemCounter> m_lstItems = new List<UIShoppinAndItemCounter>();


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public UIShoppinAndItemCounter AddItem(int nId, int nNum = 1 )
    {
        for (int i = 0; i < m_lstItems.Count; i++ )
        {
            UIShoppinAndItemCounter node_item = m_lstItems[i];
            if (node_item.m_nItemId == nId)
            {
                node_item.SetNum(node_item.GetNum() + nNum);
                return node_item;
            }
        }

        return null;
    }

    public void AddItem( UIShoppinAndItemCounter the_item )
    {
        m_lstItems.Add(the_item);
        RefreshList();
    }

    public void RemoveOneItem(UIShoppinAndItemCounter item)
    {
        m_lstItems.Remove(item);

        RefreshList();
    }

    public List<UIShoppinAndItemCounter> GetItemsList()
    {
        return m_lstItems;
    }

    void RefreshList()
    {
        for (int i = 0; i < m_lstItems.Count; i++)
        {
            UIShoppinAndItemCounter counter = m_lstItems[i];
            counter.transform.SetParent( m_containerTemp.transform );
        } // end for i


        int nCount = 0;
        int nCurRowIndex = 0;
        int nCurColIndex = 0;
        for (int i = 0; i < m_lstItems.Count; i++ )
        {
            nCurRowIndex = nCount / 3;
            nCurColIndex = nCount % 3;

            UIShoppinAndItemCounter counter = m_lstItems[i];

            counter.transform.SetParent(this.transform);
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            counter.transform.localScale = vecTempScale;


            vecTempPos.x = nCurColIndex * m_fHoriGap;
            vecTempPos.y = -nCurRowIndex * m_fVertGap;
            vecTempPos.z = 0;
            counter.transform.localPosition = vecTempPos;


            nCount++;
        } // end for i



        int nShit = nCount / 3;
        if (nCount % 3 > 0)
        {
            nShit++;
        }
        _vlg.padding.bottom = 600 * nShit;
    }


} // end class
