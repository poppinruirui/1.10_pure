﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILoading : MonoBehaviour
{
    public static UILoading s_Instance = null;

    public GameObject _containerAll;
    public Image _imgProgressBar;

    float m_fTotalTime = 0f;
    float m_fCurTime = 0f;
    bool m_bProgressing = false;

    private void Awake()
    {
        s_Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        Progressing();
    }

    public void BeginProgress( float fTotalTime )
    {
        _containerAll.SetActive( true );
        m_bProgressing = true;

        m_fTotalTime = fTotalTime;
        m_fCurTime = 0;
    }

    void Progressing()
    {
        if ( !m_bProgressing )
        {
            return;
        }

        float fPercent = m_fCurTime / m_fTotalTime;
        _imgProgressBar.fillAmount = fPercent;

        m_fCurTime += Time.fixedDeltaTime;
        if (m_fCurTime >= m_fTotalTime)
        {
            EndProgress();
        }
    }

    void EndProgress()
    {
        m_bProgressing = false;
        _containerAll.SetActive(false);
    }

} // end class
