﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFlyingCoin : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();

    public Vector3 m_vecStartPos = new Vector3();
    public Vector3 m_vecEndPos = new Vector3();
    public float m_fSession0_Time = 0f;
    public float m_fSession1_Time = 0f;
    public Vector2 m_vecSession0End = new Vector2();

    public Vector2 m_vecSession0_A = new Vector2();
    public Vector2 m_vecSession1_A = new Vector2();
    public Vector2 m_vecVelocity = new Vector2();

    int m_nStatus = -1;

    static int s_ShitCount = 0;

    public Image _imgMain;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetSpr( Sprite spr )
    {
        _imgMain.sprite = spr;
    }

    private void FixedUpdate()
    {
        Flying();
    }

    public void BeginFly()
    {
        m_nStatus = 0;
        m_vecVelocity.x = 0f;
        m_vecVelocity.y = 0f;
    }

    void Flying()
    {
        if (m_nStatus < 0)
        {
            return;
        }

        if ( m_nStatus == 0 )
        {

            m_vecVelocity.x += m_vecSession0_A.x * Time.fixedDeltaTime;
            m_vecVelocity.y += m_vecSession0_A.y * Time.fixedDeltaTime;

            vecTempPos = GetPos();

            vecTempPos.x += m_vecVelocity.x * Time.fixedDeltaTime;
            vecTempPos.y += m_vecVelocity.y * Time.fixedDeltaTime;
            vecTempPos.z = 0;
            SetPos(vecTempPos);

            bool bEndX = false;
            bool bEndY = false;

            if (m_vecVelocity.x > 0)
            {
                if ( vecTempPos.x >= m_vecSession0End.x )
                {
                    bEndX = true;
                }
            }
            else if (m_vecVelocity.x < 0)
            {
                if (vecTempPos.x <= m_vecSession0End.x)
                {
                    bEndX = true;
                }
            }
            else
            {
                bEndX = true;
            }

            if (m_vecVelocity.y > 0)
            {
                if (vecTempPos.y >= m_vecSession0End.y)
                {
                    bEndY = true;
                }
            }
            else if (m_vecVelocity.y < 0)
            {
                if (vecTempPos.y <= m_vecSession0End.y)
                {
                    bEndY = true;
                }
            }
            else
            {
                bEndY = true;
            }

            if ( bEndX && bEndY )
            {
                m_nStatus = 1;

                m_vecVelocity.x = 0f;
                m_vecVelocity.y = 0f;
            }

        } // end if ( m_nStatus == 0 )
        else if ( m_nStatus == 1 )
        {
            m_vecVelocity.x += m_vecSession1_A.x * Time.fixedDeltaTime;
            m_vecVelocity.y += m_vecSession1_A.y * Time.fixedDeltaTime;

            vecTempPos = GetPos();

            vecTempPos.x += m_vecVelocity.x * Time.fixedDeltaTime;
            vecTempPos.y += m_vecVelocity.y * Time.fixedDeltaTime;
            vecTempPos.z = 0;

            bool bEndX = false;
            bool bEndY = false;

            if (m_vecVelocity.x > 0)
            {
                if (vecTempPos.x >= m_vecEndPos.x)
                {
                    bEndX = true;
                }
            }
            else if (m_vecVelocity.x < 0)
            {
                if (vecTempPos.x <= m_vecEndPos.x)
                {
                    bEndX = true;
                }
            }
            else
            {
                bEndX = true;
            }

            if (m_vecVelocity.y > 0)
            {
                if (vecTempPos.y >= m_vecEndPos.y)
                {
                    bEndY = true;
                }
            }
            else if (m_vecVelocity.y < 0)
            {
                if (vecTempPos.y <= m_vecEndPos.y)
                {
                    bEndY = true;
                }
            }
            else
            {
                bEndY = true;
            }

            if (bEndX || bEndY)
            {
                vecTempPos = m_vecEndPos;

                m_nStatus = -1;

                ResourceManager.s_Instance.DeleteFlyingCoin( this );

                Main.s_Instance._basescaleCPosCoinIcon.BeginScale();

                s_ShitCount++;
                if (s_ShitCount > 4 )
                {
                    AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_small_coin);
                    s_ShitCount = 0;
                }
            
            }

            SetPos(vecTempPos);


        } // end if ( m_nStatus == 1 )

    }

    public Vector3 GetPos()
    {
        return this.transform.localPosition;
    }

    public void SetPos( Vector3 pos )
    {
        this.transform.localPosition = pos;
    }

} // end class
