﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPriceOffPoPo : MonoBehaviour
{

    public Text _txtDiscount;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetText( string szContent )
    {
        _txtDiscount.text = szContent;
    }

} // end class
