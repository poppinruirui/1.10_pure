﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIZengShouCounter : MonoBehaviour {

    /// <summary>
    /// UI
    /// </summary>
    public Text _txtPlanetAndDistrict;
    public Image _imgPlanetAvatar;
    public Text _txtPrestigeRaise; // 重生加成
    public Text _txtDistrictRaise; // 赛道加成

    public Image _imgCoinIcon;

    public Text _txtOfflineDps;
    public Text _txtCurPrestigeTimes;
    // end UI

    int m_nPlanetId = 0;
    int m_nDistrictId = 0;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int GetPlanetId()
    {
        return m_nPlanetId;
    }

    public int GetDistrictId()
    {
        return m_nDistrictId;
    }

    public void OnClick_ZengShou()
    {
        Main.s_Instance.OpenPrestigePanel_ZengShou( m_nPlanetId, m_nDistrictId );
    }

    public void SetPlanetIdAndDistrictId( int nPlanetId, int nDistrictId )
    {
        m_nPlanetId = nPlanetId;
        m_nDistrictId = nDistrictId;

        //  _imgPlanetAvatar.sprite = ResourceManager.s_Instance.GetPlanetAvatarByIndex(m_nPlanetId);

        DataManager.sPlanetConfig config = DataManager.s_Instance.GetPlanetConfigById(nPlanetId);

        switch(nPlanetId)
        {
            case 0:
                {
                    _txtPlanetAndDistrict.text = config.szName;
                }
                break;

            case 1:
                {
                    _txtPlanetAndDistrict.text = config.szName;
                }
                break;

            case 2:
                {
                    _txtPlanetAndDistrict.text = config.szName;
                }
                break;
        } // end switch

        _txtPlanetAndDistrict.text += "  ";

        DataManager.sTrackConfig track_config = DataManager.s_Instance.GetTrackConfigById(nPlanetId, m_nDistrictId);

        _txtPlanetAndDistrict.text = track_config.szName;//MapManager.s_Instance.GetDistrictNameByIndex(m_nDistrictId);

        _imgCoinIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId_New(nPlanetId);

    }


    
} // end class
