﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBigMapTrackInfo : MonoBehaviour {

    Vector3 vecTempScale = new Vector3();

    /// <summary>
    /// UI
    /// </summary>
    /// 
    public GameObject _containerEffect;

    public GameObject _goBlock;
    public Image _imgBlock_bg;
    public Image _imgBlock_Main;
    public GameObject _containerMainContent;
   
    public Image _imgBlock;
    public UIBlock _block;
    public GameObject _Content;
    public Image _imgQuestionMark;
    public GameObject _containerUnlockPrice; 

    public GameObject _containerUnLockPrice;
    public GameObject _containerProfitAndTime;
    public GameObject _txtIsCurDistrict;
    public GameObject _subcontainerProfitAndTime;

    public UIStars _starsPrestige;
    public Text _txtTrackName;
    public Text _txtCurOfflineGain;
    public Text _txtAdsProfitLeftTime;
    public Text _txtIsCurTrack;
    public Button _imgLock;
    public Text _txtUnlockPrice;
    public Text _txtUnlockPriceTitle;
    public Image _imgUnlockCoinType;
    public Image _imgProfitCoinType;
    public Image _imgDataBg;

    public Button _btnEnter;
    // end ui

    District m_District = null;


    public MapManager.eDistrictStatus m_eStatus = MapManager.eDistrictStatus.unlocked;
    bool m_bCanUnlock = false;
    public bool m_bLocked = true;

    float m_fBtnEnterShowTime = 0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        DoOnClickMe();

        if (m_fBtnEnterShowTime <= 0)
        {
            return;
        }
        m_fBtnEnterShowTime -= Time.deltaTime;
        if (m_fBtnEnterShowTime <= 0f)
        {
            _btnEnter.gameObject.SetActive(false);
        }

       
    }

    public void SetTrack( District district )
    {
        m_District = district;

        DataManager.sTrackConfig config = DataManager.s_Instance.GetTrackConfigById(district.GetBoundPlanet().GetId(), district.GetId());
        SetUnlockPrice(config.nUnlockPrice  /*district.GetUnlockPrice()*/ );
        _txtTrackName.text = config.szName;

        Sprite sprCoinType = ResourceManager.s_Instance.GetCoinSpriteByPlanetId_New( district.GetBoundPlanet().GetId() );
        _imgProfitCoinType.sprite = sprCoinType;
        _imgUnlockCoinType.sprite = sprCoinType;
    }

    public District GetTrack()
    {
        return m_District;
    }

    public void SetUnlockPrice(double nPrice)
    {
        _txtUnlockPrice.text = CyberTreeMath.GetFormatMoney(nPrice);//nPrice.ToString("f0");
    }

    public void SetStatus(MapManager.eDistrictStatus eStatus)
    {
        m_eStatus = eStatus;
    }

    public MapManager.eDistrictStatus GetStatus()
    {
        return m_eStatus;
    }

    public void SetLocked(bool bLocked, int nCounterIndex)
    {
        _containerUnLockPrice.SetActive(bLocked);
        _containerProfitAndTime.SetActive( !bLocked);
       
    }

    public void SetCanUnlock(bool bCan)
    {
        m_bCanUnlock = bCan;

        _containerUnLockPrice.SetActive(m_bCanUnlock);
    }

    bool m_bClickMe = false;

    public void OnClickMe()
    {
        m_bClickMe = true;
    }

    public void DoOnClickMe()
    {
        if ( !m_bClickMe)
        {
            return;
        }

        if ( MapManager.s_Instance.m_bClickProhibit )
        {
            m_bClickMe = false;
            return;
        }

        m_bClickMe = false;


        if (m_bLocked)
        {
            if (m_bCanUnlock)
            {
                MapManager.s_Instance.PreUnLockDistrict(m_District);
            }
            else
            {
                UIMsgBox.s_Instance.ShowMsg("请先解锁上一级赛道");
            }
        }
        else
        {

            if ( MapManager.s_Instance._btnCurEnterButton )
            {
                MapManager.s_Instance._btnCurEnterButton.gameObject.SetActive(false);
            }
            MapManager.s_Instance._btnCurEnterButton = _btnEnter;
            // 直接进入该赛道
            //MapManager.s_Instance.PreEnterRace(MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI, m_District.GetId());
            _btnEnter.gameObject.SetActive( true );

            m_fBtnEnterShowTime = 2f;

           
        }
    }

    public void OnClick_Enter()
    {
        bool bCur = m_District == MapManager.s_Instance.GetCurDistrict();
        MapManager.s_Instance.PreEnterRace(MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI, m_District.GetId(), bCur);
        if (!bCur)
        {
            // UIMsgBox.s_Instance.ShowMsg("正在进入，请稍候...");
            UILoading.s_Instance.BeginProgress( 1.5f );
        }
    }

    public void SetPlanetUnlock( bool bUnlock )
    {
        if (bUnlock) // 星球已解锁
        {
            _containerMainContent.SetActive( true );
            _imgBlock_Main.color = Color.white;
            _imgBlock_bg.color = Color.white;
        }
        else // 星球未解锁
        {
            _containerMainContent.SetActive(false);
            _imgBlock_Main.color = MapManager.s_Instance.m_colorPlanetUnlockBlack;
            _imgBlock_bg.color = MapManager.s_Instance.m_colorPlanetUnlockBlack;
        }


    }

    public void SetTextColorAndSize(bool bCurTrack)
    {
        if (bCurTrack)
        {
            _txtTrackName.color = MapManager.s_Instance.m_colorCurTrack;
            _txtCurOfflineGain.color = MapManager.s_Instance.m_colorCurTrack;
            _txtAdsProfitLeftTime.color = MapManager.s_Instance.m_colorCurTrack;
            _txtIsCurTrack.color = MapManager.s_Instance.m_colorCurTrack;
            _txtUnlockPrice.color = MapManager.s_Instance.m_colorCurTrack;

            vecTempScale.x = 1.1f;
            vecTempScale.y = 1.1f;
            vecTempScale.z = 1f;
            _containerMainContent.transform.localScale = vecTempScale;

        }
        else
        {
            _txtTrackName.color = Color.white;
            _txtCurOfflineGain.color = Color.white;
            _txtAdsProfitLeftTime.color = Color.white;
            _txtIsCurTrack.color = Color.white;;
            _txtUnlockPrice.color = Color.white;

            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            _containerMainContent.transform.localScale = vecTempScale;
        }
    }

} // end class

