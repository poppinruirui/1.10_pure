﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirlineManager : MonoBehaviour
{
    public static AirlineManager s_Instance = null;
    static Vector3 vecTempPos = new Vector3();

    public Vector3 m_vecLeft;
    public Vector3 m_vecLeftTop;
    public Vector3 m_vecTop;
    public Vector3 m_vecRightTop;
    public Vector3 m_vecRight;
    public Vector3 m_vecRightBottom;
    public Vector3 m_vecBottom;
    public Vector3 m_vecLeftBottom;

    public Vector3 m_vecCenterLeftTop;
    public Vector3 m_vecCenterRightTop;
    public Vector3 m_vecCenterRightBottom;
    public Vector3 m_vecCenterLeftBottom;

    public GameObject[] m_aryKeyFramePoints;

    private void Awake()
    {
        s_Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        CalculateParams();

    }

    public void CalculateParams()
    {
        for (int i = 0; i < m_aryKeyFramePoints.Length; i++ )
        {
            JTDHSceneManager.s_Instance.DoAdjust(m_aryKeyFramePoints[i]);
        }

        Main.s_Instance.TURN_RADIUS = m_aryKeyFramePoints[(int)Main.ePosType.top].transform.position.x - m_aryKeyFramePoints[(int)Main.ePosType.left_top].transform.position.x;
        Main.s_Instance.LENGTH_OF_TURN = Main.s_Instance.TURN_RADIUS * 0.5f * Mathf.PI;
        float fStraightLineLen = m_aryKeyFramePoints[(int)Main.ePosType.right_top].transform.position.x - m_aryKeyFramePoints[(int)Main.ePosType.left_top].transform.position.x;
        Main.s_Instance.PERIMETER = 4f * Main.s_Instance.LENGTH_OF_TURN + 4f * fStraightLineLen;

        m_vecLeft = m_aryKeyFramePoints[(int)Main.ePosType.left].transform.position;
        m_vecLeftTop = m_aryKeyFramePoints[(int)Main.ePosType.left_top].transform.position;
        m_vecTop = m_aryKeyFramePoints[(int)Main.ePosType.top].transform.position;
        m_vecRightTop = m_aryKeyFramePoints[(int)Main.ePosType.right_top].transform.position;
        m_vecRight = m_aryKeyFramePoints[(int)Main.ePosType.right].transform.position;
        m_vecRightBottom = m_aryKeyFramePoints[(int)Main.ePosType.right_bottom].transform.position;
        m_vecBottom = m_aryKeyFramePoints[(int)Main.ePosType.bottom].transform.position;
        m_vecLeftBottom = m_aryKeyFramePoints[(int)Main.ePosType.left_bottom].transform.position;

        m_vecCenterLeftTop.x = m_vecTop.x;
        m_vecCenterLeftTop.y = m_vecLeftTop.y;

        m_vecCenterRightTop.x = m_vecRightTop.x;
        m_vecCenterRightTop.y = m_vecRight.y;

        m_vecCenterRightBottom.x = m_vecBottom.x;
        m_vecCenterRightBottom.y = m_vecRightBottom.y;


        m_vecCenterLeftBottom.x = m_vecLeftBottom.x;
        m_vecCenterLeftBottom.y = m_vecLeft.y;


    }

    public Main.ePosType CheckPosTypeByRealtimePos( Vector3 pos )
    {
        Main.ePosType e_pos_type = Main.ePosType.left;

        if ( pos.x <= m_vecTop.x && pos.y >= m_vecLeft.y && pos.y <= m_vecLeftTop.y )
        {
            e_pos_type = Main.ePosType.left;
        }
        else if (pos.x <= m_vecTop.x && pos.y >= m_vecLeftTop.y)
        {
            e_pos_type = Main.ePosType.left_top;

        }
        else if (pos.x >= m_vecTop.x && pos.x <= m_vecRightTop.x && pos.y >= m_vecLeftTop.y)
        {
            e_pos_type = Main.ePosType.top;
        }
        else if (pos.x >= m_vecRightTop.x  && pos.y >= m_vecRight.y)
        {
            e_pos_type = Main.ePosType.right_top;
        }
        else if (pos.x >= m_vecRightTop.x && pos.y <= m_vecRight.y && pos.y >= m_vecRightBottom.y )
        {
            e_pos_type = Main.ePosType.right;
        }
        else if (pos.x >= m_vecRightTop.x && pos.y <= m_vecRightBottom.y)
        {
            e_pos_type = Main.ePosType.right_bottom;

        }
        else if (pos.x >= m_vecLeftBottom.x && pos.x <= m_vecBottom.x && pos.y <= m_vecRightBottom.y)
        {
            e_pos_type = Main.ePosType.bottom;
        }
        else if (pos.x <= m_vecLeftBottom.x && pos.y <= m_vecLeft.y)
        {
            e_pos_type = Main.ePosType.left_bottom;
        }

        return e_pos_type;
    } // CheckPosTypeByRealtimePos

    public void CalculateAndSetStartMoveStatus( Plane plane, Vector3  pos, Main.ePosType pos_type)
    {
        float fAnglePercent = 0f;
        switch(pos_type)
        {
            case Main.ePosType.left:
                {
                    vecTempPos = m_vecLeft;
                    vecTempPos.y = pos.y;
                    plane.SetPos(vecTempPos);

                    plane.transform.localRotation = Quaternion.identity;
                 //  plane.transform.Rotate(0.0f, 0.0f, 180);
                }
                break; // end left

            case Main.ePosType.left_top:
                {
                    fAnglePercent = (pos.x - m_vecLeftTop.x) / Main.s_Instance.TURN_RADIUS;
                    plane.SetCurAngle(90f * fAnglePercent);

                }
                break; // end left-top
            case Main.ePosType.top:
                {
                    vecTempPos = m_vecTop;
                    vecTempPos.x = pos.x;
                    plane.SetPos(vecTempPos);

                    plane.transform.localRotation = Quaternion.identity;
                    plane.transform.Rotate(0.0f, 0.0f, -90);
                }
                break; // end left
            case Main.ePosType.right_top:
                {
                    fAnglePercent = ( pos.x - m_vecCenterRightTop.x) / Main.s_Instance.TURN_RADIUS;
                    plane.SetCurAngle(90f * fAnglePercent);

                }
                break; // end left-top

            case Main.ePosType.right:
                {
                    vecTempPos = m_vecRight;
                    vecTempPos.y = pos.y;
                    plane.SetPos(vecTempPos);

                    plane.transform.localRotation = Quaternion.identity;
                    plane.transform.Rotate(0.0f, 0.0f, 180);
                }
                break; // end right
            case Main.ePosType.right_bottom:
                {
                    fAnglePercent = ( m_vecRightBottom.x - pos.x) / Main.s_Instance.TURN_RADIUS;
                    plane.SetCurAngle(90f * fAnglePercent);

                }
                break; // end right-bottom
            case Main.ePosType.bottom:
                {
                    vecTempPos = m_vecBottom;
                    vecTempPos.x = pos.x;
                    plane.SetPos(vecTempPos);

                    plane.transform.localRotation = Quaternion.identity;
                    plane.transform.Rotate(0.0f, 0.0f, 90);
                }
                break; // end bottom
            case Main.ePosType.left_bottom:
                {
                    fAnglePercent = (m_vecCenterLeftBottom.x - pos.x ) / Main.s_Instance.TURN_RADIUS;
                    plane.SetCurAngle(90f * fAnglePercent);

                }
                break; // end left-bottom
        } // end switch


    } // end CalculateAndSetStartMoveStatus


    // Update is called once per frame
    void Update()
    {
        
    }




} // end class
