﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyTrigger : MonoBehaviour
{
    public SpriteRenderer _srMain;


    static Vector3 vecTempScale = new Vector3();
    static Vector3 vecTempPos = new Vector3();

    public Vector2 m_vecTiaoZiPos;

    public static MoneyTrigger s_Instance = null;

    public BaseScale _baseScale;
    public BaseScale _baseScaleCointCounter;


    public GameObject _goContainerJinBi;
    public GameObject _goContainerRichJinBi;

    int m_nJinBiNumToGenerate = 0;

    float m_fTimeElapse = 0f;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start()
    {

    }



    // Update is called once per frame
    void Update()
    {
        GenerateJinBi();
    }

    public void TriggerMoney(Plane plane)
    {
        if (plane.GetPlaneStatus() != Main.ePlaneStatus.running_on_airline)
        {
            return;
        }

        if ( plane.GetPreRun() )
        {
            return;
        }

        _baseScale.BeginScale();
        _baseScaleCointCounter.BeginScale();


        if (m_nJinBiNumToGenerate < 10)
        {
            m_nJinBiNumToGenerate += 10;
        }

        double nCoinGain = plane.GetCoinGainPerRound();

        nCoinGain *= Main.s_Instance.GetTotalRaise()/*GetRaise()*/;

       
        RichTiaoZi tiaoZi = ResourceManager.s_Instance.NewRichTiaoZi().GetComponent<RichTiaoZi>();
        tiaoZi.transform.SetParent(MoneyTrigger.s_Instance._goContainerRichJinBi.transform);

       tiaoZi.SetInitScale(8f);
        tiaoZi.SetMaxScale( 12f );
        tiaoZi.SetPos(m_vecTiaoZiPos);
        tiaoZi.SetStartPos(m_vecTiaoZiPos);
        tiaoZi.Begin();
        tiaoZi.SetValue(nCoinGain);
        tiaoZi.SetIcon( ResourceManager.s_Instance.GetCoinSpriteByPlanetId(MapManager.s_Instance.GetCurPlanet().GetId() ) );


#if UNITY_IOS
       MoreMountains.NiceVibrations.MMVibrationManager.Haptic(MoreMountains.NiceVibrations.HapticTypes.Selection);
#elif UNITY_ANDROID
        IntenseVibration.Instance.Vibrate(2L);
#endif

        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_trigger_money);

        // MapManager.s_Instance.GetCurPlanet().AddCoin((int)nCoinGain);
        MapManager.s_Instance.GetCurPlanet().SetCoin(MapManager.s_Instance.GetCurPlanet().GetCoin() + nCoinGain);


        if ( FreshGuide.s_Instance.GetGuidType() == FreshGuide.eGuideType.first_in_game && FreshGuide.s_Instance.GetStatus() == 4 )
        {
            FreshGuide.s_Instance.SetStatus(5);
        }
    }

    void GenerateJinBi()
    {
        if (m_nJinBiNumToGenerate <= 0f)
        {
            return;
        }

        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse < 0.05)
        {
            return;
        }
        m_fTimeElapse = 0f;



        TiaoZiJinBi jinBi = ResourceManager.s_Instance.NewJinBi().GetComponent<TiaoZiJinBi>();
        jinBi.transform.SetParent(MoneyTrigger.s_Instance._goContainerJinBi.transform);
        vecTempScale.x = 4f;
        vecTempScale.y = 4f;
        vecTempScale.z = 1f;
        jinBi.transform.localScale = vecTempScale;
        jinBi.Begin();
        jinBi.SetSpr(ResourceManager.s_Instance.GetCoinSpriteByPlanetId(MapManager.s_Instance.GetCurPlanet().GetId()));
        jinBi.SetPos( m_vecTiaoZiPos );

        m_nJinBiNumToGenerate--;
    }
}
