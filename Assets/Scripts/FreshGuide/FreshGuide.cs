﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FreshGuide : MonoBehaviour
{
    public const int MAX_GUIDE_TYPE = 8;
    public const string KEY_GUIDE = "Guide";

    public GameObject _containerScene;

    //// My UI
    public GameObject _goMask;
    bool m_bMaskVisible = false;

    public Text _txtX;
    public Text _txtY;
    public Text _txtRadius;
    public Text _txtPawX;
    public Text _txtPawY;
    public Material m_matMask;

    public float m_fX = 0.5f;
    public float m_fY = 0.5f;
    public float m_fRadius = 0.1f;

    // end My UI


    /// <summary>
    ///  other UI
    /// </summary>
    public GameObject _uiRecommend;

    /// <summary>
    /// other scene-obj
    /// </summary>
    public GameObject _lot0;
    public GameObject _lot1;
    public GameObject _lot2;
    public GameObject _lot3;


    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public static FreshGuide s_Instance = null;

    Dictionary<eGuideType, bool> m_dicDone = new Dictionary<eGuideType, bool>();

    public enum eGuideType
    {
        none,          // 当前无指引
        first_in_game, // 首次进游戏
    };

    public Paw _paw; // 手掌
    public CFrameAnimationEffect _faClick;

    eGuideType m_eCurType = eGuideType.none;
    int m_nStatus = 0;
    int m_nSubStatus = 0;
    int[] m_aryIntParams = new int[8];
    float[] m_aryFloatParams = new float[8];
    Dictionary<string, float> m_dicFloatParams = new Dictionary<string, float>();
    float m_fTimeElapse = 0f;
    public void SetDone( eGuideType eType )
    {
        m_dicDone[eType] = true;
        SaveData();
    }

    public bool GetDone(eGuideType eType)
    {

        bool bDone = false;
        if ( !m_dicDone.TryGetValue(eType, out bDone) )
        {
            return false;
        }
        return true;
    }

    public void SaveData()
    {
        foreach( KeyValuePair<eGuideType, bool> pair in m_dicDone )
        {
            int nDone = pair.Value ?1:0;
            DataManager.s_Instance.SaveMyData("Guide" + (int)pair.Key, nDone);
        }
    }

    private void Awake()
    {
        s_Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
      
        _paw.eventMoveEnd += OnPawMoveEnd_ClickEffectPlay;

        m_matMask.SetFloat( "_Alpha", 0 );
        _paw.gameObject.SetActive( false );
        _faClick.gameObject.SetActive(false );
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void FixedUpdate()
    {
        Loop_FirstInGame();
        MaskLoop();
    }


    public void OnClickButton_ToggleGuideMask()
    {
        m_bMaskVisible = !m_bMaskVisible;
        _goMask.SetActive(m_bMaskVisible)  ;
    }

    public int GetStatus()
    {
        return m_nStatus;
    }

    public void SetPawVisible( bool bVisible )
    {
        _paw.gameObject.SetActive( bVisible );
    }

    bool m_bMasking = false;
    public void BeginMask( float fStartPosX, float fStartPosY, float fStartRadius, float fEndPosX, float fEndPosY, float fEndRadius, float fTime )
    {

        _goMask.SetActive(true);
        _paw.gameObject.SetActive(true);
        _faClick.gameObject.SetActive(true);


        m_matMask.SetFloat("_Alpha", 1);
        SetSpotlightSrcInfo(fStartPosX, fStartPosY, fStartRadius);
        SetSpotlightMoveInfo(fEndPosX, fEndPosY, fEndRadius, fTime);
        // SetSpotlightSrcInfo(0.59f, 0.56f, 0.34f);
        // SetSpotlightMoveInfo(0.59f, 0.56f, 0.09f, 1f);

        m_bMasking = true;

        m_fTimeElapse = 0;
    }

    void MaskLoop()
    {
        if ( !m_bMasking)
        {
            return;
        }
        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse > 3f)
        {
            EndMask();
        }

        SpotLightAnimation();
    }

    public void EndMask()
    {
        m_bMasking = false;

        _goMask.SetActive( false );
        _paw.gameObject.SetActive( false );
        _faClick.gameObject.SetActive(false);
    }


    public void SetStatus( int nStatus )
    {
        m_nStatus = nStatus;
        m_nSubStatus = 0;

        if ( GetGuidType() == eGuideType.first_in_game )
        {
            switch( m_nStatus )
            {
                case 4:
                    {
                        _paw.BeginFade();
                    }
                    break;

                case 5:
                    {
                        _paw.SetAlpha( 1f );
                        _paw.transform.position = _lot0.transform.position;
                        _paw._basescalePaw.enabled = true;
                        _paw._basescalePaw.BeginScale();

                        m_matMask.SetFloat( "_Alpha", 1 );

                        /*
                        m_dicFloatParams["SpotLightDestPosX"] = 0.43f;
                        m_dicFloatParams["SpotLightDestPosY"] = 0.65f;
                        m_dicFloatParams["SpotLightDestRadius"] = 0.08f;

                        float fSpotLightMoveTime = 0.5f;
                        m_dicFloatParams["SpotLightMoveSpeedX"] = (m_dicFloatParams["SpotLightDestPosX"] - m_dicFloatParams["SpotLightPosX"]) / fSpotLightMoveTime;
                        m_dicFloatParams["SpotLightMoveSpeedY"] = (m_dicFloatParams["SpotLightDestPosY"] - m_dicFloatParams["SpotLightPosY"]) / fSpotLightMoveTime;
                        m_dicFloatParams["SpotLightRadiusChangeSpeed"] = (m_dicFloatParams["SpotLightDestRadius"] - m_dicFloatParams["SpotLightCurRadius"]) / fSpotLightMoveTime;
                        */
                        SetSpotlightSrcInfo(0.43f, 0.65f, 0.19f);
                        SetSpotlightMoveInfo(0.43f, 0.65f, 0.08f, 1f);

                    }
                    break;

                case 6:
                    {
                        SetSpotlightSrcInfo(0.56f, 0.53f, 0.19f);
                        SetSpotlightMoveInfo(0.56f, 0.53f, 0.08f, 1f);

                        Lot lot = Main.s_Instance.m_aryLots[3];
                        BoxManager.s_Instance.DoDrop(lot, 2);

                        _faClick.transform.position = lot.transform.position;
                        _paw.transform.position = lot.transform.position;
                    }
                    break;

                case 7:
                    {
                        SetSpotlightMoveInfo(0.49f, 0.59f, 0.14f, 1f);

                        _paw._basescalePaw.enabled = false;
                        _faClick.BeginPlay( false );
                        _paw.BeginMove( Main.s_Instance.m_aryLots[3].transform.position,
                                       Main.s_Instance.m_aryLots[0].transform.position,
                                        1f,
                                       0.3f,
                                       true
                                      
                                      
                                      );
                    }
                    break;

                case 8:
                    {
                        SetSpotlightMoveInfo(0.35f, 0.57f, 0.16f, 1f);
                        _paw.BeginMove(Main.s_Instance.m_aryLots[0].transform.position,
                                       Main.s_Instance._goStartPos.transform.position,
                                      1f,
                                     0.3f,
                                     true


                                    );


                    } // end status 8
                    break;

                case 9:
                    {
                        _paw.BeginFade();
                    }
                    break;

                case 10:
                    {

                        _faClick.transform.SetParent(TanGeChe.s_Instance._goBtnTanGeChe.transform);

                        TanGeChe.s_Instance.SetFreeFlagVisible(true);
                        _paw.SetIsUI( true );
                        _paw.transform.SetParent( TanGeChe.s_Instance._goBtnTanGeChe.transform );
                        _paw._basescalePaw.enabled = true;
                        _paw.EndMove();
                        vecTempPos.x = 30;
                        vecTempPos.y = -100;
                        vecTempPos.z = 0;
                        _paw.transform.localPosition = vecTempPos;

                        _faClick.transform.localPosition = _paw.transform.localPosition;
                        _paw.SetAlpha( 1f );
                        vecTempScale.x = 0.8f;
                        vecTempScale.y = 0.8f;
                        _paw.transform.localScale = vecTempScale;

                        m_matMask.SetFloat( "_Alpha", 1 );
                        SetSpotlightSrcInfo( 0.62f, 0.22f, 0.37f  );
                        SetSpotlightMoveInfo(0.62f, 0.22f, 0.09f, 1f);
                    }
                    break;

                case 11:
                    {
                        TanGeChe.s_Instance.SetFreeFlagVisible( false );
                        m_matMask.SetFloat( "_Alpha", 1 );
                        SetSpotlightSrcInfo(0.59f, 0.56f, 0.34f);
                        SetSpotlightMoveInfo(0.59f, 0.56f, 0.09f, 1f);

                    }
                    break;
                case 12:
                    {
                        m_matMask.SetFloat("_Alpha", 0);
                    }
                    break;
                case 13:
                    {
                        _paw.SetAlpha(1f);
                        _faClick.transform.SetParent(AdsManager.s_Instance._btnOpenAdsPanel.transform);

                        _paw.transform.SetParent( AdsManager.s_Instance._btnOpenAdsPanel.transform );
                        vecTempPos.x = 0;
                        vecTempPos.y = 108;
                        _paw.transform.localPosition = vecTempPos;
                        vecTempScale.x = -0.8f;
                        vecTempScale.y = -0.8f;
                        _paw.transform.localScale = vecTempScale;
                        _faClick.transform.localPosition = _paw.transform.localScale;
                        m_matMask.SetFloat("_Alpha", 1);
                        SetSpotlightSrcInfo(0.5f, 0.11f, 0.26f);
                        SetSpotlightMoveInfo(0.5f, 0.11f, 0.11f, 1f);
                    }
                    break;

                case 14:
                    {
                        _paw.SetAlpha(1f);
                        _faClick.transform.SetParent(AdsManager.s_Instance._btnWatchAds.transform);

                        _paw.transform.SetParent(AdsManager.s_Instance._btnWatchAds.transform);
                        vecTempPos.x = 0;
                        vecTempPos.y = -131;
                        _paw.transform.localPosition = vecTempPos;
                        vecTempScale.x = 1f;
                        vecTempScale.y = 1f;
                        _paw.transform.localScale = vecTempScale;
                        vecTempPos.x = -18;
                        vecTempPos.y = -129;
                        _faClick.transform.localPosition = vecTempPos;
                        SetSpotlightSrcInfo(0.5f, 0.26f, 0.26f);
                        SetSpotlightMoveInfo(0.5f, 0.26f, 0.11f, 1f);
                    }
                    break;

                case 15:
                    {
                        _paw.gameObject.SetActive( false );
                        _faClick.gameObject.SetActive( false );
                        m_matMask.SetFloat( "_Alpha", 0 );
                    }
                    break;
            } // end switch
        }
    }

    public void SetSpotlightSrcInfo(float fSrcPosX, float fSrcPosY, float fSrcRadius)
    {
        m_dicFloatParams["SpotLightPosX"] = fSrcPosX;
        m_dicFloatParams["SpotLightPosY"] = fSrcPosY;
        m_dicFloatParams["SpotLightCurRadius"] = fSrcRadius;

        m_matMask.SetFloat( "_X", fSrcPosX);
        m_matMask.SetFloat( "_Y", fSrcPosY);
        m_matMask.SetFloat("_Radius", fSrcRadius);

    }

    public void SetSpotlightMoveInfo( float fDestPosX, float fDestPosY, float fDestRadius, float fSpotLightMoveTime)
    {
        m_dicFloatParams["SpotLightDestPosX"] = fDestPosX;
        m_dicFloatParams["SpotLightDestPosY"] = fDestPosY;
        m_dicFloatParams["SpotLightDestRadius"] = fDestRadius;

        float fCurRadius = m_dicFloatParams["SpotLightCurRadius"];


        m_dicFloatParams["SpotLightMoveSpeedX"] = (m_dicFloatParams["SpotLightDestPosX"] - m_dicFloatParams["SpotLightPosX"]) / fSpotLightMoveTime;
        m_dicFloatParams["SpotLightMoveSpeedY"] = (m_dicFloatParams["SpotLightDestPosY"] - m_dicFloatParams["SpotLightPosY"]) / fSpotLightMoveTime;
        float fRadiusChangeSpeed = (fDestRadius - fCurRadius) / fSpotLightMoveTime;

        m_dicFloatParams["SpotLightRadiusChangeSpeed"] = fRadiusChangeSpeed;
    }


    void SpotLightAnimation()
    {


        float fCurPosX = m_dicFloatParams["SpotLightPosX"];
        float fDestPosX = m_dicFloatParams["SpotLightDestPosX"];
        float fMoveSpeedX = m_dicFloatParams["SpotLightMoveSpeedX"];
        if (fCurPosX > fDestPosX)
        {
            fCurPosX += fMoveSpeedX * Time.fixedDeltaTime;
            m_dicFloatParams["SpotLightPosX"] = fCurPosX;
            m_matMask.SetFloat("_X", fCurPosX);
        }

        float fCurPosY = m_dicFloatParams["SpotLightPosY"];
        float fDestPosY = m_dicFloatParams["SpotLightDestPosY"];
        float fMoveSpeedY = m_dicFloatParams["SpotLightMoveSpeedY"];
        if (fCurPosY > fDestPosY)
        {
            fCurPosY += fMoveSpeedY * Time.fixedDeltaTime;
            m_dicFloatParams["SpotLightPosY"] = fCurPosY;
            m_matMask.SetFloat("_Y", fCurPosY);
        }

        float fCurRadius = m_dicFloatParams["SpotLightCurRadius"];
        float fDestRadius = m_dicFloatParams["SpotLightDestRadius"];
        float fRadiusChangeSpeed = m_dicFloatParams["SpotLightRadiusChangeSpeed"];
       
        if ((fRadiusChangeSpeed > 0 &&  fCurRadius < fDestRadius) ||
           (fRadiusChangeSpeed < 0 && fCurRadius > fDestRadius)

           )
        {
            fCurRadius += fRadiusChangeSpeed * Time.fixedDeltaTime;
            m_dicFloatParams["SpotLightCurRadius"] = fCurRadius;
            m_matMask.SetFloat("_Radius", fCurRadius);
        }


        /// end spot-light animation
    }

    public void OnPawMoveEnd_ClickEffectPlay()
    {
        if ( GetGuidType() == eGuideType.first_in_game )
        {
            if ( GetStatus() == 7 )
            {
                _faClick.BeginPlay( false );
            }
        }
    }


    public void BeginGuide(eGuideType eType )
    {
        return;

        string szData = "";
        string szKey = KEY_GUIDE + (int)eType;
        szData = DataManager.s_Instance.GetMyData_String(szKey);
        int nDone = 0;
        if ( DataManager.IsDataValid(szData) )
        {
            if ( !int.TryParse(szData, out nDone) )
            {
                nDone = 0;
            }
        }
        if (nDone > 0) // already done
        {
            return; // 这一条已经指引过了，不再执行
        }

        m_eCurType = eType;
        m_nStatus = 0;
    }

    public eGuideType GetGuidType()
    {
        return m_eCurType;
    }

    public int GetIntParam( int nIndex )
    {
        return m_aryIntParams[nIndex];
    }

    public void SetIntParam(int nIndex, int nValue)
    {
        m_aryIntParams[nIndex] = nValue;
    }

    public float GetFloatParam(int nIndex)
    {
        return m_aryFloatParams[nIndex];
    }

    public void SetFloatParam(int nIndex, float fValue)
    {
        m_aryFloatParams[nIndex] = fValue;
    }

    public void OnClickButton_IncX()
    {
        m_fX += 0.01f;
        m_matMask.SetFloat("_X", m_fX);
        _txtX.text = m_fX.ToString(  "f2" );
    }

    public void OnClickButton_DecX()
    {
        m_fX -= 0.01f;
        m_matMask.SetFloat("_X", m_fX);
        _txtX.text = m_fX.ToString("f2");
    }

    public void OnClickButton_IncY()
    {
        m_fY += 0.01f;
        m_matMask.SetFloat("_Y", m_fY);
        _txtY.text = m_fY.ToString("f2");
    }

    public void OnClickButton_DecY()
    {
        m_fY -= 0.01f;
        m_matMask.SetFloat("_Y", m_fY);
        _txtY.text = m_fY.ToString("f2");
    }

    public void OnClickButton_IncRadius()
    {
        m_fRadius += 0.01f;
        m_matMask.SetFloat("_Radius", m_fRadius);
        _txtRadius.text = m_fRadius.ToString("f2");
    }

    public void OnClickButton_DecRadius()
    {
        m_fRadius -= 0.01f;
        m_matMask.SetFloat("_Radius", m_fRadius);
        _txtRadius.text = m_fRadius.ToString("f2");
    }

    public void OnClickButton_IncPawX()
    {
        vecTempPos = _paw.transform.position;
        vecTempPos.x += 0.1f;
        _paw.transform.position = vecTempPos;

        _txtPawX.text = _paw.transform.position.x.ToString( "f2" );

    }

    public void OnClickButton_DecPawX()
    {
        vecTempPos = _paw.transform.position;
        vecTempPos.x -= 0.1f;
        _paw.transform.position = vecTempPos;

        _txtPawX.text = _paw.transform.position.x.ToString("f2");
    }

    public void OnClickButton_IncPawY()
    {
        vecTempPos = _paw.transform.position;
        vecTempPos.y += 0.1f;
        _paw.transform.position = vecTempPos;

        _txtPawY.text = _paw.transform.position.y.ToString("f2");
    }

    public void OnClickButton_DecPawY()
    {
        vecTempPos = _paw.transform.position;
        vecTempPos.y -= 0.1f;
        _paw.transform.position = vecTempPos;

        _txtPawY.text = _paw.transform.position.y.ToString("f2");
    }



    void Loop_FirstInGame()
    {
        if (m_eCurType !=  eGuideType.first_in_game)
        {
            return;
        }

        switch(m_nStatus) 
        {
            case 0:
                {
                    m_matMask.SetFloat( "_Alpha", 1 );

                    float fR0 = 0.25f;
                    float fR = 0.09f;
                    float fPosY = 0.24f;
                    _goMask.gameObject.SetActive( true );
                    m_matMask.SetFloat( "_X", 0.5f );
                    m_matMask.SetFloat("_Y", fPosY);
                    m_matMask.SetFloat("_Radius", fR0);

                    // spot-light shrink speed
                    m_dicFloatParams["SpotLightShrinkSpeed"] = (fR - fR0) / 0.75f;

                    // spot-light cur radius
                    m_dicFloatParams["SpotLightCurRadius"] = fR0;

                    // spot-light dest radius
                    m_dicFloatParams["SpotLightDestRadius"] = fR;

                    // pos y
                    m_dicFloatParams["SpotLightPosY"] = fPosY;

                    _faClick.transform.SetParent(_uiRecommend.transform);
                    _paw.transform.SetParent( _uiRecommend.transform );
                   
                    _faClick.m_bUI = false;

                    vecTempPos.x = 0;
                    vecTempPos.y = -96;
                    _paw.transform.localPosition = vecTempPos;
                    _faClick.transform.localPosition = _paw.transform.localPosition;

                    float fScale = 1f;
                    vecTempScale.x = fScale;
                    vecTempScale.y = fScale;
                    _paw.transform.localScale = vecTempScale;
                    _faClick.transform.localScale = vecTempScale;

                    _paw.SetIsUI(true);
                    _paw.gameObject.SetActive( true );
                    m_nStatus = 1;
                    SetIntParam ( 0, 0 );

        

                    _paw.gameObject.SetActive(true);
                    _faClick.gameObject.SetActive(true);
                }
                break;
            case 1:
                {
                    float fShrinkSpeed = m_dicFloatParams["SpotLightShrinkSpeed"];
                    float fCurRadius = m_dicFloatParams["SpotLightCurRadius"];
                    float fDestRadius = m_dicFloatParams["SpotLightDestRadius"];
                    if (fCurRadius > fDestRadius)
                    {
                        fCurRadius += fShrinkSpeed * Time.fixedDeltaTime;
                        m_matMask.SetFloat("_Radius", fCurRadius);
                        m_dicFloatParams["SpotLightCurRadius"] = fCurRadius;
                    }
                    else
                    {
                        m_dicFloatParams["SpotLightCurRadius"] = m_dicFloatParams["SpotLightDestRadius"];
                    }

                    if (GetIntParam(0) == 2)
                    {
                        m_nStatus = 2;
                        _paw.SetIsUI(false);
                        _paw.transform.SetParent( _containerScene.transform );
                        _faClick.transform.SetParent(_containerScene.transform);
                        _faClick.m_bUI = false;

                        vecTempPos = _lot1.transform.position;
                      
                        _paw.transform.localPosition = vecTempPos;
                        _paw._basescalePaw.enabled = false;
                        _faClick.transform.position = vecTempPos;


                        vecTempScale.x = 0.7f;
                        vecTempScale.y = 0.7f;
                        _paw.transform.localScale = vecTempScale;
                        _faClick.transform.localScale = vecTempScale;


                        // paw move speed
                        m_aryFloatParams[0] = (_lot0.transform.position.x - _lot1.transform.position.x) / 1f;
                        m_nSubStatus = 0;

                        _paw._frameaniClick.BeginPlay( false );

                        float fTotalTime = 0.5f;
                        float fDestPosY = 0.61f;

                        // spot-light move-up speed
                        m_dicFloatParams["SpotLightMoveUpSpeed"] = (fDestPosY - m_dicFloatParams["SpotLightPosY"]) / fTotalTime;

                        // spot-light dest posY
                        m_dicFloatParams["SpotLightDestPosY"] = fDestPosY;
                        m_dicFloatParams["SpotLightPosX"] = 0.5f;

                        // spot-light enlarge speed
                        fCurRadius = m_dicFloatParams["SpotLightCurRadius"];
                        float fNewDestRadius = 0.12f;
                        m_dicFloatParams["SpotLightEnlargeSpeed"] = (fNewDestRadius - fCurRadius) / fTotalTime;

                        // spot-light cur radius
                        m_dicFloatParams["SpotLightCurRadius"] = fCurRadius;

                        // spot-light dest radius
                        m_dicFloatParams["SpotLightDestRadius"] = fNewDestRadius;
                    }
                }
                break;
            case 2:
                {
                    ///// mask animation
                    /// 
                    float fPosY = m_dicFloatParams["SpotLightPosY"];
                    if (fPosY < m_dicFloatParams["SpotLightDestPosY"])
                    {
                        fPosY += m_dicFloatParams["SpotLightMoveUpSpeed"] * Time.fixedDeltaTime;
                        m_dicFloatParams["SpotLightPosY"] = fPosY;
                        m_matMask.SetFloat("_Y", fPosY);
                    }


                    float fCurRadius = m_dicFloatParams["SpotLightCurRadius"];
                    float fDestRadius = m_dicFloatParams["SpotLightDestRadius"];

                    if (fCurRadius < fDestRadius)
                    {
                        float fEnlargeSpeed = m_dicFloatParams["SpotLightEnlargeSpeed"];
                        fCurRadius += fEnlargeSpeed * Time.fixedDeltaTime;
                        m_dicFloatParams["SpotLightCurRadius"] = fCurRadius;
                        m_matMask.SetFloat( "_Radius", fCurRadius);

                    }

                       
                    /// end mask animation


                    if (m_nSubStatus == 0)
                    {
                        vecTempPos = _paw.transform.position;
                        vecTempPos.x += m_aryFloatParams[0] * Time.fixedDeltaTime;
                        _paw.transform.position = vecTempPos;
                        if (vecTempPos.x <= _lot0.transform.position.x)
                        {
                           
                            m_nSubStatus = 1;
                            m_fTimeElapse = 0;
                        }
                    }
                    else if (m_nSubStatus == 1)
                    {
                        m_fTimeElapse += Time.fixedDeltaTime;
                        if (m_fTimeElapse > 0.5f)
                        {
                            _paw._frameaniClick.BeginPlay(false);
                            _paw.transform.position = _lot1.transform.position;
                            m_nSubStatus = 0;
                        }
                    }


                    if ( MapManager.s_Instance.GetCurDistrict().GetLevel() == 2 )
                    {
                        m_nStatus = 3;

                        m_dicFloatParams["PawStartPosX"] = _lot0.transform.position.x;
                        m_dicFloatParams["PawStartPosY"] = _lot1.transform.position.y;

                        m_dicFloatParams["PawEndPosX"] = Main.s_Instance._goStartPos.transform.position.x;
                        m_dicFloatParams["PawEndPosY"] = Main.s_Instance._goStartPos.transform.position.y;

                        float fMoveTime = 1f;
                        m_dicFloatParams["PawMoveSpeedX"] = (m_dicFloatParams["PawEndPosX"] - m_dicFloatParams["PawStartPosX"]) / fMoveTime;
                        m_dicFloatParams["PawMoveSpeedY"] = (m_dicFloatParams["PawEndPosY"] - m_dicFloatParams["PawStartPosY"]) / fMoveTime;

                        vecTempPos.x = m_dicFloatParams["PawStartPosX"];
                        vecTempPos.y = m_dicFloatParams["PawStartPosY"];
                        _paw.transform.position = vecTempPos;

                        float fSpotLightMoveTime = 0.5f;
                        m_dicFloatParams["SpotLightDestPosX"] = 0.35f;
                        m_dicFloatParams["SpotLightDestPosY"] = 0.57f;
                        m_dicFloatParams["SpotLightDestRadius"] = 0.16f;
                        m_dicFloatParams["SpotLightMoveSpeedX"] = (m_dicFloatParams["SpotLightDestPosX"] - m_dicFloatParams["SpotLightPosX"]) / fSpotLightMoveTime;
                        m_dicFloatParams["SpotLightMoveSpeedY"] = (m_dicFloatParams["SpotLightDestPosY"] - m_dicFloatParams["SpotLightPosY"]) / fSpotLightMoveTime;
                        m_dicFloatParams["SpotLightRadiusChangeSpeed"] =( m_dicFloatParams["SpotLightDestRadius"] - m_dicFloatParams["SpotLightCurRadius"]) / fSpotLightMoveTime;
                        m_nSubStatus = 0;

                        _faClick.transform.position = _lot0.transform.position;
                        _faClick.BeginPlay(false);


                        m_dicFloatParams["MaskFadeSpeed"] = 1f / 0.5f;
                    }

                }
                break;

            case 3:
                {
                    /// spot-light animation
                    float fCurPosX = m_dicFloatParams["SpotLightPosX"];
                    float fDestPosX = m_dicFloatParams["SpotLightDestPosX"];
                    float fMoveSpeedX = m_dicFloatParams["SpotLightMoveSpeedX"];
                    if ( fCurPosX  > fDestPosX ) 
                    {
                        fCurPosX += fMoveSpeedX * Time.fixedDeltaTime;
                        m_dicFloatParams["SpotLightPosX"] = fCurPosX;
                        m_matMask.SetFloat( "_X", fCurPosX);
                    }

                    float fCurPosY = m_dicFloatParams["SpotLightPosY"];
                    float fDestPosY = m_dicFloatParams["SpotLightDestPosY"];
                    float fMoveSpeedY = m_dicFloatParams["SpotLightMoveSpeedY"];
                    if (fCurPosY > fDestPosY)
                    {
                        fCurPosY += fMoveSpeedY * Time.fixedDeltaTime;
                        m_dicFloatParams["SpotLightPosY"] = fCurPosY;
                        m_matMask.SetFloat("_Y", fCurPosY);
                    }

                    float fCurRadius = m_dicFloatParams["SpotLightCurRadius"];
                    float fDestRadius = m_dicFloatParams["SpotLightDestRadius"];
                    float fRadiusChangeSpeed = m_dicFloatParams["SpotLightRadiusChangeSpeed"];
                    if (fCurRadius < fDestRadius)       
                    {
                        fCurRadius += fRadiusChangeSpeed * Time.fixedDeltaTime;
                        m_dicFloatParams["SpotLightCurRadius"] = fCurRadius;
                        m_matMask.SetFloat( "_Radius", fCurRadius);
                    }


                    /// end spot-light animation

                    /// paw move
                    if (m_nSubStatus == 0)
                    {
                        bool bEndX = false;
                        bool bEndY = false;

                        vecTempPos = _paw.transform.position;
                        if (vecTempPos.x > m_dicFloatParams["PawEndPosX"])
                        {
                            vecTempPos.x += m_dicFloatParams["PawMoveSpeedX"] * Time.fixedDeltaTime;
                            _paw.transform.position = vecTempPos;
                        }
                        else
                        {
                            bEndX = true;
                        }

                        if (vecTempPos.y > m_dicFloatParams["PawEndPosY"])
                        {
                            vecTempPos.y += m_dicFloatParams["PawMoveSpeedY"] * Time.fixedDeltaTime;
                            _paw.transform.position = vecTempPos;
                        }
                        else
                        {
                            bEndY = true;
                        }

                        if (bEndX || bEndY)
                        {
                            m_nSubStatus = 1;
                            m_fTimeElapse = 0;
                        }

                    } // end if (m_nSubStatus == 0)
                    else if (m_nSubStatus == 1)
                    {
                        m_fTimeElapse += Time.fixedDeltaTime;
                        if (m_fTimeElapse > 0.3f)
                        {
                            m_nSubStatus = 0;
                            vecTempPos.x = m_dicFloatParams["PawStartPosX"];
                            vecTempPos.y = m_dicFloatParams["PawStartPosY"];
                            _paw.transform.position = vecTempPos;

                            _faClick.BeginPlay(false);
                        }


                    }

                    /// end paw move


                } // end status 3
                break;

            case 4:
                {
                    /// mask fade
                    float fCurAlpha = m_matMask.GetFloat( "_Alpha" );
                    if (fCurAlpha > 0)
                    {
                        fCurAlpha -= m_dicFloatParams["MaskFadeSpeed"] * Time.fixedDeltaTime;
                        m_matMask.SetFloat("_Alpha", fCurAlpha);
                    }


                    /// end mask fade


                } // end status 4
                break;

            case 5:
                {
                    SpotLightAnimation();
                } // end status 5
                break;

            case 6:
                {
                    SpotLightAnimation();
                } // end status 5
                break;

            case 7:
                {
                    SpotLightAnimation();
                } // end status 5
                break;
            case 8:
                {
                    SpotLightAnimation();
                } // end status 5
                break;
            case 9:
                {
                    /// mask fade
                    float fCurAlpha = m_matMask.GetFloat("_Alpha");
                    if (fCurAlpha > 0)
                    {
                        fCurAlpha -= m_dicFloatParams["MaskFadeSpeed"] * Time.fixedDeltaTime;
                        m_matMask.SetFloat("_Alpha", fCurAlpha);
                    }


                    /// end mask fade


                }
                break;
            case 10:
                {
                    SpotLightAnimation();
                }
                break;
            case 11:
                {
                    SpotLightAnimation();
                }
                break;
            case 13:
                {
                    SpotLightAnimation();
                }
                break;
            case 14:
                {
                    SpotLightAnimation();
                }
                break;
        } // end switch
         
    }

} // end class
