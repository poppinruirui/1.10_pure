﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    public GameObject m_goContainerEffects;

    public GameObject _containerDebugButtons;


    /// <summary>
    /// new version Locations
    /// </summary>
    public GameObject _goStartPos;
    public GameObject[] m_aryNodeStart;
    public GameObject[] m_aryNodeCenter;
    public GameObject[] m_aryNodeEnd;

    public Dictionary<ePosType, List<sAutoRunLocation>> m_dicLocationPoints = new Dictionary<ePosType, List<sAutoRunLocation>>();
    public GameObject[] m_aryStartLocationPointOfEachSeg;
    public sAutoRunLocation[] m_aryStartLocationInfoPointOfEachSeg = new sAutoRunLocation[8];

    //// end new version Locations

    public GameObject m_containerLocations; // 轨迹定位点

    public Text _txtDebugInfo;
    public Text _txtOfflineDetailDebug;

    public MoneyTrigger _moneyTrigger;

    public Sprite[] m_aryMoneyTrigger;

    public BaseScale _basescaleCPosCoinIcon;

    public static Vector3 vecTempPos = new Vector3();
    public static Vector3 vecTempScale = new Vector3();
    public static Color colorTemp = new Color();

    public static Main s_Instance = null;

    public Vector3 m_vecLocalPosOnLot = new Vector3(0, 0.5f);

    public float m_nCurRaise = 1;

    public MoneyCounter[] m_aryCoin0;
    public MoneyCounter[] m_aryCoin1;
    public MoneyCounter[] m_aryCoin2;

    //// ----------
    /// UI
    /// -----------


    public Text _txtPrestigeTimes;
    public Text _txtPrestigeTimes_Shadow;
    public UIStars _starsPrestigeTimes;

    public Text _txtTotalCoinOfThisPlanet;
    public TextMesh _tmTotalCoinOfThisPlanet;

    public GameObject _panelCollectOfflineProfit;
    public Text _txtOfflineProfitOfThisDistrict;

    public Button _btnOpenBigMap;
    public Button _btnCloseBigMap;
    public MoneyCounter _moneyCoin;
    public MoneyCounter[] m_aryActivePlanetCoin;

    public MoneyCounter _moneyGreenCash;
    public Text _txtDiamond;
    public Text _txtDiamond_Shadow;


    public GameObject _panelRaiseDetail; // “提升概览”面板
    public Text _txtRaiseDetail; // 显示“提升概览”的内容

    public SceneUiButton _btnBuy;

    public GameObject _BigMap;
    public GameObject _panelPrestige;
    public UIPrestige _Prestige;

    public TextMesh _txtNumOfRunningPlane;
    public Text _txtDPS;

    /// <summary>
    /// / SceneUI
    /// </summary>
    public GameObject _goRecycleBox;

    public float DISTANCE_UNIT = 3f;
    public float TURN_RADIUS = 0f;
    public Vector2 START_POS = Vector2.zero;
    public float LENGTH_OF_TURN = 0f; // 转角处的长度　
    public float PERIMETER = 0f;

    public float MERGE_OFFSET = 0.5f;
    public float MERGE_TIME = 0.2f;
    public float MERGE_WAIT_TIME = 0.2f;

    public GameObject _containerRunningPlanes;

    List<Plane> m_lstRunningPlanes = new List<Plane>();

    public float[] m_aryPosThreshold = new float[8];
    public Vector2[] m_aryStartPosOfEachSegment = new Vector2[8];
    public Vector2[] m_aryCircleCenterOfTurn = new Vector2[8];

    public float m_fMergeMoveSpeed = 0f;

    //  停机坪相关
    public const int MAX_LOT_NUM_NATURAL = 14; // 自然状态下最多解锁13个停机坪。用月卡系统可以解锁到15个
    public const int MAX_LOT_NUM = 14;
    public const int MIN_LOT_NUM =14;
    public Lot[] m_aryLots;
    public GameObject m_aryLotsPositionPointsContainers;

    public StartBelt m_StartBelt;
  
    public float[] m_aryRoundTimeByLevel;

    List<Plane> m_lstAllPlanes = new List<Plane>();

    public enum ePosType
    {
        left,
        left_top,
        top,
        right_top,
        right,
        right_bottom,
        bottom,
        left_bottom
    };

    public enum ePlaneStatus
    {
        idle,
        running_on_airline,
        dragging,
        running_to_lot,
        merging,
        treasure_box,
    };



    public Vector3 GetLocalPosOnLot()
    {
        if (false/*MapManager.s_Instance.GetCurDistrict().IsOceanOrAir()*/)
        {
            return Vector3.zero;

        }
        else
        {
            return m_vecLocalPosOnLot;
        }

        return m_vecLocalPosOnLot;
    }

    // (废弃)
    public static float GetTime()
    {
        return Time.time;
    }

    public static System.DateTime GetSystemTime()
    {
        return System.DateTime.Now;
    }

    public Vector3 m_vecStartPos = new Vector3();
    public Vector3[] m_vecNodeStartPos = new Vector3[4];
    public Vector3[] m_vecNodeCenterPos = new Vector3[4];
    public Vector3[] m_vecNodeEndPos = new Vector3[4];
    public float m_fRadius = 0f;
    public float m_fVertLineLen = 0f;
    public float m_fHoriLineLen = 0f;
    public float m_fRoundLen = 0f;
    public float m_fPosAdjustDistance = 0f;
    void CalculateLocationInfo_NewVer()
    {
        return;

        for (int i = 0; i < 4; i++ )
        {
            GameObject obj = m_aryNodeStart[i];
            JTDHSceneManager.s_Instance.DoAdjust(obj);
            obj = m_aryNodeEnd[i];
            JTDHSceneManager.s_Instance.DoAdjust(obj);
            obj = m_aryNodeCenter[i];
            JTDHSceneManager.s_Instance.DoAdjust(obj);
        }


        m_vecStartPos = _goStartPos.transform.localPosition;

        for (int i = 0; i < 4; i++)
        {
            m_vecNodeStartPos[i] = m_aryNodeStart[i].transform.localPosition;
            m_vecNodeCenterPos[i] = m_aryNodeCenter[i].transform.localPosition;
            m_vecNodeEndPos[i] = m_aryNodeEnd[i].transform.localPosition;
        } // end for i

        TURN_RADIUS = m_vecNodeCenterPos[0].x - m_vecNodeStartPos[0].x;
        m_fPosAdjustDistance = m_vecNodeStartPos[0].x - m_vecStartPos.x;
        LENGTH_OF_TURN = 0.5f * (float)Math.PI * TURN_RADIUS;
        m_fVertLineLen = m_vecNodeStartPos[0].y - m_vecNodeEndPos[3].y;
        m_fHoriLineLen = m_vecNodeStartPos[1].x - m_vecNodeEndPos[0].x;
        PERIMETER = 4f * LENGTH_OF_TURN + 2f * m_fVertLineLen + 2f * m_fHoriLineLen;
    }


    const float PARAM1 = 0.4f;
    private void Awake()
    {
        // CalculateLocationInfo_NewVer();
        Input.multiTouchEnabled = false;


        Application.targetFrameRate = 60;

        s_Instance = this;



        m_fMergeMoveSpeed = MERGE_OFFSET / MERGE_TIME * Time.fixedDeltaTime;


        m_aryRoundTimeByLevel = new float[32];
        for (int i = 0; i < m_aryRoundTimeByLevel.Length; i++)
        {
            m_aryRoundTimeByLevel[i] = 6f - 0.2f * i;
        }

        /// 注册事件
//        _btnBuy.OnClickEvent += new SceneUiButton.OnClickEventHandler(OnClickButton_BuyPlane); // 订阅举杯事件

        for (int i = 0; i < m_aryLots.Length; i++ )
        {
            Lot lot = m_aryLots[i];
            lot.SetId( i );
        }

    }

    // Use this for initialization
    void Start()
    {
        UILoading.s_Instance.BeginProgress( 1.5f );

        //CalculateLocationsInfo();

        // 读档............
        // to do

        //  MapManager.s_Instance.Init();

        DebugInfo.s_Instance.SetPlanetAndDistrict(0, 0);

#if UNITY_IOS
        MoreMountains.NiceVibrations.MMVibrationManager.Haptic(MoreMountains.NiceVibrations.HapticTypes.Selection);
#elif UNITY_ANDROID
        IntenseVibration.Instance.Vibrate(2L);
#endif

    }

    // Update is called once per frame

    bool m_bCheatFirst = true;
    float _dtLastTapTime  =0f;
    public void OnClickButton_ShowMeTheCheat()
    {
 
            if (m_bCheatFirst)
            {
                _dtLastTapTime = Time.time;
                m_bCheatFirst = false;
                return;
            }

            if ( (Time.time - _dtLastTapTime ) < 0.5f)
            {
                SetCheatVisible( true );
               
            }
            else
            {
                SetCheatVisible(false);
            }
            _dtLastTapTime = Time.time;
       
    }

    void SetCheatVisible( bool bVisible )
    {
        _containerDebugButtons.SetActive(bVisible);
    }


    float m_f10SecLoopElapse = 0f;
    void Update()
    {
        Loop_1_Sec();

        PreAccelerateLoop();

        CoinNumberChangeLoop();

       

    }

    void Loop_1_Sec()
    {

        m_f10SecLoopElapse += Time.deltaTime;
        if (m_f10SecLoopElapse < 1f)
        {
            return;
        }
        m_f10SecLoopElapse = 0;

        if (MapManager.s_Instance.GetCurDistrict() == null)
        {
            return;
        }

        double fDps = MapManager.s_Instance.GetCurDistrict().CalculateDPS();

        _txtDPS.text = CyberTreeMath.GetFormatMoney(fDps) + "/秒";
    }

    private void FixedUpdate()
    {

    }



    // 根据飞机等级，获取该飞机运行轨道一周所需的时间
    public float GetOneRoundTimeByLevel(int nLevel)
    {
        return DataManager.s_Instance.GetVehicleRunTimePerRound(nLevel);
    }

    public Lot GetLotById(int nId)
    {
        if (nId < 0 || nId >= m_aryLots.Length)
        {
            return null;
        }
        return m_aryLots[nId];
    }

    public void OnClickButton_BuyPlane()
    {
        if (UIManager.IsPointerOverUI())
        {
            return;
        }

        Lot lot = null;

        // 先判断有没有空位
        for (int i = 0; i < MapManager.s_Instance.GetCurDistrict().GetCurLotNum() /*m_aryLots.Length*/; i++)
        {
            if (!m_aryLots[i].IsTaken())
            {
                lot = m_aryLots[i];
                break;
            }
        }

        if (lot == null)
        {
            UIMsgBox.s_Instance.ShowMsg("停机坪没有空位了");
            return;
        }

        Plane plane = ResourceManager.s_Instance.NewPlane();
        plane.SetLevel(1);
        lot.SetPlane(plane);

        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_buy_plane);

    }

    public Lot GetAvailableLot()
    {
        Lot lot = null;

        // 先判断有没有空位
        for (int i = 0; i < MapManager.s_Instance.GetCurDistrict().GetCurLotNum() /*m_aryLots.Length*/; i++)
        {
            if (!m_aryLots[i].IsTaken())
            {
                lot = m_aryLots[i];
                break;
            }
        }

        if (lot == null)
        {
            UIMsgBox.s_Instance.ShowMsg("没有空泊位了");
            return null;
        }

        return lot;
    }


    public bool BuyOneVehicle(int nLevel, ref Plane the_plane)
    {
        Lot lot = null;

        // 先判断有没有空位
        for (int i = 0; i < MapManager.s_Instance.GetCurDistrict().GetCurLotNum() /*m_aryLots.Length*/; i++)
        {
            if (!m_aryLots[i].IsTaken())
            {
                lot = m_aryLots[i];
                break;
            }
        }

        if (lot == null)
        {
            UIMsgBox.s_Instance.ShowMsg("没有空泊位了");
            return false;
        }


        if (!TanGeChe.s_Instance.m_bNoPlane)
        {
            Plane plane = ResourceManager.s_Instance.NewPlane();
            plane.SetLevel(nLevel);
            lot.SetPlane(plane);
            the_plane = plane;
        }

        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_buy_plane);


        DataManager.s_Instance.SaveCurTrackPlanesData();



        int nCurTimes = MapManager.s_Instance.GetCurDistrict().GetVehicleBuyTimeById(nLevel);
        nCurTimes++;
        MapManager.s_Instance.GetCurDistrict().SetVehicleBuyTimeById(nLevel, nCurTimes);
        MapManager.s_Instance.GetCurDistrict().CalculateRecommendInfo();


        if ( FreshGuide.s_Instance.GetGuidType() == FreshGuide.eGuideType.first_in_game && FreshGuide.s_Instance.GetStatus() == 11 )
        {
            TanGeChe.s_Instance.OnClick_CloseTanGeChe();
            FreshGuide.s_Instance.SetStatus( 12 );
        }


        return true;
    }

    public void BeginRun(Plane plane)
    {
        plane.transform.SetParent(_containerRunningPlanes.transform);
        //plane.SetPos( START_POS );

        MapManager.s_Instance.GetCurDistrict().UpdateRunningPlanes();//AddRunningPlane(plane);
       
        m_StartBelt.SetRunningPlanesNum(MapManager.s_Instance.GetCurDistrict().GetRunningPlanesList().Count);

    }

    public void RemovePlaneFromeAirline(Plane plane)
    {
        MapManager.s_Instance.GetCurDistrict().GetRunningPlanesList().Remove(plane);
        MapManager.s_Instance.GetCurDistrict().RemoveRunningPlane(plane);
        m_StartBelt.SetRunningPlanesNum(MapManager.s_Instance.GetCurDistrict().GetRunningPlanesList().Count);
    }

    public bool CheckIfCanRun()
    {
        return m_StartBelt.CheckIfCanRun();
    }

    public void ShowCanMergeLots(bool bVisible, Plane planeDragging = null)
    {
        if (!bVisible)
        {
            for (int i = 0; i < MapManager.s_Instance.GetCurDistrict().GetCurLotNum() /*m_aryLots.Length*/; i++)
            {
                Lot lot = m_aryLots[i];
                lot.SetEffectCanMergeVisible(false);

            }
        }
        else
        {
            for (int i = 0; i < MapManager.s_Instance.GetCurDistrict().GetCurLotNum() /*m_aryLots.Length*/; i++)
            {
                Lot lot = m_aryLots[i];
                if (lot.IsTaken())
                {
                    Plane plane = lot.GetPlane();
                    if (plane != planeDragging && plane.GetLevel() == planeDragging.GetLevel() && plane.GetPlaneStatus() == ePlaneStatus.idle)
                    {
                        lot.SetEffectCanMergeVisible(true);
                    }
                }
            }
        }
    }

    public Lot GetOneAvailableLotToGenerateBox()
    {
        for (int i = 0; i < MapManager.s_Instance.GetCurDistrict().GetCurLotNum() /*m_aryLots.Length*/; i++)
        {
            if (!m_aryLots[i].IsTaken())
            {
                return m_aryLots[i];
            }
        }

        return null;
    }

    public string GenerateDistrictData()
    {
        string szData = "";

        return szData;
    }

    bool m_bIsVisible = false;
    public void OnClickButton_OpenBigMap()
    {
        m_bIsVisible = true;

       // UIManager.s_Instance._uiSubContainerForBigMap.SetActive(true)  ;
        _BigMap.SetActive(true);

        UIManager.s_Instance.SetSomeUiVisibleDueToStarSky(false);
        MapManager.s_Instance.StartStarSkyEffect();

        MapManager.s_Instance.UpdateUIPlanetsInfo();

        MapManager.s_Instance.m_bBigMapShowing = true;

        MapManager.s_Instance.OpenPlanetDetailPanel(MapManager.s_Instance.m_nCurSelctedIndex);
        MapManager.s_Instance.OpenBigMap();

        AccountSystem.s_Instance.SetGeneralMoneyCountersVisible( true );

        UIManager.s_Instance.OnOpenUI();


        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.DaDiTuAnNiu);

        
    }

    public void OnClickButton_CloseBigMap()
    {
        m_bIsVisible = false;

      // UIManager.s_Instance._uiSubContainerForBigMap.SetActive(false);
        _BigMap.SetActive(false);
        _btnOpenBigMap.gameObject.SetActive(true);

        UIManager.s_Instance.SetSomeUiVisibleDueToStarSky(true);

        MapManager.s_Instance.StopStarSkyEffect();

        MapManager.s_Instance.m_bBigMapShowing = false;

        //   AccountSystem.s_Instance.SetGeneralMoneyCountersVisible(false);
        AccountSystem.s_Instance.CheckIfShutDownGeneralMoneyCounter();
    }

    public bool IsVisible()
    {
        return m_bIsVisible;
    }

    public void OnClickButton_OpenPrestigePanel()
    {
        _panelPrestige.SetActive(true);
        _Prestige.UpdateInfo();

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.ChongShengAnNiu);
    }

    public void OpenPrestigePanel_ZengShou(int nPlanetId, int nDistrictId)
    {
        _panelPrestige.SetActive(true);
        _Prestige.UpdateInfo(nPlanetId, nDistrictId);
    }

    public void OnClickButton_ClosesPrestigePanel()
    {
        _panelPrestige.SetActive(false);
    }

    List<Plane> lstTempPLanes = new List<Plane>();
    public string GenerateData()
    {
        string szData = "";

        lstTempPLanes.Clear();
//        Debug.Log( "nali: " + MapManager.s_Instance.GetCurDistrict().GetCurLotNum());
        for (int i = 0; i < MapManager.s_Instance.GetCurDistrict().GetCurLotNum() /*m_aryLots.Length*/; i++)
        {
            Lot lot = m_aryLots[i];
            Plane plane = lot.GetPlane();
            if (plane == null)
            {
                continue;
            }
            lstTempPLanes.Add(plane);
            /*
            szData +=((int)(plane.GetPlaneStatus()) + "," );
            szData += (lot.GetId() + ","); // 停机坪的编号
            szData += ((plane.GetLevel()) + ","); // 该飞机的等级 
            szData += ( plane.GetRunLocationInfo().nLocationIndex + "," ); //  该飞机的当前位置编号
            szData += ( plane.GetPos().x.ToString( "f2" ) + "," );
            szData += ( plane.GetPos().y.ToString("f2")  );
            szData += "_";
            */

        }

        szData += lstTempPLanes.Count;
        szData += "|";
        for (int i = 0; i < lstTempPLanes.Count; i++)
        {
            Plane plane = lstTempPLanes[i];
            szData += ((int)(plane.GetPlaneStatus()) + ",");
            szData += (plane.GetLot().GetId() + ","); // 停机坪的编号
            szData += ((plane.GetLevel()) + ","); // 该飞机的等级 
            szData += (plane.GetRunLocationInfo().nLocationIndex + ","); //  该飞机的当前位置编号
            szData += (plane.GetPos().x.ToString("f2") + ",");
            szData += (plane.GetPos().y.ToString("f2") + ",");
            szData += ((int)plane.GetPosType() + ",");
            szData += (plane.GetcurAngle().ToString("f2"));
            if (i != lstTempPLanes.Count - 1)
            {
                szData += "_";
            }
        }



    //    Debug.Log( "save: " + szData);

        return szData;
    }

    public void ClearAll()
    {
        // 跑道的占用清空
        if (StartBelt.s_Instance)
        {
            StartBelt.s_Instance.SetRunningPlanesNum(0);
        }
        MapManager.s_Instance.GetCurDistrict().GetRunningPlanesList().Clear();



        for (int i = 0; i < m_aryLots.Length; i++)
        {
            Lot lot = m_aryLots[i];
            Plane plane = lot.GetPlane();
            if (plane != null)
            {
                //  ResourceManager.s_Instance.DeletePlane( plane );
                //  lot.SetPlane(null);
                plane.RecycleMe();
            }

        }




    }

    public void UpdateLots( District district)
    {
        for (int i = 0; i < m_aryLots.Length; i++)
        {
            Lot lot = m_aryLots[i];
            lot.UpdateSomePresent();
        }


        int nCurNaturalLootNum = district.GetCurLotNum();
        int nCurUsingMonthCard = AccountSystem.s_Instance.GetCurUsingMonthCardNum();
        int nCurLootNum = nCurNaturalLootNum;
        /*
        if (nCurNaturalLootNum > MIN_LOT_NUM)
        {
            switch (nCurUsingMonthCard)
            {
                case 0:
                    {
                        nCurLootNum += 1;
                    }
                    break;
                case 1:
                    {
                        nCurLootNum += 2;
                    }
                    break;
                case 2:
                    {
                        nCurLootNum += 2;
                    }
                    break;
            } // end switch
        } // end if (nCurNaturalLootNum > MIN_LOT_NUM)
        */
        for (int i = 0; i < m_aryLots.Length; i++ )
        {
            Lot lot = m_aryLots[i];
            lot.SetActive( false );
            lot.SetPlusSign(false);
        }
        /*
        if (nCurNaturalLootNum > MIN_LOT_NUM)
        {
            switch (nCurUsingMonthCard)
            {
                case 0:
                    {
                        m_aryLots[nCurLootNum - 1].SetPlusSign(true);
                    }
                    break;
                case 1:
                    {
                        m_aryLots[nCurLootNum - 1].SetPlusSign(true);
                    }
                    break;
                case 2:
                    {

                    }
                    break;
            } // end switch
        } // end if (nCurNaturalLootNum > MIN_LOT_NUM)
        */

        List<Vector2> lstPos = null;
        if ( !m_dicLotPosOfLevel.TryGetValue(nCurLootNum, out lstPos) )
        {
            lstPos = null;
        }
        for (int i = 0; i < nCurLootNum; i++ )
        {
            Lot lot = m_aryLots[i];
            lot.SetActive(true);

            if (lstPos != null && i < lstPos.Count)
            {
                Vector2 pos = lstPos[i];
                lot.transform.localPosition = pos;
            }


        }
    }

    public void Load(Planet planet, District district)
    {
        ClearAll();

        //// 先把停机坪刷新
        UpdateLots(district);



        string szKey = "CurTrackPlanesData" + planet.GetId() + "_" + district.GetId();
        string szData = DataManager.s_Instance.LoadMyDataByKey(szKey);


        if ( !DataManager.IsDataValid( szData) )
        {
          //  Debug.LogError( "holy shi!!!!" );
            return;
        }

        string[] aryData = szData.Split('|');
        int nPointer = 0;
        int nCountOfPlanes = int.Parse(aryData[nPointer++]);
        string szPlanesData = aryData[nPointer++];
        string[] aryPlanesData = szPlanesData.Split('_');

//        Debug.Log( "plane count = " + nCountOfPlanes);
        for (int i = 0; i < aryPlanesData.Length; i++)
        {
            if (aryPlanesData[i] == "")
            {
                continue;
            }
            string[] aryParams = aryPlanesData[i].Split(',');


            int nStatus = int.Parse(aryParams[0]);
            int nLotId = int.Parse(aryParams[1]);
            int nPlaneLevel = int.Parse(aryParams[2]);
            int nLocationIndex = int.Parse(aryParams[3]); // （如果正在跑道上跑）在跑道上的位置编号
            float fPosX = float.Parse(aryParams[4]);
            float fPosY = float.Parse(aryParams[5]);
            ePosType pos_type = (ePosType)int.Parse(aryParams[6]);
            float fCurAngle = float.Parse(aryParams[7]);

            Lot lot = GetLotById(nLotId);

            Plane plane = ResourceManager.s_Instance.NewPlane();
            plane.SetLevel(nPlaneLevel);
            lot.SetPlane(plane);

            ePlaneStatus eStatus = (ePlaneStatus)nStatus;
            if ( eStatus == ePlaneStatus.treasure_box )
            {
                eStatus = ePlaneStatus.idle;
            }

            plane.SetPlaneStatus(eStatus);

            if (eStatus == ePlaneStatus.running_on_airline) // 正在跑道上跑
            {
                lot.SetBoundPlaneAvatarVisible(true);

               

                plane.SetPosType(pos_type);
                plane.SetCurAngle(fCurAngle);
                int nShit = MapManager.s_Instance.GetCurDistrict().GetRunningPlanesList().Count;
                plane.BeginRun(0);

                vecTempPos.x = fPosX;
                vecTempPos.y = fPosY;
                vecTempPos.z = -1;
                plane.SetPos(vecTempPos);
                AirlineManager.s_Instance.CalculateAndSetStartMoveStatus(plane, plane.GetPos(), pos_type);
                plane.Run();
            }
            else
            {

            }


        } // end for i



    }

    // 打开“提升概览”面板
    public void OnClick_OpenRaiseDetail()
    {
        _panelRaiseDetail.SetActive(true);

        UpdateRaise();
    }

    public void UpdateRaise()
    {
        UpdateRaise(MapManager.s_Instance.GetCurPlanet(), MapManager.s_Instance.GetCurDistrict());
    }

    // poppin to do 统一一下规则，提升比例到底是直接乘还是“1+”
    public float UpdateRaise(Planet cur_planet, District cur_district, bool bOffline = false)
    {


        float nCurRaise = 1;



        if (cur_planet == null || cur_district == null)
        {
            return nCurRaise;
        }
        //m_nCurRaise = 1;
        _txtRaiseDetail.text = "";
        string szInfo = "";

        // 统计当前的提升明细
        //  Planet cur_planet = MapManager.s_Instance.GetCurPlanet();
        //  District cur_district = MapManager.s_Instance.GetCurDistrict();
        int nPlanetId = cur_planet.GetId();
        int nDistrictId = cur_district.GetId();
        int nPrestigeTimes = cur_district.GetPrestigeTimes();

        bool bFirst = true;


        float fPrestigePromote = DataManager.s_Instance.GetPrestigePromote(nPlanetId, nDistrictId, nPrestigeTimes);
        if (fPrestigePromote > 0)
        {
            /*
            fPrestigePromote += 1f;
            nCurRaise *= fPrestigePromote;
            szInfo += "重生加成：X" + fPrestigePromote.ToString("f1") + "倍\n";

            bFirst = false;
            szInfo += "--------------------------------\n";
            */
        }

        /*
        //// 赛道加成
       
        float fTrackRaise = MapManager.s_Instance.GetCurDistrict().GetTrackRaise();

        float fRealTrackProomote = fTrackRaise;
        if (fPrestigePromote > 0)
        {
            fRealTrackProomote = fPrestigePromote;
            szInfo += ("赛道加成(重生" + nPrestigeTimes + "次)：" + (1 + fRealTrackProomote) + "倍\n");
        }
        else 
        {
            szInfo += ("赛道加成(未重生，是基础收益倍数)：" + (1 + fRealTrackProomote) + "倍\n");
        }
     
       

        szInfo += "--------------------------------\n";
        if (fRealTrackProomote > 0)
        {
            nCurRaise *= (1 + fRealTrackProomote);
        }
        //// end 赛道加成
        */
       // nCurRaise *= ( 1 + fPrestigePromote );





        // 道具强化
        float nItemRaise = 0;
        /*
        List<UIItem> lstUsingItems = ItemSystem.s_Instance.GetUsingItemList();
        for (int i = 0; i < lstUsingItems.Count; i++ )
        {
            if (bFirst)
            {
                szInfo += "  ";
            }

            bFirst = false;

            UIItem item = lstUsingItems[i];
            if ( item.m_BagItemConfig.nType != (int)ShoppinMall.eItemType.coin_raise )
            {
                continue;
            }

            float raise_of_this_item = item.GetFloatParam(0);
            szInfo += ("强化道具：X" + raise_of_this_item + "倍\n" );
            nItemRaise += raise_of_this_item;

            if (i != lstUsingItems.Count - 1)
            {
                szInfo += "+";
            }
            else
            {

            }
        } // end for lstUsingItems 

        if (lstUsingItems.Count > 1)
        {
            szInfo += "=" + nItemRaise + "倍\n";
        }
        // end 道具强化
        */

        //// 12.10版道具强化
        List<UIShoppinAndItemCounter> lstUsingItem = ItemSystem.s_Instance.GetCurUsingItemsList();
        for (int i = 0; i < lstUsingItem.Count; i++)
        {
            if (bFirst)
            {
                szInfo += "  ";
            }

            bFirst = false;

            UIShoppinAndItemCounter item = lstUsingItem[i];
            if (item.m_nItemType != (int)ShoppinMall.eItemType.coin_raise )
            {
                continue;
            }
            float raise_of_this_item = (1f + item.m_fValue);
            szInfo += ("强化道具：" + raise_of_this_item + "倍\n");
            nItemRaise += raise_of_this_item;

            if (i != lstUsingItem.Count - 1)
            {
                szInfo += "+";
            }
            else
            {

            }




        } // end i

        if (nItemRaise > 0)
        {
            szInfo += "=x" + (nItemRaise) + "倍\n";
        }


        /// end 12.10版道具强化





        szInfo += "--------------------------------\n";

        if (nItemRaise > 0)
        {
            nCurRaise/*m_nCurRaise*/ *= (nItemRaise);
        }

        if (bFirst)
        {
            // szInfo += "  ";
        }



        //// 重生加成
        /*
        int nPresitageRaise = DataManager.s_Instance.GetPrestigeGain(nPlanetId, nDistrictId, nPrestigeTimes);
        if (nPrestigeTimes > 0)
        {
            szInfo += "X" + nPresitageRaise + "倍  重生加成" + "\n";
            nCurRaise *= nPresitageRaise;

            bFirst = false;
        }
        */

        //// 主管加成
        float fScienceTreePromote = 0;
        float fInitPromote = 0;
        float fAdminCoinRaise = AdministratorManager.s_Instance.GetCoinRaise(ref fInitPromote, ref fScienceTreePromote);
        if (fAdminCoinRaise > 0)
        {
            szInfo += "主管：X" + (1 + fAdminCoinRaise).ToString("f1") + "倍";
            if (fScienceTreePromote > 0)
            {
                szInfo += "（原始值：提升" + (fInitPromote * 100).ToString("f0") + "%；天赋点加成：" + (fScienceTreePromote * 100).ToString("f0") + "%）";
            }
            szInfo += "\n";
            nCurRaise *= (1 + fAdminCoinRaise);

            bFirst = false;
        }
        szInfo += "--------------------------------\n";


        // “主动技能”的概念废弃。现在统一走“主管”流程
        // 主动技能加成
        if (true/*!bOffline*/) // 离线状态主动技能无效
        {
            Skill skill = cur_district/*MapManager.s_Instance.GetCurDistrict()*/.GetSkill(SkillManager.eSkillType.coin_raise);

            if (skill.GetStatus() == SkillManager.eSkillStatus.working)
            {
                float fSkillRaise = skill.m_Config.fValue;



                float fSKillRaiseRaise = ScienceTree.s_Instance.GetSkillCoinRaiseRaise();
                if (fSKillRaiseRaise > 0)
                {
                    fSkillRaise *= (1 + fSKillRaiseRaise);
                    szInfo += "X" + fSkillRaise + "倍  主动技能加成(含" + fSKillRaiseRaise + "技能树加成)\n";
                }
                else
                {
                    szInfo += "X" + fSkillRaise + "倍  主动技能加成" + "\n";
                }
                nCurRaise/*m_nCurRaise*/ *= fSkillRaise;

                bFirst = false;
            }
        } // end if (!bOffline)


        /*
        // 科技树加成
        float fScienceTreeRaise = ScienceTree.s_Instance.GetCoinRaise(nPlanetId, nDistrictId);
        if (fScienceTreeRaise > 0)
        {
                szInfo += "X" + fScienceTreeRaise + "倍  科技树加成" + "\n";
                nCurRaise *= fScienceTreeRaise;

                bFirst = false;
        }
      */

        // 广告收益
        string szAdsPromoteInfo = "";
        float fAdsRaise = cur_district/*MapManager.s_Instance.GetCurDistrict()*/.GetAdsRaise(ref szAdsPromoteInfo);
        if (fAdsRaise > 1f)
        {
            //  szInfo += "X" + fAdsRaise + "倍  广告收益" + "\n";
            szInfo += szAdsPromoteInfo;
            nCurRaise/*m_nCurRaise*/ *= fAdsRaise;
        }



        szInfo += "=====================\n";
        if (nCurRaise/*m_nCurRaise*/ > 1)
        {
            szInfo += "= " + m_nCurRaise + "倍";
        }

        if (cur_district == MapManager.s_Instance.GetCurDistrict())
        {
            _txtRaiseDetail.text = szInfo;
            m_nCurRaise = nCurRaise;
        }

        return nCurRaise;
    }

    // 不含“赛道加成”和“科技树加成”。也就是说常驻状态下的加成不计入这里（不显示）
    public float GetRaise()
    {
        return m_nCurRaise;
    }

    public float GetTotalRaise()
    {
        float nCurRaise = GetRaise();


        int nPlanetId = MapManager.s_Instance.GetCurPlanet().GetId();
        int nDistrictId = MapManager.s_Instance.GetCurDistrict().GetId();
        int nPrestigeTimes = MapManager.s_Instance.GetCurDistrict().GetPrestigeTimes();


        //// 赛道加成（含重生加成）

        float fPrestigePromote = DataManager.s_Instance.GetPrestigePromote(nPlanetId, nDistrictId, nPrestigeTimes);
        float fTrackRaise = MapManager.s_Instance.GetCurDistrict().GetTrackRaise();

        float fRealTrackProomote = 0;
        if (nPrestigeTimes > 0) //有重生
        {
            fRealTrackProomote = 1f + fPrestigePromote;
        }
        else  // 没有重生
        {
            fRealTrackProomote = 1f + fTrackRaise;


        }

        nCurRaise *= fRealTrackProomote;
        //// 科技树加成

        // 天赋线加成(1号天赋线是专门负责金币收益加成的)
        // 遍历1号天赋线的所有节点，把符合条件的加成项全部累加起来
        string szTalentCoinPromoteInfo = "";
        float fTalentPromote = ScienceTree.s_Instance.GetCoinPromote(MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId(), ref szTalentCoinPromoteInfo);
        if (fTalentPromote > 1f)
        {
            nCurRaise *= fTalentPromote;

        }

        //// end 科技树加成


        return nCurRaise;
    }

    public void OnClick_CloseRaiseDetail()
    {
        _panelRaiseDetail.SetActive(false);
    }

    float m_fAccelerate = 0f;
    float m_fPreAccerlateTimeLeft = 0;
    public void PreAccelerateAll(float fAccelerate)
    {
        m_fAccelerate = fAccelerate;

        m_fPreAccerlateTimeLeft = 1f;
    }

    void PreAccelerateLoop()
    {
        if (m_fPreAccerlateTimeLeft <= 0)
        {
            return;
        }

        m_fPreAccerlateTimeLeft -= Time.deltaTime;
        if (m_fPreAccerlateTimeLeft <= 0)
        {
            SkillManager.s_Instance.m_arySkillButton[0]._effectLight.EndPlay();
            SkillManager.s_Instance.m_arySkillButton[0]._effectLight.gameObject.SetActive(false);

            AccelerateAll(m_fAccelerate);

            for (int i = 0; i < MapManager.s_Instance.GetCurDistrict().GetRunningPlanesList().Count; i++)
            {

                Plane plane = MapManager.s_Instance.GetCurDistrict().GetRunningPlanesList()[i];
                //                EffectManager.s_Instance.DeleteEffect(plane.GetAccelerateLight());
                //  plane.SetAccelerateLight(null);


            }
        }

    }

    float m_fSpeedAccelerate = 0;
    public const int MAX_ACCELERATE = 2;
    float[] m_aryAccelerate = new float[MAX_ACCELERATE];
    public void AccelerateAll(float fAccelerate, int nIndex = 0)
    {
        MapManager.s_Instance.GetCurDistrict().AccelerateAll(fAccelerate, nIndex);
        return;
        /*
        m_aryAccelerate[nIndex] = fAccelerate;
        RefreshAccelerateTotal();

        for (int i = 0; i < MapManager.s_Instance.GetCurDistrict().GetRunningPlanesList().Count; i++)
        {
            Plane plane = MapManager.s_Instance.GetCurDistrict().GetRunningPlanesList()[i];
            plane.BeginAccelerate(100000f,  m_fSpeedAccelerate);
        }
        */
    }

    public float GetAccelerate_New()
    {
        District cur_track = MapManager.s_Instance.GetCurDistrict();
        if ( cur_track == null )
        {
            return 0;
        }
        return MapManager.s_Instance.GetCurDistrict().GetAccelerate_New();

    }

 

    public void StopAccelerateAll(int nIndex = 0)
    {

        MapManager.s_Instance.GetCurDistrict().StopAccelerateAll(nIndex);
    }

    public float GetSpeedAccelerate()
    {
        float fRet = 1f;

        if (MapManager.s_Instance.GetCurPlanet() == null)
        {
            return fRet;
        }



        if (m_fSpeedAccelerate > 0)
        {
            fRet = m_fSpeedAccelerate;
        }
        float fScienceSpeedRaise = ScienceTree.s_Instance.GetAccelerateRaise(MapManager.s_Instance.GetCurPlanet().GetId());

        if (fScienceSpeedRaise > 0)
        {
            fRet *= (1 + fScienceSpeedRaise);
        }

        float fItemRaise = ItemSystem.s_Instance.GetAutomobileSpeedAccelerateByItem();
        if (fItemRaise > 1)
        {
            fRet *= fItemRaise;
        }

        return fRet;
    }


    public List<Plane> GetRunningList()
    {
        return m_lstRunningPlanes;
    }

    bool m_bLostFocus = false;
    private object _panelAds;
    public double m_fOfflineProfit = 0;
    void OnApplicationFocus(bool hasFocus)
    {
        if (MapManager.s_Instance.GetCurDistrict() == null)
        {
            return;
        }

        if (hasFocus)// has focus
        {
            if (m_bLostFocus)
            {
                District district = MapManager.s_Instance.GetCurDistrict();
                district.OnLine();

                /*
                m_fOfflineProfit = district.CalculateOffLineProfit();
                if (m_fOfflineProfit > 0)
                {
                    _panelCollectOfflineProfit.SetActive(true);
                    _txtOfflineProfitOfThisDistrict.text = m_fOfflineProfit.ToString("f0");
                }
                */
            }
            m_bLostFocus = false;
        }
        else // lost focus
        {
            MapManager.s_Instance.GetCurDistrict().OffLine();
            m_bLostFocus = true;
            //DebugInfo.s_Instance._txtPlanetAndDistrictId.text = "到底有米有" + Main.GetTime();
        }
    }

    Vector3 vecTempPosLeft = new Vector3();
    Vector3 vecTempPosRight = new Vector3();
    public void CollectOfflineProfitAnimation( GameObject goTheStartPoint = null )
    {
        if (goTheStartPoint)
        {
            UIManager.s_Instance.m_vecCoinFlyStartPos = goTheStartPoint.transform.localPosition;
        }
        else
        {
            UIManager.s_Instance.m_vecCoinFlyStartPos = UIManager.s_Instance._goStart.transform.localPosition;
        }
        UIManager.s_Instance.m_vecCoinFlyEndPos = UIManager.s_Instance._goEnd.transform.localPosition;
        vecTempPosLeft = UIManager.s_Instance._goMiddleLeft.transform.localPosition;
        vecTempPosRight = UIManager.s_Instance._goMiddleRight.transform.localPosition;

        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_bank);

        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;

        Sprite spr = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(MapManager.s_Instance.GetCurPlanet().GetId());

       

        for (int i = 0; i < 30; i++)
        {
            UIFlyingCoin coin = ResourceManager.s_Instance.NewFlyingCoin();

            coin.SetSpr(spr);

            coin.transform.SetParent(UIManager.s_Instance._containerFlyingCoins.transform);

            coin.transform.localScale = vecTempScale;
            coin.transform.localPosition = UIManager.s_Instance.m_vecCoinFlyStartPos;

            coin.m_vecSession0End.x = UnityEngine.Random.Range(vecTempPosLeft.x, vecTempPosRight.x);
            coin.m_vecSession0End.y = UnityEngine.Random.Range(vecTempPosLeft.y, vecTempPosRight.y);

            coin.m_vecEndPos = UIManager.s_Instance.m_vecCoinFlyEndPos;

            float sX = coin.m_vecSession0End.x - coin.transform.localPosition.x;
            float sY = coin.m_vecSession0End.y - coin.transform.localPosition.y;
            float t = UIManager.s_Instance.m_fSession0Time;
            t = UnityEngine.Random.Range(t, t * 2);
            coin.m_vecSession0_A.x = 2f * sX / (t * t);
            coin.m_vecSession0_A.y = 2f * sY / (t * t);

            sX = UIManager.s_Instance.m_vecCoinFlyEndPos.x - coin.m_vecSession0End.x;
            sY = UIManager.s_Instance.m_vecCoinFlyEndPos.y - coin.m_vecSession0End.y;
            t = UIManager.s_Instance.m_fSession1Time;
            t = UnityEngine.Random.Range(t, t * 2);
            coin.m_vecSession1_A.x = 2f * sX / (t * t);
            coin.m_vecSession1_A.y = 2f * sY / (t * t);


            coin.BeginFly();
        } // end for

    }

    public void OnClick_CollectOfflineProfit()
    {
        Planet planet = MapManager.s_Instance.GetCurPlanet();
        planet.SetCoin(planet.GetCoin() + m_fOfflineProfit);

        MapManager.s_Instance.GetCurDistrict().SetCurTotalOfflineGain(0);
        _panelCollectOfflineProfit.SetActive(false);




        CollectOfflineProfitAnimation();
    }

    public void OnClick_CloseOfflinePanel()
    {
        _panelCollectOfflineProfit.SetActive(false);
    }


    public void OnClick_CollectOfflineProfit_WithAds()
    {
        AdsManager.s_Instance.SetAdsType(AdsManager.eAdsType.collect_offline_profit_watching_ads);
        AdsManager.s_Instance._panelAds.SetActive(true);
        _panelCollectOfflineProfit.SetActive(false);

        Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
        AdsManager.s_Instance.OnClick_CloseAds();

    }

    public void OnClick_CollectOfflineProfit_UsingDiamond()
    {
        int nCurDiamond = (int)AccountSystem.s_Instance.GetGreenCash();
        int nDiamondCost = OfflineManager.s_Instance.GetRealDiamondCost();
        if (nCurDiamond < nDiamondCost)
        {
            UIMsgBox.s_Instance.ShowMsg("现金不足");
            return;
        }
        nCurDiamond -= nDiamondCost;
        AccountSystem.s_Instance.SetGreenCash(nCurDiamond);


        AdsManager.s_Instance.SetAdsType(AdsManager.eAdsType.collect_offline_profit_using_diamond);
        AdsManager.s_Instance._panelAds.SetActive(true);
        _panelCollectOfflineProfit.SetActive(false);

        //   Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
        AdsManager.s_Instance.OnClick_CloseAds();

    }

    private void OnClick_CloseAds()
    {
        throw new NotImplementedException();
    }

    public void SetPrestigeTimes(int nValue)
    {
        _txtPrestigeTimes.text = nValue.ToString();
        _txtPrestigeTimes_Shadow.text = nValue.ToString();

        _starsPrestigeTimes.SetStarLevel(nValue);
    }

    public void SetDiamond(double nValue)
    {
        _txtDiamond.text = nValue.ToString("f0");
        _txtDiamond_Shadow.text = nValue.ToString("f0");
    }

    public void SetDebugInfo(string szDebugInfo)
    {
        _txtDebugInfo.text = szDebugInfo;
    }

    public struct sAutoRunLocation
    {
        public int nLocationIndex;
        public Vector3 vecSrc;
        public Vector3 vecDest;
        public Vector3 vecDir; // move dir
        public Vector3 vecShowDir; // show dir
        public float fRotateAngle;
        public float fDeltaAngleToNextNode;
        public float fDistanceToNextNode;
    };

    List<sAutoRunLocation> m_lstRunLocations = new List<sAutoRunLocation>();

    public void CalculateLocationsInfo()
    {
        ePosType e_pos_type = ePosType.left;
        List<sAutoRunLocation> lstPoints = null;
        List<GameObject> lsTemp = new List<GameObject>();
        foreach (Transform child in m_containerLocations.transform)
        {
            lsTemp.Add(child.gameObject);
            colorTemp = child.gameObject.GetComponent<SpriteRenderer>().color;
              colorTemp.a = 0;
            child.gameObject.GetComponent<SpriteRenderer>().color = colorTemp;

            JTDHSceneManager.s_Instance.DoAdjust(child.gameObject);


           

          
         


          
       }





        for (int i = 0; i < lsTemp.Count; i++)
        {
            GameObject goLocation = lsTemp[i];
            sAutoRunLocation location = new sAutoRunLocation();





           



            location.vecSrc = goLocation.transform.localPosition;
            GameObject goNextLocation = null;
            GameObject goPrevLocation = null;
            if (i == lsTemp.Count - 1)
            {
                goNextLocation = lsTemp[0];
            }
            else
            {
                goNextLocation = lsTemp[i + 1];
            }

            if (i == 0)
            {
                goPrevLocation = lsTemp[lsTemp.Count - 1];
            }
            else
            {
                goPrevLocation = lsTemp[i - 1];
            }
            location.nLocationIndex = i;
            location.vecDest = goNextLocation.transform.localPosition;
            location.vecDir = location.vecDest - location.vecSrc;
            location.vecDir.Normalize();
            location.vecShowDir = location.vecSrc - goPrevLocation.transform.localPosition;
            location.vecShowDir.Normalize();
            location.fRotateAngle = CyberTreeMath.Dir2Angle(location.vecDir.x, location.vecDir.y) - 90f;
            m_lstRunLocations.Add(location);


            if (goLocation.gameObject.tag == "start_point")
            {
                if (goLocation.gameObject == m_aryStartLocationPointOfEachSeg[(int)ePosType.left])
                {
                    e_pos_type = ePosType.left;
                    m_aryStartLocationInfoPointOfEachSeg[(int)ePosType.left] = location;
                }
                else if (goLocation.gameObject == m_aryStartLocationPointOfEachSeg[(int)ePosType.left_top])
                {
                    e_pos_type = ePosType.left_top;
                    m_aryStartLocationInfoPointOfEachSeg[(int)ePosType.left_top] = location;
                }
                else if (goLocation.gameObject == m_aryStartLocationPointOfEachSeg[(int)ePosType.top])
                {
                    e_pos_type = ePosType.top;
                    m_aryStartLocationInfoPointOfEachSeg[(int)ePosType.top] = location;
                }
                else if (goLocation.gameObject == m_aryStartLocationPointOfEachSeg[(int)ePosType.right_top])
                {
                    e_pos_type = ePosType.right_top;
                    m_aryStartLocationInfoPointOfEachSeg[(int)ePosType.right_top] = location;
                }
                else if (goLocation.gameObject == m_aryStartLocationPointOfEachSeg[(int)ePosType.right])
                {
                    e_pos_type = ePosType.right;
                    m_aryStartLocationInfoPointOfEachSeg[(int)ePosType.right] = location;
                }
                else if (goLocation.gameObject == m_aryStartLocationPointOfEachSeg[(int)ePosType.right_bottom])
                {
                    e_pos_type = ePosType.right_bottom;
                    m_aryStartLocationInfoPointOfEachSeg[(int)ePosType.right_bottom] = location;
                }
                else if (goLocation.gameObject == m_aryStartLocationPointOfEachSeg[(int)ePosType.bottom])
                {
                    e_pos_type = ePosType.bottom;
                    m_aryStartLocationInfoPointOfEachSeg[(int)ePosType.bottom] = location;
                }
                else if (goLocation.gameObject == m_aryStartLocationPointOfEachSeg[(int)ePosType.left_bottom])
                {
                    e_pos_type = ePosType.left_bottom;
                    m_aryStartLocationInfoPointOfEachSeg[(int)ePosType.left_bottom] = location;
                }


                if (!m_dicLocationPoints.TryGetValue(e_pos_type, out lstPoints) || lstPoints == null)
                {
                    lstPoints = new List<sAutoRunLocation>();
                }

              
            } // end if (goLocation.gameObject.tag == "start_point")

            lstPoints.Add(location);

            m_dicLocationPoints[e_pos_type] = lstPoints;





        }

        for (int i = 0; i < m_lstRunLocations.Count; i++)
        {

            sAutoRunLocation location = m_lstRunLocations[i];
            sAutoRunLocation next_location;
            if (i == m_lstRunLocations.Count - 1)
            {
                next_location = m_lstRunLocations[0];
            }
            else
            {
                next_location = m_lstRunLocations[i + 1];
            }
            location.fDeltaAngleToNextNode = next_location.fRotateAngle - location.fRotateAngle;
            float fDisToNextNode = Vector2.Distance(location.vecSrc, location.vecDest);
            location.fDistanceToNextNode = fDisToNextNode;
            m_lstRunLocations[i] = location;
        }



    }
    public sAutoRunLocation GetLocationInfo(ref int nIndex)
    {
        if (nIndex >= m_lstRunLocations.Count)
        {
            nIndex = 0;
        }
        return m_lstRunLocations[nIndex];
    }

    public void UnlockMoreAirlineCapacity()
    {
        m_StartBelt.UnlockMoreCapacity();
    }

    public void OnClick_OpenActivityPanel()
    {
        UIMsgBox.s_Instance.ShowMsg("活动系统尚未开放");

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.ShangChengAnNiu);
    }

    Dictionary<int, List<Vector2>> m_dicLotPosOfLevel = new Dictionary<int, List<Vector2>>();
    public void AdjustSomePosition()
    {
        CalculateLocationsInfo();
        CalculateLocationInfo_NewVer();
        JTDHSceneManager.s_Instance.DoAdjust(m_StartBelt.gameObject);
        JTDHSceneManager.s_Instance.DoAdjust(_moneyTrigger.gameObject);
        JTDHSceneManager.s_Instance.DoAdjust(_goRecycleBox);
        //    JTDHSceneManager.s_Instance.DoAdjust(MoneyTrigger.s_Instance.m_vecTiaoZiPos);

    
        for (int i = 0; i < m_aryLots.Length; i++ )
        {
            Lot lot = m_aryLots[i];
            JTDHSceneManager.s_Instance.DoAdjust(lot.gameObject);
        }

        foreach ( Transform child_root in m_aryLotsPositionPointsContainers.transform)
        {
            GameObject container = child_root.gameObject;
            if ( container == null )
            {
                continue;
            }

            int nKey = container.transform.childCount;
            List<Vector2> lstPos = new List<Vector2>();
            foreach ( Transform child in container.transform )
            {
                JTDHSceneManager.s_Instance.DoAdjust(child.gameObject);
                Vector2 pos = new Vector2();
                pos = child.transform.localPosition;
                lstPos.Add( pos );

            } //foreach ( Transform child in container.transform )

            m_dicLotPosOfLevel[nKey] = lstPos;
        } // end for  foreach ( Transform child_root in m_aryLotsPositionPointsContainers.transform)

       
    } // end AdjustSomePosition()

    public ePosType CheckPosType(Vector3 pos )
    {
        Main.ePosType eType = Main.ePosType.left;

        if ( pos.y > m_aryStartLocationPointOfEachSeg[(int)ePosType.left_top].transform.localPosition.y &&
            pos.x < m_aryStartLocationPointOfEachSeg[(int)ePosType.top].transform.localPosition.x
           
           )
        {
            eType = ePosType.left_top;
        }
        else if  (pos.y > m_aryStartLocationPointOfEachSeg[(int)ePosType.left_top].transform.localPosition.y &&
            pos.x >= m_aryStartLocationPointOfEachSeg[(int)ePosType.top].transform.localPosition.x &&
            pos.x < m_aryStartLocationPointOfEachSeg[(int)ePosType.right_top].transform.localPosition.x 
           )
        {
            eType = ePosType.top;
        }
        else if (pos.y > m_aryStartLocationPointOfEachSeg[(int)ePosType.left_top].transform.localPosition.y &&
            pos.x >= m_aryStartLocationPointOfEachSeg[(int)ePosType.right_top].transform.localPosition.x

           )
        {
            eType = ePosType.right_top;
        }
        else if (pos.y <= m_aryStartLocationPointOfEachSeg[(int)ePosType.right].transform.localPosition.y &&
            pos.y > m_aryStartLocationPointOfEachSeg[(int)ePosType.right_bottom].transform.localPosition.y &&
            pos.x >= m_aryStartLocationPointOfEachSeg[(int)ePosType.right_top].transform.localPosition.x

           )
        {
            eType = ePosType.right;
        }
        else if (pos.y <= m_aryStartLocationPointOfEachSeg[(int)ePosType.right_bottom].transform.localPosition.y &&
            pos.x > m_aryStartLocationPointOfEachSeg[(int)ePosType.bottom].transform.localPosition.x

           )
        {
            eType = ePosType.right_bottom;
        }
        else if (pos.y <= m_aryStartLocationPointOfEachSeg[(int)ePosType.right_bottom].transform.localPosition.y &&
            pos.x <= m_aryStartLocationPointOfEachSeg[(int)ePosType.bottom].transform.localPosition.x &&
            pos.x > m_aryStartLocationPointOfEachSeg[(int)ePosType.left_bottom].transform.localPosition.x

           )
        {
            eType = ePosType.bottom;
        }
        else if (pos.y < m_aryStartLocationPointOfEachSeg[(int)ePosType.left].transform.localPosition.y &&
            pos.x < m_aryStartLocationPointOfEachSeg[(int)ePosType.left_bottom].transform.localPosition.x

           )
        {
            eType = ePosType.left_bottom;
        }
        else if (pos.y >= m_aryStartLocationPointOfEachSeg[(int)ePosType.left].transform.localPosition.y &&
            pos.y < m_aryStartLocationPointOfEachSeg[(int)ePosType.left_top].transform.localPosition.y &&
            pos.x < m_aryStartLocationPointOfEachSeg[(int)ePosType.top].transform.localPosition.x

           )
        {
            eType = ePosType.left;
        }

        return eType;
    }

   

    public int GetLocationIndexByRealPosAndType( Plane plane, ePosType pos_type, Vector3 pos )
    {
        int nLocationIndex = 0;
        sAutoRunLocation location_info;
        location_info.nLocationIndex = 0;
        location_info.vecSrc = Vector3.zero;

        if (pos_type == ePosType.left ||
            pos_type == ePosType.top ||
            pos_type == ePosType.right ||
            pos_type == ePosType.bottom  )
        {
            location_info = m_aryStartLocationInfoPointOfEachSeg[(int)pos_type];
        }


        switch(pos_type)
        {
            case ePosType.right:
                {
                    vecTempPos = location_info.vecSrc;
                    vecTempPos.y = pos.y;
                    plane.SetPos(vecTempPos);


                    plane.transform.localRotation = Quaternion.identity;
                    plane.transform.Rotate(0.0f, 0.0f, -180);

                }
                break;

            case ePosType.left:
                {
                    vecTempPos = location_info.vecSrc;
                    vecTempPos.y = pos.y;
                    plane.SetPos(vecTempPos);


                   // plane.transform.localRotation = Quaternion.identity;
                   // plane.transform.Rotate(0.0f, 0.0f, 180);

                }
                break;

            case ePosType.top:
                {
                    vecTempPos = location_info.vecSrc;
                    vecTempPos.x = pos.x;
                    plane.SetPos(vecTempPos);


                    plane.transform.localRotation = Quaternion.identity;
                    plane.transform.Rotate(0.0f, 0.0f, 90);

                }
                break;
            case ePosType.bottom:
                {
                    vecTempPos = location_info.vecSrc;
                    vecTempPos.x = pos.x;
                    plane.SetPos(vecTempPos);


                    plane.transform.localRotation = Quaternion.identity;
                    plane.transform.Rotate(0.0f, 0.0f, -90);

                }
                break;

            case ePosType.left_top:
            case ePosType.right_top:
            case ePosType.left_bottom:
            case ePosType.right_bottom:
                {
                    float fPercentOfAngle = 0f; 
                    location_info = GetNearestLocation(pos_type, pos, ref fPercentOfAngle);
                    plane.SetPos(selected_one.vecSrc);

                    float fCurAngle = 90f * fPercentOfAngle;
                    plane.SetCurAngle(fCurAngle);

                }
                break;

        } // end switch

        nLocationIndex = location_info.nLocationIndex;
        return nLocationIndex;
    }

    sAutoRunLocation selected_one;
    public sAutoRunLocation GetNearestLocation(ePosType pos_type, Vector3 pos, ref float fPercentOfAngle)
    {
       
        List<sAutoRunLocation> lstLocationInfo = null;
        if ( !m_dicLocationPoints.TryGetValue(pos_type, out lstLocationInfo) )
        {
            
        }

        float fCurMinDis = 0f;
        bool bFirst = true;
        for (int i = 0; i < lstLocationInfo.Count; i++ )
        {
            sAutoRunLocation location_info = lstLocationInfo[i];
            if ( bFirst )
            {
                selected_one = location_info;
                fCurMinDis = Vector2.Distance( pos, location_info.vecSrc);
                bFirst = false;
                continue;
            }
           
            float fDis = Vector2.Distance(pos, location_info.vecSrc);
            if ( fDis < fCurMinDis)
            {
                fPercentOfAngle = (float)i / (float)lstLocationInfo.Count;
                selected_one = location_info;
                fCurMinDis = fDis;
            }



        } // end for

        return selected_one;
    }

 
    double m_dShowingNumber = 0d;
    double m_dNumberChangeSpeed = 0d;
    public float m_fCoinChangeTime = 1f;
    double m_dDestCoinNumber = 0d;
    public void CoinChanged( double dCurCoin )
    {
        if (m_dShowingNumber == dCurCoin)
        {
            m_dNumberChangeSpeed = 0;
            return;
        }
        m_dNumberChangeSpeed = (dCurCoin - m_dNumberChangeSpeed) / m_fCoinChangeTime;
        m_dDestCoinNumber = dCurCoin;

    }

    void CoinNumberChangeLoop()
    {
        return;
        if (m_dNumberChangeSpeed == 0)
        {
            return;
        }

        m_dShowingNumber += m_dNumberChangeSpeed * Time.deltaTime;
        if (m_dNumberChangeSpeed > 0)
        {
            if (m_dShowingNumber >= m_dDestCoinNumber)
            {
                m_dShowingNumber = m_dDestCoinNumber;
                m_dNumberChangeSpeed = 0;
            }
        }
        else if (m_dNumberChangeSpeed < 0)
        {
            if (m_dShowingNumber <= m_dDestCoinNumber)
            {
                m_dShowingNumber = m_dDestCoinNumber;
                m_dNumberChangeSpeed = 0;
            }
        }

        _txtTotalCoinOfThisPlanet.text = CyberTreeMath.GetFormatMoney(m_dShowingNumber);
    }



} // end class
