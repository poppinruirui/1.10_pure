﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class AdventureManager_New : MonoBehaviour
{
    public static AdventureManager_New s_Instance = null;

    static Vector3 vecTempScale = new Vector3();

    public Sprite[] m_aryAdventureQualityBg;
    public Color[] m_aryAdventureQualityColor;
    public string[] m_aryAdventureName;

    public GameObject _panelAdventure;
    public GameObject _subpanelSelectAdventure;
    public GameObject _subpanelAdventuring;
    public GameObject _subpanelCollect;

    public UIAdventureCounter_New _counterAdventuring;

    /// <summary>
    /// UI
    /// </summary>
    public Text _txtRefreshColddown;
    public Text _txtWatchAdsColddown;
    public Text _txtWatchAdsReduce;
    public GameObject _containerPlay;
    public Image _imgPlay;
    public Text _txtTimeLeft;

    public UIAdventureCounter_New[] m_aryAdventureCounters;

    public Button _btnCollect;
    public Button _btnWatchAds;

    // config
    public const int ADVENTURE_NUM = 3;

    float m_fAdsReduceTime = 0f;
    float m_fAdsColdDownTotalTime = 0f;
    float m_fRefreshColddownTime = 0;

    // real time data
    int m_nRealTimeTotalTime = 0;
    int m_nRealAdsColdDownTotalTime = 0;
    int m_nRealRefreshColdDownTotalTime = 0;

    int m_bAdventuring = 0; // 0 - 未探险  1 - 探险中  2 - 探险结束(未领取奖励)。  注：领取奖励之后就直接进入“未探险”状态了 
    System.DateTime m_dtAdventureStartTime;         // 探险开始的时刻
    System.DateTime m_dtRefreshColddownStartTime;   // “重玩探险”Colddown开始时间
    System.DateTime m_dtWatchAdsColdDownStartTime;// 看广告Colddown开始的时刻

    bool m_bCollecting = false;
    bool m_bRefreshColddowning = false;
    bool m_bWatchAdsColddowning = false;

    int m_nSelectedAdentureCounterIndex = 0;

    public enum eAdventureQuality
    {
        low = 3,  // 普通
        middle,    // 稀有
        high,    // 史诗
    };

    public struct sRewardConfig
    {
        public int nItemId; // 道具ID号，对应于Item.csv表
        public int nMaxNum; // 最大刷出个数
        public int nMinNum; // 最小刷出个数
        public int nNum;    // 实时刷出个数
    };

    public struct sAdventureConfig
    {
        public eAdventureQuality eQuality; // 品质
        public string szDesc; // 描述
        public float fDuration; // 探险时长
        public int nProbability; // 刷出几率（权重）
        public List<sRewardConfig> lstMainRewardPool; // 主奖励池
        public List<sRewardConfig> lstViceRewardPool; // 副奖励池
        public int nViceRewardNum; // 副奖励刷出格数(每一格还有自己的个数)
    };

    Dictionary<eAdventureQuality, sAdventureConfig> m_dicAdventureConfig = new Dictionary<eAdventureQuality, sAdventureConfig>();

    List<eAdventureQuality> m_lstProbility = new List<eAdventureQuality>(); // 权重池

    bool m_bIsShowingAdventurePanel = false;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        string szConfigFileName_Adventure = DataManager.url + "Adventure.csv";
        StartCoroutine(LoadConfig_Adventure(szConfigFileName_Adventure));
        //LoadConfigOffline_Adventure(szConfigFileName_Adventure);

        m_fFadeScaleSpeed = 1f / m_fCollectAvatarFadeTime;
    }

    public static void CopyRewardData(ref sRewardConfig src, ref sRewardConfig dest)
    {
        dest.nItemId = src.nItemId;
        dest.nNum = src.nNum;
        dest.nMaxNum = src.nMaxNum;
        dest.nMinNum = src.nMinNum;
    }


    // Update is called once per frame
    float m_fTimeElapse = 0f;
    void Update()
    {
        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse < 1f)
        {
            return;
        }
        m_fTimeElapse = 0;

        AdventuringLoop();

        RefreshColdDownLoop();
        WatchAdsColdDownLoop();

    }

    void RefreshColdDownLoop()
    {
        if ( !m_bRefreshColddowning )
        {
            return;
        }

        int nTimeElapse = (int)(Main.GetSystemTime() - m_dtRefreshColddownStartTime).TotalSeconds;
        int nTimeLeft = m_nRealRefreshColdDownTotalTime - nTimeElapse;
        _txtRefreshColddown.text = CyberTreeMath.FormatTime(nTimeLeft, true);
        if (nTimeLeft <= 0)
        {
            m_bRefreshColddowning = false;
            _txtRefreshColddown.text = "重玩探险";
        }

    }

    void WatchAdsColdDownLoop()
    {
        if ( !m_bWatchAdsColddowning )
        {
            return;
        }

        int nTimeElapse = (int)(Main.GetSystemTime() - m_dtWatchAdsColdDownStartTime).TotalSeconds;
        int nTimeLeft = m_nRealAdsColdDownTotalTime - nTimeElapse;
        _txtWatchAdsColddown.text = CyberTreeMath.FormatTime(nTimeLeft, true);
        if (nTimeLeft <= 0)
        {
            m_bWatchAdsColddowning = false;
            _txtWatchAdsColddown.text = "-" + CyberTreeMath.FormatTime( (int)m_fAdsReduceTime );
        }
    }

    private void FixedUpdate()
    {
        CollectLoop();
    }

    void AdventuringLoop()
    {
        if ( m_bAdventuring != 1 )
        {
            return;
        }

        int nTimeElapse = (int)(Main.GetSystemTime() - m_dtAdventureStartTime).TotalSeconds;
        int nTimeLeft = m_nRealTimeTotalTime - nTimeElapse;
        _txtTimeLeft.text = CyberTreeMath.FormatTime(nTimeLeft, true);
        if (nTimeLeft <= 0)
        {
            EndAdventure();
        }
       

    }

    public void WatchAdsReduceTime()
    {
        m_bWatchAdsColddowning = true;
        m_dtWatchAdsColdDownStartTime = Main.GetSystemTime();
        m_nRealAdsColdDownTotalTime = (int)m_fAdsColdDownTotalTime;

        m_nRealTimeTotalTime -= (int)m_fAdsReduceTime;

        _containerPlay.SetActive(false);
        _txtWatchAdsColddown.gameObject.SetActive( true );
        AdsManager.s_Instance.PlayAds();
        SaveData();
    }

    public bool IsWatchAdsColdDowning()
    {
        return m_bWatchAdsColddowning;
    }

    public void OnClick_Refresh()
    {

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.ChongWanTanXianAnNiu);

        if (m_bRefreshColddowning)
        {
            UIMsgBox.s_Instance.ShowMsg("功能冷却中。。。");
            return;
        }

        UIMsgBox.s_Instance.ShowMsg("是否重玩探险", UIMsgBox.eCallBackEventType.refresh_adventrue, 3);

    }

    public void ReallyDoRefresh()
    {

        // 执行“重玩探险（刷新）”的冷却
        m_dtRefreshColddownStartTime = Main.GetSystemTime();
        m_bRefreshColddowning = true;
        m_nRealRefreshColdDownTotalTime = (int)m_fRefreshColddownTime;

        DoRefresh();
    }

    List<sRewardConfig> lstRewardTotalTemp = new List<sRewardConfig>();
    List<sRewardConfig> lstRewardSelectedTemp = new List<sRewardConfig>();
    public void DoRefresh()
    {
        Dictionary<eAdventureQuality, int> dicTemp = new Dictionary<eAdventureQuality, int>();
        for (int i = 0; i < ADVENTURE_NUM; i++)
        {
            int nIndex = CyberTreeMath.GetRandomValue(0, m_lstProbility.Count );
            eAdventureQuality eQua = m_lstProbility[nIndex];

            sAdventureConfig config = m_dicAdventureConfig[eQua];

            // 随机挑选出主奖励
            nIndex = CyberTreeMath.GetRandomValue(0, config.lstMainRewardPool.Count);
            sRewardConfig main_reward = config.lstMainRewardPool[nIndex];
            main_reward.nNum = CyberTreeMath.GetRandomValue(main_reward.nMinNum, main_reward.nMaxNum + 1);

            // 随机挑选出副奖励（多个)
            lstRewardTotalTemp.Clear();
            lstRewardSelectedTemp.Clear();
            for (int j = 0; j < config.lstViceRewardPool.Count; j++ )
            {
                lstRewardTotalTemp.Add(config.lstViceRewardPool[j]);
            } // end for j

            for (int j = 0; j < config.nViceRewardNum; j++ )
            {
                nIndex = CyberTreeMath.GetRandomValue( 0, lstRewardTotalTemp.Count);
                sRewardConfig vice_reward = lstRewardTotalTemp[nIndex];
                vice_reward.nNum = CyberTreeMath.GetRandomValue(vice_reward.nMinNum, vice_reward.nMaxNum + 1);
                lstRewardSelectedTemp.Add(vice_reward);
                lstRewardTotalTemp.RemoveAt(nIndex);
            } // end j

            m_aryAdventureCounters[i].UpdateInfo( config, main_reward, lstRewardSelectedTemp, lstRewardSelectedTemp.Count);
        } // end for i

        SaveData();
    }

    public void LoadConfigOffline_Adventure(string szFileName)
    {
        StreamReader sr = new StreamReader(szFileName);
        string szAll = sr.ReadToEnd();         string[] aryLines = szAll.Split('\n');
        int nPointer = 0;

        string[] aryGeneralConfig = aryLines[1].Split(',');
        nPointer = 0;
        m_fAdsColdDownTotalTime = float.Parse(aryGeneralConfig[nPointer++]);
        m_fAdsReduceTime = float.Parse(aryGeneralConfig[nPointer++]);
        m_fRefreshColddownTime = float.Parse(aryGeneralConfig[nPointer++]);

        for (int i = (int)eAdventureQuality.low; i <= (int)eAdventureQuality.high; i++)
        {
            string[] aryParams = aryLines[i].Split(',');
            sAdventureConfig config = new sAdventureConfig();
            nPointer = 0;
            config.eQuality = (eAdventureQuality)i;
            config.szDesc = aryParams[nPointer++];
            config.fDuration = float.Parse(aryParams[nPointer++]);
            config.nProbability = int.Parse(aryParams[nPointer++]);

            for (int j = 0; j < config.nProbability; j++)
            {
                m_lstProbility.Add((eAdventureQuality)i);
            }


            // main reward
            string[] aryMainRewardConfig = aryParams[nPointer++].Split('|');
            config.lstMainRewardPool = new List<sRewardConfig>();

            for (int j = 0; j < aryMainRewardConfig.Length; j++)
            {
                int nSubPointer = 0;
                string[] aryMainRewardParams = aryMainRewardConfig[j].Split('_');
                sRewardConfig reward_config = new sRewardConfig();
                reward_config.nItemId = int.Parse(aryMainRewardParams[nSubPointer++]);
                reward_config.nMinNum = int.Parse(aryMainRewardParams[nSubPointer++]);
                reward_config.nMaxNum = int.Parse(aryMainRewardParams[nSubPointer++]);
                config.lstMainRewardPool.Add(reward_config);

            } // end for j

            // vice reward
            string[] aryViceRewardConfig = aryParams[nPointer++].Split('|');
            config.lstViceRewardPool = new List<sRewardConfig>();

            for (int j = 0; j < aryViceRewardConfig.Length; j++)
            {
                int nSubPointer = 0;
                string[] aryViceRewardParams = aryViceRewardConfig[j].Split('_');
                sRewardConfig reward_config = new sRewardConfig();
                reward_config.nItemId = int.Parse(aryViceRewardParams[nSubPointer++]);
                reward_config.nMinNum = int.Parse(aryViceRewardParams[nSubPointer++]);
                reward_config.nMaxNum = int.Parse(aryViceRewardParams[nSubPointer++]);
                config.lstViceRewardPool.Add(reward_config);

            } // end for j

            config.nViceRewardNum = int.Parse(aryParams[nPointer++]);

            m_dicAdventureConfig[(eAdventureQuality)i] = config;

        } // end for i

        m_bAdventureConfigLoaded = true;
        DataManager.s_Instance.TryLoadMyDataCurTrackPlanesData();

    } // end LoadConfigOffline_Adventure

    public bool m_bAdventureConfigLoaded = false;
    IEnumerator LoadConfig_Adventure(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; // 等待下载

        int nPointer = 0;

        string[] aryLines = www.text.Split('\n');

        string[] aryGeneralConfig = aryLines[1].Split(',');
        nPointer = 0;
        m_fAdsColdDownTotalTime = float.Parse(aryGeneralConfig[nPointer++]);
        m_fAdsReduceTime = float.Parse(aryGeneralConfig[nPointer++]);
        m_fRefreshColddownTime = float.Parse(aryGeneralConfig[nPointer++]);

        for (int i = (int)eAdventureQuality.low; i <= (int)eAdventureQuality.high; i++)
        {
            string[] aryParams = aryLines[i].Split(',');
            sAdventureConfig config = new sAdventureConfig();
            nPointer = 0;
            config.eQuality = (eAdventureQuality)i;
            config.szDesc = aryParams[nPointer++];
            config.fDuration = float.Parse(aryParams[nPointer++]);
            config.nProbability = int.Parse(aryParams[nPointer++]);

            for (int j = 0; j < config.nProbability; j++ )
            {
                m_lstProbility.Add( (eAdventureQuality)i );
            }


            // main reward
            string[] aryMainRewardConfig = aryParams[nPointer++].Split( '|' );
            config.lstMainRewardPool = new List<sRewardConfig>();
        
            for (int j = 0; j < aryMainRewardConfig.Length; j++ )
            {
                int nSubPointer = 0;
                string[] aryMainRewardParams = aryMainRewardConfig[j].Split( '_' );
                sRewardConfig reward_config = new sRewardConfig();
                reward_config.nItemId = int.Parse(aryMainRewardParams[nSubPointer++]);
                reward_config.nMinNum = int.Parse(aryMainRewardParams[nSubPointer++]);
                reward_config.nMaxNum = int.Parse(aryMainRewardParams[nSubPointer++]);
                config.lstMainRewardPool.Add(reward_config);

            } // end for j

            // vice reward
            string[] aryViceRewardConfig = aryParams[nPointer++].Split('|');
            config.lstViceRewardPool = new List<sRewardConfig>();
       
            for (int j = 0; j < aryViceRewardConfig.Length; j++)
            {
                int nSubPointer = 0;
                string[] aryViceRewardParams = aryViceRewardConfig[j].Split('_');
                sRewardConfig reward_config = new sRewardConfig();
                reward_config.nItemId = int.Parse(aryViceRewardParams[nSubPointer++]);
                reward_config.nMinNum = int.Parse(aryViceRewardParams[nSubPointer++]);
                reward_config.nMaxNum = int.Parse(aryViceRewardParams[nSubPointer++]);
                config.lstViceRewardPool.Add(reward_config);

            } // end for j

            config.nViceRewardNum = int.Parse(aryParams[nPointer++]);

            m_dicAdventureConfig[(eAdventureQuality)i] = config;

        } // end for i

        m_bAdventureConfigLoaded = true;
        DataManager.s_Instance.TryLoadMyDataCurTrackPlanesData();



    

    } // end LoadConfig_Adventure

    public void LaodData()
    {
        string szKey = "Adventure";
        string szData = DataManager.s_Instance.GetMyData_String(szKey);

        // 如果没有发现存档，就直接刷新一次。
        if (!DataManager.IsDataValid(szData))
        {
            DoRefresh();
            return;
        }

        string[] aryData = szData.Split( '|' );

        // general data
        string[] aryGeneralData = aryData[0].Split( '_' );
        int nPointrer = 0;
        m_bAdventuring = int.Parse(aryGeneralData[nPointrer++]);
        m_nSelectedAdentureCounterIndex = int.Parse(aryGeneralData[nPointrer++]);
        System.DateTime dtAdventureStartTime = System.DateTime.Parse(aryGeneralData[nPointrer++]);
        m_bRefreshColddowning = (int.Parse(aryGeneralData[nPointrer++]) == 1) ?true:false;
        m_dtRefreshColddownStartTime = System.DateTime.Parse(aryGeneralData[nPointrer++]);
        m_bWatchAdsColddowning = (int.Parse(aryGeneralData[nPointrer++]) == 1) ? true : false;
        m_dtWatchAdsColdDownStartTime = System.DateTime.Parse(aryGeneralData[nPointrer++]);
        int nRealTimeTotalTime = int.Parse(aryGeneralData[nPointrer++]);


        for (int i = 0; i < m_aryAdventureCounters.Length; i++)
        {
            string[] aryRewardData = aryData[i+1].Split( '_' );
            eAdventureQuality eQuality = (eAdventureQuality)int.Parse(aryRewardData[0]);

            sAdventureConfig configGeneral = m_dicAdventureConfig[eQuality];

            sRewardConfig main_reward_config = new sRewardConfig();
            lstRewardSelectedTemp.Clear();
            for (int j = 1; j < aryRewardData.Length; j++ )
            {
                nPointrer = 0;
                string[] aryItemData = aryRewardData[j].Split( ',' );
                int nItemId = int.Parse(aryItemData[nPointrer++]);
                int nNum = int.Parse(aryItemData[nPointrer++]);

                if ( j == 1 )
                {
                    main_reward_config.nItemId = nItemId;
                    main_reward_config.nNum = nNum;
                }
                else
                {
                    sRewardConfig vice_reward_config = new sRewardConfig();
                    vice_reward_config.nItemId = nItemId;
                    vice_reward_config.nNum = nNum;
                    lstRewardSelectedTemp.Add(vice_reward_config);
                }
 
            } // end for j

            m_aryAdventureCounters[i].UpdateInfo(configGeneral, main_reward_config, lstRewardSelectedTemp, lstRewardSelectedTemp.Count);

        } // end for i

        if (m_bAdventuring == 1)
        {
            BeginAdventure(m_aryAdventureCounters[m_nSelectedAdentureCounterIndex]);
            m_nRealTimeTotalTime = nRealTimeTotalTime;
            m_dtAdventureStartTime = dtAdventureStartTime;

        }
        else if( m_bAdventuring == 2 )
        {
            BeginAdventure(m_aryAdventureCounters[m_nSelectedAdentureCounterIndex]);
            m_nRealTimeTotalTime = nRealTimeTotalTime;
            m_dtAdventureStartTime = dtAdventureStartTime;

            EndAdventure();
        }

        m_nRealRefreshColdDownTotalTime = (int)m_fRefreshColddownTime;
        m_nRealAdsColdDownTotalTime = (int)m_fAdsColdDownTotalTime;
        if (m_bWatchAdsColddowning)
        {
            _containerPlay.SetActive( false );
            _txtWatchAdsColddown.gameObject.SetActive( true );
        }
    }

    public void SaveData()
    {
        string szKey = "Adventure";
        string szData = "";
        szData = m_bAdventuring + "_" + m_nSelectedAdentureCounterIndex + "_"  + m_dtAdventureStartTime + "_" + ( m_bRefreshColddowning?1:0 ) + "_" + m_dtRefreshColddownStartTime + "_" + ( m_bWatchAdsColddowning?1:0 ) + "_" + m_dtWatchAdsColdDownStartTime + "_" + m_nRealTimeTotalTime + "|";

        for (int i = 0; i < m_aryAdventureCounters.Length; i++ )
        {
            UIAdventureCounter_New counter = m_aryAdventureCounters[i];
            szData += (int)counter.m_GeneralConfig.eQuality + "_" ;
            szData += counter.m_MainRewardConfig.nItemId + "," + counter.m_MainRewardConfig.nNum + "_";
            for (int j = 0; j < counter.m_nViceNum; j++ )
            {
                sRewardConfig vice_reward = counter.m_lstViceRewardConfig[j];
                szData += vice_reward.nItemId + "," + vice_reward.nNum ; 
                if ( j != counter.m_nViceNum - 1)
                {
                    szData += "_";
                }
            } // end j

            if ( i != m_aryAdventureCounters.Length - 1)
            {
                szData += "|";
            }

        } // end i

        DataManager.s_Instance.SaveMyData(szKey, szData);
      //  Debug.Log(szData);
    }

    public void OnClick_CloseAdventurePanel()
    {
        _panelAdventure.SetActive( false );
        m_bIsShowingAdventurePanel = false;
    }

    public void OnClick_OpenAdventurePanel()
    {
        _panelAdventure.SetActive(true);
        m_bIsShowingAdventurePanel = true;

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.TanXianAnNiu);
    }


    public bool IsShowingAdventurePanel()
    {
        return m_bIsShowingAdventurePanel;
    }

    public void BeginAdventure( UIAdventureCounter_New counter, bool bReallyBegin = false )
    {
        if (bReallyBegin)
        {
            m_bWatchAdsColddowning = false;
        }

        _subpanelAdventuring.SetActive( true );
        _subpanelSelectAdventure.SetActive( false );

        m_bAdventuring = 1;

        m_nRealTimeTotalTime = (int)counter.m_GeneralConfig.fDuration;

        _counterAdventuring.Init(counter);

        m_dtAdventureStartTime = Main.GetSystemTime();

        _btnCollect.gameObject.SetActive( false );
        _btnWatchAds.gameObject.SetActive(true);

        _txtWatchAdsReduce.text = "-" + CyberTreeMath.FormatTime((int)m_fAdsReduceTime);
        _containerPlay.SetActive( true );
        _txtWatchAdsColddown.gameObject.SetActive( false );


        m_nSelectedAdentureCounterIndex = counter.m_nCounterIndex;

        SaveData();


    }

    void EndAdventure()
    {
        m_bAdventuring = 2;
        _txtTimeLeft.text = "完成";

        _btnCollect.gameObject.SetActive(true);
        _btnWatchAds.gameObject.SetActive(false);

        SaveData();
    }

    public Text _txtCollectDesc;
    public BaseScale _baseScaleAvatarContainer;
    public Image _imgCollectAvatar;
    public float m_fCollectAvatarShowTime = 2f;
    public float m_fCollectAvatarFadeTime = 0.5f;
    int m_nCollectIndex = 0;
    int m_bCollectShowStatus = 0; // 0 - Showing  1 - Fading
    float m_fCollectTimeElapse = 0f;
    float m_fFadeScaleSpeed = 0f;

    public void OnClick_Collect()
    {
        BeginCollect();
    }

    void BeginCollect()
    {
        m_bAdventuring = 0;
        m_bCollecting = true;
        m_nCollectIndex = 0;
        GenerateOneCollectShow();

        _subpanelAdventuring.SetActive( false );
        _subpanelCollect.SetActive(true);

        //////  获得奖励
        // 主奖励 
        sRewardConfig reward_config;
        reward_config = _counterAdventuring.m_MainRewardConfig;
        ItemSystem.s_Instance.AddToItembag(reward_config.nItemId, reward_config.nNum);

        // 副奖励
        List<sRewardConfig> lstViceReward = _counterAdventuring.m_lstViceRewardConfig;
        for (int i = 0; i < _counterAdventuring.m_nViceNum; i++ )
        {
            ItemSystem.s_Instance.AddToItembag(lstViceReward[i].nItemId, lstViceReward[i].nNum);
        }
        ///////////
        /// 
        SaveData();

        DoRefresh();
    }

    void GenerateOneCollectShow()
    {
        if (m_nCollectIndex > _counterAdventuring.m_nViceNum )
        {
            EndCollect();
            return;
        }

        sRewardConfig reward_config;
        if (m_nCollectIndex == 0)
        {
            reward_config = _counterAdventuring.m_MainRewardConfig;
        }
        else
        {
            reward_config = _counterAdventuring.m_lstViceRewardConfig[m_nCollectIndex-1];
        }

        _txtCollectDesc.text = _counterAdventuring.m_aryRewardCounters[m_nCollectIndex]._txtDesc.text;
        _imgCollectAvatar.sprite = _counterAdventuring.m_aryRewardCounters[m_nCollectIndex]._imgAvatar.sprite;

        vecTempScale.x = _imgCollectAvatar.preferredWidth * 2f;
        vecTempScale.y = _imgCollectAvatar.preferredHeight * 2f;
        _imgCollectAvatar.GetComponent<RectTransform>().sizeDelta = vecTempScale;


        m_bCollectShowStatus = 0;
        m_fCollectTimeElapse = 0;
        m_nCollectIndex++;

        vecTempScale.x = _baseScaleAvatarContainer.m_fInitScale;
        vecTempScale.y = _baseScaleAvatarContainer.m_fInitScale;
        vecTempScale.z = 1;
        _baseScaleAvatarContainer.transform.localScale = vecTempScale;
        _baseScaleAvatarContainer.BeginScale();
    }

    void CollectLoop()
    {
        if ( !m_bCollecting )
        {
            return;
        }

        m_fCollectTimeElapse += Time.fixedDeltaTime;
        if (m_bCollectShowStatus == 0) // showing
        {
            if (m_fCollectTimeElapse >= m_fCollectAvatarShowTime)
            {
                m_bCollectShowStatus = 1;
                m_fCollectTimeElapse = 0;

            }
        }
        else if (m_bCollectShowStatus == 1) // fading
        {
            float fScale = _baseScaleAvatarContainer.transform.localScale.x;
            fScale -= m_fFadeScaleSpeed * Time.fixedDeltaTime;
            vecTempScale.x = fScale;
            vecTempScale.y = fScale;
            vecTempScale.z = 1f;
            _baseScaleAvatarContainer.transform.localScale = vecTempScale;


            if (m_fCollectTimeElapse >= m_fCollectAvatarFadeTime)
            {
                m_bCollectShowStatus = 0;
                m_fCollectTimeElapse = 0;


                GenerateOneCollectShow();
            }
        }
    }

    void EndCollect()
    {
        m_bCollecting = false;

       
        _subpanelSelectAdventure.SetActive( true );
        _subpanelCollect.SetActive( false );
    }



} // end class
