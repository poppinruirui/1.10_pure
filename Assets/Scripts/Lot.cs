﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lot : MonoBehaviour {
     
    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public GameObject m_goMain;

    public SpriteRenderer _srPlusSign;
    bool m_bShowingPlusSign = false;

    public BaseScale _basescalePeng;

    Plane m_Plane = null;

    public SpriteRenderer m_srMain;
    public SpriteRenderer m_srBoundPlaneAvatar;
    public SpriteRenderer m_srBoundPlaneAvatarMask;
    public SpriteRenderer m_srFlyingMark;

    public GameObject m_goEffectCanMerge;

    public Collider2D _Trigger;

    public SpriteRenderer _srShadow;

    public int m_nId = 0;

    public EnviromentMask _enviromentMask = null;

    private void Awake()
    {
        vecTempPos.x = 0f;
        vecTempPos.y = 0f;
        vecTempPos.z = 900f;
        m_goMain.transform.localPosition = vecTempPos;
    }

    // Use this for initialization
    void Start () {
        _srShadow.gameObject.SetActive(false);

        _enviromentMask = GameObject.Instantiate( ResourceManager.s_Instance.m_preEnviromentMask).GetComponent<EnviromentMask>();
        _enviromentMask.transform.SetParent(this.transform);
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        _enviromentMask.transform.localScale = vecTempScale;
     //   _enviromentMask.transform.localPosition = Main.s_Instance.GetLocalPosOnLot();

        _enviromentMask.gameObject.SetActive( false );

       // m_srMain.gameObject.SetActive( false );
    }

    public void UpdateSomePresent()
    {
        _enviromentMask.transform.localPosition = Main.s_Instance.GetLocalPosOnLot();

        if ( MapManager.s_Instance.GetCurDistrict().IsOceanOrAir() )
        {
            vecTempPos = m_srMain.transform.localPosition;
            vecTempPos.y = Main.s_Instance.GetLocalPosOnLot().y;
            vecTempPos.z = -1000;
            m_srMain.transform.localPosition = vecTempPos;
        }
        else
        {
            vecTempPos = m_srMain.transform.localPosition;
            vecTempPos.y = 0;
            vecTempPos.z = -1000;
            m_srMain.transform.localPosition = vecTempPos;
        }


    }

    // Update is called once per frame
    void Update () {
		
	}

    public void Reset()
    {
      

    }

    public void SetBoundPlaneAvatarVisible( bool bVisible )
    {
        if (bVisible)
        {
            m_srBoundPlaneAvatar.sprite = m_Plane.GetAvatar();
            m_srBoundPlaneAvatarMask.sprite = m_srBoundPlaneAvatar.sprite;

            m_srBoundPlaneAvatar.transform.localPosition = Main.s_Instance.GetLocalPosOnLot();
            m_srBoundPlaneAvatar.color = Color.white;
            _enviromentMask.gameObject.SetActive(true);
            _enviromentMask._sm.sprite = m_Plane.GetAvatar();
            _enviromentMask._srEnviromnet.color = JTDHSceneManager.s_Instance.GetEnviromentMaskColor( MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId());
        }
        else
        {
            m_srBoundPlaneAvatar.sprite = null;
            m_srBoundPlaneAvatarMask.sprite = null;

            _enviromentMask.gameObject.SetActive( false );
        }
       
    }

    public void SetFlyingMark( bool bVisible )
    {
        m_srFlyingMark.gameObject.SetActive( bVisible );
    }

    public void SetPlane( Plane plane, bool bSetPos = true )
    {
        if (plane)
        {
            m_Plane = plane;

            m_Plane.transform.SetParent(this.transform);
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            m_Plane.transform.localScale = vecTempScale;
            if (bSetPos)
            {
                vecTempPos = Main.s_Instance.GetLocalPosOnLot();
                m_Plane.SetPos(vecTempPos);
            }
        

            m_Plane.SetLot(this);

            //_srShadow.gameObject.SetActive(  true);
            if (MapManager.s_Instance.GetCurDistrict().IsOceanOrAir())
            {
                _srShadow.gameObject.SetActive(false);
            }
            else
            {
                _srShadow.gameObject.SetActive(true);
            }
        }
        else
        {
            m_Plane = plane;
            SetBoundPlaneAvatarVisible( false );
            SetFlyingMark( false );
            _srShadow.gameObject.SetActive(false);

        }


       
    }

    public Plane GetPlane()
    {
        return m_Plane;
    }

    public bool IsTaken()
    {
        return GetPlane() != null;
    }

    private void OnMouseDown()
    {
        if ( UIManager.IsPointerOverUI() )
        {
            return;
        }

        // 如果该停机位绑定有正在飞的飞机，则召回。
        if ( m_Plane != null && m_Plane.GetPlaneStatus() == Main.ePlaneStatus.running_on_airline )
        {
            m_Plane.Rotate(0);
            m_Plane.transform.SetParent(this.transform);
            m_Plane.BeginRunToLot( this );
            m_Plane.EndRunOnAirline();
            Main.s_Instance.RemovePlaneFromeAirline( m_Plane );
        }

        // 如果是宝箱，则打开
    }

    public void SetEffectCanMergeVisible( bool bVisible )
    {
        m_goEffectCanMerge.SetActive( bVisible );
        if (bVisible)
        {
            m_goEffectCanMerge.GetComponent<BaseScale>().BeginScale();
        }
    }

    public int GetId()
    {
        return m_nId;

    }

    public void SetId( int nId )
    {
        m_nId = nId;
    }

    public void ShowPeng()
    {
        return;;

        _basescalePeng.gameObject.SetActive( true );
        _basescalePeng.BeginScale();
    }

    public void SetMainSpr( Sprite spr )
    {
        m_srMain.sprite = spr;
    }

    public void SetActive( bool bActive )
    {
        this.gameObject.SetActive(bActive);
    }

    public void SetPlusSign( bool bPlusSign )
    {
        m_bShowingPlusSign = bPlusSign;
        _srPlusSign.gameObject.SetActive(bPlusSign);
    }


    public void BeginShowCollision()
    {
        m_srMain.color = Color.red;
        if ( m_Plane != null )
        {
            m_Plane.BeginShowCollisionEffect();
        }
    }

    public void EndShowCollisonEffect()
    {
        m_srMain.color = Color.white;
        if (m_Plane != null)
        {
            m_Plane.EndShowCollisionEffect();
        }
    }

} // end class
