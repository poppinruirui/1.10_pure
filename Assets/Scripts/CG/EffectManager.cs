﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class EffectManager : MonoBehaviour {

    public static EffectManager s_Instance = null;

    public Material m_matBlackAndWhite;

    public enum eEffectType
    {
        merge_succeed, // 合成成功
        recylce,       // 销毁
        open_box,       // 开宝箱
        unlock_new_plane_explosion,

        firework0,
        firework1,
        firework2,
        firework3,
        firework4,
        firework5,

        accelerate_light,
    };

    public GameObject[] m_aryEffectPrefabs;


    private void Awake()
    {
        s_Instance = this;
    }

    List<GameObject> m_lstMergeAnimation = new List<GameObject>();
    int m_nMergeEffectIndex = 0;

    // Use this for initialization
    void Start () {

        for (int i = 0; i < 5; i++ )
        {
            GameObject effect = GameObject.Instantiate( m_aryEffectPrefabs[(int)eEffectType.merge_succeed] );
            m_lstMergeAnimation.Add(effect);
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject NewEffect( eEffectType eType )
    {
        if ( eType == eEffectType.merge_succeed )
        {
            m_nMergeEffectIndex++;
            if (m_nMergeEffectIndex>= m_lstMergeAnimation.Count)
            {
                m_nMergeEffectIndex = 0;
            }
            return m_lstMergeAnimation[m_nMergeEffectIndex];

        }

        return GameObject.Instantiate( m_aryEffectPrefabs[(int)eType] );
    }

    public void DeleteEffect( GameObject goEffect )
    {
        GameObject.Destroy( goEffect );
    }


}
