﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseShake : MonoBehaviour {

    public int m_nTotalTimes = 0; // 摇动次数
    public float m_fShakeInterval = 0; // 摇动间隔
    public float m_fShakeAngle = 0f; // 摇动角度

    int m_nStatus = 0; // 0 - none  1 - shaking   2 - loop wait
    int m_nCount = 0;

    float m_fTimeElapse = 0f;

    public bool m_bAutoPlay = false;
    public bool m_bLoop = false;

    public float m_fLoopWaitTime = 3f;

	// Use this for initialization
	void Start () {
        if (m_bAutoPlay)
        {
            BeginShake();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        ShakingLoop();
    }

    public void BeginShake()
    {
        m_nStatus = 1;
        m_nCount = 0;
    }

    void ShakingLoop()
    {
        if ( m_nStatus != 1 )
        {
            return;
        }
        m_fTimeElapse += Time.fixedDeltaTime;
        if ( m_fTimeElapse < m_fShakeInterval )
        {
            return;
        }
        m_fTimeElapse = 0;

        float fShakeAngle = m_fShakeAngle;
        if ( m_nCount % 2 == 0 )
        {
            fShakeAngle = -m_fShakeAngle;
        }
        m_nCount++;

        this.transform.localRotation = Quaternion.identity;
        this.transform.Rotate(0.0f, 0.0f, fShakeAngle);

        if ( m_nCount >= m_nTotalTimes )
        {
           
                EndShake();

        }
    }

    public void EndShake()
    {
        m_nStatus = 2;
        this.transform.localRotation = Quaternion.identity;
        this.transform.Rotate(0.0f, 0.0f, 0);
    }

    public int GetStatus()
    {
        return m_nStatus;
    }

    public void SetStatus( int nStatus)
    {
        m_nStatus = nStatus;
    }

}
