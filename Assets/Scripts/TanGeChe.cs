﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TanGeChe : MonoBehaviour {

    public Button _btnRecommend; // “推荐购买”按钮
    public Image _imgBtnRecommendSpr; // "推进购买"按钮的贴图

    // button spr
    public Sprite m_sprCoinBuyButton;
    public Sprite m_sprCashBuyButton;
    public Sprite m_sprWatchAdsFreeButton;

    public int m_nFreshGuideStatus = 0;

    public VerticalLayoutGroup _vlg;

    public GameObject _goFreeFlag;
    public GameObject _goBtnTanGeChe;

    public UIPriceOffPoPo _imgPriceOff;
    public UIPriceOffPoPo _imgPriceOff_1;

    public Vector2 m_vecPriceOffStartPos = Vector2.zero;
    public Vector2 m_vecPriceOffSummitPos = Vector2.zero;
    public Vector2 m_vecPriceOffEndPos = Vector2.zero;
    public Vector2 m_vecPriceOffStartPos_1 = Vector2.zero;
    public Vector2 m_vecPriceOffSummitPos_1 = Vector2.zero;
    public Vector2 m_vecPriceOffEndPos_1 = Vector2.zero;
    public float m_fPriceOffAniTime = 1f;
    Vector2 m_vecPriceOffAniSpeed = Vector2.zero;
    Vector2 m_vecPriceOffAniSpeed_1 = Vector2.zero;
    float m_fPriceOffAniA = 0f;
    float m_fPriceOffAniV0 = 0f;
    float m_fPriceOffAniA_1 = 0f;
    float m_fPriceOffAniV0_1 = 0f;


    public static Vector3 vecTempScale = new Vector3();
    public static Vector3 vecTempPos = new Vector3();
    public static Vector3 vecTempPos1 = new Vector3();
    static Color colorTemp = new Color();

    public enum eVehicleCoounterStatus
    {
        unlocked, // 已解锁
        track_level_not_achieve_this_automobile_level, // 赛道等级尚未达到该载具等级
        track_level_achieve_this_automobile_level,// 赛道等级已达到该载具等级
    };

    /// <summary>
    /// UI
    /// </summary>
    public Text _txtRecommendPrice;
    public Image _imgRecommendAvatar;
    public Image _imgRecommendCoinIcon;

    public GameObject _panelRecommendDetail;
    public Text _txtRecommendDetail;
    public Toggle _toggleNoPlane;
    public Toggle _toggleNoDrop;
    public InputField _inputTrackLevel;
    //// end UI

    public static TanGeChe s_Instance = null;

    public GameObject _goCounterList;
    List<UIVehicleCounter> m_lstVehicleCounters = new List<UIVehicleCounter>();
    Dictionary<string, List<UIVehicleCounter>> m_dicVehicleCounters = new Dictionary<string, List<UIVehicleCounter>>();

    public GameObject _panelTanGeChe;

    float m_fRealTimeDiscount = 0;


    public Sprite m_sprJianYingTemp;
    public Sprite m_sprLockedBuyButton;
    public Sprite m_spUnlockedBuyButton;

    public Color m_colorUnlocked;
    public Color m_colorLocked;

    public int m_nRecommendLowerAmount = 3;
    public int m_nRecommendHigherAmount = 5;

   

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {

        string szData = PlayerPrefs.GetString( "EngeryGuide");

        if (!int.TryParse(szData, out TanGeChe.s_Instance.m_nFreshGuideStatus))
        {
            TanGeChe.s_Instance.m_nFreshGuideStatus = 0;
        }


    }

    private void FixedUpdate()
    {
        PriceOffAnimation();
    }


    // Update is called once per frame
    void Update () {
        RandomEventLoop();

      

    }

    List<UIVehicleCounter> lstTempShowingCounters = new List<UIVehicleCounter>();
    List<UIVehicleCounter> lstTempShowingUnlockedCounters = new List<UIVehicleCounter>();

    public bool  CheckIfHigherThanOneOfTheEnergyLock( int nLevel, ref bool bEqualToEnergyLock)
    {
        bEqualToEnergyLock = false;

        Dictionary<int, ResearchManager.sResearchConfig> dicRzesearchConfig = ResearchManager.s_Instance.GetResearchConfig(MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId());

        // 遍历能源锁的配置
        foreach( KeyValuePair<int, ResearchManager.sResearchConfig> pair in dicRzesearchConfig)
        {
            // 判断这一级能源锁是否已经解锁(重要)
            ResearchCounter research_counter = MapManager.s_Instance.GetCurDistrict().GetResearchCounter(pair.Key);
            UIResearchCounterContainer research_container = null;
            m_dicShareUICountersContainer.TryGetValue( pair.Key, out research_container );

            if (research_counter.GetStatus() == ResearchManager.eResearchCounterStatus.unlocked){ // 这一级能源锁已经解锁
                if (research_container)
                {
                    research_container.gameObject.SetActive(false);
                }
                continue;
            }
            else{ // 这一级能源锁尚未解锁
                if (research_container)
                {
                    research_container.gameObject.SetActive(true);
                }
            }

            if ( nLevel == pair.Key )
            {
                bEqualToEnergyLock = true;
                return true;
            }
            else if (nLevel > pair.Key)
            {
                return true;
            }

        } // end foreach

        return false;
    }

    public bool CheckIfPrevEnergyNotUnlock()
    {

        return false;
    }

    public bool CheckIfEqualToResearchLevel( int nLevel )
    {
        Dictionary<int, ResearchManager.sResearchConfig> dicRzesearchConfig = ResearchManager.s_Instance.GetResearchConfig(MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId());
        ResearchManager.sResearchConfig tempConfig;
        return dicRzesearchConfig.TryGetValue( nLevel, out tempConfig);
    }

    public float GetPriceReducePercent()
    {
        float fAdminReduceCoinPricePercent = AdministratorManager.s_Instance.GetAutomobileCoinPriceReducePercent();
        return fAdminReduceCoinPricePercent;
    }

    Dictionary<int, UIResearchCounterContainer> m_dicShareUICountersContainer = new Dictionary<int, UIResearchCounterContainer>();
    public void LoadData( int nPlanetId, int nDistrictId )
    {
        lstTempShowingCounters.Clear();
        lstTempShowingUnlockedCounters.Clear();

        foreach( KeyValuePair<int, UIResearchCounterContainer> pair in m_dicShareUICountersContainer )
        {
            pair.Value.RecycleBoundResearchCounter();
           // pair.Value.gameObject.SetActive( true );
            pair.Value.SetContainerVisible( true );
        }

        Dictionary<string, DataManager.sAutomobileConfig> dicAutomobileConfig = DataManager.s_Instance.GetAutomobileConfig();

        int i = 0;
        //for (int i = 0; i < ResourceManager.MAX_VEHICLE_NUM; i++ )
        foreach( KeyValuePair<string, DataManager.sAutomobileConfig> pair in dicAutomobileConfig  )
        {
            // 判断该交通工具是否适用于当前赛。如果不适用就跳过，不展示在工厂界面
            string szCurTrackId = MapManager.s_Instance.GetCurPlanet().GetId() + "_" + MapManager.s_Instance.GetCurDistrict().GetId();
            if ( pair.Value.szSuitableTrackId != szCurTrackId )
            {
                continue;
            }

            UIVehicleCounter counter = null;

           // m_lstVehicleCounters = null;
           // string szListKey = nPlanetId + "_" + nDistrictId;

            // 条目是复用的，不会频繁的生成和销毁
            if (i < m_lstVehicleCounters.Count)
            {
                counter = m_lstVehicleCounters[i];
            }
            else
            {
                counter = ResourceManager.s_Instance.NewVehicleCounter();
                m_lstVehicleCounters.Add(counter);
                counter.transform.SetParent(_goCounterList.transform);
                vecTempScale.x = 1f;
                vecTempScale.y = 1f;
                vecTempScale.z = 1f;
                counter.transform.localScale = vecTempScale;

                //////初始化能源锁
                int nCurLevel = i + 1;
               
              

                if (CheckIfEqualToResearchLevel(nCurLevel))
                {

                    GameObject rcc = ResearchManager.s_Instance.NewResearchCounterContainer();

                    rcc.transform.SetParent(_goCounterList.transform);

                        vecTempScale.x = 1f;
                        vecTempScale.y = 1f;
                        vecTempScale.z = 1f;
                    rcc.transform.localScale = vecTempScale;
                   // rcc.gameObject.SetActive(false);
                    m_dicShareUICountersContainer[nCurLevel] = rcc.GetComponent<UIResearchCounterContainer>();


                }

                /////

            }




            counter.gameObject.SetActive( true );
            lstTempShowingCounters.Add( counter );

            int nLevel = pair.Value.nLevel; // 本交通工具的等级和“可解锁等级”之间并没有必然联系
            int nEnergyLevel = nLevel - 1;

            eVehicleCoounterStatus eStatus = eVehicleCoounterStatus.track_level_not_achieve_this_automobile_level;

            // 新机制：能源解锁的出现改为跟某载具可解锁等级没有直接关系 



            if ( FreshGuide.s_Instance.GetGuidType() == FreshGuide.eGuideType.first_in_game && FreshGuide.s_Instance.GetStatus() == 11 && nLevel == 2)
            {
                FreshGuide.s_Instance._paw.transform.SetParent( counter._btnBuy.transform );
                vecTempPos.x = 366f;
                vecTempPos.y = -54;
                FreshGuide.s_Instance._paw.transform.localPosition = vecTempPos;
            }



            bool bEqualToEnergyLock = false;
            bool bHigherThanResearchLevel = CheckIfHigherThanOneOfTheEnergyLock(nEnergyLevel, ref bEqualToEnergyLock);
                   if (bEqualToEnergyLock && MapManager.s_Instance.GetCurDistrict().GetLevel() > nEnergyLevel) // 载具等级恰好等于（策划配置的）能源锁应该出现的等级                     {                                                                   if ( CheckIfPrevEnergyNotUnlock() ) // 本来打算出现能源锁，但是前面的能源锁还没解开，所以本级能源锁不出现。                         {                          }                         else // 正式出现能源锁                         {                             ResearchCounter rc = MapManager.s_Instance.GetCurDistrict().GetResearchCounter(nEnergyLevel);                             UIResearchCounterContainer rc_container = m_dicShareUICountersContainer[nEnergyLevel];                             rc_container.gameObject.SetActive( true );                             rc.BindContainer(rc_container);                              rc.SetActive( true );                             counter.SetBoundResearchCounter(rc);

                             rc.RefreshStatusPresentation();
                    rc.UpdateRealData();
                    /*
                    if ( IsVisible() ) // in
                    {
                        if (m_nFreshGuideStatus == 1 || m_nFreshGuideStatus == 0)
                        {
                            // SetSpotlightSrcInfo(0.59f, 0.56f, 0.34f);
                            // SetSpotlightMoveInfo(0.59f, 0.56f, 0.09f, 1f);
                            FreshGuide.s_Instance.BeginMask(0.58f, 0.41f, 0.3f, 0.58f, 0.41f, 0.09f, 1f);



                            FreshGuide.s_Instance._paw.SetIsUI(true);
                            FreshGuide.s_Instance._paw.transform.SetParent( rc._btnBeginUnlocking.transform);
                            FreshGuide.s_Instance._paw._basescalePaw.enabled = true;
                            FreshGuide.s_Instance._paw.EndMove();
                            vecTempPos.x = 1.86f;
                            vecTempPos.y = -15f;
                            vecTempPos.z = 0;
                            FreshGuide.s_Instance._paw.transform.localPosition = vecTempPos;

                            vecTempScale.x = 0.7f;
                            vecTempScale.y = 0.7f;
                            FreshGuide.s_Instance._paw.transform.localScale = vecTempScale;

                            FreshGuide.s_Instance._faClick.transform.SetParent(rc._btnBeginUnlocking.transform);
                            FreshGuide.s_Instance._faClick.transform.localPosition = FreshGuide.s_Instance._paw.transform.localPosition;
                            FreshGuide.s_Instance._paw.SetAlpha(1f);
                            vecTempScale.x = 0.8f;
                            vecTempScale.y = 0.8f;
                            FreshGuide.s_Instance._faClick.transform.localScale = vecTempScale;

                            m_nFreshGuideStatus = 2;

                            vecTempPos = _vlg.transform.localPosition;
                            vecTempPos.y = 1454f;
                            _vlg.transform.localPosition = vecTempPos;

                            DataManager.s_Instance.SaveMyData( "EngeryGuide", m_nFreshGuideStatus);
                           
                        } // end if (m_nFreshGuideStatus != 2)
                    }
                   */




                }


            }                      

            // 赛道等级已经达到该载具“可解锁的等级”
            // 注意，这个“可解锁”的状态只是理论状态，还要看该载具是否受“能源研究机制”的限制
            if (MapManager.s_Instance.GetCurDistrict().GetLevel() >= pair.Value.nCanUnlockLevel) 
            {

                // 载具等级小于所有“能源锁”的等级。注意，要遍历所有能源锁，而不只是某一个。
                // 比如：6级出现能源锁，而玩家故意不去解锁，一直在主场景上合成，导致载具等级一直上升，超过了6级，甚至达到11级，出现第二个能源锁。
                // 是允许同时出现两个或多个能源锁的。
             //   bool bEqualToEnergyLock = false;
                if (bHigherThanResearchLevel/*CheckIfHigherThanOneOfTheEnergyLock(nEnergyLevel, ref bEqualToEnergyLock) */) // 载具等级大于其中任何一个能源锁的等级
                {
                    counter.SetUnlocked(false);//黑掉，不能购买

                }
                else // 载具等级小于所有能源锁的等级
                {
                    counter.SetUnlocked(true);// 解锁了，可以正常购买
                    eStatus = eVehicleCoounterStatus.unlocked;
                }


            }
            else// 赛道等级尚未达到该载具“可解锁的等级”
            {
                counter.SetUnlocked(false); // 该载具黑掉、不可购买
               
               


            } // end  if (MapManager.s_Instance.GetCurDistrict().GetLevel() >= pair.Value.nCanUnlockLevel)
             


            /*
              // 判断这个交通工具已解锁没有（当前赛道等级大于等于该交通工具解锁所需的等级，则自动解锁）
              if ( MapManager.s_Instance.GetCurDistrict().GetLevel() >= pair.Value.nCanUnlockLevel )
              {
                  counter.SetUnlocked(true);
                  lstTempShowingUnlockedCounters.Add(counter);

                  // 还要判断该等级在“能源研究系统”中解锁没有
                  ResearchCounter rc = MapManager.s_Instance.GetCurDistrict().GetResearchCounter();
                  if (rc != null && rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked )
                  {
                      if (nLevel > rc.GetLevel())
                      {
                          counter.SetUnlocked(false);
                      }
                      else if (nLevel == rc.GetLevel())
                      {
                          //rc.transform.SetParent(counter._containerResearchLock.transform);
                          counter.SetBoundResearchCounter(rc);
                          if (rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked)
                          {
                              rc.gameObject.SetActive(true);
                          }                       

                          vecTempScale.x = 1f;
                          vecTempScale.y = 1f;
                          vecTempScale.z = 1f;
                          rc.transform.localScale = vecTempScale;

                          vecTempPos.x = 0;
                          vecTempPos.y = 0;
                          vecTempPos.z = 0;
                          rc.transform.localPosition = vecTempPos;
                      }

                  }
              }
              else
              {
                  counter.SetUnlocked( false );
              }
              */

            int nBuyTimes = MapManager.s_Instance.GetCurDistrict().GetVehicleBuyTimeById( pair.Value.nLevel );
            double nInitialPrice = pair.Value.nStartPrice_CoinValue;//DataManager.s_Instance.GetVehiclePrice(nPlanetId, nDistrictId, nLevel);
            for (int j = 0; j < nBuyTimes; j++ )
            {
                nInitialPrice *= ( 1f + pair.Value.fPriceRaisePercentPerBuy );
            }
            double nRealPrice = nInitialPrice;

            // 主管的技能加成：可以降低售价
            //float fAdminReduceCoinPricePercent = AdministratorManager.s_Instance.GetAutomobileCoinPriceReducePercent();
            //nRealPrice *= (1f - fAdminReduceCoinPricePercent);
            float fPriceReducePercent = GetPriceReducePercent();
            nRealPrice *= (1f - fPriceReducePercent);

            if (fPriceReducePercent > 0 && counter.GetUnlocked() )
            {
                counter._containerPriceOff.SetActive( true );
                counter._txtPriceOff.text = (fPriceReducePercent * 100).ToString("f0") + "%";

                float fDiscount = (fPriceReducePercent * 100);

                counter._popoDiscount.gameObject.SetActive( true );
                counter._popoDiscount.SetText(fDiscount.ToString( "f0" ) + "% " + "↓");
            }
            else
            {
                counter._containerPriceOff.SetActive(false);

                counter._popoDiscount.gameObject.SetActive(false);
            }

            double nGain = pair.Value.nBaseGain ; // DataManager.s_Instance.GetCoinGainperRound(nPlanetId, nDistrictId, nLevel);
            float fVehiclerunTimePerRound = pair.Value.fBaseSpeed ;// DataManager.s_Instance.GetVehicleRunTimePerRound(nLevel );

            counter._imgProgressBar_Gain.fillAmount = pair.Value.fGainShowPercent;
            counter._imgProgressBar_Speed.fillAmount = pair.Value.fSpeedShowPercent;

            counter._txtName.text = pair.Value.szName;
            counter._txtInitialPrice.text = "原价：" + nInitialPrice.ToString();
            counter._txtSkillDiscount.text = "技能折扣：" + (0.1f * 100 ) + "%";
            //   counter._txtScienceDiscount.text = "科技树折扣：" + (fScienceDiscount * 100) + "%"; ;

            // 如果这个载具框绑定有能源锁，则把能源锁插入在它前面
            bool bShit = false;
            ResearchCounter research_counter = counter.GetBoundResearchCounter();
            if (research_counter && research_counter.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked )
            {
                /*
                research_counter.transform.SetParent(_goCounterList.transform);

                vecTempScale.x = 1f;
                vecTempScale.y = 1f;
                vecTempScale.z = 1f;
                research_counter.transform.localScale = vecTempScale;

                bShit = true;;
                */
            }
         

            counter._txtLevel.text = "lv." + nLevel;

            int nSprIndex = nLevel - 1;
            if (nSprIndex >= ResourceManager.s_Instance.m_aryParkingPlaneSprites.Length)
            {
                Debug.LogError("nSprIndex >= ResourceManager.s_Instance.m_aryParkingPlaneSprites.Length");
                nSprIndex = 0;
            }

            counter._imgAvatar.sprite = ResourceManager.s_Instance.GetParkingPlaneSpriteByLevel( MapManager.s_Instance.GetCurDistrict().GetId(), nLevel);// ResourceManager.s_Instance.m_aryParkingPlaneSprites[nSprIndex];

            counter._txtPrice.text = CyberTreeMath.GetFormatMoney(nRealPrice) ;//nRealPrice.ToString("f0");

            if ( !counter.GetUnlocked() )
            {
                counter._txtPrice.text = pair.Value.nCanUnlockLevel.ToString();
            }

            // 根据金币种类不同，显示的金币图标不同

            counter._imgCoinType.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId_New(pair.Value.eCoinType);//m_aryCoinIcon[(int)pair.Value.eCoinType];
            counter._imgBtnBuy.sprite = m_sprCoinBuyButton;


            counter._txtGain.text = CyberTreeMath.GetFormatMoney( nGain ) ;// nGain.ToString();
            counter._txtSpeed.text = fVehiclerunTimePerRound.ToString();

            counter.m_nPrice = nRealPrice;
            counter.m_nDiamondPrice = pair.Value.nPriceDiamond;
            counter.m_nVehicleLevel = nLevel;

            counter._txtUnLockLevel.text = pair.Value.nCanUnlockLevel.ToString();

            counter.m_Config = pair.Value;

            counter.m_bWatchAdFree = false;

            counter.m_eMoneyType = DataManager.eMoneyType.planet_0_coin;


            if (eStatus == eVehicleCoounterStatus.unlocked)
            {

            }
            else
            {
                if (MapManager.s_Instance.GetCurDistrict().GetLevel() >= nLevel)
                {
                    eStatus = eVehicleCoounterStatus.track_level_achieve_this_automobile_level;
                }
                else
                {
                    eStatus = eVehicleCoounterStatus.track_level_not_achieve_this_automobile_level;
                }
            }
            switch ( eStatus )
            {
                case eVehicleCoounterStatus.unlocked:
                    {
                        counter._containerUnlockLevelAvatar.SetActive(false);
                        counter._txtQuestionMark1.gameObject.SetActive( false );
                        counter._imgAvatar.color = Color.white;

                        counter._containerPrice.SetActive( true );
                        counter._containerUnlockLevelAvatar.SetActive( false );
                        counter._txtName.color = Color.white;
                    }
                    break;
                case eVehicleCoounterStatus.track_level_not_achieve_this_automobile_level:
                    {
                        counter._containerUnlockLevelAvatar.SetActive( true );
                        counter._txtQuestionMark1.gameObject.SetActive(true);
                        counter._imgAvatar.color = m_colorLocked;

                        counter._containerPrice.SetActive(false);
                        counter._containerUnlockLevelAvatar.SetActive(true);

                        counter._txtName.color = Color.white;
                        counter._txtName.text = "？？？？？？";
                        counter._imgUnlockLevelAvatar.gameObject.SetActive(true);
                        counter._txtQuestionMark0.gameObject.SetActive(true);


                        counter._imgUnlockLevelAvatar.sprite = ResourceManager.s_Instance.GetParkingPlaneSpriteByLevel( MapManager.s_Instance.GetCurDistrict().GetId(), pair.Value.nCanUnlockLevel ) ;//ResourceManager.s_Instance.m_aryParkingPlaneSprites[pair.Value.nCanUnlockLevel - 1];
                    }
                    break;
                case eVehicleCoounterStatus.track_level_achieve_this_automobile_level:
                    {
                        counter._containerUnlockLevelAvatar.SetActive(true);
                        counter._txtQuestionMark1.gameObject.SetActive(false);
                        counter._imgAvatar.color = Color.white;

                        counter._containerPrice.SetActive(true);
                        counter._containerUnlockLevelAvatar.SetActive(true);

                        counter._txtName.color = Color.white;

                        counter._imgUnlockLevelAvatar.gameObject.SetActive( false );
                        counter._txtQuestionMark0.gameObject.SetActive(false);

                        counter._txtPrice.text = pair.Value.nCanUnlockLevel.ToString();


                    }
                    break;

            } // end switch eStatus



            /* (废弃)这里流程有问题，重写
            // 还要判断该等级在“能源研究系统”中解锁没有
            ResearchCounter rc = MapManager.s_Instance.GetCurDistrict().GetResearchCounter();
            int nTheLockLevel = -1;
            if (rc != null && rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked)
            {
                nTheLockLevel = rc.GetLevel() + 1;
            }

           



            // 判断这个交通工具已解锁没有（当前赛道等级大于等于该交通工具解锁所需的等级，则自动解锁）
            if (MapManager.s_Instance.GetCurDistrict().GetLevel() >= pair.Value.nCanUnlockLevel) // 赛道达到了该载具的“可解锁等级”
            {
                // counter.SetUnlocked(true);
                // lstTempShowingUnlockedCounters.Add(counter);
                if (nTheLockLevel <= 0)
                {
                    counter.SetUnlocked(true);
                }
                else
                {
                    if (nLevel > nTheLockLevel) // 其等级大于能源锁等级
                    {
                        counter.SetUnlocked(false); // 不能解锁，黑掉
                    }
                    else if (nLevel < nTheLockLevel )
                    {
                        counter.SetUnlocked(true);
                    }
                    else
                    {
                        counter.SetUnlocked(false);
                        counter.SetBoundResearchCounter(rc);
                        if (rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked)
                        {
                            rc.gameObject.SetActive(true);
                        }

                        vecTempScale.x = 1f;
                        vecTempScale.y = 1f;
                        vecTempScale.z = 1f;
                        rc.transform.localScale = vecTempScale;

                        vecTempPos.x = 0;
                        vecTempPos.y = 0;
                        vecTempPos.z = 0;
                        rc.transform.localPosition = vecTempPos;
                    }


                 

                }
            }
 
            else// 赛道还没达到该载具可解锁等级
            {
                counter.SetUnlocked(false); 
            }
  */

          


            i++;
               } // end foreach


        int nMaxCoinBuyLevelIndex = 0;
        m_nCurWatchAdsFreeCounterIndex = -1;
        int nCount = 0;
        for (int iter = lstTempShowingCounters.Count - 1; iter >= 0; iter-- ) // 反向遍历已解锁的所有等级
        {
            UIVehicleCounter counter = lstTempShowingCounters[iter];
            if (counter.GetUnlocked() && iter != 0)
            {
                //// 如果是已解锁的倒数第一或第二个，则显示用现金购买 
                if (nCount == 0 || nCount == 1)
                {
                    counter._imgCoinType.sprite = ResourceManager.s_Instance.GetGreenCashSprite();//m_aryCoinIcon[(int)DataManager.eMoneyType.diamond];
                    counter._txtPrice.text = counter.m_nDiamondPrice.ToString();
                    counter.m_eMoneyType = DataManager.eMoneyType.diamond;

                    counter._imgBtnBuy.sprite = m_sprCashBuyButton;

                    counter._containerPriceOff.SetActive(false);
                }

                float fWatchAdsTimeElapse = 0f;
                if ( nCount == 4 && Activity.s_Instance.CanWatchAds( ref fWatchAdsTimeElapse) || (  FreshGuide.s_Instance.GetGuidType() == FreshGuide.eGuideType.first_in_game && FreshGuide.s_Instance.GetStatus() == 11 && iter == 1) )
                {
                    counter.m_bWatchAdFree = true;
                    counter._imgCoinType.sprite = ResourceManager.s_Instance.m_sprWatchAdIcon;
                    counter._txtPrice.text = "免费";                     m_nCurWatchAdsFreeCounterIndex = counter.m_nVehicleLevel;

                    counter._imgBtnBuy.sprite = m_sprWatchAdsFreeButton;

                    counter._containerPriceOff.SetActive(false);
                }

                if ( nCount == 0 )
                {
                    nMaxCoinBuyLevelIndex = iter;
                }
               
                nCount++;
            }

            if ( nCount >= 5 )
            {
                break;
            }
        }

        nMaxCoinBuyLevelIndex -= 2;
        if (nMaxCoinBuyLevelIndex < 0)
        {
            nMaxCoinBuyLevelIndex = 0;
        }
        MapManager.s_Instance.GetCurDistrict().SetMaxCoinBuyLevel(nMaxCoinBuyLevelIndex + 1);


    }
    int m_nCurWatchAdsFreeCounterIndex = -1;

    public int GetCurWatchAdsFreeCounterIndex()
    {
        return m_nCurWatchAdsFreeCounterIndex;
    }
     

    public float GetRealTimeDiscount()
    {
        return m_fRealTimeDiscount;
    }

    bool m_bIsVisible = false;
    public void OnClick_OpenTanGeChe()
    {
        m_bIsVisible = true;

        if (FreshGuide.s_Instance.GetGuidType() == FreshGuide.eGuideType.first_in_game && FreshGuide.s_Instance.GetStatus() == 10)
        {
            FreshGuide.s_Instance.SetStatus(11);
        }

        _panelTanGeChe.SetActive( true );

        //LoadData( MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId());
        UpdateCarMallInfo();

        AccountSystem.s_Instance.SetGeneralMoneyCountersVisible( true );

        UIManager.s_Instance.OnOpenUI();

        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.GongChangAnNiu);

       
    }

    public bool IsVisible()
    {
        return m_bIsVisible;
    }

    public void OnClick_CloseTanGeChe()
    {
        // AccountSystem.s_Instance.SetGeneralMoneyCountersVisible(false);
        m_bIsVisible = false;
        AccountSystem.s_Instance.CheckIfShutDownGeneralMoneyCounter();

        _panelTanGeChe.SetActive(false);


        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.GuanBiJieMianAnNiu);
    }

    public void UpdateCarMallInfo()
    {

        MapManager.s_Instance.GetCurPlanet().SetCoin(MapManager.s_Instance.GetCurPlanet().GetCoin());


      LoadData(MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId());
    }

    ////////// 以下为“随机事件”模块
    public float m_fRandomEventInterval = 20f;
    float m_fRandomEventTimeElapse = 0f;
    bool m_bRandomEventProcessing = false;
    void RandomEventLoop()
    {

        return;
        m_fRandomEventTimeElapse += Time.deltaTime;
        if (m_fRandomEventTimeElapse >= m_fRandomEventInterval)
        {
            ExecRandomEvent();
        }
    }

    public void ExecRandomEvent()
    {
        if (m_bRandomEventProcessing)
        {
            return;
        }

        if (lstTempShowingCounters.Count < 4)
        {
            return;
        }

        UIVehicleCounter counter = lstTempShowingCounters[lstTempShowingCounters.Count - 3];
        if ( counter.m_bWatchAdFree )
        {
            return;
        }

       
        counter.m_bWatchAdFree = true;
        counter._imgCoinType.sprite = ResourceManager.s_Instance.m_sprWatchAdIcon;
        counter._txtPrice.text = "免费";

        m_fRandomEventTimeElapse = 0f;
    }

    public int s_nRecommendCount = 0;
    public District.sLevelAndCost selected_one;
    public bool NeedCalculateRecommendInfo()
    {
        return s_nRecommendCount <= 0;
    }

    public void OnClick_Recommend()
    {
        AccountSystem.s_Instance.OnCoinChanged();
        AudioManager.s_Instance.PlaySE_New(AudioManager.eSe_New.TuiJianGouMaiAnNiu);

        // 先选择一个可用的泊位
        Lot lot = Main.s_Instance.GetAvailableLot();
        if (lot == null)
        {
            return;
        }

        selected_one = MapManager.s_Instance.GetCurDistrict().GetTheSelectOne();//.CalculateRecommendInfo();


        UIVehicleCounter the_counter = m_lstVehicleCounters[selected_one.nLevel - 1];
        double dRealBuyPrice = the_counter.m_nPrice;
//        Debug.Log( "real buy price = " + dRealBuyPrice);

        double dCurCoin = AccountSystem.s_Instance.GetCoin();
        if (dCurCoin < dRealBuyPrice/*selected_one.dCurPrice*/)
        {
            UIMsgBox.s_Instance.ShowMsg("金币不足");

            return;
        }

        Plane plane = null;
        bool bRet = Main.s_Instance.BuyOneVehicle(selected_one.nLevel, ref plane);
        if (!bRet)
        {
            UIMsgBox.s_Instance.ShowMsg("购买失败");
            return;
        }

        dCurCoin -= dRealBuyPrice/*selected_one.dCurPrice*/;
        AccountSystem.s_Instance.SetCoin(dCurCoin);

        if (plane)
        {
            plane._BaseScale.BeginScale();
        }
        s_nRecommendCount--;
        if (s_nRecommendCount < 0)
        {
            s_nRecommendCount = 5;
        }

        if ( FreshGuide.s_Instance.GetGuidType() == FreshGuide.eGuideType.first_in_game )
        {
            int nCount = FreshGuide.s_Instance.GetIntParam(0);
            nCount++;
            FreshGuide.s_Instance.SetIntParam(0, nCount);


        }
    
    }

    public double GetCurPrice( Planet planet, District track, int nLevel, ref int nCurBuyTimes )
    {
        double dPrice = 0d;

        Dictionary<string, DataManager.sAutomobileConfig> dicAutomobileConfig = DataManager.s_Instance.GetAutomobileConfig();
        string szKey = planet.GetId() + "_" + track.GetId() + "_" + nLevel;
//        Debug.Log("szKey="+ szKey);
        DataManager.sAutomobileConfig config = dicAutomobileConfig[szKey];
        dPrice = config.nStartPrice_CoinValue;


        int nCurBuyTimesOfThisLevel = track.GetVehicleBuyTimeById(nLevel);
        nCurBuyTimes = nCurBuyTimesOfThisLevel;
        for (int j = 0; j < nCurBuyTimesOfThisLevel; j++)
        {
            dPrice *= (1 + config.fPriceRaisePercentPerBuy);

        } // end for j

        return dPrice;
    }

    public bool m_bNoPlane = false;
    public bool m_bNoDrop = false;
    public void OnTogggleChanged_NoPlane()
    {
        m_bNoPlane = _toggleNoPlane.isOn;



    }

    public void OnTogggleChanged_NoDrop()
    {
        m_bNoDrop = _toggleNoDrop.isOn;



    }


    public void OnInputValueChanged_TrackLevel()
    {
        District cur_track = MapManager.s_Instance.GetCurDistrict();
        int nLevel = 1;
        if ( !int.TryParse( _inputTrackLevel.text, out nLevel ))
        {
            nLevel = 1;
        }
            
        if ( nLevel > cur_track.GetLevel())
        {
            cur_track.UpdateTrackLevel( nLevel );
        }
    }

    public void OnClick_OpenRecommendDetailPanel()
    {
        _panelRecommendDetail.SetActive( true );
    }

    public void OnClick_CloseRecommendDetailPanel()
    {
        _panelRecommendDetail.SetActive(false);
    }

    public void SetPriceOffStatus( int nStatus )
    {
        m_nPriceOffAnimationStatus = nStatus;
    }

    bool m_bPriceOffPoPoVisible = false;
    public  void SetPriceOffPoPoVisible(bool bVisible)
    {
        _imgPriceOff.gameObject.SetActive(bVisible);
        _imgPriceOff_1.gameObject.SetActive(bVisible);
    }

    public bool IsPriceOffPopoVisible()
    {
        return m_bPriceOffPoPoVisible;
    }

    int m_nPriceOffAnimationStatus = 0;
    public void BeginPriceOffAnimation(  )
    {
        TanGeChe.s_Instance.SetFreeFlagVisible( false );

        SetPriceOffPoPoVisible( true );
        SetPriceOffStatus(1);

        _imgPriceOff.transform.localPosition = m_vecPriceOffStartPos;

        float fDistanceX = m_vecPriceOffEndPos.x - m_vecPriceOffStartPos.x;
        m_vecPriceOffAniSpeed.x = fDistanceX / m_fPriceOffAniTime;

        float s = m_vecPriceOffSummitPos.y - m_vecPriceOffStartPos.y;
        float t = m_fPriceOffAniTime * 0.5f;
        m_fPriceOffAniA = CyberTreeMath.GetA(s, t);
        m_fPriceOffAniV0  = CyberTreeMath.GetV0(s, t);


        m_vecPriceOffAniSpeed.y = 0;
        /////
        /// 
        _imgPriceOff_1.transform.localPosition = m_vecPriceOffStartPos_1;

        fDistanceX = m_vecPriceOffEndPos_1.x - m_vecPriceOffStartPos_1.x;
        m_vecPriceOffAniSpeed_1.x = fDistanceX / m_fPriceOffAniTime;

        s = m_vecPriceOffSummitPos_1.y - m_vecPriceOffStartPos_1.y;
        t = m_fPriceOffAniTime * 0.5f;
        m_fPriceOffAniA_1 = CyberTreeMath.GetA(s, t);
        m_fPriceOffAniV0_1 = CyberTreeMath.GetV0(s, t);


        m_vecPriceOffAniSpeed_1.y = 0;




    }

    void PriceOffAnimation()
    {
        if (m_nPriceOffAnimationStatus == 0)
        {
            return;
        }


        vecTempPos = _imgPriceOff.transform.localPosition;

        vecTempPos.x += m_vecPriceOffAniSpeed.x * Time.fixedDeltaTime;
        vecTempPos.y += m_fPriceOffAniV0 * Time.fixedDeltaTime;
        m_fPriceOffAniV0 += m_fPriceOffAniA * Time.fixedDeltaTime;
        _imgPriceOff.transform.localPosition = vecTempPos;

        ///
        vecTempPos1 = _imgPriceOff_1.transform.localPosition;

        vecTempPos1.x += m_vecPriceOffAniSpeed_1.x * Time.fixedDeltaTime;
        vecTempPos1.y += m_fPriceOffAniV0_1 * Time.fixedDeltaTime;
        m_fPriceOffAniV0_1 += m_fPriceOffAniA_1 * Time.fixedDeltaTime;
        _imgPriceOff_1.transform.localPosition = vecTempPos1;


        if (vecTempPos.x >= m_vecPriceOffEndPos.x)
        {
            _imgPriceOff.transform.localPosition = m_vecPriceOffEndPos;
            _imgPriceOff_1.transform.localPosition = m_vecPriceOffEndPos_1;

            SetPriceOffStatus(0);
        }


    }

    void EndPriceOffAnimation()
    {

    }

    public void SetFreeFlagVisible( bool bVisible )
    {
        if ( bVisible )
        {
            if ( IsPriceOffPopoVisible() )
            {
                return;
            }
        }

        _goFreeFlag.SetActive( bVisible );
    }

    public void SetRecommendButtonEnabled( bool bEnabled )
    {
        _btnRecommend.enabled = bEnabled;

        if ( bEnabled )
        {
            _imgBtnRecommendSpr.material = null;
        }
        else
        {
            _imgBtnRecommendSpr.material = EffectManager.s_Instance.m_matBlackAndWhite;

        }
    }

} // end class
