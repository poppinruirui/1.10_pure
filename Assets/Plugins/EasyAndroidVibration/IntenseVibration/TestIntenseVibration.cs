using UnityEngine;
using System.Collections;

public class TestIntenseVibration : MonoBehaviour 
{
	void Start()
	{
	}
	
	void Update()
	{
	}
	
	void OnGUI()
	{
		GUILayout.BeginArea(new Rect(Screen.width / 2.0f - 200, 0, 400, Screen.height));
		
		if (GUILayout.Button("Vibrate 50ms", GUILayout.MaxHeight(50)))
		{
			IntenseVibration.Instance.Vibrate(50);
		}
		
		if (GUILayout.Button("Vibrate 200ms", GUILayout.MaxHeight(50)))
		{
			IntenseVibration.Instance.Vibrate(200);
		}
		
		for (int i = 0; i < 10; ++i)
		{
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Vibrate 50ms (Intensity: " + i.ToString() + ")", GUILayout.MaxHeight(50)))
			{
				IntenseVibration.Instance.Vibrate(50, i);
			}
			
			if (GUILayout.Button("Vibrate 200ms (Intensity: " + i.ToString() + ")", GUILayout.MaxHeight(50)))
			{
				IntenseVibration.Instance.Vibrate(200, i);
			}
			GUILayout.EndHorizontal();
		}
		
		GUILayout.Space(25);
		
		if (GUILayout.Button("Vibrate Pattern", GUILayout.MaxHeight(50)))
		{
			long dit = 100;
			long dah = 300;
			long space = 100;
			long[] pattern = new long[16] {space, dit, space, dit, space, dit, space, dah, space, dah, space, dit, space, dit, space, dit};
			
			IntenseVibration.Instance.Vibrate(pattern, -1);
		}
		
		if (GUILayout.Button("Cancel", GUILayout.MaxHeight(50)))
		{
			IntenseVibration.Instance.Cancel();
		}
		
		GUILayout.Space(25);
			
		if (GUILayout.Button("Exit", GUILayout.MaxHeight(50)))
		{
			Application.Quit();
		}
	
		GUILayout.EndArea();
	}
}
